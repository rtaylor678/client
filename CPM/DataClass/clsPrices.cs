﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsPrices : CD.DAC
    {
        private Int32 m_IdModelPrice;
        private Int32 m_IdModel;
        private Int32 m_IdCurrency;
        private Int32 m_IdField;
        private Int32 m_PriceYear;
        private Int32 m_IdTechnicalDriver;
        private Int32 m_IdPriceSenario;
        private Double m_PriceValue;
        private Int32 m_PriceOffset;

        public Int32 IdModelPrice { get { return m_IdModelPrice; } set { m_IdModelPrice = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public Int32 IdCurrency { get { return m_IdCurrency; } set { m_IdCurrency = value; } }
        public Int32 IdField { get { return m_IdField; } set { m_IdField = value; } }
        public Int32 PriceYear { get { return m_PriceYear; } set { m_PriceYear = value; } }
        public Int32 IdTechnicalDriver { get { return m_IdTechnicalDriver; } set { m_IdTechnicalDriver = value; } }
        public Int32 IdPriceScenario { get { return m_IdPriceSenario; } set { m_IdPriceSenario = value; } }
        public Double PriceValue { get { return m_PriceValue; } set { m_PriceValue = value; } }
        public Int32 PriceOffset { get { return m_PriceOffset; } set { m_PriceOffset = value; } }

        /// <summary>
        /// Inserts record into tblModelsPrices
        /// </summary>
        /// <returns>the new IdModelPrice</returns>
        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertModelsPrices";
            listPrm = DAC.LoadParameters("@IdModelPrice",IdModelPrice,"@IdModel", IdModel, "@IdCurrency", IdCurrency, "@IdField", IdField, "@PriceYear", PriceYear, "@IdTechnicalDriver", IdTechnicalDriver, "@IdPriceScenario", IdPriceScenario, "@PriceValue", PriceValue, "@PriceOffset", PriceOffset);
            ExecuteProc(sInsert, listPrm);
            m_IdModelPrice = this.MaxID();
            return m_IdModelPrice;
        }

        /// <summary>
        /// Updates currency record in tblModelsPrices
        /// </summary>
        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdateModelsPrices";
            listPrm = DAC.LoadParameters("@IdModelPrice", IdModelPrice, "@PriceValue", PriceValue, "@PriceOffset", PriceOffset);
            ExecuteProc(sUpdate, listPrm);
        }

        /// <summary>
        /// Deletes currency record form tblModelsPrices
        /// </summary>
        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteModelsPrices";
            listPrm = LoadParameters("@IdModelPrice", IdModelPrice);
            ExecuteProc(sDelete, listPrm);
        }

        /// <summary>
        /// Gets the last IdModelPrice in tblModelsPrices
        /// </summary>
        /// <returns>interger</returns>
        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdModelPrice) FROM tblModelsPrices ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        /// <summary>
        /// List all currencies in a specific Model
        /// </summary>
        /// <param name="_IdModel">Current model to display</param>
        /// <returns>DataTable with existing values in Model</returns>
        public DataTable LoadModelsCurrencies(Int32 _IdModel)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "parpListModelsCurrencies";
            listPrm = LoadParameters("@IdModel", _IdModel);
            return ConsultProc(sConsult, listPrm).Tables[0];
        }

        public DataTable LoadExportList(string strFilter, string strOrder)
        {
            return CD.DAC.ConsultSQL("SELECT Code,Name, Sign FROM tblCurrencies WHERE  " + strFilter + " " + strOrder).Tables[0];
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListPrices " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListPrices WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdModelPrice=" + m_IdModelPrice, "");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdCurrency = (Int32)dr["IdCurrency"];
                m_IdField = (Int32)dr["IdField"];
                m_IdTechnicalDriver = (Int32)dr["IdTechnicalDriver"];
                m_IdPriceSenario = (Int32)dr["IdPriceScenario"];
                m_PriceValue = (Double)dr["PriceValue"];
                m_PriceOffset = (Int32)dr["PriceOffset"];
            }
        }
    }
}
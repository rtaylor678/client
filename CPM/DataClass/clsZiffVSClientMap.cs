﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CD;

namespace DataClass
{
    public class clsZiffVSClientMap : CD.DAC
    {
        public DataTable LoadMap(Int32 pIdModel)
        {
            return CD.DAC.ConsultSQL(
                "SELECT CA.IdClientAccount, CA.Account, CA.Name"
                    + " , CC.IdClientCostCenter, CC.Code, CC.Name AS ResourceName "
                    + " , CC.Code + '_' + CA.Account as ClientAccount "
                    + " , CC.Name + '_' + CA.Name as CliGenAccountDesc "
                    + "  , ISNULL(CCD.Amount,0) Amount "
                    + " , ZA.IdZiffAccount "
                    + " , ZA.Code as ZiffCode "
                    + " , ZA.Name as ZiffName "
                    //+ " FROM tblActivities A "
                    //+ " INNER JOIN tblResources R on R.IdActivity = A.IdActivity "
                    //+ " INNER JOIN tblClientAccounts CA on CA.IdResource = R.IdResources "
                    //+ " INNER JOIN tblClientCostCenters CC on CC.IdActivity = A.IdActivity "
                    //+ " LEFT JOIN tblClientCostData CCD on CCD.IdClientAccount = CA.IdClientAccount AND CCD.IdClientCostCenter = CC.IdClientCostCenter "
                    //+ " LEFT OUTER JOIN tblZiffClientMap ZCM on (CA.IdClientAccount = ZCM.IdClientAccount AND CC.IdClientCostCenter = ZCM.IdClientCostCenter) "
                    //+ " LEFT OUTER JOIN tblZiffAccounts ZA on ZCM.IdZiffAccount = ZA.IdZiffAccount "
                    //+ " WHERE A.IdModel = " + pIdModel.ToString()
                    + " FROM tblClientCostData CCD "
                    + " INNER JOIN tblClientCostCenters CC ON cc.IdClientCostCenter = CCD.IdClientCostCenter "
                    + " INNER JOIN tblClientAccounts CA ON CA.IdClientAccount=CCD.IdClientAccount "
                    + " INNER JOIN tblActivities A ON A.IdActivity = CC.IdActivity "
                    + " INNER JOIN tblResources R ON R.IdResources = CA.IdResource "
                    + " LEFT OUTER JOIN tblZiffClientMap ZCM ON (CA.IdClientAccount = ZCM.IdClientAccount AND CC.IdClientCostCenter = ZCM.IdClientCostCenter) "
                    + " LEFT OUTER JOIN tblZiffAccounts ZA on ZCM.IdZiffAccount = ZA.IdZiffAccount "
                    + " WHERE CCD.IdModel = " + pIdModel.ToString()
                    + " ORDER BY IdClientCostCenter  "
            ).Tables[0];
        }

        public DataTable LoadList(string strFilter, string strOrder)
        {
            return CD.DAC.ConsultSQL("select IdZiffAccount, Code, RTRIM(Name) AS Name "
                + "from tblZiffAccounts "
                + "WHERE  " + strFilter + " " + strOrder).Tables[0];
        }

        public void SaveMap(Int32 pIdCliAcnt, Int32 pIdCliCostCenter, Int32 pIdZiffAcnt)
        {
            String sQuery;
            List<SqlParameter> listPrm;
            sQuery = "parpSaveMap";
            listPrm = DAC.LoadParameters(
                    "@IdClientAccount", pIdCliAcnt,
                    "@IdClientCostCenter", pIdCliCostCenter,
                    "@IdZiffAccount", pIdZiffAcnt
                    );
            ExecuteProc(sQuery, listPrm);
        }

        public void DeleteAll(Int32 IdModel)
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeleteAllMap";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 getIdZiffAccount(Int32 pIdClientAcnt)
        {
            DataTable dtRes = CD.DAC.ConsultSQL(
                "SELECT IdZiffAccount"
                + " FROM tblZiffClientMap"
                + " WHERE IdClientAccount = " + pIdClientAcnt.ToString()
                ).Tables[0];

            Int32 intRes = 0;

            if (dtRes.Rows.Count > 0)
            {
                DataRow drFila = dtRes.Rows[0];
                intRes = Convert.ToInt32(drFila["IdZiffAccount"].ToString());
            }

            return intRes;
        }
    }
}

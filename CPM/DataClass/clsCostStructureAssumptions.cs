﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using CD;

namespace DataClass
{
    public class clsCostStructureAssumptions : CD.DAC
    {

        //public DataTable LoadList(string strFilter, string strOrder)
        //{
        //    if (strFilter == String.Empty)
        //    {
        //        return CD.DAC.ConsultSQL("SELECT * FROM vListCostStructureAssumptionsByProject " + strOrder).Tables[0];
        //    }
        //    else
        //    {
        //        return CD.DAC.ConsultSQL("SELECT * FROM vListCostStructureAssumptionsByProject WHERE  " + strFilter + " " + strOrder).Tables[0];
        //    }
        //}

        public DataTable LoadList(Int32 _IdModel, Int32 _IdField)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "allpListCostStructureAssumptionsByProject";
            listPrm = LoadParameters("@IdModel", _IdModel, "@IdField", _IdField);
            return ConsultProc(sConsult, listPrm).Tables[0];
        }

        public void UpdateRecord(Int32 pIdCostStructureAssumptions, Int32 pIdProject, Int32 pIdZiffAccount, Int32 pClient, Int32 pZiff, Int32 pIdPeerGroup, Int32 pIdField)
        {
            String sQuery;
            List<SqlParameter> listPrm;
            sQuery = "allpUpdateCostStrucAssumptionByProj";
            listPrm = LoadParameters("@pIdCostStructureAssumptions", pIdCostStructureAssumptions, "@pIdProject", pIdProject, "@pIdZiffAcnt", pIdZiffAccount, "@pClient", pClient, "@pZiff", pZiff, "@pIdPeerGroup", pIdPeerGroup, "@pIdField", pIdField);
            ExecuteProc (sQuery, listPrm);
        }

        public Int32 GetIdCostStructureAssumptions(Int32 pIdProject, Int32 pIdZiffAccount, Int32 pIdPeerGroup, Int32 pIdField)
        {
            DataTable datTmp = CD.DAC.ConsultSQL("SELECT IdCostStructureAssumptions "
                + "FROM tblCostStructureAssumptionsByProject "
                + "WHERE IdProject = " + pIdProject
                + " AND IdField = " + pIdField
                + " AND IdZiffAccount = " + pIdZiffAccount
                + " AND IdPeerGroup = " + pIdPeerGroup + "").Tables[0];

            if (datTmp.Rows.Count > 0)
            {
                return Convert.ToInt32(datTmp.Rows[0]["IdCostStructureAssumptions"].ToString());
            }
            else
            {
                return 0;
            }
        }

        public DataTable GetIdCostStructureAssumptions(Int32 pIdZiffAccount, Int32 pIdPeerGroup)
        {
            DataTable datTmp;
                
            return datTmp = CD.DAC.ConsultSQL("SELECT IdCostStructureAssumptions "
                + "FROM tblCostStructureAssumptionsByProject "
                + "WHERE IdZiffAccount = " + pIdZiffAccount
                + " AND IdPeerGroup = " + pIdPeerGroup + "").Tables[0];
        }

        public Int32 GetIdPeerGroup(Int32 pIdModel, String pPeerGroupDescription)
        {
            DataTable datTmp = CD.DAC.ConsultSQL("SELECT IdPeerGroup "
                + "FROM tblPeerGroup "
                + "WHERE IdModel = " + pIdModel
                + " AND PeerGroupCode = '" + pPeerGroupDescription + "'").Tables[0];

            if (datTmp.Rows.Count > 0)
            {
                return Convert.ToInt32(datTmp.Rows[0]["IdPeerGroup"].ToString());
            }
            else
            {
                return 0;
            }
        }

        public DataTable GetProjects(Int32 pIdModel, Int32 pIdField)
        {
            DataTable datTmp;

            return datTmp = CD.DAC.ConsultSQL("SELECT IdProject "
                + "FROM tblProjects "
                + "WHERE IdModel = " + pIdModel
                + " AND IdField = " + pIdField 
                + " AND Operation = 1").Tables[0];
        }

        public DataTable LoadExport(Int32 pIdModel)
        {
            DataTable datTmp;

            return datTmp = CD.DAC.ConsultSQL("SELECT tPG.PeerGroupCode, tZA.Code, tCSABP.Ziff, tZA.SortOrder "
                + "FROM tblCostStructureAssumptionsByProject tCSABP INNER JOIN tblPeerGroup tPG "
                + "ON tPG.IdPeerGroup = tCSABP.IdPeerGroup INNER JOIN tblZiffAccounts tZA "
                + "ON tZA.IdZiffAccount = tCSABP.IdZiffAccount "
                + "WHERE tCSABP.IdPeerGroup IN (SELECT IdPeerGroup FROM tblPeerGroup WHERE IdModel = " + pIdModel + " ) "
                + "GROUP BY tPG.PeerGroupCode, tZA.SortOrder, tZA.Code, tCSABP.Ziff, tZA.SortOrder "
                + "ORDER BY tPG.PeerGroupCode, tZA.SortOrder").Tables[0];
        }

        public void UpdateFields(Int32 _IdField, Int32 _IdPeerGroup)
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "allpUpdateCostStrucAssumptionFieldPeerGroup";
            listPrm = DAC.LoadParameters("@pIdField", _IdField, "@pIdPeerGroup", _IdPeerGroup);
            ExecuteProc(sUpdate, listPrm);

        }

    }
}
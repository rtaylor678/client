﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsAggregationLevels : CD.DAC
    {

        private Int32 m_IdAggregationLevel;
        private Int32 m_IdModel;
        private String m_Code;
        private String m_Name;
        private Object m_Parent;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;

        public Int32 IdAggregationLevel { get { return m_IdAggregationLevel; } set { m_IdAggregationLevel = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public String Code { get { return m_Code; } set { m_Code = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }
        public Object Parent { get { return m_Parent; } set { m_Parent = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertAggregationLevel";
            listPrm = DAC.LoadParameters("@IdAggregationLevel", IdAggregationLevel, "@IdModel", IdModel, "@Code", Code, "@Name", Name, "@Parent", Parent, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdAggregationLevel = this.MaxID();
            return m_IdAggregationLevel;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdateAggregationLevel";
            listPrm = DAC.LoadParameters("@IdAggregationLevel", IdAggregationLevel, "@IdModel", IdModel, "@Code", Code, "@Name", Name, "@Parent", Parent, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        public void UpdateAggregationLevelRelationLevel(Int32 _MaxRelationLevels)
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdateAggregationLevelRelationLevel";
            listPrm = DAC.LoadParameters("@IdModel", IdModel, "@MaxRelationLevels", _MaxRelationLevels);
            ExecuteProc(sUpdate, listPrm);
        }

        public void DeleteAll()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteAllAggregationLevels";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteAggregationLevel";
            listPrm = LoadParameters("@IdAggregationLevel", IdAggregationLevel);
            ExecuteProc(sDelete, listPrm);
        }

        public DataTable LoadModelsAggregationLevel(Int32 _IdModel, Int32 _Parent)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "parpListAggregationLevel";
            listPrm = LoadParameters("@IdModel", _IdModel, "@Parent", _Parent);
            return ConsultProc(sConsult, listPrm).Tables[0];
        }

        public DataTable LoadFilterModelsAggregationLevel(Int32 _IdModel, Int32 _Parent, String _Name)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "parpListFilterAggregationLevel";
            listPrm = LoadParameters("@IdModel", _IdModel, "@Parent", _Parent, "@Name", _Name);
            return ConsultProc(sConsult, listPrm).Tables[0];
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdAggregationLevel) FROM tblAggregationLevels ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadComboBox(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT 0 ID, 'None' Name, 'None' Parentname UNION ALL SELECT IdAggregationLevel ID, Name, Parentname FROM vListAggregationLevels " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT 0 ID, 'None' Name, 'None' Parentname UNION ALL SELECT IdAggregationLevel ID, Name, Parentname FROM vListAggregationLevels WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadExportList(string strFilter, string strOrder)
        {
            return CD.DAC.ConsultSQL("SELECT Code,Name,ParentCode,RelationLevel FROM vListExportAggregationLevels WHERE  " + strFilter + " " + strOrder).Tables[0];
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListAggregationLevels " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListAggregationLevels WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdAggregationLevel=" + m_IdAggregationLevel, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdAggregationLevel = (Int32)dr["IdAggregationLevel"];
                m_IdModel = (Int32)dr["IdModel"];
                m_Code = (String)dr["Code"];
                m_Name = (String)dr["Name"];
                m_Parent = (Object)dr["Parent"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }
        }

    }
}
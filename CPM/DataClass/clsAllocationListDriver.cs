﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsAllocationListDriver : CD.DAC
    {

        private Int32 m_IdAllocationListDriver;
        private Int32 m_IdModel;
        private String m_Name;
        private Int32 m_IdUnit;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;
        private Boolean m_UnitCostDenominator;

        public Int32 IdAllocationListDriver { get { return m_IdAllocationListDriver; } set { m_IdAllocationListDriver = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }
        public Int32 IdUnit { get { return m_IdUnit; } set { m_IdUnit = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }
        public Boolean UnitCostDenominator { get { return m_UnitCostDenominator; } set { m_UnitCostDenominator = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "allpInsertAllocationListDriver";
            listPrm = DAC.LoadParameters("@IdAllocationListDriver", IdAllocationListDriver, "@IdModel", IdModel, "@Name", Name, "@IdUnit", IdUnit, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification, "@UnitCostDenominator", UnitCostDenominator);
            ExecuteProc(sInsert, listPrm);
            m_IdAllocationListDriver = this.MaxID();
            return m_IdAllocationListDriver;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "allpUpdateAllocationListDriver";
            listPrm = DAC.LoadParameters("@IdAllocationListDriver", IdAllocationListDriver, "@IdModel", IdModel, "@Name", Name, "@IdUnit", IdUnit, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification, "@UnitCostDenominator", UnitCostDenominator);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeleteAllocationListDriver";
            listPrm = LoadParameters("@IdAllocationListDriver", IdAllocationListDriver);
            ExecuteProc(sDelete, listPrm);
        }

        public void DeleteAll()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeleteAllAllocationListDriver";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdAllocationListDriver) FROM vListAllocationListDriver ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadComboBox(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT IdAllocationListDriver ID, Name, Unit, TypeUnit FROM vListAllocationListDriver " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT IdAllocationListDriver ID, Name, Unit, TypeUnit FROM vListAllocationListDriver WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadExportList(string strFilter, string strOrder)
        {
            return CD.DAC.ConsultSQL("SELECT Name,TypeOperation FROM vListAllocationListDriver WHERE  " + strFilter + " " + strOrder).Tables[0];
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListAllocationListDriver " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListAllocationListDriver WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdAllocationListDriver=" + m_IdAllocationListDriver, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdAllocationListDriver = (Int32)dr["IdAllocationListDriver"];
                m_IdModel = (Int32)dr["IdModel"];
                m_Name = (String)dr["Name"];
                m_IdUnit = (Int32)dr["IdUnit"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
                m_UnitCostDenominator = (Boolean)dr["UnitCostDenominator"];
            }
        }

    }
}
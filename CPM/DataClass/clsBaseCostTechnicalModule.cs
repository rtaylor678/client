﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsBaseCostTechnicalModule : CD.DAC
    {

        #region Declarations
        #endregion

        #region Methods

        public DataTable LoadBaseCostTechnicalModule(int _IdModel, int _IdField, int _IdStage, int _BaseCostType, int _Currency, int _IdLanguage)
        {
            String sList;
            List<SqlParameter> listPrm;
            sList = "parpBaseCostTechnicalModule";
            listPrm = LoadParameters("@IdModel", _IdModel, "@IdField", _IdField, "@IdStage", _IdStage, "@BaseCostType", _BaseCostType, "@IdCurrency", _Currency, "@IdLanguage", _IdLanguage);
            return ConsultProc(sList, listPrm).Tables[0];
        }

        public DataTable LoadList(Int32 pIdModel, Int32 pIdField)
        {
            return CD.DAC.ConsultSQL("SELECT Code, Name " +
                " FROM tblParameters " +
                " WHERE  Type = 'BaseCost' AND Name Not LIKE '%External%' AND IdLanguage = 1 " +
                " UNION " +
                " SELECT IdPeerGroup, PeerGroupCode" +
                " FROM tblPeerGroup" +
                " WHERE IdModel = " + pIdModel + 
                " AND IdPeerGroup IN (SELECT IdPeerGroup FROM tblPeerGroupField WHERE IdField = " + pIdField + ")").Tables[0];
        }

        #endregion

    }
}
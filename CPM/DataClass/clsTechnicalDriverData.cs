﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsTechnicalDriverData : CD.DAC
    {

        private Int32 m_IdTechnicalDriverData;
        private Int32 m_IdModel;
        private Int32 m_IdTechnicalDriver;
        private Int32 m_IdField;
        private Int32 m_IdProject;
        private Int32 m_Year;
        private Double m_Normal;
        private Double m_Pessimistic;
        private Double m_Optimistic;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;

        public Int32 IdTechnicalDriverData { get { return m_IdTechnicalDriverData; } set { m_IdTechnicalDriverData = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public Int32 IdTechnicalDriver { get { return m_IdTechnicalDriver; } set { m_IdTechnicalDriver = value; } }
        public Int32 IdField { get { return m_IdField; } set { m_IdField = value; } }
        public Int32 IdProject { get { return m_IdProject; } set { m_IdProject = value; } }
        public Int32 Year { get { return m_Year; } set { m_Year = value; } }
        public Double Normal { get { return m_Normal; } set { m_Normal = value; } }
        public Double Pessimistic { get { return m_Pessimistic; } set { m_Pessimistic = value; } }
        public Double Optimistic { get { return m_Optimistic; } set { m_Optimistic = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertTechnicalDriverData";
            listPrm = DAC.LoadParameters("@IdTechnicalDriverData", IdTechnicalDriverData, "@IdModel", IdModel, "@IdTechnicalDriver", IdTechnicalDriver, "@IdField", IdField, "@IdProject", IdProject, "@Year", Year, "@Normal", Normal, "@Pessimistic", Pessimistic, "@Optimistic", Optimistic, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdTechnicalDriverData = this.MaxID();
            return m_IdTechnicalDriverData;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdateTechnicalDriverData";
            listPrm = DAC.LoadParameters("@IdTechnicalDriverData", IdTechnicalDriverData, "@IdModel", IdModel, "@IdTechnicalDriver", IdTechnicalDriver, "@IdField", IdField, "@IdProject", IdProject, "@Year", Year, "@Normal", Normal, "@Pessimistic", Pessimistic, "@Optimistic", Optimistic, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteTechnicalDriverData";
            listPrm = LoadParameters("@IdTechnicalDriverData", IdTechnicalDriverData);
            ExecuteProc(sDelete, listPrm);
        }

        public void DeleteAll()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteAllTechnicalDriverData";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdTechnicalDriverData) FROM tblTechnicalDriverData ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadComboBox(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT IdTechnicalDriverData ID, Year, Normal, Pessimistic, Optimistic FROM tblTechnicalDriverData " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT IdTechnicalDriverData ID, Year, Normal, Pessimistic, Optimistic FROM tblTechnicalDriverData WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadExportList(string strFilter, string strOrder)
        {
            return CD.DAC.ConsultSQL("SELECT IdTechnicalDriverData, Year, Normal, Pessimistic, Optimistic FROM vListTechnicalDriverData WHERE  " + strFilter + " " + strOrder).Tables[0];
        }
        
        public DataTable LoadList()
        {
            String sList;
            List<SqlParameter> listPrm;
            sList = "parpListTechnicalDriverData";
            listPrm = LoadParameters("@IdModel", IdModel, "@IdField", IdField, "@IdTechnicalDriver", IdTechnicalDriver, "@IdProject", IdProject);
            return ConsultProc(sList, listPrm).Tables[0];
        }

        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListTechnicalDriverData " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListTechnicalDriverData WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdTechnicalDriverData=" + m_IdTechnicalDriverData, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdTechnicalDriverData = (Int32)dr["IdTechnicalDriverData"];
                m_IdModel = (Int32)dr["IdModel"];
                m_IdTechnicalDriver = (Int32)dr["IdTechnicalDriver"];
                m_IdField = (Int32)dr["IdField"];
                m_IdProject = (Int32)dr["IdProject"];
                m_Year = (Int32)dr["Year"];
                m_Normal = Convert.ToDouble(dr["Normal"]);
                m_Pessimistic = Convert.ToDouble(dr["Pessimistic"]);
                m_Optimistic = Convert.ToDouble(dr["Optimistic"]);
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }
        }

        public DataTable LoadListExport(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListTechnicalDriverDataExport " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListTechnicalDriverDataExport WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsClientCostCenter : CD.DAC
    {

        private Int32 m_IdClientCostCenter;
        private Int32 m_IdModel;
        private String m_Code;
        private String m_Name;
        private Int32 m_IdActivity;
        private Int32 m_TypeAllocation;
        private Int32 m_OldTypeAllocation;
        private Object m_Notes  ;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;

        public Int32 IdClientCostCenter { get { return m_IdClientCostCenter; } set { m_IdClientCostCenter = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public String Code { get { return m_Code; } set { m_Code = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }
        public Int32 IdActivity { get { return m_IdActivity; } set { m_IdActivity = value; } }
        public Int32 TypeAllocation { get { return m_TypeAllocation; } set { m_TypeAllocation = value; } }
        public Int32 OldTypeAllocation { get { return m_OldTypeAllocation; } set { m_OldTypeAllocation = value; } }
        public Object Notes { get { return m_Notes; } set { m_Notes = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertClientCostCenters";
            listPrm = DAC.LoadParameters("@IdClientCostCenter", IdClientCostCenter, "@IdModel", IdModel, "@Code", Code, "@Name", Name, "@IdActivity", IdActivity, "@TypeAllocation", TypeAllocation, "@Notes", @Notes, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdClientCostCenter = this.MaxID();
            return m_IdClientCostCenter;
        }

        public int InsertImport(String _ParentCode)
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertImportClientCostCenters";
            listPrm = DAC.LoadParameters("@IdClientCostCenter", IdClientCostCenter, "@IdModel", IdModel, "@Code", Code, "@Name", Name, "@IdActivity", IdActivity, "@TypeAllocation", TypeAllocation, "@Notes", @Notes, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdClientCostCenter = this.MaxID();
            return m_IdClientCostCenter;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdateClientCostCenters";
            listPrm = DAC.LoadParameters("@IdClientCostCenter", IdClientCostCenter, "@IdModel", IdModel, "@Code", Code, "@Name", Name, "@IdActivity", IdActivity, "@TypeAllocation", TypeAllocation, "@OldTypeAllocation", OldTypeAllocation, "@Notes", @Notes, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteClientCostCenters";
            listPrm = LoadParameters("@IdClientCostCenter", IdClientCostCenter);
            ExecuteProc(sDelete, listPrm);
        }

        public void DeleteAll()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteAllClientCostCenters";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdClientCostCenter) FROM tblClientCostCenters ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadExportList(string strFilter, string strOrder)
        {
            return CD.DAC.ConsultSQL("SELECT Code,Name FROM vListClientCostCenter WHERE  " + strFilter + " " + strOrder).Tables[0];
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListClientCostCenter " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListClientCostCenter WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdClientCostCenter=" + m_IdClientCostCenter, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdClientCostCenter = (Int32)dr["IdClientCostCenter"];
                m_IdModel = (Int32)dr["IdModel"];
                m_Code = (String)dr["Code"];
                m_Name = (String)dr["Name"];
                m_IdActivity = (Int32)dr["IdActivity"];
                m_TypeAllocation = (Int32)dr["TypeAllocation"];
                m_Notes = (Object)dr["Notes"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }
        }

    }
}
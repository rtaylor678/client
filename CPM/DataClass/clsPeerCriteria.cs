﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsPeerCriteria : CD.DAC
    {

        #region Declarations

        private Int32 m_IdPeerCriteria;
        private Int32 m_IdModel;
        private String m_Description;
        private Int32 m_IdUnitPeerCriteria;
        private Boolean m_TotalUnitCostDenominator;
       
        public Int32 IdPeerCriteria { get { return m_IdPeerCriteria; } set { m_IdPeerCriteria = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public String Description { get { return m_Description; } set { m_Description = value; } }
        public Int32 IdUnitPeerCriteria { get { return m_IdUnitPeerCriteria; } set { m_IdUnitPeerCriteria = value; } }
        public Boolean TotalUnitCostDenominator { get { return m_TotalUnitCostDenominator; } set { m_TotalUnitCostDenominator = value; } }

        #endregion

        #region Methods

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "allpInsertPeerCriteria";
            listPrm = DAC.LoadParameters("@IdPeerCriteria", IdPeerCriteria, "@IdModel", IdModel, "@Description", Description, "@IdUnitPeerCriteria", IdUnitPeerCriteria, "@TotalUnitCostDenominator", TotalUnitCostDenominator);
            ExecuteProc(sInsert, listPrm);
            m_IdPeerCriteria = this.MaxID();
            return m_IdPeerCriteria;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "allpUpdatePeerCriteria";
            listPrm = DAC.LoadParameters("@IdPeerCriteria", IdPeerCriteria, "@IdModel", IdModel, "@Description", Description, "@IdUnitPeerCriteria", IdUnitPeerCriteria, "@TotalUnitCostDenominator", TotalUnitCostDenominator);
            ExecuteProc(sUpdate, listPrm);
        }

        public void DeleteAll()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeleteAllPeerCriteria";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeletePeerCriteria";
            listPrm = LoadParameters("@IdPeerCriteria", IdPeerCriteria);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdPeerCriteria) FROM tblPeerCriteria ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT * "
                + " FROM vListPeerCriteria "
                + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * "
                + " FROM vListPeerCriteria "
                + " WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdPeerCriteria =" + m_IdPeerCriteria, " ORDER BY Description");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdPeerCriteria = (Int32)dr["IdPeerCriteria"];
                m_IdModel = (Int32)dr["IdModel"];
                m_Description = (String)dr["Description"];
                m_IdUnitPeerCriteria = (Int32)dr["IdUnitPeerCriteria"];
                m_TotalUnitCostDenominator = (Boolean)dr["TotalUnitCostDenominator"];
            }
        }

        #endregion

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsFilesRepository : CD.DAC
    {

        private Int32 m_IdFileRepository;
        private Int32 m_IdModel;
        private Byte[] m_AttachFile;
        private String m_FileName;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;
        private String m_Comments;

        public Int32 IdFileRepository { get { return m_IdFileRepository; } set { m_IdFileRepository = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public Byte[] AttachFile { get { return m_AttachFile; } set { m_AttachFile = value; } }
        public String FileName { get { return m_FileName; } set { m_FileName = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }
        public String Comments { get { return m_Comments; } set { m_Comments = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "allpInsertFilesToRepository";
            listPrm = DAC.LoadParameters("@IdFileRepository", IdFileRepository, "@IdModel", IdModel, "@AttachFile", AttachFile, "@FileName", FileName, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification, "@Comments", Comments);
            ExecuteProc(sInsert, listPrm);
            m_IdFileRepository = this.MaxID();
            return m_IdFileRepository;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "allpUpdateFilesInRepository";
            listPrm = DAC.LoadParameters("@IdFileRepository", IdFileRepository, "@IdModel", IdModel, "@AttachFile", AttachFile, "@FileName", FileName, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification, "@Comments", Comments);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeleteFilesInRepository";
            listPrm = LoadParameters("@IdFileRepository", IdFileRepository);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdFileRepository) FROM tblFilesRepository ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }        
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListFilesRepository " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListFilesRepository WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdFileRepository=" + m_IdFileRepository, " ORDER BY IdFileRepository");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdFileRepository = (Int32)dr["IdFileRepository"];
                m_IdModel = (Int32)dr["IdModel"];
                m_AttachFile = (Byte[])dr["AttachFile"];
                m_FileName = (String)dr["FileName"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
                if (dr["Comments"] == System.DBNull.Value)
                    m_Comments = "";
                else
                    m_Comments = (String)dr["Comments"];
            }
        }

        public DataTable LoadComboBox(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT [IdParameters],[Code],[Name] FROM [tblParameters] WHERE [Type] = 'FilesRepository'" + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT [IdParameters],[Code],[Name] FROM [tblParameters] WHERE [Type] = 'FilesRepository' AND " + strFilter + " " + strOrder).Tables[0];
            }
        }

    }
}

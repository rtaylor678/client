﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsKeyPerformanceIndicatorsTargetsData : CD.DAC
    {
        #region Methods
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListKeyPerformanceIndicatorsTargetsData " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListKeyPerformanceIndicatorsTargetsData WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadList(Int32 _IdModel, Int32 _IdAggregationLevel, Int32 _IdField, Int32 _IdStructure, Int32 _IdCurrency, Int32 _From, Int32 _To, Int32 _IdTerm, Int32 _IdScenario, Int32 _IdEconomicScenario, Int32 _IdUnit, Int32 _IdTechnicalDriver)
        {
            String sList;
            List<SqlParameter> listPrm;
            sList = "parpListKeyPerformanceIndicatorsTargets";
            listPrm = LoadParameters("@IdModel", _IdModel, "@IdAggregationLevel", _IdAggregationLevel, "@IdField", _IdField, "@IdStructure", _IdStructure, "@IdCurrency", _IdCurrency, "@From", _From, "@To", _To, "@IdTerm", _IdTerm, "@IdScenario", _IdScenario, "@IdEconomicScenario", _IdEconomicScenario, "@IdUnit", _IdUnit, "@IdTechnicalDriver", _IdTechnicalDriver);
            return ConsultProc(sList, listPrm).Tables[0];
        }

        public DataTable LoadZiffParentAccounts(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT IdZiffAccount, Code, Name"
                    + " FROM tblZiffAccounts"
                    + " WHERE Parent IS null " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT IdZiffAccount, Code, Name "
                    + " FROM tblZiffAccounts "
                    + " WHERE Parent IS null "
                    + " AND " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadZiffAccounts(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT IdZiffAccount, Code, Name, Parent FROM tblZiffAccounts " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT IdZiffAccount, Code, Name, Parent FROM tblZiffAccounts WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadKeyPerformanceIndicatorsTargetsDataPivot(Int32 _IdTechnicalDriver, Int32 _IdField, Int32 _IdZiffAccount)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "parpListKeyPerformanceIndicatorsTargetsDataPivot";
            listPrm = LoadParameters("@IdTechnicalDriver", _IdTechnicalDriver, "@IdField", _IdField, "@IdZiffAccount", _IdZiffAccount);
            return ConsultProc(sConsult, listPrm).Tables[0];
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsOperationType : CD.DAC
    {
        private Int32 m_IdOperationType;
        private String m_Code;
        private String m_Name;

        public Int32 IdCostType { get { return m_IdOperationType; } set { m_IdOperationType = value; } }
        public String Code { get { return m_Code; } set { m_Code = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }

        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT distinct tCAL.[IdTypeOperation] ,tTO.[Name] FROM [tblCalcAggregationLevelsField] tCAL LEFT JOIN [tblTypesOperation] tTO ON tCAL.IdTypeOperation = tTO.IdTypeOperation " + strOrder).Tables[0];
                    //"SELECT DISTINCT [vListTypesOperation].[IdTypeOperation],[vListTypesOperation].[Name],[vListTypesOperation].[VisualName],[vListTypesOperation].[ParentName],[vListTypesOperation].[ShoreDesc],[vListTypesOperation].[TypeDesc],[vListTypesOperation].[LastNode],[vListTypesOperation].[Shore],[vListTypesOperation].[IdModel] FROM [vListTypesOperation] LEFT JOIN tblFields ON tblFields.IdModel = [vListTypesOperation].IdModel AND tblFields.IdTypeOperation = [vListTypesOperation].IdTypeOperation " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT distinct tCAL.[IdTypeOperation] ,tTO.[Name] FROM [tblCalcAggregationLevelsField] tCAL LEFT JOIN [tblTypesOperation] tTO ON tCAL.IdTypeOperation = tTO.IdTypeOperation  WHERE " + strFilter + " " + strOrder).Tables[0];
                    //"SELECT DISTINCT [vListTypesOperation].[IdTypeOperation],[vListTypesOperation].[Name],[vListTypesOperation].[VisualName],[vListTypesOperation].[ParentName],[vListTypesOperation].[ShoreDesc],[vListTypesOperation].[TypeDesc],[vListTypesOperation].[LastNode],[vListTypesOperation].[Shore],[vListTypesOperation].[IdModel] FROM [vListTypesOperation] LEFT JOIN tblFields ON tblFields.IdModel = [vListTypesOperation].IdModel AND tblFields.IdTypeOperation = [vListTypesOperation].IdTypeOperation WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsClientCostData : CD.DAC
    {

        private Int32 m_IdClientCostData;
        private Int32 m_IdModel;
        private Int32 m_IdClientCostCenter;
        private Int32 m_IdClientAccount;
        private Double m_Amount;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;

        public Int32 IdClientCostData { get { return m_IdClientCostData; } set { m_IdClientCostData = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public Int32 IdClientCostCenter { get { return m_IdClientCostCenter; } set { m_IdClientCostCenter = value; } }
        public Int32 IdClientAccount { get { return m_IdClientAccount; } set { m_IdClientAccount = value; } }
        public Double Amount { get { return m_Amount; } set { m_Amount = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "allpInsertClientCostData";
            listPrm = DAC.LoadParameters("@IdClientCostData", IdClientCostData, "@IdModel", IdModel, "@IdClientCostCenter", IdClientCostCenter, "@IdClientAccount", IdClientAccount, "@Amount", Amount, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdClientCostData = this.MaxID();
            return m_IdClientCostData;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "allpUpdateClientCostData";
            listPrm = DAC.LoadParameters("@IdClientCostData", IdClientCostData, "@IdModel", IdModel, "@IdClientCostCenter", IdClientCostCenter, "@IdClientAccount", IdClientAccount, "@Amount", Amount, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeleteClientCostData";
            listPrm = LoadParameters("@IdClientCostData", IdClientCostData);
            ExecuteProc(sDelete, listPrm);
        }

        public void DeleteAll()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeleteAllClientCostData";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdClientCostData) FROM tblClientCostData ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListClientCostData " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListClientCostData WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdClientCostData=" + m_IdClientCostData, " ORDER BY IdClientCostData");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdClientCostData = (Int32)dr["IdClientCostData"];
                m_IdModel = (Int32)dr["IdModel"];
                m_IdClientCostCenter = (Int32)dr["IdClientCostCenter"];
                m_IdClientAccount = (Int32)dr["IdClientAccount"];
                m_Amount = Convert.ToDouble(dr["Amount"]);
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }
        }

    }
}
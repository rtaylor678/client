﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsBaseCostByField : CD.DAC
    {

        #region Declarations
        #endregion

        #region Methods

        public DataTable LoadRows(Int32 IdModel, Int32 IdStructure)
        {
            switch (IdStructure)
            {
                case 1:
                    return CD.DAC.ConsultSQL("SELECT IdRootZiffAccount "
                                                + ", RootZiffAccount "
                                                + ", IdZiffAccount "
                                                + ", ZiffAccount "
                                            + "FROM [tblCalcBaseCostByField] "
                                            + "WHERE IdModel = " + IdModel + " AND IdRootZiffAccount IS NOT NULL "
                                            + "GROUP BY IdRootZiffAccount "
                                                + ", RootZiffAccount "
                                                + ", IdZiffAccount "
                                                + ", ZiffAccount "
                                            + "ORDER BY RootZiffAccount "
                                                + ", ZiffAccount").Tables[0];
                default:
                    return CD.DAC.ConsultSQL("SELECT IdActivity "
                                                + ", Activity "
                                                + ", IdResources "
                                                + ", Resource "
                                            + "FROM [tblCalcBaseCostByField] "
                                            + "WHERE IdModel = " + IdModel + " "
                                            + "GROUP BY IdActivity "
                                                + ", Activity "
                                                + ", IdResources "
                                                + ", Resource "
                                            + "ORDER BY cast(LEFT(Activity,PATINDEX('%[^0-9]%',Activity)-1) as int) "//Activity "//
                                                + ", Resource").Tables[0];
            }
        }

        public DataTable LoadReport(Int32 IdModel, Int32 IdStructure)
        {
            switch (IdStructure)
            {
                case 1:
                    return CD.DAC.ConsultSQL("SELECT IdRootZiffAccount "
                                                + ", RootZiffAccount "
                                                + ", IdZiffAccount "
                                                + ", ZiffAccount "
                                                + ", IdField "
                                                + ", Field "
                                                + ", SUM(ISNULL(Amount,0)) AS Amount "
                                            + "FROM [tblCalcBaseCostByField] "
                                            + "WHERE IdModel = " + IdModel + " AND IdRootZiffAccount IS NOT NULL "
                                            + "GROUP BY IdRootZiffAccount "
                                                + ", RootZiffAccount "
                                                + ", IdZiffAccount "
                                                + ", ZiffAccount "
                                                + ", IdField "
                                                + ", Field "
                                            + "ORDER BY RootZiffAccount "
                                                + ", ZiffAccount").Tables[0];
                default:
                    return CD.DAC.ConsultSQL("SELECT IdActivity "
                                                + ", Activity "
                                                + ", IdResources "
                                                + ", Resource "
                                                + ", IdField "
                                                + ", Field "
                                                + ", SUM(ISNULL(Amount,0)) AS Amount "
                                            + "FROM [tblCalcBaseCostByField] "
                                            + "WHERE IdModel = " + IdModel + " "
                                            + "GROUP BY IdActivity "
                                                + ", Activity "
                                                + ", IdResources "
                                                + ", Resource "
                                                + ", IdField "
                                                + ", Field "
                                            + "ORDER BY cast(LEFT(Activity,PATINDEX('%[^0-9]%',Activity)-1) as int) "//Activity "//
                                                + ", Resource "
                                                + ", Field").Tables[0];
            }
        }

        public DataTable LoadVolumeRows(Int32 IdModel, Int32 IdStructure)
        {
            switch (IdStructure)
            {
                case 1:
                    return CD.DAC.ConsultSQL("SELECT dbo.tblAllocationListDrivers.IdAllocationListDriver "
                                                + ", dbo.tblAllocationListDrivers.Name "
                                            + "FROM dbo.tblAllocationDriversByField INNER JOIN "
                                                + " dbo.tblAllocationListDrivers ON dbo.tblAllocationDriversByField.IdAllocationListDriver = dbo.tblAllocationListDrivers.IdAllocationListDriver INNER JOIN "
                                                + " dbo.tblFields ON dbo.tblAllocationDriversByField.IdField = dbo.tblFields.IdField "
                                            + "WHERE dbo.tblAllocationListDrivers.IdModel =  " + IdModel + " "
                                            + "AND	 dbo.tblAllocationListDrivers.UnitCostDenominator = 1 "
                                            + "GROUP BY dbo.tblAllocationListDrivers.IdAllocationListDriver "
                                                + ", dbo.tblAllocationListDrivers.Name ").Tables[0];
                default:
                    return CD.DAC.ConsultSQL("SELECT dbo.tblAllocationListDrivers.IdAllocationListDriver "
                                        + ", dbo.tblAllocationListDrivers.Name "
                                    + "FROM dbo.tblAllocationDriversByField INNER JOIN "
                                        + " dbo.tblAllocationListDrivers ON dbo.tblAllocationDriversByField.IdAllocationListDriver = dbo.tblAllocationListDrivers.IdAllocationListDriver INNER JOIN "
                                        + " dbo.tblFields ON dbo.tblAllocationDriversByField.IdField = dbo.tblFields.IdField "
                                    + "WHERE dbo.tblAllocationListDrivers.IdModel =  " + IdModel + " "
                                    + "AND	 dbo.tblAllocationListDrivers.UnitCostDenominator = 1 "
                                    + "GROUP BY dbo.tblAllocationListDrivers.IdAllocationListDriver "
                                        + ", dbo.tblAllocationListDrivers.Name ").Tables[0];
            }
        }

        public DataTable LoadVolumeReport(Int32 IdModel, Int32 IdStructure)
        {
            switch (IdStructure)
            {
                case 1:
                    return CD.DAC.ConsultSQL("SELECT dbo.tblAllocationDriversByField.IdField "
                            + ", dbo.tblFields.Name AS FieldName "
                            + ", dbo.tblAllocationListDrivers.IdAllocationListDriver "
                            + ", dbo.tblAllocationListDrivers.Name "
                            + ", SUM(ISNULL(dbo.tblAllocationDriversByField.Amount,1)) AS Amount "
                        + "FROM dbo.tblAllocationDriversByField INNER JOIN "
                            + " dbo.tblAllocationListDrivers ON dbo.tblAllocationDriversByField.IdAllocationListDriver = dbo.tblAllocationListDrivers.IdAllocationListDriver INNER JOIN "
                            + " dbo.tblFields ON dbo.tblAllocationDriversByField.IdField = dbo.tblFields.IdField "
                        + "WHERE dbo.tblAllocationListDrivers.IdModel =  " + IdModel + " "
                        + "AND	 dbo.tblAllocationListDrivers.UnitCostDenominator = 1 "
                        + "GROUP BY dbo.tblAllocationListDrivers.IdAllocationListDriver "
                            + ", dbo.tblAllocationListDrivers.Name "
                            + ", dbo.tblAllocationDriversByField.IdField "
                            + ", dbo.tblFields.Name "
                        + "ORDER BY FieldName").Tables[0];
                default:
                    return CD.DAC.ConsultSQL("SELECT dbo.tblAllocationDriversByField.IdField "
                                                + ", dbo.tblFields.Name AS FieldName "
                                                + ", dbo.tblAllocationListDrivers.IdAllocationListDriver "
                                                + ", dbo.tblAllocationListDrivers.Name "
                                                + ", SUM(ISNULL(dbo.tblAllocationDriversByField.Amount,1)) AS Amount "
                                            + "FROM dbo.tblAllocationDriversByField INNER JOIN "
                                                + " dbo.tblAllocationListDrivers ON dbo.tblAllocationDriversByField.IdAllocationListDriver = dbo.tblAllocationListDrivers.IdAllocationListDriver INNER JOIN "
                                                + " dbo.tblFields ON dbo.tblAllocationDriversByField.IdField = dbo.tblFields.IdField "
                                            + "WHERE dbo.tblAllocationListDrivers.IdModel =  " + IdModel + " "
                                            + "AND	 dbo.tblAllocationListDrivers.UnitCostDenominator = 1 "
                                            + "GROUP BY dbo.tblAllocationListDrivers.IdAllocationListDriver "
                                                + ", dbo.tblAllocationListDrivers.Name "
                                                + ", dbo.tblAllocationDriversByField.IdField "
                                                + ", dbo.tblFields.Name "
                                            + "ORDER BY FieldName").Tables[0];
            }
        }

        #endregion

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsSharedCostAllocation : CD.DAC
    {

        private Int32 m_IdSharedCostAllocation;
        private Int32 m_IdModel;
        private Int32 m_IdClientCostCenter;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;

        public Int32 IdSharedCostAllocation { get { return m_IdSharedCostAllocation; } set { m_IdSharedCostAllocation = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public Int32 IdClientCostCenter { get { return m_IdClientCostCenter; } set { m_IdClientCostCenter = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "allpInsertSharedCostAllocation";
            listPrm = DAC.LoadParameters("@IdSharedCostAllocation", IdSharedCostAllocation, "@IdModel", IdModel, "@IdClientCostCenter", IdClientCostCenter, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdSharedCostAllocation = this.MaxID();
            return m_IdSharedCostAllocation;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "allpUpdateSharedCostAllocation";
            listPrm = DAC.LoadParameters("@IdSharedCostAllocation", IdSharedCostAllocation, "@IdModel", IdModel, "@IdClientCostCenter", IdClientCostCenter, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeleteSharedCostAllocation";
            listPrm = LoadParameters("@IdSharedCostAllocation", IdSharedCostAllocation);
            ExecuteProc(sDelete, listPrm);
        }

        public DataTable LoadField(Int32 _IdClientCostCenter)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "allpListSharedCostAllocation";
            listPrm = LoadParameters("@IdModel", IdModel, "@IdSharedCostAllocation", IdSharedCostAllocation, "@IdClientCostCenter", _IdClientCostCenter);
            return ConsultProc(sConsult, listPrm).Tables[0];
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdSharedCostAllocation) FROM tblSharedCostAllocation ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadExportList(string strFilter, string strOrder)
        {
            //return CD.DAC.ConsultSQL("SELECT IdClientCostCenter,TypeOperation FROM vListSharedCostAllocation WHERE  " + strFilter + " " + strOrder).Tables[0];
            return CD.DAC.ConsultSQL("SELECT CostObjectCode, FieldCode FROM vListSharedCostAllocationExport WHERE  " + strFilter + " " + strOrder).Tables[0];
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListSharedCostAllocation " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListSharedCostAllocation WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdSharedCostAllocation=" + m_IdSharedCostAllocation, " ORDER BY IdSharedCostAllocation");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdSharedCostAllocation = (Int32)dr["IdSharedCostAllocation"];
                m_IdModel = (Int32)dr["IdModel"];
                m_IdClientCostCenter = (Int32)dr["IdClientCostCenter"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }
        }

    }
}
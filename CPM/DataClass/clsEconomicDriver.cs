﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsEconomicDriver : CD.DAC
    {
        
        private Int32 m_IdEconomicDriver;
        private Int32 m_IdModel;
        private String m_Name;
        private Int32 m_IdDriverGroup;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;

        public Int32 IdEconomicDriver { get { return m_IdEconomicDriver; } set { m_IdEconomicDriver = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }
        public Int32 IdDriverGroup { get { return m_IdDriverGroup; } set { m_IdDriverGroup = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertEconomicDriver";
            listPrm = DAC.LoadParameters("@IdEconomicDriver", IdEconomicDriver, "@IdModel", IdModel, "@Name", Name,"@IdDriverGroup",IdDriverGroup ,"@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdEconomicDriver = this.MaxID();
            return m_IdEconomicDriver;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdateEconomicDriver";
            listPrm = DAC.LoadParameters("@IdEconomicDriver", IdEconomicDriver, "@IdModel", IdModel, "@Name", Name, "@IdDriverGroup", IdDriverGroup, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteEconomicDriver";
            listPrm = LoadParameters("@IdEconomicDriver", IdEconomicDriver);
            ExecuteProc(sDelete, listPrm);
        }

        public void DeleteAll()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteAllEconomicDriver";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdEconomicDriver) FROM tblEconomicDrivers ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadComboBox(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT IdEconomicDriver ID, Name FROM tblEconomicDrivers " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT IdEconomicDriver ID, Name FROM tblEconomicDrivers WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListEconomicDrivers " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListEconomicDrivers WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdEconomicDriver=" + m_IdEconomicDriver, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdEconomicDriver = (Int32)dr["IdEconomicDriver"];
                m_IdModel = (Int32)dr["IdModel"];
                m_Name = (String)dr["Name"];
                m_IdDriverGroup = (Int32)dr["IdDriverGroup"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }
        }

    }
}
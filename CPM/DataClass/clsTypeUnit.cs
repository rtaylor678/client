﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using CD;

namespace DataClass
{
    public class clsTypeUnit : CD.DAC
    {

        private Int32 m_IdTypeUnit;
        private String m_Name;        

        public Int32 IdTypeUnit { get { return m_IdTypeUnit; } set { m_IdTypeUnit = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }
        
        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "usrpInsertTypeUnit";
            listPrm = DAC.LoadParameters("@IdTypeUnit", IdTypeUnit,  "@Name", Name);
            ExecuteProc(sInsert, listPrm);
            m_IdTypeUnit = this.MaxID();
            return m_IdTypeUnit;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "usrpUpdateTypeUnit";
            listPrm = DAC.LoadParameters("@IdTypeUnit", IdTypeUnit, "@Name", Name);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "usrpDeleteTypeUnit";
            listPrm = LoadParameters("@IdTypeUnit", m_IdTypeUnit);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdTypeUnit) FROM tblTypeUnit ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblTypeUnit " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblTypeUnit WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadComboBox(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT IdTypeUnit ID,Name FROM tblTypeUnit" + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT IdTypeUnit ID,Name FROM tblTypeUnit WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdTypeUnit =" + m_IdTypeUnit, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                m_IdTypeUnit = (Int32)dr["IdTypeUnit"];
                m_Name = (String)dr["Name"];
            }

        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsZiffAccount : CD.DAC
    {

        private Int32 m_IdZiffAccount;
        private String m_Code;
        private String m_Name;
        private Int32 m_Parent;
        private Object m_UserCreation;
        private Object m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;
        private Int32 m_IdModel;

        public Int32 IdZiffAccount { get { return m_IdZiffAccount; } set { m_IdZiffAccount = value; } }
        public String Code { get { return m_Code; } set { m_Code = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }
        public Int32 Parent { get { return m_Parent; } set { m_Parent = value; } }
        public Object UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public Object DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertZiffAccount";
            listPrm = DAC.LoadParameters("@IdZiffAccount", IdZiffAccount, "@Code", Code, "@Name", Name, "@Parent", Parent, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification, "@IdModel", IdModel);
            ExecuteProc(sInsert, listPrm);
            m_IdZiffAccount = this.MaxID();
            return m_IdZiffAccount;
        }

        public void InsertDefault()
        {
            String sInsertDefault;
            List<SqlParameter> listPrm;
            sInsertDefault = "parpInsertZiffAccountFromDefault";
            listPrm = LoadParameters("@IdModel", IdModel, "@IdUser", UserCreation);
            ExecuteProc(sInsertDefault, listPrm);
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdateZiffAccount";
            listPrm = DAC.LoadParameters("@IdZiffAccount", IdZiffAccount, "@Code", Code, "@Name", Name, "@Parent", Parent, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification, "@IdModel", IdModel);
            ExecuteProc(sUpdate, listPrm);
        }

        public void UpdateZiffAccountRelationLevel(Int32 _MaxRelationLevels)
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdateZiffAccountRelationLevel";
            listPrm = DAC.LoadParameters("@IdModel", IdModel, "@MaxRelationLevels", _MaxRelationLevels);
            ExecuteProc(sUpdate, listPrm);
        }

        public void DeleteAll()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteAllZiffAccount";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public void DeleteSubAccount()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteZiffAccountxParent";
            listPrm = LoadParameters("@IdZiffAccount", m_IdZiffAccount);
            ExecuteProc(sDelete, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteZiffAccount";
            listPrm = LoadParameters("@IdZiffAccount", m_IdZiffAccount);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdZiffAccount) FROM tblZiffAccounts ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadComboBox(string strFilter, string strOrder)
         {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT IdZiffAccount, Code, Name, Parent FROM tblZiffAccounts " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT IdZiffAccount, Code, Name, Parent FROM tblZiffAccounts WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListZiffAccounts " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListZiffAccounts WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdZiffAccount=" + m_IdZiffAccount, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                
                m_IdZiffAccount = (Int32)dr["IdZiffAccount"];
                m_Code = (String)dr["Code"];
                m_Name = (String)dr["Name"];
                if (dr["parent"] == null || dr["parent"].ToString().Trim()=="")
                    m_Parent = 0;
                else
                    m_Parent = (Int32)dr["Parent"];
                m_UserCreation = (Object)dr["UserCreation"];
                m_DateCreation = (Object)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
                m_IdModel = (Int32)dr["IdModel"];
            }
        }

        public DataTable LoadComboBoxCPM(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT Code, Name, Parent FROM tblZiffAccounts " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT Code, Name, Parent FROM tblZiffAccounts WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsActivities : CD.DAC
    {

        private Int32 m_IdActivity;
        private Int32 m_IdModel;
        private String m_Code;
        private String m_Name;
        private Int32 m_IdTypeOperation;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;

        public Int32 IdActivity { get { return m_IdActivity; } set { m_IdActivity = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public String Code { get { return m_Code; } set { m_Code = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }
        public Int32 IdTypeOperation { get { return m_IdTypeOperation; } set { m_IdTypeOperation = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertActivities";
            listPrm = DAC.LoadParameters("@IdActivity", IdActivity, "@IdModel", IdModel, "@Code", Code, "@Name", Name, "@IdTypeOperation", IdTypeOperation, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdActivity = this.MaxID();
            return m_IdActivity;
        }

        public void Update()
        {
            // ToDo: Commented out Cleanup Code as requested by Jesus 2014-03-25
            String sUpdate;
            //String sCleanupDirect;
            //String sCleanupShared;
            List<SqlParameter> listPrm;
            //List<SqlParameter> listPrmCleanupDirect;
            //List<SqlParameter> listPrmCleanupShared;
            sUpdate = "parpUpdateActivities";
            //sCleanupDirect = "parpCleanupDirectCostActivity";
            //sCleanupShared = "parpCleanupSharedCostActivity";
            listPrm = DAC.LoadParameters("@IdActivity", IdActivity, "@IdModel", IdModel, "@Code", Code, "@Name", Name, "@IdTypeOperation", IdTypeOperation, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            //listPrmCleanupDirect = DAC.LoadParameters("@IdActivity", IdActivity, "@IdModel", IdModel);
            //listPrmCleanupShared = DAC.LoadParameters("@IdActivity", IdActivity, "@IdModel", IdModel);
            ExecuteProc(sUpdate, listPrm);
            //ExecuteProc(sCleanupDirect, listPrmCleanupDirect);
            //ExecuteProc(sCleanupShared, listPrmCleanupShared);
        }

        public void DeleteAll()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteAllActivities";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteActivities";
            listPrm = LoadParameters("@IdActivity", IdActivity);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdActivity) FROM tblActivities ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadComboBox(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT IdActivity ID, Code, Name FROM tblActivities " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT IdActivity ID, Code, Name FROM tblActivities WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadExportList(string strFilter, string strOrder)
        {
            return CD.DAC.ConsultSQL("SELECT Code,Name,TypeOperation FROM vListActivities WHERE  " + strFilter + " " + strOrder).Tables[0];
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListActivities " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListActivities WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadListWithGeneral(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListActivitiesWithGeneral " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListActivitiesWithGeneral WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdActivity=" + m_IdActivity, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdActivity = (Int32)dr["IdActivity"];
                m_IdModel = (Int32)dr["IdModel"];
                m_Code = (String)dr["Code"];
                m_Name = (String)dr["Name"];
                m_IdTypeOperation = (Int32)dr["IdTypeOperation"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsTypesOperation : CD.DAC
    {

        private Int32 m_IdTypeOperation;
        private Int32 m_IdModel;
        private Int32 m_Shore;
        private Int32 m_Type;
        private String m_Name;
        private Int32 m_Parent;
        private Boolean m_LastNode;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;

        public Int32 IdTypeOperation { get { return m_IdTypeOperation; } set { m_IdTypeOperation = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public Int32 Shore { get { return m_Shore; } set { m_Shore = value; } }
        public Int32 Type { get { return m_Type; } set { m_Type = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }
        public Int32 Parent { get { return m_Parent; } set { m_Parent = value; } }
        public Boolean LastNode { get { return m_LastNode; } set { m_LastNode = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertTypesOperation";
            listPrm = DAC.LoadParameters("@IdTypeOperation", IdTypeOperation, "@IdModel", IdModel, "@Shore", Shore, "@Type", Type, "@Name", Name, "@Parent", Parent, "@LastNode", LastNode, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdTypeOperation = this.MaxID();
            return m_IdTypeOperation;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdateTypesOperation";
            listPrm = DAC.LoadParameters("@IdTypeOperation", IdTypeOperation, "@IdModel", IdModel, "@Shore", Shore, "@Type", Type, "@Name", Name, "@Parent", Parent, "@LastNode", LastNode, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteTypesOperation";
            listPrm = LoadParameters("@IdTypeOperation", m_IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public DataTable LoadModelsTypeOperation(Int32 _Type, Int32 _Shore, Int32 _IdModel, Int32 _Parent)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "parpListModelsTypesOperation";
            listPrm = LoadParameters("@Type", _Type, "@Shore", _Shore, "@IdModel", _IdModel, "@Parent", _Parent);
            return ConsultProc(sConsult, listPrm).Tables[0];
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdTypeOperation) FROM tblTypesOperation ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListTypesOperation " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListTypesOperation WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadListWithGeneral(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListTypesOperationWithGeneral " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListTypesOperationWithGeneral WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdModel=" + m_IdModel, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                
                m_IdTypeOperation = (Int32)dr["IdTypeOperation"];
                m_IdModel = (Int32)dr["IdModel"];
                m_Shore = (Int32)dr["Shore"];
                m_Type = Convert.ToInt32(dr["Type"]);
                m_Name = (String)dr["Name"];
                m_Parent = (Int32)dr["Parent"];
                m_LastNode = (Boolean)dr["LastNode"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }
        }

    }
}
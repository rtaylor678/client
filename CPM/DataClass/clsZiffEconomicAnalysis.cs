﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsZiffEconomicAnalysis : CD.DAC
    {

        private Int32 m_IdZiffEconomicAnalysis;
        private Int32 m_IdModel;
        private Int32 m_IdZiffAccount;
        private Byte[] m_AttachFile;
        private String m_FileName;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;

        public Int32 IdZiffEconomicAnalysis { get { return m_IdZiffEconomicAnalysis; } set { m_IdZiffEconomicAnalysis = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public Int32 IdZiffAccount { get { return m_IdZiffAccount; } set { m_IdZiffAccount = value; } }
        public Byte[] AttachFile { get { return m_AttachFile; } set { m_AttachFile = value; } }
        public String FileName { get { return m_FileName; } set { m_FileName = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertZiffEconomicAnalysis";
            listPrm = DAC.LoadParameters("@IdZiffEconomicAnalysis", IdZiffEconomicAnalysis, "@IdModel", IdModel, "@IdZiffAccount", IdZiffAccount, "@AttachFile", AttachFile, "@FileName", FileName, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdZiffEconomicAnalysis = this.MaxID();
            return m_IdZiffEconomicAnalysis;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdateZiffEconomicAnalysis";
            listPrm = DAC.LoadParameters("@IdZiffEconomicAnalysis", IdZiffEconomicAnalysis, "@IdModel", IdModel, "@IdZiffAccount", IdZiffAccount, "@AttachFile", AttachFile, "@FileName", FileName, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteZiffEconomicAnalysis";
            listPrm = LoadParameters("@IdZiffEconomicAnalysis", IdZiffEconomicAnalysis);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdZiffEconomicAnalysis) FROM tblZiffEconomicAnalysis ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }        
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListZiffEconomicAnalysis " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListZiffEconomicAnalysis WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdZiffEconomicAnalysis=" + m_IdZiffEconomicAnalysis, " ORDER BY IdZiffAccount");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdZiffEconomicAnalysis = (Int32)dr["IdZiffEconomicAnalysis"];
                m_IdModel = (Int32)dr["IdModel"];
                m_IdZiffAccount = (Int32)dr["IdZiffAccount"];
                m_AttachFile = (Byte[])dr["AttachFile"];
                m_FileName = (String)dr["FileName"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }
        }
    }
}

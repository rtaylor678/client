﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;
using System.Text.RegularExpressions;


namespace DataClass
{
    public class clsUsers : CD.DAC
    {

        #region '" Enumerator Declaration "'
        public enum PasswordScore
        {
            Blank = 0,
            TooShort = 1,
            RequirementsNotMet = 2,
            VeryWeak = 3,
            Weak = 4,
            Fair = 5,
            Medium = 6,
            Strong = 7,
            VeryStrong = 8,
            Requirement1NotMet = 9,
            Requirement2NotMet = 10,
            Requirement3NotMet = 11,
            Requirement4NotMet = 12

        }
        #endregion

        private Int32 m_IdUser;
        private String m_Name;
        private String m_Email;
        private String m_UserName;
        private String m_Password;
        private Int32 m_IdProfile;
        private DateTime m_LastLogin;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;

        public Int32 IdUser { get { return m_IdUser; } set { m_IdUser = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }
        public String Email { get { return m_Email; } set { m_Email = value; } }
        public String UserName { get { return m_UserName; } set { m_UserName = value; } }
        public String Password { get { return m_Password; } set { m_Password = value; } }
        public Int32 IdProfile { get { return m_IdProfile; } set { m_IdProfile = value; } }
        public DateTime LastLogin { get { return m_LastLogin; } set { m_LastLogin = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "usrpInsertUser";
            listPrm = DAC.LoadParameters("@IdUser", IdUser, "@Name", Name, "@Email", Email, "@UserName", UserName, "@Password", Password, "@IdProfile", @IdProfile, "@LastLogin", @LastLogin, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdUser = this.MaxID();
            return m_IdUser;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "usrpUpdateUser";
            listPrm = DAC.LoadParameters("@IdUser", IdUser, "@Name", Name, "@Email", Email, "@UserName", UserName, "@Password", Password, "@IdProfile", @IdProfile, "@LastLogin", @LastLogin, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "usrpDeleteUser";
            listPrm = LoadParameters("@IdUser", m_IdUser);
            ExecuteProc(sDelete, listPrm);
        }

        public int PasswordResetRequest(String Token)
        {
            Int32 passwordResetRequestReturn = 0;
            CD.DAC DAC = new CD.DAC();
            String sReset;
            List<SqlParameter> listPrm;
            sReset = "usrpPasswordResetRequest";
            listPrm = DAC.LoadParameters("@IdUser", IdUser, "@Token", Token);
            DataTable dtReturn = new DataTable();
            dtReturn = ExecuteReturn(sReset, listPrm);
            if (dtReturn.Rows.Count > 0)
            {
                passwordResetRequestReturn = Convert.ToInt32(dtReturn.Rows[0]["ReturnMessage"]);
            }
            else
            {
                passwordResetRequestReturn = 0;
            }
            return passwordResetRequestReturn;
        }

        public int PasswordResetValidate(String Token)
        {
            Int32 passwordResetValidateReturn = 0;
            CD.DAC DAC = new CD.DAC();
            String sReset;
            List<SqlParameter> listPrm;
            sReset = "usrpPasswordResetValidate";
            listPrm = DAC.LoadParameters("@Token", Token, "@Password", DAC.Encrypt("P@55word"));
            DataTable dtReturn = new DataTable();
            dtReturn = ExecuteReturn(sReset, listPrm);
            if (dtReturn.Rows.Count > 0)
            {
                passwordResetValidateReturn = Convert.ToInt32(dtReturn.Rows[0]["ReturnMessage"]);
            }
            else
            {
                passwordResetValidateReturn = 0;
            }
            return passwordResetValidateReturn;
        }

        public int PasswordDeleteToken(String Token)
        {
            Int32 passwordDeleteTokenReturn = 0;
            CD.DAC DAC = new CD.DAC();
            String sReset;
            List<SqlParameter> listPrm;
            sReset = "usrpPasswordDeleteToken";
            listPrm = DAC.LoadParameters("@Token", Token);
            DataTable dtReturn = new DataTable();
            dtReturn = ExecuteReturn(sReset, listPrm);
            if (dtReturn.Rows.Count > 0)
            {
                passwordDeleteTokenReturn = Convert.ToInt32(dtReturn.Rows[0]["ReturnMessage"]);
            }
            else
            {
                passwordDeleteTokenReturn = 0;
            }
            return passwordDeleteTokenReturn;
        }

        public int LogActivity(CD.UserLog.UserActivity Activity, Int32? Model, String Note)
        {
            Int32 logActivityReturn = 0;
            CD.DAC DAC = new CD.DAC();
            String sReset;
            List<SqlParameter> listPrm;
            sReset = "usrpInsertUserLog";
            listPrm = DAC.LoadParameters("@IdUser", IdUser, "@IdUserActivity", Activity, "@IdModel", Model, "@Note", Note);
            DataTable dtReturn = new DataTable();
            dtReturn = ExecuteReturn(sReset, listPrm);
            if (dtReturn.Rows.Count > 0)
            {
                logActivityReturn = Convert.ToInt32(dtReturn.Rows[0]["ReturnMessage"]);
            }
            else
            {
                logActivityReturn = 0;
            }
            return logActivityReturn;
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdUser) FROM tblUsers ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblUsers " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblUsers WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdUser=" + m_IdUser, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                m_IdUser = (Int32)dr["IdUser"];
                m_Name = (String)dr["Name"];
                m_Email = (String)dr["Email"];
                m_UserName = (String) dr["UserName"];
                m_Password = (String)dr["Password"];
                m_IdProfile = (Int32)dr["IdProfile"];
                m_LastLogin = (DateTime)dr["LastLogin"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }

        }

        public PasswordScore CheckStrength(string password)
        {
            int score = 0;

            // using three requirements here:  min length and two types of characters (numbers and letters)
            bool blnMinLengthRequirementMet = false;
            bool blnRequirement1Met = false;    // Has 1 Numeric
            bool blnRequirement2Met = false;    // Has 1 Lower Alpha
            bool blnRequirement3Met = false;    // Has 1 Upper Alpha
            bool blnRequirement4Met = false;    // has 1 Special Character

            // check for chars in password
            if (password.Length < 1)
                return PasswordScore.Blank;

            // if less than 6 chars, return as too short, else, plus one
            if (password.Length < 8)
            {
                return PasswordScore.TooShort;
            }
            else
            {
                score++;
                blnMinLengthRequirementMet = true;
            }

            // if 8 or more chars, plus one
            if (password.Length >= 8)
                score++;

            // if 10 or more chars, plus one
            if (password.Length >= 10)
                score++;

            // if password has a number, plus one
            if (Regex.IsMatch(password, @"[\d]", RegexOptions.ECMAScript))
            {
                score++;
                blnRequirement1Met = true;
            }

            // if password has lower case letter, plus one
            if (Regex.IsMatch(password, @"[a-z]", RegexOptions.ECMAScript))
            {
                score++;
                blnRequirement2Met = true;
            }

            // if password has upper case letter, plus one
            if (Regex.IsMatch(password, @"[A-Z]", RegexOptions.ECMAScript))
            {
                score++;
                blnRequirement3Met = true;
            }

            // if password has a special character, plus one
            if (Regex.IsMatch(password, @"[~`!@#$%\^\&\*\(\)\-_\+=\[\{\]\}\|\\;:'\""<\,>\.\?\/£]", RegexOptions.ECMAScript))
            {
                score++;
                blnRequirement4Met = true;
            }

            // if password is longer than 2 characters and has 3 repeating characters, minus one (to minimum of score of 3)
            List<char> lstPass = password.ToList();
            if (lstPass.Count >= 3)
            {
                for (int i = 2; i < lstPass.Count; i++)
                {
                    char charCurrent = lstPass[i];
                    if (charCurrent == lstPass[i - 1] && charCurrent == lstPass[i - 2] && score >= 4)
                    {
                        score++;
                    }
                }
            }

            //if (!blnMinLengthRequirementMet || !blnRequirement1Met || !blnRequirement2Met || !blnRequirement3Met || !blnRequirement4Met)
            //{
            //    return PasswordScore.RequirementsNotMet;
            //}

            if (!blnMinLengthRequirementMet)
                return PasswordScore.RequirementsNotMet;
            else if (!blnRequirement1Met)
                return PasswordScore.Requirement1NotMet;
            else if (!blnRequirement2Met)
                return PasswordScore.Requirement2NotMet;
            else if (!blnRequirement3Met)
                return PasswordScore.Requirement3NotMet;
            else if (!blnRequirement4Met)
                return PasswordScore.Requirement4NotMet;
            else
                return (PasswordScore)score;
        }
    }
}

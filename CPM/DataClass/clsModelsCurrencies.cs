﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsModelsCurrencies : CD.DAC
    {

        private Int32 m_IdModelCurrency; 
        private Int32 m_IdCurrency;
        private Int32 m_IdModel;
        private Double m_Value;
        private Boolean m_BaseCurrency;
        private Boolean m_Output;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;

        public Int32 IdModelCurrency { get { return m_IdModelCurrency; } set { m_IdModelCurrency = value; } }
        public Int32 IdCurrency { get { return m_IdCurrency; } set { m_IdCurrency = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public Double Value { get { return m_Value; } set { m_Value = value; } }
        public Boolean BaseCurrency { get { return m_BaseCurrency; } set { m_BaseCurrency = value; } }
        public Boolean Output { get { return m_Output; } set { m_Output = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertModelsCurrencies";
            listPrm = DAC.LoadParameters("@IdModelCurrency", IdModelCurrency, "@IdCurrency", IdCurrency, "@IdModel", IdModel, "@Value", Value, "@BaseCurrency", BaseCurrency, "@Output", Output, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdModelCurrency = this.MaxID();
            return m_IdModelCurrency;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdateModelsCurrencies";
            listPrm = DAC.LoadParameters("@IdModelCurrency", IdModelCurrency, "@IdCurrency", IdCurrency, "@IdModel", IdModel, "@Value", Value, "@BaseCurrency", BaseCurrency, "@Output", Output, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteModelsCurrencies";
            listPrm = LoadParameters("@IdModelCurrency", m_IdModelCurrency);
            ExecuteProc(sDelete, listPrm);
        }

        public void DeleteAll()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteModelsCurrenciesAll";
            listPrm = LoadParameters("@IdModel", m_IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdModelCurrency) FROM tblModelsCurrencies ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListCurrencies " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListCurrencies WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadModelObject(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblModelsCurrencies " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblModelsCurrencies WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadModelObject("IdModelCurrency =" + m_IdModelCurrency, " ORDER BY IdModelCurrency");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdModelCurrency = (Int32)dr["IdModelCurrency"];
                m_IdCurrency = (Int32)dr["IdCurrency"];
                m_IdModel = (Int32)dr["IdModel"];
                m_Value = Convert.ToDouble(dr["Value"]);
                m_BaseCurrency = (Boolean)dr["BaseCurrency"];
                m_Output = (Boolean)dr["Output"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }
        }

    }
}
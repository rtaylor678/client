﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web;
using InfragisticsExcel = Infragistics.Documents.Excel;

namespace DataClass
{
    public class modMain
    {

        #region Login

        public static String strLogTitleForm;
        public static String strLogUserName;
        public static String strLogPassword;
        public static String strLogLanguage;
        public static String strLogRememberMe;
        public static String strLogForgotPassword;
        public static String strLogLoginButton;
        public static String strLogLoginMessageError;
        public static String strLogForgotMessageUser;
        public static String strLogForgotMessageError;
        public static String strLogForgotMessageMail;
        public static String strLogValid;

        #endregion

        #region MasterPage

        public static String strMasterTitleForm;
        public static String strMasterAdmin;
        public static String strMasterUser;
        public static String strMasterSetting;
        public static String strMasterLogon;
        public static String strMasterLastLogin;
        public static String strMasterWindowsTitleUser;
        public static String strMasterFormUserName;
        public static String strMasterFormName;
        public static String strMasterFormPassword;
        public static String strMasterFormEmail;
        public static String strMasterFormAcceptButton;
        public static String strMasterFormCancelButton;
        public static String strMasterValid;
        public static String strMasterHome;
        public static String strMasterParametersModule;
        public static String strMasterAllocationModule;
        public static String strMasterTechnicalModule;
        public static String strMasterEconomicModule;
        public static String strMasterReportsModule;
        public static String strMasterCaseSelection;
        public static String strMasterModules;
        public static String strMasterSelectedCase;
        public static String strMasterAggregationLevels;
        public static String strMasterTypesofOperations;
        public static String strMasterListofFields;
        public static String strMasterFieldProjects;
        public static String strMasterCurrency;
        public static String strMasterActivities;
        public static String strMasterResources;
        public static String strMasterCostObjects;
        public static String strMasterChartAccounts;
        public static String strMasterZiff_Third_partyCostCategories;
        public static String strMasterCompanyCostReferences;
        public static String strMasterZiff_Third_partyCostReferences;
        public static String strMasterCostStructureAssumptions;
        public static String strMasterBaseCostData;
        public static String strMasterDirect;
        public static String strMasterShared;
        public static String strMasterHierarchy;
        public static String strMasterListofAllocationDrivers;
        public static String strMasterAllocationDriversCriteria;
        public static String strMasterAllocationDriversData;
        public static String strMasterBaseCostbyFieldCompany;
        public static String strMasterMapping;
        public static String strMasterBaseCostbyFieldZiff;
        public static String strMasterListofPeerCriteria;
        public static String strMasterPeerCriteriaData;
        public static String strMasterZiff_Third_partyBaseCost;
        public static String strMasterListTechnicalDrivers;
        public static String strMasterTechnicalDriversData;
        public static String strMasterTechnicalAssumptions;
        public static String strMasterBaseCostRates;
        public static String strMasterZiffTechnicalAnalysis;
        public static String strMasterListofEconomicDrivers;
        public static String strMasterEconomicDriversData;
        public static String strMasterEconomicAssumptions;
        public static String strMasterBaseCostRatesForecast;
        public static String strMasterZiffEconomicAnalysis;
        public static String strMasterOperatingCostProjection;
        public static String strMasterTechnicalDriversForecast;
        public static String strMasterKPIEstimates;

        #endregion

        #region Case Details Page

        public static String strCaseDetailsTitleForm;
        public static String strCaseDetailsBaseCase;
        public static String strCaseDetailsCreatedBy;
        public static String strCaseDetailsCreated;
        public static String strCaseDetailsBaseYear;
        public static String strCaseDetailsProjection;
        public static String strCaseDetailsStatus;
        public static String strCaseDetailsLevel;
        public static String strCaseDetailsNote;
        public static String strCaseDetailsFormStatus;
        public static String strCaseDetailsNewCase;
        public static String strCaseDetailsDate;

        #endregion

        #region Users

        public static String strUsersAddNewUser;
        public static String strUsersCaseSelection;
        public static String strUsersUsers;
        public static String strUsersUserName;
        public static String strUsersEmail;
        public static String strUsersLastLogin;
        public static String strUsersUser;
        public static String strUsersPassword;
        public static String strUsersRepeatPassword;
        public static String strUsersSelectRole;

        #endregion

        #region Settings

        public static String strSettingsAddNewRole;
        public static String strSettingsCaseSelection;
        public static String strSettingsRoles;
        public static String strSettingsRoleType;
        public static String strSettingsSelectedCase;
        public static String strSettingsSelectedRolePermissions;
        public static String strSettingsGrantAll;
        public static String strSettingsDenyAll;
        public static String strSettingsClearAll;
        public static String strSettingsGrantAccess;
        public static String strSettingsDenyAccess;
        public static String strSettingsRole;

        #endregion

        #region Parameter Module

        public static String strParameterModuleFormTitle;
        public static String strParameterModuleFormSubTitle1;
        public static String strParameterModuleExchangeRates;
        public static String strParameterModuleFormSubTitle2;
        public static String strParameterModuleFormSubTitle3;

        #endregion

        #region Allocation Module

        public static String strAllocationModuleFormTitle;
        public static String strAllocationModuleFormSubTitle1;
        public static String strAllocationModuleFormSubTitle2;

        #endregion

        #region ClientReference

        public static String strClientReferenceCompanyCostInput;
        public static String strClientReferenceCostAllocationProcess;
        public static String strClientReferenceAllocationDrivers;
        public static String strClientReferenceBaseCost;
        public static String strClientReferenceMappingShort;
        public static String strClientReferenceBaseCostbyFieldZiffShort;

        #endregion

        #region Technical Module

        public static String strTechnicalModuleFormTitle;
        public static String strTechnicalModuleTechnicalDriver;
        public static String strTechnicalModuleField;
        public static String strTechnicalModuleProject;
        public static String strTechnicalModuleYear;
        public static String strTechnicalModuleBase;
        public static String strTechnicalModulePessimistic;
        public static String strTechnicalModuleOptimistic;
        public static String strTechnicalModuleImportTechnicalDriverData;
        public static String strTechnicalModuleUtilizationCapacity;
        public static String strTechnicalModuleBaseRatesByCostCategory;
        public static String strTechnicalModuleBaseRatesByDrivers;

        #endregion

        #region Economic Module

        public static String strEconomicModuleFormTitle;
        public static String strEconomicModuleEconomicDrivers;
        public static String strEconomicModuleEconomicDriverGroup;
        public static String strEconomicModuleManageEconomicDriverGroups;
        public static String strEconomicModuleAddNewEconomicDriver;
        public static String strEconomicModuleDriverGroup;
        public static String strEconomicModuleEconomicDriver;
        public static String strEconomicModuleImportEconomicDrivers;
        public static String strEconomicModuleAddNewEconomicDriverGroup;
        public static String strEconomicModuleListofEconomicDrivers;
        public static String strEconomicModulePessimistic;
        public static String strEconomicModuleOptimistic;
        public static String strEconomicModuleImportEconomicDriverData;
        public static String strEconomicModuleEconomicScenarios;

        #endregion

        #region Reports Module

        public static String strReportsModuleFormTitle;
        public static String strReportsModuleKPIEstimatesLong;

        public static String strReportsModuleDriver;
        public static String strReportsModuleTechnicalDriverNum;

        public static String strReportsModuleSubFormTitle3;
        public static String strReportsModuleAggregationLevel;
        public static String strReportsModuleCostStructure;
        public static String strReportsModuleFrom;
        public static String strReportsModuleTo;
        public static String strReportsModuleTechnicalScenario;
        public static String strReportsModuleEconomicScenario;
        public static String strReportsModuleCurrency;
        public static String strReportsModuleTerm;
        public static String strReportsModuleRunReport;
        public static String strReportsModuleKPITarget;
        public static String strReportsModuleYears;
        public static String strReportsModuleReset;
        public static String strReportsModuleExportReportDataToExcel;
        public static String strReportsModuleResultsTitle;
        public static String strReportsModuleOpexResultsTitle1;
        public static String strReportsModuleOpexResultsTitle2;
        public static String strReportsModuleOpexResultsSubTitle1;
        public static String strReportsModuleOpexResultsSubTitle2;
        public static String strReportsModuleOperatingCostProjectionDetailed;
        public static String strReportsModuleCompanyBaseCostReferences;
        public static String strReportsModuleZiffBaseCostReferences;
        public static String strReportsModuleDetailedProjection;
        public static String strReportsModuleFormSubTitle1;
        public static String strReportsModuleFormSubTitle2;
        public static String strReportsModuleFormSubTitle3;
        public static String strReportsModuleFormOperationType;
        public static String strReportsModuleFormTypeCost;

        #endregion  

        #region Common Throughout

        //close
        public static String strCommonClose;
        public static String strCommonDelete;
        public static String strCommonEdit;
        public static String strCommonFunctions;
        public static String strCommonName;
        public static String strCommonOpen;
        public static String strCommonSave;
        public static String strCommonSelect;
        public static String strCommonSaveChanges;
        public static String strCommonDeleteAll;
        public static String strCommonConfirm;
        public static String strCommonYes;
        public static String strCommonNo;
        public static String strCommonFormValid;
        public static String strCommonFormInvalid;
        public static String strCommonImport;
        public static String strCommonCode;
        public static String strCommonHelp;

        #endregion

        #region Function Declaration

        public String FormatButtonText(String strStringToFormat)
        {
            //need to split the string as it is too long and doesn't wrap properly on the button
            int intMaxText = 27;
            int intStringLength = strStringToFormat.Length;

            if (intStringLength > intMaxText) //only split words if they are longer than 20 characters
            {
                int intPlace = strStringToFormat.IndexOf(" ", intStringLength / 2); //find first occurance of space when you split the string in half
                if (intPlace > intMaxText) //will not cut word properly
                {
                    intPlace = strStringToFormat.LastIndexOf(" ", intMaxText);
                }
                String strFirst = strStringToFormat.Substring(0, intPlace); //temporarily store string before space
                String strRest = strStringToFormat.Substring(intPlace, strStringToFormat.Length - intPlace); //temporarily store string after space

                return strFirst + "<br>" + strRest.TrimStart(' '); //return appended string
            }
            else
            {
                return strStringToFormat;
            }
        }

        #endregion

    }

    #region ClassLoadData

    public class DataFileType
    {

        // Enumerators
        public enum FileClass
        {
            Unknown = 0,
            Excel = 1,
            CSVTXT = 2
        }

        // Data members
        public FileClass ImportFileClass = FileClass.Unknown;
        public String FileType { get; set; }
        public Boolean IsValidType { get; set; }

        public DataFileType()
        {
        }

        public void CheckFileType(String CheckFilename)
        {
            String[] strValidFileTypes = { "XLSX", "XLSM", "XLS", "CSV", "TXT" };
            this.FileType = System.IO.Path.GetExtension(CheckFilename).ToUpper();
            this.IsValidType = false;
            // Set File Class
            if (this.FileType.Substring(0, 4) == ".XLS")
            {
                ImportFileClass = FileClass.Excel;
            }
            else if (this.FileType == ".CSV" || this.FileType == ".TXT")
            {
                ImportFileClass = FileClass.CSVTXT;
            }
            // Set Is Valid if True
            for (int i = 0; i < strValidFileTypes.Length; i++)
            {
                if (this.FileType == "." + strValidFileTypes[i])
                {
                    this.IsValidType = true;
                    break;
                }
            }
        }

    }

    public abstract class DataFileReader
    {

        // Enumerators
        public enum ImportType
        {
            Unknown = 0,
            CostCenters = 1,
            Activities = 2,
            AllocationDriverData = 3,
            AllocationDriverByField = 4,
            CostAccounts = 5,
            CostData = 6,
            DirectCostAllocation = 7,
            HierarchyCostAllocation = 8,
            Resources = 9,
            SharedCostAllocation = 10,
            Mapping = 11,
            AllocationListDriver = 12,
            TechnicalListDriver = 13,
            TechnicalDriverData = 14,
            AggregationLevels = 15,
            EconomicDriverData = 16,
            Fields = 17,
            Projects = 18,
            TechnicalDriverProfilesReport = 19,
            ZiffStructure = 20,
            EconomicDriver = 21,
            ZiffBaseCost = 22,
            ZiffPeerCriteria = 23,
            ZiffPeerCriteriaData = 24,
            PeerGroup = 25,
            Currencies = 26,
            Prices = 27,
            TechnicalAssumptions = 28,
            UtilizationCapacity = 29,
            ExternalBaseCostReferences = 30
        }

        // Function members
        public abstract void GetDataTable(ref String ReturnMessage);
        
        // Data members
        protected String[] strColumns = null;
        protected ImportType objImportEnvironment = ImportType.Unknown;
        protected Stream objImportFile = null;
        public String StringDelimiter = ",";
        public DataTable ImportTable { get; set; }

        protected String[] GetTemplateColumns(ImportType TemplateType)
        {
            String[] strGetTemplateColumnsReturn = null;

            switch (TemplateType)
            {
                case ImportType.Activities:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Activity Code", "Activity Desc", "Type of Operation" };
                        break;
                    }
                case ImportType.Resources:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Resource Code", "Resource Desc", "Activity Code" };
                        break;
                    }
                case ImportType.CostCenters:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Cost Object Code", "Cost Object Desc", "Activity Code", "Allocation", "Notes" };
                        break;
                    }
                case ImportType.CostAccounts:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Account Code", "Account Desc", "Resource Code" };
                        break;
                    }
                case ImportType.CostData:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Cost Object Code", "Account Code", "Amount" };
                        break;
                    }
                case ImportType.Mapping:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Cost Object Code", "Account Code", "External Account Code" };
                        break;
                    }
                case ImportType.AllocationListDriver:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Driver Desc", "Unit Desc" };
                        break;
                    }
                case ImportType.AllocationDriverByField:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Driver Desc", "Field Code", "Amount" };
                        break;
                    }
                case ImportType.AllocationDriverData:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Driver Desc", "Cost Object Code" };
                        break;
                    }
                case ImportType.TechnicalListDriver:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Driver Desc", "Unit Desc", "Type", "Type of Operation", "Unit Cost Denominator",	"Baseline Allocation" };
                        break;
                    }
                case ImportType.TechnicalDriverData:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Driver Desc", "Field Code", "Project Code", "Year", "Normal", "Optimistic", "Pessimistic" };
                        break;
                    }
                case ImportType.ZiffStructure:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Category Code", "Category Desc", "Parent Category Code" };
                        break;
                    }
                case ImportType.EconomicDriverData:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Driver Desc", "Year", "Normal", "Optimistic", "Pessimistic" };
                        break;
                    }
                case ImportType.Fields:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Field Code", "Field Name", "Aggregation Level", "Type of Operation", "Year Starts" };
                        break;
                    }
                case ImportType.Projects:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Case Code", "Case Name", "Field Code", "Type of Case", "Selection", "Year Starts" };
                        break;
                    }
                case ImportType.AggregationLevels:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Aggregation Code", "Aggregation Name", "Level", "Parent Aggregation Code" };
                        break;
                    }
                case ImportType.DirectCostAllocation:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Cost Object Code", "Field Code" };
                        break;
                    }
                case ImportType.SharedCostAllocation:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Cost Object Code", "Field Code" };
                        break;
                    }
                case ImportType.HierarchyCostAllocation:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Cost Object Code", "Aggregation Level" };
                        break;
                    }
                case ImportType.EconomicDriver:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Driver Desc", "Driver Group Name" };
                        break;
                    }
                case ImportType.ZiffBaseCost:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Peer Group Code", "Cost Category Code", "Unit Cost" };
                        break;
                    }
                case ImportType.ZiffPeerCriteria:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Criteria Desc", "Unit Desc" };
                        break;
                    }
                case ImportType.ZiffPeerCriteriaData:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Criteria Desc", "Peer Group Code", "Value" };
                        break;
                    }
                case ImportType.PeerGroup:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Peer Group Code", "Peer Group Description", "Fields", "Full Benchmark", "Comments" };
                        break;
                    }
                case ImportType.Currencies:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Code", "Name", "Sign", "Value", "Output" };
                        break;
                    }
                case ImportType.Prices:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Year", "Field Code", "Currency Code", "Product", "Scenario", "Price", "Offset" };
                        break;
                    }
                case ImportType.TechnicalAssumptions:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Type of Operation", "Cost Category", "Projection Criteria", "Size Drivers", "Performance Drivers", "Cycle Term" };
                        break;
                    }
                case ImportType.UtilizationCapacity:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Field", "Cost Category", "Value" };
                        break;
                    }
                case ImportType.ExternalBaseCostReferences:
                    {
                        strGetTemplateColumnsReturn = new String[] { "Peer Group Code", "Cost Category Code", "Year" };
                        break;
                    }
                default:
                    {
                        return strGetTemplateColumnsReturn;
                    }
            }
            return strGetTemplateColumnsReturn;
        }

    }

    public class ExcelReader : DataFileReader
    {

        private InfragisticsExcel.Workbook objExcel = new InfragisticsExcel.Workbook();
        private DataTable dtExcelData
        {
            get;
            set;
        }

        public ExcelReader(Stream ImportFile, ImportType ImportEnvironment)
        {
            objImportFile = ImportFile;
            objImportEnvironment = ImportEnvironment;
        }

        public override void GetDataTable(ref String ReturnMessage)
        {
            ReturnMessage = "Error processing import file.";
            objExcel = InfragisticsExcel.Workbook.Load(objImportFile);
            dtExcelData = new DataTable("tblTemplateData");
            strColumns = this.GetTemplateColumns(objImportEnvironment);

            // Get DataTable Import Region
            Int32 minCellRow = 0;
            Int32 maxCellRow = 0;
            Int32 minCellColumn = 0;
            Int32 maxCellColumn = 0;
            foreach (InfragisticsExcel.WorksheetRow row in objExcel.Worksheets["DATASHEET"].Rows)
            {
                foreach (InfragisticsExcel.WorksheetCell cell in row.Cells)
                {
                    minCellRow = Math.Min(minCellRow, cell.RowIndex);
                    maxCellRow = Math.Max(maxCellRow, cell.RowIndex);
                    minCellColumn = Math.Min(minCellColumn, cell.ColumnIndex);
                    maxCellColumn = Math.Max(maxCellColumn, cell.ColumnIndex);
                }
            }
            // Populate DataTable Columns and Double Check against defined columns
            Int32 intColumnMatch = 0;
            for (Int32 colIndex = minCellColumn; colIndex <= maxCellColumn; colIndex++)
            {
                String columnName = objExcel.Worksheets["DATASHEET"].Rows[minCellRow].Cells[colIndex].Value.ToString();
                dtExcelData.Columns.Add(columnName);
                foreach (String strColumn in strColumns)
                {
                    if (strColumn.Trim().ToLower() == columnName.Trim().ToLower())
                    {
                        intColumnMatch += 1;
                    }
                }
            }
            if (intColumnMatch != strColumns.Length)
            {
                ReturnMessage = "Error processing import file. Could not match all expected import columns in source file.";
                ImportTable = null;
                return;
            }
            // Populate Datatable Rows
            DataRow drRow = null;
            for (Int32 rowIndex = minCellRow + 1; rowIndex <= maxCellRow; rowIndex++)
            {
                drRow = dtExcelData.NewRow();
                for (Int32 colIndex = minCellColumn; colIndex <= maxCellColumn; colIndex++)
                {
                    drRow[colIndex] = objExcel.Worksheets["DATASHEET"].Rows[rowIndex].Cells[colIndex].Value;
                }
                if (!RowIsBlank(drRow, maxCellColumn))
                {
                    dtExcelData.Rows.Add(drRow);
                }
                drRow = null;
            }
            ImportTable = new DataTable();
            ImportTable = dtExcelData;
            ReturnMessage = null;
        }

        private Boolean RowIsBlank(DataRow CheckRow, Int32 ColumnCount)
        {
            Boolean rowIsBlankReturn = false;
            Int32 intBlankCount = 0;
            for (int intLoop = 0; intLoop <= ColumnCount - 1; intLoop++)
            {
                if (CheckRow.IsNull(intLoop))
                {
                    intBlankCount++;
                }
            }
            if (intBlankCount == ColumnCount)
            {
                rowIsBlankReturn = true;
            }
            return rowIsBlankReturn;
        }

    }

    public class CSVReader : DataFileReader
    {

        private StreamReader objReader;
        private Encoding encFile;
        private DataTable dtTextData
        {
            get;
            set;
        }

        public CSVReader(Stream ImportFile, ImportType ImportEnvironment) : this(ImportFile, ImportEnvironment, null) { }

        public CSVReader(Stream ImportFile, ImportType ImportEnvironment, Encoding FileEncoding)
        {
            objImportFile = ImportFile;
            objImportEnvironment = ImportEnvironment;
            encFile = FileEncoding;
        }

        public override void GetDataTable(ref String ReturnMessage)
        {
            ReturnMessage = "Error processing import file.";
            //Check the Stream to see if it is readable or not
            if (!objImportFile.CanRead)
            {
                return;
            }
            objReader = (encFile != null) ? new StreamReader(objImportFile, encFile) : new StreamReader(objImportFile);

            dtTextData = new DataTable("tblTemplateData");
            String[] strHeaders = this.GetLine();
            // Add column headers
            foreach (String strHeader in strHeaders)
            {
                dtTextData.Columns.Add(strHeader);
            }
            String[] strData;
            while ((strData = this.GetLine()) != null)
            {
                dtTextData.Rows.Add(strData);
            }
            if (dtTextData.Columns.Count == 1)
            {
                ReturnMessage = "The file format is invalid.";
                return;
            }
            ImportTable = new DataTable();
            ImportTable = dtTextData;
            ReturnMessage = null;
        }

        public String[] GetCSVLine()
        {
            String data = objReader.ReadLine();
            if (data == null) return null;
            if (data.Length == 0) return new String[0];
            //System.Collection.Generic
            ArrayList result = new ArrayList();
            //parsing CSV Data
            ParseCSVData(result, data);
            return (String[])result.ToArray(typeof(String));
        }

        private void ParseCSVData(ArrayList result, String data)
        {
            Int32 position = -1;
            while (position < data.Length)
                result.Add(ParseCSVField(ref data, ref position));
        }

        private String ParseCSVField(ref String data, ref Int32 StartSeperatorPos)
        {
            if (StartSeperatorPos == data.Length - 1)
            {
                StartSeperatorPos++;
                return "";
            }

            Int32 fromPos = StartSeperatorPos + 1;
            if (data[fromPos] == '"')
            {
                Int32 nextSingleQuote = GetSingleQuote(data, fromPos + 1);
                Int32 lines = 1;
                while (nextSingleQuote == -1)
                {
                    data = data + "\n" + objReader.ReadLine();
                    nextSingleQuote = GetSingleQuote(data, fromPos + 1);
                    lines++;
                    if (lines > 20)
                        throw new Exception("lines overflow: " + data);
                }
                StartSeperatorPos = nextSingleQuote + 1;
                String tempString = data.Substring(fromPos + 1, nextSingleQuote - fromPos - 1);
                tempString = tempString.Replace("'", "''");
                return tempString.Replace("\"\"", "\"");
            }

            Int32 nextComma = data.IndexOf(StringDelimiter, fromPos);
            if (nextComma == -1)
            {
                StartSeperatorPos = data.Length;
                return data.Substring(fromPos);
            }
            else
            {
                StartSeperatorPos = nextComma;
                return data.Substring(fromPos, nextComma - fromPos);
            }
        }

        public String[] GetLine()
        {
            String data = objReader.ReadLine();
            if (data == null) return null;
            if (data.Length == 0) return new String[0];
            //System.Collection.Generic
            ArrayList result = new ArrayList();
            //parsing CSV Data
            ParseData(result, data);
            return (String[])result.ToArray(typeof(String));
        }

        protected void ParseData(ArrayList result, String data)
        {
            Int32 position = -1;
            while (position < data.Length)
                result.Add(ParseField(ref data, ref position));
        }

        protected String ParseField(ref String data, ref Int32 StartSeperatorPos)
        {
            if (StartSeperatorPos == data.Length - 1)
            {
                StartSeperatorPos++;
                return "";
            }

            Int32 fromPos = StartSeperatorPos + 1;
            if (data[fromPos] == '"')
            {
                Int32 nextSingleQuote = GetSingleQuote(data, fromPos + 1);
                Int32 lines = 1;
                while (nextSingleQuote == -1)
                {
                    data = data + "\n" + objReader.ReadLine();
                    nextSingleQuote = GetSingleQuote(data, fromPos + 1);
                    lines++;
                    if (lines > 20)
                        throw new Exception("lines overflow: " + data);
                }
                StartSeperatorPos = nextSingleQuote + 1;
                String tempString = data.Substring(fromPos + 1, nextSingleQuote - fromPos - 1);
                tempString = tempString.Replace("'", "''");
                return tempString.Replace("\"\"", "\"");
            }

            Int32 nextComma = data.IndexOf(StringDelimiter, fromPos);
            if (nextComma == -1)
            {
                StartSeperatorPos = data.Length;
                return data.Substring(fromPos);
            }
            else
            {
                StartSeperatorPos = nextComma;
                return data.Substring(fromPos, nextComma - fromPos);
            }
        }

        private Int32 GetSingleQuote(String data, Int32 SFrom)
        {
            Int32 i = SFrom - 1;
            while (++i < data.Length)
                if (data[i] == '"')
                {
                    if (i < data.Length - 1 && data[i + 1] == '"')
                    {
                        i++;
                        continue;
                    }
                    else
                        return i;
                }
            return -1;
        }

    }

    public abstract class DataFileWriter
    {

        // Enumerators
        public enum ExportType
        {
            Unknown = 0,
            Excel = 1
        }

        public enum ExcelType
        {
            Unknown = 0,
            Excel2003 = 1,
            Excel2007 = 2
        }

        // Function members
        public abstract void BuildExportFile(ref String ReturnMessage, String RequestUser, ExcelType OutputExcelType);
        public abstract void BuildExportPortfolio(ref String ReturnMessage, String RequestUser, ExcelType OutputExcelType);
        public abstract void BuildExportData(ref String ReturnMessage, String RequestUser, ExcelType OutputExcelType);
        public abstract void BuildExportCostProjectionReport(ref String ReturnMessage, String RequestUser, ExcelType OutputExcelType);
        public abstract void BuildExportMultiFieldsReport(ref String ReturnMessage, String RequestUser, ExcelType OutputExcelType);
        public abstract void BuildExportOperationalMarginProjectionReport(ref String ReturnMessage, String RequestUser, ExcelType OutputExcelType);
        public abstract void BuildExportCostBenchmarkingReport(ref String ReturnMessage, String RequestUser, ExcelType OutputExcelType);
        public abstract void BuildExportCostVsDriversProjectionReport(ref String ReturnMessage, String RequestUser, ExcelType OutputExcelType, Int32 IdLanguage);
        public abstract void BuildExportReport(ref String ReturnMessage, String RequestUser, ExcelType OutputExcelType);
        public abstract void BuildExportMultiFieldsReportCPM(ref String ReturnMessage, String RequestUser, ExcelType OutputExcelType);

        // Data members
        protected ExportType objExportType = ExportType.Unknown;
        public List<DataReportObject> ExportReportCollection { get; set; }
        public InfragisticsExcel.Workbook objExcel = new InfragisticsExcel.Workbook();

    }

    public class ExcelWriter : DataFileWriter
    {

        private DataTable dtExcelData
        {
            get;
            set;
        }

        public ExcelWriter(ExportType ExportType)
        {
            objExportType = ExportType;
        }

        public override void BuildExportData(ref String ReturnMessage, String RequestUser, ExcelType OutputExcelType)
        {
            ReturnMessage = "Error processing export file.";

            if (OutputExcelType == ExcelType.Excel2007)
            {
                objExcel.SetCurrentFormat(InfragisticsExcel.WorkbookFormat.Excel2007);
            }
            else
            {
                objExcel.SetCurrentFormat(InfragisticsExcel.WorkbookFormat.Excel97To2003);
            }
            foreach (DataReportObject objReport in ExportReportCollection)
            {
                if (objReport != null)
                {
                    BuildWorksheetData(objReport.TabName, objReport.ReportName, RequestUser, objReport.ReportParameters, objReport.SessionDataTable);
                }
            }

            ReturnMessage = null;
        }

        private void BuildWorksheetData(String TabName, String ReportName, String UserName, String ReportParameters, DataTable SourceData)
        {
            Int32 intStartDataRow = 0; // Zero-based Index
            Int32 intCurrentColumn = 0; // Zero-based Index
            Int32 intDefaultWidth = 6500;
            InfragisticsExcel.Worksheet objWorksheet = objExcel.Worksheets.Add(TabName);

            if (SourceData != null)
            {
                if (SourceData.Rows.Count > 0)
                {
                    foreach (DataColumn objColumn in SourceData.Columns)
                    {
                        objWorksheet.Columns[intCurrentColumn].Width = intDefaultWidth;
                        objWorksheet.Rows[intStartDataRow].Cells[intCurrentColumn].Value = objColumn.ColumnName;
                        objWorksheet.Rows[intStartDataRow].Cells[intCurrentColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                        objWorksheet.Rows[intStartDataRow].Cells[intCurrentColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                        objWorksheet.Rows[intStartDataRow].Cells[intCurrentColumn].CellFormat.Alignment = InfragisticsExcel.HorizontalCellAlignment.Center;
                        objWorksheet.Rows[intStartDataRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.Red), null, InfragisticsExcel.FillPatternStyle.Solid);
                        intCurrentColumn += 1;
                    }
                    intStartDataRow += 1;
                    foreach (DataRow objRow in SourceData.Rows)
                    {
                        for (Int32 col = 0; col < SourceData.Columns.Count; col++)
                        {
                            objWorksheet.Rows[intStartDataRow].Cells[col].Value = objRow[col];
                            if (SourceData.Columns[col].DataType == typeof(System.Decimal) || SourceData.Columns[col].DataType == typeof(System.Double))
                            {
                                objWorksheet.Rows[intStartDataRow].Cells[col].CellFormat.FormatString = "#,##0.00";
                            }
                            if (SourceData.Columns[col].DataType == typeof(System.Boolean))
                            {
                                objWorksheet.Rows[intStartDataRow].Cells[col].Value = (Boolean)objWorksheet.Rows[intStartDataRow].Cells[col].Value == true ? "Yes" : "No";
                            }
                        }
                        intStartDataRow += 1;
                    }
                }
                else
                {
                    objWorksheet.Rows[intStartDataRow].Cells[0].Value = "No data...";
                    objWorksheet.Columns[0].Width = intDefaultWidth;
                }
            }
            else
            {
                objWorksheet.Rows[intStartDataRow].Cells[0].Value = "No data...";
                objWorksheet.Columns[0].Width = intDefaultWidth;
            }
        }

        public override void BuildExportFile(ref String ReturnMessage, String RequestUser, ExcelType OutputExcelType)
        {
            ReturnMessage = "Error processing export file.";

            if (OutputExcelType == ExcelType.Excel2007)
            {
                objExcel.SetCurrentFormat(InfragisticsExcel.WorkbookFormat.Excel2007);
            }
            else
            {
                objExcel.SetCurrentFormat(InfragisticsExcel.WorkbookFormat.Excel97To2003);
            }
            foreach (DataReportObject objReport in ExportReportCollection)
            {
                if (objReport != null)
                {
                    BuildWorksheet(objReport.TabName, objReport.ReportName, RequestUser, objReport.ReportParameters, objReport.SessionDataTable);
                }
            }

            ReturnMessage = null;
        }

        private void BuildWorksheet(String TabName, String ReportName, String UserName, String ReportParameters, DataTable SourceData)
        {
            Int32 intStartDataRow = 5; // Zero-based Index
            Int32 intCurrentColumn = 0; // Zero-based Index
            Int32 intDefaultWidth = 6500;
            Int32 intSpecialWidth = 9500;
            InfragisticsExcel.Worksheet objWorksheet = objExcel.Worksheets.Add(TabName);
            objWorksheet.Rows[0].Cells[0].Value = "Report Title:";
            objWorksheet.Rows[0].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
            objWorksheet.Rows[0].Cells[1].Value = ReportName;
            objWorksheet.Rows[1].Cells[0].Value = "Report Parameters:";
            objWorksheet.Rows[1].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
            objWorksheet.Rows[1].Cells[1].Value = ReportParameters;
            objWorksheet.Rows[2].Cells[0].Value = "Generated:";
            objWorksheet.Rows[2].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
            objWorksheet.Rows[2].Cells[1].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            objWorksheet.Rows[3].Cells[0].Value = "Generated By:";
            objWorksheet.Rows[3].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
            objWorksheet.Rows[3].Cells[1].Value = UserName;

            if (SourceData != null)
            {
                if (SourceData.Rows.Count > 0)
                {
                    foreach (DataColumn objColumn in SourceData.Columns)
                    {
                        if (intCurrentColumn < 2)
                            objWorksheet.Columns[intCurrentColumn].Width = intSpecialWidth;
                        else
                            objWorksheet.Columns[intCurrentColumn].Width = intDefaultWidth;
                        objWorksheet.Rows[intStartDataRow].Cells[intCurrentColumn].Value = objColumn.ColumnName.Replace("_"," ");
                        objWorksheet.Rows[intStartDataRow].Cells[intCurrentColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                        //objWorksheet.Rows[intStartDataRow].Cells[intCurrentColumn].CellFormat.FillPattern = InfragisticsExcel.FillPatternStyle.None;
                        //objWorksheet.Rows[intStartDataRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.LightGray), null, InfragisticsExcel.FillPatternStyle.Solid);
                        objWorksheet.Rows[intStartDataRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188)), null, InfragisticsExcel.FillPatternStyle.Solid);
                        objWorksheet.Rows[intStartDataRow].Cells[intCurrentColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                        objWorksheet.Rows[intStartDataRow].Cells[intCurrentColumn].CellFormat.Font.Height = 300;
                        objWorksheet.Rows[intStartDataRow].Cells[intCurrentColumn].CellFormat.Alignment = InfragisticsExcel.HorizontalCellAlignment.Center;
                        intCurrentColumn += 1;
                    }
                    intStartDataRow += 1;
                    foreach (DataRow objRow in SourceData.Rows)
                    {
                        for (Int32 col = 0; col < SourceData.Columns.Count; col++)
                        {
                            objWorksheet.Rows[intStartDataRow].Cells[col].Value = objRow[col];
                            if (SourceData.Columns[col].DataType == typeof(System.Decimal) || SourceData.Columns[col].DataType == typeof(System.Double))
                            {
                                objWorksheet.Rows[intStartDataRow].Cells[col].CellFormat.FormatString = "#,##0.00";
                            }
                        }
                        intStartDataRow += 1;
                    }
                }
                else
                {
                    objWorksheet.Rows[intStartDataRow].Cells[0].Value = "No data...";
                    objWorksheet.Columns[0].Width = intDefaultWidth;
                }
            }
            else
            {
                objWorksheet.Rows[intStartDataRow].Cells[0].Value = "No data...";
                objWorksheet.Columns[0].Width = intDefaultWidth;
            }
        }

        public override void BuildExportPortfolio(ref String ReturnMessage, String RequestUser, ExcelType OutputExcelType)
        {
            ReturnMessage = "Error processing export file.";

            if (OutputExcelType == ExcelType.Excel2007)
            {
                objExcel.SetCurrentFormat(InfragisticsExcel.WorkbookFormat.Excel2007);
            }
            else
            {
                objExcel.SetCurrentFormat(InfragisticsExcel.WorkbookFormat.Excel97To2003);
            }
            foreach (DataReportObject objReport in ExportReportCollection)
            {
                if (objReport != null)
                {
                    BuildWorksheetReport(objReport.TabName, objReport.ReportName, RequestUser, objReport.ReportParameters, objReport.SessionDataTable);
                }
            }

            ReturnMessage = null;
        }

        /// <summary>
        /// Generate portfolio export Excel report for Projection by Cost Type, specific code for this report.
        /// </summary>
        private void BuildWorksheetReport(String TabName, String ReportName, String UserName, String ReportParameters, DataTable SourceData)
        {
            Int32 intStartDataRow = 10;     // Zero-based Index
            Int32 intCurrentColumn = 0;     // Zero-based Index
            Int32 intDefaultWidth = 6500;
            String strPreviousField = "";   // Will track the field just processed
            String strPreviousProject = ""; // Will track the project just processed
            Int32 intFieldRow = 6;          // Row where Field title will go
            Int32 intProjectRow = 7;        // Row were Project Title will go
            Int32 intCurrentRow = 9;        // Current row in the worksheet
            Int32 intMovingColumn = 0;      // Keeps track of where the column is at
            Int32 intColumnHeaderRow = 9;   // Row where column headers will go   

            InfragisticsExcel.Worksheet objWorksheet = objExcel.Worksheets.Add(TabName);
            objWorksheet.Rows[0].Cells[0].Value = "Report Title:";
            objWorksheet.Rows[0].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
            objWorksheet.Rows[0].Cells[1].Value = ReportName;
            objWorksheet.Rows[1].Cells[0].Value = "Report Parameters:";
            objWorksheet.Rows[1].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
            objWorksheet.Rows[1].Cells[1].Value = ReportParameters;
            objWorksheet.Rows[2].Cells[0].Value = "Generated:";
            objWorksheet.Rows[2].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
            objWorksheet.Rows[2].Cells[1].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            objWorksheet.Rows[3].Cells[0].Value = "Generated By:";
            objWorksheet.Rows[3].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
            objWorksheet.Rows[3].Cells[1].Value = UserName;

            if (SourceData != null)
            {
                if (SourceData.Rows.Count > 0)
                {
                    foreach (DataColumn objColumn in SourceData.Columns)
                    {
                        if (!(objColumn.ColumnName == "Field" || objColumn.ColumnName == "Project"))
                        {
                            objWorksheet.Columns[intCurrentColumn].Width = intDefaultWidth;
                            if (objColumn.ColumnName != "YearProjection")
                            {
                                objWorksheet.Rows[intColumnHeaderRow].Cells[intCurrentColumn].Value = objColumn.ColumnName;
                                if (objColumn.ColumnName != "Total")
                                    objWorksheet.Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                else
                                    objWorksheet.Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188)), null, InfragisticsExcel.FillPatternStyle.Solid);
                            }
                            else
                            {
                                objWorksheet.Rows[intColumnHeaderRow].Cells[intCurrentColumn].Value = "Year";
                                objWorksheet.Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188)), null, InfragisticsExcel.FillPatternStyle.Solid);
                            }
                            objWorksheet.Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            objWorksheet.Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                            objWorksheet.Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Alignment = InfragisticsExcel.HorizontalCellAlignment.Center;
                            objWorksheet.Columns[intCurrentColumn].Width = intDefaultWidth;
                            intCurrentColumn += 1;
                        }
                    }
                    intCurrentColumn = 0;
                    foreach (DataRow objRow in SourceData.Rows)
                    {
                        // Check that the fields are the same from current row and previous row, also check whether project is the same within the field
                        if (strPreviousField != objRow["Field"].ToString() || strPreviousProject != objRow["Project"].ToString())
                        {
                            if (intCurrentRow > intStartDataRow) // Don't do it when it's first run of report creation
                            {
                                intCurrentColumn += SourceData.Columns.Count - 1; // Set current column to column at plus the number of columns in source
                                Int32 j = 0;
                                for (Int32 i = SourceData.Columns.Count - 1; i > 1; i--)
                                {
                                    //if (j == 0)
                                    //{
                                    //    objWorksheet.Rows[intColumnHeaderRow].Cells[intCurrentColumn + j].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(92,102,112)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                    //    objWorksheet.Rows[intColumnHeaderRow].Cells[intCurrentColumn + j].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                                    //}
                                    //else if (j == 1)
                                    //{
                                    //    objWorksheet.Rows[intColumnHeaderRow].Cells[intCurrentColumn + j].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(201,236,255)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                    //}
                                    //else
                                    //{
                                    //    objWorksheet.Rows[intColumnHeaderRow].Cells[intCurrentColumn + j].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.LightGray), null, InfragisticsExcel.FillPatternStyle.Solid);
                                    //}
                                    objWorksheet.Rows[intColumnHeaderRow].Cells[intCurrentColumn + j].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                    objWorksheet.Rows[intColumnHeaderRow].Cells[intCurrentColumn + j].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                                    objWorksheet.Rows[intColumnHeaderRow].Cells[intCurrentColumn + j].Value = objWorksheet.Rows[intColumnHeaderRow].Cells[intCurrentColumn - i].Value;
                                    objWorksheet.Rows[intColumnHeaderRow].Cells[intCurrentColumn + j].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                    objWorksheet.Columns[intCurrentColumn + j].Width = intDefaultWidth;
                                    j += 1;
                                }
                            }
                            objWorksheet.Rows[intFieldRow].Cells[intCurrentColumn + 1].CellFormat.TopBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Rows[intFieldRow].Cells[intCurrentColumn + 1].CellFormat.LeftBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Rows[intFieldRow].Cells[intCurrentColumn + 1].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
                            objWorksheet.Rows[intFieldRow].Cells[intCurrentColumn + 1].Value = "Field:";
                            objWorksheet.Rows[intFieldRow].Cells[intCurrentColumn + 1].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0,118,188));
                            objWorksheet.Rows[intFieldRow].Cells[intCurrentColumn + 1].CellFormat.Font.Height = 275;
                            objWorksheet.Rows[intFieldRow].Cells[intCurrentColumn + 2].CellFormat.TopBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Rows[intFieldRow].Cells[intCurrentColumn + 2].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
                            objWorksheet.Rows[intFieldRow].Cells[intCurrentColumn + 2].Value = objRow["Field"];
                            objWorksheet.Rows[intFieldRow].Cells[intCurrentColumn + 2].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188));
                            objWorksheet.Rows[intFieldRow].Cells[intCurrentColumn + 2].CellFormat.Font.Height = 275;
                            objWorksheet.Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.LeftBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.BottomBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
                            objWorksheet.Rows[intProjectRow].Cells[intCurrentColumn + 1].Value = "Project:";
                            objWorksheet.Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188));
                            objWorksheet.Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.Height = 275;
                            objWorksheet.Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.BottomBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
                            objWorksheet.Rows[intProjectRow].Cells[intCurrentColumn + 2].Value = objRow["Project"];
                            objWorksheet.Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188));
                            objWorksheet.Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Font.Height = 275;
                            objWorksheet.Rows[intFieldRow].Cells[intCurrentColumn + 3].CellFormat.TopBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Rows[intFieldRow].Cells[intCurrentColumn + 3].CellFormat.RightBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Rows[intProjectRow].Cells[intCurrentColumn + 3].CellFormat.RightBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Rows[intProjectRow].Cells[intCurrentColumn + 3].CellFormat.BottomBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            intCurrentRow = intStartDataRow;  //becomes 10
                            intMovingColumn = intCurrentColumn;
                        }

                        for (Int32 col = 2; col < SourceData.Columns.Count; col++)
                        {
                            if (col == 3)
                            {
                                Double intTotal = 0;
                                for (Int32 i = 4; i < SourceData.Columns.Count; i++)
                                {
                                    intTotal += Convert.ToDouble(objRow[i]);
                                }
                                objWorksheet.Rows[intCurrentRow].Cells[intMovingColumn].Value = intTotal;
                            }
                            else
                            {
                                objWorksheet.Rows[intCurrentRow].Cells[intMovingColumn].Value = objRow[col];
                            }
                            if (SourceData.Columns[col].DataType == typeof(System.Decimal) || SourceData.Columns[col].DataType == typeof(System.Double))
                            {
                                objWorksheet.Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.FormatString = "#,##0.00";
                            }
                            if (col == 2)
                            {
                                objWorksheet.Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(92, 102, 112)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                objWorksheet.Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                            }
                            else if (col == 3)
                            {
                                objWorksheet.Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(201,236,255)), null, InfragisticsExcel.FillPatternStyle.Solid);
                            }
 
                            intMovingColumn += 1;
                        }
                        intCurrentRow += 1;
                        intMovingColumn = intCurrentColumn;                 // Reset to start column
                        strPreviousField = objRow["Field"].ToString();      // Previous field becomes just processed field
                        strPreviousProject = objRow["Project"].ToString();  // Previous project becomes just processed project
                    }
                }
                else
                {
                    objWorksheet.Rows[intStartDataRow].Cells[0].Value = "No data...";
                    objWorksheet.Columns[0].Width = intDefaultWidth;
                }
            }
            else
            {
                objWorksheet.Rows[intStartDataRow].Cells[0].Value = "No data...";
                objWorksheet.Columns[0].Width = intDefaultWidth;
            }
        }

        public override void BuildExportCostProjectionReport(ref String ReturnMessage, String RequestUser, ExcelType OutputExcelType)
        {
            ReturnMessage = "Error processing export file.";

            if (OutputExcelType == ExcelType.Excel2007)
            {
                objExcel.SetCurrentFormat(InfragisticsExcel.WorkbookFormat.Excel2007);
            }
            else
            {
                objExcel.SetCurrentFormat(InfragisticsExcel.WorkbookFormat.Excel97To2003);
            }
            foreach (DataReportObject objReport in ExportReportCollection)
            {
                if (objReport != null)
                {
                    BuildWorksheetCostProjectionReport(objReport.TabName, objReport.ReportName, RequestUser, objReport.ReportParameters, objReport.SessionDataTable);
                }
            }

            ReturnMessage = null;
        }

        /// <summary>
        /// Generate export Excel report for Projection by Base and Incremental Line, specific code for this report.
        /// </summary>
        private void BuildWorksheetCostProjectionReport(String TabName, String ReportName, String UserName, String ReportParameters, DataTable SourceData)
        {
            Int32 intStartDataRow = 8;      // Zero-based Index
            Int32 intCurrentColumn = 0;     // Zero-based Index
            Int32 intDefaultWidth = 4000;
            Int32 intSpecialWidth = 9000;
            String strPreviousProject = ""; // Will track the project just processed
            String strPreviousField = "";   // Will track the field just processed
            Int32 intProjectRow = 5;        // Row were Project Title will go
            Int32 intCurrentRow = 7;        // Current row in the worksheet
            Int32 intMovingColumn = 0;      // Keeps track of where the column is at
            Int32 intColumnHeaderRow = 7;   // Row where column headers will go   

            InfragisticsExcel.Worksheet objWorksheet = objExcel.Worksheets.Add(TabName);

            if (SourceData != null)
            {
                if (SourceData.Rows.Count > 0)
                {
                    //Need to sort the datatable
                    DataView dv = SourceData.DefaultView;
                    if (TabName == "NewTab")
                    {
                        dv.Sort = "Field, SortOrder,ParentName,SortId ASC";//, ParentName, CostCategory ASC";
                        SourceData = dv.ToTable();
                    }
                    else
                    {
                        dv.Sort = "SortOrder,ParentName,SortId ASC";//, ParentName, CostCategory ASC";
                        SourceData = dv.ToTable();
                    }

                    foreach (DataRow objRow in SourceData.Rows)
                    {
                        // Check that the field are the same from current row and previous row, when they are not a new tab needs to be created
                        #region
                        if (TabName == "NewTab")
                        {
                            if (strPreviousField != objRow["Field"].ToString())
                            {
                                //Need to skip creating a new tab when it's initial report run
                                if (strPreviousField != "")
                                {
                                    objExcel.Worksheets[TabName].Name = strPreviousField;
                                    objExcel.Worksheets.Add(TabName);
                                    intCurrentRow = 3;                      // Set the current row for new tab
                                }

                                //Add report header information, common to all reports and pages
                                objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[0].Value = "Report Title:";
                                objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[1].Value = ReportName;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[0].Value = "Report Parameters:";
                                objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[1].Value = ReportParameters;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[0].Value = "Generated:";
                                objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[1].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[0].Value = "Generated By:";
                                objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[1].Value = UserName;
                            }
                        }
                        else
                        {
                            if (strPreviousProject == "")
                            {
                                //Add report header information, common to all reports and pages
                                objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[0].Value = "Report Title:";
                                objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[1].Value = ReportName;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[0].Value = "Report Parameters:";
                                objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[1].Value = ReportParameters;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[0].Value = "Generated:";
                                objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[1].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[0].Value = "Generated By:";
                                objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[1].Value = UserName;
                                intCurrentRow = 3;                      // Set the current row for Total Unit Cost tab
                            }
                        }
                        #endregion

                        // Check that the case are the same from current row and previous row
                        if (strPreviousProject != objRow["ParentName"].ToString())
                        {
                            #region //Add the case name
                            if (strPreviousProject == "") //only set original start point if first time through
                            {
                                intCurrentRow = intStartDataRow;  //becomes 8
                            }
                            else
                            {
                                intProjectRow = intCurrentRow + 2;                      // Set row for when the project changes
                                intColumnHeaderRow = intCurrentRow + 4;                 // Set the column header row
                                intCurrentRow += 5;                                     // Increment row
                            }
                            if (objRow["ParentName"].ToString().IndexOf("Total") != -1)
                            {
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.TopBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.LeftBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188));
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.Height = 300;
                                if (objRow["ParentName"].ToString() == "Total Field" && TabName == "Total Unit Cost")
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].Value = "All";
                                else
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].Value = objRow["ParentName"];
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188));
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.BottomBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.TopBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.RightBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
                                //objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].Value = objRow["ParentName"];
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188));
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.BottomBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
                            }
                            else
                            {
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.TopBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.LeftBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].Value = "Case:";
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Alignment = InfragisticsExcel.HorizontalCellAlignment.Right;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188));
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.Height = 275;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.BottomBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.TopBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.RightBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].Value = objRow["ParentName"];
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188));
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Font.Height = 275;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.BottomBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
                            }
                            #endregion

                            #region//Add column headers
                            foreach (DataColumn objColumn in SourceData.Columns)
                            {
                                if (!(objColumn.ColumnName == "ParentName" || objColumn.ColumnName == "Field" || objColumn.ColumnName == "SortOrder" || objColumn.ColumnName == "SortId"))
                                {
                                    if (objColumn.ColumnName != "Name" && objColumn.ColumnName != "CostCategory")
                                    {
                                        objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intDefaultWidth;
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].Value = objColumn.ColumnName;
                                        if (objColumn.Ordinal == 2)
                                            objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(228, 223, 236)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                        else
                                        {
                                            objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(92, 102, 112)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                            objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                                        }
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Alignment = InfragisticsExcel.HorizontalCellAlignment.Center;
                                    }
                                    else
                                    {
                                        objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intSpecialWidth;
                                        if (objColumn.ColumnName == "CostCategory")
                                            objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].Value = "Cost";
                                        else
                                            objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].Value = "";
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                                    }
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                    intCurrentColumn += 1;
                                }
                            }
                            intCurrentColumn = 0;
                            #endregion
                        }

                        #region//Second put data into rows
                        if (TabName == "NewTab")
                        {
                            for (Int32 col = 4; col < SourceData.Columns.Count; col++) //Don't want to display the first 4 columns
                            {
                                //If the row contains a grouped Ziff Account then grey it out
                                if (objRow[0].ToString().IndexOf('_') == -1)
                                {
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(217, 217, 217)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                }
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].Value = objRow[col];

                                if (SourceData.Columns[col].DataType == typeof(System.Decimal) || SourceData.Columns[col].DataType == typeof(System.Double))
                                {
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.FormatString = "#,##0.00";
                                }

                                intMovingColumn += 1;
                            }
                        }
                        else
                        {
                            for (Int32 col = 3; col < SourceData.Columns.Count; col++) //Don't want to display the first 3 columns
                            {
                                //If the row contains a grouped Ziff Account then grey it out
                                if (objRow[0].ToString().IndexOf('_') == -1)
                                {
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(217, 217, 217)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                }
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].Value = objRow[col];

                                if (SourceData.Columns[col].DataType == typeof(System.Decimal) || SourceData.Columns[col].DataType == typeof(System.Double))
                                {
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.FormatString = "#,##0.00";
                                }

                                intMovingColumn += 1;
                            }
                        }
                        #endregion

                        strPreviousProject = objRow["ParentName"].ToString();   // Previous project becomes just processed project
                        if (TabName == "NewTab")
                        {
                            strPreviousField = objRow["Field"].ToString();          // Previous filed becomes just processed field
                        }

                        intMovingColumn = intCurrentColumn;                     // Reset to start column
                        intCurrentRow += 1;                                     // Increment row
                    }
                    //Done processing, so need to rename last worked on tab
                    if (TabName == "NewTab")
                    {
                        objExcel.Worksheets[TabName].Name = strPreviousField;
                    }
                }
                else
                {
                    objWorksheet.Workbook.Worksheets[TabName].Rows[intStartDataRow].Cells[0].Value = "No data...";
                    objWorksheet.Workbook.Worksheets[TabName].Columns[0].Width = intDefaultWidth;
                }
            }
            else
            {
                objWorksheet.Workbook.Worksheets[TabName].Rows[intStartDataRow].Cells[0].Value = "No data...";
                objWorksheet.Workbook.Worksheets[TabName].Columns[0].Width = intDefaultWidth;
            }
        }

        public override void BuildExportMultiFieldsReport(ref String ReturnMessage, String RequestUser, ExcelType OutputExcelType)
        {
            ReturnMessage = "Error processing export file.";

            if (OutputExcelType == ExcelType.Excel2007)
            {
                objExcel.SetCurrentFormat(InfragisticsExcel.WorkbookFormat.Excel2007);
            }
            else
            {
                objExcel.SetCurrentFormat(InfragisticsExcel.WorkbookFormat.Excel97To2003);
            }
            foreach (DataReportObject objReport in ExportReportCollection)
            {
                if (objReport != null)
                {
                    BuildWorksheetMultiFieldsReport(objReport.TabName, objReport.ReportName, RequestUser, objReport.ReportParameters, objReport.SessionDataTable);
                }
            }

            ReturnMessage = null;
        }

        /// <summary>
        /// Generate export Excel report for Multiple Fields Projection, specific code for this report.
        /// </summary>
        private void BuildWorksheetMultiFieldsReport(String TabName, String ReportName, String UserName, String ReportParameters, DataTable SourceData)
        {
            Int32 intStartDataRow = 8;      // Zero-based Index
            Int32 intCurrentColumn = 0;     // Zero-based Index
            Int32 intDefaultWidth = 5000;
            Int32 intSpecialWidth = 9000;
            String strPreviousField = ""; // Will track the project just processed
            Int32 intProjectRow = 5;        // Row were Project Title will go
            Int32 intCurrentRow = 7;        // Current row in the worksheet
            Int32 intMovingColumn = 0;      // Keeps track of where the column is at
            Int32 intColumnHeaderRow = 7;   // Row where column headers will go   

            InfragisticsExcel.Worksheet objWorksheet = objExcel.Worksheets.Add(TabName);

            if (SourceData != null)
            {
                if (SourceData.Rows.Count > 0)
                {
                    //Need to sort the datatable
                    //DataView dv = SourceData.DefaultView;
                    //dv.Sort = "Field, CostCategory ASC";
                    //SourceData = dv.ToTable();

                    foreach (DataRow objRow in SourceData.Rows)
                    {
                        // Check that the case are the same from current row and previous row
                        if (strPreviousField != objRow["Field"].ToString())
                        {
                            //Need to skip creating a new tab when it's initial report run
                            if (strPreviousField != "")
                            {
                                objExcel.Worksheets[TabName].Name = strPreviousField;
                                objExcel.Worksheets.Add(TabName);
                            }

                            //Add report header information, common to all reports and pages
                            objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[0].Value = "Report Title:";
                            objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[1].Value = ReportName;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[0].Value = "Report Parameters:";
                            objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[1].Value = ReportParameters;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[0].Value = "Generated:";
                            objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[1].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[0].Value = "Generated By:";
                            objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[1].Value = UserName;

                            //Add the case name 
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.TopBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.LeftBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].Value = "Field:";
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0,118,188));
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.Height = 275;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.BottomBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.TopBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].Value = objRow["Field"];
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188));
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Font.Height = 275;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.BottomBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 3].CellFormat.TopBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 3].CellFormat.RightBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 3].CellFormat.BottomBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;

                            //Add column headers
                            foreach (DataColumn objColumn in SourceData.Columns)
                            {
                                if (!(objColumn.ColumnName == "Field" || objColumn.ColumnName == "SortID"))
                                {
                                    objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intDefaultWidth;
                                    if (objColumn.ColumnName != "Name" && objColumn.ColumnName != "CostCategory")
                                    {
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].Value = objColumn.ColumnName;
                                        if (objColumn.Ordinal == 2)
                                            objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(228, 223, 236)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                        else
                                        {
                                            objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(92, 102, 112)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                            objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Alignment = InfragisticsExcel.HorizontalCellAlignment.Center;
                                        }
                                    }
                                    else
                                    {
                                        if (objColumn.ColumnName == "CostCategory")
                                        {
                                            objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intSpecialWidth;
                                            objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].Value = "Cost";
                                        }
                                        else
                                        {
                                            objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intSpecialWidth;
                                            objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].Value = "";
                                        }
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                    }
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                                    intCurrentColumn += 1;
                                }
                            }
                            intCurrentColumn = 0;
                            intCurrentRow = intStartDataRow;  //becomes 8
                        }

                        //Second put data into rows
                        for (Int32 col = 0; col < SourceData.Columns.Count; col++)
                        {
                            //If the row contains a grouped Ziff Account then grey it out
                            if (objRow[0].ToString().IndexOf('_') == -1)
                            {
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(217, 217, 217)), null, InfragisticsExcel.FillPatternStyle.Solid);
                            }

                            //Need to skip columns Name(2) and SortID(0)
                            if (col != 2 && col != 0)
                            {
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].Value = objRow[col];

                                if (SourceData.Columns[col].DataType == typeof(System.Decimal) || SourceData.Columns[col].DataType == typeof(System.Double))
                                {
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.FormatString = "#,##0.00";
                                }

                                intMovingColumn += 1;
                            }
                        }

                        intCurrentRow += 1;
                        intMovingColumn = intCurrentColumn;                     // Reset to start column
                        strPreviousField = objRow["Field"].ToString();   // Previous project becomes just processed project
                    }
                    //Done processing, so need to rename last worked on tab
                    objExcel.Worksheets[TabName].Name = strPreviousField;
                }
                else
                {
                    objWorksheet.Workbook.Worksheets[TabName].Rows[intStartDataRow].Cells[0].Value = "No data...";
                    objWorksheet.Workbook.Worksheets[TabName].Columns[0].Width = intDefaultWidth;
                }
            }
            else
            {
                objWorksheet.Workbook.Worksheets[TabName].Rows[intStartDataRow].Cells[0].Value = "No data...";
                objWorksheet.Workbook.Worksheets[TabName].Columns[0].Width = intDefaultWidth;
            }
        }

        public override void BuildExportCostBenchmarkingReport(ref String ReturnMessage, String RequestUser, ExcelType OutputExcelType)
        {
            ReturnMessage = "Error processing export file.";

            if (OutputExcelType == ExcelType.Excel2007)
            {
                objExcel.SetCurrentFormat(InfragisticsExcel.WorkbookFormat.Excel2007);
            }
            else
            {
                objExcel.SetCurrentFormat(InfragisticsExcel.WorkbookFormat.Excel97To2003);
            }
            foreach (DataReportObject objReport in ExportReportCollection)
            {
                if (objReport != null)
                {
                    BuildWorksheetCostBenchmarkingReport(objReport.TabName, objReport.ReportName, RequestUser, objReport.ReportParameters, objReport.SessionDataTable);
                }
            }

            ReturnMessage = null;
        }

        /// <summary>
        /// Generate export Excel report for Cost Benchmarking, specific code for this report.
        /// </summary>
        private void BuildWorksheetCostBenchmarkingReport(String TabName, String ReportName, String UserName, String ReportParameters, DataTable SourceData)
        {
            Int32 intStartDataRow = 9;      // Zero-based Index
            Int32 intCurrentColumn = 0;     // Zero-based Index
            Int32 intDefaultWidth = 6000;
            Int32 intSpecialWidth = 9000;
            Int32 intProjectRow = 5;        // Row were Project Title will go
            Int32 intTotalRow = 8;          // Row were Total Cost line will go
            Int32 intCurrentRow = 7;        // Current row in the worksheet
            Int32 intMovingColumn = 0;      // Keeps track of where the column is at
            Int32 intColumnHeaderRow = 7;   // Row where column headers will go   

            InfragisticsExcel.Worksheet objWorksheet = objExcel.Worksheets.Add(TabName);

            string[] values = ReportParameters.Split('^');

            //Add report header information, common to all reports and pages
            objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[0].Value = "Report Title:";
            objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
            objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[1].Value = ReportName;
            objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[0].Value = "Report Parameters:";
            objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
            objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[1].Value = ReportParameters.Replace("^",",");
            objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[0].Value = "Generated:";
            objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
            objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[1].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[0].Value = "Generated By:";
            objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
            objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[1].Value = UserName;

            //Add the case name 
            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.TopBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.LeftBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].Value = values[0].Replace("'","").Replace(":","(s):");
            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0,118,188));
            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.BottomBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.RightBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.Height = 275;
            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].CellFormat.WrapText = InfragisticsExcel.ExcelDefaultableBoolean.True;
            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].CellFormat.VerticalAlignment = InfragisticsExcel.VerticalCellAlignment.Top;

            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 3].CellFormat.TopBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 3].CellFormat.LeftBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 3].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 3].Value = values[1].Replace("'", "").Replace(":", "(s):");
            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 3].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 3].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188));
            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 3].CellFormat.BottomBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 3].CellFormat.RightBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 3].CellFormat.Font.Height = 275;

            if (SourceData != null)
            {
                if (SourceData.Rows.Count > 0)
                {
                    //Add column headers
                    foreach (DataColumn objColumn in SourceData.Columns)
                    {
                        switch (objColumn.ColumnName)
                        {
                            case "SortOrderExport":
                                objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intSpecialWidth;
                                intCurrentColumn -= 1;
                                break;
                            case "Code":
                                objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intSpecialWidth;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].Value = "";
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                break;
                            case "Description":
                                objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intSpecialWidth;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].Value = "Total Unit Cost";
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Font.Height = 300;
                                break;
                            default:
                                objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intDefaultWidth;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].Value = objColumn.ColumnName;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Alignment = InfragisticsExcel.HorizontalCellAlignment.Center;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(92, 102, 112)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                                break;
                        }
                        intCurrentColumn += 1;
                    }
                    intCurrentColumn = 0;
                    intCurrentRow = intStartDataRow;  //becomes 9

                    //Second put data into rows
                    foreach (DataRow objRow in SourceData.Rows)
                    {
                        for (Int32 col = 1; col < SourceData.Columns.Count; col++)
                        {
                            if (objRow[1].ToString() == "Total Unit Cost")
                            {
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intTotalRow].Cells[intMovingColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(201, 236, 255)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intTotalRow].Cells[intMovingColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intTotalRow].Cells[intCurrentColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.Black);
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intTotalRow].Cells[intMovingColumn].Value = objRow[col];
                                if (SourceData.Columns[col].DataType == typeof(System.Decimal) || SourceData.Columns[col].DataType == typeof(System.Double))
                                {
                                    if (SourceData.Locale.ToString() == "es-ES")
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intTotalRow].Cells[intMovingColumn].CellFormat.FormatString = "#.##0,00";
                                    else
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intTotalRow].Cells[intMovingColumn].CellFormat.FormatString = "#,##0.00";
                                }

                            }
                            else if (objRow[1].ToString() == "Total Production")
                            {
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow + 2].Cells[intMovingColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(201, 236, 255)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow + 2].Cells[intMovingColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow + 2].Cells[intCurrentColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.Black);
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow + 2].Cells[intMovingColumn].Value = objRow[col];
                                if (SourceData.Columns[col].DataType == typeof(System.Decimal) || SourceData.Columns[col].DataType == typeof(System.Double))
                                {
                                    if (SourceData.Locale.ToString() == "es-ES")
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow + 2].Cells[intMovingColumn].CellFormat.FormatString = "#.##0,00";
                                    else
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow + 2].Cells[intMovingColumn].CellFormat.FormatString = "#,##0.00";
                                }
                                else
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow + 2].Cells[intMovingColumn].Value = objRow[col];
                            }
                            else
                            {
                                //If the row contains a grouped Ziff Account then grey it out
                                if (objRow[0].ToString().IndexOf('_') == -1)
                                {
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(217, 217, 217)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                }
                                else
                                {
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(255, 255, 255)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                }
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].Value = objRow[col];
                                if (SourceData.Columns[col].DataType == typeof(System.Decimal) || SourceData.Columns[col].DataType == typeof(System.Double))
                                {
                                    if (SourceData.Locale.ToString() == "es-ES")
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.FormatString = "#.##0,00";
                                    else
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.FormatString = "#,##0.00";
                                }
                                else
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].Value = objRow[col];

                            }
                            intMovingColumn += 1;
                        }

                        if (objRow[0].ToString() != "Total Cost")
                        {
                            intCurrentRow += 1;
                        }
                        intMovingColumn = intCurrentColumn;                     // Reset to start column
                    }
                }
                else
                {
                    objWorksheet.Workbook.Worksheets[TabName].Rows[intStartDataRow].Cells[0].Value = "No data...";
                    objWorksheet.Workbook.Worksheets[TabName].Columns[0].Width = intDefaultWidth;
                }
            }
            else
            {
                objWorksheet.Workbook.Worksheets[TabName].Rows[intStartDataRow].Cells[0].Value = "No data...";
                objWorksheet.Workbook.Worksheets[TabName].Columns[0].Width = intDefaultWidth;
            }
        }

        public override void BuildExportOperationalMarginProjectionReport(ref String ReturnMessage, String RequestUser, ExcelType OutputExcelType)
        {
            ReturnMessage = "Error processing export file.";

            if (OutputExcelType == ExcelType.Excel2007)
            {
                objExcel.SetCurrentFormat(InfragisticsExcel.WorkbookFormat.Excel2007);
            }
            else
            {
                objExcel.SetCurrentFormat(InfragisticsExcel.WorkbookFormat.Excel97To2003);
            }
            foreach (DataReportObject objReport in ExportReportCollection)
            {
                if (objReport != null)
                {
                    BuildWorksheetOperationalMarginProjectionReport(objReport.TabName, objReport.ReportName, RequestUser, objReport.ReportParameters, objReport.SessionDataTable);
                }
            }

            ReturnMessage = null;
        }

        /// <summary>
        /// Generate export Excel report for Operational Margin Projection, specific code for this report.
        /// </summary>
        private void BuildWorksheetOperationalMarginProjectionReport(String TabName, String ReportName, String UserName, String ReportParameters, DataTable SourceData)
        {
            Int32 intStartDataRow = 8;      // Zero-based Index
            Int32 intCurrentColumn = 0;     // Zero-based Index
            Int32 intDefaultWidth = 4000;
            Int32 intSpecialWidth = 6500;
            String strPreviousField = "";   // Will track the project just processed
            Int32 intProjectRow = 5;        // Row were Project Title will go
            Int32 intCurrentRow = 7;        // Current row in the worksheet
            Int32 intMovingColumn = 0;      // Keeps track of where the column is at
            Int32 intColumnHeaderRow = 7;   // Row where column headers will go   

            InfragisticsExcel.Worksheet objWorksheet = objExcel.Worksheets.Add(TabName);

            if (SourceData != null)
            {
                if (SourceData.Rows.Count > 0)
                {
                    //Add report header information, common to all reports and pages
                    objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[0].Value = "Report Title:";
                    objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                    objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[1].Value = ReportName;
                    objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[0].Value = "Report Parameters:";
                    objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                    objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[1].Value = ReportParameters;
                    objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[0].Value = "Generated:";
                    objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                    objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[1].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[0].Value = "Generated By:";
                    objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                    objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[1].Value = UserName;

                    ////Need to sort the datatable
                    //DataView dv = SourceData.DefaultView;
                    //dv.Sort = "Field, CostCategory ASC";
                    //SourceData = dv.ToTable();

                    foreach (DataRow objRow in SourceData.Rows)
                    {
                        // Check that the case are the same from current row and previous row
                        if (strPreviousField != objRow["FieldName"].ToString())
                        {
                            ////Need to skip creating a new tab when it's initial report run
                            //if (strPreviousField != "")
                            //{
                            //    objExcel.Worksheets[TabName].Name = strPreviousField;
                            //    objExcel.Worksheets.Add(TabName);
                            //}

                            //Add the case name 
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.TopBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.LeftBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].Value = "Field:";
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188));
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.Height = 275;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.BottomBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.TopBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.RightBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].Value = objRow["FieldName"];
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188));
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Font.Height = 275;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.BottomBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);

                            //Add column headers
                            foreach (DataColumn objColumn in SourceData.Columns)
                            {
                                if (!(objColumn.ColumnName == "FieldName"))
                                {
                                    if (objColumn.ColumnName != "Unit")
                                    {
                                        objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intDefaultWidth;
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].Value = objColumn.ColumnName;
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(92, 102, 112)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Alignment = InfragisticsExcel.HorizontalCellAlignment.Center;
                                    }
                                    else
                                    {
                                        objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intSpecialWidth;
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].Value = "";
                                    }
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                    intCurrentColumn += 1;
                                }
                            }
                            intCurrentColumn = 0;
                            intColumnHeaderRow += 14;
                            if (strPreviousField == "") //only set orriginal start point if first time through
                            {
                                intCurrentRow = intStartDataRow;  //becomes 8
                            }
                            else
                            {
                                intCurrentRow = intProjectRow + 3;
                            }
                        }

                        //Second put data into rows, starting at column 2 (ordinal 1)
                        for (Int32 col = 1; col < SourceData.Columns.Count; col++)
                        {
                            ////If the row contains a grouped Ziff Account then grey it out
                            //if (objRow[0].ToString().IndexOf('_') == -1)
                            //{
                            //    objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(217, 217, 217)), null, InfragisticsExcel.FillPatternStyle.Solid);
                            //}

                            ////Need to skip column Field(2)
                            //if (col != 2)
                            //{
                            //Add row before Production and Absolute Margin
                            if ((objRow[1].ToString() == "Production (k BOE)" || objRow[1].ToString().IndexOf("Absolute Margin") > -1) && col ==1)
                                intCurrentRow += 1;

                            if ((objRow[1].ToString().IndexOf("Unit Margin") > -1 || objRow[1].ToString().IndexOf("Absolute Margin") > -1) && col == 1)
                            {
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                            }

                            if ((objRow[1].ToString().IndexOf("Unit Margin") > -1 || objRow[1].ToString().IndexOf("Absolute Margin") > -1) && col != 1)
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(217, 217, 217)), null, InfragisticsExcel.FillPatternStyle.Solid);

                            if ((objRow[1].ToString() == "Production (k BOE)") && col == 1)
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(201, 236, 255)), null, InfragisticsExcel.FillPatternStyle.Solid);

                            objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].Value = objRow[col];

                            if (SourceData.Columns[col].DataType == typeof(System.Decimal) || SourceData.Columns[col].DataType == typeof(System.Double))
                            {
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.FormatString = "#,##0.00";
                            }

                            intMovingColumn += 1;
                            //}
                        }

                        intCurrentRow += 1;
                        intMovingColumn = intCurrentColumn;                     // Reset to start column
                        strPreviousField = objRow["FieldName"].ToString();      // Previous project becomes just processed project
                        intProjectRow = intCurrentRow + 2;                      // Start row of next field if any
                    }
                    ////Done processing, so need to rename last worked on tab
                    //objExcel.Worksheets[TabName].Name = strPreviousField;
                }
                else
                {
                    objWorksheet.Workbook.Worksheets[TabName].Rows[intStartDataRow].Cells[0].Value = "No data...";
                    objWorksheet.Workbook.Worksheets[TabName].Columns[0].Width = intDefaultWidth;
                }
            }
            else
            {
                objWorksheet.Workbook.Worksheets[TabName].Rows[intStartDataRow].Cells[0].Value = "No data...";
                objWorksheet.Workbook.Worksheets[TabName].Columns[0].Width = intDefaultWidth;
            }
        }

        public override void BuildExportCostVsDriversProjectionReport(ref String ReturnMessage, String RequestUser, ExcelType OutputExcelType, Int32 IdLanguage)
        {
            ReturnMessage = "Error processing export file.";

            if (OutputExcelType == ExcelType.Excel2007)
            {
                objExcel.SetCurrentFormat(InfragisticsExcel.WorkbookFormat.Excel2007);
            }
            else
            {
                objExcel.SetCurrentFormat(InfragisticsExcel.WorkbookFormat.Excel97To2003);
            }
            foreach (DataReportObject objReport in ExportReportCollection)
            {
                if (objReport != null)
                {
                    BuildWorksheetCostVsDriversProjectionReport(objReport.TabName, objReport.ReportName, RequestUser, objReport.ReportParameters, objReport.SessionDataTable, IdLanguage);
                }
            }

            ReturnMessage = null;
        }

        /// <summary>
        /// Generate export Excel report for Projection by Cost Category, specific code for this report.
        /// </summary>
        private void BuildWorksheetCostVsDriversProjectionReport(String TabName, String ReportName, String UserName, String ReportParameters, DataTable SourceData, Int32 IdLanguage)
        {
            Int32 intStartDataRow = 8;      // Zero-based Index
            Int32 intCurrentColumn = 0;     // Zero-based Index
            Int32 intDefaultWidth = 4000;
            Int32 intSpecialWidth = 6500;
            String strPreviousField = "";   // Will track the project just processed
            Int32 intProjectRow = 5;        // Row were Project Title will go
            Int32 intCurrentRow = 7;        // Current row in the worksheet
            Int32 intMovingColumn = 0;      // Keeps track of where the column is at
            Int32 intColumnHeaderRow = 7;   // Row where column headers will go   
            Boolean boolFirstPass = true;   // Track for header creation

            InfragisticsExcel.Worksheet objWorksheet = objExcel.Worksheets.Add(TabName);

            if (SourceData != null)
            {
                if (SourceData.Rows.Count > 0)
                {
                    //Add report header information, common to all reports and pages
                    objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[0].Value = "Report Title:";
                    objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                    objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[1].Value = ReportName;
                    objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[0].Value = "Report Parameters:";
                    objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                    objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[1].Value = ReportParameters;
                    objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[0].Value = "Generated:";
                    objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                    objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[1].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[0].Value = "Generated By:";
                    objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                    objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[1].Value = UserName;

                    foreach (DataRow objRow in SourceData.Rows)
                    {
                        if (boolFirstPass)
                        {
                            //Add the case name 
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.TopBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.LeftBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].Value = "Field:";
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188));
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.Height = 275;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.BottomBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.TopBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
                            if (IdLanguage == 1)
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].Value = ReportParameters.Substring(ReportParameters.IndexOf("Field:") + 8, ReportParameters.IndexOf("'", ReportParameters.IndexOf("Field:") + 8) - ReportParameters.IndexOf("'", ReportParameters.IndexOf("Field:") + 7) - 1);
                            else
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].Value = ReportParameters.Substring(ReportParameters.IndexOf("Campo:") + 8, ReportParameters.IndexOf("'", ReportParameters.IndexOf("Campo:") + 8) - ReportParameters.IndexOf("'", ReportParameters.IndexOf("Campo:") + 7) - 1);
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188));
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Font.Height = 275;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.BottomBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 3].CellFormat.TopBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 3].CellFormat.RightBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 3].CellFormat.BottomBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;

                            //Add column headers
                            foreach (DataColumn objColumn in SourceData.Columns)
                            {
                                if (!(objColumn.ColumnName == "SortOrder"))
                                {
                                    if (objColumn.ColumnName != "Unit" && objColumn.ColumnName != "CostCategory" && objColumn.ColumnName != "UnitSubname")
                                    {
                                        objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intDefaultWidth;
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].Value = objColumn.ColumnName;
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(92, 102, 112)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Alignment = InfragisticsExcel.HorizontalCellAlignment.Center;
                                    }
                                    else
                                    {
                                        objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intSpecialWidth;
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(92, 102, 112)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Font.Height = 250;
                                        if (objColumn.ColumnName == "CostCategory")
                                        {
                                            objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].Value = "Cost Category";
                                        }
                                        else if (objColumn.ColumnName == "UnitSubname")
                                        {
                                            objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intSpecialWidth + intDefaultWidth;
                                            objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].Value = "Name";
                                        }
                                        else
                                        {
                                            objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].Value = "";
                                        }
                                    }
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                    intCurrentColumn += 1;
                                }
                            }
                            intCurrentColumn = 0;
                            //intColumnHeaderRow += 14;
                            //if (strPreviousField == "") //only set orriginal start point if first time through
                            //{
                            intCurrentRow = intStartDataRow;  //becomes 8
                            //}
                            //else
                            //{
                            //    intCurrentRow = intProjectRow + 3;
                            //}
                            boolFirstPass = false;
                        }

                        //Second put data into rows, starting at column 2 (ordinal 1)
                        for (Int32 col = 1; col < SourceData.Columns.Count; col++)
                        {
                            if (objRow[1].ToString() == "Grand Totals" && col <= 3)
                            {
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Font.Height = 300;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            }
                            else if (objRow[1].ToString() == "Grand Totals")
                            {
                            //    objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(217, 217, 217)), null, InfragisticsExcel.FillPatternStyle.Solid);
                            //    objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            //    objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.Black);
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            }

                            if (objRow[1].ToString() == "Total")
                            {
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            }

                            if ((objRow[1].ToString() == "Size Drivers" || objRow[1].ToString() == "Performance Drivers" || objRow[1].ToString() == "Economic Drivers"))
                            {
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Font.Height = 300;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            }

                            objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].Value = objRow[col];

                            if (SourceData.Columns[col].DataType == typeof(System.Decimal) || SourceData.Columns[col].DataType == typeof(System.Double))
                            {
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.FormatString = "#,##0.00";
                            }

                            intMovingColumn += 1;
                        }

                        intCurrentRow += 1;
                        intMovingColumn = intCurrentColumn;                     // Reset to start column
                        intProjectRow = intCurrentRow + 2;                      // Start row of next field if any
                    }
                }
                else
                {
                    objWorksheet.Workbook.Worksheets[TabName].Rows[intStartDataRow].Cells[0].Value = "No data...";
                    objWorksheet.Workbook.Worksheets[TabName].Columns[0].Width = intDefaultWidth;
                }
            }
            else
            {
                objWorksheet.Workbook.Worksheets[TabName].Rows[intStartDataRow].Cells[0].Value = "No data...";
                objWorksheet.Workbook.Worksheets[TabName].Columns[0].Width = intDefaultWidth;
            }
        }

        public override void BuildExportReport(ref String ReturnMessage, String RequestUser, ExcelType OutputExcelType)
        {
            ReturnMessage = "Error processing export file.";

            if (OutputExcelType == ExcelType.Excel2007)
            {
                objExcel.SetCurrentFormat(InfragisticsExcel.WorkbookFormat.Excel2007);
            }
            else
            {
                objExcel.SetCurrentFormat(InfragisticsExcel.WorkbookFormat.Excel97To2003);
            }
            foreach (DataReportObject objReport in ExportReportCollection)
            {
                if (objReport != null)
                {
                    BuildWorksheetExportReport(objReport.TabName, objReport.ReportName, RequestUser, objReport.ReportParameters, objReport.SessionDataTable);
                }
            }

            ReturnMessage = null;
        }

        /// <summary>
        /// Generate export Excel report for Projection by Cost Type (Summary), specific code for this report.
        /// </summary>
        private void BuildWorksheetExportReport(String TabName, String ReportName, String UserName, String ReportParameters, DataTable SourceData)
        {
            Int32 intStartDataRow = 10;     // Zero-based Index
            Int32 intCurrentColumn = 0;     // Zero-based Index
            Int32 intDefaultWidth = 4000;
            Int32 intSpecialWidth = 9000;
            String strPreviousProject = ""; // Will track the project just processed
            String strPreviousField = "";   // Will track the field just processed
            Int32 intCurrentRow = 4;        // Current row in the worksheet
            Int32 intMovingColumn = 0;      // Keeps track of where the column is at

            InfragisticsExcel.Worksheet objWorksheet = objExcel.Worksheets.Add(TabName);
            //objWorksheet.Rows[0].Cells[0].Value = "Report Title:";
            //objWorksheet.Rows[0].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
            //objWorksheet.Rows[0].Cells[1].Value = ReportName;
            //objWorksheet.Rows[1].Cells[0].Value = "Report Parameters:";
            //objWorksheet.Rows[1].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
            //objWorksheet.Rows[1].Cells[1].Value = ReportParameters;
            //objWorksheet.Rows[2].Cells[0].Value = "Generated:";
            //objWorksheet.Rows[2].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
            //objWorksheet.Rows[2].Cells[1].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //objWorksheet.Rows[3].Cells[0].Value = "Generated By:";
            //objWorksheet.Rows[3].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
            //objWorksheet.Rows[3].Cells[1].Value = UserName;

            if (SourceData != null)
            {
                if (SourceData.Rows.Count > 0)
                {
                    //Need to sort the datatable (just in case)
                    DataView dv = SourceData.DefaultView;
                    if (TabName == "NewTab")
                    {
                        dv.Sort = "Field, SortOrder, ProjectionCriteria ASC";
                        SourceData = dv.ToTable();
                    }
                    else
                    {
                        dv.Sort = "SortOrder, Field, ProjectionCriteria ASC";
                        SourceData = dv.ToTable();
                    }

                    foreach (DataRow objRow in SourceData.Rows)
                    {
                        // Check that the field are the same from current row and previous row, when they are not a new tab needs to be created
                        #region
                        if (TabName == "NewTab")
                        {
                            if (strPreviousField != objRow["Field"].ToString())
                            {
                                //Need to skip creating a new tab when it's initial report run
                                if (strPreviousField != "")
                                {
                                    objExcel.Worksheets[TabName].Name = strPreviousField;
                                    objExcel.Worksheets.Add(TabName);
                                    intCurrentRow = 3;                      // Set the current row for new tab
                                }

                                //Add report header information, common to all reports and pages
                                objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[0].Value = "Report Title:";
                                objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[1].Value = ReportName;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[0].Value = "Report Parameters:";
                                objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[1].Value = ReportParameters;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[0].Value = "Generated:";
                                objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[1].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[0].Value = "Generated By:";
                                objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[1].Value = UserName;
                            }
                        }
                        else
                        {
                            if (strPreviousProject == "")
                            {
                                //Add report header information, common to all reports and pages
                                objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[0].Value = "Report Title:";
                                objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[1].Value = ReportName;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[0].Value = "Report Parameters:";
                                objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[1].Value = ReportParameters;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[0].Value = "Generated:";
                                objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[1].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[0].Value = "Generated By:";
                                objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[1].Value = UserName;
                                intCurrentRow = 3;                      // Set the current row for Total Unit Cost tab
                            }
                        }
                        #endregion

                        // Check that the ProjectId are the same from current row and previous row
                        if (strPreviousProject != objRow["ProjectId"].ToString())
                        {
                            intCurrentRow++;    //Force movement of row 
                            //Need to create the header row as a new ProjectId has been found
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intCurrentColumn].Value = objRow["ProjectId"].ToString();
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intCurrentColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intCurrentColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188)), null, InfragisticsExcel.FillPatternStyle.Solid);
                            objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intSpecialWidth;
                            Int32 j = 0;
                            //Colour the remaining columns in the row
                            for (Int32 i = SourceData.Columns.Count - 2; i > 1; i--)
                            {
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intCurrentColumn + j].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn + j].Width = intDefaultWidth;
                                j += 1;
                            }
                            intCurrentRow++;   //Move to next row to put section headers
                            foreach (DataColumn objColumn in SourceData.Columns)
                            {
                                if (!(objColumn.ColumnName == "Field" || objColumn.ColumnName == "SortOrder" || objColumn.ColumnName == "ProjectId"))
                                {
                                    if (objColumn.ColumnName == "ProjectionCriteria")
                                    {
                                        switch (Convert.ToInt32(objRow["SortOrder"].ToString()))
                                        {
                                            case 1:
                                            case 3:
                                            case 5:
                                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intCurrentColumn].Value = "Projection Criteria";
                                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(92, 102, 112)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intCurrentColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                                                objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intSpecialWidth;
                                                break;
                                            case 2:
                                            case 4:
                                            case 6:
                                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intCurrentColumn].Value = "";
                                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(92, 102, 112)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intCurrentColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                                                objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intSpecialWidth;
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intDefaultWidth;
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intCurrentColumn].Value = objColumn.ColumnName;
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(92, 102, 112)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intCurrentColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intCurrentColumn].CellFormat.Alignment = InfragisticsExcel.HorizontalCellAlignment.Center;
                                    }
                                    intCurrentColumn += 1;
                                }
                            }
                            intCurrentRow++;    //Move to next row to add data from row
                            intMovingColumn = 0;
                            for (Int32 col = 3; col < SourceData.Columns.Count; col++)
                            {
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].Value = objRow[col];
                                if (SourceData.Columns[col].DataType == typeof(System.Decimal) || SourceData.Columns[col].DataType == typeof(System.Double))
                                {
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.FormatString = "#,##0.00";
                                }
                                intMovingColumn += 1;
                            }
                        }
                        else
                        {
                            intMovingColumn = 0;
                            for (Int32 col = 3; col < SourceData.Columns.Count; col++)
                            {
                                if (col == 3)
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].Value = objRow[col].ToString().Replace("zzz", "");
                                else
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].Value = objRow[col];
                                if (SourceData.Columns[col].DataType == typeof(System.Decimal) || SourceData.Columns[col].DataType == typeof(System.Double))
                                {
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.FormatString = "#,##0.00";
                                }
                                if (objRow[3].ToString() == "zzzTotal")
                                {
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                }
                                intMovingColumn += 1;
                            }
                        }

                        intCurrentRow += 1;
                        //intMovingColumn = intCurrentColumn;                 // Reset to start column
                        if (TabName == "NewTab")
                        {
                            strPreviousField = objRow["Field"].ToString();      // Previous field becomes just processed field
                        }
                        strPreviousProject = objRow["ProjectId"].ToString();  // Previous project becomes just processed project
                        intCurrentColumn = 0;
                    }
                    //Done processing, so need to rename last worked on tab
                    if (TabName == "NewTab")
                    {
                        objExcel.Worksheets[TabName].Name = strPreviousField;
                    }
                }
                else
                {
                    objWorksheet.Workbook.Worksheets[TabName].Rows[intStartDataRow].Cells[0].Value = "No data...";
                    objWorksheet.Workbook.Worksheets[TabName].Columns[0].Width = intDefaultWidth;
                }
            }
            else
            {
                objWorksheet.Workbook.Worksheets[TabName].Rows[intStartDataRow].Cells[0].Value = "No data...";
                objWorksheet.Workbook.Worksheets[TabName].Columns[0].Width = intDefaultWidth;
            }
        }

        public override void BuildExportMultiFieldsReportCPM(ref String ReturnMessage, String RequestUser, ExcelType OutputExcelType)
        {
            ReturnMessage = "Error processing export file.";

            if (OutputExcelType == ExcelType.Excel2007)
            {
                objExcel.SetCurrentFormat(InfragisticsExcel.WorkbookFormat.Excel2007);
            }
            else
            {
                objExcel.SetCurrentFormat(InfragisticsExcel.WorkbookFormat.Excel97To2003);
            }
            foreach (DataReportObject objReport in ExportReportCollection)
            {
                if (objReport != null)
                {
                    BuildWorksheetMultiFieldsReportCPM(objReport.TabName, objReport.ReportName, RequestUser, objReport.ReportParameters, objReport.SessionDataTable);
                }
            }

            ReturnMessage = null;
        }

        /// <summary>
        /// Generate export Excel report for Multiple Fields Projection (CPM Level), specific code for this report.
        /// </summary>
        private void BuildWorksheetMultiFieldsReportCPM(String TabName, String ReportName, String UserName, String ReportParameters, DataTable SourceData)
        {
            Int32 intStartDataRow = 8;                  // Zero-based Index
            Int32 intCurrentColumn = 0;                 // Zero-based Index
            Int32 intDefaultWidth = 5000;               // Default column width
            Int32 intSpecialWidth = 9000;               // Special column width (first 3 columns)
            String strPreviousField = "";               // Will track the field just processed
            Int32 intProjectRow = 5;                    // Row were Project Title will go
            Int32 intCurrentRow = 7;                    // Current row in the worksheet
            Int32 intMovingColumn = 0;                  // Keeps track of where the column is at
            Int32 intColumnHeaderRow = 7;               // Row where column headers will go   
            Int32 intModelCount = 1;                    // Will track the count of models 
            String strPreviousModel = "All Models";     // Will track the model just processed

            InfragisticsExcel.Worksheet objWorksheet = objExcel.Worksheets.Add(TabName);

            if (SourceData != null)
            {
                if (SourceData.Rows.Count > 0)
                {
                    foreach (DataRow objRow in SourceData.Rows)
                    {
                        // Check that the field are the same from current row and previous row
                        #region
                        if (strPreviousField != objRow["Field"].ToString())
                        {
                            //Need to skip creating a new tab when it's initial report run
                            #region if (strPreviousField != "")
                            if (strPreviousField != "")
                            {
                                // Check the Model at for incrementing tab name as they may have same field names
                                if (strPreviousModel != "All Models")
                                {
                                    objExcel.Worksheets[TabName].Name = strPreviousField + "(" + intModelCount.ToString() + ")";
                                    if (strPreviousModel != objRow["ModelName"].ToString())
                                    {
                                        intModelCount++;
                                    }
                                }
                                else
                                    objExcel.Worksheets[TabName].Name = strPreviousField;
                                objExcel.Worksheets.Add(TabName);
                                strPreviousModel = objRow["ModelName"].ToString();
                            }
                            #endregion if (strPreviousField != "")

                            //Add report header information, common to all reports and pages
                            #region Report Title
                            objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[0].Value = "Report Title:";
                            objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[0].Cells[1].Value = ReportName;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[0].Value = "Report Parameters:";
                            objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[1].Cells[1].Value = ReportParameters;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[0].Value = "Generated:";
                            objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[2].Cells[1].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[0].Value = "Generated By:";
                            objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[0].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[3].Cells[1].Value = UserName;
                            #endregion Report Title

                            //Add the case name 
                            #region Case Name
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.TopBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.LeftBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].Value = "Field:";
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188));
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.Font.Height = 275;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 1].CellFormat.BottomBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.TopBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].Value = objRow["Field"];
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188));
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Font.Height = 275;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.BottomBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 2].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White), null, InfragisticsExcel.FillPatternStyle.Solid);
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 3].CellFormat.TopBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 3].CellFormat.RightBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            objWorksheet.Workbook.Worksheets[TabName].Rows[intProjectRow].Cells[intCurrentColumn + 3].CellFormat.BottomBorderStyle = Infragistics.Documents.Excel.CellBorderLineStyle.Thin;
                            #endregion Case Name

                            //Add column headers
                            #region Column Headers
                            foreach (DataColumn objColumn in SourceData.Columns)
                            {
                                if (!(objColumn.ColumnName == "Field" || objColumn.ColumnName == "SortID"))
                                {
                                    objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intDefaultWidth;
                                    if (objColumn.ColumnName != "Name" && objColumn.ColumnName != "CostCategory" && objColumn.ColumnName != "ModelName")
                                    {
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].Value = objColumn.ColumnName;
                                        if (objColumn.Ordinal == 2)
                                            objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(228, 223, 236)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                        else
                                        {
                                            objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(92, 102, 112)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                            objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Alignment = InfragisticsExcel.HorizontalCellAlignment.Center;
                                        }
                                    }
                                    else
                                    {
                                        if (objColumn.ColumnName == "CostCategory")
                                        {
                                            objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intSpecialWidth;
                                            objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].Value = "Cost";
                                        }
                                        else if (objColumn.ColumnName == "ModelName")
                                        {
                                            objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intSpecialWidth;
                                            objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].Value = "Model";
                                        }
                                        else
                                        {
                                            objWorksheet.Workbook.Worksheets[TabName].Columns[intCurrentColumn].Width = intSpecialWidth;
                                            objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].Value = "";
                                        }
                                        objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(0, 118, 188)), null, InfragisticsExcel.FillPatternStyle.Solid);
                                    }
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Font.Bold = InfragisticsExcel.ExcelDefaultableBoolean.True;
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intColumnHeaderRow].Cells[intCurrentColumn].CellFormat.Font.ColorInfo = new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.White);
                                    intCurrentColumn += 1;
                                }
                            }
                            intCurrentColumn = 0;
                            intCurrentRow = intStartDataRow;  //becomes 8
                        }
                            #endregion Column Headers
                        #endregion

                        //Second put data into rows
                        #region Data row creation
                        for (Int32 col = 0; col < SourceData.Columns.Count; col++)
                        {
                            //If the row contains a grouped Ziff Account then grey it out
                            if (objRow[0].ToString().IndexOf('_') == -1)
                            {
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.Fill = new InfragisticsExcel.CellFillPattern(new InfragisticsExcel.WorkbookColorInfo(System.Drawing.Color.FromArgb(217, 217, 217)), null, InfragisticsExcel.FillPatternStyle.Solid);
                            }

                            //Need to skip columns SortID(0) and Field(4)
                            if (col != 0 && col != 4)
                            {
                                objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].Value = objRow[col];

                                if (SourceData.Columns[col].DataType == typeof(System.Decimal) || SourceData.Columns[col].DataType == typeof(System.Double))
                                {
                                    objWorksheet.Workbook.Worksheets[TabName].Rows[intCurrentRow].Cells[intMovingColumn].CellFormat.FormatString = "#,##0.00";
                                }

                                intMovingColumn += 1;
                            }
                        }
                        #endregion Data row creation

                        intCurrentRow += 1;
                        intMovingColumn = intCurrentColumn;             // Reset to start column
                        strPreviousField = objRow["Field"].ToString();  // Previous field becomes just processed field
                    }
                    //Done processing, so need to rename last worked on tab
                    objExcel.Worksheets[TabName].Name = strPreviousField + "(" + intModelCount.ToString() + ")";
                }
                else
                {
                    objWorksheet.Workbook.Worksheets[TabName].Rows[intStartDataRow].Cells[0].Value = "No data...";
                    objWorksheet.Workbook.Worksheets[TabName].Columns[0].Width = intDefaultWidth;
                }
            }
            else
            {
                objWorksheet.Workbook.Worksheets[TabName].Rows[intStartDataRow].Cells[0].Value = "No data...";
                objWorksheet.Workbook.Worksheets[TabName].Columns[0].Width = intDefaultWidth;
            }
        }    
    } 

    public class DataReportObject
    {
        public DataTable SessionDataTable { get; set; }
        public String TabName { get; set; }
        public String ReportName { get; set; }
        public String ReportParameters { get; set; }
        public System.Drawing.Bitmap ChartImage { get; set; }
        
        public DataReportObject()
        {
            // Do nothing
        }

        public DataReportObject(DataTable InputDataTable, String InputTabName, String InputReportName, String InputReportParameters, ArrayList InputExcludeColumns, String InputDataSort, ArrayList InputColumnDisplayNames, Bitmap InputChartImage)
        {
            if (InputExcludeColumns == null)
            {
                InputExcludeColumns = new ArrayList();
            }
            if (InputColumnDisplayNames == null)
            {
                InputColumnDisplayNames = new ArrayList();
            }
            SessionDataTable = null;
            if (InputDataTable != null)
            {
                if (InputDataTable.Rows.Count > 0)
                {
                    SessionDataTable = InputDataTable.Select(null, InputDataSort).CopyToDataTable();

                    foreach (DataColumn colName in SessionDataTable.Columns)
                    {
                        if (colName.ColumnName.StartsWith("Id"))
                        {
                            InputExcludeColumns.Add(colName.ColumnName);
                        }
                    }
                    foreach (DataTableColumnDisplay colDisplay in InputColumnDisplayNames)
                    {
                        if (colDisplay.ColumnName != colDisplay.DisplayText)
                        {
                            SessionDataTable.Columns[colDisplay.ColumnName].ColumnName = colDisplay.DisplayText;
                        }
                    }
                    foreach (var colDrop in InputExcludeColumns)
                    {
                        SessionDataTable.Columns.Remove(SessionDataTable.Columns[colDrop.ToString()]);
                    }
                }
            }
            TabName = InputTabName;
            ReportName = InputReportName;
            ReportParameters = (InputReportParameters == null ? "No information available" : InputReportParameters);
            ChartImage = InputChartImage;
        }
    }

    public class DataTableColumnDisplay
    {
        public String ColumnName { get; set; }
        public String DisplayText { get; set; }

        public DataTableColumnDisplay(String InputColumnName, String InputDisplayText)
        {
            ColumnName = InputColumnName;
            DisplayText = InputDisplayText;
        }
    }

    #endregion

}
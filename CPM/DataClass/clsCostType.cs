﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsCostType : CD.DAC
    {
        private Int32 m_IdCostType;
        private String m_Code;
        private String m_Name;

        public Int32 IdCostType { get { return m_IdCostType; } set { m_IdCostType = value; } }
        public String Code { get { return m_Code; } set { m_Code = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }

        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblCostType " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblCostType WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsModels : CD.DAC
    {

        private Int32 m_IdModel;
        private String m_Name;
        private Int32 m_BaseYear;
        private Int32 m_Projection;
        private Int32 m_Status;
        private String m_Notes;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;
        private Int32 m_Level;
        private Object m_DataUpdated;
        private Object m_CalcModuleParameter;
        private Object m_CalcModuleAllocation;
        private Object m_CalcModuleTechnical;
        private Object m_CalcModuleEconomic;
        
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }
        public Int32 BaseYear { get { return m_BaseYear; } set { m_BaseYear = value; } }
        public Int32 Projection { get { return m_Projection; } set { m_Projection = value; } }
        public Int32 Status { get { return m_Status; } set { m_Status = value; } }
        public String Notes { get { return m_Notes; } set { m_Notes = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }
        public Int32 Level { get { return m_Level; } set { m_Level = value; } }
        public Object DataUpdated { get { return m_DataUpdated; } set { m_DataUpdated = value; } }
        public Object CalcModuleParameter { get { return m_CalcModuleParameter; } set { m_CalcModuleParameter = value; } }
        public Object CalcModuleAllocation { get { return m_CalcModuleAllocation; } set { m_CalcModuleAllocation = value; } }
        public Object CalcModuleTechnical { get { return m_CalcModuleTechnical; } set { m_CalcModuleTechnical = value; } }
        public Object CalcModuleEconomic { get { return m_CalcModuleEconomic; } set { m_CalcModuleEconomic = value; } }
        
        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "usrpInsertModel";
            listPrm = DAC.LoadParameters("@IdModel", IdModel, "@Name", Name, "@BaseYear", BaseYear, "@Projection", Projection, "@Status", Status, "@Notes", Notes, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification, "@Level", Level);
            ExecuteProc(sInsert, listPrm);
            m_IdModel = this.MaxID();
            InsertDefaultPermission();
            UpdateModelYears();
            return m_IdModel;
        }

        public int InsertModelBase(Int32 _IdModelBase, Int32 _MaxRelationLevels)
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "usrpInsertModelBase";
            listPrm = DAC.LoadParameters("@IdModel", IdModel, "@IdModelBase", _IdModelBase, "@Name", Name, "@BaseYear", BaseYear, "@Projection", Projection, "@Status", Status, "@Notes", Notes, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification, "@Level", Level);
            ExecuteProc(sInsert, listPrm);
            m_IdModel = this.MaxID();
            InsertDefaultPermission();
            UpdateModelYears();
            UpdateTimestamp(true, false, false, false, false);
            CheckRecalcModuleParameter(_MaxRelationLevels);
            CheckRecalcModuleAllocation();
            return m_IdModel;
        }

        public void InsertDefaultPermission()
        {
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "usrpInsertModelDefaultPermission";
            listPrm = DAC.LoadParameters("@IdModel", IdModel, "@UserCreation", UserCreation);
            ExecuteProc(sInsert, listPrm);
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "usrpUpdateModel";
            listPrm = DAC.LoadParameters("@IdModel", IdModel, "@Name", Name, "@BaseYear", BaseYear, "@Projection", Projection, "@Status", Status, "@Notes", Notes, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification, "@Level", Level);
            ExecuteProc(sUpdate, listPrm);
            UpdateModelYears();
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "usrpDeleteModel";
            listPrm = LoadParameters("@IdModel", m_IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public void UpdateModelYears()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "calcModelYears";
            listPrm = DAC.LoadParameters("@IdModel", IdModel);
            ExecuteProc(sUpdate, listPrm);
        }

        public void UpdateTimestamp(Boolean SetDataUpdated, Boolean SetCalcModuleParameter, Boolean SetCalcModuleAllocation, Boolean SetCalcModuleTechnical, Boolean SetCalcModuleEconomic)
        {
            String sUpdateTimestamp;
            List<SqlParameter> listPrm;
            sUpdateTimestamp = "calcUpdateModelDates";
            listPrm = DAC.LoadParameters("@IdModel", IdModel, "@DataUpdated", Convert.ToInt32(SetDataUpdated), "@ModuleParameter", Convert.ToInt32(SetCalcModuleParameter), "@ModuleAllocation", Convert.ToInt32(SetCalcModuleAllocation), "@ModuleTechnical", Convert.ToInt32(SetCalcModuleTechnical), "@ModuleEconomic", Convert.ToInt32(SetCalcModuleEconomic));
            ExecuteProc(sUpdateTimestamp, listPrm);
            loadObject();
        }

        public void CheckRecalcModuleParameter(Int32 _MaxRelationLevels)
        {
            String sCheckRecalc;
            List<SqlParameter> listPrm;
            sCheckRecalc = "calcModuleParameter";
            listPrm = DAC.LoadParameters("@IdModel", IdModel, "@MaxRelationLevels", _MaxRelationLevels);
            loadObject();
            if (this.DataUpdated != null && this.CalcModuleParameter != null)
            {
                if (Convert.ToDateTime(this.DataUpdated) >= Convert.ToDateTime(this.CalcModuleParameter))
                {
                    ExecuteProc(sCheckRecalc, listPrm);
                    this.UpdateTimestamp(false, true, false, false, false);
                }
            }
            loadObject();
        }

        public void PrecalcModuleAllocation()
        {
            String sPrecalc;
            List<SqlParameter> listPrm;
            sPrecalc = "calcModuleAllocationPreCalc";
            listPrm = DAC.LoadParameters("@IdModel", IdModel);
            ExecuteProc(sPrecalc, listPrm);
        }

        public void CheckRecalcModuleAllocation()
        {
            String sCheckRecalc;
            List<SqlParameter> listPrm;
            sCheckRecalc = "calcModuleAllocation";
            listPrm = DAC.LoadParameters("@IdModel", IdModel);
            loadObject();
            if (this.DataUpdated != null && this.CalcModuleAllocation != null)
            {
                if (Convert.ToDateTime(this.DataUpdated) >= Convert.ToDateTime(this.CalcModuleAllocation))
                {
                    ExecuteProc(sCheckRecalc, listPrm);
                    this.UpdateTimestamp(false, false, true, false, false);
                }
            }
            loadObject();
        }

        public void PrecalcModuleTechnical()
        {
            String sPrecalc;
            List<SqlParameter> listPrm;
            sPrecalc = "calcModuleTechnicalPreCalc";
            listPrm = DAC.LoadParameters("@IdModel", IdModel);
            ExecuteProc(sPrecalc, listPrm);
        }

        public void CheckRecalcModuleTechnical()
        {
            String sCheckRecalc;
            List<SqlParameter> listPrm;
            sCheckRecalc = "calcModuleTechnical";
            listPrm = DAC.LoadParameters("@IdModel", IdModel);
            loadObject();
            if (this.DataUpdated != null && this.CalcModuleTechnical != null)
            {
                if (Convert.ToDateTime(this.DataUpdated) >= Convert.ToDateTime(this.CalcModuleTechnical))
                {
                    /** Check that the process isn't already running **/
                    ExecuteProc(sCheckRecalc, listPrm);
                    this.UpdateTimestamp(false, false, false, true, false);
                }
            }
            loadObject();
        }

        public void PrecalcModuleEconomic()
        {
            String sPrecalc;
            List<SqlParameter> listPrm;
            sPrecalc = "calcModuleEconomicPreCalc";
            listPrm = DAC.LoadParameters("@IdModel", IdModel);
            ExecuteProc(sPrecalc, listPrm);
        }

        public void CheckRecalcModuleEconomic()
        {
            String sCheckRecalc;
            List<SqlParameter> listPrm;
            sCheckRecalc = "calcModuleEconomic";
            listPrm = DAC.LoadParameters("@IdModel", IdModel);
            loadObject();
            if (this.DataUpdated != null && this.CalcModuleEconomic != null)
            {
                if (Convert.ToDateTime(this.DataUpdated) >= Convert.ToDateTime(this.CalcModuleEconomic))
                {
                    ExecuteProc(sCheckRecalc, listPrm);
                    this.UpdateTimestamp(false, false, false,false, true);
                }
            }
            loadObject();
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdModel) FROM tblModels ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadComboBox(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT IdModel ID,Name FROM tblModels " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT IdModel ID,Name FROM tblModels WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadComboBoxCPM(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT IdModel ID,Name FROM tblModels " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT tblModels.IdModel ID,tblModels.Name FROM tblModels  INNER JOIN tblModelsCurrencies ON tblModels.IdModel = tblModelsCurrencies.IdModel WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadListWithGeneral(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT IdModel ID,Name FROM vListModelsWithGeneral " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT IdModel ID,Name FROM vListModelsWithGeneral WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListModels " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListModels WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataSet LoadPercentageConfiguration()
        { 
            String sList;
            List<SqlParameter> listPrm;
            sList = "parpPercentageConfiguration";
            listPrm = LoadParameters("@IdModel", IdModel);
            return ConsultProc(sList, listPrm);
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdModel=" + m_IdModel, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                m_IdModel = (Int32)dr["IdModel"];
                m_Name = (String)dr["Name"];
                m_BaseYear = (Int32)dr["BaseYear"];
                m_Projection = Convert.ToInt32(dr["IdProjection"]);
                m_Status = (Int32)dr["Status"];
                m_Notes = (String)dr["Notes"];
                m_Level = (Int32)dr["Level"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
                m_DataUpdated = (Object)dr["DateDataUpdated"];
                m_CalcModuleParameter = (Object)dr["DateCalcModuleParameter"];
                m_CalcModuleAllocation = (Object)dr["DateCalcModuleAllocation"];
                m_CalcModuleTechnical = (Object)dr["DateCalcModuleTechnical"];
                m_CalcModuleEconomic = (Object)dr["DateCalcModuleEconomic"];
            }
        }

        public DataSet LoadOpexProjection(Int32 _IdModel, Int32 _IdAggregationLevel, Int32 _IdField, Int32 _IdStructure, Int32 _From, Int32 _To, Int32 _IdTechnicalScenario, Int32 _IdEconomicScenario, Int32 _IdCurrency, Int32 _IdTerm, Int32 _IdTechnicalDriver, Int32 _IdCostType, Int32 _IdTypeOperation, Int32 _Factor)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "reppOpexProjection";
            listPrm = LoadParameters("@IdModel", _IdModel, "@IdAggregationLevel", _IdAggregationLevel, "@IdField", _IdField, "@IdStructure", _IdStructure, "@From", _From, "@To", _To, "@IdTechnicalScenario", _IdTechnicalScenario, "@IdEconomicScenario", _IdEconomicScenario, "@IdCurrency", _IdCurrency, "@IdTerm", _IdTerm, "@IdTechnicalDriver", _IdTechnicalDriver, "@IdCostType", _IdCostType, "@IdTypeOperation", _IdTypeOperation, "@Factor", _Factor);
            return ConsultProc(sConsult, listPrm);
        }

        public DataSet LoadOpexProjectionDetailed(Int32 _IdModel, Int32 _IdAggregationLevel, Int32 _IdField, Int32 _IdStructure, Int32 _From, Int32 _To, Int32 _IdTechnicalScenario, Int32 _IdEconomicScenario, Int32 _IdCurrency, Int32 _IdTerm, Int32 _IdTechnicalDriver, Int32 _IdCostType, Int32 _IdTypeOperation, Int32 _IdLanguage, Int32 _Factor)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "reppOpexProjectionDetailed";
            listPrm = LoadParameters("@IdModel", _IdModel, "@IdAggregationLevel", _IdAggregationLevel, "@IdField", _IdField, "@IdStructure", _IdStructure, "@From", _From, "@To", _To, "@IdTechnicalScenario", _IdTechnicalScenario, "@IdEconomicScenario", _IdEconomicScenario, "@IdCurrency", _IdCurrency, "@IdTerm", _IdTerm, "@IdTechnicalDriver", _IdTechnicalDriver, "@IdCostType", _IdCostType, "@IdTypeOperation", _IdTypeOperation, "@IdLanguage", _IdLanguage, "@Factor", _Factor);
            return ConsultProc(sConsult, listPrm);
        }

        public DataSet LoadOpexProjectionExport(Int32 _IdModel, Int32 _IdAggregationLevel, String _IdField, Int32 _IdStructure, Int32 _From, Int32 _To, Int32 _IdTechnicalScenario, Int32 _IdEconomicScenario, Int32 _IdCurrency, Int32 _IdTerm, Int32 _IdTechnicalDriver, Int32 _IdCostType, Int32 _IdTypeOperation, Int32 _Factor)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "reppOpexProjectionExport";
            listPrm = LoadParameters("@IdModel", _IdModel, "@IdAggregationLevel", _IdAggregationLevel, "@IdField", _IdField, "@IdStructure", _IdStructure, "@From", _From, "@To", _To, "@IdTechnicalScenario", _IdTechnicalScenario, "@IdEconomicScenario", _IdEconomicScenario, "@IdCurrency", _IdCurrency, "@IdTerm", _IdTerm, "@IdTechnicalDriver", _IdTechnicalDriver, "@IdCostType", _IdCostType, "@IdTypeOperation", _IdTypeOperation, "@Factor", _Factor);
            return ConsultProc(sConsult, listPrm);
        }

        public String GetDefaultCurrencySymbol(Int32 _IdModel)
        {
            String getDefaultCurrencySymbolReturn = "(???)";
            clsCurrencies objCurrencies = new clsCurrencies();
            DataTable dtCurrencies = objCurrencies.LoadModelsCurrencies(_IdModel);
            DataRow[] drCurrencies = dtCurrencies.Select("BaseCurrency=1");
            if (drCurrencies != null)
            {
                if (drCurrencies.Length > 0)
                {
                    getDefaultCurrencySymbolReturn = "(" + drCurrencies[0]["Code"].ToString() + ")";
                }
            }
            drCurrencies = null;
            dtCurrencies = null;
            objCurrencies = null;
            return getDefaultCurrencySymbolReturn;
        }

        public DataSet LoadBaseRatesByDrivers(Int32 _IdModel, Int32 _IdAggregationLevel, Int32 _IdField, Int32 _IdCurrency, Int32 _IdStage, Int32 _BaseCostType, Int32 _IdLanguage)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "parpBaseRatesByDriversTechnicalModule";
            listPrm = LoadParameters("@IdModel", _IdModel, "@IdAggregationLevel", _IdAggregationLevel, "@IdField", _IdField, "@IdCurrency", _IdCurrency, "@IdStage", _IdStage, "@BaseCostType", _BaseCostType, "@IdLanguage", _IdLanguage);
            return ConsultProc(sConsult, listPrm);
        }

        public DataSet LoadMultiFieldsOpexProjection(Int32 _IdModel, Int32 _IdAggregationLevel, String _IdField, Int32 _IdStructure, Int32 _From, Int32 _To, Int32 _IdTechnicalScenario, Int32 _IdEconomicScenario, Int32 _IdCurrency, Int32 _IdTerm, Int32 _IdTechnicalDriver, Int32 _IdCostType, Int32 _IdTypeOperation, Int32 _Factor, String _CostCategories)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "reppOpexProjectionMultiFields";
            listPrm = LoadParameters("@IdModel", _IdModel, "@IdAggregationLevel", _IdAggregationLevel, "@IdField", _IdField, "@IdStructure", _IdStructure, "@From", _From, "@To", _To, "@IdTechnicalScenario", _IdTechnicalScenario, "@IdEconomicScenario", _IdEconomicScenario, "@IdCurrency", _IdCurrency, "@IdTerm", _IdTerm, "@IdTechnicalDriver", _IdTechnicalDriver, "@IdCostType", _IdCostType, "@IdTypeOperation", _IdTypeOperation, "@Factor", _Factor, "@CostCategories", _CostCategories);
            return ConsultProc(sConsult, listPrm);
        }

        public DataSet LoadMultiFieldsOpexProjection(String _IdModel, Int32 _IdStructure, Int32 _From, Int32 _To, Int32 _IdTechnicalScenario, Int32 _IdEconomicScenario, Int32 _IdCurrency, Int32 _IdTerm, Int32 _IdCostType, Int32 _IdTypeOperation, Int32 _Factor, String _CostCategories)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "reppOpexProjectionMultiFieldsCPM";
            listPrm = LoadParameters("@IdModel", _IdModel, "@IdStructure", _IdStructure, "@From", _From, "@To", _To, "@IdTechnicalScenario", _IdTechnicalScenario, "@IdEconomicScenario", _IdEconomicScenario, "@IdCurrency", _IdCurrency, "@IdTerm", _IdTerm, "@IdCostType", _IdCostType, "@IdTypeOperation", _IdTypeOperation, "@Factor", _Factor, "@CostCategories", _CostCategories);
            return ConsultProc(sConsult, listPrm);
        }

        public DataSet LoadOperationalMarginsOpexProjection(Int32 _IdModel, String _IdField, Int32 _From, Int32 _To, Int32 _IdTechnicalScenario, Int32 _IdEconomicScenario, Int32 _IdCurrency, Int32 _IdTerm, Int32 _IdTypeOperation, Int32 _IdPriceScenario)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "reppOpexProjectionOperationalMargins";
            listPrm = LoadParameters("@IdModel", _IdModel, "@IdField", _IdField, "@From", _From, "@To", _To, "@IdTechnicalScenario", _IdTechnicalScenario, "@IdEconomicScenario", _IdEconomicScenario, "@IdCurrency", _IdCurrency, "@IdTerm", _IdTerm, "@IdTypeOperation", _IdTypeOperation, "@IdPriceScenario", _IdPriceScenario);
            return ConsultProc(sConsult, listPrm);
        }

        public DataSet LoadCostBenchmarking(Int32 _IdModel, String _IdField, String _IdPeerGroup, Int32 _IdCurrency)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "reppCostBenchmarking";
            listPrm = LoadParameters("@IdModel", _IdModel, "@IdField", _IdField, "@IdPeerGroup", _IdPeerGroup, "@IdCurrency", _IdCurrency);
            return ConsultProc(sConsult, listPrm);
        }

        public DataSet LoadCostVsDriversProjection(Int32 _IdModel, Int32 _IdAggregationLevel, Int32 _IdField, Int32 _From, Int32 _To, Int32 _IdTechnicalScenario, Int32 _IdEconomicScenario, Int32 _IdCurrency, Int32 _IdTerm, Int32 _IdTechnicalDriver, Int32 _IdEconomicDriver, Int32 _IdCostType, Int32 _Factor, String _CostCategories)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "reppCostVsDriversProjection";
            listPrm = LoadParameters("@IdModel", _IdModel, "@IdAggregationLevel", _IdAggregationLevel, "@IdField", _IdField, "@From", _From, "@To", _To, "@IdTechnicalScenario", _IdTechnicalScenario, "@IdEconomicScenario", _IdEconomicScenario, "@IdCurrency", _IdCurrency, "@IdTerm", _IdTerm, "@IdTechnicalDriver", _IdTechnicalDriver, "@IdCostType", _IdCostType, "@Factor", _Factor, "@CostCategories", _CostCategories, "@IdEconomicDriver", _IdEconomicDriver);
            return ConsultProc(sConsult, listPrm);
        }

        public DataSet LoadOpexProjectionChartOnly(Int32 _IdModel, Int32 _IdAggregationLevel, Int32 _IdField, Int32 _IdStructure, Int32 _From, Int32 _To, Int32 _IdTechnicalScenario, Int32 _IdEconomicScenario, Int32 _IdCurrency, Int32 _IdTerm, Int32 _IdTechnicalDriver, Int32 _IdCostType, Int32 _IdTypeOperation, Int32 _Factor)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "reppOpexProjectionChartOnly";
            listPrm = LoadParameters("@IdModel", _IdModel, "@IdAggregationLevel", _IdAggregationLevel, "@IdField", _IdField, "@IdStructure", _IdStructure, "@From", _From, "@To", _To, "@IdTechnicalScenario", _IdTechnicalScenario, "@IdEconomicScenario", _IdEconomicScenario, "@IdCurrency", _IdCurrency, "@IdTerm", _IdTerm, "@IdTechnicalDriver", _IdTechnicalDriver, "@IdCostType", _IdCostType, "@IdTypeOperation", _IdTypeOperation, "@Factor", _Factor);
            return ConsultProc(sConsult, listPrm);
        }

        public DataTable GetBaseYearAndProjection(string strFilter, string strOrder)
        {
            {
                if (strFilter == String.Empty)
                {
                    return CD.DAC.ConsultSQL("SELECT IdModel ID,Name FROM tblModels " + strOrder).Tables[0];
                }
                else
                {
                    return CD.DAC.ConsultSQL("SELECT min(BaseYear) AS BaseYear, max(Projection) AS Projection FROM tblModels  INNER JOIN tblModelsCurrencies ON tblModels.IdModel = tblModelsCurrencies.IdModel WHERE  " + strFilter + " " + strOrder).Tables[0];
                }
            }
        }
    }
}
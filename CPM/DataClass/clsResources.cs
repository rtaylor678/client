﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsResources : CD.DAC
    {

        private Int32 m_IdResources;
        private Int32 m_IdModel;
        private String m_Code;
        private String m_Name;
        private Object m_Parent;
        private Object m_TypeAllocation;
        private Object m_IdActivity;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;

        public Int32 IdResources { get { return m_IdResources; } set { m_IdResources = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public String Code { get { return m_Code; } set { m_Code = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }
        public Object Parent { get { return m_Parent; } set { m_Parent = value; } }
        public Object TypeAllocation { get { return m_TypeAllocation; } set { m_TypeAllocation = value; } }
        public Object IdActivity { get { return m_IdActivity; } set { m_IdActivity = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertResources";
            listPrm = DAC.LoadParameters("@IdResources", IdResources, "@IdModel", IdModel, "@Code", Code, "@Name", Name, "@Parent", Parent, "@TypeAllocation", TypeAllocation, "@IdActivity", IdActivity, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdResources = this.MaxID();
            return m_IdResources;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdateResources";
            listPrm = DAC.LoadParameters("@IdResources", IdResources, "@IdModel", IdModel, "@Code", Code, "@Name", Name, "@Parent", Parent, "@TypeAllocation", TypeAllocation, "@IdActivity", IdActivity, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteResources";
            listPrm = LoadParameters("@IdResources", IdResources);
            ExecuteProc(sDelete, listPrm);
        }

        public void DeleteAll()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteAllResources";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public DataTable LoadModelsResources(Int32 _IdModel, Int32 _Parent)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "parpListResources";
            listPrm = LoadParameters("@IdModel", _IdModel, "@Parent", _Parent);
            return ConsultProc(sConsult, listPrm).Tables[0];
        }

        public DataTable LoadFilterModelsResources(Int32 _IdModel, Int32 _Parent, String _Name)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "parpListFilterResources";
            listPrm = LoadParameters("@IdModel", _IdModel, "@Parent", _Parent, "@Name", _Name);
            return ConsultProc(sConsult, listPrm).Tables[0];
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdResources) FROM tblResources ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable GetComboBox(Int32 _IdModel, String NameResource)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "parpListResourcesLastNode";
            listPrm = LoadParameters("@IdModel", _IdModel, "@NameResource", NameResource);
            return ConsultProc(sConsult, listPrm).Tables[0];
        }

        public DataTable LoadExportList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListResources " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListResources WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListResources " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListResources WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdResources=" + m_IdResources, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdResources = (Int32)dr["IdResources"];
                m_IdModel = (Int32)dr["IdModel"];
                m_Code = (String)dr["Code"];
                m_Name = (String)dr["Name"];
                m_Parent = (Object)dr["Parent"];
                m_TypeAllocation = (Object)dr["TypeAllocation"];
                m_IdActivity = (Object)dr["IdActivity"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }
        }

    }
}
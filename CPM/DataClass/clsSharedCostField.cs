﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsSharedCostField : CD.DAC
    {

        private Int32 m_IdSharedCostField;
        private Int32 m_IdSharedCostAllocation;
        private Int32 m_IdField;

        public Int32 IdSharedCostField { get { return m_IdSharedCostField; } set { m_IdSharedCostField = value; } }
        public Int32 IdSharedCostAllocation { get { return m_IdSharedCostAllocation; } set { m_IdSharedCostAllocation = value; } }
        public Int32 IdField { get { return m_IdField; } set { m_IdField = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "allpInsertSharedCostField";
            listPrm = DAC.LoadParameters("@IdSharedCostField", IdSharedCostField, "@IdSharedCostAllocation", IdSharedCostAllocation, "@IdField", IdField);
            ExecuteProc(sInsert, listPrm);
            m_IdSharedCostField = this.MaxID();
            return m_IdSharedCostField;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "allpUpdateSharedCostField";
            listPrm = DAC.LoadParameters("@IdSharedCostField", IdSharedCostField, "@IdSharedCostAllocation", IdSharedCostAllocation, "@IdField", IdField);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeleteSharedCostField";
            listPrm = LoadParameters("@IdSharedCostField", IdSharedCostField);
            ExecuteProc(sDelete, listPrm);
        }

        public void DeleteAll()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeleteSharedCostFieldAll";
            listPrm = LoadParameters("@IdSharedCostAllocation", IdSharedCostAllocation);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdSharedCostField) FROM tblSharedCostField ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblSharedCostField " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblSharedCostField WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdSharedCostField=" + m_IdSharedCostField, " ORDER BY IdSharedCostField");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdSharedCostField = (Int32)dr["IdSharedCostField"];
                m_IdSharedCostAllocation = (Int32)dr["IdSharedCostAllocation"];
                m_IdField = (Int32)dr["IdField"];
            }
        }

    }
}
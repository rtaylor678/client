﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsProjects : CD.DAC
    {

        #region Declarations

        private Int32 m_IdProject;
        private Int32 m_IdModel;
        private String m_Code;
        private String m_Name;
        private Int32 m_IdField;
        private Int32 m_Operation;
        private Int32 m_Starts;
        private Object m_TypeAllocation;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;
        private Boolean m_CaseSelection;

        public Int32 IdProject { get { return m_IdProject; } set { m_IdProject = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public String Code { get { return m_Code; } set { m_Code = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }
        public Int32 IdField { get { return m_IdField; } set { m_IdField = value; } }
        public Int32 Starts { get { return m_Starts; } set { m_Starts = value; } }
        public Int32 Operation { get { return m_Operation; } set { m_Operation = value; } }
        public Object TypeAllocation { get { return m_TypeAllocation; } set { m_TypeAllocation = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }
        public Boolean CaseSelection { get { return m_CaseSelection; } set { m_CaseSelection = value; } }

        #endregion

        #region Methods

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertProjects";
            listPrm = DAC.LoadParameters("@IdProject", IdProject, "@IdModel", IdModel, "@Code", Code, "@Name", Name, "@IdField", IdField, "@Operation", Operation, "@Starts", Starts, "@TypeAllocation", TypeAllocation, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification, "@CaseSelection", CaseSelection);
            ExecuteProc(sInsert, listPrm);
            m_IdProject = this.MaxID();
            return m_IdProject;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdateProjects";
            listPrm = DAC.LoadParameters("@IdProject", IdProject, "@IdModel", IdModel, "@Code", Code, "@Name", Name, "@IdField", IdField, "@Operation", Operation, "@Starts", Starts, "@TypeAllocation", TypeAllocation, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification, "@CaseSelection", CaseSelection);
            ExecuteProc(sUpdate, listPrm);
        }

        public void DeleteAll()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteAllProjects";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteProjects";
            listPrm = LoadParameters("@IdProject", IdProject);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdProject) FROM tblProjects ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadComboBox(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT IdProject ID, Code, Name FROM tblProjects " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT IdProject ID, Code, Name FROM tblProjects WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadExportList(string strFilter, string strOrder)
        {
            return CD.DAC.ConsultSQL("SELECT Code, Name, Field, Starts, OperationName FROM vListProjects WHERE  " + strFilter + " " + strOrder).Tables[0];
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT *, Name +' | '+ OperationName AS VisualName FROM vListProjects " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT *, Name +' | '+ OperationName AS VisualName FROM vListProjects WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdProject =" + m_IdProject, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdProject = (Int32)dr["IdProject"];
                m_IdModel = (Int32)dr["IdModel"];
                m_Code = (String)dr["Code"];
                m_Name = (String)dr["Name"];
                m_IdField = (Int32)dr["IdField"];
                m_Operation = (Int32)dr["Operation"];
                m_Starts = (Int32)dr["Starts"];
                m_TypeAllocation = (Object)dr["TypeAllocation"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
                m_CaseSelection = (Boolean)dr["CaseSelection"];
            }
        }

        public bool IsCaseSelectionChecked(int _IdModel, int _IdField)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "parpIsCaseSelectionChecked";
            listPrm = LoadParameters("@IdModel", _IdModel, "@IdField", _IdField);
            DataTable dt = ConsultProc(sConsult, listPrm).Tables[0];

            if (dt.Rows.Count > 0)
                return true;
            else
                return false;
        }

        #endregion

    }
}
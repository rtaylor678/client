﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsTypePriceEconomic : CD.DAC
    {

        private Int32 m_IdTypePriceEconomic;
        private Int32 m_IdZiffAccount;
        private Int32 m_IdModel;
        private Int32 m_ProjectionCriteria;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;
        

        public Int32 IdTypePriceEconomic { get { return m_IdTypePriceEconomic; } set { m_IdTypePriceEconomic = value; } }
        public Int32 IdZiffAccount { get { return m_IdZiffAccount; } set { m_IdZiffAccount = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public Int32 ProjectionCriteria { get { return m_ProjectionCriteria; } set { m_ProjectionCriteria = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertTypePriceEconomic";
            listPrm = DAC.LoadParameters("@IdTypePriceEconomic", IdTypePriceEconomic, "@IdZiffAccount", IdZiffAccount, "@IdModel", IdModel, "@ProjectionCriteria", ProjectionCriteria, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdTypePriceEconomic = this.MaxID();
            return m_IdTypePriceEconomic;
        }

        public int InsertWeight(Int32 _IdTypePriceWeight, Int32 _IdTypePriceEconomic, Int32 _IdEconomicDriver, Double _Weight)
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "ecpInsertTypePriceWeight";
            listPrm = DAC.LoadParameters("@IdTypePriceWeight", _IdTypePriceWeight, "@IdTypePriceEconomic", _IdTypePriceEconomic, "@IdEconomicDriver", _IdEconomicDriver, "@Weight", _Weight);
            return ExecuteProc(sInsert, listPrm) ;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdateTypePriceEconomic";
            listPrm = DAC.LoadParameters("@IdTypePriceEconomic", IdTypePriceEconomic, "@IdZiffAccount", IdZiffAccount, "@IdModel", IdModel, "@ProjectionCriteria", ProjectionCriteria, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        public int UpdateWeight(Int32 _IdTypePriceWeight, Int32 _IdTypePriceEconomic, Int32 _IdEconomicDriver, Double _Weight)
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "ecpUpdateTypePriceWeight";
            listPrm = DAC.LoadParameters("@IdTypePriceWeight", _IdTypePriceWeight, "@IdTypePriceEconomic", _IdTypePriceEconomic, "@IdEconomicDriver", _IdEconomicDriver, "@Weight", _Weight);
            return ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteTypePriceEconomic";
            listPrm = LoadParameters("@IdTypePriceEconomic", IdTypePriceEconomic);
            ExecuteProc(sDelete, listPrm);
        }

        public void DeleteWeight(Int32 _IdTypePriceWeight)
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "ecpDeleteTypePriceWeight";
            listPrm = LoadParameters("@IdTypePriceWeight", _IdTypePriceWeight);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdTypePriceEconomic) FROM tblTypePriceEconomic ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadList()
        {
            String sList;
            List<SqlParameter> listPrm;
            sList = "parpListTypePriceEconomic";
            listPrm = LoadParameters("@IdModel", IdModel);
            return ConsultProc(sList, listPrm).Tables[0];
        }

        public DataTable LoadDriverWeight(Int32 _IdModel)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "ecpListDriverWeight";
            listPrm = LoadParameters("@IdModel", _IdModel, "@IdTypePriceEconomic", IdTypePriceEconomic);
            return ConsultProc(sConsult, listPrm).Tables[0];
        }
                       
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListTypePriceEconomic " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListTypePriceEconomic WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdTypePriceEconomic=" + m_IdTypePriceEconomic, " ORDER BY IdTypePriceEconomic");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdTypePriceEconomic = (Int32)dr["IdTypePriceEconomic"];
                m_IdZiffAccount = (Int32)dr["IdZiffAccount"];
                m_IdModel = (Int32)dr["IdModel"];
                m_ProjectionCriteria = (Int32)dr["ProjectionCriteria"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }
        }

    }
}
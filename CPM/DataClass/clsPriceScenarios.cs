﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsPriceScenarios : CD.DAC
    {
        private Int32 m_IdPriceScenario;
        private Int32 m_IdModel;
        private String m_SenarioName;

        public Int32 IdPriceScenario { get { return m_IdPriceScenario; } set { m_IdPriceScenario = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public String ScenarioName { get { return m_SenarioName; } set { m_SenarioName = value; } }

        /// <summary>
        /// Inserts record into tblPriceScenarios
        /// </summary>
        /// <returns>the new IdPriceScenario</returns>
        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertPriceScenarios";
            listPrm = DAC.LoadParameters("@IdPriceScenario", IdPriceScenario, "@IdModel", IdModel, "@ScenarioName", ScenarioName);
            ExecuteProc(sInsert, listPrm);
            IdPriceScenario = this.MaxID();
            return IdPriceScenario;
        }

        /// <summary>
        /// Updates currency record in tblPriceScenarios
        /// </summary>
        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdatePriceScenarios";
            listPrm = DAC.LoadParameters("@IdPriceScenario", IdPriceScenario, "@IdModel", IdModel, "@ScenarioName", ScenarioName);
            ExecuteProc(sUpdate, listPrm);
        }

        /// <summary>
        /// Deletes currency record form tblPriceScenarios
        /// </summary>
        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeletePriceScenarios";
            listPrm = LoadParameters("@IdPriceScenario", IdPriceScenario);
            ExecuteProc(sDelete, listPrm);
        }

        /// <summary>
        /// Gets the last IdModelPrice in tblPriceScenarios
        /// </summary>
        /// <returns>interger</returns>
        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdPriceScenario) FROM tblPriceScenarios ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblPriceScenarios " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblPriceScenarios WHERE " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdPriceScenario=" + m_IdPriceScenario, " ORDER BY ScenarioName");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdPriceScenario = (Int32)dr["IdPriceScenario"];
                m_SenarioName = (String)dr["ScenarioName"];
            }
        }

        public DataTable LoadComboBox(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT IdPriceScenario ID, ScenarioName Name FROM tblPriceScenarios " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT IdPriceScenario ID, ScenarioName Name FROM tblPriceScenarios WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }
        

    }
}
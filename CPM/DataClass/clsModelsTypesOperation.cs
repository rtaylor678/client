﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsModelsTypesOperation : CD.DAC
    {

        private Int32 m_IdModelTypeOperation; 
        private Int32 m_IdTypeOperation;
        private Int32 m_IdModel;

        public Int32 IdModelTypeOperation { get { return m_IdModelTypeOperation; } set { m_IdModelTypeOperation = value; } }
        public Int32 IdTypeOperation { get { return m_IdTypeOperation; } set { m_IdTypeOperation = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertModelsTypesOperation";
            listPrm = DAC.LoadParameters("@IdModelTypeOperation", IdModelTypeOperation, "@IdModel", IdModel, "@IdTypeOperation", IdTypeOperation);
            ExecuteProc(sInsert, listPrm);
            m_IdModelTypeOperation = this.MaxID();
            return m_IdModelTypeOperation;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdateModelsTypesOperation";
            listPrm = DAC.LoadParameters("@IdModelTypeOperation", IdModelTypeOperation, "@IdModel", IdModel, "@IdTypeOperation", IdTypeOperation);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteModelsTypesOperation";
            listPrm = LoadParameters("@IdModel", m_IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdModelTypeOperation) FROM tblModelsTypesOperation ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblModelsTypesOperation " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblModelsTypesOperation WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdModelTypeOperation=" + m_IdModel, " ORDER BY IdModel");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdModelTypeOperation = (Int32)dr["IdModelTypeOperation"];
                m_IdTypeOperation = (Int32)dr["IdTypeOperation"];
                m_IdModel = (Int32)dr["IdModel"];                
            }
        }

    }
}
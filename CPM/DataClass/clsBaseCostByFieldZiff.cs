﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsBaseCostByFieldZiff : CD.DAC
    {

        #region Declarations
        #endregion

        #region Methods

        public DataTable LoadRows(Int32 IdModel)
        {
            return CD.DAC.ConsultSQL("SELECT IdRootZiffAccount "
                                        + ", RootZiffAccount "
                                        + ", IdZiffAccount "
                                        + ", ZiffAccount "
                                    + "FROM [tblCalcBaseCostByField] "
                                    + "WHERE IdModel = " + IdModel + " AND IdRootZiffAccount IS NOT NULL "
                                    + "GROUP BY IdRootZiffAccount "
                                        + ", RootZiffAccount "
                                        + ", IdZiffAccount "
                                        + ", ZiffAccount "
                                    + "ORDER BY RootZiffAccount "
                                        + ", ZiffAccount").Tables[0];
        }

        public DataTable LoadReport(Int32 IdModel)
        {
            return CD.DAC.ConsultSQL("SELECT IdRootZiffAccount "
                                        + ", RootZiffAccount "
                                        + ", IdZiffAccount "
                                        + ", ZiffAccount "
                                        + ", IdField "
                                        + ", Field "
                                        + ", SUM(Amount) AS Amount "
                                    + "FROM [tblCalcBaseCostByField] "
                                    + "WHERE IdModel = " + IdModel + " AND IdRootZiffAccount IS NOT NULL "
                                    + "GROUP BY IdRootZiffAccount "
                                        + ", RootZiffAccount "
                                        + ", IdZiffAccount "
                                        + ", ZiffAccount "
                                        + ", IdField "
                                        + ", Field "
                                    + "ORDER BY RootZiffAccount "
                                        + ", ZiffAccount").Tables[0];
        }

        public DataTable LoadVolumeRows(Int32 IdModel)
        {
            return CD.DAC.ConsultSQL("SELECT dbo.tblAllocationListDrivers.IdAllocationListDriver "
                                        + ", dbo.tblAllocationListDrivers.Name "
                                    + "FROM dbo.tblAllocationDriversByField INNER JOIN "
                                        + " dbo.tblAllocationListDrivers ON dbo.tblAllocationDriversByField.IdAllocationListDriver = dbo.tblAllocationListDrivers.IdAllocationListDriver INNER JOIN "
                                        + " dbo.tblFields ON dbo.tblAllocationDriversByField.IdField = dbo.tblFields.IdField "
                                    + "WHERE dbo.tblAllocationListDrivers.IdModel =  " + IdModel + " "
                                    + "AND	 dbo.tblAllocationListDrivers.UnitCostDenominator = 1 "
                                    + "GROUP BY dbo.tblAllocationListDrivers.IdAllocationListDriver "
                                        + ", dbo.tblAllocationListDrivers.Name ").Tables[0];
        }

        public DataTable LoadVolumeReport(Int32 IdModel)
        {
            return CD.DAC.ConsultSQL("SELECT dbo.tblAllocationDriversByField.IdField "
                                        + ", dbo.tblFields.Name AS FieldName "
                                        + ", dbo.tblAllocationListDrivers.IdAllocationListDriver "
                                        + ", dbo.tblAllocationListDrivers.Name "
                                        + ", SUM(dbo.tblAllocationDriversByField.Amount) AS Amount "
                                    + "FROM dbo.tblAllocationDriversByField INNER JOIN "
                                        + " dbo.tblAllocationListDrivers ON dbo.tblAllocationDriversByField.IdAllocationListDriver = dbo.tblAllocationListDrivers.IdAllocationListDriver INNER JOIN "
                                        + " dbo.tblFields ON dbo.tblAllocationDriversByField.IdField = dbo.tblFields.IdField "
                                    + "WHERE dbo.tblAllocationListDrivers.IdModel =  " + IdModel + " "
                                    + "AND	 dbo.tblAllocationListDrivers.UnitCostDenominator = 1 "
                                    + "GROUP BY dbo.tblAllocationListDrivers.IdAllocationListDriver "
                                        + ", dbo.tblAllocationListDrivers.Name "
                                        + ", dbo.tblAllocationDriversByField.IdField "
                                        + ", dbo.tblFields.Name "
                                    + "ORDER BY FieldName").Tables[0];
        }

        #endregion

    }
}
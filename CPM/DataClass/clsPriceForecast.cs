﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsPriceForecast : CD.DAC
    {

        public DataTable LoadRows(Int32 IdModel)
        {
            return CD.DAC.ConsultSQL("SELECT IdZiffAccount "
                                        + ", CodeZiff "
                                        + ", ZiffAccount "
                                        + ", DisplayCode "
                                        + ", RootZiffAccount "
                                    + "FROM [tblCalcProjectionEconomic] "
                                    + "WHERE IdModel = " + IdModel + " "
                                    + "GROUP BY IdZiffAccount "
                                        + ", CodeZiff "
                                        + ", ZiffAccount "
                                        + ", DisplayCode "
                                        + ", RootZiffAccount "
                                        + ", SortOrder "
                                    + "ORDER BY SortOrder").Tables[0];
        }

        public DataTable LoadReport(Int32 IdModel, Int32 IdStage)
        {
            String strForecastColumn = "";
            switch (IdStage)
            {
                case 1:
                    strForecastColumn = "EFNormal";
                    break;
                case 2:
                    strForecastColumn = "EFOptimistic";
                    break;
                case 3:
                    strForecastColumn = "EFPessimistic";
                    break;
            }

            return CD.DAC.ConsultSQL("SELECT IdZiffAccount "
                                        + ", CodeZiff "
                                        + ", ZiffAccount "
                                        + ", DisplayCode "
                                        + ", RootZiffAccount "
                                        + ", Year "
                                        + ", SUM(" + strForecastColumn + ") AS Forecast "
                                    + "FROM [tblCalcProjectionEconomic] "
                                    + "WHERE IdModel = " + IdModel + " "
                                    + "GROUP BY IdZiffAccount "
                                        + ", CodeZiff "
                                        + ", ZiffAccount "
                                        + ", DisplayCode "
                                        + ", RootZiffAccount "
                                        + ", Year "
                                        + ", SortOrder "
                                    + "ORDER BY SortOrder, Year").Tables[0];
        }

        //private Int32 m_IdPriceForecast;
        //private Int32 m_IdZiffAccount;
        //private Int32 m_IdModel;
        //private Int32 m_Year;
        //private Int32 m_IdStage;
        //private Double m_ValueStage;
        //private Int32 m_UserCreation;
        //private DateTime m_DateCreation;
        //private Object m_UserModification;
        //private Object m_DateModification;

        //public Int32 IdPriceForecast { get { return m_IdPriceForecast; } set { m_IdPriceForecast = value; } }
        //public Int32 IdZiffAccount { get { return m_IdZiffAccount; } set { m_IdZiffAccount = value; } }
        //public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        //public Int32 Year { get { return m_Year; } set { m_Year = value; } }
        //public Int32 IdStage { get { return m_IdStage; } set { m_IdStage = value; } }
        //public Double ValueStage { get { return m_ValueStage; } set { m_ValueStage = value; } }
        //public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        //public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        //public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        //public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        //public int Insert()
        //{
        //    CD.DAC DAC = new CD.DAC();
        //    String sInsert;
        //    List<SqlParameter> listPrm;
        //    sInsert = "parpInsertPriceForecast";
        //    listPrm = DAC.LoadParameters("@IdPriceForecast", IdPriceForecast,"@IdZiffAccount", IdZiffAccount , "@IdModel", IdModel, "@Year", Year, "@IdStage", IdStage ,"@ValueStage", ValueStage , "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
        //    ExecuteProc(sInsert, listPrm);
        //    m_IdPriceForecast = this.MaxID();
        //    return m_IdPriceForecast;
        //}

        //public void Update()
        //{
        //    String sUpdate;
        //    List<SqlParameter> listPrm;
        //    sUpdate = "parpUpdatePriceForecast";
        //    listPrm = DAC.LoadParameters("@IdPriceForecast", IdPriceForecast, "@IdZiffAccount", IdZiffAccount, "@IdModel", IdModel, "@Year", Year, "@IdStage", IdStage, "@ValueStage", ValueStage, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
        //    ExecuteProc(sUpdate, listPrm);
        //}

        //public void Delete()
        //{
        //    String sDelete;
        //    List<SqlParameter> listPrm;
        //    sDelete = "parpDeleteAllPriceForecast";
        //    listPrm = LoadParameters("@IdPriceForecast", IdPriceForecast);
        //    ExecuteProc(sDelete, listPrm);
        //}

        //public void DeleteAll()
        //{
        //    String sDelete;
        //    List<SqlParameter> listPrm;
        //    sDelete = "parpDeleteAllPriceForecast";
        //    listPrm = LoadParameters("@IdModel", IdModel);
        //    ExecuteProc(sDelete, listPrm);
        //}

        //public Int32 MaxID()
        //{
        //    Int32 intValue = 0;
        //    DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdPriceForecast) FROM tblPriceForecast ").Tables[0];

        //    if (dt.Rows.Count > 0)
        //    {
        //        intValue = Convert.ToInt32(dt.Rows[0][0]);
        //    }

        //    return intValue;
        //}


        //public DataSet LoadList()
        //{
        //    String sList;
        //    List<SqlParameter> listPrm;
        //    //sList = "parpListPriceForecast";
        //    sList = "calcListProjectionEconomic";
        //    listPrm = LoadParameters("@IdModel", IdModel,  "@IdStage", IdStage);
        //    return ConsultProc(sList, listPrm);
        //}
       
        //public DataTable LoadList(string strFilter, string strOrder)
        //{
        //    if (strFilter == String.Empty) 
        //    {
        //        return CD.DAC.ConsultSQL("SELECT * FROM vListPriceForecast " + strOrder).Tables[0];
        //    }
        //    else
        //    {
        //        return CD.DAC.ConsultSQL("SELECT * FROM vListPriceForecast WHERE  " + strFilter + " " + strOrder).Tables[0];
        //    }
        //}

        //public void loadObject()
        //{
        //    DataTable dt = LoadList("IdZiffAccount=" + m_IdZiffAccount + " And IdModel=" + m_IdModel + " And IdStage=" + m_IdStage + " And [Year]=" + m_Year, " ORDER BY IdPriceForecast");
        //    if (dt.Rows.Count > 0)
        //    {
        //        DataRow dr = dt.Rows[0];
        //        m_IdPriceForecast = (Int32)dr["IdPriceForecast"];
        //        m_IdModel = (Int32)dr["IdModel"];
        //        m_IdZiffAccount = (Int32)dr["IdZiffAccount"];
        //        m_Year = (Int32)dr["Year"];
        //        m_IdStage = (Int32)dr["IdStage"];
        //        m_ValueStage = Convert.ToDouble(dr["ValueStage"]);
        //        m_UserCreation = (Int32)dr["UserCreation"];
        //        m_DateCreation = (DateTime)dr["DateCreation"];
        //        m_UserModification = (Object)dr["UserModification"];
        //        m_DateModification = (Object)dr["DateModification"];
        //    }
        //}

    }
}
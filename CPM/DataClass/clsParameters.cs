﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using CD;

namespace DataClass
{
    public class clsParameters : CD.DAC
    {

        private Int32 m_IdParameters;
        private String m_Type;
        private Int32 m_Code;
        private Int32 m_IdLanguage;
        private String m_Name;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;

        public Int32 IdParameters { get { return m_IdParameters; } set { m_IdParameters = value; } }
        public String Type { get { return m_Type; } set { m_Type = value; } }
        public Int32 Code { get { return m_Code; } set { m_Code = value; } }
        public Int32 IdLanguage { get { return m_IdLanguage; } set { m_IdLanguage = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "usrpInsertParameters";
            listPrm = DAC.LoadParameters("@IdParameters", IdParameters, "@Type", Type, "@Code", Code, "@Name", Name, "@IdLanguage", IdLanguage, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdParameters = this.MaxID();
            return m_IdParameters;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "usrpUpdateParameters";
            listPrm = DAC.LoadParameters("@IdParameters", IdParameters, "@Type", Type, "@Code", Code, "@Name", Name, "@IdLanguage", IdLanguage, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "usrpDeleteParameters";
            listPrm = LoadParameters("@IdParameters", m_IdParameters);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdParameters) FROM tblParameters ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblParameters " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblParameters WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdParameters =" + m_IdParameters, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                m_IdParameters = (Int32)dr["IdParameters"];
                m_Type = (String)dr["Type"];
                m_Code = (Int32)dr["Code"];
                m_Name = (String)dr["Name"];
                m_IdLanguage = (Int32)dr["IdLanguage"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }

        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsPriceComposition : CD.DAC
    {

        private Int32 m_IdPriceComposition;
        private Int32 m_IdModel;
        private Int32 m_IdZiffAccount;
        private Double m_Tradable;
        private Double m_NonTradable;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;

        public Int32 IdPriceComposition { get { return m_IdPriceComposition; } set { m_IdPriceComposition = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public Int32 IdZiffAccount { get { return m_IdZiffAccount; } set { m_IdZiffAccount = value; } }
        public Double Tradable { get { return m_Tradable; } set { m_Tradable = value; } }
        public Double NonTradable { get { return m_NonTradable; } set { m_NonTradable = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertPriceComposition";
            listPrm = DAC.LoadParameters("@IdPriceComposition", IdPriceComposition, "@IdZiffAccount", IdZiffAccount, "@IdModel", IdModel, "@Tradable", Tradable, "@NonTradable", NonTradable, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdPriceComposition = this.MaxID();
            return m_IdPriceComposition;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdatePriceComposition";
            listPrm = DAC.LoadParameters("@IdPriceComposition", IdPriceComposition, "@IdZiffAccount", IdZiffAccount, "@IdModel", IdModel, "@Tradable", Tradable, "@NonTradable", NonTradable, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeletePriceComposition";
            listPrm = LoadParameters("@IdPriceComposition", IdPriceComposition);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdPriceComposition) FROM tblPriceComposition ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadList()
        {
            String sList;
            List<SqlParameter> listPrm;
            sList = "parpListPriceComposition";
            listPrm = LoadParameters("@IdModel", IdModel);
            return ConsultProc(sList, listPrm).Tables[0];
        }
                       
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListPriceComposition " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListPriceComposition WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdPriceComposition=" + m_IdPriceComposition, " ORDER BY IdPriceComposition");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdPriceComposition = (Int32)dr["IdPriceComposition"];
                m_IdZiffAccount = (Int32)dr["IdZiffAccount"];
                m_IdModel = (Int32)dr["IdModel"];
                m_Tradable = Convert.ToDouble(dr["Tradable"]);
                m_NonTradable = Convert.ToDouble(dr["NonTradable"]);
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }
        }

    }
}
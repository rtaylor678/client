﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using CD;

namespace DataClass
{
    public class clsProfiles : CD.DAC
    {

        private Int32 m_IdProfile;
        private String m_Name;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;
        private Int32 m_Type;

        public Int32 IdProfile { get { return m_IdProfile; } set { m_IdProfile = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }
        public Int32 Type { get { return m_Type; } set { m_Type = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "usrpInsertProfile";
            listPrm = DAC.LoadParameters("@IdProfile", IdProfile, "@Name", Name, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification, "@Type", Type);
            ExecuteProc(sInsert, listPrm);
            m_IdProfile = this.MaxID();
            return m_IdProfile;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "usrpUpdateProfile";
            listPrm = DAC.LoadParameters("@IdProfile", IdProfile, "@Name", Name, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification, "@Type", Type);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "usrpDeleteProfile";
            listPrm = LoadParameters("@IdProfile", m_IdProfile);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdProfile) FROM tblProfiles ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblProfiles " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblProfiles WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadListRoles(string strUserFilter)
        {
            return CD.DAC.ConsultSQL("SELECT * FROM tblProfilesRoles INNER JOIN tblUsers ON tblProfilesRoles.IdProfile=tblUsers.IdProfile WHERE tblProfilesRoles.IdModel=0 AND tblProfilesRoles.IdRole=2 AND tblUsers.IdUser=" + strUserFilter).Tables[0];
        }

        public DataTable LoadComboBox(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT IdProfile ID,Name FROM tblProfiles " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT IdProfile ID,Name FROM tblProfiles WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdProfile =" + m_IdProfile, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                m_IdProfile = (Int32)dr["IdProfile"];
                m_Name = (String)dr["Name"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
                m_Type = (Int32)dr["Type"];
            }

        }

    }
}
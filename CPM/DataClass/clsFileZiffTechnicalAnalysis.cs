﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsFileZiffTechnicalAnalysis : CD.DAC
    {

        private Int32 m_IdFileZiffTechnicalAnalysis;
        private Int32 m_IdZiffTechnicalAnalysis;
        private byte[] m_AttachFile;
        private String m_FileName;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;        

        public Int32 IdFileZiffTechnicalAnalysis { get { return m_IdFileZiffTechnicalAnalysis; } set { m_IdZiffTechnicalAnalysis = value; } }
        public Int32 IdZiffTechnicalAnalysis { get { return m_IdZiffTechnicalAnalysis; } set { m_IdZiffTechnicalAnalysis = value; } }
        public byte[] AttachFile { get { return m_AttachFile; } set { m_AttachFile = value; } }
        public String FileName { get { return m_FileName; } set { m_FileName = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertFileZiffTechnicalAnalysis";
            listPrm = DAC.LoadParameters("@IdFileZiffTechnicalAnalysis", IdFileZiffTechnicalAnalysis, "@IdZiffTechnicalAnalysis", IdZiffTechnicalAnalysis, "@AttachFile", AttachFile, "@FileName", FileName, "@UserCreation", UserCreation, "@DateCreation", DateCreation);
            ExecuteProc(sInsert, listPrm);
            m_IdFileZiffTechnicalAnalysis = this.MaxID();
            return m_IdFileZiffTechnicalAnalysis;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdateFileZiffTechnicalAnalysis";
            listPrm = DAC.LoadParameters("@IdFileZiffTechnicalAnalysis", IdFileZiffTechnicalAnalysis, "@IdZiffTechnicalAnalysis", IdZiffTechnicalAnalysis, "@AttachFile", AttachFile, "@FileName", FileName, "@UserCreation", UserCreation, "@DateCreation", DateCreation);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteFileZiffTechnicalAnalysis";
            listPrm = LoadParameters("@IdFileZiffTechnicalAnalysis", IdFileZiffTechnicalAnalysis);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdFileZiffTechnicalAnalysis) FROM tblFileZiffTechnicalAnalysis ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }
            return intValue;
        }

        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListFileZiffTechnicalAnalysis " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListFileZiffTechnicalAnalysis  WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdFileZiffTechnicalAnalysis=" + m_IdFileZiffTechnicalAnalysis, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                m_IdFileZiffTechnicalAnalysis = (Int32)dr["IdFileZiffTechnicalAnalysis"];
                m_IdZiffTechnicalAnalysis = (Int32)dr["IdZiffTechnicalAnalysis"];
                m_AttachFile = (byte[])dr["AttachFile"];
                m_FileName = (String)dr["FileName"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];                
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsOperationsCostProjection : CD.DAC
    {
        #region Declarations
        #endregion

        #region Methods

        public DataTable LoadVolumeRows(Int32 IdModel)
        {
            return CD.DAC.ConsultSQL("SELECT IdTechnicalDriver "
                                        + ", Name "
                                    + "FROM	tblTechnicalDrivers "
                                    + "WHERE IdModel = " + IdModel + " "
                                    + "AND	 UnitCostDenominator = 1 ").Tables[0];
        }

        public DataTable LoadVolumeReport(Int32 IdModel)
        {
            return CD.DAC.ConsultSQL("SELECT dbo.tblTechnicalDriverData.IdField "
                                        + ", dbo.tblFields.Name AS FieldName "
                                        + ", dbo.tblTechnicalDriverData.IdTechnicalDriver "
                                        + ", dbo.tblTechnicalDrivers.Name "
                                        + ", dbo.tblTechnicalDriverData.Year "
                                        + ", SUM(dbo.tblTechnicalDriverData.Normal) AS Normal "
                                        + ", SUM(dbo.tblTechnicalDriverData.Pessimistic) AS Pessimistic "
                                        + ", SUM(dbo.tblTechnicalDriverData.Optimistic) AS Optimistic "
                                    + "FROM	dbo.tblTechnicalDriverData INNER JOIN "
                                        + " dbo.tblTechnicalDrivers ON dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND "
                                        + "	dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN "
                                        + "	dbo.tblFields ON dbo.tblFields.IdField = dbo.tblTechnicalDriverData.IdField "
                                    + "WHERE dbo.tblTechnicalDriverData.IdModel = " + IdModel + " "
                                    + "AND	 dbo.tblTechnicalDrivers.UnitCostDenominator = 1 "
                                    + "GROUP BY dbo.tblTechnicalDriverData.IdField "
                                        + ", dbo.tblFields.Name "
                                        + ", dbo.tblTechnicalDriverData.IdTechnicalDriver "
                                        + ", dbo.tblTechnicalDrivers.Name "
                                        + ", dbo.tblTechnicalDriverData.Year "
                                    + "ORDER BY dbo.tblTechnicalDriverData.IdField "
                                    + ", dbo.tblTechnicalDriverData.Year ").Tables[0];
        }

        public DataTable LoadVolumeReportGroup(Int32 IdModel)
        {
            return CD.DAC.ConsultSQL("SELECT dbo.tblTechnicalDriverData.Year "
                                        + ", SUM(dbo.tblTechnicalDriverData.Normal) AS Normal "
                                        + ", SUM(dbo.tblTechnicalDriverData.Pessimistic) AS Pessimistic "
                                        + ", SUM(dbo.tblTechnicalDriverData.Optimistic) AS Optimistic "
                                    + "FROM	dbo.tblTechnicalDriverData INNER JOIN "
                                        + " dbo.tblTechnicalDrivers ON dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND "
                                        + " dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN "
                                        + "	dbo.tblFields ON dbo.tblFields.IdField = dbo.tblTechnicalDriverData.IdField "
                                    + "WHERE dbo.tblTechnicalDriverData.IdModel = " + IdModel + " "
                                    + "AND 	 dbo.tblTechnicalDrivers.UnitCostDenominator = 1 "
                                    + "GROUP BY dbo.tblTechnicalDriverData.IdTechnicalDriver "
                                        + ", dbo.tblTechnicalDrivers.Name "
                                        + ", dbo.tblTechnicalDriverData.Year "
                                    + "ORDER BY dbo.tblTechnicalDriverData.Year ").Tables[0];
        }

        #endregion

    }
}

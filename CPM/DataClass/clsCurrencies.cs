﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsCurrencies : CD.DAC
    {
        private Int32 m_IdCurrency;
        private String m_Code;
        private String m_Sign;
        private String m_Name;

        public Int32 IdCurrency { get { return m_IdCurrency; } set { m_IdCurrency = value; } }
        public String Code { get { return m_Code; } set { m_Code = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }
        public String Sign { get { return m_Sign; } set { m_Sign = value; } }

        /// <summary>
        /// Inserts new currency record into tblCurrencies
        /// </summary>
        /// <returns>the new IdCurrency</returns>
        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertCurrencies";
            listPrm = DAC.LoadParameters("@IdCurrency", IdCurrency, "@Code", Code, "@Name", Name, "@Sign", Sign);
            ExecuteProc(sInsert, listPrm);
            m_IdCurrency = this.MaxID();
            return m_IdCurrency;
        }

        /// <summary>
        /// Updates currency record in tblCurrencies
        /// </summary>
        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdateCurrencies";
            listPrm = DAC.LoadParameters("@IdCurrency", IdCurrency, "@Code", Code, "@Name", Name, "@Sign", Sign);
            ExecuteProc(sUpdate, listPrm);
        }

        /// <summary>
        /// Deletes currency record form tblCurrencies
        /// </summary>
        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteCurrencies";
            listPrm = LoadParameters("@IdCurrency", IdCurrency);
            ExecuteProc(sDelete, listPrm);
        }

        /// <summary>
        /// Gets the last IdCurrency in tblCurrencies
        /// </summary>
        /// <returns>interger</returns>
        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdCurrency) FROM tblCurrencies ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        /// <summary>
        /// List all currencies in a specific Model
        /// </summary>
        /// <param name="_IdModel">Current model to display</param>
        /// <returns>DataTable with existing values in Model</returns>
        public DataTable LoadModelsCurrencies(Int32 _IdModel)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "parpListModelsCurrencies";
            listPrm = LoadParameters("@IdModel", _IdModel);
            return ConsultProc(sConsult, listPrm).Tables[0];
        }

        public DataTable LoadExportList(string strFilter, string strOrder)
        {
            return CD.DAC.ConsultSQL("SELECT Code,Name, Sign FROM tblCurrencies WHERE  " + strFilter + " " + strOrder).Tables[0];
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListCurrencies " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListCurrencies WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdCurrency=" + m_IdCurrency, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdCurrency = (Int32)dr["IdCurrency"];
                m_Code = (String)dr["Code"];
                m_Name = (String)dr["Name"];
                m_Sign = (String)dr["Sign"];
            }
        }

        public DataTable GetCurrencyFactor(Int32 _IdModel, Int32 _IdCurrency)
        {
            return CD.DAC.ConsultSQL("SELECT [Value] FROM tblModelsCurrencies WHERE [IdCurrency] = " + _IdCurrency + " AND [IdModel] = " + _IdModel).Tables[0];
        }

        public bool CurrencyExists(Int32 _IdModel, Int32 _IdCurrency)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "parpCheckCurrencies";
            listPrm = LoadParameters("@IdModel", _IdModel, "@IdCurrency", _IdCurrency);
            DataTable dt = ConsultProc(sConsult, listPrm).Tables[0];

            if (dt.Rows.Count > 0)
                return true;
            else
                return false;
        }

    }
}
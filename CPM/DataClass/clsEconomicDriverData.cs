﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsEconomicDriverData : CD.DAC
    {

        private Int32 m_IdEconomicDriverData;
        private Int32 m_IdEconomicDriver;
        private Int32 m_IdModel;
        private Int32 m_Year;
        private Double m_Normal;
        private Double m_NormalElasticity;
        private Double m_NormalWeight;
        private Double m_Optimistic;
        private Double m_OptimisticElasticity;
        private Double m_OptimisticWeight;
        private Double m_Pessimistic;
        private Double m_PessimisticElasticity;
        private Double m_PessimisticWeight;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;

        public Int32 IdEconomicDriverData { get { return m_IdEconomicDriverData; } set { m_IdEconomicDriverData = value; } }
        public Int32 IdEconomicDriver { get { return m_IdEconomicDriver; } set { m_IdEconomicDriver = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public Int32 Year { get { return m_Year; } set { m_Year = value; } }
        public Double Normal { get { return m_Normal; } set { m_Normal = value; } }
        public Double NormalElasticity { get { return m_NormalElasticity; } set { m_NormalElasticity = value; } }
        public Double NormalWeight { get { return m_NormalWeight; } set { m_NormalWeight = value; } }
        public Double Optimistic { get { return m_Optimistic; } set { m_Optimistic = value; } }
        public Double OptimisticElasticity { get { return m_OptimisticElasticity; } set { m_OptimisticElasticity = value; } }
        public Double OptimisticWeight { get { return m_OptimisticWeight; } set { m_OptimisticWeight = value; } }
        public Double Pessimistic { get { return m_Pessimistic; } set { m_Pessimistic = value; } }
        public Double PessimisticElasticity { get { return m_PessimisticElasticity; } set { m_PessimisticElasticity = value; } }
        public Double PessimisticWeight { get { return m_PessimisticWeight; } set { m_PessimisticWeight = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertEconomicDriverData";
            listPrm = DAC.LoadParameters("@IdEconomicDriverData", IdEconomicDriverData, "@IdEconomicDriver", IdEconomicDriver, "@IdModel", IdModel, "@Year", Year, "@Normal", Normal, "@NormalElasticity", NormalElasticity, "@NormalWeight", NormalWeight, "@Optimistic", Optimistic, "@OptimisticElasticity", OptimisticElasticity, "@OptimisticWeight", OptimisticWeight, "@Pessimistic", Pessimistic, "@PessimisticElasticity", PessimisticElasticity, "@PessimisticWeight", PessimisticWeight, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdEconomicDriverData = this.MaxID();
            return m_IdEconomicDriverData;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdateEconomicDriverData";
            listPrm = DAC.LoadParameters("@IdEconomicDriverData", IdEconomicDriverData, "@IdEconomicDriver", IdEconomicDriver, "@IdModel", IdModel, "@Year", Year, "@Normal", Normal, "@NormalElasticity", NormalElasticity, "@NormalWeight", NormalWeight, "@Optimistic", Optimistic, "@OptimisticElasticity", OptimisticElasticity, "@OptimisticWeight", OptimisticWeight, "@Pessimistic", Pessimistic, "@PessimisticElasticity", PessimisticElasticity, "@PessimisticWeight", PessimisticWeight, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteEconomicDriverData";
            listPrm = LoadParameters("@IdEconomicDriverData", IdEconomicDriverData);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdEconomicDriverData) FROM tblEconomicDriverData ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        
        public DataTable LoadList()
        {
            String sList;
            List<SqlParameter> listPrm;
            sList = "parpListEconomicDriverData";
            listPrm = LoadParameters("@IdModel",IdModel,"@IdEconomicDriver", IdEconomicDriver);
            return ConsultProc(sList, listPrm).Tables[0];
        }

        public double PercentageResidual(int EconomicDriver,int Model, int Year,int Stage)
        {
            String sList;
            double result=0;
            List<SqlParameter> listPrm;
            sList = "parpRestPercentageEconomicDriver";
            listPrm = LoadParameters("@Model", Model, "@year", Year, "@EconomicDriver", EconomicDriver, "@Stage", Stage);
            result = Convert.ToDouble(ConsultProc(sList, listPrm).Tables[0].Rows[0][0]);
            result = 100 - result;
            return result;
        }

        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListEconomicDriverData " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListEconomicDriverData WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdEconomicDriverData=" + m_IdEconomicDriverData, "");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdEconomicDriverData = (Int32)dr["IdEconomicDriverData"];
                m_IdModel = (Int32)dr["IdModel"];
                m_IdEconomicDriver = (Int32)dr["IdEconomicDriver"];
                m_Year = (Int32)dr["Year"];
                m_Normal = Convert.ToDouble(dr["Normal"]);
                m_Pessimistic = Convert.ToDouble(dr["Pessimistic"]);
                m_Optimistic = Convert.ToDouble(dr["Optimistic"]);
                m_NormalElasticity = Convert.ToDouble(dr["NormalElasticity"]);
                m_PessimisticElasticity = Convert.ToDouble(dr["PessimisticElasticity"]);
                m_OptimisticElasticity = Convert.ToDouble(dr["OptimisticElasticity"]);
                m_NormalWeight = Convert.ToDouble(dr["NormalWeight"]);
                m_PessimisticWeight = Convert.ToDouble(dr["PessimisticWeight"]);
                m_OptimisticWeight = Convert.ToDouble(dr["OptimisticWeight"]);
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }
        }

        public DataTable LoadListExport(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListEconomicDriverDataExport " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListEconomicDriverDataExport WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

    }
}

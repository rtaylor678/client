﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsFields : CD.DAC
    {

        private Int32 m_IdField;
        private Int32 m_IdModel;
        private String m_Code;
        private String m_Name;
        private Int32 m_IdAggregationLevel;
        private Int32 m_IdTypeOperation;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;
        private Int32 m_Starts;
        private Int32 m_OldStarts;

        public Int32 IdField { get { return m_IdField; } set { m_IdField = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public String Code { get { return m_Code; } set { m_Code = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }
        public Int32 IdAggregationLevel { get { return m_IdAggregationLevel; } set { m_IdAggregationLevel = value; } }
        public Int32 IdTypeOperation { get { return m_IdTypeOperation; } set { m_IdTypeOperation = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }
        public Int32 Starts { get { return m_Starts; } set { m_Starts = value; } }
        public Int32 OldStarts { get { return m_OldStarts; } set { m_OldStarts = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertFields";
            listPrm = DAC.LoadParameters("@IdField", IdField, "@IdModel", IdModel, "@Code", Code, "@Name", Name, "@IdAggregationLevel", IdAggregationLevel, "@IdTypeOperation", IdTypeOperation, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification, "@Starts", Starts);
            ExecuteProc(sInsert, listPrm);
            m_IdField = this.MaxID();
            return m_IdField;
        }

        public void Update()
        {
            // ToDo: Commented out Cleanup Code as requested by Jesus 2014-03-25
            String sUpdate;
            //String sCleanupDirect;
            //String sCleanupShared;
            List<SqlParameter> listPrm;
            //List<SqlParameter> listPrmCleanupDirect;
            //List<SqlParameter> listPrmCleanupShared;
            sUpdate = "parpUpdateFields";
            //sCleanupDirect = "parpCleanupDirectCostField";
            //sCleanupShared = "parpCleanupSharedCostField";
            listPrm = DAC.LoadParameters("@IdField", IdField, "@IdModel", IdModel, "@Code", Code, "@Name", Name, "@IdAggregationLevel", IdAggregationLevel, "@IdTypeOperation", IdTypeOperation, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification, "@Starts", Starts, "@OldStarts", OldStarts);
            //listPrmCleanupDirect = DAC.LoadParameters("@IdField", IdField, "@IdModel", IdModel);
            //listPrmCleanupShared = DAC.LoadParameters("@IdField", IdField, "@IdModel", IdModel);
            ExecuteProc(sUpdate, listPrm);
            //ExecuteProc(sCleanupDirect, listPrmCleanupDirect);
            //ExecuteProc(sCleanupShared, listPrmCleanupShared);
        }

        public void DeleteAll()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteAllFields";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteFields";
            listPrm = LoadParameters("@IdField", IdField);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdField) FROM tblFields ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadComboBox(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT IdField ID, Code, Name FROM tblFields " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT IdField ID, Code, Name FROM tblFields WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadExportList(string strFilter, string strOrder)
        {
            return CD.DAC.ConsultSQL("SELECT Code,Name, AggregationLevels, TypeOperation FROM vListFields WHERE  " + strFilter + " " + strOrder).Tables[0];
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            //Added DISTINCT to queries as otherwise it was returning duplicate values and causing errors on creation of datatable in calling code
            //tested all calls to LoadList and results still good for all other calls. R Manubens 2014-06-26
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT DISTINCT * FROM vListFields " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT DISTINCT * FROM vListFields WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadListDrivers(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListFieldsDrivers " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListFieldsDrivers WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdField =" + m_IdField, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdField = (Int32)dr["IdField"];
                m_IdModel = (Int32)dr["IdModel"];
                m_Code = (String)dr["Code"];
                m_Name = (String)dr["Name"];
                m_IdAggregationLevel = (Int32)dr["IdAggregationLevel"];
                m_IdTypeOperation = (Int32)dr["IdTypeOperation"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
                m_Starts = (Int32)dr["Starts"];
            }
        }

        public Int32 ValidateFieldNewStarts(int intIdField, int intStartDate)
        {
            DataTable dt = CD.DAC.ConsultSQL("SELECT COUNT(IdProject) FROM tblProjects WHERE IdField = " + intIdField + " AND Operation = 1 AND Starts < " + intStartDate).Tables[0];

            if (dt.Rows.Count>0)
                return Convert.ToInt32(dt.Rows[0][0]);
            else
                return 0;
        }

        public DataTable LoadListByPeerGroups(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT DISTINCT * FROM vListFieldsPerPeerGroup " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT DISTINCT * FROM vListFieldsPerPeerGroup WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

    }
}
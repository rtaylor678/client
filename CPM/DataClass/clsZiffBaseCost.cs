﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsZiffBaseCost : CD.DAC
    {

        #region Declarations

        private Int32 m_IdZiffBaseCost;
        private Int32 m_IdField;
        private Int32 m_IdZiffAccount;
        private Int32 m_IdPeerCriteria;
        private Double m_UnitCost;
        private Double m_Quantity;
        private Double m_TotalCost;
        private Int32 m_IdModel;
        private Int32 m_IdPeerGroup;

        public Int32 IdZiffBaseCost { get { return m_IdZiffBaseCost; } set { m_IdZiffBaseCost = value; } }
        public Int32 IdField { get { return m_IdField; } set { m_IdField = value; } }
        public Int32 IdZiffAccount { get { return m_IdZiffAccount; } set { m_IdZiffAccount = value; } }
        public Int32 IdPeerCriteria { get { return m_IdPeerCriteria; } set { m_IdPeerCriteria = value; } }
        public Double UnitCost { get { return m_UnitCost; } set { m_UnitCost = value; } }
        public Double Quantity { get { return m_Quantity; } set { m_Quantity = value; } }
        public Double TotalCost { get { return m_TotalCost; } set { m_TotalCost = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public Int32 IdPeerGroup { get { return m_IdPeerGroup; } set { m_IdPeerGroup = value; } }

        #endregion

        #region Methods

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "allpInsertZiffBaseCost";
            listPrm = DAC.LoadParameters("@IdZiffBaseCost", IdZiffBaseCost, "@IdPeerGroup", IdPeerGroup, "@IdZiffAccount", IdZiffAccount, "@IdPeerCriteria", IdPeerCriteria, "@UnitCost", UnitCost, "@Quantity", Quantity, "@TotalCost", TotalCost);
            ExecuteProc(sInsert, listPrm);
            m_IdPeerCriteria = this.MaxID();
            return m_IdPeerCriteria;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "allpUpdateZiffBaseCost";
            listPrm = DAC.LoadParameters("@IdZiffBaseCost", IdZiffBaseCost, "@IdPeerGroup", IdPeerGroup, "@IdZiffAccount", IdZiffAccount, "@IdPeerCriteria", IdPeerCriteria, "@UnitCost", UnitCost, "@Quantity", Quantity, "@TotalCost", TotalCost);
            ExecuteProc(sUpdate, listPrm);
        }

        public void DeleteAll()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeleteAllZiffBaseCost";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeleteZiffBaseCost";
            listPrm = LoadParameters("@IdZiffBaseCost", IdZiffBaseCost);
            ExecuteProc(sDelete, listPrm);
        }

        public void UpdateQuantity()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "allpUpdateZiffBaseCostQuantity";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sUpdate, listPrm);
        }

        public void UpdateQuantity(String pExternalAccountCode, String pPeerGroupDescription, Double pUnitCost)
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "allpUpdateZiffBaseCostUnitCost";
            listPrm = LoadParameters("@IdModel", IdModel, "@ExternalAccountCode", pExternalAccountCode, "@PeerGroupDescription", pPeerGroupDescription, "@UnitCost", pUnitCost);
            ExecuteProc(sUpdate, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdZiffBaseCost) FROM tblZiffBaseCost ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT * "
                + " FROM tblCalcBaseCostByFieldZiff "
                + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * "
                + " FROM tblCalcBaseCostByFieldZiff "
                + " WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public Double GetCostPeerCriteria(Int32 pIDField, Int32 pidPeer, Int32 pidZiffAcnt)
        {
            DataTable datTmp = CD.DAC.ConsultSQL("SELECT UnitCost "
                + "FROM tblZiffBaseCost "
                //+ "WHERE IdField = " + pIDField.ToString()
                + "WHERE IdZiffBaseCost = " + pIDField.ToString()
                + " AND IdZiffAccount = " + pidZiffAcnt.ToString()
                + " AND IdPeerCriteria = " + pidPeer.ToString() + "").Tables[0];

            if (datTmp.Rows.Count > 0)
            {
                return Convert.ToDouble(datTmp.Rows[0]["UnitCost"].ToString());
            }
            else
            {
                return 0.0;
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdZiffBaseCost = " + m_IdZiffBaseCost, "");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdZiffBaseCost = (Int32)dr["IdZiffBaseCost"];
                m_IdField = (Int32)dr["IdField"];
                m_IdZiffAccount = (Int32)dr["IdZiffAccount"];
                m_IdPeerCriteria = (Int32)dr["IdPeerCriteria"];
                m_UnitCost = (Double)dr["UnitCost"];
                m_Quantity = (Double)dr["Quantity"];
                m_TotalCost = (Double)dr["TotalCost"];
            }
        }

        public Int32 SearchZiffBaseCost()
        {
            CD.DAC DAC = new CD.DAC();
            String sSelect;
            List<SqlParameter> listPrm;
            sSelect = "allpSearchZiffBaseCost";
            listPrm = DAC.LoadParameters("@IdField", IdField, "@IdZiffAccount", IdZiffAccount, "@IdPeerCriteria", IdPeerCriteria);
            DataTable dt = ConsultProc(sSelect, listPrm).Tables[0];
            if (dt.Rows.Count > 0)
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
            else
            {
                return 0;
            }
        }

        public DataTable LoadListExternalBaseCostReferences()
        {
            CD.DAC DAC = new CD.DAC();
            String sList;
            List<SqlParameter> listPrm;
            sList = "allpListExternalBaseCostReferences";
            listPrm = DAC.LoadParameters("@IdModel", IdModel, "@IdPeerGroup", IdPeerGroup);
            return ConsultProc(sList, listPrm).Tables[0];
        }

        public DataTable LoadExport()
        {
            CD.DAC DAC = new CD.DAC();
            String sList;
            List<SqlParameter> listPrm;
            sList = "allpListExportExternalBaseCostReferences";
            listPrm = DAC.LoadParameters("@IdModel", IdModel, "@IdPeerGroup", IdPeerGroup);
            return ConsultProc(sList, listPrm).Tables[0];
        }

        #endregion

    }
}
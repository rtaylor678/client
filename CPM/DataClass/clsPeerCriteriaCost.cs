﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsPeerCriteriaCost : CD.DAC
    {

        #region Declarations

        private int m_IdPeerCriteria;
        private int m_IdField;
        private double m_Quantity;
        private int m_IdModel;
        private int m_IdPeerGroup;
        
        public Int32 IdPeerCriteria { get { return m_IdPeerCriteria; } set { m_IdPeerCriteria = value; } }
        public Int32 IdField { get { return m_IdField; } set { m_IdField = value; } }
        public Double Quantity { get { return m_Quantity; } set { m_Quantity = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public Int32 IdPeerGroup { get { return m_IdPeerGroup; } set { m_IdPeerGroup = value; } }
        
        #endregion

        #region Methods

        public void Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "allpInsertPeerCriteriaCost";
            listPrm = DAC.LoadParameters("@IdPeerCriteria", IdPeerCriteria, "@IdPeerGroup", IdPeerGroup, "@Quantity", Quantity);
            ExecuteProc(sInsert, listPrm);
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "allpUpdatePeerCriteriaCost";
            listPrm = DAC.LoadParameters("@IdPeerCriteria", IdPeerCriteria, "@IdPeerGroup", IdPeerGroup, "@Quantity", Quantity);
            ExecuteProc(sUpdate, listPrm);
        }

        public void DeleteAll()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeleteAllPeerCriteriaCost";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeletePeerCriteriaCost";
            listPrm = LoadParameters("@IdPeerCriteria", IdPeerCriteria, "@IdPeerGroup", IdPeerGroup);
            ExecuteProc(sDelete, listPrm);
        }

        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT * "
                + " FROM vListPeerCriteriaCost "
                + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * "
                + " FROM vListPeerCriteriaCost "
                + " WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadListPrices(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT IdPeerCriteria,PeerCriteria "
                + " FROM vListPeerCriteriaCost "
                + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT IdPeerCriteria,PeerCriteria "
                + " FROM vListPeerCriteriaCost "
                + " WHERE  " + strFilter + " GROUP BY IdPeerCriteria,PeerCriteria " + strOrder).Tables[0];
            }
        }


        public void loadObject()
        {
            DataTable dt = LoadList("IdPeerCriteria = " + m_IdPeerCriteria + " AND IdPeerGroup = " + m_IdPeerGroup, " AND Quantity IS NOT NULL");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdPeerCriteria = (Int32)dr["IdPeerCriteria"];
                m_IdPeerGroup = (Int32)dr["IdPeerGroup"];
                m_Quantity = (Double)dr["Quantity"];
            }
        }

        public DataTable LoadListPeerCriteria()
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "allpListPeerCriteria";
            listPrm = LoadParameters("@IdModel", IdModel, "@IdPeerGroup", IdPeerGroup);
            return ConsultProc(sConsult, listPrm).Tables[0];
        }

        #endregion

    }
}
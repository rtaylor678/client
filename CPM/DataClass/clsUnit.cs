﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using CD;

namespace DataClass
{
    public class clsUnit : CD.DAC
    {

        private Int32 m_IdUnit;
        private Int32 m_IdTypeUnit;
        private String m_Name;

        public Int32 IdUnit { get { return m_IdUnit; } set { m_IdUnit = value; } }
        public Int32 IdTypeUnit { get { return m_IdTypeUnit; } set { m_IdTypeUnit = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "usrpInsertUnit";
            listPrm = DAC.LoadParameters("@IdUnit", IdUnit, "@IdTypeUnit", IdTypeUnit, "@Name", Name);
            ExecuteProc(sInsert, listPrm);
            m_IdTypeUnit = this.MaxID();
            return m_IdTypeUnit;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "usrpUpdateUnit";
            listPrm = DAC.LoadParameters("@IdUnit", IdUnit, "@IdTypeUnit", IdTypeUnit, "@Name", Name);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "usrpDeleteUnit";
            listPrm = LoadParameters("@IdUnit", m_IdUnit);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdUnit) FROM tblUnits ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT *, TypeUnitName AS UnitType FROM vListUnit " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT *, TypeUnitName AS UnitType FROM vListUnit WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadComboBox(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT IdUnit ID,Name FROM tblUnits " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT IdUnit ID,Name FROM tblUnits WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdUnit =" + m_IdUnit, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                m_IdUnit = (Int32)dr["IdUnit"];
                m_IdTypeUnit = (Int32)dr["IdTypeUnit"];
                m_Name = (String)dr["Name"];
            }

        }

    }
}
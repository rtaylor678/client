﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsProfileRoles : CD.DAC
    {

        private Int32 m_IdProfileRoles;
        private Int32 m_IdProfile;
        private Int32 m_IdRole;
        private Int32 m_IdModel;
        private Boolean m_DenyAccess;
        private Boolean m_GrantAccess;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;
        private Int32 m_IdLanguage; // used only to pass the Session Variable

        public Int32 IdProfileRoles { get { return m_IdProfileRoles; } set { m_IdProfileRoles = value; } }
        public Int32 IdProfile { get { return m_IdProfile; } set { m_IdProfile = value; } }
        public Int32 IdRole { get { return m_IdRole; } set { m_IdRole = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public Boolean DenyAccess { get { return m_DenyAccess; } set { m_DenyAccess = value; } }
        public Boolean GrantAccess { get { return m_GrantAccess; } set { m_GrantAccess = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }
        public Int32 IdLanguage { get { return m_IdLanguage; } set { m_IdLanguage = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "usrpInsertProfileRoles";
            listPrm = DAC.LoadParameters("@IdProfileRoles", IdProfileRoles, "@IdProfile", IdProfile, "@IdRole", IdRole, "@IdModel", IdModel, "@DenyAccess", DenyAccess, "@GrantAccess", GrantAccess, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdModel = this.MaxID();
            return m_IdModel;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "usrpUpdateProfileRoles";
            listPrm = DAC.LoadParameters("@IdProfileRoles", IdProfileRoles, "@IdProfile", IdProfile, "@IdRole", IdRole, "@IdModel", IdModel, "@DenyAccess", DenyAccess, "@GrantAccess", GrantAccess, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "usrpDeleteProfileRoles";
            listPrm = LoadParameters("@IdProfileRoles", m_IdProfileRoles);
            ExecuteProc(sDelete, listPrm);
        }

        public void DeleteRoles()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "usrpDeleteProfileRolesxProfile";
            listPrm = LoadParameters("@IdProfile", m_IdProfile);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdProfileRoles) FROM tblProfilesRoles ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblProfilesRoles " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblProfilesRoles WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadPermission(int _IdModel, int _IdProfile)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "usrpListProfilePermissions";
            listPrm = LoadParameters("@IdModel", _IdModel, "@IdProfile", _IdProfile);
            return ConsultProc(sConsult, listPrm).Tables[0];
        }

        public DataTable LoadProfileRoles()
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "usrpListProfileRoles";
            listPrm = LoadParameters("@IdModel", m_IdModel, "@IdProfile", m_IdProfile, "@IdLanguage", m_IdLanguage);
            return ConsultProc(sConsult, listPrm).Tables[0];
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdProfileRoles=" + m_IdProfileRoles, " ORDER BY IdModel");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                m_IdProfileRoles = (Int32)dr["IdProfileRoles"];
                m_IdProfile = (Int32)dr["IdProfile"];
                m_IdRole = (Int32)dr["IdRole"];
                m_IdModel = (Int32)dr["IdModel"];
                m_DenyAccess = (Boolean)dr["DenyAccess"];
                m_GrantAccess = (Boolean)dr["GrantAccess"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }
        }

    }
}
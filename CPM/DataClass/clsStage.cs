﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using CD;

namespace DataClass
{
    public class clsStage : CD.DAC
    {

        private Int32 m_IdStage;
        private String m_Name;

        public Int32 IdStage { get { return m_IdStage; } set { m_IdStage = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "usrpInsertStage";
            listPrm = DAC.LoadParameters("@IdStage", IdStage, "@Name", Name);
            ExecuteProc(sInsert, listPrm);
            m_IdStage = this.MaxID();
            return m_IdStage;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "usrpUpdateStage";
            listPrm = DAC.LoadParameters("@IdStage", IdStage, "@Name", Name);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "usrpDeleteStage";
            listPrm = LoadParameters("@IdStage", m_IdStage);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdStage) FROM tblStage ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblStage " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblStage WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadComboBox(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT IdStage ID,Name FROM tblStage " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT IdStage ID,Name FROM tblStage WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdStage =" + m_IdStage, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                m_IdStage = (Int32)dr["IdStage"];
                m_Name = (String)dr["Name"];
            }
        }

    }
}
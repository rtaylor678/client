﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsPeerGroupField : CD.DAC
    {

        private Int32 m_IdPeerGroupField;
        private Int32 m_IdPeerGroup;
        private Int32 m_IdField;
        private Int32 m_IdModel;

        public Int32 IdPeerGroupField { get { return m_IdPeerGroupField; } set { m_IdPeerGroupField = value; } }
        public Int32 IdPeerGroup { get { return m_IdPeerGroup; } set { m_IdPeerGroup = value; } }
        public Int32 IdField { get { return m_IdField; } set { m_IdField = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "allpInsertPeerGroupField";
            listPrm = DAC.LoadParameters("@IdPeerGroupField", IdPeerGroupField, "@IdPeerGroup", IdPeerGroup, "@IdField", IdField);
            ExecuteProc(sInsert, listPrm);
            m_IdPeerGroupField = this.MaxID();
            return m_IdPeerGroupField;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "allpUpdatePeerGroupField";
            listPrm = DAC.LoadParameters("@IdPeerGroupField", IdPeerGroupField, "@IdPeerGroup", IdPeerGroup, "@IdField", IdField);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeletePeerGroupField";
            listPrm = LoadParameters("@IdPeerGroupField", IdPeerGroupField);
            ExecuteProc(sDelete, listPrm);
        }

        public void DeleteAll()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeletePeerGroupFieldAll";
            listPrm = LoadParameters("@IdPeerGroup", IdPeerGroup);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdPeerGroupField) FROM tblPeerGroupField ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblPeerGroupField " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblPeerGroupField WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdPeerGroupField=" + m_IdPeerGroupField, " ORDER BY IdPeerGroupField");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdPeerGroupField = (Int32)dr["IdPeerGroupField"];
                m_IdPeerGroup = (Int32)dr["IdPeerGroup"];
                m_IdField = (Int32)dr["IdField"];
            }
        }

        public Int32 GetFieldId(String _Field)
        {
            DataTable dt = CD.DAC.ConsultSQL("SELECT IdField FROM tblFields WHERE IdModel = " + IdModel + " AND UPPER(Code) = '" + _Field.ToUpper() + "'").Tables[0]; 
            if (dt.Rows.Count>0)
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
            else
            {
                return 0;
            }
        }

        public DataTable LoadField(Int32 _IdPeerGroup)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "allpListPeerGroupField";
            listPrm = LoadParameters("@IdModel", IdModel, "@IdPeerGroup", _IdPeerGroup);
            return ConsultProc(sConsult, listPrm).Tables[0];
        }

    }
}
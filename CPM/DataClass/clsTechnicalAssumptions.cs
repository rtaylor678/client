﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsTechnicalAssumptions : CD.DAC
    {

        private Int32 m_IdTechnicalAssumption;
        private Int32 m_IdModel;
        private Int32 m_IdTypeOperation;
        private Int32 m_IdTechnicalDriverSize;
        private Int32 m_IdTechnicalDriverPerformance;
        private Int32 m_IdZiffAccount;
        private Int32 m_CycleValue;
        private Int32 m_ProjectionCriteria;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;

        public Int32 IdTechnicalAssumption { get { return m_IdTechnicalAssumption; } set { m_IdTechnicalAssumption = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public Int32 IdTypeOperation { get { return m_IdTypeOperation; } set { m_IdTypeOperation = value; } }
        public Int32 IdTechnicalDriverSize { get { return m_IdTechnicalDriverSize; } set { m_IdTechnicalDriverSize = value; } }
        public Int32 IdTechnicalDriverPerformance { get { return m_IdTechnicalDriverPerformance; } set { m_IdTechnicalDriverPerformance = value; } }
        public Int32 IdZiffAccount { get { return m_IdZiffAccount; } set { m_IdZiffAccount = value; } }
        public Int32 CycleValue { get { return m_CycleValue; } set { m_CycleValue = value; } }
        public Int32 ProjectionCriteria { get { return m_ProjectionCriteria; } set { m_ProjectionCriteria = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertTechnicalAssumption";
            listPrm = DAC.LoadParameters("@IdTechnicalAssumption", IdTechnicalAssumption, "@IdTechnicalDriverSize", IdTechnicalDriverSize, "@IdTechnicalDriverPerformance", IdTechnicalDriverPerformance, "@IdZiffAccount", IdZiffAccount, "@IdModel", IdModel, "@IdTypeOperation", IdTypeOperation, "@CycleValue", CycleValue, "@ProjectionCriteria", ProjectionCriteria, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdTechnicalAssumption = this.MaxID();
            return m_IdTechnicalAssumption;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdateTechnicalAssumption";
            listPrm = DAC.LoadParameters("@IdTechnicalAssumption", IdTechnicalAssumption, "@IdTechnicalDriverSize", IdTechnicalDriverSize, "@IdTechnicalDriverPerformance", IdTechnicalDriverPerformance, "@IdZiffAccount", IdZiffAccount, "@IdModel", IdModel, "@IdTypeOperation", IdTypeOperation, "@CycleValue", CycleValue, "@ProjectionCriteria", ProjectionCriteria, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteTechnicalAssumption";
            listPrm = LoadParameters("@IdTechnicalAssumption", IdTechnicalAssumption);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdTechnicalAssumption) FROM tblTechnicalAssumption ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }        
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListTechnicalAssumption " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListTechnicalAssumption WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void CleanupSemifixed()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteTechnicalAssumptionCleanupSemifixed";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdTechnicalAssumption=" + m_IdTechnicalAssumption, " ORDER BY CodeZiffAccount, NameZiffAccount");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdTechnicalAssumption = (Int32)dr["IdTechnicalAssumption"];
                m_IdModel = (Int32)dr["IdModel"];
                m_IdTypeOperation = (Int32)dr["IdTypeOperation"];
                m_IdTechnicalDriverSize = (Int32)dr["IdTechnicalDriverSize"];
                m_IdTechnicalDriverPerformance = (Int32)dr["IdTechnicalDriverPerformance"];
                m_IdZiffAccount = (Int32)dr["IdZiffAccount"];
                m_CycleValue = (Int32)dr["CycleValue"];
                m_ProjectionCriteria = (Int32)dr["ProjectionCriteria"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }
        }

        public DataTable LoadList(Int32 _idModel, Int32 _idField, Int32 _idTypeOperation)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "tecpListTechnicalAssumption";
            listPrm = LoadParameters("@IdModel", _idModel, "@IdField", _idField, "@IdTypeOperation", _idTypeOperation);
            DataSet ds = ConsultProc(sConsult, listPrm);
            return ds.Tables[0];
        }

        public DataTable LoadExport()
        {
            CD.DAC DAC = new CD.DAC();
            String sList;
            List<SqlParameter> listPrm;
            sList = "tecpListExportTechnicalAssumptions";
            listPrm = DAC.LoadParameters("@IdModel", IdModel, "@IdTypeOperation", IdTypeOperation);
            return ConsultProc(sList, listPrm).Tables[0];
        }

        public DataTable LoadListImport(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblTechnicalAssumption " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblTechnicalAssumption WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObjectImport()
        {
            DataTable dt = LoadListImport("IdTechnicalAssumption=" + m_IdTechnicalAssumption, "");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdTechnicalAssumption = (Int32)dr["IdTechnicalAssumption"];
                m_IdModel = (Int32)dr["IdModel"];
                m_IdTypeOperation = (Int32)dr["IdTypeOperation"];
                m_IdTechnicalDriverSize = (Int32)dr["IdTechnicalDriverSize"];
                m_IdTechnicalDriverPerformance = (Int32)dr["IdTechnicalDriverPerformance"];
                m_IdZiffAccount = (Int32)dr["IdZiffAccount"];
                m_CycleValue = (Int32)dr["CycleValue"];
                m_ProjectionCriteria = (Int32)dr["ProjectionCriteria"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }
        }

    }
}
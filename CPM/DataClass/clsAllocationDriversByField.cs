﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsAllocationDriversByField : CD.DAC
    {

        private Int32 m_IdAllocationDriversByField;
        private Int32 m_IdAllocationListDriver;
        private Int32 m_IdField;
        private Double m_Amount;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;

        public Int32 IdAllocationDriversByField { get { return m_IdAllocationDriversByField; } set { m_IdAllocationDriversByField = value; } }
        public Int32 IdAllocationListDriver { get { return m_IdAllocationListDriver; } set { m_IdAllocationListDriver = value; } }
        public Int32 IdField { get { return m_IdField; } set { m_IdField = value; } }
        public Double Amount { get { return m_Amount; } set { m_Amount = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "allpInsertAllocationDriversByField";
            listPrm = DAC.LoadParameters("@IdAllocationDriversByField", IdAllocationDriversByField, "@IdAllocationListDriver", IdAllocationListDriver, "@IdField", IdField, "@Amount", Amount, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdAllocationDriversByField = this.MaxID();
            return m_IdAllocationDriversByField;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "allpUpdateAllocationDriversByField";
            listPrm = DAC.LoadParameters("@IdAllocationDriversByField", IdAllocationDriversByField, "@IdAllocationListDriver", IdAllocationListDriver, "@IdField", IdField, "@Amount", Amount, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeleteAllocationDriversByField";
            listPrm = LoadParameters("@IdAllocationDriversByField", IdAllocationDriversByField);
            ExecuteProc(sDelete, listPrm);
        }

        public void DeleteAll()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeleteAllocationDriversByFieldAll";
            listPrm = LoadParameters("@IdAllocationListDriver", IdAllocationListDriver);
            ExecuteProc(sDelete, listPrm);
        }

        public void DeleteAllByModel(Int32 IdModel)
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeleteAllAllocationDriversByField";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdAllocationDriversByField) FROM vListAllocationDriversByField ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadFields(Int32 _IdModel)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "allpListDriverDataFields";
            listPrm = LoadParameters("@IdModel", _IdModel, "@IdAllocationListDriver", IdAllocationListDriver);
            return ConsultProc(sConsult, listPrm).Tables[0];
        }

        public DataTable LoadDriverDataCostCenter(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListAllocationDriversByFieldCC " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListAllocationDriversByFieldCC WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadComboBox(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT IdAllocationDriversByField ID, Name FROM vListAllocationDriversByField " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT IdAllocationDriversByField ID, Name FROM vListAllocationDriversByField WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadExportList(string strFilter, string strOrder)
        {
            return CD.DAC.ConsultSQL("SELECT Name,TypeOperation FROM vListAllocationDriversByField WHERE  " + strFilter + " " + strOrder).Tables[0];
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListAllocationDriversByField " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListAllocationDriversByField WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdAllocationDriversByField=" + m_IdAllocationDriversByField, "");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdAllocationDriversByField = (Int32)dr["IdAllocationDriversByField"];
                m_IdAllocationListDriver = (Int32)dr["IdAllocationListDriver"];
                m_IdField = (Int32)dr["IdField"];
                m_Amount = Convert.ToDouble(dr["Amount"]);
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }
        }

        public DataTable LoadExportDriversDataList(string strFilter, string strOrder)
        {
            return CD.DAC.ConsultSQL("SELECT * FROM vListAllocationDriversData WHERE  " + strFilter + " " + strOrder).Tables[0];
        }
    }
}

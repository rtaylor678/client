﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsDirectCostAllocation : CD.DAC
    {

        private Int32 m_IdDirectCostAllocation;
        private Int32 m_IdModel;
        private Int32 m_IdClientCostCenter;
        private Double m_Cost;
        private Int32 m_IdField;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;

        public Int32 IdDirectCostAllocation { get { return m_IdDirectCostAllocation; } set { m_IdDirectCostAllocation = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public Int32 IdClientCostCenter { get { return m_IdClientCostCenter; } set { m_IdClientCostCenter = value; } }
        public Double Cost { get { return m_Cost; } set { m_Cost = value; } }
        public Int32 IdField { get { return m_IdField; } set { m_IdField = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "allpInsertDirectCostAllocation";
            listPrm = DAC.LoadParameters("@IdDirectCostAllocation", IdDirectCostAllocation, "@IdModel", IdModel, "@IdClientCostCenter", IdClientCostCenter, "@Cost", Cost, "@IdField", IdField, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdDirectCostAllocation = this.MaxID();
            return m_IdDirectCostAllocation;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "allpUpdateDirectCostAllocation";
            listPrm = DAC.LoadParameters("@IdDirectCostAllocation", IdDirectCostAllocation, "@IdModel", IdModel, "@IdClientCostCenter", IdClientCostCenter, "@Cost", Cost, "@IdField", IdField, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeleteDirectCostAllocation";
            listPrm = LoadParameters("@IdDirectCostAllocation", IdDirectCostAllocation);
            ExecuteProc(sDelete, listPrm);
        }

        public DataTable LoadField(Int32 _IdClientCostCenter)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "allpListDirectCostAllocation";
            listPrm = LoadParameters("@IdModel", IdModel, "@IdDirectCostAllocation", IdDirectCostAllocation, "@IdClientCostCenter", _IdClientCostCenter);
            return ConsultProc(sConsult, listPrm).Tables[0];
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdDirectCostAllocation) FROM tblDirectCostAllocation ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadExportList(string strFilter, string strOrder)
        {
            return CD.DAC.ConsultSQL("SELECT IdClientCostCenter,TypeOperation FROM vListDirectCostAllocation WHERE  " + strFilter + " " + strOrder).Tables[0];
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListDirectCostAllocation " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListDirectCostAllocation WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdDirectCostAllocation=" + m_IdDirectCostAllocation, " ORDER BY IdDirectCostAllocation");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdDirectCostAllocation = (Int32)dr["IdDirectCostAllocation"];
                m_IdModel = (Int32)dr["IdModel"];
                m_IdClientCostCenter = (Int32)dr["IdClientCostCenter"];
                if (dr["Cost"] != DBNull.Value)
                    m_Cost = Convert.ToDouble(dr["Cost"]);
                else
                    m_Cost = 0;
                m_IdField = (Int32)dr["IdField"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }
        }

    }
}
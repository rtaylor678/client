﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsTechnicalDrivers : CD.DAC
    {

        private Int32 m_IdTechnicalDriver;
        private Int32 m_IdModel;
        private String m_Name;
        private Int32 m_IdUnit;
        private Int32 m_TypeDriver;
        private Int32 m_IdTypeOperation;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;
        private Boolean m_UnitCostDenominator;
        private Boolean m_BaselineAllocation;

        public Int32 IdTechnicalDriver { get { return m_IdTechnicalDriver; } set { m_IdTechnicalDriver = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }
        public Int32 IdUnit { get { return m_IdUnit; } set { m_IdUnit = value; } }
        public Int32 TypeDriver { get { return m_TypeDriver; } set { m_TypeDriver = value; } }
        public Int32 IdTypeOperation { get { return m_IdTypeOperation; } set { m_IdTypeOperation = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }
        public Boolean UnitCostDenominator { get { return m_UnitCostDenominator; } set { m_UnitCostDenominator = value; } }
        public Boolean BaselineAllocation { get { return m_BaselineAllocation; } set { m_BaselineAllocation = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertTechnicalDriver";
            listPrm = DAC.LoadParameters("@IdTechnicalDriver", IdTechnicalDriver, "@IdModel", IdModel, "@Name", Name, "@IdUnit", IdUnit, "@TypeDriver", TypeDriver, "@IdTypeOperation", IdTypeOperation, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification, "@UnitCostDenominator", UnitCostDenominator, "@BaselineAllocation", BaselineAllocation);
            ExecuteProc(sInsert, listPrm);
            m_IdTechnicalDriver = this.MaxID();
            return m_IdTechnicalDriver;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdateTechnicalDriver";
            listPrm = DAC.LoadParameters("@IdTechnicalDriver", IdTechnicalDriver, "@IdModel", IdModel, "@Name", Name, "@IdUnit", IdUnit, "@TypeDriver", TypeDriver, "@IdTypeOperation", IdTypeOperation, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification, "@UnitCostDenominator", UnitCostDenominator, "@BaselineAllocation",BaselineAllocation);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteTechnicalDriver";
            listPrm = LoadParameters("@IdTechnicalDriver", IdTechnicalDriver);
            ExecuteProc(sDelete, listPrm);
        }

        public void DeleteAll()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteAllTechnicalDriver";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdTechnicalDriver) FROM tblTechnicalDrivers ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadComboBox(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT tblTechnicalDrivers.IdTechnicalDriver ID, tblTechnicalDrivers.Name FROM tblTechnicalDrivers LEFT OUTER JOIN tblTypesOperation ON tblTechnicalDrivers.IdTypeOperation = tblTypesOperation.IdTypeOperation " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT tblTechnicalDrivers.IdTechnicalDriver ID, tblTechnicalDrivers.Name FROM tblTechnicalDrivers LEFT OUTER JOIN tblTypesOperation ON tblTechnicalDrivers.IdTypeOperation = tblTypesOperation.IdTypeOperation WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadExportList(string strFilter, string strOrder)
        {
            return CD.DAC.ConsultSQL("SELECT IdTechnicalDriver, Name, NameTypeOperation, NameTypeDriver, NameUnit, NameTypeUnit FROM vListTechnicalDrivers WHERE  " + strFilter + " " + strOrder).Tables[0];
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListTechnicalDrivers " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListTechnicalDrivers WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdTechnicalDriver=" + m_IdTechnicalDriver, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdTechnicalDriver = (Int32)dr["IdTechnicalDriver"];
                m_IdModel = (Int32)dr["IdModel"];
                m_Name = (String)dr["Name"];
                m_IdUnit = (Int32)dr["IdUnit"];
                m_TypeDriver = (Int32)dr["TypeDriver"];
                m_IdTypeOperation = (Int32)dr["IdTypeOperation"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
                m_UnitCostDenominator = (Boolean)dr["UnitCostDenominator"];
                m_BaselineAllocation = (Boolean)dr["BaselineAllocation"];
            }
        }

    }
}
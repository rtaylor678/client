﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsUtilizationCapacity : CD.DAC
    {

        private Int32 m_IdUtilization;
        private Int32 m_IdModel;
        private String m_CodeZiff;
        private Int32 m_IdField;
        private Int32 m_IdStage;
        private Int32 m_BaseCostType;
        private Int32 m_UtilizationValue;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;
        private Double m_CurrentValue;

        public Int32 IdUtilization { get { return m_IdUtilization; } set { m_IdUtilization = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public String CodeZiff { get { return m_CodeZiff; } set { m_CodeZiff = value; } }
        public Int32 IdField { get { return m_IdField; } set { m_IdField = value; } }
        public Int32 IdStage { get { return m_IdStage; } set { m_IdStage = value; } }
        public Int32 BaseCostType { get { return m_BaseCostType; } set { m_BaseCostType = value; } }
        public Int32 UtilizationValue { get { return m_UtilizationValue; } set { m_UtilizationValue = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }
        public Double CurrentValue { get { return m_CurrentValue; } }

        public void Insert()
        {
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "tecpInsertUtilizationCapacity";
            listPrm = DAC.LoadParameters("@IdModel", IdModel, "@UserCreation", UserCreation, "@DateCreation", DateCreation);
            ExecuteProc(sInsert, listPrm);
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "tecpUpdateUtilizationCapacity";
            listPrm = DAC.LoadParameters("@IdUtilization", IdUtilization, "@IdModel", IdModel, "@UtilizationValue", UtilizationValue, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        //public void Delete()
        //{
        //    String sDelete;
        //    List<SqlParameter> listPrm;
        //    sDelete = "parpDeleteTechnicalAssumption";
        //    listPrm = LoadParameters("@IdTechnicalAssumption", IdTechnicalAssumption);
        //    ExecuteProc(sDelete, listPrm);
        //}

        //public Int32 MaxID()
        //{
        //    Int32 intValue = 0;
        //    DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdTechnicalAssumption) FROM tblTechnicalAssumption ").Tables[0];

        //    if (dt.Rows.Count > 0)
        //    {
        //        intValue = Convert.ToInt32(dt.Rows[0][0]);
        //    }

        //    return intValue;
        //}        
        
        //public DataTable LoadList(string strFilter, string strOrder)
        //{
        //    if (strFilter == String.Empty) 
        //    {
        //        return CD.DAC.ConsultSQL("SELECT * FROM vListTechnicalAssumption " + strOrder).Tables[0];
        //    }
        //    else
        //    {
        //        return CD.DAC.ConsultSQL("SELECT * FROM vListTechnicalAssumption WHERE  " + strFilter + " " + strOrder).Tables[0];
        //    }
        //}

        //public void CleanupSemifixed()
        //{
        //    String sDelete;
        //    List<SqlParameter> listPrm;
        //    sDelete = "parpDeleteTechnicalAssumptionCleanupSemifixed";
        //    listPrm = LoadParameters("@IdModel", IdModel);
        //    ExecuteProc(sDelete, listPrm);
        //}

        //public void loadObject()
        //{
        //    DataTable dt = LoadList("IdTechnicalAssumption=" + m_IdUtilization, " ORDER BY CodeZiffAccount, NameZiffAccount");
        //    if (dt.Rows.Count > 0)
        //    {
        //        DataRow dr = dt.Rows[0];

        //        m_IdUtilization = (Int32)dr["IdTechnicalAssumption"];
        //        m_IdModel = (Int32)dr["IdModel"];
        //        m_CodeZiff = (String)dr["IdTypeOperation"];
        //        m_IdField = (Int32)dr["IdTechnicalDriverSize"];
        //        m_IdStage = (Int32)dr["IdTechnicalDriverPerformance"];
        //        m_BaseCostType = (Int32)dr["IdZiffAccount"];
        //        m_UtilizationValue = (Int32)dr["CycleValue"];
        //        m_UserCreation = (Int32)dr["UserCreation"];
        //        m_DateCreation = (DateTime)dr["DateCreation"];
        //        m_UserModification = (Object)dr["UserModification"];
        //        m_DateModification = (Object)dr["DateModification"];
        //    }
        //}

        public DataSet LoadUtilizationCapacity(Int32 _IdModel, Int32 _IdStage, Int32 _BaseCostType)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "tecpUtilizationCapacity";
            listPrm = LoadParameters("@IdModel", _IdModel, "@IdStage", _IdStage, "@BaseCostType", _BaseCostType);
            return ConsultProc(sConsult, listPrm);
        }

        public Int32 GetIdUtilization(Int32 _IdModel, String _CodeZiff, Int32 _IdField, Int32 _IdStage, Int32 _BaseCostType)
        {

            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT IdUtilization, UtilizationValue FROM tblUtilizationCapacity WHERE IdModel = " + _IdModel + " AND CodeZiff = '" + _CodeZiff + "' AND IdField = " + _IdField + " AND IdStage = " + _IdStage + " AND BaseCostType = " + _BaseCostType + "").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
                m_CurrentValue = Convert.ToDouble(dt.Rows[0][1]);
            }

            return intValue;
        }

        public Int32 GetIdUtilization(Int32 _IdModel, Int32 _IdField, Int32 _IdZiffAccount)
        {

            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT IdUtilization, UtilizationValue FROM tblUtilizationCapacity WHERE IdModel = " + _IdModel + " AND IdZiffAccount = " + _IdZiffAccount + " AND IdField = " + _IdField).Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
                m_CurrentValue = Convert.ToDouble(dt.Rows[0][1]);
            }

            return intValue;
        }

        public Int32 GetIdField(String _Name, Int32 _IdModel)
        {
            Int32 intID = 0;

            DataTable dtField = CD.DAC.ConsultSQL("SELECT DISTINCT * FROM vListFields WHERE Name = '" + _Name + "' AND IdModel = " + _IdModel + " ORDER BY Name").Tables[0];

            if (dtField.Rows.Count > 0)
            {
                intID = (Int32)dtField.Rows[0]["IdField"];
            }

            return intID;
        }

    }
}
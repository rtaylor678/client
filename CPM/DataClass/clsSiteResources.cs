﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using CD;

namespace DataClass
{
    public class clsSiteResources : CD.DAC
    {

        private Int32 m_IdLanguage;
        private String m_Name;
        private String m_Flag;
        private Boolean m_Dafault;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;

        public Int32 IdLanguage { get { return m_IdLanguage; } set { m_IdLanguage = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }
        public String Flag { get { return m_Flag; } set { m_Flag = value; } }
        public Boolean Default { get { return m_Dafault; } set { m_Dafault = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "usrpInsertLanguage";
            listPrm = DAC.LoadParameters("@IdLanguage", IdLanguage, "@Name", Name, "@Flag", Flag, "@Default", Default, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            return m_IdLanguage;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "usrpUpdateLanguage";
            listPrm = LoadParameters("@IdLanguage", IdLanguage, "@Name", Name, "@Flag", Flag, "@Default", Default, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblSiteResources " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM tblSiteResources WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdLanguage=" + m_IdLanguage, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                m_IdLanguage = (Int32)dr["IdLanguage"];
                m_Name = (String)dr["Name"];
                m_Flag = (String)dr["Flag"];
                m_Dafault = (Boolean) dr["Dafault"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }

        }

    }
}
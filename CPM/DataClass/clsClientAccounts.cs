﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsClientAccounts : CD.DAC
    {

        private Int32 m_IdClientAccount;
        private Int32 m_IdModel;
        private String m_Account;
        private String m_Name;
        private Object m_IdResource;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;

        public Int32 IdClientAccount { get { return m_IdClientAccount; } set { m_IdClientAccount = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public String Account { get { return m_Account; } set { m_Account = value; } }
        public String Name { get { return m_Name; } set { m_Name = value; } }
        public Object IdResource { get { return m_IdResource; } set { m_IdResource = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "parpInsertClientAccounts";
            listPrm = DAC.LoadParameters("@IdClientAccount", IdClientAccount, "@IdModel", IdModel, "@Account", Account, "@Name", Name, "@IdResource", IdResource, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdClientAccount = this.MaxID();
            return m_IdClientAccount;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "parpUpdateClientAccounts";
            listPrm = DAC.LoadParameters("@IdClientAccount", IdClientAccount, "@IdModel", IdModel, "@Account", Account, "@Name", Name, "@IdResource", IdResource, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteClientAccounts";
            listPrm = LoadParameters("@IdClientAccount", IdClientAccount);
            ExecuteProc(sDelete, listPrm);
        }

        public void DeleteAll()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "parpDeleteAllClientAccounts";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdClientAccount) FROM tblClientAccounts ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadComboBox(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT IdClientAccount ID, Account, Name FROM tblClientAccounts " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT IdClientAccount ID, Account, Name FROM tblClientAccounts WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadExportList(string strFilter, string strOrder)
        {
            return CD.DAC.ConsultSQL("SELECT Account, Name, ResourceName FROM vListClientAccounts WHERE  " + strFilter + " " + strOrder).Tables[0];
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListClientAccounts " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListClientAccounts WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadAccountCeCo(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListAccountsCeCo " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListAccountsCeCo WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdClientAccount=" + m_IdClientAccount, " ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdClientAccount = (Int32)dr["IdClientAccount"];
                m_IdModel = (Int32)dr["IdModel"];
                m_Account = (String)dr["Account"];
                m_Name = (String)dr["Name"];
                m_IdResource = (Object)dr["IdResource"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }
        }

    }
}
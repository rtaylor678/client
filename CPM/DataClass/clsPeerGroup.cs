﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsPeerGroup : CD.DAC
    {

        #region Declarations

        private Int32 m_IdPeerGroup;
        private Int32 m_IdModel;
        private String m_PeerGroupDescription;
        private String m_PeerGroupCode;
        private Boolean m_FullBenchmark;
        private String m_Comments;
      
        public Int32 IdPeerGroup { get { return m_IdPeerGroup; } set { m_IdPeerGroup = value; } }
        public Int32 IdModel { get { return m_IdModel; } set { m_IdModel = value; } }
        public String PeerGroupDescription { get { return m_PeerGroupDescription; } set { m_PeerGroupDescription = value; } }
        public String PeerGroupCode { get { return m_PeerGroupCode; } set { m_PeerGroupCode = value; } }
        public Boolean FullBenchmark { get { return m_FullBenchmark; } set { m_FullBenchmark = value; } }
        public String Comments { get { return m_Comments; } set { m_Comments = value; } }

        #endregion

        #region Methods

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "allpInsertPeerGroup";
            listPrm = DAC.LoadParameters("@IdPeerGroup", IdPeerGroup, "@IdModel", IdModel, "@PeerGroupDescription", PeerGroupDescription, "@PeerGroupCode", PeerGroupCode, "@FullBenchmark", FullBenchmark, "@Comments", Comments);
            ExecuteProc(sInsert, listPrm);
            m_IdPeerGroup = this.MaxID();
            return m_IdPeerGroup;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "allpUpdatePeerGroup";
            listPrm = DAC.LoadParameters("@IdPeerGroup", IdPeerGroup, "@IdModel", IdModel, "@PeerGroupDescription", PeerGroupDescription, "@PeerGroupCode", PeerGroupCode, "@FullBenchmark", FullBenchmark, "@Comments", Comments);
            ExecuteProc(sUpdate, listPrm);
        }

        public void DeleteAll()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeleteAllPeerGroup";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeletePeerGroup";
            listPrm = LoadParameters("@IdPeerGroup", IdPeerGroup);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdPeerGroup) FROM tblPeerGroup ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT * "
                + " FROM vListPeerGroup "
                + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * "
                + " FROM vListPeerGroup "
                + " WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadComboBox(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT PG.IdModel,PG.IdPeerGroup,PG.PeerGroupCode,PG.PeerGroupDescription,PGF.IdField FROM tblPeerGroupField PGF INNER JOIN tblPeerGroup PG ON PG.IdPeerGroup = PGF.IdPeerGroup " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT PG.IdModel,PG.IdPeerGroup,PG.PeerGroupCode,PG.PeerGroupDescription,PGF.IdField FROM tblPeerGroupField PGF INNER JOIN tblPeerGroup PG ON PG.IdPeerGroup = PGF.IdPeerGroup WHERE " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdPeerGroup =" + m_IdPeerGroup, " ORDER BY PeerGroupDescription");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdPeerGroup = (Int32)dr["IdPeerGroup"];
                m_IdModel = (Int32)dr["IdModel"];
                m_PeerGroupDescription = (String)dr["PeerGroupDescription"];
                m_PeerGroupCode = (String)dr["PeerGroupCode"];
                m_FullBenchmark = (Boolean)dr["FullBenchmark"];
                m_Comments = (String)dr["Comments"];
            }
        }

        #endregion

    }
}
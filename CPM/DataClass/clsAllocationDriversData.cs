﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CD;

namespace DataClass
{
    public class clsAllocationDriversData : CD.DAC
    {

        private Int32 m_IdAllocationDriverData;
        private Int32 m_IdAllocationListDriver;
        private Int32 m_IdClientCostCenter;
        private Int32 m_UserCreation;
        private DateTime m_DateCreation;
        private Object m_UserModification;
        private Object m_DateModification;

        public Int32 IdAllocationDriverData { get { return m_IdAllocationDriverData; } set { m_IdAllocationDriverData = value; } }
        public Int32 IdAllocationListDriver { get { return m_IdAllocationListDriver; } set { m_IdAllocationListDriver = value; } }
        public Int32 IdClientCostCenter { get { return m_IdClientCostCenter; } set { m_IdClientCostCenter = value; } }
        public Int32 UserCreation { get { return m_UserCreation; } set { m_UserCreation = value; } }
        public DateTime DateCreation { get { return m_DateCreation; } set { m_DateCreation = value; } }
        public Object UserModification { get { return m_UserModification; } set { m_UserModification = value; } }
        public Object DateModification { get { return m_DateModification; } set { m_DateModification = value; } }

        public int Insert()
        {
            CD.DAC DAC = new CD.DAC();
            String sInsert;
            List<SqlParameter> listPrm;
            sInsert = "allpInsertAllocationDriverData";
            listPrm = DAC.LoadParameters("@IdAllocationDriverData", IdAllocationDriverData, "@IdAllocationListDriver", IdAllocationListDriver, "@IdClientCostCenter", IdClientCostCenter, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sInsert, listPrm);
            m_IdAllocationDriverData = this.MaxID();
            return m_IdAllocationDriverData;
        }

        public void Update()
        {
            String sUpdate;
            List<SqlParameter> listPrm;
            sUpdate = "allpUpdateAllocationDriverData";
            listPrm = DAC.LoadParameters("@IdAllocationDriverData", IdAllocationDriverData, "@IdAllocationListDriver", IdAllocationListDriver, "@IdClientCostCenter", IdClientCostCenter, "@UserCreation", UserCreation, "@DateCreation", DateCreation, "@UserModification", UserModification, "@DateModification", DateModification);
            ExecuteProc(sUpdate, listPrm);
        }

        public void Delete()
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeleteAllocationDriverData";
            listPrm = LoadParameters("@IdAllocationDriverData", IdAllocationDriverData);
            ExecuteProc(sDelete, listPrm);
        }

        public void DeleteAll(Int32 IdModel)
        {
            String sDelete;
            List<SqlParameter> listPrm;
            sDelete = "allpDeleteAllocationDriverDataAll";
            listPrm = LoadParameters("@IdModel", IdModel);
            ExecuteProc(sDelete, listPrm);
        }

        public Int32 MaxID()
        {
            Int32 intValue = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT MAX(IdAllocationDriverData) FROM vListAllocationDriverByCostCenter ").Tables[0];

            if (dt.Rows.Count > 0)
            {
                intValue = Convert.ToInt32(dt.Rows[0][0]);
            }

            return intValue;
        }

        public DataTable LoadCostCenter(Int32 _IdModel)
        {
            String sConsult;
            List<SqlParameter> listPrm;
            sConsult = "allpListDriverDataCostCenter";
            listPrm = LoadParameters("@IdModel", _IdModel, "@IdAllocationDriverData", IdAllocationDriverData);
            return ConsultProc(sConsult, listPrm).Tables[0];
        }

        public DataTable LoadDriverDataCostCenter(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListAllocationDriverByCostCenterCC " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListAllocationDriverByCostCenterCC WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadComboBox(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty)
            {
                return CD.DAC.ConsultSQL("SELECT IdAllocationDriverData ID, Name FROM vAllocationDriverByCostCenter " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT IdAllocationDriverData ID, Name FROM vListAllocationDriverData WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public DataTable LoadExportList(string strFilter, string strOrder)
        {
            return CD.DAC.ConsultSQL("SELECT Name,TypeOperation FROM vListAllocationDriverByCostCenter WHERE  " + strFilter + " " + strOrder).Tables[0];
        }
        
        public DataTable LoadList(string strFilter, string strOrder)
        {
            if (strFilter == String.Empty) 
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListAllocationDriverByCostCenter " + strOrder).Tables[0];
            }
            else
            {
                return CD.DAC.ConsultSQL("SELECT * FROM vListAllocationDriverByCostCenter WHERE  " + strFilter + " " + strOrder).Tables[0];
            }
        }

        public void loadObject()
        {
            DataTable dt = LoadList("IdAllocationDriverData=" + m_IdAllocationDriverData, "");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                m_IdAllocationDriverData = (Int32)dr["IdAllocationDriverData"];
                m_IdAllocationListDriver = (Int32)dr["IdAllocationListDriver"];
                m_IdClientCostCenter = (Int32)dr["IdClientCostCenter"];
                m_UserCreation = (Int32)dr["UserCreation"];
                m_DateCreation = (DateTime)dr["DateCreation"];
                m_UserModification = (Object)dr["UserModification"];
                m_DateModification = (Object)dr["DateModification"];
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace AppClass
{
    public class clsApplication
    {

        #region '" Enumerator Declaration "'
        #endregion

        #region '" Variable Declaration "'

        private static clsUtility objUtility = new clsUtility();

        #endregion

        #region '" Property Declaration "'

        public String AppVersion { get; set; }
        public String AppDataPath { get; set; }
        public Boolean LicenseValid { get; set; }
        public String IssuedTo { get; set; }
        public String IssuedDate { get; set; }
        public String ExpiryDate { get; set; }
        public Boolean ModuleParameter { get; set; }
        public Boolean ModuleAllocation { get; set; }
        public Boolean ModuleTechnical { get; set; }
        public Boolean ModuleEconomic { get; set; }
        public Boolean ModuleReports { get; set; }
        public Int32 MaxRelationLevels { get; set; }
        public Int32 EconomicProjectionYears { get; set; }
        public List<Int32> AllowedRoles { get; set; }

        #endregion

        #region '" Function Declaration "'

        /// <summary>
        /// Class initialization, main class for storing information about the application
        /// </summary>
        public clsApplication()
        {
            AppVersion = "Version 01.02.0317";
            AppDataPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, "App_Data");
            MaxRelationLevels = 10;
            EconomicProjectionYears = 5;
        }

        /// <summary>
        /// Reads encrypted XML license file generated for the individual application/client instance and sets various environmental property values
        /// </summary>
        public void LoadAppLicense()
        {
            DataSet dsLicense = new DataSet();
            dsLicense = objUtility.XMLtoDS(AppDataPath + @"/license.xml");

            LicenseValid = false;
            ModuleParameter = false;
            ModuleAllocation = false;
            ModuleTechnical = false;
            ModuleEconomic = false;
            ModuleReports = false;

            try
            {
                if (dsLicense.Tables["License"].Rows[0]["LicenseKey"].ToString() == ConfigurationManager.AppSettings["LicenseKey"])
                {
                    LicenseValid = true;
                    IssuedTo = dsLicense.Tables["License"].Rows[0]["IssuedTo"].ToString();
                    IssuedDate = dsLicense.Tables["License"].Rows[0]["IssuedDate"].ToString();
                    ExpiryDate = dsLicense.Tables["License"].Rows[0]["ExpiryDate"].ToString();
                    foreach (DataRow objAttribute in dsLicense.Tables["Attribute"].Rows)
                    {
                        switch (objAttribute["AttributeName"].ToString())
                        {
                            case "ModuleParameter":
                                {
                                    if (objAttribute["AttributeValue"].ToString().ToUpper() == "TRUE")
                                    {
                                        ModuleParameter = true;
                                    }
                                    break;
                                }
                            case "ModuleAllocation":
                                {
                                    if (objAttribute["AttributeValue"].ToString().ToUpper() == "TRUE")
                                    {
                                        ModuleAllocation = true;
                                    }
                                    break;
                                }
                            case "ModuleTechnical":
                                {
                                    if (objAttribute["AttributeValue"].ToString().ToUpper() == "TRUE")
                                    {
                                        ModuleTechnical = true;
                                    }
                                    break;
                                }
                            case "ModuleEconomic":
                                {
                                    if (objAttribute["AttributeValue"].ToString().ToUpper() == "TRUE")
                                    {
                                        ModuleEconomic = true;
                                    }
                                    break;
                                }
                            case "ModuleReports":
                                {
                                    if (objAttribute["AttributeValue"].ToString().ToUpper() == "TRUE")
                                    {
                                        ModuleReports = true;
                                    }
                                    break;
                                }
                            case "MaxRelationLevels":
                                {
                                    MaxRelationLevels = Convert.ToInt32(objAttribute["AttributeValue"]);
                                    break;
                                }
                            default:
                                {
                                    break;
                                }
                        }
                    }
                }
            }
            catch
            {
                LicenseValid = false;
                ModuleParameter = false;
                ModuleAllocation = false;
                ModuleTechnical = false;
                ModuleEconomic = false;
                ModuleReports = false;
            }
        }

        /// <summary>
        /// Streams out the requested embedded resource file to the current HttpContext
        /// </summary>
        /// <param name="CurrentContext">Passed in from the current page requesting the file</param>
        /// <param name="ResourceFileName">Name of the resource file</param>
        /// <param name="ResourceType">Folder where the resource file is stored</param>
        public void StreamFile(HttpContext CurrentContext, String ResourceFileName, String ResourceType)
        {
            CurrentContext.Response.Clear();
            CurrentContext.Response.AddHeader("content-disposition", "attachment; filename=" + ResourceFileName);
            CurrentContext.Response.ContentType = "application/octet-stream";
            MemoryStream outputStream = new MemoryStream();
            objUtility.CopyStream(objUtility.StreamEmbeddedFile(ResourceFileName, ResourceType), outputStream);
            outputStream.WriteTo(CurrentContext.Response.OutputStream);
            CurrentContext.Response.End();
            CurrentContext.Response.Flush();
        }

        /// <summary>
        /// Streams out the requested Byte Array file to the current HttpContext
        /// </summary>
        /// <param name="CurrentContext">Passed in from the current page requesting the file</param>
        /// <param name="FileName">Name of the file</param>
        /// <param name="DatabaseFile">File Byte Array (eg. as stored in SQL Column)</param>
        public void StreamFile(HttpContext CurrentContext, String FileName, Byte[] DatabaseFile)
        {
            CurrentContext.Response.Clear();
            CurrentContext.Response.AddHeader("content-disposition", "attachment; filename=" + FileName);
            CurrentContext.Response.ContentType = "application/octet-stream";
            MemoryStream outputStream = new MemoryStream(DatabaseFile);
            outputStream.WriteTo(CurrentContext.Response.OutputStream);
            CurrentContext.Response.Flush();
        }

        /// <summary>
        /// Reads an embedded resource file into a string (eg. text files). Files must exist in the AppClass folder
        /// </summary>
        /// <param name="ResourceFileName">File name, including extension, of the resource to read</param>
        /// <param name="ResourceType">Folder in AppClass where the embedded resource is stored</param>
        /// <returns>String read from the embedded resource</returns>
        public String ReadFile(String ResourceFileName, String ResourceType)
        {
            return objUtility.ReadEmbededFile(ResourceFileName, ResourceType);
        }

        #endregion

    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace AppClass
{
    public class clsUtility
    {

        #region '" Enumerator Declaration "'
        #endregion

        #region '" Variable Declaration "'

        private Int32 intPasswordIteration = 5;
        private Int32 intKeySize = 256;
        private String strHashAlgorithm = "SHA1";
        private String strPassPhrase = "{2F681734-0095-4528-B0A3-781B175BB3C3}";
        private String strSaltValue = "{B26921B3-514D-467D-8AE6-A57F8748CD23}";
        private String strInitialVector = "03DB452ED39345EF";

        #endregion

        #region '" Property Declaration "'
        #endregion

        #region '" Function Declaration "'

        /// <summary>
        /// Class initialization, class used for running common operations to simplify code in other areas
        /// </summary>
        public clsUtility()
        {
            // Do nothing
        }

        /// <summary>
        /// Reads an embedded resource file into a string (eg. text files). Files must exist in the AppClass Template folder
        /// </summary>
        /// <param name="Filename">File name, including extension, of the resource to read</param>
        /// <param name="FileType">Folder in AppClass where the embedded resource is stored</param>
        /// <returns>String read from the embedded resource</returns>
        public String ReadEmbededFile(String Filename, String FileType)
        {
            String readEmbededFileReturn = null;
            try
            {
                Stream objFile = StreamEmbeddedFile(Filename, FileType);
                System.IO.StreamReader objStreamReader = null;
                objStreamReader = new System.IO.StreamReader(objFile, System.Text.Encoding.Default);
                readEmbededFileReturn = objStreamReader.ReadToEnd();
                objFile = null;
                objStreamReader = null;
            }
            catch
            {
                readEmbededFileReturn = null;
            }
            finally
            {
                // Do nothing
            }
            return readEmbededFileReturn;
        }

        /// <summary>
        /// Reads an embedded resource file into a stream. Files must exist in the AppClass Project
        /// </summary>
        /// <param name="Filename">File name, including extension, of the resource to stream</param>
        /// <param name="FileType">Folder in AppClass where the embedded resource is stored</param>
        /// <returns>Output stream of embedded resource</returns>
        public Stream StreamEmbeddedFile(String Filename, String FileType)
        {
            Stream streamEmbededFileReturn = null;
            try
            {
                System.Reflection.Assembly objAssembly = null;
                objAssembly = System.Reflection.Assembly.GetExecutingAssembly();
                streamEmbededFileReturn = objAssembly.GetManifestResourceStream("AppClass." + FileType + "." + Filename);
            }
            catch
            {
                streamEmbededFileReturn = null;
            }
            finally
            {
                // Do nothing
            }
            return streamEmbededFileReturn;
        }

        /// <summary>
        /// Takes an encrypted XML data file and converts it into a DataSet object for reading
        /// </summary>
        /// <param name="FileNamePath">The path where the encrypted XML data file is stored</param>
        /// <returns></returns>
        public DataSet XMLtoDS(String FileNamePath)
        {
            System.Data.DataSet dsXMLtoDSReturn = null;
            try
            {
                MemoryStream objSourceStream = null;
                objSourceStream = DecryptFile(FileNamePath);
                dsXMLtoDSReturn = new DataSet();
                dsXMLtoDSReturn.ReadXml(objSourceStream);
            }
            catch
            {
                dsXMLtoDSReturn = new DataSet();
            }
            return dsXMLtoDSReturn;
        }

        /// <summary>
        /// Decrypts an encrypted file into a memory stream
        /// </summary>
        /// <param name="FileNamePath">The path where the encrypted XML data file is stored</param>
        /// <returns></returns>
        public MemoryStream DecryptFile(String FileNamePath)
        {
            MemoryStream readFileReturn = null;
            try
            {
                String strEncString = "";
                String strXMLData = "";
                StreamReader objStreamReader = null;
                objStreamReader = File.OpenText(FileNamePath);
                strEncString = objStreamReader.ReadToEnd();
                objStreamReader.Close();
                strXMLData = Decrypt(strEncString, strPassPhrase, strSaltValue, strHashAlgorithm, intPasswordIteration, strInitialVector, intKeySize);
                byte[] byteArray = Encoding.UTF8.GetBytes(strXMLData);
                readFileReturn = new MemoryStream(byteArray);
            }
            catch
            {
                readFileReturn = null;
            }
            return readFileReturn;
        }

        /// <summary>
        /// Takes an encrypted string and outputs the decrypted value
        /// </summary>
        /// <param name="cipherText">Encryted string to decrypt</param>
        /// <param name="passPhrase">Decryption Passphrase</param>
        /// <param name="saltValue">Decryption Salt Value</param>
        /// <param name="hashAlgorithm">Hash Algorithm</param>
        /// <param name="passwordIterations">Number of password iterations</param>
        /// <param name="initVector">Initialization Vector</param>
        /// <param name="keySize">Key Size</param>
        /// <returns>Decrypted string value</returns>
        public String Decrypt(String cipherText, String passPhrase, String saltValue, String hashAlgorithm, Int32 passwordIterations, String initVector, Int32 keySize)
        {
            String strDecryptReturn = "";
            try
            {
                Byte[] initVectorBytes = null;
                initVectorBytes = Encoding.ASCII.GetBytes(initVector);
                Byte[] saltValueBytes = null;
                saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
                Byte[] cipherTextBytes = null;
                cipherTextBytes = Convert.FromBase64String(cipherText);
                PasswordDeriveBytes password = null;
                password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
                Byte[] keyBytes = null;
                keyBytes = password.GetBytes(Convert.ToInt32(keySize / 8));
                RijndaelManaged symmetricKey = null;
                symmetricKey = new RijndaelManaged();
                symmetricKey.Mode = CipherMode.CBC;
                ICryptoTransform decryptor = null;
                decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
                MemoryStream memoryStream = null;
                memoryStream = new MemoryStream(cipherTextBytes);
                CryptoStream cryptoStream = null;
                cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
                Byte[] plainTextBytes = null;
                plainTextBytes = new Byte[cipherTextBytes.Length];
                Int32 decryptedByteCount = 0;
                decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                cryptoStream.Close();
                memoryStream.Close();
                String plainText = null;
                plainText = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                strDecryptReturn = plainText;
            }
            catch
            {
                strDecryptReturn = null;
            }
            return strDecryptReturn;
        }

        /// <summary>
        /// Copies one stream into another to allow conversion between stream types eg. Stream to MemoryStream
        /// </summary>
        /// <param name="InputStream">Input Stream</param>
        /// <param name="OutputStream">Output Stream</param>
        public void CopyStream(Stream InputStream, Stream OutputStream)
        {
            byte[] buffer = new byte[16*1024];
            int read;

            try
            {
                while ((read = InputStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    OutputStream.Write(buffer, 0, read);
                }
            }
            catch
            {
                // Do nothing
            }
        }

        #endregion

    }
}
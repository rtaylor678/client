﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CD
{
    public class UserLog
    {

        #region '" Enumerator Declaration "'

        public enum UserActivity
        {
            Unknown = 0,
            LoginSuccess = 1,
            LoginFailed = 2,
            PasswordResetRequested = 3,
            PasswordResetCompleted = 4,
            CaseCreated = 5,
            CaseDeleted = 6,
            CaseEdited = 7,
            CaseOpened = 8
        }

        #endregion

    }
}

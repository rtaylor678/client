﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Security.Cryptography;
using System.IO;
using System.Dynamic;
using System.Configuration;
using System.Globalization;

namespace CD
{
    public class DAC
    {
        
        private String m_DB;
        private String m_Password;
        private String m_Server;
        private String m_User;
        private String m_StrCon;

        private void LoadAppConfig()
        {
            this.m_Server = ConfigurationManager.AppSettings["Server"];
            this.m_User = ConfigurationManager.AppSettings["User"];
            this.m_Password = ConfigurationManager.AppSettings["Password"];
            this.m_DB = ConfigurationManager.AppSettings["DB"];
        }

        private void LoadStrConfig()
        {
            this.m_StrCon = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        }

        private SqlConnection Conect()
        { 
            this.LoadStrConfig();
            String scon = m_StrCon;
            SqlConnection cn = new SqlConnection(scon);
            cn.Open();
            return cn;
        }

        private void CloseConection(SqlConnection cn)
        {
            if (cn.State != ConnectionState.Closed) 
            {
                cn.Close();
            }
        }

        private SqlTransaction StartTransaction()
        { 
            return this.Conect().BeginTransaction();
        }

        private SqlTransaction StartTransaction(IsolationLevel isol)
        { 
            return this.Conect().BeginTransaction(isol);
        }

        private void AbortTransaction(SqlTransaction trn)
        {
            try
            {
                SqlConnection cn = trn.Connection;
                trn.Rollback();
                this.CloseConection(cn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void EndTransaction(SqlTransaction trn)
        {
            try
            {
                SqlConnection cn = trn.Connection;
                trn.Commit();
                this.CloseConection(cn);
            }
            catch (Exception ex)
            {
                this.AbortTransaction(trn);
                throw ex;
            }
        }

        public static DataSet ConsultSQL(String sQuery)
        {
            DAC objDac = new DAC();
            return objDac.Consult(sQuery);
        }

        protected DataSet Consult(String sQuery)
        {
            DataSet ds;
            SqlTransaction trn = this.StartTransaction();
            try 
            {
                ds = this.Consult(sQuery, trn);
                this.EndTransaction(trn);
            }
            catch (Exception ex)
            {
                this.AbortTransaction(trn);
                throw ex;
            }
            return ds;
        }

        protected DataSet Consult(String sQuery, SqlTransaction trn)
        {
            SqlDataAdapter ada = new SqlDataAdapter(sQuery, trn.Connection);
            ada.SelectCommand.Transaction = trn;
            ada.SelectCommand.CommandTimeout = 3600000;
            DataSet ds = new DataSet();
            ada.Fill(ds);
            return ds;
        }

        protected DataSet ConsultProc(string NameProc, List<SqlParameter> listPrm)
        {
            DataSet ds;
            SqlTransaction trn = this.StartTransaction();
            try
            {
                ds = this.ConsultProc(NameProc, listPrm, trn);
                this.EndTransaction(trn);

            }
            catch (Exception ex)
            {
                this.AbortTransaction(trn);
                throw ex;
            }
            return ds;
        }

        protected DataSet ConsultProc(string NameProc, List<SqlParameter> listPrm, SqlTransaction trn)
        {
            DataSet ds =  new DataSet();

            SqlDataAdapter ada = new SqlDataAdapter(NameProc, trn.Connection);
            ada.SelectCommand.Transaction = trn;
            this.LoadParameters(ada.SelectCommand, listPrm);
            ada.SelectCommand.CommandType = CommandType.StoredProcedure;
            ada.SelectCommand.CommandTimeout = 3600000;
            ada.Fill(ds);

            return ds;
        }

        private Object MaxID(String sTabla, SqlTransaction trn)
        {
            Object Obj = new Object();
            try
            {
                SqlCommand cmd = new SqlCommand("MaxID", trn.Connection);
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Tabla", sTabla));
                Obj = cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Obj;
        }

        public static List<SqlParameter> LoadParameters(params Object[] ListObj)
        {
            List<SqlParameter> listPrm = new List<SqlParameter>();
            SqlParameter prm;

            Int32 count = ListObj.Length / 2;
            Int32 c = 0;
            for (int i = 0; i < count; i++)
            { 
                prm = new SqlParameter(Convert.ToString(ListObj[c]), ((ListObj[c + 1])==null)? DBNull.Value: ListObj[c + 1]);
                listPrm.Add(prm);
                c += 2;
            }
            return listPrm;
        }

        protected void LoadParameters(SqlCommand cmd, List<SqlParameter> listPrm)
        {
            List<SqlParameter> list = new List<SqlParameter>();
            list = listPrm;
            cmd.Parameters.AddRange(list.ToArray<SqlParameter>());      
        }

        protected DateTime ReturnDate()
        { 
            String sQuery  = "SELECT GetDate()";
            return Convert.ToDateTime(this.ReturnValue(sQuery));
        }

        public static DateTime ReturnDateServer()
        {
           DAC objDac = new DAC();
           return objDac.ReturnDate();
        }

        protected Object ReturnValue(String sQuery)
        { 
            SqlTransaction trn  = this.StartTransaction();
            Object obj = new Object();
            try
            {
                obj = this.ReturnValue(sQuery, trn);
                this.EndTransaction(trn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return obj;
        }

        protected Object ReturnValue(String sQuery, SqlTransaction trn)
        {
            Object obj = new Object();
            try
            {
                SqlCommand cmd = new SqlCommand(sQuery, trn.Connection);
                cmd.Transaction = trn;
                obj = cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return obj;
        }

        protected Int32 Execute(String sDML)
        { 
            SqlTransaction trn = this.StartTransaction();
            Int32 control;
            try
            {
                control = this.Execute(sDML, trn);
                if (control > 0)
                {
                    this.EndTransaction(trn);
                }
                else
                {
                    this.AbortTransaction(trn);
                }
            }
            catch (Exception ex)
            {
                this.AbortTransaction(trn);
                throw ex;
            }
            return control;
        }

        protected Int32 Execute(String sDML, SqlTransaction trn)
        {
            SqlCommand cmd = new SqlCommand(sDML, trn.Connection);
            cmd.Transaction = trn;
            return cmd.ExecuteNonQuery();
        }

        protected Int32 ExecuteInsertProc(String NameProc, List<SqlParameter> listPrm, SqlTransaction trn)
        {
            SqlCommand cmd = new SqlCommand(NameProc, trn.Connection);
            try
            {
                cmd.Transaction = trn;
                this.LoadParameters(cmd, listPrm);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters[0].Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return (Int32)cmd.Parameters[0].Value;
        }

        protected Int32 ExecuteInsertProc(String NameProc, List<SqlParameter> listPrm)
        {
            SqlTransaction trn = this.StartTransaction();
            SqlCommand cmd = new SqlCommand(NameProc, trn.Connection);
            try
            {
                cmd.Transaction = trn;
                this.LoadParameters(cmd, listPrm);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters[0].Direction = ParameterDirection.Output;

                if (cmd.ExecuteNonQuery() > 0)
                {
                    this.EndTransaction(trn);
                }
                else
                {
                    this.AbortTransaction(trn);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return (Int32)cmd.Parameters[0].Value;
        }

        public static Int32 ExecuteProcedure(String NameProc, List<SqlParameter> listPrm)
        {
            DAC objDac = new DAC();
            return objDac.ExecuteProc(NameProc, listPrm);
        }

        protected Int32 ExecuteProc(String NameProc, List<SqlParameter> listPrm)
        {
            SqlTransaction trn = this.StartTransaction();
            try
            {
                Int32 control = this.ExecuteProc(NameProc, listPrm, trn);

                if (control > 0)
                {
                    this.EndTransaction(trn);
                }
                else
                {
                    this.AbortTransaction(trn);
                }
                return control;
            }
            catch (Exception ex)
            {
                this.AbortTransaction(trn);
                throw ex;
            }
        }

        protected Int32 ExecuteProc(String NameProc, List<SqlParameter> listPrm, SqlTransaction trn)
        {
            SqlCommand cmd = new SqlCommand(NameProc, trn.Connection);
            cmd.Transaction = trn;
            this.LoadParameters(cmd, listPrm);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 3600000;
            return cmd.ExecuteNonQuery();
        }

        protected DataTable ExecuteReturn(String NameProc, List<SqlParameter> listPrm)
        {
            SqlTransaction trn = this.StartTransaction();
            try
            {
                DataTable control = this.ExecuteReturn(NameProc, listPrm, trn);

                if (control.Rows.Count > 0)
                {
                    this.EndTransaction(trn);
                }
                else
                {
                    this.AbortTransaction(trn);
                }
                return control;
            }
            catch (Exception ex)
            {
                this.AbortTransaction(trn);
                throw ex;
            }
        }

        protected DataTable ExecuteReturn(String NameProc, List<SqlParameter> listPrm, SqlTransaction trn)
        {
            SqlCommand cmd = new SqlCommand(NameProc, trn.Connection);
            cmd.Transaction = trn;
            this.LoadParameters(cmd, listPrm);
            cmd.CommandType = CommandType.StoredProcedure;
            return DataReaderToDataTable(cmd.ExecuteReader());
        }

        protected DataTable DataReaderToDataTable(System.Data.Common.DbDataReader DataReader)
        {
            DataTable dtDataTable = new DataTable();
            do
            {
                DataTable dtSchemaTable = DataReader.GetSchemaTable();
                if (!(dtSchemaTable == null))
                {
                    for (int i = 0; i < dtSchemaTable.Rows.Count; i++)
                    {
                        DataRow drRow = dtSchemaTable.Rows[i];
                        string strColumn = (string)drRow["ColumnName"];
                        DataColumn dcColumn = new DataColumn(strColumn, (Type)drRow["DataType"]);
                        dtDataTable.Columns.Add(dcColumn);
                    }
                    while (DataReader.Read())
                    {
                        DataRow drRow = dtDataTable.NewRow();
                        for (int i = 0; i < DataReader.FieldCount; i++)
                        {
                            drRow[i] = DataReader.GetValue(i);
                        }
                        dtDataTable.Rows.Add(drRow);
                    }
                }
                else
                {
                    DataColumn dcColumn = new DataColumn("RowsAffected");
                    dtDataTable.Columns.Add(dcColumn);
                    DataRow drRow = dtDataTable.NewRow();
                    drRow[0] = DataReader.RecordsAffected;
                    dtDataTable.Rows.Add(drRow);
                }
            }
            while (DataReader.NextResult());
            DataReader.Close();
            DataReader.Dispose();
            DataReader = null;
            return dtDataTable;
        }

        public static string Encrypt(string plainText)
        {
            String strSaltValue = ConfigurationManager.AppSettings["SaltValue"];
            try
            {
                byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

                byte[] keyBytes = new Rfc2898DeriveBytes("P@@Sw0rd", Encoding.ASCII.GetBytes("S@LT&KEY")).GetBytes(256 / 8);
                var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
                var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(strSaltValue));

                byte[] cipherTextBytes;

                using (var memoryStream = new MemoryStream())
                {
                    using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                        cryptoStream.FlushFinalBlock();
                        cipherTextBytes = memoryStream.ToArray();
                        cryptoStream.Close();
                    }
                    memoryStream.Close();
                }
                return Convert.ToBase64String(cipherTextBytes);
            }
            catch
            {
                return null;
            }
        }

        public static string Decrypt(string encryptedText)
        {
            String strSaltValue = ConfigurationManager.AppSettings["SaltValue"];
            try
            {
                byte[] cipherTextBytes = Convert.FromBase64String(encryptedText);
                byte[] keyBytes = new Rfc2898DeriveBytes("P@@Sw0rd", Encoding.ASCII.GetBytes("S@LT&KEY")).GetBytes(256 / 8);
                var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

                var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(strSaltValue));
                var memoryStream = new MemoryStream(cipherTextBytes);
                var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
                byte[] plainTextBytes = new byte[cipherTextBytes.Length];

                int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                memoryStream.Close();
                cryptoStream.Close();
                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
            }
            catch
            {
                return null;
            }
        }

        public string GetJSON(DataTable dt)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            try
            {
                if (dt.Rows.Count > 0)
                {
                    Hashtable ht = new Hashtable();
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        ht.Add(i, dt.Columns[i].ColumnName);
                    }
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        sb.Append("{");
                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            sb.Append(string.Format("\"{0}\":\"{1}\",",
                            ht[j], dt.Rows[i][j].ToString()));
                        }
                        sb.Remove(sb.ToString().LastIndexOf(","), 1);
                        sb.Append("},");
                    }
                    sb.Remove(sb.ToString().LastIndexOf(","), 1);
                    ht.Clear();
                    ht = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ///sb.Append("]}");
                sb.Append("]");
            }
            return sb.ToString();
        }

    }
}

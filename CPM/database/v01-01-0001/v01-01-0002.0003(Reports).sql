/****** Object:  StoredProcedure [dbo].[reppOpexProjection]    Script Date: 03/23/2016 10:26:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[reppOpexProjection]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[reppOpexProjection]
GO

/****** Object:  StoredProcedure [dbo].[reppOpexProjection]    Script Date: 11/30/2015 13:35:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:		Gareth Slater
-- Create date: 2014-05-05
-- Description:	Module Report Opex Projection
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 6/10/2015	RM		Added @Factor parameter
-- 8/5/2015		RM		Cleaned up unnecessary code in stored procedure
-- 11/27/2005	RM		Added export table output, chart table output for cases and volumes
-- ===========================================================================================
/****** Object:  StoredProcedure [dbo].[reppOpexProjection]    Script Date: 02/18/2016 09:08:25 ******/
CREATE PROCEDURE [dbo].[reppOpexProjection](
@IdModel INT,
@IdAggregationLevel INT,
@IdField NVARCHAR(MAX),
@IdStructure INT,
@From INT,
@To INT,
@IdTechnicalScenario INT,
@IdEconomicScenario INT,
@IdCurrency INT,
@IdTerm INT,
@IdTechnicalDriver INT,
@IdCostType INT,
@IdTypeOperation INT,
@Factor INT
)
AS
BEGIN

	BEGIN /** DROP Temporary Tables **/
	IF OBJECT_ID('tempdb..#FieldList') IS NOT NULL DROP TABLE #FieldList
	IF OBJECT_ID('tempdb..#AccountList') IS NOT NULL DROP TABLE #AccountList
	IF OBJECT_ID('tempdb..#TechDriver') IS NOT NULL DROP TABLE #TechDriver
	IF OBJECT_ID('tempdb..#Opex') IS NOT NULL DROP TABLE #Opex
	IF OBJECT_ID('tempdb..#BL') IS NOT NULL DROP TABLE #BL
	IF OBJECT_ID('tempdb..#BO') IS NOT NULL DROP TABLE #BO
	IF OBJECT_ID('tempdb..#Chart') IS NOT NULL DROP TABLE #Chart
	IF OBJECT_ID('tempdb..#ChartList') IS NOT NULL DROP TABLE #ChartList
	IF OBJECT_ID('tempdb..#VolumeReportBLRows') IS NOT NULL DROP TABLE #VolumeReportBLRows  
	IF OBJECT_ID('tempdb..#VolumeReportBL') IS NOT NULL DROP TABLE #VolumeReportBL  
	IF OBJECT_ID('tempdb..#VolumeReportBORows') IS NOT NULL DROP TABLE #VolumeReportBORows
	IF OBJECT_ID('tempdb..#VolumeReportBO') IS NOT NULL DROP TABLE #VolumeReportBO
	IF OBJECT_ID('tempdb..#VolumeReportBLList') IS NOT NULL DROP TABLE #VolumeReportBLList 
	IF OBJECT_ID('tempdb..#VolumeReportBOList') IS NOT NULL DROP TABLE #VolumeReportBOList 
	IF OBJECT_ID('tempdb..#VolumeReportGrouped') IS NOT NULL DROP TABLE #VolumeReportGrouped
	IF OBJECT_ID('tempdb..#VolumeReportGroupedBL') IS NOT NULL DROP TABLE #VolumeReportGroupedBL 
	IF OBJECT_ID('tempdb..#VolumeReportGroupedBO') IS NOT NULL DROP TABLE #VolumeReportGroupedBO 
	IF OBJECT_ID('tempdb..#ChartCost') IS NOT NULL DROP TABLE #ChartCost
	IF OBJECT_ID('tempdb..#ChartListCost') IS NOT NULL DROP TABLE #ChartListCost
	IF OBJECT_ID('tempdb..#ChartVolume') IS NOT NULL DROP TABLE #ChartVolume
	IF OBJECT_ID('tempdb..#ChartListVolume') IS NOT NULL DROP TABLE #ChartListVolume
	IF OBJECT_ID('tempdb..#ExportOpex') IS NOT NULL DROP TABLE #ExportOpex
	IF OBJECT_ID('tempdb..#ExportBL') IS NOT NULL DROP TABLE #ExportBL
	IF OBJECT_ID('tempdb..#ExportBO') IS NOT NULL DROP TABLE #ExportBO	
	END
	
	BEGIN /** Build List Tables for reports **/
	/** Technical Scenario List **/
	DECLARE @CaseTypeList NVARCHAR(MAX) = 'Baseline, Incremental' 

	DECLARE @StartYear INT = @From;
	DECLARE @EndYear INT = @To;
	
	/** Get Base Year **/
	DECLARE @BaseYear INT
	SELECT @BaseYear = [BaseYear] FROM [tblModels] WHERE IdModel=@IdModel

	/** Build Year Columns **/
	DECLARE @YearColumnList NVARCHAR(MAX)='';	
	DECLARE @YearFrom INT
	SET @YearFrom=@From	
	WHILE (@YearFrom <= @To)
	BEGIN
		SET @YearColumnList =  @YearColumnList + '[' + CAST(@YearFrom AS VARCHAR(4)) + ']';
		SET @YearFrom = @YearFrom + 1;
		IF (@YearFrom <= @To)
		BEGIN
			SET @YearColumnList = @YearColumnList + ',';
		END
	END
	
	/** Build Field List **/
	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF (@IdAggregationLevel=0 AND @IdField=0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0 AND @IdField>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdField>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	DECLARE @FieldCount INT;
	SELECT @FieldCount=COUNT([IdField]) FROM #FieldList
	
	/** Get Chart Columns to Temp Table **/
	CREATE TABLE #AccountList (ColumnName NVARCHAR(255) PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF @IdStructure=1
	BEGIN
		INSERT INTO #AccountList SELECT [RootZiffAccount] FROM tblCalcProjectionTechnical WHERE IdField IN (SELECT [IdField] FROM #FieldList) AND [IdModel]=@IdModel GROUP BY [RootZiffAccount] ORDER BY [RootZiffAccount];
	END
	ELSE IF @IdStructure=2
	BEGIN
		INSERT INTO #AccountList SELECT [Activity] FROM tblCalcProjectionTechnical WHERE IdField IN (SELECT [IdField] FROM #FieldList) AND [IdModel]=@IdModel GROUP BY [Activity], [SortOrder] ORDER BY [SortOrder], [Activity];
	END
	
	/** Generate Chart Columns from Temp Table **/
	DECLARE @AccountColumnList NVARCHAR(MAX)='';
	DECLARE @AccountName NVARCHAR(255);
	DECLARE AccountList CURSOR FOR
	SELECT ColumnName FROM #AccountList;

	OPEN AccountList;
	FETCH NEXT FROM AccountList INTO @AccountName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @AccountColumnList=''
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + '[' + @AccountName + ']';
		END
		ELSE
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + ',[' + @AccountName + ']';
		END
		FETCH NEXT FROM AccountList INTO @AccountName;
	END

	CLOSE AccountList;
	DEALLOCATE AccountList;
	
	/** Get Currency Factor to use **/
	DECLARE @CurrencyFactor FLOAT;
	SELECT @CurrencyFactor=[Value] FROM tblModelsCurrencies WHERE [IdCurrency]=@IdCurrency AND [IdModel]=@IdModel;
	
	/** Build Technical Driver Factors (used for KPI Report) **/
	CREATE TABLE #TechDriver ([Year] INT, [Normal] FLOAT, [Optimistic] FLOAT, [Pessimistic] FLOAT) ON [PRIMARY]
	IF @IdTechnicalDriver=0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, 1 AS [Normal], 1 AS [Optimistic], 1 AS [Pessimistic]
		FROM tblCalcModelYears
		WHERE [IdModel]=@IdModel AND [Year] BETWEEN @StartYear AND @EndYear;
	END
	ELSE IF @IdTechnicalDriver>0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, SUM([Normal]) AS [Normal], SUM([Pessimistic]) AS [Pessimistic], SUM([Optimistic]) AS [Optimistic]
		FROM tblTechnicalDriverData
		WHERE [IdTechnicalDriver]=@IdTechnicalDriver AND [IdModel]=@IdModel AND IdField IN (SELECT [IdField] FROM #FieldList)
		GROUP BY Year;
	END
	END /** Build List Tables for reports **/
	
	BEGIN /** Build Volume Tables **/
	/** Report (Baseline)**/
	CREATE TABLE #VolumeReportBLRows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLRows ([ParentName], [Name], [YearProjection], [Amount])	
	SELECT	DISTINCT N'All Cost Accounts' AS ParentName, dbo.tblTechnicalDrivers.Name AS Name,
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Normal,0))
				WHEN @IdTechnicalScenario=2 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Optimistic,0))
				WHEN @IdTechnicalScenario=3 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0))
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject AND
			dbo.tblTechnicalDriverData.Year >= dbo.tblProjects.Starts 
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=1
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	GROUP BY  dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.Name
	ORDER BY dbo.tblTechnicalDriverData.Year
	
	CREATE TABLE #VolumeReportBL ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBL([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBLRows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBLList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBL WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #VolumeReportBLList to Fill Properly **/
	INSERT INTO #VolumeReportBL ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBLList.ParentName,#VolumeReportBLList.Name, #VolumeReportBLList.YearProjection,0 AS Amount
	FROM	#VolumeReportBLList LEFT OUTER JOIN
			#VolumeReportBL ON #VolumeReportBLList.ParentName=#VolumeReportBL.ParentName AND #VolumeReportBLList.Name=#VolumeReportBL.Name AND #VolumeReportBLList.YearProjection=#VolumeReportBL.YearProjection
	WHERE	#VolumeReportBL.YearProjection IS NULL;

	/** Delete all rows that have 0 (zero) Amount **/
	DELETE #VolumeReportBL WHERE Amount=0

	/** Report (Business Opportunities)**/
	CREATE TABLE #VolumeReportBORows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBORows ([ParentName], [Name], [YearProjection], [Amount])	
	SELECT	DISTINCT N'All Cost Accounts' AS ParentName, dbo.tblTechnicalDrivers.Name AS Name,
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Normal,0))
				WHEN @IdTechnicalScenario=2 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Optimistic,0))
				WHEN @IdTechnicalScenario=3 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0))
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject AND
			dbo.tblTechnicalDriverData.Year >= dbo.tblProjects.Starts
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=2
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	GROUP BY  dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.Name
	ORDER BY dbo.tblTechnicalDriverData.Year

	CREATE TABLE #VolumeReportBO ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBO([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBORows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBOList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBOList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBO WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #VolumeReportBOList to Fill Properly **/
	INSERT INTO #VolumeReportBO ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBOList.ParentName,#VolumeReportBOList.Name, #VolumeReportBOList.YearProjection,0 AS Amount
	FROM	#VolumeReportBOList LEFT OUTER JOIN
			#VolumeReportBO ON #VolumeReportBOList.ParentName=#VolumeReportBO.ParentName AND #VolumeReportBOList.Name=#VolumeReportBO.Name AND #VolumeReportBOList.YearProjection=#VolumeReportBO.YearProjection
	WHERE	#VolumeReportBO.YearProjection IS NULL;

	/** Delete all rows that have 0 (zero) Amount **/
	DELETE #VolumeReportBO WHERE Amount=0

	/** Report (All Fields Grouped)**/
	CREATE TABLE #VolumeReportGroupedBL ([YearProjection] INT, [Amount] FLOAT, [ParentName] NVARCHAR(255), [Name] NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBL ([YearProjection], [Amount], [ParentName], [Name])
	SELECT YearProjection,SUM(amount) AS Amount,'GroupedBL',Name-- 'Baseline'
	FROM #VolumeReportBL
	GROUP BY YearProjection,Name
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGroupedBO ([YearProjection] INT, [Amount] FLOAT, [ParentName] NVARCHAR(255), [Name] NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBO ([YearProjection], [Amount], [ParentName], [Name])
	SELECT YearProjection,SUM(amount) AS Amount,'GroupedBO',Name-- 'Incremental'
	FROM #VolumeReportBO
	GROUP BY YearProjection,Name
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGrouped ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255),[YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGrouped ([ParentName], [Name], [YearProjection], [Amount])
	SELECT N'All Cost Accounts' AS ParentName,Name,YearProjection,SUM(Amount) AS 'Amount'
	FROM
	(
		SELECT 'VolumeReportGroupedBL' AS "ProjectType", ParentName, Name, YearProjection,SUM(Amount) AS "Amount"
		FROM #VolumeReportGroupedBL 
		GROUP BY ParentName,Name,YearProjection
		UNION
		SELECT 'VolumeReportGroupedBO', ParentName, Name, YearProjection, SUM(Amount) AS "Amount"
		FROM #VolumeReportGroupedBO 
		GROUP BY ParentName,Name,YearProjection
	) AS VolumeReportGroupedTotals
	GROUP BY Name,YearProjection
	ORDER BY Name,YearProjection
	END  /** Build Volume Tables **/

	DECLARE @TotalCapacityDriver INT = 0;

	/** Get grouped driver value for Base Year **/
	SELECT @TotalCapacityDriver = Amount FROM #VolumeReportGrouped WHERE YearProjection=@BaseYear

	BEGIN /** Build Source Data Table - Total **/
	CREATE TABLE #Opex (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #Opex ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT 'Opex' AS ParentName, 
		CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS Name, 
		tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList)
	GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, tblCalcProjectionTechnical.Year;
	END /** Build Source Data Table - Total **/
	
	BEGIN /** Build Source Data Table - Chart **/
	CREATE TABLE #Chart (ParentName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #Chart ([ParentName],[YearProjection],[CostProjection])
	SELECT CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS ParentName, 
		tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, tblCalcProjectionTechnical.Year;
	
	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #ChartList (ParentName NVARCHAR(255), YearProjection INT) ON [PRIMARY]
	INSERT INTO #ChartList ([ParentName],[YearProjection])
	SELECT CNList.ParentName, CYList.Year FROM
		(SELECT [ParentName] FROM #Chart GROUP BY [ParentName])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList
	WHERE CYList.Year BETWEEN @StartYear AND @EndYear
	
	/** Add Any Missing Years to Make the Chart Fill Properly (eg. Fields with only Cyclical Drivers) **/
	INSERT INTO #Chart ([ParentName],[YearProjection],[CostProjection])
	SELECT #ChartList.ParentName, #ChartList.YearProjection, NULL AS CostProjection FROM
		#ChartList LEFT OUTER JOIN
		#Chart ON #ChartList.ParentName=#Chart.ParentName AND #ChartList.YearProjection=#Chart.YearProjection
	WHERE #Chart.YearProjection IS NULL;	
	END /** Build Source Data Table - Chart **/
	
	BEGIN /** Build BL and BO Tables**/
	IF @IdTechnicalDriver=0
	BEGIN
		/** Build Source Data Table - Baseline and Business Opportunities (No Techical KPI Required) **/
		CREATE TABLE #BL (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
		CREATE TABLE #BO (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
		IF @FieldCount = 1
		BEGIN
			/** Baseline **/
			INSERT INTO #BL ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'Baseline' AS ParentName, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=1 AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, tblCalcProjectionTechnical.Year;
			
			/** Business Opportunities **/
			INSERT INTO #BO ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'Incremental Line' AS ParentName, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=2 AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, tblCalcProjectionTechnical.Year;
		END
		ELSE IF @FieldCount <> 1
		BEGIN
			/** Baseline **/
			INSERT INTO #BL ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'Baseline' AS ParentName, --Project AS Name, 
				CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=1 AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END/**tblCalcProjectionTechnical.Project**/, tblCalcProjectionTechnical.Year;
			
			/** Business Opportunities **/
			INSERT INTO #BO ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'Incremental Line' AS ParentName, --Project AS Name, 
				CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS Name,
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=2 AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END/**tblCalcProjectionTechnical.Project**/, tblCalcProjectionTechnical.Year;
		END
	END
	END /** Build BL and BO Tables**/
	
	BEGIN /** Build Source Data Table - Chart Cost **/
	CREATE TABLE #ChartCost (ParentName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	/** Add Baseline Totals **/
	INSERT INTO #ChartCost ([ParentName],[YearProjection],[CostProjection])
	SELECT 'Baseline', YearProjection, SUM(CostProjection)
	FROM #BL
	GROUP BY YearProjection
	/** Add Incremental Totals **/
	INSERT INTO #ChartCost ([ParentName],[YearProjection],[CostProjection])
	SELECT 'Incremental', YearProjection, SUM(CostProjection)
	FROM #BO
	GROUP BY YearProjection
 
	/** Get Full List of Years and Names **/
	CREATE TABLE #ChartListCost (ParentName NVARCHAR(255), YearProjection INT) ON [PRIMARY]
	INSERT INTO #ChartListCost ([ParentName],[YearProjection])
	SELECT CNList.ParentName, CYList.Year FROM
		(SELECT [ParentName] FROM #ChartCost GROUP BY [ParentName])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList
	WHERE CYList.Year BETWEEN @StartYear AND @EndYear

	/** Add Any Missing Years to Make the Chart Fill Properly **/
	INSERT INTO #ChartCost ([ParentName],[YearProjection],[CostProjection])
	SELECT #ChartListCost.ParentName, #ChartListCost.YearProjection, 0 AS CostProjection 
	FROM #ChartListCost LEFT OUTER JOIN
		#ChartCost ON #ChartListCost.ParentName=#ChartCost.ParentName AND #ChartListCost.YearProjection=#ChartCost.YearProjection
	WHERE #ChartCost.YearProjection IS NULL;	
	END /** Build Source Data Table - Chart Cost **/
	
	BEGIN /** Build Source Data Table - Chart Volume **/
	CREATE TABLE #ChartVolume (ParentName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	/** Add Baseline Totals **/
	INSERT INTO #ChartVolume ([ParentName],[YearProjection],[CostProjection])
	SELECT 'Baseline', YearProjection, Amount
	FROM #VolumeReportGroupedBL
	/** Add Incremental Totals **/
	INSERT INTO #ChartVolume ([ParentName],[YearProjection],[CostProjection])
	SELECT 'Incremental', YearProjection, Amount
	FROM #VolumeReportGroupedBO

	/** Get Full List of Years and Names **/
	CREATE TABLE #ChartListVolume (ParentName NVARCHAR(255), YearProjection INT) ON [PRIMARY]
	INSERT INTO #ChartListVolume ([ParentName],[YearProjection])
	SELECT CNList.ParentName, CYList.Year FROM
		(SELECT [ParentName] FROM #ChartVolume GROUP BY [ParentName])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList
	WHERE CYList.Year BETWEEN @StartYear AND @EndYear

	/** Add Any Missing Years to Make the Chart Fill Properly **/
	INSERT INTO #ChartVolume ([ParentName],[YearProjection],[CostProjection])
	SELECT #ChartListVolume.ParentName, #ChartListVolume.YearProjection, 0 AS CostProjection 
	FROM #ChartListVolume LEFT OUTER JOIN
		#ChartVolume ON #ChartListVolume.ParentName=#ChartVolume.ParentName AND #ChartListVolume.YearProjection=#ChartVolume.YearProjection
	WHERE #ChartVolume.YearProjection IS NULL;	
	END /** Build Source Data Table - Chart Volume **/
	
	BEGIN /** Build Export Data Table - Total **/
	CREATE TABLE #ExportOpex (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #ExportOpex ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS ParentName, 
		CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
		tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END, tblCalcProjectionTechnical.Year;
	END /** Build Export Data Table - Total **/
	
	BEGIN /** Build Export Baseline and Incremental **/
	IF @IdTechnicalDriver=0
	BEGIN
		/** Build Source Data Table - Baseline and Business Opportunities (No Techical KPI Required) **/
		CREATE TABLE #ExportBL (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
		CREATE TABLE #ExportBO (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
		IF @FieldCount = 1
		BEGIN
			/** Baseline **/
			INSERT INTO #ExportBL ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS ParentName, 
				CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=1 AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END, tblCalcProjectionTechnical.Year;
			
			/** Business Opportunities **/
			INSERT INTO #ExportBO ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT  CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS ParentName, 
				CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=2 AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END, tblCalcProjectionTechnical.Year;
		END
		ELSE IF @FieldCount <> 1
		BEGIN
			/** Baseline **/
			INSERT INTO #ExportBL ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT  CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS ParentName, 
				CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=1 AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END, tblCalcProjectionTechnical.Year;
			
			/** Business Opportunities **/
			INSERT INTO #ExportBO ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT  CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS ParentName, 
				CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=2 AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END, tblCalcProjectionTechnical.Year;
		END
	END
	END /** Build Export Baseline and Incremental **/
	
	BEGIN /** Add Unit Cost factor, IF NECESSARY **/
	IF @IdCostType = 2
	BEGIN		 
		IF @IdField <>0
		BEGIN		
			UPDATE	#BL
			SET		#BL.CostProjection = CASE WHEN BL.Amount = 0 THEN #BL.CostProjection ELSE #BL.CostProjection/BL.Amount END
			FROM	#BL INNER JOIN (SELECT #VolumeReportBL.YearProjection, SUM(#VolumeReportBL.Amount) AS Amount FROM #VolumeReportBL GROUP BY YearProjection) AS BL ON
					#BL.YearProjection = BL.YearProjection 

			UPDATE	#BO
			SET		#BO.CostProjection = CASE WHEN BO.Amount = 0 THEN #BO.CostProjection ELSE #BO.CostProjection/BO.Amount END
			FROM	#BO INNER JOIN (SELECT #VolumeReportBO.YearProjection, SUM(#VolumeReportBO.Amount) AS Amount FROM #VolumeReportBO GROUP BY YearProjection) AS BO ON
					#BO.YearProjection = BO.YearProjection 

			UPDATE	#ExportBL
			SET		#ExportBL.CostProjection = CASE WHEN BL.Amount = 0 THEN #ExportBL.CostProjection ELSE #ExportBL.CostProjection/BL.Amount END
			FROM	#ExportBL INNER JOIN (SELECT #VolumeReportBL.YearProjection, SUM(#VolumeReportBL.Amount) AS Amount FROM #VolumeReportBL GROUP BY YearProjection) AS BL ON
					#ExportBL.YearProjection = BL.YearProjection 

			UPDATE	#ExportBO
			SET		#ExportBO.CostProjection = CASE WHEN BO.Amount = 0 THEN #ExportBO.CostProjection ELSE #ExportBO.CostProjection/BO.Amount END
			FROM	#ExportBO INNER JOIN (SELECT #VolumeReportBO.YearProjection, SUM(#VolumeReportBO.Amount) AS Amount FROM #VolumeReportBO GROUP BY YearProjection) AS BO ON
					#ExportBO.YearProjection = #ExportBO.YearProjection 

		END
		ELSE
		BEGIN
			UPDATE	#BL
			SET		#BL.CostProjection = CASE WHEN #VolumeReportBL.Amount = 0 THEN #BL.CostProjection ELSE #BL.CostProjection/#VolumeReportBL.Amount END
			FROM	#BL INNER JOIN #VolumeReportBL ON
					#BL.YearProjection = #VolumeReportBL.YearProjection AND #BL.Name=#VolumeReportBL.Name

			UPDATE	#BO
			SET		#BO.CostProjection = CASE WHEN #VolumeReportBO.Amount = 0 THEN #BO.CostProjection ELSE #BO.CostProjection/#VolumeReportBO.Amount END
			FROM	#BO INNER JOIN #VolumeReportBO ON
					#BO.YearProjection = #VolumeReportBO.YearProjection AND #BO.Name=#VolumeReportBO.Name

			UPDATE	#ExportBL
			SET		#ExportBL.CostProjection = CASE WHEN #VolumeReportBL.Amount = 0 THEN #ExportBL.CostProjection ELSE #ExportBL.CostProjection/#VolumeReportBL.Amount END
			FROM	#ExportBL INNER JOIN #VolumeReportBL ON
					#ExportBL.YearProjection = #VolumeReportBL.YearProjection AND #ExportBL.Name=#VolumeReportBL.Name

			UPDATE	#ExportBO
			SET		#ExportBO.CostProjection = CASE WHEN #VolumeReportBO.Amount = 0 THEN #ExportBO.CostProjection ELSE #ExportBO.CostProjection/#VolumeReportBO.Amount END
			FROM	#ExportBO INNER JOIN #VolumeReportBO ON
					#ExportBO.YearProjection = #VolumeReportBO.YearProjection AND #ExportBO.Name=#VolumeReportBO.Name
		END
		UPDATE	#Opex
		SET		#Opex.CostProjection = CASE WHEN #VolumeReportGrouped.Amount = 0 THEN #Opex.CostProjection * 0 ELSE #Opex.CostProjection/#VolumeReportGrouped.Amount END
		FROM	#Opex INNER JOIN #VolumeReportGrouped ON
				#Opex.YearProjection = #VolumeReportGrouped.YearProjection

		UPDATE	#Chart
		SET		#Chart.CostProjection = CASE WHEN #VolumeReportGrouped.Amount = 0 THEN #Chart.CostProjection * 0 ELSE #Chart.CostProjection/#VolumeReportGrouped.Amount END
		FROM	#Chart INNER JOIN #VolumeReportGrouped ON
				#Chart.YearProjection = #VolumeReportGrouped.YearProjection

		UPDATE	#ChartCost
		SET		#ChartCost.CostProjection = CASE WHEN #VolumeReportGroupedBL.Amount = 0 THEN #ChartCost.CostProjection * 0 ELSE #ChartCost.CostProjection/#VolumeReportGroupedBL.Amount END
		FROM	#ChartCost INNER JOIN #VolumeReportGroupedBL ON
				#ChartCost.YearProjection = #VolumeReportGroupedBL.YearProjection

		UPDATE	#ExportOpex
		SET		#ExportOpex.CostProjection = CASE WHEN #VolumeReportGrouped.Amount = 0 THEN #ExportOpex.CostProjection * 0 ELSE #ExportOpex.CostProjection/#VolumeReportGrouped.Amount END
		FROM	#ExportOpex INNER JOIN #VolumeReportGrouped ON
				#ExportOpex.YearProjection = #VolumeReportGrouped.YearProjection
	END
	END /** Add Unit Cost factor, IF NECESSARY **/

	BEGIN /** Build Output Table **/
	DECLARE @SQLOpex NVARCHAR(MAX);
	SET @SQLOpex = 'SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #Opex PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
	EXEC(@SQLOpex);

	DECLARE @SQLOpexChart NVARCHAR(MAX);
	SET @SQLOpexChart = 'SELECT [YearProjection] AS [Year],' + @AccountColumnList + ' FROM #Chart PIVOT (SUM(CostProjection) FOR ParentName IN (' + @AccountColumnList + ')) AS PivotTable ORDER BY [YearProjection]';
	EXEC(@SQLOpexChart);

	IF @IdTechnicalDriver=0
	BEGIN
		DECLARE @SQLOpexBL NVARCHAR(MAX);
		SET @SQLOpexBL = 'SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #BL PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name] DESC';
		EXEC(@SQLOpexBL);

		DECLARE @SQLOpexBO NVARCHAR(MAX);
		SET @SQLOpexBO = 'SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #BO PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name] DESC';
		EXEC(@SQLOpexBO);
	END

	DECLARE @SQLVolumeTotal NVARCHAR(MAX);
	SET @SQLVolumeTotal = 'SELECT [ParentName], [Name],' + @YearColumnList + ' FROM #VolumeReportGrouped PIVOT (SUM(Amount) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
	EXEC(@SQLVolumeTotal);

	DECLARE @SQLVolumeBL NVARCHAR(MAX);
	SET @SQLVolumeBL = 'SELECT [ParentName], [Name],' + @YearColumnList + ' FROM #VolumeReportGroupedBL PIVOT (SUM(Amount) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
	EXEC(@SQLVolumeBL);
	
	DECLARE @SQLVolumeBO NVARCHAR(MAX);
	SET @SQLVolumeBO = 'SELECT [ParentName], [Name],' + @YearColumnList + ' FROM #VolumeReportGroupedBO PIVOT (SUM(Amount) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName],[Name]';
	EXEC(@SQLVolumeBO);

	UPDATE #ChartCost SET [CostProjection]=0 WHERE [CostProjection] IS NULL
	DECLARE @SQLOpexChartCost NVARCHAR(MAX);
	SET @SQLOpexChartCost = 'SELECT [YearProjection] AS [Year],' + @CaseTypeList + ' FROM #ChartCost PIVOT (SUM(CostProjection) FOR ParentName IN (' + @CaseTypeList + ')) AS PivotTable ORDER BY [YearProjection]';
	EXEC(@SQLOpexChartCost);

	UPDATE #ChartVolume SET [CostProjection]=0 WHERE [CostProjection] IS NULL
	DECLARE @SQLOpexChartVolume NVARCHAR(MAX);
	SET @SQLOpexChartVolume = 'SELECT [YearProjection] AS [Year],' + @CaseTypeList + ' FROM #ChartVolume PIVOT (SUM(CostProjection) FOR ParentName IN (' + @CaseTypeList + ')) AS PivotTable ORDER BY [YearProjection]';
	EXEC(@SQLOpexChartVolume);

	/** Export Data **/
	SELECT * FROM #ExportOpex
	SELECT * FROM #ExportBL
	SELECT * FROM #ExportBO

	/** Output Stats Table for UI **/
	IF @IdTechnicalDriver=0
	BEGIN
		SELECT @FieldCount AS FieldCount, COUNT(*) AS AccountCount FROM #AccountList;
	END
	END /** Build Output Table **/

END

GO

/****** Object:  StoredProcedure [dbo].[reppOpexProjectionDetailed]    Script Date: 03/23/2016 10:25:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[reppOpexProjectionDetailed]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[reppOpexProjectionDetailed]
GO

/****** Object:  StoredProcedure [dbo].[reppOpexProjectionDetailed]    Script Date: 02/18/2016 09:11:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-03-31
-- Description:	Module Report Opex Projection detailed Baseline and Business Opportunities
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 6/10/2015	RM		Added @Factor parameter
-- 8/5/2015		RM		Cleaned up code, removed what wasn't being used.
-- ===========================================================================================

CREATE PROCEDURE [dbo].[reppOpexProjectionDetailed](
@IdModel INT,
@IdAggregationLevel INT,
@IdField INT,
@IdStructure INT,
@From INT,
@To INT,
@IdTechnicalScenario INT,
@IdEconomicScenario INT,
@IdCurrency INT,
@IdTerm INT,
@IdTechnicalDriver INT,
@IdCostType INT,
@IdTypeOperation INT,
@IdLanguage INT,
@Factor INT
)
AS
BEGIN

	BEGIN /** DROP Temporary Tables **/
	IF OBJECT_ID('tempdb..#FieldList') IS NOT NULL DROP TABLE #FieldList
	IF OBJECT_ID('tempdb..#AccountList') IS NOT NULL DROP TABLE #AccountList
	IF OBJECT_ID('tempdb..#TechDriver') IS NOT NULL DROP TABLE #TechDriver
	IF OBJECT_ID('tempdb..#Opex') IS NOT NULL DROP TABLE #Opex
	IF OBJECT_ID('tempdb..#BL') IS NOT NULL DROP TABLE #BL
	IF OBJECT_ID('tempdb..#BO') IS NOT NULL DROP TABLE #BO
	IF OBJECT_ID('tempdb..#BLList') IS NOT NULL DROP TABLE #BLList
	IF OBJECT_ID('tempdb..#BOList') IS NOT NULL DROP TABLE #BOList
	IF OBJECT_ID('tempdb..#Chart') IS NOT NULL DROP TABLE #Chart
	IF OBJECT_ID('tempdb..#ChartList') IS NOT NULL DROP TABLE #ChartList
	IF OBJECT_ID('tempdb..#ProjectionCriteriaList') IS NOT NULL DROP TABLE #ProjectionCriteriaList
	IF OBJECT_ID('tempdb..#tblCalcProjectionTechnical') IS NOT NULL DROP TABLE #tblCalcProjectionTechnical
	IF OBJECT_ID('tempdb..#VolumeReportBLRows') IS NOT NULL DROP TABLE #VolumeReportBLRows  
	IF OBJECT_ID('tempdb..#VolumeReportBL') IS NOT NULL DROP TABLE #VolumeReportBL  
	IF OBJECT_ID('tempdb..#VolumeReportBORows') IS NOT NULL DROP TABLE #VolumeReportBORows
	IF OBJECT_ID('tempdb..#VolumeReportBO') IS NOT NULL DROP TABLE #VolumeReportBO
	IF OBJECT_ID('tempdb..#VolumeReportBLList') IS NOT NULL DROP TABLE #VolumeReportBLList 
	IF OBJECT_ID('tempdb..#VolumeReportBOList') IS NOT NULL DROP TABLE #VolumeReportBOList 
	IF OBJECT_ID('tempdb..#VolumeReportGrouped') IS NOT NULL DROP TABLE #VolumeReportGrouped
	IF OBJECT_ID('tempdb..#VolumeReportGroupedBL') IS NOT NULL DROP TABLE #VolumeReportGroupedBL 
	IF OBJECT_ID('tempdb..#VolumeReportGroupedBO') IS NOT NULL DROP TABLE #VolumeReportGroupedBO 
	IF OBJECT_ID('tempdb..#BLScreen') IS NOT NULL DROP TABLE #BLScreen
	IF OBJECT_ID('tempdb..#BOScreen') IS NOT NULL DROP TABLE #BOScreen
	IF OBJECT_ID('tempdb..#TotalScreen') IS NOT NULL DROP TABLE #TotalScreen
	IF OBJECT_ID('tempdb..#ProjectionYears') IS NOT NULL DROP TABLE #ProjectionYears
	IF OBJECT_ID('tempdb..#EconomicDrivers') IS NOT NULL DROP TABLE #EconomicDrivers
	IF OBJECT_ID('tempdb..#MissingEconomicDrivers') IS NOT NULL DROP TABLE #MissingEconomicDrivers	
	IF OBJECT_ID('tempdb..#PerformanceDrivers') IS NOT NULL DROP TABLE #PerformanceDrivers
	IF OBJECT_ID('tempdb..#MissingPerformanceDrivers') IS NOT NULL DROP TABLE #MissingPerformanceDrivers
	IF OBJECT_ID('tempdb..#SizeDrivers') IS NOT NULL DROP TABLE #SizeDrivers
	IF OBJECT_ID('tempdb..#MissingSizeDrivers') IS NOT NULL DROP TABLE #MissingSizeDrivers	
	IF OBJECT_ID('tempdb..#TechDriversBL') IS NOT NULL DROP TABLE #TechDriversBL  
	IF OBJECT_ID('tempdb..#TechDriversBO') IS NOT NULL DROP TABLE #TechDriversBO
	IF OBJECT_ID('tempdb..#TechDriversAll') IS NOT NULL DROP TABLE #TechDriversAll
	IF OBJECT_ID('tempdb..#TechDriversBLList') IS NOT NULL DROP TABLE #TechDriversBLList 
	IF OBJECT_ID('tempdb..#TechDriversBOList') IS NOT NULL DROP TABLE #TechDriversBOList 
	IF OBJECT_ID('tempdb..#ExportReport') IS NOT NULL DROP TABLE #ExportReport
	IF OBJECT_ID('tempdb..#BLScreenFields') IS NOT NULL DROP TABLE #BLScreenFields
	IF OBJECT_ID('tempdb..#BOScreenFields') IS NOT NULL DROP TABLE #BOScreenFields
	IF OBJECT_ID('tempdb..#TotalScreenFields') IS NOT NULL DROP TABLE #TotalScreenFields
	END /** DROP Temporary Tables **/
	
	BEGIN /** Build List Tables for reports **/
	/** Structure Value - ZiffEnergy=1 Client=2 **/
	/** Term Value - Real=1 (Run Economic Model) Nominal=2 (No Economic Influence) **/
	DECLARE @StartYear INT = @From;
	DECLARE @EndYear INT = @To;

	DECLARE @TotalCapacityDriver INT = 0;

	/** Get Base Year **/
	DECLARE @BaseYear INT
	SELECT @BaseYear = [BaseYear] FROM [tblModels] WHERE IdModel=@IdModel
		
	/** Build Year Columns **/
	DECLARE @YearColumnList NVARCHAR(MAX)='';
	DECLARE @YearColumnListSum NVARCHAR(MAX)='';
	DECLARE @YearFrom INT
	SET @YearFrom=@From	
	WHILE (@YearFrom <= @To)
	BEGIN
		SET @YearColumnList =  @YearColumnList + '[' + CAST(@YearFrom AS VARCHAR(4)) + ']';
		SET @YearColumnListSum =  @YearColumnListSum + 'SUM([' + CAST(@YearFrom AS VARCHAR(4)) + ']) AS [' + CAST(@YearFrom AS VARCHAR(4)) + ']';
		SET @YearFrom = @YearFrom + 1;
		IF (@YearFrom <= @To)
		BEGIN
			SET @YearColumnList = @YearColumnList + ',';
			SET @YearColumnListSum = @YearColumnListSum + ',';
		END
	END
	
	/** Get Currency Factor to use **/
	DECLARE @CurrencyFactor FLOAT;
	SELECT @CurrencyFactor=[Value] FROM tblModelsCurrencies WHERE [IdCurrency]=@IdCurrency AND [IdModel]=@IdModel;

	/** Build Field List **/
	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF (@IdAggregationLevel=0 AND @IdField=0 AND @IdTypeOperation=0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0 AND @IdField>0 AND @IdTypeOperation>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdField]=@IdField AND [IdModel]=@IdModel AND [IdTypeOperation]=@IdTypeOperation GROUP BY [IdField];
	END
	ELSE IF (@IdField>0 AND @IdTypeOperation>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdField]=@IdField AND [IdModel]=@IdModel AND [IdTypeOperation]=@IdTypeOperation GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0 AND @IdField>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0 AND @IdTypeOperation>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdModel]=@IdModel AND [IdTypeOperation]=@IdTypeOperation GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdField>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdTypeOperation>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdTypeOperation]=@IdTypeOperation AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	DECLARE @FieldCount INT;
	SELECT @FieldCount=COUNT([IdField]) FROM #FieldList

	/** Build Projection Criteria List **/
	CREATE TABLE #ProjectionCriteriaList (Code INT PRIMARY KEY NOT NULL, [Name] NVARCHAR(20), [SortOrder] INT) ON [PRIMARY]
	INSERT INTO #ProjectionCriteriaList (Code, Name, SortOrder)
	SELECT Code, Name, CASE WHEN Code=1 THEN 1 
			WHEN Code=3 THEN 2
			WHEN Code=4 THEN 3
			WHEN Code=2 THEN 4
			ELSE 5 END  
	FROM tblParameters 
	WHERE [Type]='ProjectionCriteria' and IdLanguage=@IdLanguage AND Code<90

	/** Build Criteria Columns **/
	DECLARE @ProjectionCriteriaList NVARCHAR(MAX)='';	
	DECLARE @ProjectionCriteriaList2 NVARCHAR(MAX)='SUM([Total]) AS Total,';	
	DECLARE @ProjectionCriteriaName NVARCHAR(255);
	DECLARE ProjectionCriteriaList CURSOR FOR
	SELECT Name FROM #ProjectionCriteriaList ORDER BY SortOrder;

	OPEN ProjectionCriteriaList;
	FETCH NEXT FROM ProjectionCriteriaList INTO @ProjectionCriteriaName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @ProjectionCriteriaList=''
		BEGIN
			SET @ProjectionCriteriaList =  @ProjectionCriteriaList + '[' + @ProjectionCriteriaName + ']';
			SET @ProjectionCriteriaList2 =  @ProjectionCriteriaList2 + 'SUM(ISNULL([' + @ProjectionCriteriaName + '],0)) AS ' + @ProjectionCriteriaName;
		END
		ELSE
		BEGIN
			SET @ProjectionCriteriaList =  @ProjectionCriteriaList + ',[' + @ProjectionCriteriaName + ']';
			SET @ProjectionCriteriaList2 =  @ProjectionCriteriaList2 + ',SUM(ISNULL([' + @ProjectionCriteriaName + '],0)) AS ' + @ProjectionCriteriaName;
		END
		FETCH NEXT FROM ProjectionCriteriaList INTO @ProjectionCriteriaName;
	END

	CLOSE ProjectionCriteriaList;
	DEALLOCATE ProjectionCriteriaList;

	/** Build Technical Driver Factors (used for KPI Report) **/
	CREATE TABLE #TechDriver ([Year] INT, [Normal] FLOAT, [Optimistic] FLOAT, [Pessimistic] FLOAT) ON [PRIMARY]
	IF @IdTechnicalDriver=0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, 1 AS [Normal], 1 AS [Optimistic], 1 AS [Pessimistic]
		FROM tblCalcModelYears
		WHERE [IdModel]=@IdModel AND [Year] BETWEEN @StartYear AND @EndYear;
	END
	ELSE IF @IdTechnicalDriver>0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, SUM([Normal]) AS [Normal], SUM([Pessimistic]) AS [Pessimistic], SUM([Optimistic]) AS [Optimistic]
		FROM tblTechnicalDriverData
		WHERE [IdTechnicalDriver]=@IdTechnicalDriver AND [IdModel]=@IdModel AND IdField IN (SELECT [IdField] FROM #FieldList)
		AND	dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
		GROUP BY Year;
	END

	/** Get Chart Columns to Temp Table **/
	CREATE TABLE #AccountList (ColumnName NVARCHAR(255) PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF @IdStructure=1
	BEGIN
		INSERT INTO #AccountList SELECT [RootZiffAccount] FROM tblCalcProjectionTechnical WHERE IdField IN (SELECT [IdField] FROM #FieldList) AND [IdModel]=@IdModel GROUP BY [RootZiffAccount] ORDER BY [RootZiffAccount];
	END
	ELSE IF @IdStructure=2
	BEGIN
		INSERT INTO #AccountList SELECT [Activity] FROM tblCalcProjectionTechnical WHERE IdField IN (SELECT [IdField] FROM #FieldList) AND [IdModel]=@IdModel GROUP BY [Activity], [SortOrder] ORDER BY [SortOrder], [Activity];
	END
	
	/** Generate Chart Columns from Temp Table **/
	DECLARE @AccountColumnList NVARCHAR(MAX)='';
	DECLARE @AccountName NVARCHAR(255);
	DECLARE AccountList CURSOR FOR
	SELECT ColumnName FROM #AccountList;

	OPEN AccountList;
	FETCH NEXT FROM AccountList INTO @AccountName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @AccountColumnList=''
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + '[' + @AccountName + ']';
		END
		ELSE
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + ',[' + @AccountName + ']';
		END
		FETCH NEXT FROM AccountList INTO @AccountName;
	END

	CLOSE AccountList;
	DEALLOCATE AccountList;
	
	END /** Build List Tables for reports **/

	BEGIN /** Build Volume Tables **/
	/** Report (Baseline)**/
	CREATE TABLE #VolumeReportBLRows ([ParentName] NVARCHAR(255), [ProjectCode] NVARCHAR(255), [ProjectName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLRows ([ParentName], [ProjectCode], [ProjectName], [Name], [YearProjection], [Amount])	
	SELECT	DISTINCT N'All Cost Accounts' AS ParentName, dbo.tblProjects.Code, dbo.tblProjects.Name, dbo.tblTechnicalDrivers.Name AS Name,
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN ISNULL(dbo.tblTechnicalDriverData.Normal,0)
				WHEN @IdTechnicalScenario=2 THEN ISNULL(dbo.tblTechnicalDriverData.Optimistic,0)
				WHEN @IdTechnicalScenario=3 THEN ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0)
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject AND
			dbo.tblTechnicalDriverData.Year >= dbo.tblProjects.Starts
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=1
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	ORDER BY dbo.tblTechnicalDriverData.Year

	CREATE TABLE #VolumeReportBL ([ParentName] NVARCHAR(255), [ProjectCode] NVARCHAR(255), [ProjectName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBL ([ParentName], [ProjectCode], [ProjectName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, ProjectCode, ProjectName, Name, YearProjection, Amount
	FROM	#VolumeReportBLRows
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBLList (ParentName NVARCHAR(255), [ProjectCode] NVARCHAR(255), [ProjectName] NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLList ([ParentName], [ProjectCode], [ProjectName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.ProjectCode, CNList.ProjectName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[ProjectCode],[ProjectName],[Name] FROM #VolumeReportBL WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[ProjectCode],[ProjectName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #VolumeReportBLList to Fill Properly **/
	INSERT INTO #VolumeReportBL ([ParentName], [ProjectCode], [ProjectName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBLList.ParentName,#VolumeReportBLList.ProjectCode,#VolumeReportBLList.ProjectName,#VolumeReportBLList.Name,#VolumeReportBLList.YearProjection,0 AS Amount
	FROM	#VolumeReportBLList LEFT OUTER JOIN
			#VolumeReportBL ON #VolumeReportBLList.ParentName=#VolumeReportBL.ParentName AND #VolumeReportBLList.Name=#VolumeReportBL.Name AND #VolumeReportBLList.YearProjection=#VolumeReportBL.YearProjection
	WHERE	#VolumeReportBL.YearProjection IS NULL;

	/** Delete all rows that have 0 (zero) Amount **/
	DELETE #VolumeReportBL WHERE Amount=0

	/** Report (Business Opportunities)**/
	CREATE TABLE #VolumeReportBORows ([ParentName] NVARCHAR(255), [ProjectCode] NVARCHAR(255), [ProjectName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBORows ([ParentName], [ProjectCode], [ProjectName], [Name], [YearProjection], [Amount])
	SELECT	DISTINCT N'All Cost Accounts' AS ParentName, dbo.tblProjects.Code, dbo.tblProjects.Name, dbo.tblTechnicalDrivers.Name AS Name,
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN ISNULL(dbo.tblTechnicalDriverData.Normal,0)
				WHEN @IdTechnicalScenario=2 THEN ISNULL(dbo.tblTechnicalDriverData.Optimistic,0)
				WHEN @IdTechnicalScenario=3 THEN ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0)
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON
			dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject AND
			dbo.tblTechnicalDriverData.Year >= dbo.tblProjects.Starts
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=2
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	ORDER BY dbo.tblTechnicalDriverData.Year

	CREATE TABLE #VolumeReportBO ([ParentName] NVARCHAR(255), [ProjectCode] NVARCHAR(255), [ProjectName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBO ([ParentName], [ProjectCode], [ProjectName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, ProjectCode, ProjectName, Name, YearProjection, Amount
	FROM	#VolumeReportBORows
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBOList ([ParentName] NVARCHAR(255), [ProjectCode] NVARCHAR(255), [ProjectName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBOList ([ParentName], [ProjectCode], [ProjectName], [Name], [YearProjection], [Amount])
	SELECT CNList.ParentName, CNList.ProjectCode, CNList.ProjectName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[ProjectCode],[ProjectName],[Name] FROM #VolumeReportBO WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[ProjectCode],[ProjectName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #VolumeReportBOList to Fill Properly **/
	INSERT INTO #VolumeReportBO ([ParentName], [ProjectCode], [ProjectName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBOList.ParentName,#VolumeReportBOList.ProjectCode,#VolumeReportBOList.ProjectName,#VolumeReportBOList.Name, #VolumeReportBOList.YearProjection,0 AS Amount
	FROM	#VolumeReportBOList LEFT OUTER JOIN
			#VolumeReportBO ON #VolumeReportBOList.ParentName=#VolumeReportBO.ParentName AND #VolumeReportBOList.Name=#VolumeReportBO.Name AND #VolumeReportBOList.YearProjection=#VolumeReportBO.YearProjection
	WHERE	#VolumeReportBO.YearProjection IS NULL;

	/** Delete all rows that have 0 (zero) Amount **/
	DELETE #VolumeReportBO WHERE Amount=0

	/** Report (All Fields Grouped)**/
	CREATE TABLE #VolumeReportGroupedBL ([YearProjection] INT, [Amount] FLOAT, [ParentName] NVARCHAR(255), [Name] NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBL ([YearProjection], [Amount], [ParentName], [Name])
	SELECT YearProjection,SUM(amount) AS Amount,'GroupedBL',Name
	FROM #VolumeReportBL
	GROUP BY YearProjection,Name
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGroupedBO ([YearProjection] INT, [Amount] FLOAT, [ParentName] NVARCHAR(255), [Name] NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBO ([YearProjection], [Amount], [ParentName], [Name])
	SELECT YearProjection,SUM(amount) AS Amount,'GroupedBO',Name
	FROM #VolumeReportBO
	GROUP BY YearProjection,Name
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGrouped ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255),[YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGrouped ([ParentName], [Name], [YearProjection], [Amount])
	SELECT N'All Cost Accounts' AS ParentName,Name,YearProjection,SUM(Amount) AS 'Amount'
	FROM
	(
		SELECT 'VolumeReportGroupedBL' AS "ProjectType", ParentName, Name, YearProjection,SUM(Amount) AS "Amount"
		FROM #VolumeReportGroupedBL 
		GROUP BY ParentName,Name,YearProjection
		UNION
		SELECT 'VolumeReportGroupedBO', ParentName, Name, YearProjection, SUM(Amount) AS "Amount"
		FROM #VolumeReportGroupedBO 
		GROUP BY ParentName,Name,YearProjection
	) AS VolumeReportGroupedTotals
	GROUP BY Name,YearProjection
	ORDER BY Name,YearProjection
	END /** Build Volume Tables **/
	
	BEGIN /** Build Source Data Table - Baseline and Business Opportunities (No Techical KPI Required) **/
	CREATE TABLE #BL (FieldProject NVARCHAR(255), Field NVARCHAR(255), Project NVARCHAR(255), ProjectionCriteria NVARCHAR(255), YearProjection INT, Total FLOAT, CostProjection FLOAT, SortOrder INT) ON [PRIMARY]
	CREATE TABLE #BO (FieldProject NVARCHAR(255), Field NVARCHAR(255), Project NVARCHAR(255), ProjectionCriteria NVARCHAR(255), YearProjection INT, Total FLOAT, CostProjection FLOAT, SortOrder INT) ON [PRIMARY]

	/** BL **/
	INSERT INTO #BL ([FieldProject],[Field],[Project],[ProjectionCriteria],[YearProjection],[Total],[CostProjection],[SortOrder])
	SELECT DISTINCT CPT.Field + ' ('+ CPT.Project + ')' AS FieldProject, CPT.Field, CPT.Project, P.Name AS ProjectionCriteria, CPT.Year AS YearProjection, 0,
		ISNULL(SUM ( 
			CASE WHEN @IdTechnicalScenario=1 THEN CPT.TFNormal * CPT.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN CPT.TFOptimistic * CPT.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN CPT.TFPessimistic * CPT.BCRPessimistic 
			ELSE 0 END 
			* @CurrencyFactor
			/ @Factor 
			* CPT.CaseSelection
		),0) AS CostProjection, P.SortOrder
	FROM tblCalcProjectionTechnical CPT LEFT OUTER JOIN #ProjectionCriteriaList P ON
		CPT.ProjectionCriteria = P.Code
	WHERE CPT.IdModel=@IdModel
	  AND CPT.IdField IN (SELECT [IdField] FROM #FieldList) 
	  AND CPT.Operation = 1 
	GROUP BY CPT.Field + ' ('+ CPT.Project + ')',CPT.Field, CPT.Project, P.Name, CPT.Year, P.SortOrder
	
	/** BO **/
	INSERT INTO #BO ([FieldProject],[Field],[Project],[ProjectionCriteria],[YearProjection],[Total],[CostProjection],[SortOrder])
	SELECT CPT.Field + ' ('+ CPT.Project + ')' AS FieldProject, CPT.Field, CPT.Project, P.Name AS ProjectionCriteria, CPT.Year AS YearProjection, 0,
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN CPT.TFNormal * CPT.BCRNormal 
				WHEN @IdTechnicalScenario=2 THEN CPT.TFOptimistic * CPT.BCROptimistic 
				WHEN @IdTechnicalScenario=3 THEN CPT.TFPessimistic * CPT.BCRPessimistic 
				ELSE 0 
			END 
			* @CurrencyFactor
			/ @Factor 
			* CPT.CaseSelection
		) AS CostProjection, P.SortOrder
	FROM tblCalcProjectionTechnical CPT JOIN #ProjectionCriteriaList P ON
		CPT.ProjectionCriteria = P.Code
	WHERE CPT.IdModel=@IdModel
	  AND CPT.IdField IN (SELECT [IdField] FROM #FieldList) 
	  AND CPT.Operation = 2
	  AND CPT.Year BETWEEN @StartYear AND @EndYear
	GROUP BY CPT.Field + ' ('+ CPT.Project + ')',CPT.Field, CPT.Project, P.Name, CPT.Year, P.SortOrder
	
	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #BLList (FieldProject NVARCHAR(255), Field NVARCHAR(255), Project NVARCHAR(255), ProjectionCriteria NVARCHAR(255), YearProjection INT, Total FLOAT, SortOrder INT) ON [PRIMARY]
	INSERT INTO #BLList ([FieldProject],[Field],[Project],[ProjectionCriteria],[YearProjection],[Total],[SortOrder])
	SELECT CNList.FieldProject, CNList.Field, CNList.Project, CPList.Name, CYList.Year, 0, CPList.SortOrder FROM
		(SELECT [FieldProject],[Field],[Project] FROM #BL GROUP BY [FieldProject],[Field],[Project])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList CROSS JOIN
		(SELECT [NAME], [SortOrder] FROM #ProjectionCriteriaList) AS CPList

	CREATE TABLE #BOList (FieldProject NVARCHAR(255), Field NVARCHAR(255), Project NVARCHAR(255), ProjectionCriteria NVARCHAR(255), YearProjection INT, Total FLOAT, SortOrder INT) ON [PRIMARY]
	INSERT INTO #BOList ([FieldProject],[Field],[Project],[ProjectionCriteria],[YearProjection],[Total],[SortOrder])
	SELECT CNList.FieldProject, CNList.Field, CNList.Project, CPList.Name, CYList.Year, 0, CPList.SortOrder FROM
		(SELECT [FieldProject],[Field],[Project] FROM #BO GROUP BY [FieldProject],[Field],[Project])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList CROSS JOIN
		(SELECT [NAME], [SortOrder] FROM #ProjectionCriteriaList) AS CPList

	/** Add Any Missing Years to Make the Chart Fill Properly (eg. Fields with only Cyclical Drivers) **/
	INSERT INTO #BL([FieldProject],[Field],[Project],[ProjectionCriteria],[YearProjection],[Total],[CostProjection],[SortOrder])
	SELECT #BLList.FieldProject, #BLList.Field, #BLList.Project, #BLList.ProjectionCriteria, #BLList.YearProjection, 0 AS Total, NULL AS CostProjection , #BLList.SortOrder
	FROM #BLList LEFT OUTER JOIN
		 #BL ON #BLList.FieldProject=#BL.FieldProject AND #BLList.Field=#BL.Field AND #BLList.Project=#BL.Project AND #BLList.ProjectionCriteria=#BL.ProjectionCriteria AND #BLList.YearProjection=#BL.YearProjection AND #BLList.SortOrder=#BL.SortOrder
	WHERE #BL.YearProjection IS NULL AND #BL.YearProjection BETWEEN @From AND @To;	

	INSERT INTO #BO([FieldProject],[Field],[Project],[ProjectionCriteria],[YearProjection],[Total],[CostProjection],[SortOrder])
	SELECT #BOList.FieldProject, #BOList.Field, #BOList.Project, #BOList.ProjectionCriteria, #BOList.YearProjection, 0 AS Total, NULL AS CostProjection , #BOList.SortOrder
	FROM #BOList LEFT OUTER JOIN
		 #BO ON #BOList.FieldProject=#BO.FieldProject AND #BOList.Field=#BO.Field AND #BOList.Project=#BO.Project AND #BOList.ProjectionCriteria=#BO.ProjectionCriteria AND #BOList.YearProjection=#BO.YearProjection AND #BOList.SortOrder=#BO.SortOrder
	WHERE #BO.YearProjection IS NULL AND #BO.YearProjection BETWEEN @From AND @To;	
	END /** Build Source Data Table - Baseline and Business Opportunities (No Techical KPI Required) **/
	
	BEGIN /** Create all years selected **/
	CREATE TABLE #ProjectionYears (Year INT) ON [PRIMARY]
	DECLARE @cntYear INT

	SET @cntYear = @From

	WHILE @cntYear <= @To
	BEGIN
	   INSERT INTO #ProjectionYears ([Year])
	   VALUES(@cntYear)
	   SET @cntYear = @cntYear + 1;
	END;
	END /** Create all years selected **/
	
	BEGIN /** Size Drivers **/
	CREATE TABLE #SizeDrivers (IdModel INT, IdField INT, IdTechnicalDriver INT, Name VARCHAR(150), YearProjection INT, Normal INT, Pessimistic INT, Optimistic INT) ON [PRIMARY]
	IF (@IdTechnicalDriver = 0) /** All technical drivers **/
	BEGIN	
		INSERT INTO #SizeDrivers([IdModel],[IdField],[IdTechnicalDriver],[Name],[YearProjection],[Normal],[Pessimistic],[Optimistic])
		SELECT  A.IdModel, A.IdField, A.IdTechnicalDriver, A.Name,A.Year, SUM(A.Normal) AS Normal, SUM(A.Pessimistic) AS Pessimistic, SUM(A.Optimistic) AS Optimistic
		FROM
		(		
		SELECT  'OPERATION = 1' AS 'OPERATION',dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField, dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.Name, 
				dbo.tblTechnicalDriverData.Year, SUM(dbo.tblTechnicalDriverData.Normal) AS Normal, SUM(dbo.tblTechnicalDriverData.Pessimistic) AS Pessimistic, SUM(dbo.tblTechnicalDriverData.Optimistic) AS Optimistic
		FROM    dbo.tblProjects INNER JOIN
				dbo.tblTechnicalDriverData ON dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject INNER JOIN
				dbo.tblTechnicalDrivers ON dbo.tblTechnicalDriverData.IdTechnicalDriver = dbo.tblTechnicalDrivers.IdTechnicalDriver
		WHERE	(dbo.tblTechnicalDrivers.TypeDriver=1) and tblProjects.Operation=1
		GROUP BY dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.Name, dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField
		HAVING  (dbo.tblTechnicalDrivers.IdModel = @IdModel) AND (dbo.tblTechnicalDriverData.IdField IN (SELECT IdField FROM #FieldList)) AND 
				(dbo.tblTechnicalDrivers.IdTechnicalDriver IN (SELECT dbo.tblTechnicalDrivers.IdTechnicalDriver FROM dbo.tblTechnicalDrivers WHERE dbo.tblTechnicalDrivers.IdModel = @IdModel))
		UNION
		SELECT	'OPERATION = 2' AS 'OPERATION',dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField, dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.Name, 
				dbo.tblTechnicalDriverData.Year, SUM(dbo.tblTechnicalDriverData.Normal) AS Normal, SUM(dbo.tblTechnicalDriverData.Pessimistic) AS Pessimistic, SUM(dbo.tblTechnicalDriverData.Optimistic) AS Optimistic
		FROM	dbo.tblProjects INNER JOIN
				dbo.tblTechnicalDriverData ON dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject INNER JOIN
				dbo.tblTechnicalDrivers ON dbo.tblTechnicalDriverData.IdTechnicalDriver = dbo.tblTechnicalDrivers.IdTechnicalDriver
		WHERE	(dbo.tblTechnicalDrivers.TypeDriver=1) and tblProjects.Operation=2 AND dbo.tblTechnicalDriverData.Year >= tblProjects.Starts
		GROUP BY dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.Name, dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField
		HAVING  (dbo.tblTechnicalDrivers.IdModel = @IdModel) AND (dbo.tblTechnicalDriverData.IdField IN (SELECT IdField FROM #FieldList)) AND 
				(dbo.tblTechnicalDrivers.IdTechnicalDriver IN (SELECT dbo.tblTechnicalDrivers.IdTechnicalDriver FROM dbo.tblTechnicalDrivers WHERE dbo.tblTechnicalDrivers.IdModel = @IdModel))
		) A
		GROUP BY A.IdModel, A.IdField, A.IdTechnicalDriver, A.Name,A.Year
	END
	ELSE
	BEGIN
		INSERT INTO #SizeDrivers([IdModel],[IdField],[IdTechnicalDriver],[Name],[YearProjection],[Normal],[Pessimistic],[Optimistic])
		SELECT  dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField, dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.Name, 
				dbo.tblTechnicalDriverData.Year, SUM(dbo.tblTechnicalDriverData.Normal) AS Normal, SUM(dbo.tblTechnicalDriverData.Pessimistic) AS Pessimistic, SUM(dbo.tblTechnicalDriverData.Optimistic) AS Optimistic
		FROM    dbo.tblProjects INNER JOIN
				dbo.tblTechnicalDriverData ON dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject INNER JOIN
				dbo.tblTechnicalDrivers ON dbo.tblTechnicalDriverData.IdTechnicalDriver = dbo.tblTechnicalDrivers.IdTechnicalDriver
		WHERE	(dbo.tblTechnicalDrivers.TypeDriver=1)
		GROUP BY dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.Name, dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField
		HAVING  (dbo.tblTechnicalDrivers.IdModel = @IdModel) AND (dbo.tblTechnicalDriverData.IdField IN (SELECT IdField FROM #FieldList)) AND (dbo.tblTechnicalDrivers.IdTechnicalDriver = @IdTechnicalDriver)
	END

	/** Get full list of Size Drivers **/
	CREATE  TABLE #MissingSizeDrivers (IdModel INT, IdField INT, IdTechnicalDriver INT, Name VARCHAR(150), YearProjection INT, Pessimistic INT, Normal INT, Optimistic INT) ON [PRIMARY]		
	INSERT INTO #MissingSizeDrivers ([IdModel],[IdField],[IdTechnicalDriver],[Name],[YearProjection],[Normal],[Pessimistic],[Optimistic])
	SELECT DISTINCT CNList.IdModel, CNList.IdField, CNList.IdTechnicalDriver, CNList.Name, CYList.Year, 0, 0, 0 
	FROM
		(SELECT [IdModel], IdField, IdTechnicalDriver, YearProjection, Name FROM #SizeDrivers WHERE YearProjection IS NOT NULL GROUP BY IdModel, IdField, IdTechnicalDriver, YearProjection, Name) AS CNList CROSS JOIN
		(SELECT [Year] FROM #ProjectionYears) AS CYList

	/** Add Any Missing Year to #SizeDrivers to Fill Properly **/
	INSERT INTO #SizeDrivers([IdModel],[IdField],[IdTechnicalDriver],[Name],[YearProjection],[Normal],[Pessimistic],[Optimistic])
	SELECT msd.[IdModel],msd.[IdField],msd.[IdTechnicalDriver],msd.[Name],msd.[YearProjection],0,0,0
	FROM #SizeDrivers sd FULL JOIN 
		#MissingSizeDrivers msd ON sd.IdModel = msd.IdModel AND sd.IdField=msd.IdField AND sd.IdTechnicalDriver=msd.IdTechnicalDriver AND sd.Name=msd.Name AND sd.YearProjection=msd.YearProjection
	WHERE sd.YearProjection IS NULL
	END /** Size Drivers **/

	BEGIN /** Performance Drivers **/
	CREATE TABLE #PerformanceDrivers (IdModel INT, IdField INT, IdTechnicalDriver INT, TypeDriver INT, Name VARCHAR(150), YearProjection INT, Normal INT, Pessimistic INT, Optimistic INT) ON [PRIMARY]
	INSERT INTO #PerformanceDrivers ([IdModel],[IdField],[IdTechnicalDriver],[TypeDriver],[Name],[YearProjection],[Pessimistic],[Normal],[Optimistic])
	SELECT  A.IdModel, A.IdField, A.IdTechnicalDriver, A.TypeDriver, A.Name,A.Year, SUM(A.Normal) AS Normal, SUM(A.Pessimistic) AS Pessimistic, SUM(A.Optimistic) AS Optimistic
	FROM
	(		
	SELECT  'OPERATION = 1' AS 'OPERATION',dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField, dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.TypeDriver, dbo.tblTechnicalDrivers.Name, 
			dbo.tblTechnicalDriverData.Year, SUM(dbo.tblTechnicalDriverData.Normal) AS Normal, SUM(dbo.tblTechnicalDriverData.Pessimistic) AS Pessimistic, SUM(dbo.tblTechnicalDriverData.Optimistic) AS Optimistic
	FROM    dbo.tblProjects INNER JOIN
			dbo.tblTechnicalDriverData ON dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject INNER JOIN
			dbo.tblTechnicalDrivers ON dbo.tblTechnicalDriverData.IdTechnicalDriver = dbo.tblTechnicalDrivers.IdTechnicalDriver
	WHERE	(dbo.tblTechnicalDrivers.TypeDriver=2) and tblProjects.Operation=1
	GROUP BY dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.Name, dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField, dbo.tblTechnicalDrivers.TypeDriver
	HAVING  (dbo.tblTechnicalDrivers.IdModel = @IdModel) AND (dbo.tblTechnicalDriverData.IdField IN (SELECT IdField FROM #FieldList)) AND 
			(dbo.tblTechnicalDrivers.IdTechnicalDriver IN (SELECT dbo.tblTechnicalDrivers.IdTechnicalDriver FROM dbo.tblTechnicalDrivers WHERE dbo.tblTechnicalDrivers.IdModel = @IdModel))
	UNION
	SELECT	'OPERATION = 2' AS 'OPERATION',dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField, dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.TypeDriver, dbo.tblTechnicalDrivers.Name, 
			dbo.tblTechnicalDriverData.Year, SUM(dbo.tblTechnicalDriverData.Normal) AS Normal, SUM(dbo.tblTechnicalDriverData.Pessimistic) AS Pessimistic, SUM(dbo.tblTechnicalDriverData.Optimistic) AS Optimistic
	FROM	dbo.tblProjects INNER JOIN
			dbo.tblTechnicalDriverData ON dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject INNER JOIN
			dbo.tblTechnicalDrivers ON dbo.tblTechnicalDriverData.IdTechnicalDriver = dbo.tblTechnicalDrivers.IdTechnicalDriver
	WHERE	(dbo.tblTechnicalDrivers.TypeDriver=2) and tblProjects.Operation=2 AND dbo.tblTechnicalDriverData.Year >= tblProjects.Starts
	GROUP BY dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.Name, dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField, dbo.tblTechnicalDrivers.TypeDriver
	HAVING  (dbo.tblTechnicalDrivers.IdModel = @IdModel) AND (dbo.tblTechnicalDriverData.IdField IN (SELECT IdField FROM #FieldList)) AND 
			(dbo.tblTechnicalDrivers.IdTechnicalDriver IN (SELECT dbo.tblTechnicalDrivers.IdTechnicalDriver FROM dbo.tblTechnicalDrivers WHERE dbo.tblTechnicalDrivers.IdModel = @IdModel))
	) A
	GROUP BY A.IdModel, A.IdField, A.IdTechnicalDriver, A.Name, A.Year, A.TypeDriver

	/** Get full list of Performance Drivers **/
	CREATE TABLE #MissingPerformanceDrivers (IdModel INT, IdField INT, IdTechnicalDriver INT, TypeDriver INT, Name VARCHAR(150), YearProjection INT, Normal INT, Pessimistic INT, Optimistic INT) ON [PRIMARY]
	INSERT INTO #MissingPerformanceDrivers ([IdModel],[IdField],[IdTechnicalDriver],[TypeDriver],[Name],[YearProjection],[Normal],[Pessimistic],[Optimistic])
	SELECT DISTINCT CNList.IdModel, CNList.IdField, CNList.IdTechnicalDriver, CNList.TypeDriver, CNList.Name, CYList.Year, 0, 0, 0 
	FROM
		(SELECT [IdModel], IdField, IdTechnicalDriver, TypeDriver, YearProjection, Name FROM #PerformanceDrivers WHERE YearProjection IS NOT NULL GROUP BY IdModel, IdField, IdTechnicalDriver, TypeDriver, YearProjection, Name) AS CNList CROSS JOIN
		(SELECT [Year] FROM #ProjectionYears) AS CYList

	/** Add Any Missing Year to #PerformanceDrivers to Fill Properly **/
	INSERT INTO #PerformanceDrivers ([IdModel],[IdField],[IdTechnicalDriver],[TypeDriver],[Name],[YearProjection],[Normal],[Pessimistic],[Optimistic])
	SELECT mpd.[IdModel],mpd.[IdField],mpd.[IdTechnicalDriver],mpd.[TypeDriver],mpd.[Name],mpd.[YearProjection],0,0,0
	FROM #PerformanceDrivers pd FULL JOIN 
		#MissingPerformanceDrivers mpd ON pd.IdModel = mpd.IdModel AND pd.IdField=mpd.IdField AND pd.IdTechnicalDriver=mpd.IdTechnicalDriver AND pd.TypeDriver=mpd.TypeDriver AND pd.Name=mpd.Name AND pd.YearProjection=mpd.YearProjection
	WHERE pd.YearProjection IS NULL
	END /** Performance Drivers **/

	BEGIN /** Build Source Data Table - Chart **/
	CREATE TABLE #Chart (ParentName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #Chart ([ParentName],[YearProjection],[CostProjection])
	SELECT tblParameters.Name AS ParentName,
		tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel INNER JOIN 
		tblParameters ON tblCalcProjectionTechnical.ProjectionCriteria = tblParameters.Code
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	AND	tblParameters.Type = 'ProjectionCriteria' and tblParameters.IdLanguage = @IdLanguage
	GROUP BY tblParameters.Name, tblCalcProjectionTechnical.Year;

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #ChartList (ParentName NVARCHAR(255), YearProjection INT) ON [PRIMARY]
	INSERT INTO #ChartList ([ParentName],[YearProjection])
	SELECT CNList.ParentName, CYList.Year FROM
		(SELECT [ParentName] FROM #Chart GROUP BY [ParentName])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList
	WHERE CYList.Year BETWEEN @StartYear AND @EndYear
	
	/** Add Any Missing Years to Make the Chart Fill Properly (eg. Fields with only Cyclical Drivers) **/
	INSERT INTO #Chart ([ParentName],[YearProjection],[CostProjection])
	SELECT #ChartList.ParentName, #ChartList.YearProjection, 0 AS CostProjection FROM
		#ChartList LEFT OUTER JOIN
		#Chart ON #ChartList.ParentName=#Chart.ParentName AND #ChartList.YearProjection=#Chart.YearProjection
	WHERE #Chart.YearProjection IS NULL;	
	END /** Build Source Data Table - Chart **/
	
	BEGIN /** Prepare Totals, BL and BO Data for Screen Output (Grouped rather than per field) **/
	CREATE TABLE #TotalScreen (ProjectId NVARCHAR(50), ProjectionCriteria NVARCHAR(255), YearProjection INT, Total FLOAT, CostProjection FLOAT, SortOrder INT) ON [PRIMARY]
	CREATE TABLE #BLScreen (ProjectId NVARCHAR(50), ProjectionCriteria NVARCHAR(255), YearProjection INT, Total FLOAT, CostProjection FLOAT, SortOrder INT) ON [PRIMARY]
	CREATE TABLE #BOScreen (ProjectId NVARCHAR(50), ProjectionCriteria NVARCHAR(255), YearProjection INT, Total FLOAT, CostProjection FLOAT, SortOrder INT) ON [PRIMARY]
	CREATE TABLE #TotalScreenFields (ProjectId NVARCHAR(50), ProjectionCriteria NVARCHAR(255), YearProjection INT, Total FLOAT, CostProjection FLOAT, SortOrder INT) ON [PRIMARY]
	CREATE TABLE #BLScreenFields (ProjectId NVARCHAR(50), ProjectionCriteria NVARCHAR(255), YearProjection INT, Total FLOAT, CostProjection FLOAT, SortOrder INT) ON [PRIMARY]
	CREATE TABLE #BOScreenFields (ProjectId NVARCHAR(50), ProjectionCriteria NVARCHAR(255), YearProjection INT, Total FLOAT, CostProjection FLOAT, SortOrder INT) ON [PRIMARY]

	INSERT INTO #TotalScreen (ProjectId, YearProjection, ProjectionCriteria, Total, CostProjection, SortOrder)
	SELECT 'Totals', YearProjection, ProjectionCriteria, SUM(Total) AS "Total", SUM(CostProjection) AS "CostProjection", SortOrder 
	FROM
	(
		SELECT 'BL' AS "ProjectType", ProjectionCriteria, YearProjection, SUM(Total) AS "Total", SUM(CostProjection) AS "CostProjection", SortOrder 
		FROM #BL 
		GROUP BY ProjectionCriteria,YearProjection,SortOrder 
		UNION
		SELECT 'BO', ProjectionCriteria, YearProjection, SUM(Total) AS "Total", SUM(CostProjection) AS "CostProjection", SortOrder 
		FROM #BO 
		GROUP BY ProjectionCriteria,YearProjection,SortOrder 
	) AS ScreenTotals
	GROUP BY ProjectionCriteria,YearProjection,SortOrder 
	ORDER BY SortOrder
	
	INSERT INTO #BLScreen (ProjectId, YearProjection, ProjectionCriteria, Total, CostProjection, SortOrder)
	SELECT 'BL', YearProjection, ProjectionCriteria, SUM(Total) AS "Total", SUM(CostProjection) AS "CostProjection", SortOrder 
	FROM #BL 
	GROUP BY ProjectionCriteria,YearProjection,SortOrder 
	ORDER BY SortOrder

	INSERT INTO #BOScreen (ProjectId, YearProjection, ProjectionCriteria, Total, CostProjection, SortOrder)
	SELECT 'BO', YearProjection, ProjectionCriteria, SUM(Total) AS "Total", SUM(CostProjection) AS "CostProjection", SortOrder 
	FROM #BO 
	GROUP BY ProjectionCriteria,YearProjection,SortOrder 
	ORDER BY SortOrder

	INSERT INTO #TotalScreenFields (ProjectId, ProjectionCriteria, YearProjection, Total, CostProjection, SortOrder)
	SELECT Field, ProjectionCriteria, YearProjection, SUM(Total) AS "Total", SUM(CostProjection) AS "CostProjection", SortOrder 
	FROM
	(
		SELECT 'BL' AS "ProjectType", Field, ProjectionCriteria, YearProjection, SUM(Total) AS "Total", SUM(CostProjection) AS "CostProjection", SortOrder 
		FROM #BL 
		GROUP BY Field,ProjectionCriteria,YearProjection,SortOrder 
		UNION
		SELECT 'BO', Field, ProjectionCriteria, YearProjection, SUM(Total) AS "Total", SUM(CostProjection) AS "CostProjection", SortOrder 
		FROM #BO 
		GROUP BY Field,ProjectionCriteria,YearProjection,SortOrder 
	) AS ScreenTotals
	GROUP BY Field,ProjectionCriteria,YearProjection,SortOrder 
	ORDER BY Field,SortOrder
	
	INSERT INTO #BLScreenFields (ProjectId, YearProjection, ProjectionCriteria, Total, CostProjection, SortOrder)
	SELECT Field, YearProjection, ProjectionCriteria, SUM(Total) AS "Total", SUM(CostProjection) AS "CostProjection", SortOrder 
	FROM #BL 
	GROUP BY Field,ProjectionCriteria,YearProjection,SortOrder 
	ORDER BY Field,SortOrder

	INSERT INTO #BOScreenFields (ProjectId, YearProjection, ProjectionCriteria, Total, CostProjection, SortOrder)
	SELECT Field, YearProjection, ProjectionCriteria, SUM(Total) AS "Total", SUM(CostProjection) AS "CostProjection", SortOrder 
	FROM #BO 
	GROUP BY Field,ProjectionCriteria,YearProjection,SortOrder 
	ORDER BY Field,SortOrder
	END /** Prepare Totals, BL and BO Data for Screen Output (Grouped rather than per field) **/
	
	BEGIN /** Add Unit Cost factor, IF NECESSARY **/
	IF @IdCostType = 2
	BEGIN		 
		UPDATE	#BL
		SET		#BL.CostProjection = CASE WHEN BL.Amount = 0 THEN #BL.CostProjection ELSE #BL.CostProjection/BL.Amount END
		FROM	#BL INNER JOIN (SELECT #VolumeReportGroupedBL.YearProjection, #VolumeReportGroupedBL.Amount AS Amount FROM #VolumeReportGroupedBL) AS BL ON
				#BL.YearProjection = BL.YearProjection

		UPDATE	#BO
		SET		#BO.CostProjection = CASE WHEN BO.Amount = 0 THEN #BO.CostProjection ELSE #BO.CostProjection/BO.Amount END
		FROM	#BO INNER JOIN (SELECT #VolumeReportGroupedBO.YearProjection, #VolumeReportGroupedBO.Amount AS Amount FROM #VolumeReportGroupedBO) AS BO ON
				#BO.YearProjection = BO.YearProjection 

		UPDATE	#BLScreen
		SET		#BLScreen.CostProjection = CASE WHEN BL.Amount = 0 THEN #BLScreen.CostProjection ELSE #BLScreen.CostProjection/BL.Amount END
		FROM	#BLScreen INNER JOIN (SELECT #VolumeReportGroupedBL.YearProjection, #VolumeReportGroupedBL.Amount AS Amount FROM #VolumeReportGroupedBL) AS BL ON
				#BLScreen.YearProjection = BL.YearProjection

		UPDATE	#BOScreen
		SET		#BOScreen.CostProjection = CASE WHEN BO.Amount = 0 THEN #BOScreen.CostProjection ELSE #BOScreen.CostProjection/BO.Amount END
		FROM	#BOScreen INNER JOIN (SELECT #VolumeReportGroupedBO.YearProjection, #VolumeReportGroupedBO.Amount AS Amount FROM #VolumeReportGroupedBO) AS BO ON
				#BOScreen.YearProjection = BO.YearProjection 

		UPDATE	#TotalScreen
		SET		#TotalScreen.CostProjection = CASE WHEN BLBO.Amount = 0 THEN #TotalScreen.CostProjection ELSE #TotalScreen.CostProjection/BLBO.Amount END
		FROM	#TotalScreen INNER JOIN (SELECT #VolumeReportGrouped.YearProjection, #VolumeReportGrouped.Amount AS Amount FROM #VolumeReportGrouped) AS BLBO ON
				#TotalScreen.YearProjection = BLBO.YearProjection 

		UPDATE	#BLScreenFields
		SET		#BLScreenFields.CostProjection = CASE WHEN BL.Amount = 0 THEN #BLScreenFields.CostProjection ELSE #BLScreenFields.CostProjection/BL.Amount END
		FROM	#BLScreenFields INNER JOIN (SELECT #VolumeReportGroupedBL.YearProjection, #VolumeReportGroupedBL.Amount AS Amount FROM #VolumeReportGroupedBL) AS BL ON
				#BLScreenFields.YearProjection = BL.YearProjection

		UPDATE	#BOScreenFields
		SET		#BOScreenFields.CostProjection = CASE WHEN BO.Amount = 0 THEN #BOScreenFields.CostProjection ELSE #BOScreenFields.CostProjection/BO.Amount END
		FROM	#BOScreenFields INNER JOIN (SELECT #VolumeReportGroupedBO.YearProjection, #VolumeReportGroupedBO.Amount AS Amount FROM #VolumeReportGroupedBO) AS BO ON
				#BOScreenFields.YearProjection = BO.YearProjection 

		UPDATE	#TotalScreenFields
		SET		#TotalScreenFields.CostProjection = CASE WHEN BLBO.Amount = 0 THEN #TotalScreenFields.CostProjection ELSE #TotalScreenFields.CostProjection/BLBO.Amount END
		FROM	#TotalScreenFields INNER JOIN (SELECT #VolumeReportGrouped.YearProjection, #VolumeReportGrouped.Amount AS Amount FROM #VolumeReportGrouped) AS BLBO ON
				#TotalScreenFields.YearProjection = BLBO.YearProjection 

		UPDATE	#Chart
		SET		#Chart.CostProjection = CASE WHEN #VolumeReportGrouped.Amount = 0 THEN #Chart.CostProjection * 0 ELSE #Chart.CostProjection/#VolumeReportGrouped.Amount END
		FROM	#Chart INNER JOIN #VolumeReportGrouped ON
				#Chart.YearProjection = #VolumeReportGrouped.YearProjection
	END
	END /** Add Unit Cost factor, IF NECESSARY **/
	
	BEGIN /** Build Output Table **/
	CREATE TABLE #ExportReport (Field NVARCHAR(50), SortOrder INT, ProjectId NVARCHAR(50), ProjectionCriteria NVARCHAR(255)) ON [PRIMARY]
	/** Build Year Columns **/
	DECLARE @sqlReport nvarchar(4000)
	SET @YearFrom=@From	
	WHILE (@YearFrom <= @To)
	BEGIN
		SET @sqlReport = 'ALTER TABLE #ExportReport ADD "' + CAST(@YearFrom AS VARCHAR(4)) + '" FLOAT';
		EXEC (@sqlReport);
		SET @YearFrom = @YearFrom + 1;
	END

	BEGIN /** Tables[0]: Screen Totals **/
	DECLARE @SQLOpexTotals NVARCHAR(MAX);
	SET @SQLOpexTotals = 'SELECT [ProjectId],[ProjectionCriteria],' + @YearColumnList + ' FROM #TotalScreen PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [SortOrder]';
	EXEC(@SQLOpexTotals);
	END /** Tables[0]: Screen Totals **/
	
	BEGIN /** Tables[1]: Screen Baseline **/
	DECLARE @SQLOpexBL NVARCHAR(MAX);
	SET @SQLOpexBL = 'SELECT [ProjectId],[ProjectionCriteria],' + @YearColumnList + ' FROM #BLScreen PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [SortOrder]';
	EXEC(@SQLOpexBL);
	END /** Tables[1]: Screen Baseline **/
		
	BEGIN /** Tables[2]: Screen Incremental Line **/
	DECLARE @SQLOpexBO NVARCHAR(MAX);
	SET @SQLOpexBO = 'SELECT [ProjectId],[ProjectionCriteria],' + @YearColumnList + ' FROM #BOScreen PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [SortOrder]';
	EXEC(@SQLOpexBO);
	END /** Tables[2]: Screen Incremental Line **/
	
	BEGIN /** Tables[3]: Export Baseline **/
	DECLARE @SQLOpexBLExport NVARCHAR(MAX);
	SET @SQLOpexBLExport = 'WITH Pivoted AS (SELECT [Field],[Project],[YearProjection],[Total],' + @ProjectionCriteriaList + ' FROM #BL PIVOT (SUM(CostProjection) FOR [ProjectionCriteria] IN (' + @ProjectionCriteriaList + ')) AS PivotTable ) SELECT [Field],[Project],[YearProjection],' + @ProjectionCriteriaList2 + ' FROM Pivoted GROUP BY [Field],[Project],[YearProjection]';
	EXEC(@SQLOpexBLExport);
	END /** Tables[3]: Export Baseline **/

	BEGIN /** Tables[4]:Export Incremental Line **/
	DECLARE @SQLOpexBOExport NVARCHAR(MAX);
	SET @SQLOpexBOExport = 'WITH Pivoted AS (SELECT [Field],[Project],[YearProjection],[Total],' + @ProjectionCriteriaList + ' FROM #BO PIVOT (SUM(CostProjection) FOR [ProjectionCriteria] IN (' + @ProjectionCriteriaList + ')) AS PivotTable ) SELECT [Field],[Project],[YearProjection],' + @ProjectionCriteriaList2 + ' FROM Pivoted GROUP BY [Field],[Project],[YearProjection]';
	EXEC(@SQLOpexBOExport);
	END /** Tables[4]:Export Incremental Line **/
		
	BEGIN /** Tables[5]: Grand Total Technical Drivers **/
	INSERT INTO #VolumeReportGrouped ([ParentName], [Name], [YearProjection], [Amount])
	SELECT 'All Cost Accounts',#SizeDrivers.Name, #SizeDrivers.YearProjection,
			CASE WHEN @IdTechnicalScenario=1 THEN ISNULL(#SizeDrivers.Normal,0)
				WHEN @IdTechnicalScenario=2 THEN ISNULL(#SizeDrivers.Optimistic,0)
				WHEN @IdTechnicalScenario=3 THEN ISNULL(#SizeDrivers.Pessimistic,0)
			ELSE 0 END AS Amount
	FROM #SizeDrivers LEFT OUTER JOIN #VolumeReportGrouped ON
		#SizeDrivers.Name = #VolumeReportGrouped.Name AND #SizeDrivers.YearProjection = #VolumeReportGrouped.YearProjection
	WHERE #VolumeReportGrouped.YearProjection IS NULL

	INSERT INTO #VolumeReportGrouped ([ParentName], [Name], [YearProjection], [Amount])
	SELECT 'All Cost Accounts',#PerformanceDrivers.Name, #PerformanceDrivers.YearProjection,
			CASE WHEN @IdTechnicalScenario=1 THEN ISNULL(#PerformanceDrivers.Normal,0)
				WHEN @IdTechnicalScenario=2 THEN ISNULL(#PerformanceDrivers.Optimistic,0)
				WHEN @IdTechnicalScenario=3 THEN ISNULL(#PerformanceDrivers.Pessimistic,0)
			ELSE 0 END AS Amount
	FROM #PerformanceDrivers LEFT OUTER JOIN #VolumeReportGrouped ON
		#PerformanceDrivers.Name = #VolumeReportGrouped.Name AND #PerformanceDrivers.YearProjection = #VolumeReportGrouped.YearProjection
	WHERE #VolumeReportGrouped.YearProjection IS NULL
	
	DECLARE @SQLVolumeTotal NVARCHAR(MAX);
	SET @SQLVolumeTotal = 'SELECT [ParentName] AS ProjectId, [Name] AS ProjectionCriteria,' + @YearColumnList + ' FROM #VolumeReportGrouped PIVOT (SUM(Amount) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
	EXEC(@SQLVolumeTotal);
	END /** Tables[5]: Grand Total Technical Drivers **/
	
	BEGIN /** Tables[6]: Total Baseline Technical drivers **/
	CREATE TABLE #TechDriversBL ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #TechDriversBL ([ParentName], [Name], [YearProjection], [Amount])
	SELECT	/*N'All Cost Accounts'*/ dbo.tblFields.Name AS ParentName, dbo.tblTechnicalDrivers.Name AS Name,
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			SUM(CASE WHEN @IdTechnicalScenario=1 THEN ISNULL(dbo.tblTechnicalDriverData.Normal,0)
				WHEN @IdTechnicalScenario=2 THEN ISNULL(dbo.tblTechnicalDriverData.Optimistic,0)
				WHEN @IdTechnicalScenario=3 THEN ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0)
			ELSE 0 END) AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject AND
			dbo.tblTechnicalDriverData.Year >= dbo.tblProjects.Starts INNER JOIN
			dbo.tblFields ON dbo.tblTechnicalDriverData.IdField = dbo.tblFields.IdField
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblProjects.[Operation]=1
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	GROUP BY dbo.tblTechnicalDrivers.Name, dbo.tblTechnicalDriverData.Year, dbo.tblFields.Name
	ORDER BY ParentName, dbo.tblTechnicalDrivers.Name, dbo.tblTechnicalDriverData.Year

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #TechDriversBLList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #TechDriversBLList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportGrouped GROUP BY [ParentName],[Name] HAVING [ParentName] <> 'All Cost Accounts')AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel AND [Year] BETWEEN @StartYear AND @EndYear) AS CYList 

	/** Add Any Missing Years to #TechDriversBL to Fill Properly **/
	INSERT INTO #TechDriversBL ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#TechDriversBLList.ParentName,#TechDriversBLList.Name,#TechDriversBLList.YearProjection,0 AS Amount
	FROM	#TechDriversBLList LEFT OUTER JOIN
			#TechDriversBL ON #TechDriversBLList.ParentName=#TechDriversBL.ParentName AND #TechDriversBLList.Name=#TechDriversBL.Name AND #TechDriversBLList.YearProjection=#TechDriversBL.YearProjection
	WHERE	#TechDriversBL.YearProjection IS NULL;
	
	DECLARE @SQLVolumeBL NVARCHAR(MAX);
	SET @SQLVolumeBL = 'WITH Pivoted AS (SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #TechDriversBL PIVOT (SUM(Amount) FOR [YearProjection] IN (' + @YearColumnList + ')) AS PivotTable ) SELECT ''All Cost Accounts'' AS ProjectId, [Name] AS ProjectionCriteria,' + @YearColumnListSum + ' FROM Pivoted GROUP BY [Name]';
	EXEC(@SQLVolumeBL);
	END /** Tables[6]: Total Baseline Technical drivers **/
	
	BEGIN /** Tables[7]: Total Incremental Line Technical Drivers **/
	CREATE TABLE #TechDriversBO ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #TechDriversBO ([ParentName], [Name], [YearProjection], [Amount])
	SELECT	/*DISTINCT N'All Cost Accounts'*/ dbo.tblFields.Name AS ParentName, dbo.tblTechnicalDrivers.Name AS Name,
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			SUM(CASE WHEN @IdTechnicalScenario=1 THEN ISNULL(dbo.tblTechnicalDriverData.Normal,0)
				WHEN @IdTechnicalScenario=2 THEN ISNULL(dbo.tblTechnicalDriverData.Optimistic,0)
				WHEN @IdTechnicalScenario=3 THEN ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0)
			ELSE 0 END) AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject AND
			dbo.tblTechnicalDriverData.Year >= dbo.tblProjects.Starts INNER JOIN
			dbo.tblFields ON dbo.tblTechnicalDriverData.IdField = dbo.tblFields.IdField
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblProjects.[Operation]=2
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	GROUP BY dbo.tblTechnicalDrivers.Name, dbo.tblTechnicalDriverData.Year, dbo.tblFields.Name
	ORDER BY ParentName, dbo.tblTechnicalDrivers.Name, dbo.tblTechnicalDriverData.Year

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #TechDriversBOList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #TechDriversBOList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportGrouped GROUP BY [ParentName],[Name] HAVING [ParentName] <> 'All Cost Accounts')AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel AND [Year] BETWEEN @StartYear AND @EndYear) AS CYList 

	/** Add Any Missing Years to #TechDriversBOList to Fill Properly **/
	INSERT INTO #TechDriversBO ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#TechDriversBOList.ParentName,#TechDriversBOList.Name,#TechDriversBOList.YearProjection,0 AS Amount
	FROM	#TechDriversBOList LEFT OUTER JOIN
			#TechDriversBO ON #TechDriversBOList.ParentName=#TechDriversBO.ParentName AND #TechDriversBOList.Name=#TechDriversBO.Name AND #TechDriversBOList.YearProjection=#TechDriversBO.YearProjection
	WHERE	#TechDriversBO.YearProjection IS NULL;

	DECLARE @SQLVolumeBO NVARCHAR(MAX);
	SET @SQLVolumeBO = 'WITH Pivoted AS (SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #TechDriversBO PIVOT (SUM(Amount) FOR [YearProjection] IN (' + @YearColumnList + ')) AS PivotTable ) SELECT ''All Cost Accounts'' AS ProjectId, [Name] AS ProjectionCriteria,' + @YearColumnListSum + ' FROM Pivoted GROUP BY [Name]';
	EXEC(@SQLVolumeBO);
	END /** Tables[7]: Total Incremental Line Technical Drivers **/

	CREATE TABLE #TechDriversAll ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #TechDriversAll ([ParentName], [Name], [YearProjection], [Amount])
	SELECT A.ParentName, A.Name, A.YearProjection, SUM(A.Amount)
	FROM
	(
	SELECT ParentName, Name, YearProjection, Amount
	FROM #TechDriversBL
	UNION
	SELECT ParentName, Name, YearProjection, Amount
	FROM #TechDriversBO
	) A
	GROUP BY A.ParentName, A.Name, A.YearProjection
	
	BEGIN /** Tables[8]: Chart Data **/
	DECLARE @SQLOpexChart NVARCHAR(MAX);
	SET @SQLOpexChart = 'SELECT [YearProjection] AS [Year],' + @ProjectionCriteriaList + ' FROM #Chart PIVOT (SUM(CostProjection) FOR ParentName IN (' + @ProjectionCriteriaList + ')) AS PivotTable ORDER BY [YearProjection]';
	EXEC(@SQLOpexChart);
	END /** Tables[8]: Chart Data **/
	
	BEGIN /** Tables[9]: Stats **/
	/** Output Stats Table for UI **/
	SELECT COUNT(*) AS ReportCriteriaCount FROM #ProjectionCriteriaList
	END /** Tables[9]: Stats **/

	BEGIN /** Tables [10 (Grand Totals)], [11 (Fields)]: Export Report **/
	/** Total Projection **/
	SET @sqlReport = 'INSERT INTO #ExportReport SELECT ''Grand Total'',1, ''Total Projection'' AS ProjectId,[ProjectionCriteria],' + @YearColumnList + ' FROM #TotalScreen PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [SortOrder]';
	EXEC(@sqlReport);
	SET @sqlReport = 'INSERT INTO #ExportReport SELECT ''Grand Total'',1, ''Total Projection'' AS ProjectId, ''zzzTotal'' AS [ProjectionCriteria],' + @YearColumnListSum + ' FROM #ExportReport WHERE SortOrder = 1';
	EXEC(@sqlReport);
	SET @sqlReport = 'INSERT INTO #ExportReport SELECT ProjectId,1, ''Total Projection'',[ProjectionCriteria],' + @YearColumnList + ' FROM #TotalScreenFields PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [SortOrder]';
	EXEC(@sqlReport);
	SET @sqlReport = 'WITH Pivoted AS (SELECT [ProjectId],[ProjectionCriteria],' + @YearColumnList + ' FROM #TotalScreenFields PIVOT (SUM(CostProjection) FOR [YearProjection] IN (' + @YearColumnList + ')) AS PivotTable ) INSERT INTO #ExportReport SELECT [ProjectId] AS ''Field'',1 AS ''SortOrder'',''Total Projection'' AS ''Project Id'',''zzzTotal'' AS ''ProjectionCriteria'',' + @YearColumnListSum + ' FROM Pivoted GROUP BY [ProjectId]';
	EXEC(@sqlReport);

	/** Grand Total Technical Drivers **/
	SET @sqlReport = 'WITH Pivoted AS (SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #TechDriversAll PIVOT (SUM(Amount) FOR [YearProjection] IN (' + @YearColumnList + ')) AS PivotTable ) INSERT INTO #ExportReport SELECT ''Grand Total'',2 AS ''SortOrder'',''Grand Total Technical Drivers'' AS ''Project Id'',Name AS ''ProjectionCriteria'',' + @YearColumnListSum + ' FROM Pivoted GROUP BY [Name]';
	EXEC(@sqlReport);
	SET @sqlReport = 'WITH Pivoted AS (SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #TechDriversAll PIVOT (SUM(Amount) FOR [YearProjection] IN (' + @YearColumnList + ')) AS PivotTable ) INSERT INTO #ExportReport SELECT ParentName AS ''Field'',2 AS ''SortOrder'',''Grand Total Technical Drivers'' AS ''Project Id'',Name AS ''ProjectionCriteria'',' + @YearColumnListSum + ' FROM Pivoted GROUP BY [ParentName], [Name]';
	EXEC(@sqlReport);

	/** Total Projection for Baseline**/
	SET @sqlReport = 'INSERT INTO #ExportReport SELECT ''Grand Total'',3, ''Total Projection for Baseline'' AS ProjectId, [ProjectionCriteria],' + @YearColumnList + ' FROM #BLScreen PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [SortOrder]';
	EXEC(@sqlReport);
	SET @sqlReport = 'INSERT INTO #ExportReport SELECT ''Grand Total'',3, ''Total Projection for Baseline'' AS ProjectId, ''zzzTotal'' AS [ProjectionCriteria],' + @YearColumnListSum + ' FROM #ExportReport WHERE SortOrder = 3';
	EXEC(@sqlReport);
	SET @sqlReport = 'INSERT INTO #ExportReport SELECT ProjectId,3, ''Total Projection for Baseline'',[ProjectionCriteria],' + @YearColumnList + ' FROM #BLScreenFields PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [SortOrder]';
	EXEC(@sqlReport);
	SET @sqlReport = 'WITH Pivoted AS (SELECT [ProjectId],[ProjectionCriteria],' + @YearColumnList + ' FROM #BLScreenFields PIVOT (SUM(CostProjection) FOR [YearProjection] IN (' + @YearColumnList + ')) AS PivotTable ) INSERT INTO #ExportReport SELECT [ProjectId] AS ''Field'',3 AS ''SortOrder'',''Total Projection for Baseline'' AS ''Project Id'',''zzzTotal'' AS ''ProjectionCriteria'',' + @YearColumnListSum + ' FROM Pivoted GROUP BY [ProjectId]';
	EXEC(@sqlReport);

	/** Total Baseline Technical Drivers **/
	SET @sqlReport = 'WITH Pivoted AS (SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #TechDriversBL PIVOT (SUM(Amount) FOR [YearProjection] IN (' + @YearColumnList + ')) AS PivotTable ) INSERT INTO #ExportReport SELECT ''Grand Total'',4 AS ''SortOrder'',''Total Baseline Technical Drivers'' AS ''Project Id'',Name AS ''ProjectionCriteria'',' + @YearColumnListSum + ' FROM Pivoted GROUP BY [Name]';
	EXEC(@sqlReport);
	SET @sqlReport = 'WITH Pivoted AS (SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #TechDriversBL PIVOT (SUM(Amount) FOR [YearProjection] IN (' + @YearColumnList + ')) AS PivotTable ) INSERT INTO #ExportReport SELECT ParentName AS ''Field'',4 AS ''SortOrder'',''Total Baseline Technical Drivers'' AS ''Project Id'',Name AS ''ProjectionCriteria'',' + @YearColumnListSum + ' FROM Pivoted GROUP BY [ParentName], [Name]';
	EXEC(@sqlReport);
	
	/** Total Projection for Incremental Line **/
	SET @sqlReport = 'INSERT INTO #ExportReport SELECT ''Grand Total'',5, ''Total Projection for Incremental Line'' AS ProjectId, [ProjectionCriteria],' + @YearColumnList + ' FROM #BOScreen PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [SortOrder]';
	EXEC(@sqlReport);
	SET @sqlReport = 'INSERT INTO #ExportReport SELECT ''Grand Total'',5, ''Total Projection for Incremental Line'' AS ProjectId, ''zzzTotal'' AS [ProjectionCriteria],' + @YearColumnListSum + ' FROM #ExportReport WHERE SortOrder = 5';
	EXEC(@sqlReport);
	SET @sqlReport = 'INSERT INTO #ExportReport SELECT ProjectId,5, ''Total Projection for Incremental Line'',[ProjectionCriteria],' + @YearColumnList + ' FROM #BOScreenFields PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [SortOrder]';
	EXEC(@sqlReport);
	SET @sqlReport = 'WITH Pivoted AS (SELECT [ProjectId],[ProjectionCriteria],' + @YearColumnList + ' FROM #BOScreenFields PIVOT (SUM(CostProjection) FOR [YearProjection] IN (' + @YearColumnList + ')) AS PivotTable ) INSERT INTO #ExportReport SELECT [ProjectId] AS ''Field'',5 AS ''SortOrder'',''Total Projection for Incremental Line'' AS ''Project Id'',''zzzTotal'' AS ''ProjectionCriteria'',' + @YearColumnListSum + ' FROM Pivoted GROUP BY [ProjectId]';
	EXEC(@sqlReport);

	/**Total Incremental Line Technical Drivers **/
	SET @sqlReport = 'WITH Pivoted AS (SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #TechDriversBO PIVOT (SUM(Amount) FOR [YearProjection] IN (' + @YearColumnList + ')) AS PivotTable ) INSERT INTO #ExportReport SELECT ''Grand Total'',6 AS ''SortOrder'',''Total Incremental Line Technical Drivers'' AS ''Project Id'',Name AS ''ProjectionCriteria'',' + @YearColumnListSum + ' FROM Pivoted GROUP BY [Name]';
	EXEC(@sqlReport);
	SET @sqlReport = 'WITH Pivoted AS (SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #TechDriversBO PIVOT (SUM(Amount) FOR [YearProjection] IN (' + @YearColumnList + ')) AS PivotTable ) INSERT INTO #ExportReport SELECT ParentName AS ''Field'',6 AS ''SortOrder'',''Total Incremental Line Technical Drivers'' AS ''Project Id'',Name AS ''ProjectionCriteria'',' + @YearColumnListSum + ' FROM Pivoted GROUP BY [ParentName], [Name]';
	EXEC(@sqlReport);

	/** Grand Total Export **/
	SELECT * FROM #ExportReport WHERE Field = 'Grand Total' ORDER BY SortOrder, Field, ProjectionCriteria
	
	/** Fields Export **/
	SELECT * FROM #ExportReport WHERE Field <> 'Grand Total' ORDER BY Field, SortOrder, ProjectionCriteria
	
	END /** Tables [10 (Grand Total)], [11 (Fields)]: Export Report **/
	
	END /** Build Output Table **/

END

GO

/****** Object:  StoredProcedure [dbo].[reppOpexProjectionExport]    Script Date: 02/18/2016 09:18:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[reppOpexProjectionExport]    Script Date: 03/23/2016 10:24:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[reppOpexProjectionExport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[reppOpexProjectionExport]
GO

-- ===========================================================================================
-- Author:		Rodrigo Manubens
-- Create date:	2016-02-04
-- Description:	Report Opex Projection Export
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- ===========================================================================================
CREATE PROCEDURE [dbo].[reppOpexProjectionExport](
@IdModel INT,
@IdAggregationLevel INT,
@IdField NVARCHAR(MAX),
@IdStructure INT,
@From INT,
@To INT,
@IdTechnicalScenario INT,
@IdEconomicScenario INT,
@IdCurrency INT,
@IdTerm INT,
@IdTechnicalDriver INT,
@IdCostType INT,
@IdTypeOperation INT,
@Factor INT
)
AS
BEGIN

	BEGIN /** DROP Temporary Tables **/
	IF OBJECT_ID('tempdb..#FieldList') IS NOT NULL DROP TABLE #FieldList
	IF OBJECT_ID('tempdb..#AccountList') IS NOT NULL DROP TABLE #AccountList
	IF OBJECT_ID('tempdb..#TechDriver') IS NOT NULL DROP TABLE #TechDriver
	IF OBJECT_ID('tempdb..#VolumeReportBLRows') IS NOT NULL DROP TABLE #VolumeReportBLRows  
	IF OBJECT_ID('tempdb..#VolumeReportBL') IS NOT NULL DROP TABLE #VolumeReportBL  
	IF OBJECT_ID('tempdb..#VolumeReportBORows') IS NOT NULL DROP TABLE #VolumeReportBORows
	IF OBJECT_ID('tempdb..#VolumeReportBO') IS NOT NULL DROP TABLE #VolumeReportBO
	IF OBJECT_ID('tempdb..#VolumeReportBLList') IS NOT NULL DROP TABLE #VolumeReportBLList 
	IF OBJECT_ID('tempdb..#VolumeReportBOList') IS NOT NULL DROP TABLE #VolumeReportBOList 
	IF OBJECT_ID('tempdb..#VolumeReportGrouped') IS NOT NULL DROP TABLE #VolumeReportGrouped
	IF OBJECT_ID('tempdb..#VolumeReportGroupedBL') IS NOT NULL DROP TABLE #VolumeReportGroupedBL 
	IF OBJECT_ID('tempdb..#VolumeReportGroupedBO') IS NOT NULL DROP TABLE #VolumeReportGroupedBO 
	IF OBJECT_ID('tempdb..#ExportOpexBL') IS NOT NULL DROP TABLE #ExportOpexBL
	IF OBJECT_ID('tempdb..#ExportOpexBLSubtotal') IS NOT NULL DROP TABLE #ExportOpexBLSubtotal
	IF OBJECT_ID('tempdb..#ExportOpexBO') IS NOT NULL DROP TABLE #ExportOpexBO
	IF OBJECT_ID('tempdb..#ExportOpexBOSubtotal') IS NOT NULL DROP TABLE #ExportOpexBOSubtotal
	IF OBJECT_ID('tempdb..#ExportOpexTotal') IS NOT NULL DROP TABLE #ExportOpexTotal
	IF OBJECT_ID('tempdb..#ExportOpexAll') IS NOT NULL DROP TABLE #ExportOpexAll
	IF OBJECT_ID('tempdb..#ExportOpexGrand') IS NOT NULL DROP TABLE #ExportOpexGrand
	IF OBJECT_ID('tempdb..#ZiffAccounts') IS NOT NULL DROP TABLE #ZiffAccounts
	END /** DROP Temporary Tables **/

	BEGIN /** Build Lists **/
	/** Technical Scenario List **/
	DECLARE @CaseTypeList NVARCHAR(MAX) = 'Baseline, Incremental' 

	DECLARE @StartYear INT = @From;
	DECLARE @EndYear INT = @To;
	
	/** Get Base Year **/
	DECLARE @BaseYear INT
	SELECT @BaseYear = [BaseYear] FROM [tblModels] WHERE IdModel=@IdModel

	/** Build Year Columns **/
	DECLARE @YearColumnList NVARCHAR(MAX)='';	
	DECLARE @YearFrom INT
	SET @YearFrom=@From	
	WHILE (@YearFrom <= @To)
	BEGIN
		SET @YearColumnList =  @YearColumnList + '[' + CAST(@YearFrom AS VARCHAR(4)) + ']';
		SET @YearFrom = @YearFrom + 1;
		IF (@YearFrom <= @To)
		BEGIN
			SET @YearColumnList = @YearColumnList + ',';
		END
	END
	
	/** Build Field List **/
	/** Need to check whether there are more than one field being passed in, look for comma in input variable **/
	DECLARE @nonAll int 
	SELECT @nonAll = CHARINDEX(',', @IdField);
	DECLARE @SQL1 AS NVARCHAR(MAX)

	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF (@nonAll = 0) /** Not multiple fields **/
	BEGIN	
		IF (@IdAggregationLevel=0 AND cast(@IdField AS INT)=0) /** Single field and All was selected, Aggregation is All **/
		BEGIN
			INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdModel]=@IdModel GROUP BY [IdField];
		END
		ELSE IF (@IdAggregationLevel>0 AND cast(@IdField AS INT)>0) /** Single fields and specifc selected, Aggregation specific **/
		BEGIN
			INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
		END
		ELSE IF (@IdAggregationLevel>0) /** Specific Aggregation with All fields **/
		BEGIN
			INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdModel]=@IdModel GROUP BY [IdField];
		END
		ELSE IF (cast(@IdField AS INT)>0)
		BEGIN
			INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
		END
	END
	ELSE /** Multiple fields **/
	BEGIN
		IF (@IdAggregationLevel=0) /** Multiple fields and Aggregation All was selected **/
		BEGIN
			SET @SQL1 = 'INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdModel]= ' + cast(@IdModel AS VARCHAR(10)) + ' AND [IdField] IN (' + @IdField + ') GROUP BY [IdField];'
		END
		ELSE IF (@IdAggregationLevel>0) /** Multiple fields and specifc Aggregation was selected **/
		BEGIN
			SET @SQL1 = 'INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]= ' + cast(@IdAggregationLevel AS VARCHAR(10)) + ' AND [IdField] IN (' + @IdField + ') AND [IdModel]=' + cast(@IdModel AS VARCHAR(10)) + ' GROUP BY [IdField];'
		END
		EXEC(@SQL1)
	END

	DECLARE @FieldCount INT;
	SELECT @FieldCount=COUNT([IdField]) FROM #FieldList
	
	/** Get Chart Columns to Temp Table **/
	CREATE TABLE #AccountList (ColumnName NVARCHAR(255) PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF @IdStructure=1
	BEGIN
		INSERT INTO #AccountList SELECT [RootZiffAccount] FROM tblCalcProjectionTechnical WHERE IdField IN (SELECT [IdField] FROM #FieldList) AND [IdModel]=@IdModel GROUP BY [RootZiffAccount] ORDER BY [RootZiffAccount];
	END
	ELSE IF @IdStructure=2
	BEGIN
		INSERT INTO #AccountList SELECT [Activity] FROM tblCalcProjectionTechnical WHERE IdField IN (SELECT [IdField] FROM #FieldList) AND [IdModel]=@IdModel GROUP BY [Activity], [SortOrder] ORDER BY [SortOrder], [Activity];
	END
	
	/** Generate Chart Columns from Temp Table **/
	DECLARE @AccountColumnList NVARCHAR(MAX)='';
	DECLARE @AccountName NVARCHAR(255);
	DECLARE AccountList CURSOR FOR
	SELECT ColumnName FROM #AccountList;

	OPEN AccountList;
	FETCH NEXT FROM AccountList INTO @AccountName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @AccountColumnList=''
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + '[' + @AccountName + ']';
		END
		ELSE
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + ',[' + @AccountName + ']';
		END
		FETCH NEXT FROM AccountList INTO @AccountName;
	END

	CLOSE AccountList;
	DEALLOCATE AccountList;
	
	/** Get Currency Factor to use **/
	DECLARE @CurrencyFactor FLOAT;
	SELECT @CurrencyFactor=[Value] FROM tblModelsCurrencies WHERE [IdCurrency]=@IdCurrency AND [IdModel]=@IdModel;
	
	/** Build Technical Driver Factors (used for KPI Report) **/
	CREATE TABLE #TechDriver ([Year] INT, [Normal] FLOAT, [Optimistic] FLOAT, [Pessimistic] FLOAT) ON [PRIMARY]
	IF @IdTechnicalDriver=0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, 1 AS [Normal], 1 AS [Optimistic], 1 AS [Pessimistic]
		FROM tblCalcModelYears
		WHERE [IdModel]=@IdModel AND [Year] BETWEEN @StartYear AND @EndYear;
	END
	ELSE IF @IdTechnicalDriver>0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, SUM([Normal]) AS [Normal], SUM([Pessimistic]) AS [Pessimistic], SUM([Optimistic]) AS [Optimistic]
		FROM tblTechnicalDriverData
		WHERE [IdTechnicalDriver]=@IdTechnicalDriver AND [IdModel]=@IdModel AND IdField IN (SELECT [IdField] FROM #FieldList)
		GROUP BY Year;
	END
	END  /** Build Lists **/
	
	BEGIN /** Build Volume Tables **/
	/** Report (Baseline)**/
	CREATE TABLE #VolumeReportBLRows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLRows ([ParentName], [Name], [YearProjection], [Amount])	
	SELECT	DISTINCT N'All Cost Accounts' AS ParentName, dbo.tblTechnicalDrivers.Name AS Name,
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Normal,0))
				WHEN @IdTechnicalScenario=2 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Optimistic,0))
				WHEN @IdTechnicalScenario=3 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0))
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject AND
			dbo.tblTechnicalDriverData.Year >= dbo.tblProjects.Starts
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=1
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	GROUP BY  dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.Name
	ORDER BY dbo.tblTechnicalDriverData.Year
	
	CREATE TABLE #VolumeReportBL ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBL([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBLRows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBLList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBL WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #VolumeReportBLList to Fill Properly **/
	INSERT INTO #VolumeReportBL ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBLList.ParentName,#VolumeReportBLList.Name, #VolumeReportBLList.YearProjection,0 AS Amount
	FROM	#VolumeReportBLList LEFT OUTER JOIN
			#VolumeReportBL ON #VolumeReportBLList.ParentName=#VolumeReportBL.ParentName AND #VolumeReportBLList.Name=#VolumeReportBL.Name AND #VolumeReportBLList.YearProjection=#VolumeReportBL.YearProjection
	WHERE	#VolumeReportBL.YearProjection IS NULL;

	/** Delete all rows that have 0 (zero) Amount **/
	DELETE #VolumeReportBL WHERE Amount=0

	/** Report (Business Opportunities)**/
	CREATE TABLE #VolumeReportBORows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBORows ([ParentName], [Name], [YearProjection], [Amount])	
	SELECT	DISTINCT N'All Cost Accounts' AS ParentName, dbo.tblTechnicalDrivers.Name AS Name,
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Normal,0))
				WHEN @IdTechnicalScenario=2 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Optimistic,0))
				WHEN @IdTechnicalScenario=3 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0))
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject AND
			dbo.tblTechnicalDriverData.Year >= dbo.tblProjects.Starts
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=2
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	GROUP BY  dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.Name
	ORDER BY dbo.tblTechnicalDriverData.Year

	CREATE TABLE #VolumeReportBO ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBO([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBORows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBOList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBOList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBO WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #VolumeReportBOList to Fill Properly **/
	INSERT INTO #VolumeReportBO ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBOList.ParentName,#VolumeReportBOList.Name, #VolumeReportBOList.YearProjection,0 AS Amount
	FROM	#VolumeReportBOList LEFT OUTER JOIN
			#VolumeReportBO ON #VolumeReportBOList.ParentName=#VolumeReportBO.ParentName AND #VolumeReportBOList.Name=#VolumeReportBO.Name AND #VolumeReportBOList.YearProjection=#VolumeReportBO.YearProjection
	WHERE	#VolumeReportBO.YearProjection IS NULL;

	/** Delete all rows that have 0 (zero) Amount **/
	DELETE #VolumeReportBO WHERE Amount=0

	/** Report (All Fields Grouped)**/
	CREATE TABLE #VolumeReportGroupedBL ([YearProjection] INT, [Amount] FLOAT, [ParentName] NVARCHAR(255), [Name] NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBL ([YearProjection], [Amount], [ParentName], [Name])
	SELECT YearProjection,SUM(amount) AS Amount,'GroupedBL',Name
	FROM #VolumeReportBL
	GROUP BY YearProjection,Name
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGroupedBO ([YearProjection] INT, [Amount] FLOAT, [ParentName] NVARCHAR(255), [Name] NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBO ([YearProjection], [Amount], [ParentName], [Name])
	SELECT YearProjection,SUM(amount) AS Amount,'GroupedBO',Name
	FROM #VolumeReportBO
	GROUP BY YearProjection,Name
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGrouped ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255),[YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGrouped ([ParentName], [Name], [YearProjection], [Amount])
	SELECT N'All Cost Accounts' AS ParentName,Name,YearProjection,SUM(Amount) AS 'Amount'
	FROM
	(
		SELECT 'VolumeReportGroupedBL' AS "ProjectType", ParentName, Name, YearProjection,SUM(Amount) AS "Amount"
		FROM #VolumeReportGroupedBL 
		GROUP BY ParentName,Name,YearProjection
		UNION
		SELECT 'VolumeReportGroupedBO', ParentName, Name, YearProjection, SUM(Amount) AS "Amount"
		FROM #VolumeReportGroupedBO 
		GROUP BY ParentName,Name,YearProjection
	) AS VolumeReportGroupedTotals
	GROUP BY Name,YearProjection
	ORDER BY Name,YearProjection
	END /** Build Volume Tables **/
	
	DECLARE @TotalCapacityDriver INT = 0;
	/** Get grouped driver value for Base Year **/
	SELECT @TotalCapacityDriver = Amount FROM #VolumeReportGrouped WHERE YearProjection=@BaseYear

	BEGIN /** Build Export Data Table - Baselines (SortOrder=3) **/
	CREATE TABLE #ExportOpexBL (Field NVARCHAR(255), ParentName NVARCHAR(255), CostCategory NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT, SortOrder INT, SortID NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #ExportOpexBL ([Field], [ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID])
	SELECT tblCalcProjectionTechnical.Field, tblProjects.Code AS ParentName, tblCalcProjectionTechnical.CodeZiff AS CostCategory, tblCalcProjectionTechnical.ZiffAccount AS Name, tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, 3 AS SortOrder
		, tblCalcProjectionTechnical.RootCodeZiff + '_' + tblCalcProjectionTechnical.CodeZiff AS SortID
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel INNER JOIN
		tblProjects ON tblCalcProjectionTechnical.IdProject = tblProjects.IdProject
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel 
	AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) 
	AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	AND tblCalcProjectionTechnical.Operation = 1
	GROUP BY tblCalcProjectionTechnical.Field,tblProjects.Code, tblCalcProjectionTechnical.CodeZiff,tblCalcProjectionTechnical.ZiffAccount,tblCalcProjectionTechnical.Year
	, tblCalcProjectionTechnical.RootCodeZiff + '_' + tblCalcProjectionTechnical.CodeZiff
	UNION
	SELECT tblCalcProjectionTechnical.Field, tblProjects.Code AS ParentName,tblCalcProjectionTechnical.RootCodeZiff AS CostCategory, tblCalcProjectionTechnical.RootZiffAccount AS Name, tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, 3 AS SortOrder
		, tblCalcProjectionTechnical.RootCodeZiff AS SortID
	FROM tblCalcProjectionTechnical  LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel INNER JOIN
		tblProjects ON tblCalcProjectionTechnical.IdProject = tblProjects.IdProject
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel 
	AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) 
	AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	AND tblCalcProjectionTechnical.Operation = 1
	GROUP BY tblCalcProjectionTechnical.Field, tblProjects.Code,tblCalcProjectionTechnical.RootCodeZiff,tblCalcProjectionTechnical.RootZiffAccount,tblCalcProjectionTechnical.Year
	, tblCalcProjectionTechnical.RootCodeZiff

	/** Get Full List of ZiffAccounts **/
	CREATE TABLE #ZiffAccounts (Field NVARCHAR(255), ParentName NVARCHAR(255), CostCategory NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT, SortOrder INT, SortID NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #ZiffAccounts ([Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID])
	SELECT CNList.Field, CNList.ParentName, CYList.Code, CYList.Name, CNList.YearProjection, 0 AS CostProjection, 3 AS SortOrder, CYList.SortOrder AS SortID
	FROM
		(SELECT [Field],[ParentName],[YearProjection] FROM #ExportOpexBL WHERE [ParentName] IS NOT NULL GROUP BY [Field],[ParentName],[YearProjection])AS CNList CROSS JOIN
		(SELECT [Code],[Name],[SortOrder] FROM tblZiffAccounts WHERE IdModel=@IdModel) AS CYList 
	
	/** Add Any Missing ZiffAccounts to #ExportOpexBL to Fill Properly **/
	INSERT INTO #ExportOpexBL ([Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID])
	SELECT	#ZiffAccounts.Field,#ZiffAccounts.ParentName,#ZiffAccounts.CostCategory,#ZiffAccounts.Name, #ZiffAccounts.YearProjection, 0 AS CostProjection, 3 AS SortOrder, #ZiffAccounts.SortID
	FROM	#ZiffAccounts LEFT OUTER JOIN
			#ExportOpexBL ON #ZiffAccounts.Field=#ExportOpexBL.Field AND #ZiffAccounts.ParentName=#ExportOpexBL.ParentName AND #ZiffAccounts.Name=#ExportOpexBL.Name AND #ZiffAccounts.CostCategory=#ExportOpexBL.CostCategory AND #ZiffAccounts.YearProjection=#ExportOpexBL.YearProjection
	WHERE	#ExportOpexBL.YearProjection IS NULL;
	END /** Build Export Data Table - Baselines **/

	BEGIN /** Build Export Data Table - Baselines Subtotal (SortOrder=2) **/
	CREATE TABLE #ExportOpexBLSubtotal (Field NVARCHAR(255), ParentName NVARCHAR(255), CostCategory NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT, SortOrder INT, SortID NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #ExportOpexBLSubtotal ([Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID])
	SELECT tblCalcProjectionTechnical.Field, 'Total Baseline' AS ParentName,tblCalcProjectionTechnical.CodeZiff AS CostCategory, tblCalcProjectionTechnical.ZiffAccount AS Name, tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, 2 AS SortOrder, tblCalcProjectionTechnical.RootCodeZiff + '_' + tblCalcProjectionTechnical.CodeZiff AS SortID
	FROM tblCalcProjectionTechnical  LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel 
	AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) 
	AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	AND tblCalcProjectionTechnical.Operation = 1
	GROUP BY tblCalcProjectionTechnical.Field,tblCalcProjectionTechnical.CodeZiff,tblCalcProjectionTechnical.ZiffAccount,tblCalcProjectionTechnical.Year, tblCalcProjectionTechnical.RootCodeZiff + '_' + tblCalcProjectionTechnical.CodeZiff
	UNION
	SELECT tblCalcProjectionTechnical.Field, 'Total Baseline'  AS ParentName,tblCalcProjectionTechnical.RootCodeZiff AS CostCategory, tblCalcProjectionTechnical.RootZiffAccount AS Name, tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, 2 AS SortOrder, tblCalcProjectionTechnical.RootCodeZiff AS SortID
	FROM tblCalcProjectionTechnical  LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel 
	AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) 
	AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	AND tblCalcProjectionTechnical.Operation = 1
	GROUP BY tblCalcProjectionTechnical.Field,tblCalcProjectionTechnical.RootCodeZiff,tblCalcProjectionTechnical.RootZiffAccount,tblCalcProjectionTechnical.Year, tblCalcProjectionTechnical.RootCodeZiff

	/** Get Full List of ZiffAccounts **/
	DELETE #ZiffAccounts
	INSERT INTO #ZiffAccounts ([Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID])
	SELECT CNList.Field, CNList.ParentName, CYList.Code, CYList.Name, CNList.YearProjection, 0 AS CostProjection, 2 AS SortOrder, CYList.SortOrder AS SortID
	FROM
		(SELECT [Field],[ParentName],[YearProjection] FROM #ExportOpexBLSubtotal WHERE [ParentName] IS NOT NULL GROUP BY [Field],[ParentName],[YearProjection])AS CNList CROSS JOIN
		(SELECT [Code],[Name],[SortOrder] FROM tblZiffAccounts WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing ZiffAccounts to #ExportOpexBLSubtotal to Fill Properly **/
	INSERT INTO #ExportOpexBLSubtotal ([Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID])
	SELECT	#ZiffAccounts.Field,#ZiffAccounts.ParentName,#ZiffAccounts.CostCategory,#ZiffAccounts.Name, #ZiffAccounts.YearProjection, 0 AS CostProjection, 2 AS SortOrder, #ZiffAccounts.SortID
	FROM	#ZiffAccounts LEFT OUTER JOIN
			#ExportOpexBLSubtotal ON #ZiffAccounts.Field=#ExportOpexBLSubtotal.Field AND #ZiffAccounts.ParentName=#ExportOpexBLSubtotal.ParentName AND #ZiffAccounts.Name=#ExportOpexBLSubtotal.Name AND #ZiffAccounts.CostCategory=#ExportOpexBLSubtotal.CostCategory AND #ZiffAccounts.YearProjection=#ExportOpexBLSubtotal.YearProjection
	WHERE	#ExportOpexBLSubtotal.YearProjection IS NULL;
	END /** Build Export Data Table - Baselines Subtotal**/
	
	BEGIN /** Build Export Data Table - Business Opportunities (SortOrder=5) **/
	CREATE TABLE #ExportOpexBO (Field NVARCHAR(255),ParentName NVARCHAR(255), CostCategory NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT, SortOrder INT, SortID NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #ExportOpexBO ([Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID])
	SELECT tblCalcProjectionTechnical.Field, tblProjects.Code AS ParentName, tblCalcProjectionTechnical.CodeZiff AS CostCategory, tblCalcProjectionTechnical.ZiffAccount AS Name, tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, 5 AS SortOrder, tblCalcProjectionTechnical.RootCodeZiff + '_' + tblCalcProjectionTechnical.CodeZiff AS SortID
	FROM tblCalcProjectionTechnical  LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel INNER JOIN
		tblProjects ON tblCalcProjectionTechnical.IdProject = tblProjects.IdProject
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel 
	AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) 
	AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	AND tblCalcProjectionTechnical.Operation = 2
	GROUP BY tblCalcProjectionTechnical.Field,tblProjects.Code, tblCalcProjectionTechnical.CodeZiff,tblCalcProjectionTechnical.ZiffAccount,tblCalcProjectionTechnical.Year, tblCalcProjectionTechnical.RootCodeZiff + '_' + tblCalcProjectionTechnical.CodeZiff
	UNION
	SELECT tblCalcProjectionTechnical.Field,tblProjects.Code AS ParentName,tblCalcProjectionTechnical.RootCodeZiff AS CostCategory, tblCalcProjectionTechnical.RootZiffAccount AS Name, tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, 5 AS SortOrder, tblCalcProjectionTechnical.RootCodeZiff AS SortID
	FROM tblCalcProjectionTechnical  LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel INNER JOIN
		tblProjects ON tblCalcProjectionTechnical.IdProject = tblProjects.IdProject
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel 
	AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) 
	AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	AND tblCalcProjectionTechnical.Operation = 2
	GROUP BY tblCalcProjectionTechnical.Field,tblProjects.Code,tblCalcProjectionTechnical.RootCodeZiff,tblCalcProjectionTechnical.RootZiffAccount,tblCalcProjectionTechnical.Year, tblCalcProjectionTechnical.RootCodeZiff

	/** Get Full List of ZiffAccounts **/
	DELETE #ZiffAccounts
	INSERT INTO #ZiffAccounts ([Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID])
	SELECT CNList.Field, CNList.ParentName, CYList.Code, CYList.Name, CNList.YearProjection, 0 AS CostProjection, 5 AS SortOrder, CYList.SortOrder AS SortID
	FROM
		(SELECT [Field],[ParentName],[YearProjection] FROM #ExportOpexBO WHERE [ParentName] IS NOT NULL GROUP BY [Field],[ParentName],[YearProjection])AS CNList CROSS JOIN
		(SELECT [Code],[Name],[SortOrder] FROM tblZiffAccounts WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing ZiffAccounts to #ExportOpexBO to Fill Properly **/
	INSERT INTO #ExportOpexBO ([Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID])
	SELECT	#ZiffAccounts.Field,#ZiffAccounts.ParentName,#ZiffAccounts.CostCategory,#ZiffAccounts.Name, #ZiffAccounts.YearProjection,0 AS CostProjection, 5 AS SortOrder, #ZiffAccounts.SortID
	FROM	#ZiffAccounts LEFT OUTER JOIN
			#ExportOpexBO ON #ZiffAccounts.Field=#ExportOpexBO.Field AND #ZiffAccounts.ParentName=#ExportOpexBO.ParentName AND #ZiffAccounts.Name=#ExportOpexBO.Name AND #ZiffAccounts.CostCategory=#ExportOpexBO.CostCategory AND #ZiffAccounts.YearProjection=#ExportOpexBO.YearProjection
	WHERE	#ExportOpexBO.YearProjection IS NULL;
	END /** Build Export Data Table - Business Opportunities **/

	BEGIN /** Build Export Data Table - Business Opportunities Subtotal (SortOrder=4) **/
	CREATE TABLE #ExportOpexBOSubtotal (Field NVARCHAR(255),ParentName NVARCHAR(255), CostCategory NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT, SortOrder INT, SortID NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #ExportOpexBOSubtotal ([Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID])
	SELECT tblCalcProjectionTechnical.Field, 'Total Incremental' AS ParentName,tblCalcProjectionTechnical.CodeZiff AS CostCategory, tblCalcProjectionTechnical.ZiffAccount AS Name, tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, 4 AS SortOrder, tblCalcProjectionTechnical.RootCodeZiff + '_' + tblCalcProjectionTechnical.CodeZiff AS SortID
	FROM tblCalcProjectionTechnical  LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel 
	AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) 
	AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	AND tblCalcProjectionTechnical.Operation = 2
	GROUP BY tblCalcProjectionTechnical.Field,tblCalcProjectionTechnical.CodeZiff,tblCalcProjectionTechnical.ZiffAccount,tblCalcProjectionTechnical.Year, tblCalcProjectionTechnical.RootCodeZiff + '_' + tblCalcProjectionTechnical.CodeZiff
	UNION
	SELECT tblCalcProjectionTechnical.Field, 'Total Incremental' AS ParentName,tblCalcProjectionTechnical.RootCodeZiff  AS CostCategory, tblCalcProjectionTechnical.RootZiffAccount AS Name, tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, 4 AS SortOrder, tblCalcProjectionTechnical.RootCodeZiff AS SortID
	FROM tblCalcProjectionTechnical  LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel 
	AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) 
	AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	AND tblCalcProjectionTechnical.Operation = 2
	GROUP BY tblCalcProjectionTechnical.Field,tblCalcProjectionTechnical.RootCodeZiff,tblCalcProjectionTechnical.RootZiffAccount,tblCalcProjectionTechnical.Year, tblCalcProjectionTechnical.RootCodeZiff

	/** Get Full List of ZiffAccounts **/
	DELETE #ZiffAccounts
	INSERT INTO #ZiffAccounts ([Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID])
	SELECT CNList.Field, CNList.ParentName, CYList.Code, CYList.Name, CNList.YearProjection, 0 AS CostProjection, 4 AS SortOrder, CYList.SortOrder AS SortID 
	FROM
		(SELECT [Field],[ParentName],[YearProjection] FROM #ExportOpexBOSubtotal WHERE [ParentName] IS NOT NULL GROUP BY [Field],[ParentName],[YearProjection])AS CNList CROSS JOIN
		(SELECT [Code],[Name],[SortOrder] FROM tblZiffAccounts WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing ZiffAccounts to #ExportOpexBOSubtotal to Fill Properly **/
	INSERT INTO #ExportOpexBOSubtotal ([Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID])
	SELECT	#ZiffAccounts.Field, #ZiffAccounts.ParentName,#ZiffAccounts.CostCategory,#ZiffAccounts.Name, #ZiffAccounts.YearProjection, 0 AS CostProjection, 4 AS SortOrder, #ZiffAccounts.SortID
	FROM	#ZiffAccounts LEFT OUTER JOIN
			#ExportOpexBOSubtotal ON #ZiffAccounts.Field=#ExportOpexBOSubtotal.Field AND #ZiffAccounts.ParentName=#ExportOpexBOSubtotal.ParentName AND #ZiffAccounts.Name=#ExportOpexBOSubtotal.Name AND #ZiffAccounts.CostCategory=#ExportOpexBOSubtotal.CostCategory AND #ZiffAccounts.YearProjection=#ExportOpexBOSubtotal.YearProjection
	WHERE	#ExportOpexBOSubtotal.YearProjection IS NULL;
	END /** Build Export Data Table - Business Opportunities Subtotal**/

	BEGIN /** Build Export Data Table - Total (SortOrder=1) **/
	CREATE TABLE #ExportOpexTotal (Field NVARCHAR(255),ParentName NVARCHAR(255), CostCategory NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT, SortOrder INT, SortID NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #ExportOpexTotal ([Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID])
	SELECT tblCalcProjectionTechnical.Field, 'Total Field' AS ParentName,tblCalcProjectionTechnical.CodeZiff AS CostCategory, tblCalcProjectionTechnical.ZiffAccount AS Name, tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, 1 AS SortOrder, tblCalcProjectionTechnical.RootCodeZiff + '_' + tblCalcProjectionTechnical.CodeZiff AS SortID
	FROM tblCalcProjectionTechnical  LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel 
	AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) 
	AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	GROUP BY tblCalcProjectionTechnical.Field,tblCalcProjectionTechnical.CodeZiff,tblCalcProjectionTechnical.ZiffAccount,tblCalcProjectionTechnical.Year, tblCalcProjectionTechnical.RootCodeZiff + '_' + tblCalcProjectionTechnical.CodeZiff
	UNION
	SELECT tblCalcProjectionTechnical.Field, 'Total Field' AS ParentName,tblCalcProjectionTechnical.RootCodeZiff AS CostCategory, tblCalcProjectionTechnical.RootZiffAccount AS Name, tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, 1 AS SortOrder, tblCalcProjectionTechnical.RootCodeZiff AS SortID
	FROM tblCalcProjectionTechnical  LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel 
	AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) 
	AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	GROUP BY tblCalcProjectionTechnical.Field,tblCalcProjectionTechnical.RootCodeZiff,tblCalcProjectionTechnical.RootZiffAccount,tblCalcProjectionTechnical.Year, tblCalcProjectionTechnical.RootCodeZiff

	/** Get Full List of ZiffAccounts **/
	DELETE #ZiffAccounts
	INSERT INTO #ZiffAccounts ([Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID])
	SELECT CNList.Field, CNList.ParentName, CYList.Code, CYList.Name, CNList.YearProjection, 0 AS CostProjection, 1 AS SortOrder, CYList.SortOrder AS SortID
	FROM
		(SELECT [Field],[ParentName],[YearProjection] FROM #ExportOpexTotal WHERE [ParentName] IS NOT NULL GROUP BY [Field],[ParentName],[YearProjection])AS CNList CROSS JOIN
		(SELECT [Code],[Name],[SortOrder] FROM tblZiffAccounts WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing ZiffAccounts to #ExportOpexTotal to Fill Properly **/
	INSERT INTO #ExportOpexTotal ([Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID])
	SELECT	#ZiffAccounts.Field,#ZiffAccounts.ParentName,#ZiffAccounts.CostCategory,#ZiffAccounts.Name, #ZiffAccounts.YearProjection, 0 AS CostProjection, 1 AS SortOrder, #ZiffAccounts.SortID
	FROM	#ZiffAccounts LEFT OUTER JOIN
			#ExportOpexTotal ON #ZiffAccounts.Field=#ExportOpexTotal.Field AND #ZiffAccounts.ParentName=#ExportOpexTotal.ParentName AND #ZiffAccounts.Name=#ExportOpexTotal.Name AND #ZiffAccounts.CostCategory=#ExportOpexTotal.CostCategory AND #ZiffAccounts.YearProjection=#ExportOpexTotal.YearProjection
	WHERE	#ExportOpexTotal.YearProjection IS NULL;
	END /** Build Export Data Table - Total **/
	
	BEGIN /** Add Unit Cost factor, IF NECESSARY **/
	IF @IdCostType = 2
	BEGIN		 
		UPDATE	#ExportOpexBL
		SET		#ExportOpexBL.CostProjection = CASE WHEN #VolumeReportGrouped.Amount = 0 THEN #ExportOpexBL.CostProjection * 0 ELSE #ExportOpexBL.CostProjection/#VolumeReportGrouped.Amount END
		FROM	#ExportOpexBL INNER JOIN #VolumeReportGrouped ON
				#ExportOpexBL.YearProjection = #VolumeReportGrouped.YearProjection

		UPDATE	#ExportOpexBLSubtotal
		SET		#ExportOpexBLSubtotal.CostProjection = CASE WHEN #VolumeReportGrouped.Amount = 0 THEN #ExportOpexBLSubtotal.CostProjection * 0 ELSE #ExportOpexBLSubtotal.CostProjection/#VolumeReportGrouped.Amount END
		FROM	#ExportOpexBLSubtotal INNER JOIN #VolumeReportGrouped ON
				#ExportOpexBLSubtotal.YearProjection = #VolumeReportGrouped.YearProjection

		UPDATE	#ExportOpexBO
		SET		#ExportOpexBO.CostProjection = CASE WHEN #VolumeReportGrouped.Amount = 0 THEN #ExportOpexBO.CostProjection * 0 ELSE #ExportOpexBO.CostProjection/#VolumeReportGrouped.Amount END
		FROM	#ExportOpexBO INNER JOIN #VolumeReportGrouped ON
				#ExportOpexBO.YearProjection = #VolumeReportGrouped.YearProjection

		UPDATE	#ExportOpexBOSubtotal
		SET		#ExportOpexBOSubtotal.CostProjection = CASE WHEN #VolumeReportGrouped.Amount = 0 THEN #ExportOpexBOSubtotal.CostProjection * 0 ELSE #ExportOpexBOSubtotal.CostProjection/#VolumeReportGrouped.Amount END
		FROM	#ExportOpexBOSubtotal INNER JOIN #VolumeReportGrouped ON
				#ExportOpexBOSubtotal.YearProjection = #VolumeReportGrouped.YearProjection

		UPDATE	#ExportOpexTotal
		SET		#ExportOpexTotal.CostProjection = CASE WHEN #VolumeReportGrouped.Amount = 0 THEN #ExportOpexTotal.CostProjection * 0 ELSE #ExportOpexTotal.CostProjection/#VolumeReportGrouped.Amount END
		FROM	#ExportOpexTotal INNER JOIN #VolumeReportGrouped ON
				#ExportOpexTotal.YearProjection = #VolumeReportGrouped.YearProjection
	END
	END /** Add Unit Cost factor, IF NECESSARY **/
	
	BEGIN /** Build Export Data Table - All **/
	CREATE TABLE #ExportOpexAll (Field NVARCHAR(255),ParentName NVARCHAR(255), CostCategory NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT, SortOrder INT, SortID NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #ExportOpexAll ([Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID])
	SELECT [Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID]
	FROM #ExportOpexBL

	INSERT INTO #ExportOpexAll ([Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID])
	SELECT [Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID]
	FROM #ExportOpexBLSubtotal
	
	INSERT INTO #ExportOpexAll ([Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID])
	SELECT [Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID]
	FROM #ExportOpexBO
	
	INSERT INTO #ExportOpexAll ([Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID])
	SELECT [Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID]
	FROM #ExportOpexBOSubtotal
	
	INSERT INTO #ExportOpexAll ([Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID])
	SELECT [Field],[ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortID]
	FROM #ExportOpexTotal

	CREATE TABLE #ExportOpexGrand (ParentName NVARCHAR(255), CostCategory NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT, SortOrder INT, SortId NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #ExportOpexGrand ([ParentName],[CostCategory],[Name],[YearProjection],[CostProjection],[SortOrder],[SortId])
	SELECT [ParentName],[CostCategory],[Name],[YearProjection],SUM([CostProjection]) AS CostProjection,[SortOrder],[SortID]
	FROM #ExportOpexAll
	GROUP BY [ParentName],[CostCategory],[Name],[YearProjection],[SortOrder],[SortId]

	END /** Build Export Data Table - All **/
	
	BEGIN /** Build Output Table **/
	DECLARE @SQLOpexAll NVARCHAR(MAX);
	SET @SQLOpexAll = 'SELECT [SortId],[Field],[SortOrder],[ParentName],[CostCategory],[Name],' + @YearColumnList + ' FROM #ExportOpexAll PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [Field],[SortOrder],[ParentName],[SortID]';
	EXEC(@SQLOpexAll);

	DECLARE @SQLOpexGrand NVARCHAR(MAX);
	SET @SQLOpexGrand = 'SELECT [SortId],[SortOrder],[ParentName],[CostCategory],[Name],' + @YearColumnList + ' FROM #ExportOpexGrand PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [SortOrder],[ParentName],[SortID]';
	EXEC(@SQLOpexGrand);
	END /** Build Output Table **/
	
END

GO

/****** Object:  StoredProcedure [dbo].[reppOpexProjectionOperationalMargins]    Script Date: 03/23/2016 10:23:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[reppOpexProjectionOperationalMargins]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[reppOpexProjectionOperationalMargins]
GO

/****** Object:  StoredProcedure [dbo].[reppOpexProjectionOperationalMargins]    Script Date: 02/18/2016 09:55:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:		Rodrigo Manubens
-- Create date:	2016-02-04
-- Description:	Report Opex Projection Operational Margins
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- ===========================================================================================
CREATE PROCEDURE [dbo].[reppOpexProjectionOperationalMargins](
@IdModel INT,
@IdField NVARCHAR(MAX),
@From INT,
@To INT,
@IdTechnicalScenario INT,
@IdEconomicScenario INT,
@IdCurrency INT,
@IdTerm INT,
@IdTypeOperation INT,
@IdPriceScenario INT
)
AS
BEGIN

	BEGIN /** DROP Temporary Tables **/
	IF OBJECT_ID('tempdb..#FieldList') IS NOT NULL DROP TABLE #FieldList
	IF OBJECT_ID('tempdb..#AccountList') IS NOT NULL DROP TABLE #AccountList
	IF OBJECT_ID('tempdb..#VolumeReportBLRows') IS NOT NULL DROP TABLE #VolumeReportBLRows  
	IF OBJECT_ID('tempdb..#VolumeReportBL') IS NOT NULL DROP TABLE #VolumeReportBL  
	IF OBJECT_ID('tempdb..#VolumeReportBORows') IS NOT NULL DROP TABLE #VolumeReportBORows
	IF OBJECT_ID('tempdb..#VolumeReportBO') IS NOT NULL DROP TABLE #VolumeReportBO
	IF OBJECT_ID('tempdb..#VolumeReportBLList') IS NOT NULL DROP TABLE #VolumeReportBLList 
	IF OBJECT_ID('tempdb..#VolumeReportBOList') IS NOT NULL DROP TABLE #VolumeReportBOList 
	IF OBJECT_ID('tempdb..#VolumeReportGrouped') IS NOT NULL DROP TABLE #VolumeReportGrouped
	IF OBJECT_ID('tempdb..#VolumeReportGroupedBL') IS NOT NULL DROP TABLE #VolumeReportGroupedBL 
	IF OBJECT_ID('tempdb..#VolumeReportGroupedBO') IS NOT NULL DROP TABLE #VolumeReportGroupedBO 
	IF OBJECT_ID('tempdb..#ZiffAccounts') IS NOT NULL DROP TABLE #ZiffAccounts
	IF OBJECT_ID('tempdb..#Prices') IS NOT NULL DROP TABLE #Prices
	IF OBJECT_ID('tempdb..#UnitCosts') IS NOT NULL DROP TABLE #UnitCosts
	IF OBJECT_ID('tempdb..#AbsoluteCost') IS NOT NULL DROP TABLE #AbsoluteCost
	IF OBJECT_ID('tempdb..#Production') IS NOT NULL DROP TABLE #Production
	IF OBJECT_ID('tempdb..#Totals') IS NOT NULL DROP TABLE #Totals
	IF OBJECT_ID('tempdb..#Opex') IS NOT NULL DROP TABLE #Opex
	IF OBJECT_ID('tempdb..#YearList') IS NOT NULL DROP TABLE #YearList
	IF OBJECT_ID('tempdb..#FieldsFillers') IS NOT NULL DROP TABLE #FieldsFillers
	IF OBJECT_ID('tempdb..#ChartTotals') IS NOT NULL DROP TABLE #ChartTotals
	IF OBJECT_ID('tempdb..#AbsoluteRevenue') IS NOT NULL DROP TABLE #AbsoluteRevenue
	IF OBJECT_ID('tempdb..#UnitMargin') IS NOT NULL DROP TABLE #UnitMargin
	IF OBJECT_ID('tempdb..#UnitRevenue') IS NOT NULL DROP TABLE #UnitRevenue
	IF OBJECT_ID('tempdb..#AbsoluteMargin') IS NOT NULL DROP TABLE #AbsoluteMargin
	END /** DROP Temporary Tables **/
	
	BEGIN /** Lists **/
	/** Technical Scenario List **/
	DECLARE @CaseTypeList NVARCHAR(MAX) = 'Baseline, Incremental' 

	DECLARE @StartYear INT = @From;
	DECLARE @EndYear INT = @To;
	
	/** Get Base Year **/
	DECLARE @BaseYear INT
	SELECT @BaseYear = [BaseYear] FROM [tblModels] WHERE IdModel=@IdModel

	/** Get Base Currency **/
	DECLARE @BaseCurrency INT
	SELECT @BaseCurrency = IdCurrency FROM tblModelsCurrencies WHERE IdModel=@IdModel AND BaseCurrency=1	
	
	/** Get Currency Factor to use **/
	DECLARE @CurrencyFactor FLOAT;
	SELECT @CurrencyFactor = [Value] FROM tblModelsCurrencies WHERE [IdCurrency]=@IdCurrency AND [IdModel]=@IdModel;
	
	/** Get Currency Code **/
	DECLARE @CurrencyCode NVARCHAR(3)
	SELECT @CurrencyCode = Code FROM tblCurrencies WHERE IdCurrency = @IdCurrency
	
	/** Build Year Columns and Year List Table**/
	CREATE TABLE #YearList (ProjectionYear INT)
	DECLARE @YearColumnList NVARCHAR(MAX)='';	
	DECLARE @YearFrom INT
	SET @YearFrom=@From	
	WHILE (@YearFrom <= @To)
	BEGIN
		INSERT INTO #YearList Values(@YearFrom)
		SET @YearColumnList =  @YearColumnList + '[' + CAST(@YearFrom AS VARCHAR(4)) + ']';
		SET @YearFrom = @YearFrom + 1;
		IF (@YearFrom <= @To)
		BEGIN
			SET @YearColumnList = @YearColumnList + ',';
		END
	END

	/** Build Field List **/
	/** Need to check whether there are more than one field being passed in, look for comma in input variable **/
	DECLARE @nonAll int 
	SELECT @nonAll = CHARINDEX(',', @IdField);
	DECLARE @SQL1 AS NVARCHAR(MAX)

	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF (@nonAll = 0) /** Not multiple fields **/
	BEGIN	
		INSERT INTO #FieldList VALUES(@IdField)
	END
	ELSE /** Multiple fields **/
	BEGIN
		SET @SQL1 = 'INSERT INTO #FieldList SELECT [IdField] FROM tblFields WHERE [IdModel]= ' + cast(@IdModel AS VARCHAR(10)) + ' AND [IdField] IN (' + @IdField + ') GROUP BY [IdField];'
		EXEC(@SQL1)
	END

	DECLARE @FieldCount INT;
	SELECT @FieldCount=COUNT([IdField]) FROM #FieldList
	
	/** Generate Chart Columns from Temp Table **/
	DECLARE @FieldColumnList NVARCHAR(MAX)='';
	DECLARE @FieldName NVARCHAR(255);
	DECLARE @sqlField nvarchar(4000)
	DECLARE FieldList CURSOR FOR
	SELECT Name FROM [tblFields] where IdField in (SELECT IdField FROM #FieldList);

	OPEN FieldList;
	FETCH NEXT FROM FieldList INTO @FieldName;

	CREATE TABLE #ChartTotals (Year INT) ON [PRIMARY]

	IF (@FieldCount > 1)
	BEGIN
		SET @FieldColumnList = 'All';
		SET @sqlField = 'ALTER TABLE #ChartTotals ADD "All_UC" float; ALTER TABLE #ChartTotals ADD "All_NP" float; ALTER TABLE #ChartTotals ADD "All_OM" float;'
		EXEC(@sqlField)
	END		
	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @FieldColumnList=''
		BEGIN
			SET @FieldColumnList =  @FieldColumnList + '[' + @FieldName + ']';
		END
		ELSE
		BEGIN
			SET @FieldColumnList =  @FieldColumnList + ',[' + @FieldName + ']';
		END

		SET @sqlField = 'ALTER TABLE #ChartTotals ADD "' + @FieldName + '_UC" float; ALTER TABLE #ChartTotals ADD "' + @FieldName + '_NP" float; ALTER TABLE #ChartTotals ADD "' + @FieldName + '_OM" float;'
		EXEC(@sqlField)
		FETCH NEXT FROM FieldList INTO @FieldName;
	END

	CLOSE FieldList;
	DEALLOCATE FieldList;

	/** Get Chart Columns to Temp Table **/
	CREATE TABLE #AccountList (ColumnName NVARCHAR(255) PRIMARY KEY NOT NULL) ON [PRIMARY]
	INSERT INTO #AccountList SELECT [RootZiffAccount] FROM tblCalcProjectionTechnical WHERE IdField IN (SELECT [IdField] FROM #FieldList) AND [IdModel]=@IdModel GROUP BY [RootZiffAccount] ORDER BY [RootZiffAccount];
	
	/** Generate Chart Columns from Temp Table **/
	DECLARE @AccountColumnList NVARCHAR(MAX)='';
	DECLARE @AccountName NVARCHAR(255);
	DECLARE AccountList CURSOR FOR
	SELECT ColumnName FROM #AccountList;

	OPEN AccountList;
	FETCH NEXT FROM AccountList INTO @AccountName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @AccountColumnList=''
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + '[' + @AccountName + ']';
		END
		ELSE
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + ',[' + @AccountName + ']';
		END
		FETCH NEXT FROM AccountList INTO @AccountName;
	END

	CLOSE AccountList;
	DEALLOCATE AccountList;
	END /** Lists **/
	
	BEGIN /** Build Volume Tables **/
	/** Report (Baseline)**/
	CREATE TABLE #VolumeReportBLRows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLRows ([ParentName], [Name], [YearProjection], [Amount])	
	SELECT	/*N'All Cost Accounts'*/tblFields.Name AS ParentName, dbo.tblTechnicalDrivers.Name AS Name,
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Normal,0))
				WHEN @IdTechnicalScenario=2 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Optimistic,0))
				WHEN @IdTechnicalScenario=3 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0))
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject AND
			dbo.tblTechnicalDriverData.Year >= dbo.tblProjects.Starts INNER JOIN
			dbo.tblFields ON dbo.tblTechnicalDriverData.IdField=dbo.tblFields.IdField
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=1
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	GROUP BY  dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.Name, dbo.tblFields.Name
	ORDER BY dbo.tblTechnicalDriverData.Year

	CREATE TABLE #VolumeReportBL ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBL([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBLRows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBLList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBL WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #VolumeReportBLList to Fill Properly **/
	INSERT INTO #VolumeReportBL ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBLList.ParentName,#VolumeReportBLList.Name, #VolumeReportBLList.YearProjection,0 AS Amount
	FROM	#VolumeReportBLList LEFT OUTER JOIN
			#VolumeReportBL ON #VolumeReportBLList.ParentName=#VolumeReportBL.ParentName AND #VolumeReportBLList.Name=#VolumeReportBL.Name AND #VolumeReportBLList.YearProjection=#VolumeReportBL.YearProjection
	WHERE	#VolumeReportBL.YearProjection IS NULL AND #VolumeReportBL.YearProjection BETWEEN @StartYear AND @EndYear;

	/** Delete all rows that have 0 (zero) Amount **/
	DELETE #VolumeReportBL WHERE Amount=0

	/** Report (Business Opportunities)**/
	CREATE TABLE #VolumeReportBORows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBORows ([ParentName], [Name], [YearProjection], [Amount])	
	SELECT	/*N'All Cost Accounts'*/tblFields.Name, dbo.tblTechnicalDrivers.Name AS Name,
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Normal,0))
				WHEN @IdTechnicalScenario=2 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Optimistic,0))
				WHEN @IdTechnicalScenario=3 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0))
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject AND
			dbo.tblTechnicalDriverData.Year >= dbo.tblProjects.Starts INNER JOIN
			dbo.tblFields ON dbo.tblTechnicalDriverData.IdField=dbo.tblFields.IdField
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=2
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	GROUP BY  dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.Name, dbo.tblFields.Name
	ORDER BY dbo.tblTechnicalDriverData.Year

	CREATE TABLE #VolumeReportBO ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBO([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBORows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBOList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBOList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBO WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #VolumeReportBOList to Fill Properly **/
	INSERT INTO #VolumeReportBO ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBOList.ParentName,#VolumeReportBOList.Name, #VolumeReportBOList.YearProjection,0 AS Amount
	FROM	#VolumeReportBOList LEFT OUTER JOIN
			#VolumeReportBO ON #VolumeReportBOList.ParentName=#VolumeReportBO.ParentName AND #VolumeReportBOList.Name=#VolumeReportBO.Name AND #VolumeReportBOList.YearProjection=#VolumeReportBO.YearProjection
	WHERE	#VolumeReportBO.YearProjection IS NULL  AND #VolumeReportBO.YearProjection BETWEEN @StartYear AND @EndYear;;

	/** Delete all rows that have 0 (zero) Amount **/
	DELETE #VolumeReportBO WHERE Amount=0

	/** Report (All Fields Grouped)**/
	CREATE TABLE #VolumeReportGroupedBL ([YearProjection] INT, [Amount] FLOAT, [ParentName] NVARCHAR(255), [Name] NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBL ([YearProjection], [Amount], [ParentName], [Name])
	SELECT YearProjection,SUM(amount) AS Amount,ParentName,Name--'GroupedBL',Name-- 'Baseline'
	FROM #VolumeReportBL
	GROUP BY YearProjection,Name,ParentName
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGroupedBO ([YearProjection] INT, [Amount] FLOAT, [ParentName] NVARCHAR(255), [Name] NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBO ([YearProjection], [Amount], [ParentName], [Name])
	SELECT YearProjection,SUM(amount) AS Amount,ParentName,Name--'GroupedBO',Name-- 'Incremental'
	FROM #VolumeReportBO
	GROUP BY YearProjection,Name,ParentName
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGrouped ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255),[YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGrouped ([ParentName], [Name], [YearProjection], [Amount])
	SELECT N'All Cost Accounts' AS ParentName,Name,YearProjection,SUM(Amount) AS 'Amount'
	FROM
	(
		SELECT 'VolumeReportGroupedBL' AS "ProjectType", ParentName, Name, YearProjection,SUM(Amount) AS "Amount"
		FROM #VolumeReportGroupedBL 
		GROUP BY ParentName,Name,YearProjection
		UNION
		SELECT 'VolumeReportGroupedBO', ParentName, Name, YearProjection, SUM(Amount) AS "Amount"
		FROM #VolumeReportGroupedBO 
		GROUP BY ParentName,Name,YearProjection
	) AS VolumeReportGroupedTotals
	GROUP BY Name,YearProjection
	ORDER BY Name,YearProjection
	END  /** Build Volume Tables **/

	DECLARE @TotalCapacityDriver INT = 0;

	/** Get grouped driver value for Base Year **/
	SELECT @TotalCapacityDriver = Amount FROM #VolumeReportGrouped WHERE YearProjection=@BaseYear

	BEGIN /** Opex Source Data **/
	/** Build Source Data Table - Total **/
	CREATE TABLE #Opex (ParentName NVARCHAR(255), CostCategory NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #Opex ([ParentName],[CostCategory],[Name],[YearProjection],[CostProjection])
	SELECT tblCalcProjectionTechnical.Field AS ParentName, tblCalcProjectionTechnical.CodeZiff AS CostCategory, tblCalcProjectionTechnical.ZiffAccount AS Name, tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList)
	GROUP BY tblCalcProjectionTechnical.Field, tblCalcProjectionTechnical.CodeZiff, tblCalcProjectionTechnical.ZiffAccount, tblCalcProjectionTechnical.Year
	UNION
	SELECT tblCalcProjectionTechnical.Field AS ParentName, tblCalcProjectionTechnical.RootCodeZiff AS CostCategory, tblCalcProjectionTechnical.RootZiffAccount AS Name, tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList)
	GROUP BY tblCalcProjectionTechnical.Field, tblCalcProjectionTechnical.RootCodeZiff, tblCalcProjectionTechnical.RootZiffAccount, tblCalcProjectionTechnical.Year;

	/** Get Full List of ZiffAccounts **/
	CREATE TABLE #ZiffAccounts (ParentName NVARCHAR(255), CostCategory NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #ZiffAccounts ([ParentName],[CostCategory],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CYList.Code, CYList.Name, YList.ProjectionYear, 0 FROM
		(SELECT [ParentName] FROM #Opex WHERE [ParentName] IS NOT NULL GROUP BY [ParentName])AS CNList CROSS JOIN
		(SELECT [Code],[Name] FROM tblZiffAccounts WHERE IdModel=@IdModel) AS CYList CROSS JOIN
		(SELECT [projectionyear] FROM #YearList) AS YList

	/** Add Any Missing ZiffAccounts to #Opex to Fill Properly **/
	INSERT INTO #Opex ([ParentName],[CostCategory],[Name],[YearProjection],[CostProjection])
	SELECT	#ZiffAccounts.ParentName,#ZiffAccounts.CostCategory,#ZiffAccounts.Name, #ZiffAccounts.YearProjection,0 AS CostProjection
	FROM	#ZiffAccounts LEFT OUTER JOIN
			#Opex ON #ZiffAccounts.ParentName=#Opex.ParentName AND #ZiffAccounts.Name=#Opex.Name AND #ZiffAccounts.CostCategory=#Opex.CostCategory AND #ZiffAccounts.YearProjection=#Opex.YearProjection
	WHERE	#Opex.YearProjection IS NULL;
	END /** Opex Source Data **/
	
	BEGIN /** Absolute Cost **/
	CREATE TABLE #AbsoluteCost (FieldName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #AbsoluteCost ([FieldName],[YearProjection],[CostProjection])
	SELECT tblCalcProjectionTechnical.Field AS ParentName,tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection
	FROM tblCalcProjectionTechnical  LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel 
	AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) 
	AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	GROUP BY tblCalcProjectionTechnical.Field,tblCalcProjectionTechnical.Year

	/** Get Full List of ZiffAccounts **/
	CREATE TABLE #FieldsFillers (FieldName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #FieldsFillers ([FieldName],[YearProjection],[CostProjection])
	SELECT CYList.Name, YList.ProjectionYear, 0 FROM
		(SELECT [FieldName] FROM #AbsoluteCost WHERE [FieldName] IS NOT NULL GROUP BY [FieldName])AS CNList CROSS JOIN
		(SELECT [Name] FROM tblFields WHERE IdModel = @IdModel AND IdField IN (SELECT * FROM #FieldList)) AS CYList CROSS JOIN
		(SELECT [ProjectionYear] FROM #YearList) AS YList

	/** Add Any Missing Years to #AbsoluteCost to Fill Properly **/
	INSERT INTO #AbsoluteCost ([FieldName],[YearProjection],[CostProjection])
	SELECT	#FieldsFillers.FieldName, #FieldsFillers.YearProjection,0 AS CostProjection
	FROM	#FieldsFillers LEFT OUTER JOIN
			#AbsoluteCost ON #FieldsFillers.FieldName=#AbsoluteCost.FieldName AND #FieldsFillers.YearProjection=#AbsoluteCost.YearProjection
	WHERE	#AbsoluteCost.YearProjection IS NULL;
	END /** Absolute Cost **/

	BEGIN /** Production **/
	CREATE TABLE #Production (FieldName NVARCHAR(255),Name NVARCHAR(255), YearProjection INT, Amount FLOAT) ON [PRIMARY]

	--DECLARE FieldList CURSOR FOR
	--SELECT Name FROM [tblFields] where IdField in (SELECT IdField FROM #FieldList);

	--OPEN FieldList;
	--FETCH NEXT FROM FieldList INTO @FieldName;

	--WHILE @@FETCH_STATUS = 0
	--BEGIN		
	--	INSERT INTO #Production ([FieldName],[Name],[YearProjection],[Amount])
	--	SELECT @FieldName, Name, YearProjection,Amount/1000 FROM #VolumeReportGroupedBL--#VolumeReportGrouped
	--	FETCH NEXT FROM FieldList INTO @FieldName;

	--	INSERT INTO #Production ([FieldName],[Name],[YearProjection],[Amount])
	--	SELECT @FieldName, Name, YearProjection,Amount/1000 FROM #VolumeReportGroupedBO--#VolumeReportGrouped
	--	FETCH NEXT FROM FieldList INTO @FieldName;
	--END

	--CLOSE FieldList;
	--DEALLOCATE FieldList;
	INSERT INTO #Production ([FieldName],[Name],[YearProjection],[Amount])
	SELECT ParentName, Name, YearProjection,Amount/1000 FROM #VolumeReportGroupedBL--#VolumeReportGrouped

	INSERT INTO #Production ([FieldName],[Name],[YearProjection],[Amount])
	SELECT ParentName, Name, YearProjection,Amount/1000 FROM #VolumeReportGroupedBO--#VolumeReportGrouped

	END /** Production **/

	BEGIN /** Prices **/
	CREATE TABLE #Prices (FieldName NVARCHAR(255), Name NVARCHAR(255), PriceYear INT, NetPrice FLOAT) ON [PRIMARY]
	IF (@nonAll = 0) /** Not multiple fields **/
	BEGIN	
		INSERT INTO #Prices ([FieldName],[Name],[PriceYear],[NetPrice])
		SELECT [tblFields].Name AS FieldName,[vListPrices].Description,[vListPrices].[PriceYear],
			   CASE WHEN vListPrices.IdCurrency=@BaseCurrency THEN [vListPrices].[NetPrice] ELSE [vListPrices].[NetPrice] / MC.Value END
		  FROM [vListPrices] INNER JOIN 
			   [tblFields] ON
			   [vListPrices].IdField = [tblFields].IdField INNER JOIN
			   (
				 SELECT IdCurrency, Value from tblModelsCurrencies where IdModel=@Idmodel
			   ) MC ON MC.IdCurrency = vListPrices.IdCurrency
		 WHERE [vListPrices].IdModel=@IdModel AND [vListPrices].IdField<>0 AND [vListPrices].IdField IN (@IdField) AND [vListPrices].PriceYear BETWEEN  @StartYear AND @EndYear AND [vListPrices].IdPriceScenario=@IdPriceScenario
		UNION
		SELECT tblfields.[Name],[vListPrices].Description,vListPrices.[PriceYear],
			   CASE WHEN vListPrices.IdCurrency=@BaseCurrency THEN [vListPrices].[NetPrice] ELSE [vListPrices].[NetPrice] / MC.Value END
		  FROM [vListPrices] LEFT OUTER JOIN tblFields ON
				vListPrices.IdModel = tblFields.IdModel  INNER JOIN
			   (
				 SELECT IdCurrency, Value from tblModelsCurrencies where IdModel=@Idmodel
			   ) MC ON MC.IdCurrency = vListPrices.IdCurrency
		 WHERE vListPrices.IdModel=@IdModel AND vListPrices.IdField=0 AND tblFields.IdField IN (@IdField)
		   AND vListPrices.PriceYear NOT IN (SELECT [PriceYear] FROM [vListPrices] WHERE IdModel=@IdModel AND IdField<>0 AND IdField IN (@IdField)) AND vListPrices.IdField NOT IN (@IdField)
		   AND [vListPrices].PriceYear BETWEEN  @StartYear AND @EndYear
		   AND [vListPrices].IdPriceScenario=@IdPriceScenario
	END
	ELSE /** Multiple fields **/
	BEGIN
		DECLARE @FieldId NVARCHAR(255);
		DECLARE FieldList CURSOR FOR
		SELECT IdField FROM [tblFields] where IdField in (SELECT IdField FROM #FieldList);

		OPEN FieldList;
		FETCH NEXT FROM FieldList INTO @FieldId;

		WHILE @@FETCH_STATUS = 0
		BEGIN		
			INSERT INTO #Prices ([FieldName],[Name],[PriceYear],[NetPrice])
			SELECT [tblFields].Name AS FieldName,[vListPrices].Description,[vListPrices].[PriceYear],
				   CASE WHEN vListPrices.IdCurrency=@BaseCurrency THEN [vListPrices].[NetPrice] ELSE [vListPrices].[NetPrice] / MC.Value END
			  FROM [vListPrices] INNER JOIN 
				   [tblFields] ON
				   [vListPrices].IdField = [tblFields].IdField INNER JOIN
				   (
					 SELECT IdCurrency, Value from tblModelsCurrencies where IdModel=@Idmodel
				   ) MC ON MC.IdCurrency = vListPrices.IdCurrency
			 WHERE [vListPrices].IdModel=@IdModel AND [vListPrices].IdField<>0 AND [vListPrices].IdField IN (@FieldId) AND [vListPrices].PriceYear BETWEEN  @StartYear AND @EndYear AND [vListPrices].IdPriceScenario=@IdPriceScenario
			UNION
			SELECT tblfields.[Name],[vListPrices].Description,vListPrices.[PriceYear],
				   CASE WHEN vListPrices.IdCurrency=@BaseCurrency THEN [vListPrices].[NetPrice] ELSE [vListPrices].[NetPrice] / MC.Value END
			  FROM [vListPrices] LEFT OUTER JOIN tblFields ON
					vListPrices.IdModel = tblFields.IdModel INNER JOIN
				   (
					 SELECT IdCurrency, Value from tblModelsCurrencies where IdModel=@Idmodel
				   ) MC ON MC.IdCurrency = vListPrices.IdCurrency 
			 WHERE vListPrices.IdModel=@IdModel AND vListPrices.IdField=0 AND tblFields.IdField IN (@FieldId)
			  AND vListPrices.PriceYear NOT IN (SELECT [PriceYear] FROM [vListPrices] WHERE IdModel=@IdModel AND IdField<>0 AND IdField IN (@FieldId)) AND vListPrices.IdField NOT IN (@FieldId)
			  AND [vListPrices].PriceYear BETWEEN  @StartYear AND @EndYear
			  AND [vListPrices].IdPriceScenario=@IdPriceScenario
			  
			FETCH NEXT FROM FieldList INTO @FieldId;
		END

		CLOSE FieldList;
		DEALLOCATE FieldList;
	END
	END /** Prices **/

	BEGIN /** Unit Costs **/
	CREATE TABLE #UnitCosts (FieldName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #UnitCosts ([FieldName],[YearProjection],[CostProjection])
	SELECT #AbsoluteCost.FieldName,#AbsoluteCost.YearProjection,#AbsoluteCost.CostProjection/(SUM(#Production.Amount)*1000)
	FROM	#AbsoluteCost INNER JOIN #Production ON
			#AbsoluteCost.FieldName = #Production.FieldName AND
			#AbsoluteCost.YearProjection = #Production.YearProjection
	GROUP BY #AbsoluteCost.FieldName,#AbsoluteCost.YearProjection,#AbsoluteCost.CostProjection
	END /** Unit Costs **/

	BEGIN /** Absolute Revenue **/
	CREATE TABLE #AbsoluteRevenue (FieldName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #AbsoluteRevenue ([FieldName],[YearProjection],[CostProjection])
	SELECT #Prices.[FieldName],[YearProjection],((SUM(#Production.Amount) * #Prices.NetPrice * 1000))* @CurrencyFactor AS Revenue
	FROM #Prices INNER JOIN 
		 #Production ON #Prices.PriceYear = #Production.YearProjection AND #Prices.Name = #Production.Name AND #Prices.FieldName = #Production.FieldName
	GROUP BY #Prices.[FieldName],#Production.YearProjection,#Prices.NetPrice
	END /** Absolute Revenue **/
	
	BEGIN /** Unit Revenue **/
	CREATE TABLE #UnitRevenue (FieldName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #UnitRevenue ([FieldName],[YearProjection],[CostProjection])
	SELECT #AbsoluteRevenue.[FieldName],#AbsoluteRevenue.[YearProjection], (#AbsoluteRevenue.CostProjection * @CurrencyFactor) / ((SUM(#Production.Amount)* @CurrencyFactor) * 1000) AS Revenue
	FROM #AbsoluteRevenue INNER JOIN 
		 #Production ON #AbsoluteRevenue.FieldName = #Production.FieldName AND
		 #AbsoluteRevenue.YearProjection = #Production.YearProjection
	GROUP BY #AbsoluteRevenue.[FieldName],#AbsoluteRevenue.[YearProjection],#AbsoluteRevenue.CostProjection
	END /** Unit Revenue **/

	BEGIN /** Unit Margin **/
	CREATE TABLE #UnitMargin (FieldName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #UnitMargin ([FieldName],[YearProjection],[CostProjection])
	SELECT #UnitRevenue.FieldName,#UnitRevenue.[YearProjection],#UnitRevenue.CostProjection-#UnitCosts.CostProjection
	FROM #UnitRevenue INNER JOIN 
		 #UnitCosts ON #UnitRevenue.YearProjection = #UnitCosts.YearProjection AND #UnitRevenue.FieldName = #UnitCosts.FieldName
	END /** Unit Margin **/
	
	BEGIN /** Absolute Margin **/
	CREATE TABLE #AbsoluteMargin (FieldName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #AbsoluteMargin ([FieldName],[YearProjection],[CostProjection])
	SELECT #AbsoluteRevenue.FieldName,#AbsoluteRevenue.[YearProjection],#AbsoluteRevenue.CostProjection-#AbsoluteCost.CostProjection
	FROM #AbsoluteRevenue INNER JOIN 
		 #AbsoluteCost ON #AbsoluteRevenue.YearProjection = #AbsoluteCost.YearProjection AND #AbsoluteRevenue.FieldName = #AbsoluteCost.FieldName
	END /** Absolute Margin **/
	
	BEGIN /** Totals **/
	CREATE TABLE #Totals (FieldName NVARCHAR(255),YearProjection INT, Unit NVARCHAR(255), Value FLOAT, SortOrder INT) ON [PRIMARY]
	
	IF (@FieldCount > 1) /** Must Consolidate fields totals **/
	BEGIN
		/** Unit Revenue **/
		INSERT INTO #Totals (FieldName,YearProjection,Unit, Value, SortOrder)
		SELECT 'All' AS FieldName,#AbsoluteRevenue.YearProjection,'Unit Revenue ($' + @CurrencyCode + '/BOE)',SUM(#AbsoluteRevenue.CostProjection * @CurrencyFactor)/ ((SUM(P.Amount) * @CurrencyFactor) * 1000) AS Revenue,2
		FROM	#AbsoluteRevenue INNER JOIN 
				(SELECT FieldName, YearProjection, SUM(Amount) AS Amount 
				 FROM #Production
				 GROUP BY FieldName,YearProjection) AS P ON
				#AbsoluteRevenue.FieldName = P.FieldName AND
				#AbsoluteRevenue.YearProjection = P.YearProjection
		GROUP BY #AbsoluteRevenue.YearProjection

		/** Unit Costs **/
		INSERT INTO #Totals (FieldName,YearProjection,Unit, Value, SortOrder)
		SELECT 'All',#AbsoluteCost.YearProjection,'Unit Cost ($' + @CurrencyCode + '/BOE)', SUM(#AbsoluteCost.CostProjection) / (SUM(P.Amount)*1000),3
		FROM	#AbsoluteCost INNER JOIN 
				(SELECT FieldName, YearProjection, SUM(Amount) AS Amount 
				 FROM #Production
				 GROUP BY FieldName,YearProjection) AS P ON
				#AbsoluteCost.FieldName = P.FieldName AND
				#AbsoluteCost.YearProjection = P.YearProjection
		GROUP BY #AbsoluteCost.YearProjection

		/** Unit Margin **/
		INSERT INTO #Totals (FieldName, YearProjection, Unit, Value, SortOrder)
		SELECT 'All' AS FieldName,UnitRevenue.[YearProjection],'Unit Margin ($' + @CurrencyCode + '/BOE)',SUM(UnitRevenue.CostProjection) - SUM (UnitCosts.CostProjection), 1
		FROM (SELECT YearProjection, SUM(Value) AS CostProjection 
				 FROM #Totals
				 WHERE FieldName = 'All' AND Unit = 'Unit Revenue ($' + @CurrencyCode + '/BOE)'
				 GROUP BY YearProjection) AS UnitRevenue INNER JOIN 
			 (SELECT YearProjection, SUM(Value) AS CostProjection 
				 FROM #Totals
				 WHERE FieldName = 'All' AND Unit = 'Unit Cost ($' + @CurrencyCode + '/BOE)'
				 GROUP BY YearProjection) AS UnitCosts ON 
			 UnitRevenue.YearProjection = UnitCosts.YearProjection --AND UnitRevenue.FieldName = UnitCosts.FieldName
		GROUP BY UnitRevenue.YearProjection

		/** Production **/	
		INSERT INTO #Totals (FieldName,YearProjection,Unit, Value, SortOrder)
		SELECT 'All' AS FieldName,[YearProjection],'Production (k BOE)',sum([Amount]), 4
		FROM #Production
		GROUP BY [YearProjection]

		/** Absolute Revenue **/	
		INSERT INTO #Totals (FieldName,YearProjection,Unit, Value, SortOrder)
		SELECT 'All' AS [FieldName],[YearProjection],'Absolute Revenue (k $' + @CurrencyCode + ')', SUM([CostProjection])/1000 AS Revenue, 6
		FROM #AbsoluteRevenue
		GROUP BY YearProjection
		
		/** Absolute Cost **/ 
		INSERT INTO #Totals (FieldName,YearProjection,Unit, Value, SortOrder)
		SELECT 'All' AS [FieldName],[YearProjection],'Absolute Cost (k $' + @CurrencyCode + ')',SUM([CostProjection])/1000, 7
		FROM #AbsoluteCost
		GROUP BY YearProjection

		/** Absolute Margin **/
		INSERT INTO #Totals (FieldName,YearProjection,Unit, Value, SortOrder)
		SELECT 'All' AS FieldName, YearProjection, 'Absolute Margin (k $' + @CurrencyCode + ')', SUM(CostProjection)/1000, 5
		FROM #AbsoluteMargin
		GROUP BY YearProjection

	END
	
	INSERT INTO #Totals (FieldName,YearProjection,Unit, Value, SortOrder)
	SELECT FieldName,YearProjection,'Unit Margin ($' + @CurrencyCode + '/BOE)',CostProjection AS Margin, 1
	FROM #UnitMargin

	INSERT INTO #Totals (FieldName,YearProjection,Unit, Value, SortOrder)
	SELECT [FieldName],YearProjection,'Unit Revenue ($' + @CurrencyCode + '/BOE)',CostProjection, 2
	FROM #UnitRevenue

	INSERT INTO #Totals (FieldName,YearProjection,Unit, Value, SortOrder)
	SELECT [FieldName],[YearProjection],'Unit Cost ($' + @CurrencyCode + '/BOE)',[CostProjection], 3
	FROM #UnitCosts

	INSERT INTO #Totals (FieldName,YearProjection,Unit, Value, SortOrder)
	SELECT [FieldName],[YearProjection],'Production (k BOE)',sum([Amount]), 4
	FROM #Production
	GROUP BY [FieldName],[YearProjection]

	INSERT INTO #Totals (FieldName,YearProjection,Unit, Value, SortOrder)
	SELECT [FieldName],[YearProjection],'Absolute Revenue (k $' + @CurrencyCode + ')', [CostProjection]/1000 AS Revenue, 6
	FROM #AbsoluteRevenue
		 
	INSERT INTO #Totals (FieldName,YearProjection,Unit, Value, SortOrder)
	SELECT [FieldName],[YearProjection],'Absolute Cost (k $' + @CurrencyCode + ')',[CostProjection]/1000, 7
	FROM #AbsoluteCost

	INSERT INTO #Totals (FieldName,YearProjection,Unit, Value, SortOrder)
	SELECT FieldName, YearProjection, 'Absolute Margin (k $' + @CurrencyCode + ')', CostProjection/1000, 5
	FROM #AbsoluteMargin
	END /** Totals **/
	
	BEGIN /** Output for display and export **/
	/** Build Output Table **/
	-- Table Data (Tables[0])
	DECLARE @SQLOpexOperationalMargins NVARCHAR(MAX);
	SET @SQLOpexOperationalMargins = 'SELECT [SortOrder],[FieldName],[Unit],' + @YearColumnList + ' FROM #Totals PIVOT (SUM(Value) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY FieldName,SortOrder';
	EXEC(@SQLOpexOperationalMargins);

	-- Chart Data
	DECLARE @sqlChartTotals nvarchar(4000)
	DECLARE @YearProjection INT
	DECLARE @UnitValue FLOAT
	DECLARE @YearFound BIT

	DECLARE ChartTotals CURSOR FOR
	SELECT FieldName,YearProjection,Value 
	FROM #Totals
	WHERE Unit LIKE 'Unit Cost%'

	OPEN ChartTotals;
	FETCH NEXT FROM ChartTotals INTO @FieldName,@YearProjection,@UnitValue

	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- Search that the year isn't already in the table otherwise it's an update not an insert
		SELECT @YearFound = COUNT(*) FROM #ChartTotals WHERE Year = @YearProjection
		IF @YearFound = 0
		BEGIN -- Insert 
			SET @sqlChartTotals = 'INSERT INTO #ChartTotals (Year,"' + @FieldName + '_UC") VALUES (' + CAST(@YearProjection AS NVARCHAR(30)) + ',' + CAST(@UnitValue AS NVARCHAR(30)) + ')'
			EXEC(@sqlChartTotals)
		END
		ELSE
		BEGIN -- Update
			SET @sqlChartTotals = 'UPDATE #ChartTotals SET "' + @FieldName + '_UC" = ' + CAST(@UnitValue AS NVARCHAR(30)) + ' WHERE Year = ' + CAST(@YearProjection AS NVARCHAR(30)) 
			EXEC(@sqlChartTotals)
		END
		
		FETCH NEXT FROM ChartTotals INTO @FieldName,@YearProjection,@UnitValue
	END

	CLOSE ChartTotals;
	DEALLOCATE ChartTotals;

	DECLARE ChartTotals CURSOR FOR
	SELECT FieldName,YearProjection,Value
	FROM #Totals
	WHERE Unit LIKE 'Unit Revenue%'

	OPEN ChartTotals;
	FETCH NEXT FROM ChartTotals INTO @FieldName,@YearProjection,@UnitValue

	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- Search that the year isn't already in the table otherwise it's an update not an insert
		SELECT @YearFound = COUNT(*) FROM #ChartTotals WHERE Year = @YearProjection
		IF @YearFound = 0
		BEGIN -- Insert 
			SET @sqlChartTotals = 'INSERT INTO #ChartTotals (Year,"' + @FieldName + '_NP") VALUES (' + CAST(@YearProjection AS NVARCHAR(30)) + ',' + CAST(@UnitValue AS NVARCHAR(30)) + ')'
			EXEC(@sqlChartTotals)
		END
		ELSE
		BEGIN -- Update
			SET @sqlChartTotals = 'UPDATE #ChartTotals SET "' + @FieldName + '_NP" = ' + CAST(@UnitValue AS NVARCHAR(30)) + ' WHERE Year = ' + CAST(@YearProjection AS NVARCHAR(30)) 
			EXEC(@sqlChartTotals)
		END
		
		FETCH NEXT FROM ChartTotals INTO @FieldName,@YearProjection,@UnitValue
	END

	CLOSE ChartTotals;
	DEALLOCATE ChartTotals;
	
	DECLARE ChartTotals CURSOR FOR
	SELECT FieldName,YearProjection,Value 
	FROM #Totals
	WHERE Unit LIKE 'Unit Margin%'

	OPEN ChartTotals;
	FETCH NEXT FROM ChartTotals INTO @FieldName,@YearProjection,@UnitValue

	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- Search that the year isn't already in the table otherwise it's an update not an insert
		SELECT @YearFound = COUNT(*) FROM #ChartTotals WHERE Year = @YearProjection
		IF @YearFound = 0
		BEGIN -- Insert 
			SET @sqlChartTotals = 'INSERT INTO #ChartTotals (Year,"' + @FieldName + '_OM") VALUES (' + CAST(@YearProjection AS NVARCHAR(30)) + ',' + CAST(@UnitValue AS NVARCHAR(30)) + ')'
			EXEC(@sqlChartTotals)
		END
		ELSE
		BEGIN -- Update
			SET @sqlChartTotals = 'UPDATE #ChartTotals SET "' + @FieldName + '_OM" = ' + CAST(@UnitValue AS NVARCHAR(30)) + ' WHERE Year = ' + CAST(@YearProjection AS NVARCHAR(30)) 
			EXEC(@sqlChartTotals)
		END
		
		FETCH NEXT FROM ChartTotals INTO @FieldName,@YearProjection,@UnitValue
	END

	CLOSE ChartTotals;
	DEALLOCATE ChartTotals;
	
	SELECT * FROM #ChartTotals ORDER BY Year
	
	/** Output Stats Table for UI **/ 
	-- (Tables[4])
	IF (@FieldCount > 1)
		SELECT @FieldCount + 1 AS FieldCount;
	ELSE
		SELECT @FieldCount AS FieldCount;
	END  /** Output for display and export **/
	
	BEGIN /** Drop Temporary Tables **/
	DROP TABLE #FieldList
	DROP TABLE #AccountList
	DROP TABLE #VolumeReportBLRows  
	DROP TABLE #VolumeReportBL  
	DROP TABLE #VolumeReportBORows
	DROP TABLE #VolumeReportBO
	DROP TABLE #VolumeReportBLList 
	DROP TABLE #VolumeReportBOList 
	DROP TABLE #VolumeReportGrouped
	DROP TABLE #VolumeReportGroupedBL 
	DROP TABLE #VolumeReportGroupedBO 
	DROP TABLE #ZiffAccounts
	DROP TABLE #Prices
	DROP TABLE #UnitCosts
	DROP TABLE #AbsoluteCost
	DROP TABLE #Production
	DROP TABLE #Totals
	DROP TABLE #Opex
	DROP TABLE #YearList
	DROP TABLE #FieldsFillers
	DROP TABLE #ChartTotals
	DROP TABLE #AbsoluteRevenue
	DROP TABLE #UnitMargin
	DROP TABLE #UnitRevenue
	DROP TABLE #AbsoluteMargin
	END /** Drop Temporary Tables **/

END

GO

/****** Object:  StoredProcedure [dbo].[reppOpexProjectionMultiFields]    Script Date: 03/23/2016 10:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[reppOpexProjectionMultiFields]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[reppOpexProjectionMultiFields]
GO

/****** Object:  StoredProcedure [dbo].[reppOpexProjectionMultiFields]    Script Date: 02/18/2016 09:58:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-11-25
-- Description:	Module Report Opex Projection for Multi Fields
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
--
-- ===========================================================================================
CREATE PROCEDURE [dbo].[reppOpexProjectionMultiFields](
@IdModel INT,
@IdAggregationLevel INT,
@IdField NVARCHAR(MAX),
@IdStructure INT,
@From INT,
@To INT,
@IdTechnicalScenario INT,
@IdEconomicScenario INT,
@IdCurrency INT,
@IdTerm INT,
@IdTechnicalDriver INT,
@IdCostType INT,
@IdTypeOperation INT,
@Factor INT,
@CostCategories NVARCHAR(MAX)
)
AS
BEGIN

	BEGIN /** DROP Temporary Tables **/
	IF OBJECT_ID('tempdb..#FieldList') IS NOT NULL DROP TABLE #FieldList
	IF OBJECT_ID('tempdb..#CostCategoriesList') IS NOT NULL DROP TABLE #CostCategoriesList
	IF OBJECT_ID('tempdb..#AccountList') IS NOT NULL DROP TABLE #AccountList
	IF OBJECT_ID('tempdb..#TechDriver') IS NOT NULL DROP TABLE #TechDriver
	IF OBJECT_ID('tempdb..#Opex') IS NOT NULL DROP TABLE #Opex
	IF OBJECT_ID('tempdb..#BL') IS NOT NULL DROP TABLE #BL
	IF OBJECT_ID('tempdb..#BO') IS NOT NULL DROP TABLE #BO
	IF OBJECT_ID('tempdb..#Chart') IS NOT NULL DROP TABLE #Chart
	IF OBJECT_ID('tempdb..#ChartList') IS NOT NULL DROP TABLE #ChartList
	IF OBJECT_ID('tempdb..#VolumeReportBLRows') IS NOT NULL DROP TABLE #VolumeReportBLRows  
	IF OBJECT_ID('tempdb..#VolumeReportBL') IS NOT NULL DROP TABLE #VolumeReportBL  
	IF OBJECT_ID('tempdb..#VolumeReportBORows') IS NOT NULL DROP TABLE #VolumeReportBORows
	IF OBJECT_ID('tempdb..#VolumeReportBO') IS NOT NULL DROP TABLE #VolumeReportBO
	IF OBJECT_ID('tempdb..#VolumeReportBLList') IS NOT NULL DROP TABLE #VolumeReportBLList 
	IF OBJECT_ID('tempdb..#VolumeReportBOList') IS NOT NULL DROP TABLE #VolumeReportBOList 
	IF OBJECT_ID('tempdb..#VolumeReportGrouped') IS NOT NULL DROP TABLE #VolumeReportGrouped
	IF OBJECT_ID('tempdb..#VolumeReportGroupedBL') IS NOT NULL DROP TABLE #VolumeReportGroupedBL 
	IF OBJECT_ID('tempdb..#VolumeReportGroupedBO') IS NOT NULL DROP TABLE #VolumeReportGroupedBO 
	IF OBJECT_ID('tempdb..#Export') IS NOT NULL DROP TABLE #Export
	END /** DROP Temporary Tables **/
	
	BEGIN /** List Creation **/
	DECLARE @StartYear INT = @From;
	DECLARE @EndYear INT = @To;
	
	BEGIN /** Get Base Year **/
	DECLARE @BaseYear INT
	SELECT @BaseYear = [BaseYear] FROM [DBCPM].[dbo].[tblModels] WHERE IdModel=@IdModel
	END /** Get Base Year **/
	
	BEGIN /** Build Year Columns **/
	DECLARE @YearColumnList NVARCHAR(MAX)='';	
	DECLARE @YearFrom INT
	SET @YearFrom=@From	
	WHILE (@YearFrom <= @To)
	BEGIN
		SET @YearColumnList =  @YearColumnList + '[' + CAST(@YearFrom AS VARCHAR(4)) + ']';
		SET @YearFrom = @YearFrom + 1;
		IF (@YearFrom <= @To)
		BEGIN
			SET @YearColumnList = @YearColumnList + ',';
		END
	END
	END /** Build Year Columns **/
	
	BEGIN /** Build Field List **/
	/** Need to check whether there are more than one field being passed in, look for comma in input variable **/
	DECLARE @nonAll int 
	SELECT @nonAll = CHARINDEX(',', @IdField);
	DECLARE @SQL1 AS NVARCHAR(MAX)

	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF (@nonAll = 0) /** Not multiple fields **/
	BEGIN	
		IF (@IdAggregationLevel=0 AND cast(@IdField AS INT)=0) /** Single field and All was selected, Aggregation is All **/
		BEGIN
			INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdModel]=@IdModel GROUP BY [IdField];
		END
		ELSE IF (@IdAggregationLevel>0 AND cast(@IdField AS INT)>0) /** Single fields and specifc selected, Aggregation specific **/
		BEGIN
			INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
		END
		ELSE IF (@IdAggregationLevel>0) /** Specific Aggregation with All fields **/
		BEGIN
			INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdModel]=@IdModel GROUP BY [IdField];
		END
		ELSE IF (cast(@IdField AS INT)>0)
		BEGIN
			INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
		END
	END
	ELSE /** Multiple fields **/
	BEGIN
		IF (@IdAggregationLevel=0) /** Multiple fields and Aggregation All was selected **/
		BEGIN
			SET @SQL1 = 'INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdModel]= ' + cast(@IdModel AS VARCHAR(10)) + ' AND [IdField] IN (' + @IdField + ') GROUP BY [IdField];'
		END
		ELSE IF (@IdAggregationLevel>0) /** Multiple fields and specifc Aggregation was selected **/
		BEGIN
			SET @SQL1 = 'INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]= ' + cast(@IdAggregationLevel AS VARCHAR(10)) + ' AND [IdField] IN (' + @IdField + ') AND [IdModel]=' + cast(@IdModel AS VARCHAR(10)) + ' GROUP BY [IdField];'
		END
		EXEC(@SQL1)
	END

	DECLARE @FieldCount INT;
	SELECT @FieldCount=COUNT([IdField]) FROM #FieldList

	/** Generate Chart Columns from Temp Table **/
	DECLARE @FieldColumnList NVARCHAR(MAX)='';
	DECLARE @FieldName NVARCHAR(255);
	DECLARE FieldList CURSOR FOR
	SELECT Name FROM [tblFields] where IdField in (SELECT IdField FROM #FieldList);

	OPEN FieldList;
	FETCH NEXT FROM FieldList INTO @FieldName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @FieldColumnList=''
		BEGIN
			SET @FieldColumnList =  @FieldColumnList + '[' + @FieldName + ']';
		END
		ELSE
		BEGIN
			SET @FieldColumnList =  @FieldColumnList + ',[' + @FieldName + ']';
		END
		FETCH NEXT FROM FieldList INTO @FieldName;
	END

	CLOSE FieldList;
	DEALLOCATE FieldList;
	END /** Build Field List **/
	
	BEGIN /** Build Cost Categories List **/
	/** Need to check whether there are more than one field being passed in, look for comma in input variable **/
	DECLARE @nonAllCC int 
	SELECT @nonAllCC = CHARINDEX(',', @CostCategories);
	DECLARE @SQL2 AS NVARCHAR(MAX)
	
	CREATE TABLE #CostCategoriesList (IdZiffAccount INT PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF (@nonAllCC = 0) /** Not multiple Cost Categories **/
	BEGIN
		IF (CAST(@CostCategories AS INT) = 0)/** 0 SELECTED (ALL) **/
		BEGIN
			INSERT INTO #CostCategoriesList SELECT [IdZiffAccount] FROM tblZiffAccounts WHERE [IdModel] = @IdModel AND Parent IS NULL ORDER BY Parent;
		END
		ELSE IF (CAST(@CostCategories AS INT) > 0)/** Specific Cost Category Selected **/
		BEGIN
			INSERT INTO #CostCategoriesList SELECT [IdZiffAccount] FROM tblZiffAccounts WHERE [IdModel] = @IdModel AND [IdZiffAccount] = @CostCategories GROUP BY [IdZiffAccount];
		END
	END
	ELSE /** Multiple Cost Categories **/
	BEGIN
		SET @SQL2 = 'INSERT INTO #CostCategoriesList SELECT [IdZiffAccount] FROM tblZiffAccounts WHERE [IdModel] = ' + cast(@IdModel AS VARCHAR(10)) + ' AND [IdZiffAccount] IN (' + @CostCategories + ') GROUP BY [IdZiffAccount];'
		EXEC(@SQL2)
	END

	DECLARE @CostCategoriesCount INT;
	SELECT @CostCategoriesCount=COUNT([IdZiffAccount]) FROM #CostCategoriesList
	END /** Build Cost Categories List **/
	
	BEGIN /** Build Account List**/
	/** Get Chart Columns to Temp Table **/
	CREATE TABLE #AccountList (ColumnName NVARCHAR(255) PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF @IdStructure=1
	BEGIN
		INSERT INTO #AccountList SELECT [RootZiffAccount] FROM tblCalcProjectionTechnical WHERE IdField IN (SELECT [IdField] FROM #FieldList) AND [IdModel]=@IdModel GROUP BY [RootZiffAccount] ORDER BY [RootZiffAccount];
	END
	ELSE IF @IdStructure=2
	BEGIN
		INSERT INTO #AccountList SELECT [Activity] FROM tblCalcProjectionTechnical WHERE IdField IN (SELECT [IdField] FROM #FieldList) AND [IdModel]=@IdModel GROUP BY [Activity], [SortOrder] ORDER BY [SortOrder], [Activity];
	END
	
	/** Generate Chart Columns from Temp Table **/
	DECLARE @AccountColumnList NVARCHAR(MAX)='';
	DECLARE @AccountName NVARCHAR(255);
	DECLARE AccountList CURSOR FOR
	SELECT ColumnName FROM #AccountList;

	OPEN AccountList;
	FETCH NEXT FROM AccountList INTO @AccountName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @AccountColumnList=''
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + '[' + @AccountName + ']';
		END
		ELSE
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + ',[' + @AccountName + ']';
		END
		FETCH NEXT FROM AccountList INTO @AccountName;
	END

	CLOSE AccountList;
	DEALLOCATE AccountList;
	END /** Build Account List**/
	
	BEGIN /** Get Currency Factor to use **/
	DECLARE @CurrencyFactor FLOAT;
	SELECT @CurrencyFactor=[Value] FROM tblModelsCurrencies WHERE [IdCurrency]=@IdCurrency AND [IdModel]=@IdModel;
	END /** Get Currency Factor to use **/
	
	BEGIN /** Build Technical Driver Factors (used for KPI Report) **/
	CREATE TABLE #TechDriver ([Year] INT, [Normal] FLOAT, [Optimistic] FLOAT, [Pessimistic] FLOAT) ON [PRIMARY]
	IF @IdTechnicalDriver=0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, 1 AS [Normal], 1 AS [Optimistic], 1 AS [Pessimistic]
		FROM tblCalcModelYears
		WHERE [IdModel]=@IdModel AND [Year] BETWEEN @StartYear AND @EndYear;
	END
	ELSE IF @IdTechnicalDriver>0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, SUM([Normal]) AS [Normal], SUM([Pessimistic]) AS [Pessimistic], SUM([Optimistic]) AS [Optimistic]
		FROM tblTechnicalDriverData
		WHERE [IdTechnicalDriver]=@IdTechnicalDriver AND [IdModel]=@IdModel AND IdField IN (SELECT [IdField] FROM #FieldList)
		GROUP BY Year;
	END
	END /** Build Technical Driver Factors (used for KPI Report) **/
	END  /** List Creation **/
	
	BEGIN /** Build Volume Tables **/
	/** Report (Baseline)**/
	CREATE TABLE #VolumeReportBLRows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLRows ([ParentName], [Name], [YearProjection], [Amount])	
		SELECT	DISTINCT N'All Cost Accounts' AS ParentName, dbo.tblFields.Name AS Name, --dbo.tblProjects.Name AS Name, 
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN ISNULL(dbo.tblTechnicalDriverData.Normal,0)
				WHEN @IdTechnicalScenario=2 THEN ISNULL(dbo.tblTechnicalDriverData.Optimistic,0)
				WHEN @IdTechnicalScenario=3 THEN ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0)
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject AND
			dbo.tblTechnicalDriverData.Year >= dbo.tblProjects.Starts INNER JOIN
			dbo.tblFields ON dbo.tblProjects.IdField = dbo.tblFields.IdField
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=1
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	ORDER BY dbo.tblTechnicalDriverData.Year
	
	CREATE TABLE #VolumeReportBL ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBL([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBLRows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBLList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBL WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #VolumeReportBLList to Fill Properly **/
	INSERT INTO #VolumeReportBL ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBLList.ParentName,#VolumeReportBLList.Name, #VolumeReportBLList.YearProjection,0 AS Amount
	FROM	#VolumeReportBLList LEFT OUTER JOIN
			#VolumeReportBL ON #VolumeReportBLList.ParentName=#VolumeReportBL.ParentName AND #VolumeReportBLList.Name=#VolumeReportBL.Name AND #VolumeReportBLList.YearProjection=#VolumeReportBL.YearProjection
	WHERE	#VolumeReportBL.YearProjection IS NULL;

	/** Delete all rows that have 0 (zero) Amount **/
	DELETE #VolumeReportBL WHERE Amount=0

	/** Report (Business Opportunities)**/
	CREATE TABLE #VolumeReportBORows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBORows ([ParentName], [Name], [YearProjection], [Amount])	
		SELECT	DISTINCT N'All Cost Accounts' AS ParentName, dbo.tblFields.Name AS Name, --dbo.tblProjects.Name AS Name, 
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN ISNULL(dbo.tblTechnicalDriverData.Normal,0)
				WHEN @IdTechnicalScenario=2 THEN ISNULL(dbo.tblTechnicalDriverData.Optimistic,0)
				WHEN @IdTechnicalScenario=3 THEN ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0)
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject AND
			dbo.tblTechnicalDriverData.Year >= dbo.tblProjects.Starts INNER JOIN
			dbo.tblFields ON dbo.tblProjects.IdField = dbo.tblFields.IdField
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=2
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	ORDER BY dbo.tblTechnicalDriverData.Year

	CREATE TABLE #VolumeReportBO ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBO([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBORows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBOList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBOList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBO WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #VolumeReportBOList to Fill Properly **/
	INSERT INTO #VolumeReportBO ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBOList.ParentName,#VolumeReportBOList.Name, #VolumeReportBOList.YearProjection,0 AS Amount
	FROM	#VolumeReportBOList LEFT OUTER JOIN
			#VolumeReportBO ON #VolumeReportBOList.ParentName=#VolumeReportBO.ParentName AND #VolumeReportBOList.Name=#VolumeReportBO.Name AND #VolumeReportBOList.YearProjection=#VolumeReportBO.YearProjection
	WHERE	#VolumeReportBO.YearProjection IS NULL;

	/** Delete all rows that have 0 (zero) Amount **/
	DELETE #VolumeReportBO WHERE Amount=0

	/** Report (All Fields Grouped)**/
	CREATE TABLE #VolumeReportGroupedBL ([YearProjection] INT, [Amount] FLOAT, [ParentName] NVARCHAR(255), [Name] NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBL ([YearProjection], [Amount], [ParentName], [Name])
	SELECT YearProjection,SUM(amount) AS Amount,'GroupedBL',Name
	FROM #VolumeReportBL
	GROUP BY YearProjection,Name
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGroupedBO ([YearProjection] INT, [Amount] FLOAT, [ParentName] NVARCHAR(255), [Name] NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBO ([YearProjection], [Amount], [ParentName], [Name])
	SELECT YearProjection,SUM(amount) AS Amount,'GroupedBO',Name
	FROM #VolumeReportBO
	GROUP BY YearProjection,Name
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGrouped ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255),[YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGrouped ([ParentName], [Name], [YearProjection], [Amount])
	SELECT N'All Cost Accounts' AS ParentName,Name,YearProjection,SUM(Amount) AS 'Amount'
	FROM
	(
		SELECT 'VolumeReportGroupedBL' AS "ProjectType", ParentName, Name, YearProjection,SUM(Amount) AS "Amount"
		FROM #VolumeReportGroupedBL 
		GROUP BY ParentName,Name,YearProjection
		UNION
		SELECT 'VolumeReportGroupedBO', ParentName, Name, YearProjection, SUM(Amount) AS "Amount"
		FROM #VolumeReportGroupedBO 
		GROUP BY ParentName,Name,YearProjection
	) AS VolumeReportGroupedTotals
	GROUP BY Name,YearProjection
	ORDER BY Name,YearProjection

	END /** Build Volume Tables **/

	BEGIN /** Get grouped driver value for Base Year **/
	DECLARE @TotalCapacityDriver INT = 0;

	SELECT @TotalCapacityDriver = Amount FROM #VolumeReportGrouped WHERE YearProjection=@BaseYear
	END /** Get grouped driver value for Base Year **/
	
	BEGIN /** Build Source Data Table - Total (#Opex)**/
	CREATE TABLE #Opex (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT, SortOrder INT) ON [PRIMARY]
	INSERT INTO #Opex ([ParentName],[Name],[YearProjection],[CostProjection],[SortOrder])
	SELECT N'All Fields' AS ParentName, 
		tblCalcProjectionTechnical.Field AS Name, 
		tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, 1 AS SortOrder
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList)
	AND tblCalcProjectionTechnical.IdRootZiffAccount IN (SELECT [IdZiffAccount] FROM #CostCategoriesList) 
	AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	GROUP BY tblCalcProjectionTechnical.Field, tblCalcProjectionTechnical.Year;
	END /** Build Source Data Table - Total (#Opex) **/

	BEGIN /** Build Source Data Table - Chart **/
	CREATE TABLE #Chart (ParentName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #Chart ([ParentName],[YearProjection],[CostProjection])
	SELECT  tblCalcProjectionTechnical.Field AS ParentName, 
		tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	AND tblCalcProjectionTechnical.IdRootZiffAccount IN (SELECT [IdZiffAccount] FROM #CostCategoriesList)
	GROUP BY  tblCalcProjectionTechnical.Field, tblCalcProjectionTechnical.Year;
	
	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #ChartList (ParentName NVARCHAR(255), YearProjection INT) ON [PRIMARY]
	INSERT INTO #ChartList ([ParentName],[YearProjection])
	SELECT CNList.ParentName, CYList.Year FROM
		(SELECT [ParentName] FROM #Chart GROUP BY [ParentName])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList
	WHERE CYList.Year BETWEEN @StartYear AND @EndYear
	
	/** Add Any Missing Years to Make the Chart Fill Properly (eg. Fields with only Cyclical Drivers) **/
	INSERT INTO #Chart ([ParentName],[YearProjection],[CostProjection])
	SELECT #ChartList.ParentName, #ChartList.YearProjection, NULL AS CostProjection FROM
		#ChartList LEFT OUTER JOIN
		#Chart ON #ChartList.ParentName=#Chart.ParentName AND #ChartList.YearProjection=#Chart.YearProjection
	WHERE #Chart.YearProjection IS NULL;	
	END /** Build Source Data Table - Chart **/
	
	BEGIN /** Build Source Data Table - Export **/
	CREATE TABLE #Export (CostCategory NVARCHAR(255), Name NVARCHAR(255), Field NVARCHAR(255), YearProjection INT, CostProjection FLOAT, SortID NVARCHAR(255),SortOrder INT) ON [PRIMARY]
	INSERT INTO #Export ([CostCategory],[Name],[Field],[YearProjection],[CostProjection],[SortID],[SortOrder])
	SELECT tblCalcProjectionTechnical.CodeZiff AS CostCategory, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
		tblCalcProjectionTechnical.Field AS Field, tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, tblCalcProjectionTechnical.RootCodeZiff + '_' + tblCalcProjectionTechnical.CodeZiff AS SortID, 2 AS SortOrder--1 AS SortOrder
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList)
	AND tblCalcProjectionTechnical.IdRootZiffAccount IN (SELECT [IdZiffAccount] FROM #CostCategoriesList)
	GROUP BY tblCalcProjectionTechnical.CodeZiff,tblCalcProjectionTechnical.Field, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END,tblCalcProjectionTechnical.Year, 
			tblCalcProjectionTechnical.RootCodeZiff + '_' + tblCalcProjectionTechnical.CodeZiff
	UNION
	SELECT tblCalcProjectionTechnical.RootCodeZiff AS CostCategory, tblCalcProjectionTechnical.RootZiffAccount AS Name, tblCalcProjectionTechnical.Field AS Field, tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, tblCalcProjectionTechnical.RootCodeZiff AS SortID, 2 AS SortOrder
	FROM tblCalcProjectionTechnical  LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel INNER JOIN
		tblProjects ON tblCalcProjectionTechnical.IdProject = tblProjects.IdProject
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel 
	AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) 
	AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	AND tblCalcProjectionTechnical.Operation = 1
	GROUP BY tblCalcProjectionTechnical.RootCodeZiff,tblCalcProjectionTechnical.RootZiffAccount,tblCalcProjectionTechnical.Field,tblCalcProjectionTechnical.Year, tblCalcProjectionTechnical.RootCodeZiff
	UNION
	SELECT tblCalcProjectionTechnical.CodeZiff AS CostCategory, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
		'All' AS Field, tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, tblCalcProjectionTechnical.RootCodeZiff + '_' + tblCalcProjectionTechnical.CodeZiff AS SortID, 1 AS SortOrder--3 AS SortOrder
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList)
	AND tblCalcProjectionTechnical.IdRootZiffAccount IN (SELECT [IdZiffAccount] FROM #CostCategoriesList)
	GROUP BY tblCalcProjectionTechnical.CodeZiff, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END,tblCalcProjectionTechnical.Year, 
			tblCalcProjectionTechnical.RootCodeZiff + '_' + tblCalcProjectionTechnical.CodeZiff
	UNION
	SELECT tblCalcProjectionTechnical.RootCodeZiff AS CostCategory, tblCalcProjectionTechnical.RootZiffAccount AS Name, 'All' AS Field, tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, tblCalcProjectionTechnical.RootCodeZiff AS SortID, 1 AS SortOrder--4 AS SortOrder
	FROM tblCalcProjectionTechnical  LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel INNER JOIN
		tblProjects ON tblCalcProjectionTechnical.IdProject = tblProjects.IdProject
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel 
	AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) 
	AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	AND tblCalcProjectionTechnical.Operation = 1
	GROUP BY tblCalcProjectionTechnical.RootCodeZiff,tblCalcProjectionTechnical.RootZiffAccount,tblCalcProjectionTechnical.Year, tblCalcProjectionTechnical.RootCodeZiff
	END /** Build Source Data Table - Export **/

	BEGIN /** Build BL and BO Tables**/	
	IF @IdTechnicalDriver=0
	BEGIN
		/** Build Source Data Table - Baseline and Business Opportunities (No Techical KPI Required) **/
		CREATE TABLE #BL (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
		CREATE TABLE #BO (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
		IF @FieldCount = 1
		BEGIN
			/** Baseline **/
			INSERT INTO #BL ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, --CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS Name, 
				CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=1 
			AND tblCalcProjectionTechnical.IdRootZiffAccount IN (SELECT [IdZiffAccount] FROM #CostCategoriesList)
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END, tblCalcProjectionTechnical.Year;

			/** Business Opportunities **/
			INSERT INTO #BO ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, --CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS Name, 
				CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=2 
			AND tblCalcProjectionTechnical.IdRootZiffAccount IN (SELECT [IdZiffAccount] FROM #CostCategoriesList)
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END, tblCalcProjectionTechnical.Year;
		END
		ELSE IF @FieldCount <> 1
		BEGIN
			/** Baseline **/
			INSERT INTO #BL ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, Project AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=1 
			AND tblCalcProjectionTechnical.IdRootZiffAccount IN (SELECT [IdZiffAccount] FROM #CostCategoriesList)
			GROUP BY tblCalcProjectionTechnical.Project, tblCalcProjectionTechnical.Year;
			
			/** Business Opportunities **/
			INSERT INTO #BO ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, Project Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=2 
			AND tblCalcProjectionTechnical.IdRootZiffAccount IN (SELECT [IdZiffAccount] FROM #CostCategoriesList)
			GROUP BY tblCalcProjectionTechnical.Project, tblCalcProjectionTechnical.Year;
		END
	END
	END /** Build BL and BO Tables**/
	
	BEGIN /** Add Unit Cost factor, IF NECESSARY **/
	IF @IdCostType = 2
	BEGIN		 
		UPDATE	#Opex
		SET		#Opex.CostProjection = CASE WHEN #VolumeReportGrouped.Amount = 0 THEN #Opex.CostProjection * 0 ELSE #Opex.CostProjection/#VolumeReportGrouped.Amount END
		FROM	#Opex INNER JOIN #VolumeReportGrouped ON
				#Opex.YearProjection = #VolumeReportGrouped.YearProjection AND
				#Opex.Name = #VolumeReportGrouped.Name

		UPDATE	#Chart
		SET		#Chart.CostProjection = CASE WHEN #VolumeReportGrouped.Amount = 0 THEN #Chart.CostProjection * 0 ELSE #Chart.CostProjection/#VolumeReportGrouped.Amount END
		FROM	#Chart INNER JOIN #VolumeReportGrouped ON
				#Chart.YearProjection = #VolumeReportGrouped.YearProjection AND
				#Chart.ParentName = #VolumeReportGrouped.Name
	END

	DECLARE @DisplayedFields INT
	SELECT @DisplayedFields = COUNT(A.Name) 
	FROM 
	(SELECT Name FROM #Opex group by Name) AS A

	IF @IdCostType = 2 -- UNIT COST
	BEGIN
		INSERT INTO #Opex ([ParentName],[Name],[YearProjection],[CostProjection],[SortOrder])
		SELECT 'All Fields', '99', NULL, NULL,2
		
		INSERT INTO #Opex ([ParentName],[Name],[YearProjection],[CostProjection],[SortOrder])
		SELECT 'All Fields', 'Unit Cost Average', YearProjection, SUM(CostProjection)/@DisplayedFields AS CostProjection,3
		FROM #Opex
		GROUP BY YearProjection
	END
	ELSE
	BEGIN
		INSERT INTO #Opex ([ParentName],[Name],[YearProjection],[CostProjection],[SortOrder])
		SELECT 'All Fields', '99', NULL, NULL,2

		INSERT INTO #Opex ([ParentName],[Name],[YearProjection],[CostProjection],[SortOrder])
		SELECT 'All Fields', 'Total Cost', YearProjection, SUM(CostProjection) AS CostProjection,3 
		FROM #Opex
		GROUP BY YearProjection
	END
	END /** Add Unit Cost factor, IF NECESSARY **/
	
	BEGIN /** Build Output Table **/
	DECLARE @SQLOpex NVARCHAR(MAX);
	SET @SQLOpex = 'SELECT [ParentName], [Name],' + @YearColumnList + ' FROM #Opex PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [SortOrder], [ParentName], [Name]';
	EXEC(@SQLOpex);

	DECLARE @SQLOpexChart NVARCHAR(MAX);
	SET @SQLOpexChart = 'SELECT [YearProjection] AS [Year],' + @FieldColumnList + ' FROM #Chart PIVOT (SUM(CostProjection) FOR ParentName IN (' + @FieldColumnList + ')) AS PivotTable ORDER BY [YearProjection]';
	EXEC(@SQLOpexChart);

	DECLARE @SQLExport NVARCHAR(MAX);
	SET @SQLExport = 'SELECT [SortID], [CostCategory],[Name],[Field],' + @YearColumnList + ' FROM #Export PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [SortOrder],[Field],[SortID]';
	EXEC(@SQLExport);

	IF @IdTechnicalDriver=0
	BEGIN
		SELECT @FieldCount AS FieldCount;
	END	
	END /** Build Output Table **/

END

GO

/****** Object:  StoredProcedure [dbo].[reppCostBenchmarking]    Script Date: 03/23/2016 10:18:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[reppCostBenchmarking]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[reppCostBenchmarking]
GO

/****** Object:  StoredProcedure [dbo].[reppCostBenchmarking]    Script Date: 02/24/2016 22:52:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:		Rodrigo Manubens
-- Create date:	2016-02-04
-- Description:	Report Cost Benchmarking
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- ===========================================================================================
CREATE PROCEDURE [dbo].[reppCostBenchmarking](
@IdModel INT, 
@IdPeerGroup NVARCHAR(MAX), 
@IdField NVARCHAR(MAX),
@IdCurrency INT
)
AS
BEGIN

	BEGIN /** DROP Temporary Tables **/
	IF OBJECT_ID('tempdb..#FieldList') IS NOT NULL DROP TABLE #FieldList
	IF OBJECT_ID('tempdb..#PeerGroupList') IS NOT NULL DROP TABLE #PeerGroupList
	IF OBJECT_ID('tempdb..#FieldVolume') IS NOT NULL DROP TABLE #FieldVolume
	IF OBJECT_ID('tempdb..#InternalCosts') IS NOT NULL DROP TABLE #InternalCosts
	IF OBJECT_ID('tempdb..#ZiffAccounts') IS NOT NULL DROP TABLE #ZiffAccounts
	IF OBJECT_ID('tempdb..#Costs') IS NOT NULL DROP TABLE #Costs
	IF OBJECT_ID('tempdb..#ExportCosts') IS NOT NULL DROP TABLE #ExportCosts
   	IF OBJECT_ID('tempdb..#ExternalCostReferences') IS NOT NULL DROP TABLE #ExternalCostReferences	
   	IF OBJECT_ID('tempdb..#CostChart') IS NOT NULL DROP TABLE #CostChart
	IF OBJECT_ID('tempdb..#ZiffAccountsExternal') IS NOT NULL DROP TABLE #ZiffAccountsExternal
	IF OBJECT_ID('tempdb..#ParentList') IS NOT NULL DROP TABLE #ParentList
	END /** DROP Temporary Tables **/
	
	BEGIN /** Build List Tables for reports **/	
	/** Build Peer Group List **/
	/** Need to check whether there are more than one peer group being passed in, look for comma in input variable **/
	DECLARE @SQL1 AS NVARCHAR(MAX)
	DECLARE @nonAllPeer INT
	SELECT @nonAllPeer = CHARINDEX(',', @IdPeerGroup);

	CREATE TABLE #PeerGroupList (IdPeerGroup INT PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF (@nonAllPeer = 0) /** Not multiple fields **/
	BEGIN
		IF (@IdPeerGroup = '-1') OR (@IdPeerGroup = '0')
			SET @SQL1 = 'INSERT INTO #PeerGroupList SELECT [IdPeerGroup] from vListFieldsPerPeerGroup WHERE  IdModel = ' + CAST(@IdModel AS NVARCHAR(100)) + ' AND IdPeerGroup IN (SELECT IdPeerGroup FROM tblPeerGroup WHERE IdModel = ' + CAST(@IdModel AS NVARCHAR(100)) + ') GROUP BY [IdPeerGroup];'
		ELSE
			INSERT INTO #PeerGroupList VALUES(@IdPeerGroup)
	END
	ELSE /** Multiple fields **/
	BEGIN
		SET @SQL1 = 'INSERT INTO #PeerGroupList SELECT [IdPeerGroup] FROM vListFieldsPerPeerGroup WHERE IdModel = ' + CAST(@IdModel AS NVARCHAR(100)) + ' AND IdPeerGroup IN (' + @IdPeerGroup + ') GROUP BY [IdPeerGroup];'
	END
	EXEC(@SQL1)

	DECLARE @PeerGroupCount INT;
	SELECT @PeerGroupCount=COUNT(IdPeerGroup) FROM #PeerGroupList
	
	/** Generate Peer Group columns to be used for output of data **/
	DECLARE @PeerGroupsList NVARCHAR(MAX)='';
	DECLARE @PeerGroupName NVARCHAR(255);
	DECLARE PeerGroupsList CURSOR FOR
	SELECT PeerGroupCode FROM tblPeerGroup WHERE IdModel=@IdModel AND IdPeerGroup IN (SELECT IdPeerGroup FROM #PeerGroupList);

	OPEN PeerGroupsList;
	FETCH NEXT FROM PeerGroupsList INTO @PeerGroupName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @PeerGroupsList=''
		BEGIN
			SET @PeerGroupsList =  @PeerGroupsList + '[' + @PeerGroupName + ']';
		END
		ELSE
		BEGIN
			SET @PeerGroupsList =  @PeerGroupsList + ',[' + @PeerGroupName + ']';
		END
		FETCH NEXT FROM PeerGroupsList INTO @PeerGroupName;
	END

	CLOSE PeerGroupsList;
	DEALLOCATE PeerGroupsList;

	/** Build Field List **/
	/** Need to check whether there are more than one field being passed in, look for comma in input variable **/
	DECLARE @nonAll int 
	SELECT @nonAll = CHARINDEX(',', @IdField);
	
	DECLARE @IsThere INT = 0,@WhereComma INT = 0, @ListLength INT = 0
	
	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF (@nonAll = 0) /** Not multiple fields **/
	BEGIN
		IF (@IdField = '0')
			SET @SQL1 = 'INSERT INTO #FieldList SELECT IdField from vListFieldsPerPeerGroup WHERE IdModel = ' + cast(@IdModel AS VARCHAR(10)) + ' AND IdPeerGroup IN (SELECT IdPeergroup from #PeerGroupList) GROUP BY IdField;'
		ELSE	
			SET @SQL1 = 'INSERT INTO #FieldList VALUES(' + @IdField + ')'
	END
	ELSE /** Multiple fields **/
	BEGIN
		SET @SQL1 = 'INSERT INTO #FieldList SELECT [IdField] FROM vListFieldsPerPeerGroup WHERE [IdModel] = ' + cast(@IdModel AS VARCHAR(10)) + ' AND [IdField] IN (' + @IdField + ') GROUP BY [IdField];'
	END
	EXEC(@SQL1)

	DECLARE @FieldCount INT;
	SELECT @FieldCount=COUNT([IdField]) FROM #FieldList

	/** Generate Field Columns from Temp Table **/
	DECLARE @FieldColumnList NVARCHAR(MAX)='';
	DECLARE @FieldName NVARCHAR(255);
	DECLARE FieldList CURSOR FOR
	SELECT Name FROM [tblFields] where IdField in (SELECT IdField FROM #FieldList);

	OPEN FieldList;
	FETCH NEXT FROM FieldList INTO @FieldName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		SET @IsThere = CHARINDEX(@FieldName,@PeerGroupsList)
		SET @WhereComma = CHARINDEX(',',@PeerGroupsList,@IsThere)
		SET @ListLength = LEN(@PeerGroupsList)
--select @PeerGroupsList '@PeerGroupsList', @FieldName '@FieldName', @IsThere '@IsThere', @WhereComma '@WhereComma', @ListLength '@ListLength'	

		IF @FieldColumnList=''
		BEGIN
			IF @IsThere = 0
				SET @FieldColumnList =  @FieldColumnList + '[' + @FieldName + ']';
			ELSE
			BEGIN
				SET @FieldColumnList =  @FieldColumnList + '[' + @FieldName + ']';
				IF @WhereComma = 0
					SET @PeerGroupsList = STUFF(@PeerGroupsList,@ListLength,1,'_PG]')
				ELSE
					SET @PeerGroupsList = STUFF(@PeerGroupsList,@WhereComma,1,'_PG]')
			END
		END
		ELSE
		BEGIN
			IF @IsThere = 0
				SET @FieldColumnList =  @FieldColumnList + ',[' + @FieldName + ']';
			ELSE
			BEGIN
				SET @FieldColumnList =  @FieldColumnList + ',[' + @FieldName + ']';
				IF @WhereComma = 0
					SET @PeerGroupsList = STUFF(@PeerGroupsList,@ListLength,1,'_PG]')
				ELSE
					SET @PeerGroupsList = STUFF(@PeerGroupsList,@WhereComma-1,1,'_PG]')
			END
		END
		FETCH NEXT FROM FieldList INTO @FieldName;
	END

	CLOSE FieldList;
	DEALLOCATE FieldList;

	DECLARE @ReferenceDescriptionList NVARCHAR(MAX)
	SET @ReferenceDescriptionList = @FieldColumnList + ',' + @PeerGroupsList

	/** Get Currency Factor to use **/
	DECLARE @CurrencyFactor FLOAT;
	SELECT @CurrencyFactor=[Value] FROM tblModelsCurrencies WHERE [IdCurrency]=@IdCurrency AND [IdModel]=@IdModel;
	END  /** Build List Tables for reports **/
	
	BEGIN  /** Build Volume Tables **/
	CREATE TABLE #FieldVolume (IdField INT, FieldName NVARCHAR(255), IdAllocationListDriver INT, Name NVARCHAR(255), VolumeAmount FLOAT) ON [PRIMARY]
	INSERT INTO #FieldVolume 
	SELECT dbo.tblAllocationDriversByField.IdField
		, dbo.tblFields.Name AS FieldName
		, dbo.tblAllocationListDrivers.IdAllocationListDriver
		, dbo.tblAllocationListDrivers.Name
		, SUM(dbo.tblAllocationDriversByField.Amount) AS VolumeAmount
	FROM dbo.tblAllocationDriversByField INNER JOIN
		 dbo.tblAllocationListDrivers ON dbo.tblAllocationDriversByField.IdAllocationListDriver = dbo.tblAllocationListDrivers.IdAllocationListDriver INNER JOIN
		 dbo.tblFields ON dbo.tblAllocationDriversByField.IdField = dbo.tblFields.IdField
	WHERE dbo.tblAllocationListDrivers.IdModel =  @IdModel 
	AND	 dbo.tblAllocationListDrivers.UnitCostDenominator = 1
	and tblFields.IdField IN (SELECT [IdField] FROM #FieldList)
	GROUP BY dbo.tblAllocationListDrivers.IdAllocationListDriver
		, dbo.tblAllocationListDrivers.Name
		, dbo.tblAllocationDriversByField.IdField
		, dbo.tblFields.Name
	ORDER BY FieldName                           
	END  /** Build Volume Tables **/

	BEGIN /** Internal Costs **/
	CREATE TABLE #InternalCosts (IdModel INT,IdField INT, Field NVARCHAR(255),RootCodeZiff NVARCHAR(255), RootZiffAccount NVARCHAR(255), InternalCost FLOAT, SortOrderExport NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #InternalCosts (IdModel,IdField,Field,RootCodeZiff,RootZiffAccount,InternalCost,SortOrderExport)
	SELECT [tblCalcBaseCostByField].IdModel
		, [tblCalcBaseCostByField].IdField
		, [tblCalcBaseCostByField].Field
		, [tblCalcBaseCostByField].CodeZiff
		, [tblCalcBaseCostByField].ZiffAccount
		, SUM(ROUND([tblCalcBaseCostByField].Amount,2)) AS InternalCost
		, [tblCalcBaseCostByField].RootCodeZiff + '_' + [tblCalcBaseCostByField].CodeZiff AS SortOrderExport
	FROM [tblCalcBaseCostByField] 
	WHERE [tblCalcBaseCostByField].IdModel = @IdModel  AND IdRootZiffAccount IS NOT NULL AND [tblCalcBaseCostByField].IdField IN (SELECT [IdField] FROM #FieldList)
	GROUP BY [tblCalcBaseCostByField].IdModel
		, [tblCalcBaseCostByField].IdField
		, [tblCalcBaseCostByField].Field
		, [tblCalcBaseCostByField].CodeZiff
		, [tblCalcBaseCostByField].ZiffAccount
		, [tblCalcBaseCostByField].RootCodeZiff
	UNION
	SELECT [tblCalcBaseCostByField].IdModel
		, [tblCalcBaseCostByField].IdField
		, [tblCalcBaseCostByField].Field
		, [tblCalcBaseCostByField].RootCodeZiff
		, [tblCalcBaseCostByField].RootZiffAccount
		, SUM([tblCalcBaseCostByField].Amount) AS InternalCost
		, [tblCalcBaseCostByField].RootCodeZiff AS SortOrderExport
	FROM [tblCalcBaseCostByField] 
	WHERE [tblCalcBaseCostByField].IdModel = @IdModel  AND IdRootZiffAccount IS NOT NULL AND [tblCalcBaseCostByField].IdField IN (SELECT [IdField] FROM #FieldList)
	GROUP BY [tblCalcBaseCostByField].IdModel
		, [tblCalcBaseCostByField].IdField
		, [tblCalcBaseCostByField].Field
		, [tblCalcBaseCostByField].RootCodeZiff
		, [tblCalcBaseCostByField].RootZiffAccount
	ORDER BY 4		

	/** Get Full List of ZiffAccounts **/
	CREATE TABLE #ZiffAccounts (IdModel INT, IdField INT, Field NVARCHAR(255), RootCodeZiff NVARCHAR(255), Parent NVARCHAR(255), RootZiffAccount NVARCHAR(255), InternalCost FLOAT, SortOrderExport NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #ZiffAccounts (IdModel,IdField,Field,RootCodeZiff,Parent,RootZiffAccount,InternalCost,SortOrderExport)
	SELECT CNList.IdModel, CNList.IdField, CNList.Field, CYList.Code, CYList.Parent, CYList.Name, 0, CYList.SortOrder 
	FROM
		(SELECT [IdModel],[IdField],[Field] FROM #InternalCosts WHERE [RootCodeZiff] IS NOT NULL GROUP BY [IdModel],[IdField],[Field])AS CNList CROSS JOIN
		(SELECT [Code],[Name],[Parent],[SortOrder] FROM tblZiffAccounts WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing ZiffAccounts to #InternalCosts to Fill Properly **/
	INSERT INTO #InternalCosts (IdModel,IdField,Field,RootCodeZiff,RootZiffAccount,InternalCost,SortOrderExport)
	SELECT	#ZiffAccounts.IdModel,#ZiffAccounts.IdField,#ZiffAccounts.Field, #ZiffAccounts.RootCodeZiff, #ZiffAccounts.RootZiffAccount, 0 AS InternalCost, #ZiffAccounts.SortOrderExport
	FROM	#ZiffAccounts LEFT OUTER JOIN
			#InternalCosts ON #ZiffAccounts.IdModel=#InternalCosts.IdModel AND #ZiffAccounts.IdField=#InternalCosts.IdField AND #ZiffAccounts.RootCodeZiff=#InternalCosts.RootCodeZiff
	WHERE	#InternalCosts.RootCodeZiff IS NULL;

	/* Apply Volume to get Unit Cost */
	UPDATE	#InternalCosts
	SET		#InternalCosts.InternalCost = #InternalCosts.InternalCost/FieldVolume.VolumeAmount
	FROM	#InternalCosts LEFT OUTER JOIN 
			( SELECT IdField, SUM(VolumeAmount) AS VolumeAmount FROM #FieldVolume GROUP BY IdField) AS FieldVolume ON
			#InternalCosts.IdField = FieldVolume.IdField
	END /** Internal Costs **/

	BEGIN /** External Costs **/
	DECLARE @CheckedField INT = 0
	/* Populate tblCalcExternalCostReferences */
	DELETE FROM tblCalcExternalCostReferences WHERE IdModel=@IdModel
	
	INSERT INTO tblCalcExternalCostReferences (IdModel,IdZiffBaseCost,IdZiffAccount,IdPeerGroup,IdPeerCriteria,UnitCost,Quantity,TotalCost,CodeZiff,ZiffAccount,IdRootZiffAccount,
										 RootCodeZiff,RootZiffAccount,PeerCriteria,Unit,IdUnitPeerCriteria,TechApplicable,TFNormal,TFPessimistic,TFOptimistic,BCRNormal,
										 BCRPessimistic,BCROptimistic, SortOrder, ExternalAccountCode, PeerGroupDescription)	
	SELECT CBCBFZ.IdModel,CBCBFZ.IdZiffBaseCost,CBCBFZ.IdZiffAccount,CBCBFZ.IdPeerGroup,CBCBFZ.IdPeerCriteria,CBCBFZ.UnitCost,CBCBFZ.Quantity,CBCBFZ.TotalCost,
		   '&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;'+CBCBFZ.CodeZiff as CodeZiff,CBCBFZ.ZiffAccount,CBCBFZ.IdRootZiffAccount,CBCBFZ.RootCodeZiff,CBCBFZ.RootZiffAccount,
		   CBCBFZ.PeerCriteria,CBCBFZ.Unit,CBCBFZ.IdUnitPeerCriteria,CBCBFZ.TechApplicable,CBCBFZ.TFNormal,CBCBFZ.TFPessimistic,CBCBFZ.TFOptimistic,CBCBFZ.BCRNormal,
		   CBCBFZ.BCRPessimistic,CBCBFZ.BCROptimistic,ZA.SortOrder, CBCBFZ.CodeZiff AS ExternalAccountCode , PG.PeerGroupCode
	FROM tblCalcBaseCostByFieldZiff CBCBFZ left outer join tblZiffAccounts ZA on 
		 ZA.IdModel=CBCBFZ.IdModel and ZA.IdZiffAccount=CBCBFZ.IdZiffAccount INNER JOIN tblPeerGroup PG ON
		 CBCBFZ.IdPeerGroup = PG.IdPeerGroup
	WHERE CBCBFZ.IdModel = @IdModel AND CBCBFZ.IdField IN (SELECT tblPeerGroupField.IdField
															FROM tblPeerCriteriaCost INNER JOIN
																tblPeerGroup ON tblPeerCriteriaCost.IdPeerGroup = tblPeerGroup.IdPeerGroup INNER JOIN
																tblPeerGroupField ON tblPeerGroup.IdPeerGroup = tblPeerGroupField.IdPeerGroup
															WHERE IdModel = @IdModel
															GROUP BY tblPeerGroupField.IdField) 

	CREATE TABLE #ExternalCostReferences (IdModel INT,IdZiffAccount INT,IdField INT,IdPeerGroup INT,PeerGroupDescription VARCHAR(100),CodeZiff VARCHAR(50),ZiffAccount VARCHAR(150),UnitCost FLOAT, SortOrderExport NVARCHAR(255))	
	INSERT INTO #ExternalCostReferences (IdModel,IdZiffAccount,IdField,IdPeerGroup,PeerGroupDescription,CodeZiff,ZiffAccount,UnitCost,SortOrderExport)	
	SELECT CECR.IdModel,CECR.IdZiffAccount,PGF.IdField,CECR.IdPeerGroup,CECR.PeerGroupDescription,CECR.ExternalAccountCode,CECR.ZiffAccount,CECR.UnitCost,CECR.SortOrder
	FROM tblCalcExternalCostReferences CECR INNER JOIN tblPeerGroupField PGF ON
		CECR.IdPeerGroup=PGF.IdPeerGroup
	WHERE CECR.IdModel= @IdModel 
	AND CECR.IdPeerGroup IN (SELECT IdPeerGroup FROM #PeerGroupList)
	GROUP BY CECR.IdModel,CECR.IdZiffAccount,PGF.IdField,CECR.IdPeerGroup,CECR.PeerGroupDescription,CECR.ExternalAccountCode,CECR.ZiffAccount,CECR.UnitCost,CECR.SortOrder
	UNION 
	SELECT ZA.IdModel,ZA.IdZiffAccount,A.IdField,A.IdPeerGroup,A.PeerGroupCode,ZA.Code,ZA.Name,SUM(A.UnitCost),A.RootCodeZiff
	FROM tblZiffAccounts ZA INNER JOIN
	(SELECT CBC.RootCodeZiff,CBC.UnitCost,CBC.IdField,PG.IdPeerGroup,PG.PeerGroupCode
	 FROM tblCalcBaseCostByFieldZiff CBC INNER JOIN tblPeerGroup PG 
	 ON CBC.IdPeerGroup=PG.IdPeerGroup 
	 WHERE CBC.IdModel = @IdModel
	 AND CBC.IdPeerGroup  IN (SELECT IdPeerGroup FROM #PeerGroupList)
	 GROUP BY CBC.RootCodeZiff,CBC.UnitCost,CBC.IdField,PG.IdPeerGroup,PG.PeerGroupCode) AS A 
	ON ZA.Code = A.RootCodeZiff
	WHERE ZA.IdModel = @IdModel
	AND ZA.Parent IS NULL
	GROUP BY ZA.IdModel,ZA.IdZiffAccount,A.IdField,A.IdPeerGroup,A.PeerGroupCode,ZA.Code,ZA.Name,A.RootCodeZiff
	ORDER BY 4,3,6

	/** Get Full List of ZiffAccounts **/
	CREATE TABLE #ZiffAccountsExternal (IdModel INT, IdZiffAccount INT, IdField INT, IdPeerGroup INT, PeerGroupDescription NVARCHAR(255), CodeZiff NVARCHAR(255), ZiffAccount NVARCHAR(255), InternalCost FLOAT, SortOrderExport NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #ZiffAccountsExternal (IdModel,IdZiffAccount,IdField,IdPeerGroup,PeerGroupDescription,CodeZiff,ZiffAccount,InternalCost,SortOrderExport)
	SELECT CNList.IdModel, CYList.IdZiffAccount, CNList.IdField, CNList.IdPeerGroup, CNList.PeerGroupDescription, CYList.Code, CYList.Name, 0, CYList.SortOrder 
	FROM
		(SELECT [IdModel],[IdField],[IdPeerGroup],[PeerGroupDescription] FROM #ExternalCostReferences WHERE [CodeZiff] IS NOT NULL GROUP BY [IdModel],[IdField],[PeerGroupDescription],[IdPeerGroup])AS CNList CROSS JOIN
		(SELECT [Code],[Name],[Parent],[SortOrder],[IdZiffAccount] FROM tblZiffAccounts WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing ZiffAccounts to #ExternalCostReferences to Fill Properly **/
	INSERT INTO #ExternalCostReferences (IdModel,IdZiffAccount,IdField,IdPeerGroup,PeerGroupDescription,CodeZiff,ZiffAccount,UnitCost,SortOrderExport)	
	SELECT	#ZiffAccountsExternal.IdModel,#ZiffAccountsExternal.IdZiffAccount,#ZiffAccountsExternal.IdField,#ZiffAccountsExternal.IdPeerGroup, #ZiffAccountsExternal.PeerGroupDescription,#ZiffAccountsExternal.CodeZiff, #ZiffAccountsExternal.ZiffAccount, 0 AS InternalCost, #ZiffAccountsExternal.SortOrderExport
	FROM	#ZiffAccountsExternal LEFT OUTER JOIN
			#ExternalCostReferences ON #ZiffAccountsExternal.IdModel=#ExternalCostReferences.IdModel AND #ZiffAccountsExternal.IdField=#ExternalCostReferences.IdField AND #ZiffAccountsExternal.CodeZiff=#ExternalCostReferences.CodeZiff
	WHERE	#ExternalCostReferences.CodeZiff IS NULL;

	/** Need to differentiate the peer groups from the fields when they have the same names **/
	DECLARE @CheckingField NVARCHAR(MAX),@StartPos INT = 0
	SET @WhereComma = CHARINDEX(',',@FieldColumnList,@StartPos)
	WHILE (1=1)
	BEGIN
		SET @CheckingField = REPLACE(REPLACE(SUBSTRING(@FieldColumnList,@StartPos+1,@WhereComma-@StartPos-1),'[',''),']','')
		SET @StartPos = @WhereComma
		SET @WhereComma = CHARINDEX(',',@FieldColumnList,@StartPos+1)

		UPDATE #ExternalCostReferences SET PeerGroupDescription = PeerGroupDescription + '_PG' WHERE PeerGroupDescription = @CheckingField
		IF @WhereComma = 0 BREAK
	END
	/** Execute last instance **/
	SET @CheckingField = REPLACE(REPLACE(SUBSTRING(@FieldColumnList,@StartPos+1,LEN(@FieldColumnList)-@StartPos-1),'[',''),']','')
	UPDATE #ExternalCostReferences SET PeerGroupDescription = PeerGroupDescription + '_PG' WHERE PeerGroupDescription = @CheckingField

	END /** External Costs **/
	
	BEGIN /** Generate Parent List for Export Use **/
	CREATE TABLE #ParentList (Code NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #ParentList (Code)
	SELECT CYList.Code
	FROM
		(SELECT [IdModel],[IdField],[Field] FROM #InternalCosts WHERE [RootCodeZiff] IS NOT NULL GROUP BY [IdModel],[IdField],[Field])AS CNList CROSS JOIN
		(SELECT [Code],[Name],[Parent],[SortOrder] FROM tblZiffAccounts WHERE IdModel=@IdModel) AS CYList 
	WHERE CYList.Parent IS NULL
	GROUP BY CYList.Code
	END /** Generate Parent List for Export Use **/
	
	BEGIN /** FOR DISPLAY (#Costs) **/
	CREATE TABLE #Costs (RootCodeZiff NVARCHAR(255), RootZiffAccount NVARCHAR(255), ReferenceDescription NVARCHAR(255), ReferenceCost FLOAT, SortOrder INT, SortOrderExport NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #Costs (RootCodeZiff, RootZiffAccount,ReferenceDescription,ReferenceCost,SortOrder,SortOrderExport)
	SELECT #InternalCosts.RootCodeZiff,#InternalCosts.RootZiffAccount,#InternalCosts.Field,ROUND(#InternalCosts.InternalCost * @CurrencyFactor,2),1,#InternalCosts.SortOrderExport
	FROM #InternalCosts LEFT OUTER JOIN #ZiffAccounts ON #ZiffAccounts.RootCodeZiff = #InternalCosts.RootCodeZiff 
	WHERE #ZiffAccounts.Parent IS NULL
	GROUP BY #InternalCosts.Field,#InternalCosts.RootCodeZiff,#InternalCosts.RootZiffAccount,#InternalCosts.InternalCost,#InternalCosts.SortOrderExport
	UNION
	SELECT CodeZiff,ZiffAccount,PeerGroupDescription,ROUND(UnitCost * @CurrencyFactor,2),1,#ExternalCostReferences.SortOrderExport
	FROM #ExternalCostReferences LEFT OUTER JOIN #ZiffAccounts ON #ZiffAccounts.RootCodeZiff = #ExternalCostReferences.CodeZiff  
	WHERE #ZiffAccounts.Parent IS NULL
	ORDER BY 1,3

	/** Generate Code columns to be used for output of chart data **/
	DECLARE @CodeList NVARCHAR(MAX)='FieldTotal';
	DECLARE @CodeName NVARCHAR(255);
	DECLARE CodeList CURSOR FOR
	SELECT /*RootCodeZiff*/ RootZiffAccount FROM #Costs GROUP BY /*RootCodeZiff*/ RootZiffAccount;

	OPEN CodeList;
	FETCH NEXT FROM CodeList INTO @CodeName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @CodeList=''
		BEGIN
			SET @CodeList =  @CodeList + '[' + @CodeName + ']';
		END
		ELSE
		BEGIN
			SET @CodeList =  @CodeList + ',[' + @CodeName + ']';
		END
		FETCH NEXT FROM CodeList INTO @CodeName;
	END

	CLOSE CodeList;
	DEALLOCATE CodeList;

	INSERT INTO #Costs (RootCodeZiff, RootZiffAccount,ReferenceDescription,ReferenceCost,SortOrder)
	SELECT 'Total Production','',FieldName,ROUND(VolumeAmount,2),4
	FROM #FieldVolume 
	WHERE IdField in (SELECT IdField FROM #FieldList)

	INSERT INTO #Costs (RootCodeZiff, RootZiffAccount,ReferenceDescription,ReferenceCost,SortOrder)
	SELECT 'Total Production','',tblPeerGroup.PeerGroupCode, ROUND(tblPeerCriteriaCost.Quantity,2) AS VolumeAmount,4
	FROM tblPeerGroupField INNER JOIN tblPeerCriteriaCost ON tblPeerCriteriaCost.IdPeerGroup = tblPeerGroupField.IdPeerGroup INNER JOIN
	tblPeerGroup ON tblPeerGroupField.IdPeerGroup=tblPeerGroup.IdPeerGroup
	WHERE tblPeerCriteriaCost.IdPeerGroup IN (SELECT IdPeerGroup FROM #PeerGroupList)
	AND tblPeerGroupField.IdField in (select IdField from #FieldList)
	GROUP BY tblPeerGroup.PeerGroupCode,tblPeerCriteriaCost.Quantity	

	INSERT INTO #Costs (RootCodeZiff, RootZiffAccount,ReferenceDescription,ReferenceCost,SortOrder)
	SELECT 99,'','','',3
	
	INSERT INTO #Costs (RootCodeZiff, RootZiffAccount,ReferenceDescription,ReferenceCost,SortOrder)
	SELECT 'Total Unit Cost','',ReferenceDescription,ROUND(SUM(ReferenceCost),2),2
	FROM #Costs
	WHERE RootCodeZiff <> 'Total Production'
	GROUP BY ReferenceDescription
	END  /** FOR DISPLAY (#Costs) **/
		
	BEGIN /** FOR EXPORT (#ExportCosts) **/
	CREATE TABLE #ExportCosts (RootCodeZiff NVARCHAR(255), RootZiffAccount NVARCHAR(255), ReferenceDescription NVARCHAR(255), ReferenceCost FLOAT, SortOrder INT, SortOrderExport NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #ExportCosts (RootCodeZiff, RootZiffAccount,ReferenceDescription,ReferenceCost,SortOrder,SortOrderExport)
	SELECT RootCodeZiff,RootZiffAccount,Field,ROUND(InternalCost * @CurrencyFactor,2),1,SortOrderExport
	FROM #InternalCosts
	UNION
	SELECT CodeZiff,ZiffAccount,PeerGroupDescription,ROUND(UnitCost * @CurrencyFactor,2),1,SortOrderExport
	FROM #ExternalCostReferences
	ORDER BY 3,6

	INSERT INTO #ExportCosts (RootCodeZiff, RootZiffAccount,ReferenceDescription,ReferenceCost,SortOrder,SortOrderExport)
	SELECT 'Total Production','',FieldName,ROUND(SUM(VolumeAmount),2),2,'ZYX_01'
	FROM #FieldVolume 
	WHERE IdField in (SELECT IdField FROM #FieldList)
	GROUP BY FieldName

	INSERT INTO #ExportCosts (RootCodeZiff, RootZiffAccount,ReferenceDescription,ReferenceCost,SortOrder,SortOrderExport)
	SELECT 'Total Production','',tblPeerGroup.PeerGroupCode, ROUND(tblPeerCriteriaCost.Quantity,2) AS VolumeAmount, 2,'ZYX_01'
	FROM tblPeerGroupField INNER JOIN tblPeerCriteriaCost ON tblPeerCriteriaCost.IdPeerGroup = tblPeerGroupField.IdPeerGroup INNER JOIN
	tblPeerGroup ON tblPeerGroupField.IdPeerGroup=tblPeerGroup.IdPeerGroup
	WHERE tblPeerCriteriaCost.IdPeerGroup IN (SELECT IdPeerGroup FROM #PeerGroupList) 
	AND tblPeerGroupField.IdField in (select IdField from #FieldList)
	GROUP BY tblPeerGroup.PeerGroupCode,tblPeerCriteriaCost.Quantity	

	INSERT INTO #ExportCosts (RootCodeZiff, RootZiffAccount,ReferenceDescription,ReferenceCost,SortOrder,SortOrderExport)
	SELECT 'Total Unit Cost','',ReferenceDescription,ROUND(SUM(ReferenceCost),2),0,'AAA_01'
	FROM #ExportCosts
	WHERE RootCodeZiff <> 'Total Production' AND RootCodeZiff IN (SELECT Code FROM #ParentList)

	GROUP BY ReferenceDescription
	END /** FOR EXPORT (#ExportCosts) **/

	BEGIN /** Build Output Table **/
	DECLARE @SQLOutput NVARCHAR(MAX);
	SET @SQLOutput = 'SELECT [RootCodeZiff] AS Code, [RootZiffAccount] AS Description,' + @ReferenceDescriptionList + ' FROM #Costs PIVOT (SUM(ReferenceCost) FOR ReferenceDescription IN (' + @ReferenceDescriptionList + ')) AS PivotTable ORDER BY [SortOrder],[RootCodeZiff]';
	EXEC(@SQLOutput);

	DECLARE @SQLExport NVARCHAR(MAX);
	SET @SQLExport = 'SELECT [SortOrderExport], [RootCodeZiff] AS Code, [RootZiffAccount] AS Description,' + @ReferenceDescriptionList + ' FROM #ExportCosts PIVOT (SUM(ReferenceCost) FOR ReferenceDescription IN (' + @ReferenceDescriptionList + ')) AS PivotTable ORDER BY [SortOrder],[SortOrderExport],[RootCodeZiff]';
	EXEC(@SQLExport);

	SELECT ReferenceDescription AS Field, CASE WHEN RootZiffAccount = '' THEN 'FieldTotal' ELSE RootZiffAccount END AS Code, ReferenceCost AS Value INTO #CostChart FROM #Costs WHERE RootCodeZiff <> 'Total Production' AND RootCodeZiff <> '99' AND ReferenceDescription <> '' ORDER by ReferenceDescription

	DECLARE @SQLChart NVARCHAR(MAX);
	SET @SQLChart = 'SELECT Field,' + @CodeList + ' FROM #CostChart PIVOT (SUM(Value) FOR Code IN (' + @CodeList + ')) AS PivotTable ORDER BY [FieldTotal]';
	EXEC(@SQLChart);

	DECLARE @CategoriesCount INT
	SELECT @CategoriesCount = COUNT(A.RootZiffAccount)
	FROM
	(SELECT RootZiffAccount FROM #Costs GROUP BY RootCodeZiff,RootZiffAccount HAVING RootZiffAccount <> '' ) AS A

	/** Output Stats Table for UI **/
	SELECT @FieldCount AS FieldCount, @CategoriesCount AS CategoriesCount, @PeerGroupCount AS PeerGroupCount;
	END /** Build Output Table **/

END

GO

/****** Object:  StoredProcedure [dbo].[reppCostVsDriversProjection]    Script Date: 03/23/2016 10:19:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[reppCostVsDriversProjection]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[reppCostVsDriversProjection]
GO

/****** Object:  StoredProcedure [dbo].[reppCostVsDriversProjection]    Script Date: 03/04/2016 06:43:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:		Rodrigo Manubens
-- Create date:	2016-03-04
-- Description:	Report Cost Vs Drivers Projection
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- ===========================================================================================
CREATE PROCEDURE [dbo].[reppCostVsDriversProjection](
@IdModel INT,
@IdAggregationLevel INT, 
@IdField INT,
@From INT,
@To INT,
@IdTechnicalScenario INT,
@IdEconomicScenario INT,
@IdCurrency INT,
@IdTerm INT,
@IdTechnicalDriver INT,
@IdEconomicDriver INT,
@CostCategories NVARCHAR(MAX),
@Factor INT,
@IdCostType INT
)
AS
BEGIN

	BEGIN /** DROP Temporary Tables **/
	/** DROP Temporary Tables **/
	IF OBJECT_ID('tempdb..#FieldList') IS NOT NULL DROP TABLE #FieldList
	IF OBJECT_ID('tempdb..#CostCategoryList') IS NOT NULL DROP TABLE #CostCategoryList
	IF OBJECT_ID('tempdb..#VolumeReportBLRows') IS NOT NULL DROP TABLE #VolumeReportBLRows  
	IF OBJECT_ID('tempdb..#VolumeReportBL') IS NOT NULL DROP TABLE #VolumeReportBL  
	IF OBJECT_ID('tempdb..#VolumeReportBORows') IS NOT NULL DROP TABLE #VolumeReportBORows
	IF OBJECT_ID('tempdb..#VolumeReportBO') IS NOT NULL DROP TABLE #VolumeReportBO
	IF OBJECT_ID('tempdb..#VolumeReportBLList') IS NOT NULL DROP TABLE #VolumeReportBLList 
	IF OBJECT_ID('tempdb..#VolumeReportBOList') IS NOT NULL DROP TABLE #VolumeReportBOList 
	IF OBJECT_ID('tempdb..#VolumeReportGrouped') IS NOT NULL DROP TABLE #VolumeReportGrouped
	IF OBJECT_ID('tempdb..#VolumeReportGroupedBL') IS NOT NULL DROP TABLE #VolumeReportGroupedBL 
	IF OBJECT_ID('tempdb..#VolumeReportGroupedBO') IS NOT NULL DROP TABLE #VolumeReportGroupedBO 
	IF OBJECT_ID('tempdb..#ZiffAccounts') IS NOT NULL DROP TABLE #ZiffAccounts
	IF OBJECT_ID('tempdb..#Totals') IS NOT NULL DROP TABLE #Totals
	IF OBJECT_ID('tempdb..#ProjectionYears') IS NOT NULL DROP TABLE #ProjectionYears
	IF OBJECT_ID('tempdb..#EconomicDrivers') IS NOT NULL DROP TABLE #EconomicDrivers
	IF OBJECT_ID('tempdb..#MissingEconomicDrivers') IS NOT NULL DROP TABLE #MissingEconomicDrivers	
	IF OBJECT_ID('tempdb..#PerformanceDrivers') IS NOT NULL DROP TABLE #PerformanceDrivers
	IF OBJECT_ID('tempdb..#MissingPerformanceDrivers') IS NOT NULL DROP TABLE #MissingPerformanceDrivers
	IF OBJECT_ID('tempdb..#SizeDrivers') IS NOT NULL DROP TABLE #SizeDrivers
	IF OBJECT_ID('tempdb..#MissingSizeDrivers') IS NOT NULL DROP TABLE #MissingSizeDrivers	
	IF OBJECT_ID('tempdb..#Opex') IS NOT NULL DROP TABLE #Opex
	IF OBJECT_ID('tempdb..#ChartTotals') IS NOT NULL DROP TABLE #ChartTotals
	IF OBJECT_ID('tempdb..#UnitCosts') IS NOT NULL DROP TABLE #UnitCosts
	IF OBJECT_ID('tempdb..#AbsoluteCost') IS NOT NULL DROP TABLE #AbsoluteCost
	IF OBJECT_ID('tempdb..#Production') IS NOT NULL DROP TABLE #Production
	IF OBJECT_ID('tempdb..#YearList') IS NOT NULL DROP TABLE #YearList
	IF OBJECT_ID('tempdb..#FieldsFillers') IS NOT NULL DROP TABLE #FieldsFillers
	END /** DROP Temporary Tables **/

	BEGIN /** Build List Tables for reports **/
	/** Technical Scenario List **/
	DECLARE @CaseTypeList NVARCHAR(MAX) = 'Baseline, Incremental' 

	DECLARE @StartYear INT = @From;
	DECLARE @EndYear INT = @To;
	
	/** Get Base Year **/
	DECLARE @BaseYear INT
	SELECT @BaseYear = [BaseYear] FROM [tblModels] WHERE IdModel=@IdModel

	/** Build Year Columns and Year List Table **/
	CREATE TABLE #YearList (ProjectionYear INT)
	DECLARE @YearColumnList NVARCHAR(MAX)='';
	DECLARE @YearFrom INT
	SET @YearFrom=@From	
	WHILE (@YearFrom <= @To)
	BEGIN
		INSERT INTO #YearList Values(@YearFrom)
		SET @YearColumnList =  @YearColumnList + '[' + CAST(@YearFrom AS VARCHAR(4)) + ']';
		SET @YearFrom = @YearFrom + 1;
		IF (@YearFrom <= @To)
		BEGIN
			SET @YearColumnList = @YearColumnList + ',';
		END
	END

	/** Build Field List **/
	/** Need to check whether there are more than one field being passed in, look for comma in input variable **/
	--DECLARE @nonAll int 
	--SELECT @nonAll = CHARINDEX(',', @IdField);
	DECLARE @SQL1 AS NVARCHAR(MAX)

	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF (@IdField = 0) /** Not multiple fields **/
	BEGIN	
		--INSERT INTO #FieldList VALUES(@IdField)
		IF (@IdAggregationLevel=0 AND @IdField=0)
		BEGIN
			INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdModel]=@IdModel GROUP BY [IdField];
		END
		ELSE IF (@IdAggregationLevel>0 AND @IdField>0)
		BEGIN
			INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
		END
		ELSE IF (@IdField>0)
		BEGIN
			INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
		END
		ELSE IF (@IdAggregationLevel>0)
		BEGIN
			INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdModel]=@IdModel GROUP BY [IdField];
		END
		ELSE IF (@IdField>0)
		BEGIN
			INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
		END
	END
	ELSE /** Multiple fields **/
	BEGIN
		SET @SQL1 = 'INSERT INTO #FieldList SELECT [IdField] FROM tblFields WHERE [IdModel]= ' + cast(@IdModel AS VARCHAR(10)) + ' AND [IdField] IN (' + cast(@IdField as varchar(10)) + ') GROUP BY [IdField];'
		EXEC(@SQL1)
	END

	DECLARE @FieldCount INT;
	SELECT @FieldCount=COUNT([IdField]) FROM #FieldList

	/** Build Cost Categories List **/
	/** Need to check whether there are more than one field being passed in, look for comma in input variable **/
	DECLARE @nonAllCC int 
	SELECT @nonAllCC = CHARINDEX(',', @CostCategories);
	DECLARE @SQL2 AS NVARCHAR(MAX)

	CREATE TABLE #CostCategoryList (IdZiffAccount INT PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF (@nonAllCC = 0) /** Not multiple fields **/
	BEGIN	
		IF (@CostCategories = 0)
		BEGIN
			INSERT INTO #CostCategoryList 
			SELECT IdZiffAccount FROM tblZiffAccounts WHERE IdModel = @IdModel AND Parent IS NULL
		END
		ELSE
			INSERT INTO #CostCategoryList VALUES(@CostCategories)
	END
	ELSE /** Multiple fields **/
	BEGIN
		SET @SQL1 = 'INSERT INTO #CostCategoryList SELECT [IdZiffAccount] FROM tblZiffAccounts WHERE [IdModel]= ' + cast(@IdModel AS VARCHAR(10)) + ' AND [IdZiffAccount] IN (' + @CostCategories + ') GROUP BY [IdZiffAccount];'
		EXEC(@SQL1)
	END

	/** Get Currency Factor to use **/
	DECLARE @CurrencyFactor FLOAT;
	SELECT @CurrencyFactor=[Value] FROM tblModelsCurrencies WHERE [IdCurrency]=@IdCurrency AND [IdModel]=@IdModel;
	END /** Build List Tables for reports **/

	BEGIN	/** Build Volume Tables **/
	/** Report (Baseline)**/
	CREATE TABLE #VolumeReportBLRows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLRows ([ParentName], [Name], [YearProjection], [Amount])	
	SELECT	DISTINCT N'All Cost Accounts' AS ParentName, dbo.tblTechnicalDrivers.Name AS Name,
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Normal,0))
				WHEN @IdTechnicalScenario=2 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Optimistic,0))
				WHEN @IdTechnicalScenario=3 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0))
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject AND
			dbo.tblTechnicalDriverData.Year >= dbo.tblProjects.Starts
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=1
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	GROUP BY  dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.Name
	ORDER BY dbo.tblTechnicalDriverData.Year
	
	CREATE TABLE #VolumeReportBL ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBL([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBLRows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBLList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBL WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #VolumeReportBLList to Fill Properly **/
	INSERT INTO #VolumeReportBL ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBLList.ParentName,#VolumeReportBLList.Name, #VolumeReportBLList.YearProjection,0 AS Amount
	FROM	#VolumeReportBLList LEFT OUTER JOIN
			#VolumeReportBL ON #VolumeReportBLList.ParentName=#VolumeReportBL.ParentName AND #VolumeReportBLList.Name=#VolumeReportBL.Name AND #VolumeReportBLList.YearProjection=#VolumeReportBL.YearProjection
	WHERE	#VolumeReportBL.YearProjection IS NULL;

	/** Delete all rows that have 0 (zero) Amount **/
	DELETE #VolumeReportBL WHERE Amount=0

	/** Report (Business Opportunities)**/
	CREATE TABLE #VolumeReportBORows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBORows ([ParentName], [Name], [YearProjection], [Amount])	
	SELECT	DISTINCT N'All Cost Accounts' AS ParentName, dbo.tblTechnicalDrivers.Name AS Name,
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Normal,0))
				WHEN @IdTechnicalScenario=2 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Optimistic,0))
				WHEN @IdTechnicalScenario=3 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0))
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject AND
			dbo.tblTechnicalDriverData.Year >= dbo.tblProjects.Starts
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=2
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	GROUP BY  dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.Name
	ORDER BY dbo.tblTechnicalDriverData.Year

	CREATE TABLE #VolumeReportBO ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBO([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBORows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBOList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBOList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBO WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #VolumeReportBOList to Fill Properly **/
	INSERT INTO #VolumeReportBO ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBOList.ParentName,#VolumeReportBOList.Name, #VolumeReportBOList.YearProjection,0 AS Amount
	FROM	#VolumeReportBOList LEFT OUTER JOIN
			#VolumeReportBO ON #VolumeReportBOList.ParentName=#VolumeReportBO.ParentName AND #VolumeReportBOList.Name=#VolumeReportBO.Name AND #VolumeReportBOList.YearProjection=#VolumeReportBO.YearProjection
	WHERE	#VolumeReportBO.YearProjection IS NULL;

	/** Delete all rows that have 0 (zero) Amount **/
	DELETE #VolumeReportBO WHERE Amount=0

	/** Report (All Fields Grouped)**/
	CREATE TABLE #VolumeReportGroupedBL ([YearProjection] INT, [Amount] FLOAT, [ParentName] NVARCHAR(255), [Name] NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBL ([YearProjection], [Amount], [ParentName], [Name])
	SELECT YearProjection,SUM(amount) AS Amount,'GroupedBL',Name
	FROM #VolumeReportBL
	GROUP BY YearProjection,Name
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGroupedBO ([YearProjection] INT, [Amount] FLOAT, [ParentName] NVARCHAR(255), [Name] NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBO ([YearProjection], [Amount], [ParentName], [Name])
	SELECT YearProjection,SUM(amount) AS Amount,'GroupedBO',Name
	FROM #VolumeReportBO
	GROUP BY YearProjection,Name
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGrouped ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255),[YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGrouped ([ParentName], [Name], [YearProjection], [Amount])
	SELECT N'All Cost Accounts' AS ParentName,Name,YearProjection,SUM(Amount) AS 'Amount'
	FROM
	(
		SELECT 'VolumeReportGroupedBL' AS "ProjectType", ParentName, Name, YearProjection,SUM(Amount) AS "Amount"
		FROM #VolumeReportGroupedBL 
		GROUP BY ParentName,Name,YearProjection
		UNION
		SELECT 'VolumeReportGroupedBO', ParentName, Name, YearProjection, SUM(Amount) AS "Amount"
		FROM #VolumeReportGroupedBO 
		GROUP BY ParentName,Name,YearProjection
	) AS VolumeReportGroupedTotals
	GROUP BY Name,YearProjection
	ORDER BY Name,YearProjection
	END 	/** Build Volume Tables **/

	DECLARE @TotalCapacityDriver INT = 0;

	/** Get grouped driver value for Base Year **/
	SELECT @TotalCapacityDriver = Amount FROM #VolumeReportGrouped WHERE YearProjection=@BaseYear

	BEGIN /** Build Source Data Table - Total **/
	CREATE TABLE #Opex (ParentName NVARCHAR(255), IdRootZiffAccount INT, CostCategory NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #Opex ([ParentName],[IdRootZiffAccount],[CostCategory],[Name],[YearProjection],[CostProjection])
	SELECT tblCalcProjectionTechnical.Field AS ParentName, tblCalcProjectionTechnical.IdRootZiffAccount, tblCalcProjectionTechnical.RootCodeZiff AS CostCategory, 
		tblCalcProjectionTechnical.RootZiffAccount AS Name, 
		tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) 
	AND tblCalcProjectionTechnical.IdRootZiffAccount in (SELECT IdZiffAccount FROM #CostCategoryList)
	AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	GROUP BY tblCalcProjectionTechnical.RootZiffAccount, tblCalcProjectionTechnical.Year,tblCalcProjectionTechnical.RootCodeZiff,tblCalcProjectionTechnical.IdRootZiffAccount,tblCalcProjectionTechnical.Field
	END /** Build Source Data Table - Total **/
		
	BEGIN /** Create all years selected **/
	CREATE TABLE #ProjectionYears (Year INT) ON [PRIMARY]
	DECLARE @cntYear INT

	SET @cntYear = @From

	WHILE @cntYear <= @To
	BEGIN
	   INSERT INTO #ProjectionYears ([Year])
	   VALUES(@cntYear)
	   SET @cntYear = @cntYear + 1;
	END;
	END /** Create all years selected **/
	
	BEGIN /** Absolute Cost **/
	/** Absolute Cost **/
	CREATE TABLE #AbsoluteCost (FieldName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #AbsoluteCost ([FieldName],[YearProjection],[CostProjection])
	SELECT tblCalcProjectionTechnical.Field AS ParentName,tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection
	FROM tblCalcProjectionTechnical  LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel 
	AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) 
	AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	GROUP BY tblCalcProjectionTechnical.Field,tblCalcProjectionTechnical.Year

	/** Get Full List of ZiffAccounts **/
	CREATE TABLE #FieldsFillers (FieldName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #FieldsFillers ([FieldName],[YearProjection],[CostProjection])
	SELECT CYList.Name, YList.ProjectionYear, 0 FROM
		(SELECT [FieldName] FROM #AbsoluteCost WHERE [FieldName] IS NOT NULL GROUP BY [FieldName])AS CNList CROSS JOIN
		(SELECT [Name] FROM tblFields WHERE IdModel = @IdModel AND IdField IN (SELECT * FROM #FieldList)) AS CYList CROSS JOIN
		(SELECT [ProjectionYear] FROM #YearList) AS YList

	/** Add Any Missing Years to #AbsoluteCost to Fill Properly **/
	INSERT INTO #AbsoluteCost ([FieldName],[YearProjection],[CostProjection])
	SELECT	#FieldsFillers.FieldName, #FieldsFillers.YearProjection,0 AS CostProjection
	FROM	#FieldsFillers LEFT OUTER JOIN
			#AbsoluteCost ON #FieldsFillers.FieldName=#AbsoluteCost.FieldName AND #FieldsFillers.YearProjection=#AbsoluteCost.YearProjection
	WHERE	#AbsoluteCost.YearProjection IS NULL;
	END /** Absolute Cost **/
	
	BEGIN /** Production **/
	/** Production **/
	CREATE TABLE #Production (FieldName NVARCHAR(255),Name NVARCHAR(255), YearProjection INT, Amount FLOAT) ON [PRIMARY]

	DECLARE @FieldName NVARCHAR(255);
	DECLARE FieldList CURSOR FOR
	SELECT Name FROM [tblFields] where IdField in (SELECT IdField FROM #FieldList);

	OPEN FieldList;
	FETCH NEXT FROM FieldList INTO @FieldName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		INSERT INTO #Production ([FieldName],[Name],[YearProjection],[Amount])
		SELECT @FieldName, Name, YearProjection,Amount/1000 FROM #VolumeReportGroupedBL--#VolumeReportGrouped
		FETCH NEXT FROM FieldList INTO @FieldName;

		INSERT INTO #Production ([FieldName],[Name],[YearProjection],[Amount])
		SELECT @FieldName, Name, YearProjection,Amount/1000 FROM #VolumeReportGroupedBO--#VolumeReportGrouped
		FETCH NEXT FROM FieldList INTO @FieldName;
	END

	CLOSE FieldList;
	DEALLOCATE FieldList;
	END /** Production **/

	BEGIN /** Unit Costs **/
	/** Unit Costs **/
	CREATE TABLE #UnitCosts (FieldName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #UnitCosts ([FieldName],[YearProjection],[CostProjection])
	SELECT #AbsoluteCost.FieldName,#AbsoluteCost.YearProjection,#AbsoluteCost.CostProjection/(SUM(#Production.Amount)*1000)
	FROM	#AbsoluteCost INNER JOIN #Production ON
			#AbsoluteCost.YearProjection = #Production.YearProjection
	GROUP BY #AbsoluteCost.FieldName,#AbsoluteCost.YearProjection,#AbsoluteCost.CostProjection
	END /** Unit Costs **/

	BEGIN /** Size Drivers **/
	CREATE TABLE #SizeDrivers (IdModel INT, IdField INT, IdTechnicalDriver INT, Name VARCHAR(150), YearProjection INT, Normal INT, Pessimistic INT, Optimistic INT) ON [PRIMARY]
	IF (@IdTechnicalDriver = 0) /** All technical drivers **/
	BEGIN	
		INSERT INTO #SizeDrivers([IdModel],[IdField],[IdTechnicalDriver],[Name],[YearProjection],[Normal],[Pessimistic],[Optimistic])
		SELECT  A.IdModel, A.IdField, A.IdTechnicalDriver, A.Name,A.Year, SUM(A.Normal) AS Normal, SUM(A.Pessimistic) AS Pessimistic, SUM(A.Optimistic) AS Optimistic
		FROM
		(		
		SELECT  'OPERATION = 1' AS 'OPERATION',dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField, dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.Name, 
				dbo.tblTechnicalDriverData.Year, SUM(dbo.tblTechnicalDriverData.Normal) AS Normal, SUM(dbo.tblTechnicalDriverData.Pessimistic) AS Pessimistic, SUM(dbo.tblTechnicalDriverData.Optimistic) AS Optimistic
		FROM    dbo.tblProjects INNER JOIN
				dbo.tblTechnicalDriverData ON dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject INNER JOIN
				dbo.tblTechnicalDrivers ON dbo.tblTechnicalDriverData.IdTechnicalDriver = dbo.tblTechnicalDrivers.IdTechnicalDriver
		WHERE	(dbo.tblTechnicalDrivers.TypeDriver=1) and tblProjects.Operation=1
		GROUP BY dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.Name, dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField
		HAVING  (dbo.tblTechnicalDrivers.IdModel = @IdModel) AND (dbo.tblTechnicalDriverData.IdField IN (SELECT IdField FROM #FieldList)) AND 
				(dbo.tblTechnicalDrivers.IdTechnicalDriver IN (SELECT dbo.tblTechnicalDrivers.IdTechnicalDriver FROM dbo.tblTechnicalDrivers WHERE dbo.tblTechnicalDrivers.IdModel = @IdModel))
		UNION
		SELECT	'OPERATION = 2' AS 'OPERATION',dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField, dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.Name, 
				dbo.tblTechnicalDriverData.Year, SUM(dbo.tblTechnicalDriverData.Normal) AS Normal, SUM(dbo.tblTechnicalDriverData.Pessimistic) AS Pessimistic, SUM(dbo.tblTechnicalDriverData.Optimistic) AS Optimistic
		FROM	dbo.tblProjects INNER JOIN
				dbo.tblTechnicalDriverData ON dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject INNER JOIN
				dbo.tblTechnicalDrivers ON dbo.tblTechnicalDriverData.IdTechnicalDriver = dbo.tblTechnicalDrivers.IdTechnicalDriver
		WHERE	(dbo.tblTechnicalDrivers.TypeDriver=1) and tblProjects.Operation=2 AND dbo.tblTechnicalDriverData.Year >= tblProjects.Starts
		GROUP BY dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.Name, dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField
		HAVING  (dbo.tblTechnicalDrivers.IdModel = @IdModel) AND (dbo.tblTechnicalDriverData.IdField IN (SELECT IdField FROM #FieldList)) AND 
				(dbo.tblTechnicalDrivers.IdTechnicalDriver IN (SELECT dbo.tblTechnicalDrivers.IdTechnicalDriver FROM dbo.tblTechnicalDrivers WHERE dbo.tblTechnicalDrivers.IdModel = @IdModel))
		) A
		GROUP BY A.IdModel, A.IdField, A.IdTechnicalDriver, A.Name,A.Year
	END
	ELSE
	BEGIN
		INSERT INTO #SizeDrivers([IdModel],[IdField],[IdTechnicalDriver],[Name],[YearProjection],[Normal],[Pessimistic],[Optimistic])
		SELECT  dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField, dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.Name, 
				dbo.tblTechnicalDriverData.Year, SUM(dbo.tblTechnicalDriverData.Normal) AS Normal, SUM(dbo.tblTechnicalDriverData.Pessimistic) AS Pessimistic, SUM(dbo.tblTechnicalDriverData.Optimistic) AS Optimistic
		FROM    dbo.tblProjects INNER JOIN
				dbo.tblTechnicalDriverData ON dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject INNER JOIN
				dbo.tblTechnicalDrivers ON dbo.tblTechnicalDriverData.IdTechnicalDriver = dbo.tblTechnicalDrivers.IdTechnicalDriver
		WHERE	(dbo.tblTechnicalDrivers.TypeDriver=1)
		GROUP BY dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.Name, dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField
		HAVING  (dbo.tblTechnicalDrivers.IdModel = @IdModel) AND (dbo.tblTechnicalDriverData.IdField IN (SELECT IdField FROM #FieldList)) AND (dbo.tblTechnicalDrivers.IdTechnicalDriver = @IdTechnicalDriver)
	END

	/** Get full list of Size Drivers **/
	CREATE  TABLE #MissingSizeDrivers (IdModel INT, IdField INT, IdTechnicalDriver INT, Name VARCHAR(150), YearProjection INT, Pessimistic INT, Normal INT, Optimistic INT) ON [PRIMARY]		
	INSERT INTO #MissingSizeDrivers ([IdModel],[IdField],[IdTechnicalDriver],[Name],[YearProjection],[Normal],[Pessimistic],[Optimistic])
	SELECT DISTINCT CNList.IdModel, CNList.IdField, CNList.IdTechnicalDriver, CNList.Name, CYList.Year, 0, 0, 0 
	FROM
		(SELECT [IdModel], IdField, IdTechnicalDriver, YearProjection, Name FROM #SizeDrivers WHERE YearProjection IS NOT NULL GROUP BY IdModel, IdField, IdTechnicalDriver, YearProjection, Name) AS CNList CROSS JOIN
		(SELECT [Year] FROM #ProjectionYears) AS CYList

	/** Add Any Missing Year to #SizeDrivers to Fill Properly **/
	INSERT INTO #SizeDrivers([IdModel],[IdField],[IdTechnicalDriver],[Name],[YearProjection],[Normal],[Pessimistic],[Optimistic])
	SELECT msd.[IdModel],msd.[IdField],msd.[IdTechnicalDriver],msd.[Name],msd.[YearProjection],0,0,0
			--ISNULL(sd.[Normal],(SELECT Normal FROM #SizeDrivers sd3 WHERE YearProjection IN (SELECT MAX(YearProjection) FROM #SizeDrivers sd2 WHERE sd2.Name=msd.Name) AND sd3.Name=msd.Name)) AS Normal,
			--ISNULL(sd.[Pessimistic],(SELECT Pessimistic FROM #SizeDrivers sd3 WHERE YearProjection IN (SELECT MAX(YearProjection) FROM #SizeDrivers sd2 WHERE sd2.Name=msd.Name) AND sd3.Name=msd.Name)) AS Pesssimistic,
			--ISNULL(sd.[Optimistic],(SELECT Optimistic FROM #SizeDrivers sd3 WHERE YearProjection IN (SELECT MAX(YearProjection) FROM #SizeDrivers sd2 WHERE sd2.Name=msd.Name) AND sd3.Name=msd.Name)) AS Optimistic
	FROM #SizeDrivers sd FULL JOIN 
		#MissingSizeDrivers msd ON sd.IdModel = msd.IdModel AND sd.IdField=msd.IdField AND sd.IdTechnicalDriver=msd.IdTechnicalDriver AND sd.Name=msd.Name AND sd.YearProjection=msd.YearProjection
	WHERE sd.YearProjection IS NULL
	END /** Size Drivers **/

	BEGIN /** Performance Drivers **/
	CREATE TABLE #PerformanceDrivers (IdModel INT, IdField INT, IdTechnicalDriver INT, TypeDriver INT, Name VARCHAR(150), YearProjection INT, Normal INT, Pessimistic INT, Optimistic INT) ON [PRIMARY]
	IF (@IdTechnicalDriver = 0) /** All technical drivers **/
	BEGIN
		INSERT INTO #PerformanceDrivers ([IdModel],[IdField],[IdTechnicalDriver],[TypeDriver],[Name],[YearProjection],[Pessimistic],[Normal],[Optimistic])
		SELECT  A.IdModel, A.IdField, A.IdTechnicalDriver, A.TypeDriver, A.Name,A.Year, SUM(A.Normal) AS Normal, SUM(A.Pessimistic) AS Pessimistic, SUM(A.Optimistic) AS Optimistic
		FROM
		(		
		SELECT  dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField, dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.TypeDriver, dbo.tblTechnicalDrivers.Name, 
				dbo.tblTechnicalDriverData.Year, SUM(dbo.tblTechnicalDriverData.Normal) AS Normal, SUM(dbo.tblTechnicalDriverData.Pessimistic) AS Pessimistic, SUM(dbo.tblTechnicalDriverData.Optimistic) AS Optimistic
		FROM    dbo.tblProjects INNER JOIN
				dbo.tblTechnicalDriverData ON dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject INNER JOIN
				dbo.tblTechnicalDrivers ON dbo.tblTechnicalDriverData.IdTechnicalDriver = dbo.tblTechnicalDrivers.IdTechnicalDriver
		WHERE	(dbo.tblTechnicalDrivers.TypeDriver=2) and tblProjects.Operation=1
		GROUP BY dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.Name, dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField, dbo.tblTechnicalDrivers.TypeDriver
		HAVING  (dbo.tblTechnicalDrivers.IdModel = @IdModel) AND (dbo.tblTechnicalDriverData.IdField IN (SELECT IdField FROM #FieldList)) AND 
				(dbo.tblTechnicalDrivers.IdTechnicalDriver IN (SELECT dbo.tblTechnicalDrivers.IdTechnicalDriver FROM dbo.tblTechnicalDrivers WHERE dbo.tblTechnicalDrivers.IdModel = @IdModel))
		UNION
		SELECT	dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField, dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.TypeDriver, dbo.tblTechnicalDrivers.Name, 
				dbo.tblTechnicalDriverData.Year, SUM(dbo.tblTechnicalDriverData.Normal) AS Normal, SUM(dbo.tblTechnicalDriverData.Pessimistic) AS Pessimistic, SUM(dbo.tblTechnicalDriverData.Optimistic) AS Optimistic
		FROM	dbo.tblProjects INNER JOIN
				dbo.tblTechnicalDriverData ON dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject INNER JOIN
				dbo.tblTechnicalDrivers ON dbo.tblTechnicalDriverData.IdTechnicalDriver = dbo.tblTechnicalDrivers.IdTechnicalDriver
		WHERE	(dbo.tblTechnicalDrivers.TypeDriver=2) and tblProjects.Operation=2 AND dbo.tblTechnicalDriverData.Year >= tblProjects.Starts
		GROUP BY dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.Name, dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField, dbo.tblTechnicalDrivers.TypeDriver
		HAVING  (dbo.tblTechnicalDrivers.IdModel = @IdModel) AND (dbo.tblTechnicalDriverData.IdField IN (SELECT IdField FROM #FieldList)) AND 
				(dbo.tblTechnicalDrivers.IdTechnicalDriver IN (SELECT dbo.tblTechnicalDrivers.IdTechnicalDriver FROM dbo.tblTechnicalDrivers WHERE dbo.tblTechnicalDrivers.IdModel = @IdModel))
		) A
		GROUP BY A.IdModel, A.IdField, A.IdTechnicalDriver, A.Name, A.Year, A.TypeDriver
	END
	ELSE
	BEGIN
		INSERT INTO #PerformanceDrivers ([IdModel],[IdField],[IdTechnicalDriver],[TypeDriver],[Name],[YearProjection],[Pessimistic],[Normal],[Optimistic])
		SELECT  A.IdModel, A.IdField, A.IdTechnicalDriver, A.TypeDriver, A.Name,A.Year, SUM(A.Normal) AS Normal, SUM(A.Pessimistic) AS Pessimistic, SUM(A.Optimistic) AS Optimistic
		FROM
		(		
		SELECT  dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField, dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.TypeDriver, dbo.tblTechnicalDrivers.Name, 
				dbo.tblTechnicalDriverData.Year, SUM(dbo.tblTechnicalDriverData.Normal) AS Normal, SUM(dbo.tblTechnicalDriverData.Pessimistic) AS Pessimistic, SUM(dbo.tblTechnicalDriverData.Optimistic) AS Optimistic
		FROM    dbo.tblProjects INNER JOIN
				dbo.tblTechnicalDriverData ON dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject INNER JOIN
				dbo.tblTechnicalDrivers ON dbo.tblTechnicalDriverData.IdTechnicalDriver = dbo.tblTechnicalDrivers.IdTechnicalDriver
		WHERE	(dbo.tblTechnicalDrivers.TypeDriver=2) and tblProjects.Operation=1
		GROUP BY dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.Name, dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField, dbo.tblTechnicalDrivers.TypeDriver
		HAVING  (dbo.tblTechnicalDrivers.IdModel = @IdModel) AND (dbo.tblTechnicalDriverData.IdField IN (SELECT IdField FROM #FieldList)) AND 
				(dbo.tblTechnicalDrivers.IdTechnicalDriver = @IdTechnicalDriver)
		UNION
		SELECT	dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField, dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.TypeDriver, dbo.tblTechnicalDrivers.Name, 
				dbo.tblTechnicalDriverData.Year, SUM(dbo.tblTechnicalDriverData.Normal) AS Normal, SUM(dbo.tblTechnicalDriverData.Pessimistic) AS Pessimistic, SUM(dbo.tblTechnicalDriverData.Optimistic) AS Optimistic
		FROM	dbo.tblProjects INNER JOIN
				dbo.tblTechnicalDriverData ON dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject INNER JOIN
				dbo.tblTechnicalDrivers ON dbo.tblTechnicalDriverData.IdTechnicalDriver = dbo.tblTechnicalDrivers.IdTechnicalDriver
		WHERE	(dbo.tblTechnicalDrivers.TypeDriver=2) and tblProjects.Operation=2 AND dbo.tblTechnicalDriverData.Year >= tblProjects.Starts
		GROUP BY dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.Name, dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDriverData.IdField, dbo.tblTechnicalDrivers.TypeDriver
		HAVING  (dbo.tblTechnicalDrivers.IdModel = @IdModel) AND (dbo.tblTechnicalDriverData.IdField IN (SELECT IdField FROM #FieldList)) AND 
				(dbo.tblTechnicalDrivers.IdTechnicalDriver  = @IdTechnicalDriver)
		) A
		GROUP BY A.IdModel, A.IdField, A.IdTechnicalDriver, A.Name, A.Year, A.TypeDriver	
	END
	
	/** Get full list of Performance Drivers **/
	CREATE TABLE #MissingPerformanceDrivers (IdModel INT, IdField INT, IdTechnicalDriver INT, TypeDriver INT, Name VARCHAR(150), YearProjection INT, Normal INT, Pessimistic INT, Optimistic INT) ON [PRIMARY]
	INSERT INTO #MissingPerformanceDrivers ([IdModel],[IdField],[IdTechnicalDriver],[TypeDriver],[Name],[YearProjection],[Normal],[Pessimistic],[Optimistic])
	SELECT DISTINCT CNList.IdModel, CNList.IdField, CNList.IdTechnicalDriver, CNList.TypeDriver, CNList.Name, CYList.Year, 0, 0, 0 
	FROM
		(SELECT [IdModel], IdField, IdTechnicalDriver, TypeDriver, YearProjection, Name FROM #PerformanceDrivers WHERE YearProjection IS NOT NULL GROUP BY IdModel, IdField, IdTechnicalDriver, TypeDriver, YearProjection, Name) AS CNList CROSS JOIN
		(SELECT [Year] FROM #ProjectionYears) AS CYList

	/** Add Any Missing Year to #PerformanceDrivers to Fill Properly **/
	INSERT INTO #PerformanceDrivers ([IdModel],[IdField],[IdTechnicalDriver],[TypeDriver],[Name],[YearProjection],[Normal],[Pessimistic],[Optimistic])
	SELECT mpd.[IdModel],mpd.[IdField],mpd.[IdTechnicalDriver],mpd.[TypeDriver],mpd.[Name],mpd.[YearProjection],
			mpd.[Normal]/*ISNULL(pd.[Normal],(SELECT Normal FROM #PerformanceDrivers pd3 WHERE YearProjection IN (SELECT MAX(YearProjection) FROM #PerformanceDrivers pd2 WHERE pd2.Name=mpd.Name) AND pd3.Name=mpd.Name))*/ AS Normal,
			mpd.[Pessimistic]/*ISNULL(pd.[Pessimistic],(SELECT Pessimistic FROM #PerformanceDrivers pd3 WHERE YearProjection = (SELECT MAX(YearProjection) FROM #PerformanceDrivers pd2 WHERE pd2.Name=mpd.Name) AND pd3.Name=mpd.Name))*/ AS Pesssimistic,
			mpd.[Optimistic]/*ISNULL(pd.[Optimistic],(SELECT Optimistic FROM #PerformanceDrivers pd3 WHERE YearProjection = (SELECT MAX(YearProjection) FROM #PerformanceDrivers pd2 WHERE pd2.Name=mpd.Name) AND pd3.Name=mpd.Name))*/ AS Optimistic
	FROM #PerformanceDrivers pd FULL JOIN 
		#MissingPerformanceDrivers mpd ON pd.IdModel = mpd.IdModel AND pd.IdField=mpd.IdField AND pd.IdTechnicalDriver=mpd.IdTechnicalDriver AND pd.TypeDriver=mpd.TypeDriver AND pd.Name=mpd.Name AND pd.YearProjection=mpd.YearProjection
	WHERE pd.YearProjection IS NULL
	END /** Performance Drivers **/
	
	BEGIN /** Economic Drivers **/
	CREATE TABLE #EconomicDrivers (IdModel INT, IdEconomicDriver INT, YearProjection INT, Name VARCHAR(150), Normal INT, Pessimistic INT, Optimistic INT) ON [PRIMARY]
	IF (@IdEconomicDriver = 0) /** All technical drivers **/
	BEGIN
		INSERT INTO #EconomicDrivers ([IdModel],[IdEconomicDriver],[YearProjection],[Name],[Normal],[Pessimistic],[Optimistic])
		SELECT	dbo.tblEconomicDrivers.IdModel, dbo.tblEconomicDrivers.IdEconomicDriver, dbo.tblEconomicDriverData.Year, dbo.tblEconomicDrivers.Name, dbo.tblEconomicDriverData.Normal, dbo.tblEconomicDriverData.Pessimistic, dbo.tblEconomicDriverData.Optimistic
		FROM	dbo.tblEconomicDriverData INNER JOIN
				dbo.tblEconomicDrivers ON dbo.tblEconomicDriverData.IdEconomicDriver = dbo.tblEconomicDrivers.IdEconomicDriver
		WHERE     (dbo.tblEconomicDrivers.IdModel = @IdModel) AND (dbo.tblEconomicDrivers.IdEconomicDriver IN(SELECT dbo.tblEconomicDrivers.IdEconomicDriver FROM dbo.tblEconomicDrivers WHERE dbo.tblEconomicDrivers.IdModel = @IdModel))
	END
	ELSE
	BEGIN
		INSERT INTO #EconomicDrivers ([IdModel],[IdEconomicDriver],[YearProjection],[Name],[Normal],[Pessimistic],[Optimistic])
		SELECT	dbo.tblEconomicDrivers.IdModel, dbo.tblEconomicDrivers.IdEconomicDriver, dbo.tblEconomicDriverData.Year, dbo.tblEconomicDrivers.Name, dbo.tblEconomicDriverData.Normal, dbo.tblEconomicDriverData.Pessimistic, dbo.tblEconomicDriverData.Optimistic
		FROM	dbo.tblEconomicDriverData INNER JOIN
				dbo.tblEconomicDrivers ON dbo.tblEconomicDriverData.IdEconomicDriver = dbo.tblEconomicDrivers.IdEconomicDriver
		WHERE     (dbo.tblEconomicDrivers.IdModel = @IdModel) AND (dbo.tblEconomicDrivers.IdEconomicDriver = @IdEconomicDriver)
	END

	/** Get full list of Economic Drivers **/
	CREATE TABLE #MissingEconomicDrivers (IdModel INT, IdEconomicDriver INT, YearProjection INT, Name VARCHAR(150), Normal INT, Pessimistic INT, Optimistic INT) ON [PRIMARY]
	INSERT INTO #MissingEconomicDrivers ([IdModel],[IdEconomicDriver],[YearProjection],[Name],[Normal],[Pessimistic],[Optimistic])
	SELECT DISTINCT CNList.IdModel, CNList.IdEconomicDriver, CYList.Year, CNList.Name, 0, 0, 0 
	FROM
		(SELECT [IdModel], IdEconomicDriver, YearProjection, Name FROM #EconomicDrivers WHERE YearProjection IS NOT NULL GROUP BY IdModel, IdEconomicDriver, YearProjection, Name) AS CNList CROSS JOIN
		(SELECT [Year] FROM #ProjectionYears) AS CYList

	/** Add Any Missing Year to #EconomicDrivers to Fill Properly **/
	INSERT INTO #EconomicDrivers ([IdModel],[IdEconomicDriver],[YearProjection],[Name],[Normal],[Pessimistic],[Optimistic])
	SELECT med.[IdModel],med.[IdEconomicDriver],med.[YearProjection],med.[Name],
			med.[Normal]/*ISNULL(ed.[Normal],(SELECT Normal FROM #EconomicDrivers ed3 WHERE YearProjection IN (SELECT MAX(YearProjection) FROM #EconomicDrivers ed2 WHERE ed2.Name=med.Name) AND ed3.Name=med.Name))*/ AS Normal,
			med.[Pessimistic]/*ISNULL(ed.[Pessimistic],(SELECT Pessimistic FROM #EconomicDrivers ed3 WHERE YearProjection IN (SELECT MAX(YearProjection) FROM #EconomicDrivers ed2 WHERE ed2.Name=med.Name) AND ed3.Name=med.Name))*/ AS Pesssimistic,
			med.[Optimistic]/*ISNULL(ed.[Optimistic],(SELECT Optimistic FROM #EconomicDrivers ed3 WHERE YearProjection IN (SELECT MAX(YearProjection) FROM #EconomicDrivers ed2 WHERE ed2.Name=med.Name) AND ed3.Name=med.Name))*/ AS Optimistic
	FROM #EconomicDrivers ed FULL JOIN 
		#MissingEconomicDrivers med ON ed.IdModel = med.IdModel AND ed.IdEconomicDriver=med.IdEconomicDriver and ed.Name=med.Name and ed.YearProjection=med.YearProjection
	WHERE ed.YearProjection IS NULL
	ORDER by 1,2,3
	END /** Economic Drivers **/

	BEGIN /** Add Unit Cost factor, IF NECESSARY **/
	IF @IdCostType = 2
	BEGIN	
		--UPDATE	#Opex
		--SET		#Opex.CostProjection = CASE WHEN #UnitCosts.CostProjection = 0 THEN #Opex.CostProjection * 0 ELSE #Opex.CostProjection/#UnitCosts.CostProjection END
		--FROM	#Opex INNER JOIN #UnitCosts ON
		--		#Opex.YearProjection = #UnitCosts.YearProjection
			UPDATE	#Opex
			SET		#Opex.CostProjection = CASE WHEN BL.Amount = 0 THEN #Opex.CostProjection ELSE #Opex.CostProjection/BL.Amount END
			FROM	#Opex INNER JOIN (SELECT #VolumeReportGrouped.YearProjection, SUM(#VolumeReportGrouped.Amount) AS Amount FROM #VolumeReportGrouped GROUP BY YearProjection) AS BL ON
					#Opex.YearProjection = BL.YearProjection 
		
		
	END
	END /** Add Unit Cost factor, IF NECESSARY **/
	
	BEGIN /** Totals **/
	CREATE TABLE #Totals (Unit NVARCHAR(255), CostCategory NVARCHAR(255), UnitSubname NVARCHAR(255), YearProjection INT, Value FLOAT, SortOrder INT) ON [PRIMARY]
	/** Grand Total **/
	INSERT INTO #Totals ([Unit],[CostCategory],[UnitSubname],[YearProjection],[Value],[SortOrder])
	SELECT 'Grand Totals',NULL,NULL,NULL,NULL,1

	CREATE TABLE #ZiffAccounts (ParentName NVARCHAR(255), CostCategory NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #ZiffAccounts([CostCategory],[Name])
	SELECT [Code],[Name] FROM tblZiffAccounts WHERE IdModel=@IdModel AND Parent IS NULL

	INSERT INTO #Totals ([Unit],[CostCategory],[UnitSubname],[YearProjection],[Value],[SortOrder])
	SELECT '',#Opex.CostCategory,#Opex.Name,#Opex.YearProjection,#Opex.CostProjection,2 
	FROM #Opex WHERE IdRootZiffAccount in (SELECT IdZiffAccount FROM #CostCategoryList)

	INSERT INTO #Totals ([Unit],[CostCategory],[UnitSubname],[YearProjection],[Value],[SortOrder])
	SELECT 'SubTotal','','Total',#Opex.YearProjection,SUM(#Opex.CostProjection),3
	FROM #Opex WHERE IdRootZiffAccount in (SELECT IdZiffAccount FROM #CostCategoryList)
	GROUP BY #Opex.YearProjection

	/** Size Drivers **/
	INSERT INTO #Totals ([Unit],[CostCategory],[UnitSubname],[YearProjection],[Value],[SortOrder])
	SELECT 'Size Drivers',NULL,NULL,NULL,NULL,4

	INSERT INTO #Totals ([Unit],[CostCategory],[UnitSubname],[YearProjection],[Value],[SortOrder])
	SELECT '', '', sd.Name,sd.YearProjection, 
	CASE WHEN @IdTechnicalScenario=1 THEN sd.Normal
			WHEN @IdTechnicalScenario=2 THEN sd.Optimistic
			WHEN @IdTechnicalScenario=3 THEN sd.Pessimistic
	END AS Value, 5
	FROM #SizeDrivers sd
	
	/** Performance Drivers **/
	INSERT INTO #Totals ([Unit],[CostCategory],[UnitSubname],[YearProjection],[Value],[SortOrder])
	SELECT 'Performance Drivers',NULL,NULL,NULL,NULL,6

	INSERT INTO #Totals ([Unit],[CostCategory],[UnitSubname],[YearProjection],[Value],[SortOrder])
	SELECT '', '', pd.Name, pd.YearProjection, 
	CASE WHEN @IdTechnicalScenario=1 THEN pd.Normal/@Factor
			WHEN @IdTechnicalScenario=2 THEN pd.Optimistic
			WHEN @IdTechnicalScenario=3 THEN pd.Pessimistic
	END AS Value, 7
	FROM #PerformanceDrivers pd

	/** Economic Drivers **/
	INSERT INTO #Totals ([Unit],[CostCategory],[UnitSubname],[YearProjection],[Value],[SortOrder])
	SELECT 'Economic Drivers',NULL,NULL,NULL,NULL,8
	
	INSERT INTO #Totals ([Unit],[CostCategory],[UnitSubname],[YearProjection],[Value],[SortOrder])
	SELECT '', '', ed.Name, ed.YearProjection, 
	CASE WHEN @IdEconomicScenario=1 THEN ed.Normal
			WHEN @IdEconomicScenario=2 THEN ed.Optimistic
			WHEN @IdEconomicScenario=3 THEN ed.Pessimistic
	END AS Value, 9
	FROM #EconomicDrivers ed

	DELETE #Totals WHERE YearProjection NOT BETWEEN @StartYear AND @EndYear
	END /** Totals **/

	BEGIN /** Build Output Table **/
	/** Table Data **/
	DECLARE @SQLOpexOperationalMargins NVARCHAR(MAX);
	SET @SQLOpexOperationalMargins = 'SELECT [SortOrder],[Unit],[CostCategory],[UnitSubname],' + @YearColumnList + ' FROM #Totals PIVOT (SUM(Value) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY SortOrder';
	EXEC(@SQLOpexOperationalMargins);

	/** Create Chart Table **/
	CREATE TABLE #ChartTotals (Year INT) ON [PRIMARY]

	DECLARE @DriversCount INT = 0, @CategoriesCount INT = 0, @EconomicDriversCount INT = 0;
	DECLARE @FieldColumnList NVARCHAR(MAX)='';
	DECLARE @CostCategoryName NVARCHAR(255);
	DECLARE @UnitSubname NVARCHAR(255);
	DECLARE @SortOrder INT;
	DECLARE @sqlField nvarchar(4000)
	DECLARE FieldList CURSOR FOR
	SELECT CostCategory,UnitSubname,SortOrder FROM #Totals where SortOrder IN (2,5,7,9) GROUP BY CostCategory,UnitSubname,SortOrder

	OPEN FieldList;
	FETCH NEXT FROM FieldList INTO @CostCategoryName,@UnitSubname,@SortOrder;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @SortOrder = 2 
		BEGIN
			SET @sqlField = 'ALTER TABLE #ChartTotals ADD "' + @UnitSubName + '" float'
			EXEC(@sqlField)
			SET @CategoriesCount = @CategoriesCount + 1
		END
		ELSE
		BEGIN
			SET @sqlField = 'ALTER TABLE #ChartTotals ADD "' + @UnitSubName + '" float'
			EXEC(@sqlField)
			IF @SortOrder = 9
				SET @EconomicDriversCount = @EconomicDriversCount + 1
			ELSE
				SET @DriversCount = @DriversCount + 1
		END		
		
		FETCH NEXT FROM FieldList INTO @CostCategoryName,@UnitSubname,@SortOrder;
	END

	CLOSE FieldList;
	DEALLOCATE FieldList;	
	
	DECLARE @sqlChartTotals nvarchar(4000)
	DECLARE @YearProjection INT
	DECLARE @UnitValue FLOAT
	DECLARE @YearFound BIT

	DECLARE ChartTotals CURSOR FOR
	SELECT CostCategory, UnitSubname, YearProjection, SUM(Value )
	FROM #Totals
	WHERE SortOrder = 2
	GROUP BY CostCategory, UnitSubname, YearProjection

	OPEN ChartTotals;
	FETCH NEXT FROM ChartTotals INTO @CostCategoryName,@UnitSubname,@YearProjection,@UnitValue

	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- Search that the year isn't already in the table otherwise it's an update not an insert
		SELECT @YearFound = COUNT(*) FROM #ChartTotals WHERE Year = @YearProjection
		IF @YearFound = 0
		BEGIN -- Insert 
			SET @sqlChartTotals = 'INSERT INTO #ChartTotals (Year,"' + @UnitSubname + '") VALUES (' + CAST(@YearProjection AS NVARCHAR(30)) + ',' + CAST(@UnitValue AS NVARCHAR(30)) + ')'
			EXEC(@sqlChartTotals)
		END
		ELSE
		BEGIN -- Update
			SET @sqlChartTotals = 'UPDATE #ChartTotals SET "' + @UnitSubname + '" = ' + CAST(@UnitValue AS NVARCHAR(30)) + ' WHERE Year = ' + CAST(@YearProjection AS NVARCHAR(30)) 
			EXEC(@sqlChartTotals)
		END
		
		FETCH NEXT FROM ChartTotals INTO @CostCategoryName,@UnitSubname,@YearProjection,@UnitValue
	END

	CLOSE ChartTotals;
	DEALLOCATE ChartTotals;

	DECLARE ChartTotals CURSOR FOR
	SELECT CostCategory, UnitSubname, YearProjection, SUM(Value )
	FROM #Totals
	WHERE SortOrder in (5,7,9)
	GROUP BY CostCategory, UnitSubname, YearProjection

	OPEN ChartTotals;
	FETCH NEXT FROM ChartTotals INTO @CostCategoryName,@UnitSubname,@YearProjection,@UnitValue

	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- Search that the year isn't already in the table otherwise it's an update not an insert
		SELECT @YearFound = COUNT(*) FROM #ChartTotals WHERE Year = @YearProjection
		IF @YearFound = 0
		BEGIN -- Insert 
			SET @sqlChartTotals = 'INSERT INTO #ChartTotals (Year,"' + @UnitSubname + '") VALUES (' + CAST(@YearProjection AS NVARCHAR(30)) + ',' + CAST(@UnitValue AS NVARCHAR(30)) + ')'
			EXEC(@sqlChartTotals)
		END
		ELSE
		BEGIN -- Update
			SET @sqlChartTotals = 'UPDATE #ChartTotals SET "' + @UnitSubname + '" = ' + CAST(@UnitValue AS NVARCHAR(30)) + ' WHERE Year = ' + CAST(@YearProjection AS NVARCHAR(30)) 
			EXEC(@sqlChartTotals)
		END
		
		FETCH NEXT FROM ChartTotals INTO @CostCategoryName,@UnitSubname,@YearProjection,@UnitValue
	END

	CLOSE ChartTotals;
	DEALLOCATE ChartTotals;

	SELECT * FROM #ChartTotals

	/** Output Stats Table for UI **/ 
	-- (Tables[3])
	SELECT @DriversCount AS DriversCount, @CategoriesCount AS CategoriesCount, @EconomicDriversCount AS EconomicDriversCount;

	SELECT SortOrder, UnitSubname AS 'UnitName' FROM #Totals WHERE SortOrder IN (2) GROUP BY SortOrder, UnitSubname
	UNION
	SELECT SortOrder, UnitSubname AS 'UnitName' FROM #Totals WHERE SortOrder IN (5,7,9) GROUP BY SortOrder, UnitSubname

	END /** Build Output Table **/
	
	BEGIN /** DROP Temporary Tables **/
	DROP TABLE #FieldList
	DROP TABLE #CostCategoryList
	DROP TABLE #VolumeReportBLRows  
	DROP TABLE #VolumeReportBL  
	DROP TABLE #VolumeReportBORows
	DROP TABLE #VolumeReportBO
	DROP TABLE #VolumeReportBLList 
	DROP TABLE #VolumeReportBOList 
	DROP TABLE #VolumeReportGrouped
	DROP TABLE #VolumeReportGroupedBL 
	DROP TABLE #VolumeReportGroupedBO 
	DROP TABLE #ZiffAccounts
	DROP TABLE #PerformanceDrivers
	DROP TABLE #SizeDrivers
	DROP TABLE #Totals
	DROP TABLE #ProjectionYears
	DROP TABLE #EconomicDrivers
	DROP TABLE #MissingEconomicDrivers	
	DROP TABLE #MissingPerformanceDrivers
	DROP TABLE #MissingSizeDrivers	
	DROP TABLE #Opex
	DROP TABLE #ChartTotals
	DROP TABLE #UnitCosts
	DROP TABLE #AbsoluteCost
	DROP TABLE #Production
	DROP TABLE #YearList
	DROP TABLE #FieldsFillers	
	END /** DROP Temporary Tables **/

END

GO

/****** Object:  StoredProcedure [dbo].[reppOpexProjectionMultiFieldsCPM]    Script Date: 06/30/2016 13:26:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[reppOpexProjectionMultiFieldsCPM]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[reppOpexProjectionMultiFieldsCPM]
GO

/****** Object:  StoredProcedure [dbo].[reppOpexProjectionMultiFieldsCPM]    Script Date: 06/30/2016 13:26:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2016-06-30
-- Description:	Module Report Opex Projection for Multi Fields at the CPM level
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
--
-- ===========================================================================================
CREATE PROCEDURE [dbo].[reppOpexProjectionMultiFieldsCPM](
@IdModel NVARCHAR(MAX),
@IdStructure INT,
@From INT,
@To INT,
@IdTechnicalScenario INT,
@IdEconomicScenario INT,
@IdCurrency INT,
@IdTerm INT,
@IdCostType INT,
@IdTypeOperation INT,
@Factor INT,
@CostCategories NVARCHAR(MAX)
)
AS
BEGIN

	DECLARE @IdTechnicalDriver INT = 0
	DECLARE @SQL1 AS NVARCHAR(MAX), @SQL2 AS NVARCHAR(MAX)

	BEGIN /** DROP Temporary Tables **/
	IF OBJECT_ID('tempdb..#ModelList') IS NOT NULL DROP TABLE #ModelList
	IF OBJECT_ID('tempdb..#FieldList') IS NOT NULL DROP TABLE #FieldList
	IF OBJECT_ID('tempdb..#CostCategoriesList') IS NOT NULL DROP TABLE #CostCategoriesList
	IF OBJECT_ID('tempdb..#AccountList') IS NOT NULL DROP TABLE #AccountList
	IF OBJECT_ID('tempdb..#TechDriver') IS NOT NULL DROP TABLE #TechDriver
	IF OBJECT_ID('tempdb..#Opex') IS NOT NULL DROP TABLE #Opex
	IF OBJECT_ID('tempdb..#BL') IS NOT NULL DROP TABLE #BL
	IF OBJECT_ID('tempdb..#BO') IS NOT NULL DROP TABLE #BO
	IF OBJECT_ID('tempdb..#Chart') IS NOT NULL DROP TABLE #Chart
	IF OBJECT_ID('tempdb..#ChartList') IS NOT NULL DROP TABLE #ChartList
	IF OBJECT_ID('tempdb..#VolumeReportBLRows') IS NOT NULL DROP TABLE #VolumeReportBLRows  
	IF OBJECT_ID('tempdb..#VolumeReportBL') IS NOT NULL DROP TABLE #VolumeReportBL  
	IF OBJECT_ID('tempdb..#VolumeReportBORows') IS NOT NULL DROP TABLE #VolumeReportBORows
	IF OBJECT_ID('tempdb..#VolumeReportBO') IS NOT NULL DROP TABLE #VolumeReportBO
	IF OBJECT_ID('tempdb..#VolumeReportBLList') IS NOT NULL DROP TABLE #VolumeReportBLList 
	IF OBJECT_ID('tempdb..#VolumeReportBOList') IS NOT NULL DROP TABLE #VolumeReportBOList 
	IF OBJECT_ID('tempdb..#VolumeReportGrouped') IS NOT NULL DROP TABLE #VolumeReportGrouped
	IF OBJECT_ID('tempdb..#VolumeReportGroupedBL') IS NOT NULL DROP TABLE #VolumeReportGroupedBL 
	IF OBJECT_ID('tempdb..#VolumeReportGroupedBO') IS NOT NULL DROP TABLE #VolumeReportGroupedBO 
	IF OBJECT_ID('tempdb..#Export') IS NOT NULL DROP TABLE #Export
	END /** DROP Temporary Tables **/
	
	BEGIN /** List Creation **/
	DECLARE @StartYear INT = @From;
	DECLARE @EndYear INT = @To;

	BEGIN /** Get Base Year **/
	DECLARE @BaseYear INT, @ParmDefinition NVARCHAR(MAX)
	SET @SQL2 = 'SELECT @retBaseYearOUT = [BaseYear] FROM tblModels WHERE [IdModel] IN (' + @IdModel + ') GROUP BY [BaseYear];'

	SET @ParmDefinition = N'@retBaseYearOUT INT OUTPUT'
	EXEC sp_executesql @Query=@SQL2,@Params=@ParmDefinition,@retBaseYearOUT=@BaseYear OUTPUT
	END /** Get Base Year **/
	
	BEGIN /** Build Year Columns **/
	DECLARE @YearColumnList NVARCHAR(MAX)='';	
	DECLARE @YearFrom INT
	SET @YearFrom=@From	
	WHILE (@YearFrom <= @To)
	BEGIN
		SET @YearColumnList =  @YearColumnList + '[' + CAST(@YearFrom AS VARCHAR(4)) + ']';
		SET @YearFrom = @YearFrom + 1;
		IF (@YearFrom <= @To)
		BEGIN
			SET @YearColumnList = @YearColumnList + ',';
		END
	END
	END /** Build Year Columns **/

	BEGIN /** Build Model List **/
	CREATE TABLE #ModelList (IdModel INT PRIMARY KEY NOT NULL) ON [PRIMARY]
	SET @SQL2 = 'INSERT INTO #ModelList SELECT [IdModel] FROM tblModels WHERE [IdModel] IN (' + @IdModel + ') GROUP BY [IdModel];'
	EXEC(@SQL2)
	END /** Build Model List **/
	
	BEGIN /** Build Field List **/
	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL) ON [PRIMARY]
	SET @SQL1 = 'INSERT INTO #FieldList SELECT [IdField] FROM tblFields WHERE [IdModel] IN (' + @IdModel + ') GROUP BY [IdField];'
	EXEC(@SQL1)

	DECLARE @FieldCount INT;
	SELECT @FieldCount=COUNT([IdField]) FROM #FieldList

	/** Generate Chart Columns from Temp Table **/
	DECLARE @FieldColumnList NVARCHAR(MAX)='';
	DECLARE @FieldName NVARCHAR(255);
	DECLARE @ModelId INT;
	DECLARE FieldList CURSOR FOR
	SELECT IdModel,Name FROM [tblFields] where IdField in (SELECT IdField FROM #FieldList);

	OPEN FieldList;
	FETCH NEXT FROM FieldList INTO @ModelId, @FieldName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @FieldColumnList=''
		BEGIN
			SET @FieldColumnList =  @FieldColumnList + '[' + @FieldName + '_' + CAST(@ModelId AS NVARCHAR(MAX)) + ']';
		END
		ELSE
		BEGIN
			SET @FieldColumnList =  @FieldColumnList + ',[' + @FieldName + '_' + CAST(@ModelId AS NVARCHAR(MAX)) + ']';
		END
		FETCH NEXT FROM FieldList INTO @ModelId, @FieldName;
	END

	CLOSE FieldList;
	DEALLOCATE FieldList;
	END /** Build Field List **/
	
	BEGIN /** Build Cost Categories List **/
	/** Need to check whether there are more than one field being passed in, look for comma in input variable **/
	DECLARE @nonAllCC int 
	SELECT @nonAllCC = CHARINDEX(',', @CostCategories);
	
	CREATE TABLE #CostCategoriesList (IdZiffAccount INT PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF (@nonAllCC = 0) /** Not multiple Cost Categories **/
	BEGIN
		IF (LEN(@CostCategories) = 1) /** 0 SELECTED (ALL) **/
		BEGIN
			INSERT INTO #CostCategoriesList SELECT [IdZiffAccount] FROM tblZiffAccounts WHERE [IdModel] IN (SELECT IdModel FROM #ModelList) AND Parent IS NULL ORDER BY Parent;
		END
		ELSE /** Specific Cost Category Selected **/
		BEGIN
			SET @SQL2 = 'INSERT INTO #CostCategoriesList SELECT [IdZiffAccount] FROM tblZiffAccounts WHERE [IdModel] IN (SELECT IdModel FROM #ModelList) AND [Code] IN (''' + @CostCategories + ''') GROUP BY [IdZiffAccount];'
			EXEC(@SQL2)
		END
	END
	ELSE /** Multiple Cost Categories **/
	BEGIN
		SET @CostCategories = REPLACE(@CostCategories,',',''',''')
		SET @SQL2 = 'INSERT INTO #CostCategoriesList SELECT [IdZiffAccount] FROM tblZiffAccounts WHERE [IdModel] IN (SELECT IdModel FROM #ModelList) AND [Code] IN (''' + @CostCategories + ''') GROUP BY [IdZiffAccount];'
		EXEC(@SQL2)
	END

	DECLARE @CostCategoriesCount INT;
	SELECT @CostCategoriesCount = COUNT([IdZiffAccount]) FROM #CostCategoriesList
	END /** Build Cost Categories List **/
	
	BEGIN /** Build Account List**/
	/** Get Chart Columns to Temp Table **/
	CREATE TABLE #AccountList (ColumnName NVARCHAR(255) PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF @IdStructure=1
	BEGIN
		INSERT INTO #AccountList SELECT [RootZiffAccount] FROM tblCalcProjectionTechnical WHERE IdField IN (SELECT [IdField] FROM #FieldList) AND [IdModel] IN (SELECT IdModel FROM #ModelList) GROUP BY [RootZiffAccount] ORDER BY [RootZiffAccount];
	END
	ELSE IF @IdStructure=2
	BEGIN
		INSERT INTO #AccountList SELECT [Activity] FROM tblCalcProjectionTechnical WHERE IdField IN (SELECT [IdField] FROM #FieldList) AND [IdModel] IN (SELECT IdModel FROM #ModelList) GROUP BY [Activity], [SortOrder] ORDER BY [SortOrder], [Activity];
	END
	
	/** Generate Chart Columns from Temp Table **/
	DECLARE @AccountColumnList NVARCHAR(MAX)='';
	DECLARE @AccountName NVARCHAR(255);
	DECLARE AccountList CURSOR FOR
	SELECT ColumnName FROM #AccountList;

	OPEN AccountList;
	FETCH NEXT FROM AccountList INTO @AccountName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @AccountColumnList=''
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + '[' + @AccountName + ']';
		END
		ELSE
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + ',[' + @AccountName + ']';
		END
		FETCH NEXT FROM AccountList INTO @AccountName;
	END

	CLOSE AccountList;
	DEALLOCATE AccountList;
	END /** Build Account List**/
	
	BEGIN /** Get Currency Factor to use **/
	DECLARE @CurrencyFactor FLOAT;
	SELECT @CurrencyFactor=[Value] FROM tblModelsCurrencies WHERE [IdCurrency]=@IdCurrency AND [IdModel] IN (SELECT IdModel FROM #ModelList);
	END /** Get Currency Factor to use **/
	
	BEGIN /** Build Technical Driver Factors (used for KPI Report) **/
	CREATE TABLE #TechDriver ([Year] INT, [Normal] FLOAT, [Optimistic] FLOAT, [Pessimistic] FLOAT) ON [PRIMARY]
	IF @IdTechnicalDriver=0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, 1 AS [Normal], 1 AS [Optimistic], 1 AS [Pessimistic]
		FROM tblCalcModelYears
		WHERE [IdModel] IN (SELECT IdModel FROM #ModelList) AND [Year] BETWEEN @StartYear AND @EndYear;
	END
	ELSE IF @IdTechnicalDriver>0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, SUM([Normal]) AS [Normal], SUM([Pessimistic]) AS [Pessimistic], SUM([Optimistic]) AS [Optimistic]
		FROM tblTechnicalDriverData
		WHERE [IdTechnicalDriver]=@IdTechnicalDriver AND [IdModel] IN (SELECT IdModel FROM #ModelList) AND IdField IN (SELECT [IdField] FROM #FieldList)
		GROUP BY Year;
	END
	END /** Build Technical Driver Factors (used for KPI Report) **/
	END  /** List Creation **/
	
	BEGIN /** Build Volume Tables **/
	/** Report (Baseline)**/
	CREATE TABLE #VolumeReportBLRows ([IdModel] INT, [ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLRows ([IdModel], [ParentName], [Name], [YearProjection], [Amount])	
	SELECT DISTINCT	dbo.tblTechnicalDriverData.IdModel, N'All Cost Accounts' AS ParentName, dbo.tblFields.Name AS Name, --dbo.tblProjects.Name AS Name, 
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN ISNULL(dbo.tblTechnicalDriverData.Normal,0)
				WHEN @IdTechnicalScenario=2 THEN ISNULL(dbo.tblTechnicalDriverData.Optimistic,0)
				WHEN @IdTechnicalScenario=3 THEN ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0)
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject AND
			dbo.tblTechnicalDriverData.Year >= dbo.tblProjects.Starts INNER JOIN
			dbo.tblFields ON dbo.tblProjects.IdField = dbo.tblFields.IdField
	WHERE	dbo.tblTechnicalDriverData.IdModel IN (SELECT IdModel FROM #ModelList)
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=1
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	ORDER BY dbo.tblTechnicalDriverData.Year

	CREATE TABLE #VolumeReportBL ([IdModel] INT, [ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBL([IdModel], [ParentName], [Name], [YearProjection], [Amount])
	SELECT	IdModel, ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBLRows
	GROUP BY IdModel, ParentName, Name, YearProjection
	ORDER BY IdModel, ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBLList ([IdModel] INT, ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLList ([IdModel], [ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.IdModel, CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [IdModel], [ParentName],[Name] FROM #VolumeReportBL WHERE [ParentName] IS NOT NULL GROUP BY [IdModel], [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel IN (SELECT IdModel FROM #ModelList)) AS CYList 

	/** Add Any Missing Years to #VolumeReportBLList to Fill Properly **/
	INSERT INTO #VolumeReportBL ([IdModel], [ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBLList.IdModel, #VolumeReportBLList.ParentName,#VolumeReportBLList.Name, #VolumeReportBLList.YearProjection,0 AS Amount
	FROM	#VolumeReportBLList LEFT OUTER JOIN
			#VolumeReportBL ON #VolumeReportBLList.ParentName=#VolumeReportBL.ParentName AND #VolumeReportBLList.Name=#VolumeReportBL.Name 
			AND #VolumeReportBLList.YearProjection=#VolumeReportBL.YearProjection AND #VolumeReportBLList.IdModel=#VolumeReportBL.IdModel
	WHERE	#VolumeReportBL.YearProjection IS NULL;

	/** Delete all rows that have 0 (zero) Amount **/
	DELETE #VolumeReportBL WHERE Amount=0

	/** Report (Business Opportunities)**/
	CREATE TABLE #VolumeReportBORows ([IdModel] INT, [ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBORows ([IdModel], [ParentName], [Name], [YearProjection], [Amount])	
	SELECT DISTINCT dbo.tblTechnicalDriverData.IdModel, N'All Cost Accounts' AS ParentName, dbo.tblFields.Name AS Name, --dbo.tblProjects.Name AS Name, 
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN ISNULL(dbo.tblTechnicalDriverData.Normal,0)
				WHEN @IdTechnicalScenario=2 THEN ISNULL(dbo.tblTechnicalDriverData.Optimistic,0)
				WHEN @IdTechnicalScenario=3 THEN ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0)
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject AND
			dbo.tblTechnicalDriverData.Year >= dbo.tblProjects.Starts INNER JOIN
			dbo.tblFields ON dbo.tblProjects.IdField = dbo.tblFields.IdField
	WHERE	dbo.tblTechnicalDriverData.IdModel IN (SELECT IdModel FROM #ModelList)
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=2
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	ORDER BY dbo.tblTechnicalDriverData.Year

	CREATE TABLE #VolumeReportBO ([IdModel] INT, [ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBO([IdModel], [ParentName], [Name], [YearProjection], [Amount])
	SELECT	IdModel, ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBORows
	GROUP BY IdModel, ParentName, Name, YearProjection
	ORDER BY IdModel, ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBOList ([IdModel] INT, ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBOList ([IdModel], [ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.IdModel, CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [IdModel], [ParentName],[Name] FROM #VolumeReportBO WHERE [ParentName] IS NOT NULL GROUP BY [IdModel], [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel IN (SELECT IdModel FROM #ModelList)) AS CYList 

	/** Add Any Missing Years to #VolumeReportBOList to Fill Properly **/
	INSERT INTO #VolumeReportBO ([IdModel], [ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBOList.IdModel, #VolumeReportBOList.ParentName,#VolumeReportBOList.Name, #VolumeReportBOList.YearProjection,0 AS Amount
	FROM	#VolumeReportBOList LEFT OUTER JOIN
			#VolumeReportBO ON #VolumeReportBOList.ParentName=#VolumeReportBO.ParentName AND #VolumeReportBOList.Name=#VolumeReportBO.Name 
			AND #VolumeReportBOList.YearProjection=#VolumeReportBO.YearProjection AND #VolumeReportBOList.IdModel=#VolumeReportBO.IdModel
	WHERE	#VolumeReportBO.YearProjection IS NULL;

	/** Delete all rows that have 0 (zero) Amount **/
	DELETE #VolumeReportBO WHERE Amount=0

	/** Report (All Fields Grouped)**/
	CREATE TABLE #VolumeReportGroupedBL ([IdModel] INT, [YearProjection] INT, [Amount] FLOAT, [ParentName] NVARCHAR(255), [Name] NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBL ([IdModel], [YearProjection], [Amount], [ParentName], [Name])
	SELECT IdModel,YearProjection,SUM(amount) AS Amount,'GroupedBL',Name
	FROM #VolumeReportBL
	GROUP BY IdModel,YearProjection,Name
	ORDER BY IdModel,YearProjection

	CREATE TABLE #VolumeReportGroupedBO ([IdModel] INT, [YearProjection] INT, [Amount] FLOAT, [ParentName] NVARCHAR(255), [Name] NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBO ([IdModel], [YearProjection], [Amount], [ParentName], [Name])
	SELECT IdModel,YearProjection,SUM(amount) AS Amount,'GroupedBO',Name
	FROM #VolumeReportBO
	GROUP BY IdModel,YearProjection,Name
	ORDER BY IdModel,YearProjection

	CREATE TABLE #VolumeReportGrouped ([IdModel] INT, [ParentName] NVARCHAR(255), [Name] NVARCHAR(255),[YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGrouped ([IdModel], [ParentName], [Name], [YearProjection], [Amount])
	SELECT IdModel,N'All Cost Accounts' AS ParentName,Name,YearProjection,SUM(Amount) AS 'Amount'
	FROM
	(
		SELECT 'VolumeReportGroupedBL' AS "ProjectType", IdModel, ParentName, Name, YearProjection,SUM(Amount) AS "Amount"
		FROM #VolumeReportGroupedBL 
		GROUP BY IdModel,ParentName,Name,YearProjection
		UNION
		SELECT 'VolumeReportGroupedBO', IdModel, ParentName, Name, YearProjection, SUM(Amount) AS "Amount"
		FROM #VolumeReportGroupedBO 
		GROUP BY IdModel,ParentName,Name,YearProjection
	) AS VolumeReportGroupedTotals
	GROUP BY IdModel,Name,YearProjection
	ORDER BY IdModel,Name,YearProjection
	END /** Build Volume Tables **/

	BEGIN /** Build Source Data Table - Total (#Opex)**/
	CREATE TABLE #Opex (IdModel INT, ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT, SortOrder INT) ON [PRIMARY]
	INSERT INTO #Opex ([IdModel],[ParentName],[Name],[YearProjection],[CostProjection],[SortOrder])
	SELECT tblCalcProjectionTechnical.IdModel,tblModels.Name AS ParentName,tblCalcProjectionTechnical.Field AS Name,tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, 1 AS SortOrder
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel INNER JOIN
		tblModels ON tblCalcProjectionTechnical.IdModel = tblModels.IdModel
	WHERE tblCalcProjectionTechnical.IdModel IN (SELECT IdModel FROM #ModelList) AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList)
	AND tblCalcProjectionTechnical.IdRootZiffAccount IN (SELECT [IdZiffAccount] FROM #CostCategoriesList) 
	AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	GROUP BY tblCalcProjectionTechnical.IdModel,tblModels.Name,tblCalcProjectionTechnical.Field, tblCalcProjectionTechnical.Year;
	END /** Build Source Data Table - Total (#Opex) **/

	BEGIN /** Build Source Data Table - Chart **/
	CREATE TABLE #Chart (ParentName NVARCHAR(255), IdModel INT, YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #Chart ([ParentName],[IdModel],[YearProjection],[CostProjection])
	SELECT  tblCalcProjectionTechnical.Field + '_' + CAST(tblCalcProjectionTechnical.IdModel AS NVARCHAR(MAX)) AS ParentName, tblCalcProjectionTechnical.IdModel,
		tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdRootZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel IN (SELECT IdModel FROM #ModelList) AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	AND tblCalcProjectionTechnical.IdZiffAccount IN (SELECT [IdZiffAccount] FROM #CostCategoriesList)
	GROUP BY  tblCalcProjectionTechnical.Field, tblCalcProjectionTechnical.IdModel, tblCalcProjectionTechnical.Year;
	
	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #ChartList (ParentName NVARCHAR(255), IdModel INT, YearProjection INT) ON [PRIMARY]
	INSERT INTO #ChartList ([ParentName],[IdModel],[YearProjection])
	SELECT CNList.ParentName, CNList.IdModel, CYList.Year FROM
		(SELECT [ParentName],[IdModel] FROM #Chart GROUP BY [ParentName],[IdModel])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel IN (SELECT IdModel FROM #ModelList)) AS CYList
	WHERE CYList.Year BETWEEN @StartYear AND @EndYear
	
	/** Add Any Missing Years to Make the Chart Fill Properly (eg. Fields with only Cyclical Drivers) **/
	INSERT INTO #Chart ([ParentName],[IdModel],[YearProjection],[CostProjection])
	SELECT #ChartList.ParentName, #ChartList.IdModel, #ChartList.YearProjection, NULL AS CostProjection FROM
		#ChartList LEFT OUTER JOIN
		#Chart ON #ChartList.ParentName=#Chart.ParentName AND #ChartList.YearProjection=#Chart.YearProjection AND #ChartList.IdModel=#Chart.IdModel
	WHERE #Chart.YearProjection IS NULL;	
	END /** Build Source Data Table - Chart **/

	BEGIN /** Build Source Data Table - Export **/
	CREATE TABLE #Export (IdModel INT, ModelName NVARCHAR(255), CostCategory NVARCHAR(255), Name NVARCHAR(255), Field NVARCHAR(255), YearProjection INT, CostProjection FLOAT, SortID NVARCHAR(255),SortOrder INT) ON [PRIMARY]
	INSERT INTO #Export ([IdModel],[ModelName],[CostCategory],[Name],[Field],[YearProjection],[CostProjection],[SortID],[SortOrder])
	SELECT tblCalcProjectionTechnical.IdModel, tblModels.Name, tblCalcProjectionTechnical.CodeZiff AS CostCategory, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
		tblCalcProjectionTechnical.Field AS Field, tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, tblCalcProjectionTechnical.RootCodeZiff + '_' + tblCalcProjectionTechnical.CodeZiff AS SortID, 3 AS SortOrder
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel INNER JOIN
		tblModels ON tblCalcProjectionTechnical.IdModel = tblModels.IdModel
	WHERE tblCalcProjectionTechnical.IdModel IN (SELECT IdModel FROM #ModelList) AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList)
	AND tblCalcProjectionTechnical.IdRootZiffAccount IN (SELECT [IdZiffAccount] FROM #CostCategoriesList)
	GROUP BY tblCalcProjectionTechnical.IdModel,tblModels.Name,tblCalcProjectionTechnical.CodeZiff,tblCalcProjectionTechnical.Field, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END,
			tblCalcProjectionTechnical.Year, tblCalcProjectionTechnical.RootCodeZiff + '_' + tblCalcProjectionTechnical.CodeZiff
	UNION
	SELECT tblCalcProjectionTechnical.IdModel, tblModels.Name, tblCalcProjectionTechnical.RootCodeZiff AS CostCategory, tblCalcProjectionTechnical.RootZiffAccount AS Name, 
		tblCalcProjectionTechnical.Field AS Field, tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, tblCalcProjectionTechnical.RootCodeZiff AS SortID, 3 AS SortOrder
	FROM tblCalcProjectionTechnical  LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel INNER JOIN
		tblProjects ON tblCalcProjectionTechnical.IdProject = tblProjects.IdProject INNER JOIN
		tblModels ON tblCalcProjectionTechnical.IdModel = tblModels.IdModel
	WHERE tblCalcProjectionTechnical.IdModel IN (SELECT IdModel FROM #ModelList) 
	AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) 
	AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	AND tblCalcProjectionTechnical.Operation = 1
	GROUP BY tblCalcProjectionTechnical.IdModel,tblModels.Name,tblCalcProjectionTechnical.RootCodeZiff,tblCalcProjectionTechnical.RootZiffAccount,tblCalcProjectionTechnical.Field,tblCalcProjectionTechnical.Year, 
			tblCalcProjectionTechnical.RootCodeZiff
	UNION
	SELECT tblCalcProjectionTechnical.IdModel, tblModels.Name, tblCalcProjectionTechnical.CodeZiff AS CostCategory, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
		'All' AS Field, tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, tblCalcProjectionTechnical.RootCodeZiff + '_' + tblCalcProjectionTechnical.CodeZiff AS SortID, 2 AS SortOrder
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel INNER JOIN
		tblModels ON tblCalcProjectionTechnical.IdModel = tblModels.IdModel
	WHERE tblCalcProjectionTechnical.IdModel IN (SELECT IdModel FROM #ModelList) AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList)
	AND tblCalcProjectionTechnical.IdRootZiffAccount IN (SELECT [IdZiffAccount] FROM #CostCategoriesList)
	GROUP BY tblCalcProjectionTechnical.IdModel, tblModels.Name, tblCalcProjectionTechnical.CodeZiff, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END,tblCalcProjectionTechnical.Year, 
			tblCalcProjectionTechnical.RootCodeZiff + '_' + tblCalcProjectionTechnical.CodeZiff
	UNION
	SELECT tblCalcProjectionTechnical.IdModel, tblModels.Name, tblCalcProjectionTechnical.RootCodeZiff AS CostCategory, tblCalcProjectionTechnical.RootZiffAccount AS Name, 'All' AS Field, 
		tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, tblCalcProjectionTechnical.RootCodeZiff AS SortID, 2 AS SortOrder
	FROM tblCalcProjectionTechnical  LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel INNER JOIN
		tblProjects ON tblCalcProjectionTechnical.IdProject = tblProjects.IdProject INNER JOIN
		tblModels ON tblCalcProjectionTechnical.IdModel = tblModels.IdModel
	WHERE tblCalcProjectionTechnical.IdModel IN (SELECT IdModel FROM #ModelList) 
	AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) 
	AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	AND tblCalcProjectionTechnical.Operation = 1
	GROUP BY tblCalcProjectionTechnical.IdModel,tblModels.Name,tblCalcProjectionTechnical.RootCodeZiff,tblCalcProjectionTechnical.RootZiffAccount,tblCalcProjectionTechnical.Year, tblCalcProjectionTechnical.RootCodeZiff
	UNION
	SELECT -1, 'All Models', tblCalcProjectionTechnical.CodeZiff AS CostCategory, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
		'All (All Selected Models)' AS Field, tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, tblCalcProjectionTechnical.RootCodeZiff + '_' + tblCalcProjectionTechnical.CodeZiff AS SortID, 1 AS SortOrder
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel INNER JOIN
		tblModels ON tblCalcProjectionTechnical.IdModel = tblModels.IdModel
	WHERE tblCalcProjectionTechnical.IdModel IN (SELECT IdModel FROM #ModelList) AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList)
	AND tblCalcProjectionTechnical.IdRootZiffAccount IN (SELECT [IdZiffAccount] FROM #CostCategoriesList)
	GROUP BY tblCalcProjectionTechnical.CodeZiff, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END,tblCalcProjectionTechnical.Year, 
			tblCalcProjectionTechnical.RootCodeZiff + '_' + tblCalcProjectionTechnical.CodeZiff
	UNION
	SELECT -1, 'All Models', tblCalcProjectionTechnical.RootCodeZiff AS CostCategory, tblCalcProjectionTechnical.RootZiffAccount AS Name, 'All (All Selected Models)' AS Field, tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection, tblCalcProjectionTechnical.RootCodeZiff AS SortID, 1 AS SortOrder
	FROM tblCalcProjectionTechnical  LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel INNER JOIN
		tblProjects ON tblCalcProjectionTechnical.IdProject = tblProjects.IdProject INNER JOIN
		tblModels ON tblCalcProjectionTechnical.IdModel = tblModels.IdModel
	WHERE tblCalcProjectionTechnical.IdModel IN (SELECT IdModel FROM #ModelList) 
	AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) 
	AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	AND tblCalcProjectionTechnical.Operation = 1
	GROUP BY tblCalcProjectionTechnical.RootCodeZiff,tblCalcProjectionTechnical.RootZiffAccount,tblCalcProjectionTechnical.Year, tblCalcProjectionTechnical.RootCodeZiff

	END /** Build Source Data Table - Export **/
		
	BEGIN /** Build BL and BO Tables**/
	IF @IdTechnicalDriver=0
	BEGIN
		/** Build Source Data Table - Baseline and Business Opportunities (No Techical KPI Required) **/
		CREATE TABLE #BL (IdModel INT, ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
		CREATE TABLE #BO (IdModel INT, ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
		IF @FieldCount = 1
		BEGIN
			/** Baseline **/
			INSERT INTO #BL ([IdModel],[ParentName],[Name],[YearProjection],[CostProjection])
			SELECT tblCalcProjectionTechnical.IdModel, N'All Cost Accounts' AS ParentName, 
				CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel IN (SELECT IdModel FROM #ModelList) AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=1 
			AND tblCalcProjectionTechnical.IdRootZiffAccount IN (SELECT [IdZiffAccount] FROM #CostCategoriesList)
			GROUP BY tblCalcProjectionTechnical.IdModel, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END, tblCalcProjectionTechnical.Year;

			/** Business Opportunities **/
			INSERT INTO #BO ([IdModel],[ParentName],[Name],[YearProjection],[CostProjection])
			SELECT tblCalcProjectionTechnical.IdModel, N'All Cost Accounts' AS ParentName, 
				CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel IN (SELECT IdModel FROM #ModelList) AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=2 
			AND tblCalcProjectionTechnical.IdRootZiffAccount IN (SELECT [IdZiffAccount] FROM #CostCategoriesList)
			GROUP BY tblCalcProjectionTechnical.IdModel, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END, tblCalcProjectionTechnical.Year;

		END
		ELSE IF @FieldCount <> 1
		BEGIN
			/** Baseline **/
			INSERT INTO #BL ([IdModel],[ParentName],[Name],[YearProjection],[CostProjection])
			SELECT tblCalcProjectionTechnical.IdModel, N'All Cost Accounts' AS ParentName, Project AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel IN (SELECT IdModel FROM #ModelList) AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=1 
			AND tblCalcProjectionTechnical.IdRootZiffAccount IN (SELECT [IdZiffAccount] FROM #CostCategoriesList)
			GROUP BY tblCalcProjectionTechnical.IdModel, tblCalcProjectionTechnical.Project, tblCalcProjectionTechnical.Year;
			
			/** Business Opportunities **/
			INSERT INTO #BO ([IdModel],[ParentName],[Name],[YearProjection],[CostProjection])
			SELECT tblCalcProjectionTechnical.IdModel, N'All Cost Accounts' AS ParentName, Project Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel IN (SELECT IdModel FROM #ModelList) AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=2 
			AND tblCalcProjectionTechnical.IdRootZiffAccount IN (SELECT [IdZiffAccount] FROM #CostCategoriesList)
			GROUP BY tblCalcProjectionTechnical.IdModel, tblCalcProjectionTechnical.Project, tblCalcProjectionTechnical.Year;
		END
	END
	END /** Build BL and BO Tables**/

	BEGIN /** Add Unit Cost factor, IF NECESSARY **/
	IF @IdCostType = 2
	BEGIN		 
		UPDATE	#Opex
		SET		#Opex.CostProjection = CASE WHEN #VolumeReportGrouped.Amount = 0 THEN #Opex.CostProjection * 0 ELSE #Opex.CostProjection/#VolumeReportGrouped.Amount END
		FROM	#Opex INNER JOIN #VolumeReportGrouped ON
				#Opex.YearProjection = #VolumeReportGrouped.YearProjection AND
				#Opex.Name = #VolumeReportGrouped.Name AND
				#Opex.IdModel = #VolumeReportGrouped.IdModel

		UPDATE	#Chart
		SET		#Chart.CostProjection = CASE WHEN #VolumeReportGrouped.Amount = 0 THEN #Chart.CostProjection * 0 ELSE #Chart.CostProjection/#VolumeReportGrouped.Amount END
		FROM	#Chart INNER JOIN #VolumeReportGrouped ON
				#Chart.YearProjection = #VolumeReportGrouped.YearProjection AND
				#Chart.ParentName = #VolumeReportGrouped.Name AND
				#Chart.IdModel = #VolumeReportGrouped.IdModel
	END

	DECLARE @DisplayedFields INT
	SELECT @DisplayedFields = COUNT(A.Name) 
	FROM 
	(SELECT Name FROM #Opex group by Name) AS A

	IF @IdCostType = 2 -- UNIT COST
	BEGIN
		INSERT INTO #Opex ([IdModel],[ParentName],[Name],[YearProjection],[CostProjection],[SortOrder])
		SELECT 999999,'All Fields', '99', NULL, NULL,2
		
		INSERT INTO #Opex ([IdModel],[ParentName],[Name],[YearProjection],[CostProjection],[SortOrder])
		SELECT 999999,'All Fields', 'Unit Cost Average', YearProjection, SUM(CostProjection)/@DisplayedFields AS CostProjection,3
		FROM #Opex
		GROUP BY YearProjection
	END
	ELSE
	BEGIN
		INSERT INTO #Opex ([IdModel],[ParentName],[Name],[YearProjection],[CostProjection],[SortOrder])
		SELECT 999999,'All Fields', '99', NULL, NULL,2

		INSERT INTO #Opex ([IdModel],[ParentName],[Name],[YearProjection],[CostProjection],[SortOrder])
		SELECT 999999,'All Fields', 'Total Cost', YearProjection, SUM(CostProjection) AS CostProjection,3 
		FROM #Opex
		GROUP BY YearProjection
	END
	END /** Add Unit Cost factor, IF NECESSARY **/
	
	BEGIN /** Build Output Table **/
	DECLARE @SQLOpex NVARCHAR(MAX);
	SET @SQLOpex = 'SELECT [IdModel], [ParentName], [Name],' + @YearColumnList + ' FROM #Opex PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [IdModel], [SortOrder], [ParentName], [Name]';
	EXEC(@SQLOpex);

	DECLARE @SQLOpexChart NVARCHAR(MAX);
	SET @SQLOpexChart = 'SELECT [IdModel], [YearProjection] AS [Year],' + @FieldColumnList + ' FROM #Chart PIVOT (SUM(CostProjection) FOR ParentName IN (' + @FieldColumnList + ')) AS PivotTable ORDER BY [IdModel],[YearProjection]';
	EXEC(@SQLOpexChart);

	DECLARE @SQLExport NVARCHAR(MAX);
	SET @SQLExport = 'SELECT [SortID],[IdModel],[ModelName],[CostCategory],[Name],[Field],' + @YearColumnList + ' FROM #Export PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [IdModel],[SortOrder],[Field],[SortID]';
	EXEC(@SQLExport);

	IF @IdTechnicalDriver=0
	BEGIN
		SELECT @FieldCount AS FieldCount;
	END	
	END /** Build Output Table **/

END

GO

/****** Object:  StoredProcedure [dbo].[reppOpexProjectionChartOnly]    Script Date: 03/23/2016 10:19:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[reppOpexProjectionChartOnly]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[reppOpexProjectionChartOnly]
GO

/****** Object:  StoredProcedure [dbo].[reppOpexProjectionChartOnly]    Script Date: 07/04/2016 08:37:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2016-04-14
-- Description:	Module Report Opex Projection Chart Only to be used by reppCostVsDriversProjectionChartOnly
-- ================================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
--
-- ================================================================================================
CREATE PROCEDURE [dbo].[reppOpexProjectionChartOnly](
@IdModel INT,
@IdAggregationLevel INT,
@IdField NVARCHAR(MAX),
@IdStructure INT,
@From INT,
@To INT,
@IdTechnicalScenario INT,
@IdEconomicScenario INT,
@IdCurrency INT,
@IdTerm INT,
@IdTechnicalDriver INT,
@IdCostType INT,
@IdTypeOperation INT,
@Factor INT
)
AS
BEGIN

	BEGIN /** DROP Temporary Tables **/
	IF OBJECT_ID('tempdb..#FieldList') IS NOT NULL DROP TABLE #FieldList
	IF OBJECT_ID('tempdb..#AccountList') IS NOT NULL DROP TABLE #AccountList
	IF OBJECT_ID('tempdb..#TechDriver') IS NOT NULL DROP TABLE #TechDriver
	IF OBJECT_ID('tempdb..#Opex') IS NOT NULL DROP TABLE #Opex
	IF OBJECT_ID('tempdb..#BL') IS NOT NULL DROP TABLE #BL
	IF OBJECT_ID('tempdb..#BO') IS NOT NULL DROP TABLE #BO
	IF OBJECT_ID('tempdb..#Chart') IS NOT NULL DROP TABLE #Chart
	IF OBJECT_ID('tempdb..#ChartList') IS NOT NULL DROP TABLE #ChartList
	IF OBJECT_ID('tempdb..#VolumeReportBLRows') IS NOT NULL DROP TABLE #VolumeReportBLRows  
	IF OBJECT_ID('tempdb..#VolumeReportBL') IS NOT NULL DROP TABLE #VolumeReportBL  
	IF OBJECT_ID('tempdb..#VolumeReportBORows') IS NOT NULL DROP TABLE #VolumeReportBORows
	IF OBJECT_ID('tempdb..#VolumeReportBO') IS NOT NULL DROP TABLE #VolumeReportBO
	IF OBJECT_ID('tempdb..#VolumeReportBLList') IS NOT NULL DROP TABLE #VolumeReportBLList 
	IF OBJECT_ID('tempdb..#VolumeReportBOList') IS NOT NULL DROP TABLE #VolumeReportBOList 
	IF OBJECT_ID('tempdb..#VolumeReportGrouped') IS NOT NULL DROP TABLE #VolumeReportGrouped
	IF OBJECT_ID('tempdb..#VolumeReportGroupedBL') IS NOT NULL DROP TABLE #VolumeReportGroupedBL 
	IF OBJECT_ID('tempdb..#VolumeReportGroupedBO') IS NOT NULL DROP TABLE #VolumeReportGroupedBO 
	IF OBJECT_ID('tempdb..#ChartCost') IS NOT NULL DROP TABLE #ChartCost
	IF OBJECT_ID('tempdb..#ChartListCost') IS NOT NULL DROP TABLE #ChartListCost
	IF OBJECT_ID('tempdb..#ChartVolume') IS NOT NULL DROP TABLE #ChartVolume
	IF OBJECT_ID('tempdb..#ChartListVolume') IS NOT NULL DROP TABLE #ChartListVolume
	IF OBJECT_ID('tempdb..#ExportOpex') IS NOT NULL DROP TABLE #ExportOpex
	IF OBJECT_ID('tempdb..#ExportBL') IS NOT NULL DROP TABLE #ExportBL
	IF OBJECT_ID('tempdb..#ExportBO') IS NOT NULL DROP TABLE #ExportBO	
	END /** DROP Temporary Tables **/
	
	BEGIN /** Build List Tables for reports **/
	/** Technical Scenario List **/
	DECLARE @CaseTypeList NVARCHAR(MAX) = 'Baseline, Incremental' 

	DECLARE @StartYear INT = @From;
	DECLARE @EndYear INT = @To;
	
	/** Get Base Year **/
	DECLARE @BaseYear INT
	SELECT @BaseYear = [BaseYear] FROM [tblModels] WHERE IdModel=@IdModel

	/** Build Year Columns **/
	DECLARE @YearColumnList NVARCHAR(MAX)='';	
	DECLARE @YearFrom INT
	SET @YearFrom=@From	
	WHILE (@YearFrom <= @To)
	BEGIN
		SET @YearColumnList =  @YearColumnList + '[' + CAST(@YearFrom AS VARCHAR(4)) + ']';
		SET @YearFrom = @YearFrom + 1;
		IF (@YearFrom <= @To)
		BEGIN
			SET @YearColumnList = @YearColumnList + ',';
		END
	END
	
	/** Build Field List **/
	/** Need to check whether there are more than one field being passed in, look for comma in input variable **/
	DECLARE @nonAll int 
	SELECT @nonAll = CHARINDEX(',', @IdField);
	DECLARE @SQL1 AS NVARCHAR(MAX)

	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF (@nonAll = 0) /** Not multiple fields **/
	BEGIN	
		IF (@IdAggregationLevel=0 AND cast(@IdField AS INT)=0) /** Single field and All was selected, Aggregation is All **/
		BEGIN
			INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdModel]=@IdModel GROUP BY [IdField];
		END
		ELSE IF (@IdAggregationLevel>0 AND cast(@IdField AS INT)>0) /** Single fields and specifc selected, Aggregation specific **/
		BEGIN
			INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
		END
		ELSE IF (@IdAggregationLevel>0) /** Specific Aggregation with All fields **/
		BEGIN
			INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdModel]=@IdModel GROUP BY [IdField];
		END
		ELSE IF (cast(@IdField AS INT)>0)
		BEGIN
			INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
		END
	END
	ELSE /** Multiple fields **/
	BEGIN
		IF (@IdAggregationLevel=0) /** Multiple fields and Aggregation All was selected **/
		BEGIN
			SET @SQL1 = 'INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdModel]= ' + cast(@IdModel AS VARCHAR(10)) + ' AND [IdField] IN (' + @IdField + ') GROUP BY [IdField];'
		END
		ELSE IF (@IdAggregationLevel>0) /** Multiple fields and specifc Aggregation was selected **/
		BEGIN
			SET @SQL1 = 'INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]= ' + cast(@IdAggregationLevel AS VARCHAR(10)) + ' AND [IdField] IN (' + @IdField + ') AND [IdModel]=' + cast(@IdModel AS VARCHAR(10)) + ' GROUP BY [IdField];'
		END
		EXEC(@SQL1)
	END

	DECLARE @FieldCount INT;
	SELECT @FieldCount=COUNT([IdField]) FROM #FieldList
	
	/** Get Chart Columns to Temp Table **/
	CREATE TABLE #AccountList (ColumnName NVARCHAR(255) PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF @IdStructure=1
	BEGIN
		INSERT INTO #AccountList SELECT [RootZiffAccount] FROM tblCalcProjectionTechnical WHERE IdField IN (SELECT [IdField] FROM #FieldList) AND [IdModel]=@IdModel GROUP BY [RootZiffAccount] ORDER BY [RootZiffAccount];
	END
	ELSE IF @IdStructure=2
	BEGIN
		INSERT INTO #AccountList SELECT [Activity] FROM tblCalcProjectionTechnical WHERE IdField IN (SELECT [IdField] FROM #FieldList) AND [IdModel]=@IdModel GROUP BY [Activity], [SortOrder] ORDER BY [SortOrder], [Activity];
	END
	
	/** Generate Chart Columns from Temp Table **/
	DECLARE @AccountColumnList NVARCHAR(MAX)='';
	DECLARE @AccountName NVARCHAR(255);
	DECLARE AccountList CURSOR FOR
	SELECT ColumnName FROM #AccountList;

	OPEN AccountList;
	FETCH NEXT FROM AccountList INTO @AccountName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @AccountColumnList=''
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + '[' + @AccountName + ']';
		END
		ELSE
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + ',[' + @AccountName + ']';
		END
		FETCH NEXT FROM AccountList INTO @AccountName;
	END

	CLOSE AccountList;
	DEALLOCATE AccountList;
	
	/** Get Currency Factor to use **/
	DECLARE @CurrencyFactor FLOAT;
	SELECT @CurrencyFactor=[Value] FROM tblModelsCurrencies WHERE [IdCurrency]=@IdCurrency AND [IdModel]=@IdModel;
	
	/** Build Technical Driver Factors (used for KPI Report) **/
	CREATE TABLE #TechDriver ([Year] INT, [Normal] FLOAT, [Optimistic] FLOAT, [Pessimistic] FLOAT) ON [PRIMARY]
	IF @IdTechnicalDriver=0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, 1 AS [Normal], 1 AS [Optimistic], 1 AS [Pessimistic]
		FROM tblCalcModelYears
		WHERE [IdModel]=@IdModel AND [Year] BETWEEN @StartYear AND @EndYear;
	END
	ELSE IF @IdTechnicalDriver>0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, SUM([Normal]) AS [Normal], SUM([Pessimistic]) AS [Pessimistic], SUM([Optimistic]) AS [Optimistic]
		FROM tblTechnicalDriverData
		WHERE [IdTechnicalDriver]=@IdTechnicalDriver AND [IdModel]=@IdModel AND IdField IN (SELECT [IdField] FROM #FieldList)
		GROUP BY Year;
	END
	END /** Build List Tables for reports **/
		
	BEGIN /** Build Volume Tables **/
	/** Report (Baseline)**/
	CREATE TABLE #VolumeReportBLRows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLRows ([ParentName], [Name], [YearProjection], [Amount])	
	SELECT	DISTINCT N'All Cost Accounts' AS ParentName, dbo.tblTechnicalDrivers.Name AS Name,
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Normal,0))
				WHEN @IdTechnicalScenario=2 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Optimistic,0))
				WHEN @IdTechnicalScenario=3 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0))
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject AND
			dbo.tblTechnicalDriverData.Year >= dbo.tblProjects.Starts
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=1
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	GROUP BY  dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.Name
	ORDER BY dbo.tblTechnicalDriverData.Year
	
	CREATE TABLE #VolumeReportBL ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBL([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBLRows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBLList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBL WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #VolumeReportBLList to Fill Properly **/
	INSERT INTO #VolumeReportBL ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBLList.ParentName,#VolumeReportBLList.Name, #VolumeReportBLList.YearProjection,0 AS Amount
	FROM	#VolumeReportBLList LEFT OUTER JOIN
			#VolumeReportBL ON #VolumeReportBLList.ParentName=#VolumeReportBL.ParentName AND #VolumeReportBLList.Name=#VolumeReportBL.Name AND #VolumeReportBLList.YearProjection=#VolumeReportBL.YearProjection
	WHERE	#VolumeReportBL.YearProjection IS NULL;

	/** Delete all rows that have 0 (zero) Amount **/
	DELETE #VolumeReportBL WHERE Amount=0

	/** Report (Business Opportunities)**/
	CREATE TABLE #VolumeReportBORows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBORows ([ParentName], [Name], [YearProjection], [Amount])	
	SELECT	DISTINCT N'All Cost Accounts' AS ParentName, dbo.tblTechnicalDrivers.Name AS Name,
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Normal,0))
				WHEN @IdTechnicalScenario=2 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Optimistic,0))
				WHEN @IdTechnicalScenario=3 THEN SUM(ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0))
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject AND
			dbo.tblTechnicalDriverData.Year >= dbo.tblProjects.Starts
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=2
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	GROUP BY  dbo.tblTechnicalDriverData.Year, dbo.tblTechnicalDrivers.Name
	ORDER BY dbo.tblTechnicalDriverData.Year

	CREATE TABLE #VolumeReportBO ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBO([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBORows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBOList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBOList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBO WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #VolumeReportBOList to Fill Properly **/
	INSERT INTO #VolumeReportBO ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBOList.ParentName,#VolumeReportBOList.Name, #VolumeReportBOList.YearProjection,0 AS Amount
	FROM	#VolumeReportBOList LEFT OUTER JOIN
			#VolumeReportBO ON #VolumeReportBOList.ParentName=#VolumeReportBO.ParentName AND #VolumeReportBOList.Name=#VolumeReportBO.Name AND #VolumeReportBOList.YearProjection=#VolumeReportBO.YearProjection
	WHERE	#VolumeReportBO.YearProjection IS NULL;

	/** Delete all rows that have 0 (zero) Amount **/
	DELETE #VolumeReportBO WHERE Amount=0

	/** Report (All Fields Grouped)**/
	CREATE TABLE #VolumeReportGroupedBL ([YearProjection] INT, [Amount] FLOAT, [ParentName] NVARCHAR(255), [Name] NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBL ([YearProjection], [Amount], [ParentName], [Name])
	SELECT YearProjection,SUM(amount) AS Amount,'GroupedBL',Name-- 'Baseline'
	FROM #VolumeReportBL
	GROUP BY YearProjection,Name
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGroupedBO ([YearProjection] INT, [Amount] FLOAT, [ParentName] NVARCHAR(255), [Name] NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBO ([YearProjection], [Amount], [ParentName], [Name])
	SELECT YearProjection,SUM(amount) AS Amount,'GroupedBO',Name-- 'Incremental'
	FROM #VolumeReportBO
	GROUP BY YearProjection,Name
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGrouped ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255),[YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGrouped ([ParentName], [Name], [YearProjection], [Amount])
	SELECT N'All Cost Accounts' AS ParentName,Name,YearProjection,SUM(Amount) AS 'Amount'
	FROM
	(
		SELECT 'VolumeReportGroupedBL' AS "ProjectType", ParentName, Name, YearProjection,SUM(Amount) AS "Amount"
		FROM #VolumeReportGroupedBL 
		GROUP BY ParentName,Name,YearProjection
		UNION
		SELECT 'VolumeReportGroupedBO', ParentName, Name, YearProjection, SUM(Amount) AS "Amount"
		FROM #VolumeReportGroupedBO 
		GROUP BY ParentName,Name,YearProjection
	) AS VolumeReportGroupedTotals
	GROUP BY Name,YearProjection
	ORDER BY Name,YearProjection
	END /** Build Volume Tables **/
	
	DECLARE @TotalCapacityDriver INT = 0;

	/** Get grouped driver value for Base Year **/
	SELECT @TotalCapacityDriver = Amount FROM #VolumeReportGrouped WHERE YearProjection=@BaseYear

	BEGIN /** Build Source Data Table - Total **/
	CREATE TABLE #Opex (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #Opex ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT 'All Cost Categories' AS ParentName, 
		CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS Name, 
		tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList)
	GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, tblCalcProjectionTechnical.Year;
	END /** Build Source Data Table - Total **/
	
	BEGIN /** Build Source Data Table - Chart **/
	CREATE TABLE #Chart (ParentName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #Chart ([ParentName],[YearProjection],[CostProjection])
	SELECT CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS ParentName, 
		tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
			* tblCalcProjectionTechnical.CaseSelection
		) AS CostProjection
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, tblCalcProjectionTechnical.Year;
	
	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #ChartList (ParentName NVARCHAR(255), YearProjection INT) ON [PRIMARY]
	INSERT INTO #ChartList ([ParentName],[YearProjection])
	SELECT CNList.ParentName, CYList.Year FROM
		(SELECT [ParentName] FROM #Chart GROUP BY [ParentName])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList
	WHERE CYList.Year BETWEEN @StartYear AND @EndYear
	
	/** Add Any Missing Years to Make the Chart Fill Properly (eg. Fields with only Cyclical Drivers) **/
	INSERT INTO #Chart ([ParentName],[YearProjection],[CostProjection])
	SELECT #ChartList.ParentName, #ChartList.YearProjection, NULL AS CostProjection FROM
		#ChartList LEFT OUTER JOIN
		#Chart ON #ChartList.ParentName=#Chart.ParentName AND #ChartList.YearProjection=#Chart.YearProjection
	WHERE #Chart.YearProjection IS NULL;	
	END /** Build Source Data Table - Chart **/
		
	BEGIN /** Build BL and BO Tables**/
	IF @IdTechnicalDriver=0
	BEGIN
		/** Build Source Data Table - Baseline and Business Opportunities (No Techical KPI Required) **/
		CREATE TABLE #BL (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
		CREATE TABLE #BO (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
		IF @FieldCount = 1
		BEGIN
			/** Baseline **/
			INSERT INTO #BL ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=1 AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, tblCalcProjectionTechnical.Year;
			
			/** Business Opportunities **/
			INSERT INTO #BO ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=2 AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, tblCalcProjectionTechnical.Year;
		END
		ELSE IF @FieldCount <> 1
		BEGIN
			/** Baseline **/
			INSERT INTO #BL ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, --Project AS Name, 
				CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=1 AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END/**tblCalcProjectionTechnical.Project**/, tblCalcProjectionTechnical.Year;
			
			/** Business Opportunities **/
			INSERT INTO #BO ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, --Project AS Name, 
				CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS Name,
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=2 AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END/**tblCalcProjectionTechnical.Project**/, tblCalcProjectionTechnical.Year;
		END
	END
	END /** Build BL and BO Tables**/
	
	BEGIN /** Build Source Data Table - Chart Cost **/
	CREATE TABLE #ChartCost (ParentName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	/** Add Baseline Totals **/
	INSERT INTO #ChartCost ([ParentName],[YearProjection],[CostProjection])
	SELECT 'Baseline', YearProjection, SUM(CostProjection)
	FROM #BL
	GROUP BY YearProjection
	/** Add Incremental Totals **/
	INSERT INTO #ChartCost ([ParentName],[YearProjection],[CostProjection])
	SELECT 'Incremental', YearProjection, SUM(CostProjection)
	FROM #BO
	GROUP BY YearProjection
 
	/** Get Full List of Years and Names **/
	CREATE TABLE #ChartListCost (ParentName NVARCHAR(255), YearProjection INT) ON [PRIMARY]
	INSERT INTO #ChartListCost ([ParentName],[YearProjection])
	SELECT CNList.ParentName, CYList.Year FROM
		(SELECT [ParentName] FROM #ChartCost GROUP BY [ParentName])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList
	WHERE CYList.Year BETWEEN @StartYear AND @EndYear

	/** Add Any Missing Years to Make the Chart Fill Properly **/
	INSERT INTO #ChartCost ([ParentName],[YearProjection],[CostProjection])
	SELECT #ChartListCost.ParentName, #ChartListCost.YearProjection, 0 AS CostProjection 
	FROM #ChartListCost LEFT OUTER JOIN
		#ChartCost ON #ChartListCost.ParentName=#ChartCost.ParentName AND #ChartListCost.YearProjection=#ChartCost.YearProjection
	WHERE #ChartCost.YearProjection IS NULL;	
	END /** Build Source Data Table - Chart Cost **/
	
	BEGIN /** Build Source Data Table - Chart Volme **/
	CREATE TABLE #ChartVolume (ParentName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	/** Add Baseline Totals **/
	INSERT INTO #ChartVolume ([ParentName],[YearProjection],[CostProjection])
	SELECT 'Baseline', YearProjection, Amount
	FROM #VolumeReportGroupedBL
	/** Add Incremental Totals **/
	INSERT INTO #ChartVolume ([ParentName],[YearProjection],[CostProjection])
	SELECT 'Incremental', YearProjection, Amount
	FROM #VolumeReportGroupedBO

	/** Get Full List of Years and Names **/
	CREATE TABLE #ChartListVolume (ParentName NVARCHAR(255), YearProjection INT) ON [PRIMARY]
	INSERT INTO #ChartListVolume ([ParentName],[YearProjection])
	SELECT CNList.ParentName, CYList.Year FROM
		(SELECT [ParentName] FROM #ChartVolume GROUP BY [ParentName])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList
	WHERE CYList.Year BETWEEN @StartYear AND @EndYear

	/** Add Any Missing Years to Make the Chart Fill Properly **/
	INSERT INTO #ChartVolume ([ParentName],[YearProjection],[CostProjection])
	SELECT #ChartListVolume.ParentName, #ChartListVolume.YearProjection, 0 AS CostProjection 
	FROM #ChartListVolume LEFT OUTER JOIN
		#ChartVolume ON #ChartListVolume.ParentName=#ChartVolume.ParentName AND #ChartListVolume.YearProjection=#ChartVolume.YearProjection
	WHERE #ChartVolume.YearProjection IS NULL;	
	END /** Build Source Data Table - Chart Volme **/
	
	BEGIN /** Build Export Data Table - Total **/
	CREATE TABLE #ExportOpex (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #ExportOpex ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS ParentName, 
		CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
		tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
		) AS CostProjection
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END, tblCalcProjectionTechnical.Year;
	END /** Build Export Data Table - Total **/
	
	BEGIN /** Build Export Baseline and Incremental **/
	IF @IdTechnicalDriver=0
	BEGIN
		/** Build Source Data Table - Baseline and Business Opportunities (No Techical KPI Required) **/
		CREATE TABLE #ExportBL (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
		CREATE TABLE #ExportBO (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
		IF @FieldCount = 1
		BEGIN
			/** Baseline **/
			INSERT INTO #ExportBL ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS ParentName, 
				CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=1 AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END, tblCalcProjectionTechnical.Year;
			
			/** Business Opportunities **/
			INSERT INTO #ExportBO ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT  CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS ParentName, 
				CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=2 AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END, tblCalcProjectionTechnical.Year;
		END
		ELSE IF @FieldCount <> 1
		BEGIN
			/** Baseline **/
			INSERT INTO #ExportBL ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT  CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS ParentName, 
				CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=1 AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END, tblCalcProjectionTechnical.Year;
			
			/** Business Opportunities **/
			INSERT INTO #ExportBO ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT  CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS ParentName, 
				CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
					* tblCalcProjectionTechnical.CaseSelection
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=2 AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END, tblCalcProjectionTechnical.Year;
		END
	END
	END /** Build Export Baseline and Incremental **/
	
	BEGIN /** Add Unit Cost factor, IF NECESSARY **/
	IF @IdCostType = 2
	BEGIN		 
		IF @nonAll <>0
		BEGIN		
			UPDATE	#BL
			SET		#BL.CostProjection = CASE WHEN BL.Amount = 0 THEN #BL.CostProjection ELSE #BL.CostProjection/BL.Amount END
			FROM	#BL INNER JOIN (SELECT #VolumeReportBL.YearProjection, SUM(#VolumeReportBL.Amount) AS Amount FROM #VolumeReportBL GROUP BY YearProjection) AS BL ON
					#BL.YearProjection = BL.YearProjection 

			UPDATE	#BO
			SET		#BO.CostProjection = CASE WHEN BO.Amount = 0 THEN #BO.CostProjection ELSE #BO.CostProjection/BO.Amount END
			FROM	#BO INNER JOIN (SELECT #VolumeReportBO.YearProjection, SUM(#VolumeReportBO.Amount) AS Amount FROM #VolumeReportBO GROUP BY YearProjection) AS BO ON
					#BO.YearProjection = BO.YearProjection 

			UPDATE	#ExportBL
			SET		#ExportBL.CostProjection = CASE WHEN BL.Amount = 0 THEN #ExportBL.CostProjection ELSE #ExportBL.CostProjection/BL.Amount END
			FROM	#ExportBL INNER JOIN (SELECT #VolumeReportBL.YearProjection, SUM(#VolumeReportBL.Amount) AS Amount FROM #VolumeReportBL GROUP BY YearProjection) AS BL ON
					#ExportBL.YearProjection = BL.YearProjection 

			UPDATE	#ExportBO
			SET		#ExportBO.CostProjection = CASE WHEN BO.Amount = 0 THEN #ExportBO.CostProjection ELSE #ExportBO.CostProjection/BO.Amount END
			FROM	#ExportBO INNER JOIN (SELECT #VolumeReportBO.YearProjection, SUM(#VolumeReportBO.Amount) AS Amount FROM #VolumeReportBO GROUP BY YearProjection) AS BO ON
					#ExportBO.YearProjection = #ExportBO.YearProjection 

		END
		ELSE
		BEGIN
			UPDATE	#BL
			SET		#BL.CostProjection = CASE WHEN #VolumeReportBL.Amount = 0 THEN #BL.CostProjection ELSE #BL.CostProjection/#VolumeReportBL.Amount END
			FROM	#BL INNER JOIN #VolumeReportBL ON
					#BL.YearProjection = #VolumeReportBL.YearProjection AND #BL.Name=#VolumeReportBL.Name

			UPDATE	#BO
			SET		#BO.CostProjection = CASE WHEN #VolumeReportBO.Amount = 0 THEN #BO.CostProjection ELSE #BO.CostProjection/#VolumeReportBO.Amount END
			FROM	#BO INNER JOIN #VolumeReportBO ON
					#BO.YearProjection = #VolumeReportBO.YearProjection AND #BO.Name=#VolumeReportBO.Name

			UPDATE	#ExportBL
			SET		#ExportBL.CostProjection = CASE WHEN #VolumeReportBL.Amount = 0 THEN #ExportBL.CostProjection ELSE #ExportBL.CostProjection/#VolumeReportBL.Amount END
			FROM	#ExportBL INNER JOIN #VolumeReportBL ON
					#ExportBL.YearProjection = #VolumeReportBL.YearProjection AND #ExportBL.Name=#VolumeReportBL.Name

			UPDATE	#ExportBO
			SET		#ExportBO.CostProjection = CASE WHEN #VolumeReportBO.Amount = 0 THEN #ExportBO.CostProjection ELSE #ExportBO.CostProjection/#VolumeReportBO.Amount END
			FROM	#ExportBO INNER JOIN #VolumeReportBO ON
					#ExportBO.YearProjection = #VolumeReportBO.YearProjection AND #ExportBO.Name=#VolumeReportBO.Name
		END
		UPDATE	#Opex
		SET		#Opex.CostProjection = CASE WHEN #VolumeReportGrouped.Amount = 0 THEN #Opex.CostProjection * 0 ELSE #Opex.CostProjection/#VolumeReportGrouped.Amount END
		FROM	#Opex INNER JOIN #VolumeReportGrouped ON
				#Opex.YearProjection = #VolumeReportGrouped.YearProjection

		UPDATE	#Chart
		SET		#Chart.CostProjection = CASE WHEN #VolumeReportGrouped.Amount = 0 THEN #Chart.CostProjection * 0 ELSE #Chart.CostProjection/#VolumeReportGrouped.Amount END
		FROM	#Chart INNER JOIN #VolumeReportGrouped ON
				#Chart.YearProjection = #VolumeReportGrouped.YearProjection

		UPDATE	#ChartCost
		SET		#ChartCost.CostProjection = CASE WHEN #VolumeReportGroupedBL.Amount = 0 THEN #ChartCost.CostProjection * 0 ELSE #ChartCost.CostProjection/#VolumeReportGroupedBL.Amount END
		FROM	#ChartCost INNER JOIN #VolumeReportGroupedBL ON
				#ChartCost.YearProjection = #VolumeReportGroupedBL.YearProjection

		UPDATE	#ExportOpex
		SET		#ExportOpex.CostProjection = CASE WHEN #VolumeReportGrouped.Amount = 0 THEN #ExportOpex.CostProjection * 0 ELSE #ExportOpex.CostProjection/#VolumeReportGrouped.Amount END
		FROM	#ExportOpex INNER JOIN #VolumeReportGrouped ON
				#ExportOpex.YearProjection = #VolumeReportGrouped.YearProjection
	END
	END /** Add Unit Cost factor, IF NECESSARY **/
	
	BEGIN /** Build Output Table **/
	DECLARE @SQLOpexChart NVARCHAR(MAX);
	SET @SQLOpexChart = 'SELECT [YearProjection] AS [Year],' + @AccountColumnList + ' FROM #Chart PIVOT (SUM(CostProjection) FOR ParentName IN (' + @AccountColumnList + ')) AS PivotTable ORDER BY [YearProjection]';
	EXEC(@SQLOpexChart);
	END /** Build Output Table **/
	
END

GO


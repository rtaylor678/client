USE [DBCPM]
GO
/****** Object:  StoredProcedure [dbo].[calcModuleTechnical]    Script Date: 10/02/2015 14:06:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Gareth Slater
-- Create date: 2014-04-03
-- Description:	Module Technical Calc
-- ================================================================================================ 
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 2015-08-04	RM		Added a new code to poulate table tblCalcBaselineUnitCostDenominatorSumAmount
-- 2015-08-15	GS		Updated to work with tblCalcCostStructureAssumptionsByField and Handling Semi-Variable Assumptions
-- 2015-10-02	RM		Added a cleanup at end of procedure to remove data from calc tables as data
--						is not needed anywhere else
-- ================================================================================================	
ALTER PROCEDURE [dbo].[calcModuleTechnical](
@IdModel int
)
AS
BEGIN
	
	DECLARE @intMinYear INT;
	DECLARE @intMaxYear INT;
	SELECT @intMinYear=BaseYear, @intMaxYear=BaseYear+Projection FROM tblModels WHERE [IdModel]=@IdModel;

	DECLARE @intLoop int = 0;
	DECLARE @intMaxLevel int = 0;
	SELECT @intMaxLevel = MAX([RelationLevel])+1 FROM tblZiffAccounts WHERE [IdModel]=@IdModel;
	
	/** Precalc **/
	EXEC calcModuleTechnicalPreCalc @IdModel;
	
	/** Cleanup Calc Tables for New Data **/
	DELETE FROM tblCalcModelMultiBaseline WHERE IdModel=@IdModel;
	DELETE FROM tblCalcUnitCostDenominatorSumAmount WHERE IdModel=@IdModel;
	DELETE FROM tblCalcUnitCostDenominatorAmount WHERE IdModel=@IdModel;
	DELETE FROM tblCalcBaselineUnitCostDenominatorSumAmount WHERE IdModel=@IdModel;
	DELETE FROM tblCalcCostStructureAssumptionsByField WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalDriverData WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalAssumption WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalDriverAssumption WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE [IdModel]=@IdModel;
	
	/** Base table records - which fields have multiple baselines **/
	INSERT INTO tblCalcModelMultiBaseline ([IdModel],[IdField])
	SELECT tblFields.IdModel, tblFields.IdField
	FROM tblFields INNER JOIN
		tblProjects ON tblFields.IdField = tblProjects.IdField
	GROUP BY tblFields.IdModel, tblFields.IdField, tblProjects.Operation
	HAVING SUM(1)>1 AND tblProjects.Operation=1 AND tblFields.IdModel=@IdModel;

	/** Base table records - Total Value of Denominator Drivers by Field **/
	INSERT INTO tblCalcUnitCostDenominatorSumAmount ([IdModel],[IdField],[Year],[SumNormal],[SumPessimistic],[SumOptimistic])
	SELECT tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year, SUM(tblTechnicalDriverData.Normal) AS SumNormal, SUM(tblTechnicalDriverData.Pessimistic) 
		AS SumPessimistic, SUM(tblTechnicalDriverData.Optimistic) AS SumOptimistic
	FROM tblTechnicalDriverData INNER JOIN
		tblTechnicalDrivers ON tblTechnicalDriverData.IdTechnicalDriver = tblTechnicalDrivers.IdTechnicalDriver AND tblTechnicalDriverData.IdModel = tblTechnicalDrivers.IdModel INNER JOIN
		tblProjects ON tblTechnicalDriverData.IdProject = tblProjects.IdProject
	WHERE tblTechnicalDrivers.UnitCostDenominator=1 AND tblProjects.Operation=1
	GROUP BY tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year
	HAVING tblTechnicalDrivers.IdModel=@IdModel;

	/** Base table records - Total Value of Denominator Drivers by Field/Project **/
	INSERT INTO tblCalcUnitCostDenominatorAmount ([IdModel],[IdField],[IdProject],[Year],[SumNormal],[SumPessimistic],[SumOptimistic])
	SELECT tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.IdProject, tblTechnicalDriverData.Year, SUM(tblTechnicalDriverData.Normal) AS SumNormal, 
		SUM(tblTechnicalDriverData.Pessimistic) AS SumPessimistic, SUM(tblTechnicalDriverData.Optimistic) AS SumOptimistic
	FROM tblTechnicalDriverData INNER JOIN
		tblTechnicalDrivers ON tblTechnicalDriverData.IdTechnicalDriver = tblTechnicalDrivers.IdTechnicalDriver AND tblTechnicalDriverData.IdModel = tblTechnicalDrivers.IdModel INNER JOIN
		tblProjects ON tblTechnicalDriverData.IdProject = tblProjects.IdProject AND tblTechnicalDriverData.IdModel = tblProjects.IdModel
	WHERE tblTechnicalDrivers.UnitCostDenominator=1 AND tblProjects.Operation=1
	GROUP BY tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year, tblTechnicalDriverData.IdProject
	HAVING tblTechnicalDrivers.IdModel=@IdModel;

	/** Base table records - Baseline Unit Cost Denominator **/
	INSERT INTO tblCalcBaselineUnitCostDenominatorSumAmount ([IdModel],[IdField],[Year],[SumNormal],[SumPessimistic],[SumOptimistic])
	SELECT tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year, SUM(tblTechnicalDriverData.Normal) AS SumNormal, 
		SUM(tblTechnicalDriverData.Pessimistic) AS SumPessimistic, SUM(tblTechnicalDriverData.Optimistic) AS SumOptimistic
	FROM tblTechnicalDriverData INNER JOIN
		tblTechnicalDrivers ON tblTechnicalDriverData.IdTechnicalDriver = tblTechnicalDrivers.IdTechnicalDriver AND 
		tblTechnicalDriverData.IdModel = tblTechnicalDrivers.IdModel INNER JOIN
		tblCalcModelMultiBaseline ON tblTechnicalDriverData.IdModel = tblCalcModelMultiBaseline.IdModel AND 
		tblTechnicalDriverData.IdField = tblCalcModelMultiBaseline.IdField right outer join 
		tblProjects  ON tblTechnicalDriverData.IdModel = tblProjects.IdModel AND
		tblTechnicalDriverData.IdField = tblProjects.IdField AND 
		tblTechnicalDriverData.IdProject = tblProjects.IdProject
	WHERE tblTechnicalDrivers.UnitCostDenominator=1
	AND tblProjects.Operation=1
	GROUP BY tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year
	HAVING tblTechnicalDrivers.IdModel=@IdModel;

	/** Check if @intMaxYear (projection years) are all required (based on Denominator Totals) **/
	DECLARE @intMaxYearDenominator INT;
	SELECT @intMaxYearDenominator=MAX([Year]) FROM tblCalcUnitCostDenominatorSumAmount WHERE [IdModel]=@IdModel AND ([SumNormal]>0 OR [SumPessimistic]>0 OR [SumOptimistic]>0) GROUP BY [IdModel];

	IF (@intMaxYearDenominator<@intMaxYear)
	BEGIN
		SET @intMaxYear = @intMaxYearDenominator;
	END

	/** Base table records - Baseline Cost Structure Assumptions By Field **/
	INSERT INTO tblCalcCostStructureAssumptionsByField ([IdModel],[IdField],[IdZiffAccount],[Client],[Ziff])
	SELECT tblProjects.IdModel, tblProjects.IdField, tblCostStructureAssumptionsByProject.IdZiffAccount, tblCostStructureAssumptionsByProject.Client, tblCostStructureAssumptionsByProject.Ziff
	FROM tblCostStructureAssumptionsByProject INNER JOIN
		tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject
	GROUP BY tblCostStructureAssumptionsByProject.IdZiffAccount, tblCostStructureAssumptionsByProject.Client, tblCostStructureAssumptionsByProject.Ziff, tblProjects.IdField, tblProjects.IdModel
	HAVING tblProjects.IdModel=@IdModel;

	/** Create Base for Technical Driver Data **/
	INSERT INTO tblCalcTechnicalDriverData ([IdModel],[IdTechnicalDriver],[IdField],[IdProject],[IdTypeOperation],[Year],[Normal],[Pessimistic],[Optimistic])
	SELECT tblTechnicalDriverData.IdModel, tblTechnicalDriverData.IdTechnicalDriver, tblTechnicalDriverData.IdField, CASE WHEN tblProjects.Operation = 1 THEN 0 ELSE tblProjects.IdProject END AS IdProject, 
		tblFields.IdTypeOperation, tblTechnicalDriverData.Year, SUM(tblTechnicalDriverData.Normal) AS Normal, SUM(tblTechnicalDriverData.Pessimistic) AS Pessimistic, 
		SUM(tblTechnicalDriverData.Optimistic) AS Optimistic
	FROM tblTechnicalDriverData INNER JOIN
		tblFields ON tblTechnicalDriverData.IdField = tblFields.IdField AND tblTechnicalDriverData.IdModel = tblFields.IdModel INNER JOIN
		tblProjects ON tblTechnicalDriverData.IdModel = tblProjects.IdModel AND tblTechnicalDriverData.IdProject = tblProjects.IdProject
	GROUP BY tblTechnicalDriverData.IdModel, tblTechnicalDriverData.IdTechnicalDriver, tblTechnicalDriverData.IdField, tblFields.IdTypeOperation, tblTechnicalDriverData.Year, 
		CASE WHEN tblProjects.Operation = 1 THEN 0 ELSE tblProjects.IdProject END
	HAVING tblTechnicalDriverData.IdModel=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- SETUP HIERARCHY OF ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Set Hierarchy of Technical Assumptions (Specific TypeOperation) **/
	SET @intLoop = @intMaxLevel;
	WHILE @intLoop > 0 BEGIN
		SET @intLoop = @intLoop - 1
		
		INSERT INTO tblCalcTechnicalAssumption ([IdModel],[IdZiffAccount],[IdTypeOperation],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria])
		SELECT tblTechnicalAssumption.IdModel, tblTechnicalAssumption.IdZiffAccount, tblTechnicalAssumption.IdTypeOperation, 
			tblTechnicalAssumption.IdTechnicalDriverSize, tblTechnicalAssumption.IdTechnicalDriverPerformance, 
			tblTechnicalAssumption.CycleValue, tblTechnicalAssumption.ProjectionCriteria
		FROM tblTechnicalAssumption INNER JOIN
			tblZiffAccounts ON tblTechnicalAssumption.IdZiffAccount = tblZiffAccounts.IdZiffAccount AND 
			tblTechnicalAssumption.IdModel = tblZiffAccounts.IdModel
		WHERE tblTechnicalAssumption.IdTypeOperation<>0 AND tblZiffAccounts.RelationLevel=@intLoop AND tblTechnicalAssumption.IdModel=@IdModel;
	END
	
	SET @intLoop = @intMaxLevel;
	WHILE @intLoop > 0 BEGIN
		SET @intLoop = @intLoop - 1
		
		INSERT INTO tblCalcTechnicalAssumption ([IdModel],[IdZiffAccount],[IdTypeOperation],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria])
		SELECT tblTechnicalAssumption.IdModel, tblZiffAccounts.IdZiffAccount, tblTechnicalAssumption.IdTypeOperation, 
			tblTechnicalAssumption.IdTechnicalDriverSize, tblTechnicalAssumption.IdTechnicalDriverPerformance, 
			tblTechnicalAssumption.CycleValue, tblTechnicalAssumption.ProjectionCriteria
		FROM tblTechnicalAssumption INNER JOIN
			tblCalcZiffAccountParentChild ON tblTechnicalAssumption.IdZiffAccount = tblCalcZiffAccountParentChild.IdParent AND 
			tblTechnicalAssumption.IdModel = tblCalcZiffAccountParentChild.IdModel INNER JOIN
			tblZiffAccounts ON tblCalcZiffAccountParentChild.IdChild = tblZiffAccounts.IdZiffAccount AND 
			tblCalcZiffAccountParentChild.IdModel = tblZiffAccounts.IdModel INNER JOIN
			tblZiffAccounts AS tblZiffAccountsParent ON tblTechnicalAssumption.IdZiffAccount = tblZiffAccountsParent.IdZiffAccount
		WHERE tblTechnicalAssumption.IdTypeOperation<>0 AND tblZiffAccountsParent.RelationLevel=@intLoop AND tblTechnicalAssumption.IdModel=@IdModel AND tblZiffAccounts.IdZiffAccount NOT IN (SELECT CTA.IdZiffAccount FROM tblCalcTechnicalAssumption AS CTA WHERE CTA.IdTypeOperation=tblTechnicalAssumption.IdTypeOperation AND CTA.IdModel=@IdModel);
	END
	
	/** Set Hierarchy of Technical Assumptions (General/All TypeOperation) **/
	SET @intLoop = @intMaxLevel;
	WHILE @intLoop > 0 BEGIN
		SET @intLoop = @intLoop - 1
		
		INSERT INTO tblCalcTechnicalAssumption ([IdModel],[IdZiffAccount],[IdTypeOperation],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria])
		SELECT tblTechnicalAssumption.IdModel, tblTechnicalAssumption.IdZiffAccount, TypeOpList.IdTypeOperation, tblTechnicalAssumption.IdTechnicalDriverSize, 
			tblTechnicalAssumption.IdTechnicalDriverPerformance, 
			tblTechnicalAssumption.CycleValue, tblTechnicalAssumption.ProjectionCriteria
		FROM tblTechnicalAssumption INNER JOIN
			tblZiffAccounts ON tblTechnicalAssumption.IdZiffAccount = tblZiffAccounts.IdZiffAccount AND 
			tblTechnicalAssumption.IdModel = tblZiffAccounts.IdModel CROSS JOIN
			  (SELECT IdTypeOperation FROM tblFields WHERE (IdModel=@IdModel) GROUP BY IdTypeOperation) AS TypeOpList
		WHERE tblTechnicalAssumption.IdTypeOperation=0 AND tblZiffAccounts.RelationLevel=@intLoop AND tblTechnicalAssumption.IdModel=@IdModel AND tblZiffAccounts.IdZiffAccount NOT IN (SELECT IdZiffAccount FROM tblCalcTechnicalAssumption AS CTA WHERE IdTypeOperation=TypeOpList.IdTypeOperation AND IdModel=@IdModel);
	END
	
	SET @intLoop = @intMaxLevel;
	WHILE @intLoop > 0 BEGIN
		SET @intLoop = @intLoop - 1
		
		INSERT INTO tblCalcTechnicalAssumption ([IdModel],[IdZiffAccount],[IdTypeOperation],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria])
		SELECT tblTechnicalAssumption.IdModel, tblZiffAccounts.IdZiffAccount, TypeOpList.IdTypeOperation, tblTechnicalAssumption.IdTechnicalDriverSize, 
			tblTechnicalAssumption.IdTechnicalDriverPerformance, 
			tblTechnicalAssumption.CycleValue, tblTechnicalAssumption.ProjectionCriteria
		FROM tblTechnicalAssumption INNER JOIN
			tblCalcZiffAccountParentChild ON tblTechnicalAssumption.IdZiffAccount = tblCalcZiffAccountParentChild.IdParent AND 
			tblTechnicalAssumption.IdModel = tblCalcZiffAccountParentChild.IdModel INNER JOIN
			tblZiffAccounts ON tblCalcZiffAccountParentChild.IdChild = tblZiffAccounts.IdZiffAccount AND 
			tblCalcZiffAccountParentChild.IdModel = tblZiffAccounts.IdModel INNER JOIN
			tblZiffAccounts AS tblZiffAccountsParent ON tblTechnicalAssumption.IdZiffAccount = tblZiffAccountsParent.IdZiffAccount CROSS JOIN
			  (SELECT IdTypeOperation FROM tblFields WHERE (IdModel=@IdModel) GROUP BY IdTypeOperation) AS TypeOpList
		WHERE tblTechnicalAssumption.IdTypeOperation=0 AND tblZiffAccountsParent.RelationLevel=@intLoop AND tblTechnicalAssumption.IdModel=@IdModel AND tblZiffAccounts.IdZiffAccount NOT IN (SELECT IdZiffAccount FROM tblCalcTechnicalAssumption AS CTA WHERE IdTypeOperation=TypeOpList.IdTypeOperation AND IdModel=@IdModel);
	END
	
	/** Remove all that have been marked as N/A **/
	DELETE FROM tblCalcTechnicalAssumption WHERE [ProjectionCriteria]=98 AND [IdModel]=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- SETUP COMPANY BASE AND FORECAST ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Create Company BaseYear Technical Driver Assumption Lookup **/
	DECLARE @YearBase INT;
	DECLARE YearList CURSOR FOR
	SELECT Client AS Year
	FROM tblCalcCostStructureAssumptionsByField
	WHERE IdModel=@IdModel
	GROUP BY Client
	HAVING Client > 0
	ORDER BY Year;

	OPEN YearList;
	FETCH NEXT FROM YearList INTO @YearBase;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		INSERT INTO tblCalcTechnicalDriverAssumption ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff])
		SELECT 1, tblCalcTechnicalAssumption.IdModel, tblCalcTechnicalAssumption.IdZiffAccount, tblCalcTechnicalAssumption.IdTypeOperation, tblFields.IdField, 
			CASE WHEN tblProjects.Operation = 1 THEN 0 ELSE tblProjects.IdProject END AS IdProject, @YearBase + CASE WHEN tblCalcTechnicalAssumption.ProjectionCriteria = 4 AND 
			tblCalcCostStructureAssumptionsByField.Client<>@intMinYear THEN tblCalcTechnicalAssumption.CycleValue - 1 ELSE 0 END AS [Year], tblProjects.Operation, CAST(1 AS BIT) AS BaseYear, 
			tblCalcTechnicalAssumption.IdTechnicalDriverSize, tblCalcTechnicalAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalAssumption.CycleValue, 
			tblCalcTechnicalAssumption.ProjectionCriteria, tblCalcCostStructureAssumptionsByField.Client, tblCalcCostStructureAssumptionsByField.Ziff
		FROM tblCalcTechnicalAssumption LEFT OUTER JOIN
			tblCalcCostStructureAssumptionsByField LEFT OUTER JOIN
			tblFields ON tblCalcCostStructureAssumptionsByField.IdField = tblFields.IdField ON tblCalcTechnicalAssumption.IdZiffAccount = tblCalcCostStructureAssumptionsByField.IdZiffAccount AND 
			tblFields.IdTypeOperation = tblCalcTechnicalAssumption.IdTypeOperation LEFT OUTER JOIN
			tblProjects ON tblFields.IdField = tblProjects.IdField
		GROUP BY tblCalcTechnicalAssumption.IdModel, tblCalcTechnicalAssumption.IdZiffAccount, tblCalcTechnicalAssumption.IdTypeOperation, tblFields.IdField, 
			CASE WHEN tblProjects.Operation = 1 THEN 0 ELSE tblProjects.IdProject END, tblProjects.Operation, tblCalcTechnicalAssumption.IdTechnicalDriverSize, 
			tblCalcTechnicalAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalAssumption.CycleValue, tblCalcTechnicalAssumption.ProjectionCriteria, 
			tblCalcCostStructureAssumptionsByField.Client, tblCalcCostStructureAssumptionsByField.Ziff
		HAVING tblCalcCostStructureAssumptionsByField.Client=@YearBase AND tblProjects.Operation=1 AND tblCalcTechnicalAssumption.IdModel=@IdModel;

		FETCH NEXT FROM YearList INTO @YearBase;
	END

	CLOSE YearList;
	DEALLOCATE YearList;

	/** Set Semi-Variable Flag **/
	UPDATE tblCalcTechnicalDriverAssumption SET [SemivarForecast]=1 WHERE ProjectionCriteria=5 AND Operation=1 AND IdModel=@IdModel;
	
	/** Update Company BaseYear Driver Values **/
	EXEC calcModuleTechnicalDriverUpdate @IdModel,1,0;
	
	/** Update Company BaseCost Value for BaseYear **/
	UPDATE tblCalcBaseCostByField SET
		[TechApplicable]=1,
		[TFNormal] = 1,
		[TFPessimistic] = 1,
		[TFOptimistic] = 1
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField
	WHERE tblCalcTechnicalDriverAssumption.BaseYear=1 AND tblCalcTechnicalDriverAssumption.Operation=1 AND tblCalcTechnicalDriverAssumption.ProjectionCriteria IN (1,2) AND tblCalcBaseCostByField.IdModel=@IdModel;

	UPDATE tblCalcBaseCostByField SET
		tblCalcBaseCostByField.TechApplicable=1,
		tblCalcBaseCostByField.TFNormal = CBCBF.TFNormal,
		tblCalcBaseCostByField.TFPessimistic = CBCBF.TFPessimistic, 
		tblCalcBaseCostByField.TFOptimistic = CBCBF.TFOptimistic
	FROM tblCalcBaseCostByField INNER JOIN
		(
		SELECT tblCalcBaseCostByField_1.IdModel, tblCalcBaseCostByField_1.IdActivity, tblCalcBaseCostByField_1.IdResources, 
			tblCalcBaseCostByField_1.IdClientAccount, tblCalcBaseCostByField_1.IdField, tblCalcBaseCostByField_1.IdClientCostCenter, 
			tblCalcBaseCostByField_1.IdZiffAccount, SUM(CASE WHEN [SizeNormal] IS NULL 
			THEN 0 ELSE [SizeNormal] * (CASE WHEN ISNULL([PerformanceNormal], 0) = 0 THEN 1 ELSE [PerformanceNormal] END) END) AS TFNormal, 
			SUM(CASE WHEN [SizePessimistic] IS NULL THEN 0 ELSE [SizePessimistic] * (CASE WHEN ISNULL([PerformancePessimistic], 0) 
			= 0 THEN 1 ELSE [PerformancePessimistic] END) END) AS TFPessimistic, SUM(CASE WHEN [SizeOptimistic] IS NULL 
			THEN 0 ELSE [SizeOptimistic] * (CASE WHEN ISNULL([PerformanceOptimistic], 0) = 0 THEN 1 ELSE [PerformanceOptimistic] END) END) 
			AS TFOptimistic
		FROM tblCalcBaseCostByField AS tblCalcBaseCostByField_1 INNER JOIN
			tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField_1.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
			tblCalcBaseCostByField_1.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount AND 
			tblCalcBaseCostByField_1.IdField = tblCalcTechnicalDriverAssumption.IdField
			WHERE tblCalcTechnicalDriverAssumption.BaseYear=1 AND tblCalcTechnicalDriverAssumption.Operation=1 AND 
			tblCalcTechnicalDriverAssumption.ProjectionCriteria IN (3, 4, 5)
		GROUP BY tblCalcBaseCostByField_1.IdModel, tblCalcBaseCostByField_1.IdActivity, tblCalcBaseCostByField_1.IdResources, 
			tblCalcBaseCostByField_1.IdClientAccount, tblCalcBaseCostByField_1.IdField, tblCalcBaseCostByField_1.IdClientCostCenter, 
			tblCalcBaseCostByField_1.IdZiffAccount
		) AS CBCBF ON tblCalcBaseCostByField.IdModel = CBCBF.IdModel AND 
		tblCalcBaseCostByField.IdActivity = CBCBF.IdActivity AND tblCalcBaseCostByField.IdResources = CBCBF.IdResources AND 
		tblCalcBaseCostByField.IdClientAccount = CBCBF.IdClientAccount AND tblCalcBaseCostByField.IdField = CBCBF.IdField AND 
		tblCalcBaseCostByField.IdClientCostCenter = CBCBF.IdClientCostCenter AND tblCalcBaseCostByField.IdZiffAccount = CBCBF.IdZiffAccount
	WHERE tblCalcBaseCostByField.IdModel = @IdModel;
	
	UPDATE tblCalcBaseCostByField SET
		[BCRNormal] = CASE WHEN ISNULL([TFNormal],0)=0 THEN 0 ELSE [Amount]/[TFNormal] END,
		[BCRPessimistic] = CASE WHEN ISNULL([TFPessimistic],0)=0 THEN 0 ELSE [Amount]/[TFPessimistic] END,
		[BCROptimistic] = CASE WHEN ISNULL([TFOptimistic],0)=0 THEN 0 ELSE [Amount]/[TFOptimistic] END
	WHERE TechApplicable=1 AND IdModel=@IdModel;

	UPDATE tblCalcBaseCostByField SET
		[BCRNormal] = 0,
		[BCRPessimistic] = 0,
		[BCROptimistic] = 0
	WHERE TechApplicable=0 AND IdModel=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- SETUP ZIFF BASE AND FORECAST ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Create Ziff BaseYear Technical Driver Assumption Lookup **/
	DECLARE @YearBaseZiff INT;
	DECLARE YearListZiff CURSOR FOR
	SELECT Ziff AS Year
	FROM tblCalcCostStructureAssumptionsByField
	WHERE IdModel=@IdModel
	GROUP BY Ziff
	HAVING Ziff > 0
	ORDER BY Year;

	OPEN YearListZiff;
	FETCH NEXT FROM YearListZiff INTO @YearBaseZiff;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		INSERT INTO tblCalcTechnicalDriverAssumptionZiff ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff])
		SELECT 1, tblCalcTechnicalAssumption.IdModel, tblCalcTechnicalAssumption.IdZiffAccount, tblCalcTechnicalAssumption.IdTypeOperation, tblFields.IdField, 
			CASE WHEN tblProjects.Operation = 1 THEN 0 ELSE tblProjects.IdProject END AS IdProject, @YearBaseZiff + CASE WHEN tblCalcTechnicalAssumption.ProjectionCriteria = 4 AND 
			tblCalcCostStructureAssumptionsByField.Ziff = 0 THEN tblCalcTechnicalAssumption.CycleValue - 1 ELSE 0 END AS [Year], tblProjects.Operation, CAST(1 AS BIT) AS BaseYear, 
			tblCalcTechnicalAssumption.IdTechnicalDriverSize, tblCalcTechnicalAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalAssumption.CycleValue, 
			tblCalcTechnicalAssumption.ProjectionCriteria, tblCalcCostStructureAssumptionsByField.Client, tblCalcCostStructureAssumptionsByField.Ziff
		FROM tblCalcTechnicalAssumption LEFT OUTER JOIN
			tblCalcCostStructureAssumptionsByField LEFT OUTER JOIN
			tblFields ON tblCalcCostStructureAssumptionsByField.IdField = tblFields.IdField ON tblCalcTechnicalAssumption.IdZiffAccount = tblCalcCostStructureAssumptionsByField.IdZiffAccount AND 
			tblFields.IdTypeOperation = tblCalcTechnicalAssumption.IdTypeOperation LEFT OUTER JOIN
			tblProjects ON tblFields.IdField = tblProjects.IdField
		GROUP BY tblCalcTechnicalAssumption.IdModel, tblCalcTechnicalAssumption.IdZiffAccount, tblCalcTechnicalAssumption.IdTypeOperation, tblFields.IdField, 
			CASE WHEN tblProjects.Operation = 1 THEN 0 ELSE tblProjects.IdProject END, tblProjects.Operation, tblCalcTechnicalAssumption.IdTechnicalDriverSize, 
			tblCalcTechnicalAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalAssumption.CycleValue, tblCalcTechnicalAssumption.ProjectionCriteria, 
			tblCalcCostStructureAssumptionsByField.Client, tblCalcCostStructureAssumptionsByField.Ziff
		HAVING tblCalcCostStructureAssumptionsByField.Ziff=@YearBaseZiff AND tblProjects.Operation=1 AND tblCalcTechnicalAssumption.IdModel=@IdModel;

		FETCH NEXT FROM YearListZiff INTO @YearBaseZiff;
	END

	CLOSE YearListZiff;
	DEALLOCATE YearListZiff;

	/** Set Semi-Variable Flag **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET [SemivarForecast]=1 WHERE ProjectionCriteria=5 AND Operation=1 AND IdModel=@IdModel;
	
	/** Update Ziff BaseYear Driver Values **/
	EXEC calcModuleTechnicalDriverUpdate @IdModel,2,1;
	
	/** Update Ziff BaseCost Value for BaseYear **/
	UPDATE tblCalcBaseCostByFieldZiff SET
		[TechApplicable]=1,
		[TFNormal] = 1,
		[TFPessimistic] = 1,
		[TFOptimistic] = 1
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField
	WHERE tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 AND tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria IN (1,2) AND tblCalcBaseCostByFieldZiff.IdModel=@IdModel;

	UPDATE tblCalcBaseCostByFieldZiff SET
		tblCalcBaseCostByFieldZiff.TechApplicable=1,
		tblCalcBaseCostByFieldZiff.TFNormal = CBCBFZ.TFNormal,
		tblCalcBaseCostByFieldZiff.TFPessimistic = CBCBFZ.TFPessimistic,
		tblCalcBaseCostByFieldZiff.TFOptimistic = CBCBFZ.TFOptimistic
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		 (
		 SELECT tblCalcBaseCostByFieldZiff_1.IdModel, tblCalcBaseCostByFieldZiff_1.IdField, tblCalcBaseCostByFieldZiff_1.IdZiffAccount, 
			 SUM(CASE WHEN [SizeNormal] IS NULL THEN 0 ELSE [SizeNormal] * (CASE WHEN ISNULL([PerformanceNormal], 0) 
			 = 0 THEN 1 ELSE [PerformanceNormal] END) END) AS TFNormal, SUM(CASE WHEN [SizePessimistic] IS NULL 
			 THEN 0 ELSE [SizePessimistic] * (CASE WHEN ISNULL([PerformancePessimistic], 0) = 0 THEN 1 ELSE [PerformancePessimistic] END) END) 
			 AS TFPessimistic, SUM(CASE WHEN [SizeOptimistic] IS NULL THEN 0 ELSE [SizeOptimistic] * (CASE WHEN ISNULL([PerformanceOptimistic], 0) 
			 = 0 THEN 1 ELSE [PerformanceOptimistic] END) END) AS TFOptimistic
			 FROM tblCalcBaseCostByFieldZiff AS tblCalcBaseCostByFieldZiff_1 INNER JOIN
			 tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff_1.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
			 tblCalcBaseCostByFieldZiff_1.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount AND 
			 tblCalcBaseCostByFieldZiff_1.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField
		 WHERE tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 AND 
			tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria IN (3, 4, 5)
		 GROUP BY tblCalcBaseCostByFieldZiff_1.IdModel, tblCalcBaseCostByFieldZiff_1.IdField, tblCalcBaseCostByFieldZiff_1.IdZiffAccount
		 ) AS CBCBFZ ON 
		 tblCalcBaseCostByFieldZiff.IdModel = CBCBFZ.IdModel AND tblCalcBaseCostByFieldZiff.IdField = CBCBFZ.IdField AND 
		 tblCalcBaseCostByFieldZiff.IdZiffAccount = CBCBFZ.IdZiffAccount
	WHERE tblCalcBaseCostByFieldZiff.IdModel = @IdModel;

	UPDATE tblCalcBaseCostByFieldZiff SET
		[BCRNormal] = CASE WHEN ISNULL([TFNormal],0)=0 THEN 0 ELSE [TotalCost]/[TFNormal] END,
		[BCRPessimistic] = CASE WHEN ISNULL([TFPessimistic],0)=0 THEN 0 ELSE [TotalCost]/[TFPessimistic] END,
		[BCROptimistic] = CASE WHEN ISNULL([TFOptimistic],0)=0 THEN 0 ELSE [TotalCost]/[TFOptimistic] END
	WHERE TechApplicable=1 AND IdModel=@IdModel;
	
	UPDATE tblCalcBaseCostByFieldZiff SET
		[BCRNormal] = 0,
		[BCRPessimistic] = 0,
		[BCROptimistic] = 0
	WHERE TechApplicable=0 AND IdModel=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- UPDATE BASE COST RATES BASE YEAR
	--------------------------------------------------------------------------------------
	
	/** Update Company Base Cost Rates **/
	UPDATE tblCalcTechnicalDriverAssumption SET
		[BCRNormal] = CalcBaseCostByFieldSummary.BCRNormal,
		[BCRPessimistic] = CalcBaseCostByFieldSummary.BCRPessimistic,
		[BCROptimistic] = CalcBaseCostByFieldSummary.BCROptimistic
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(BCRNormal) AS BCRNormal, SUM(BCRPessimistic) AS BCRPessimistic, SUM(BCROptimistic) AS BCROptimistic
		FROM tblCalcBaseCostByField AS CBCBF
		GROUP BY IdModel, IdField, IdZiffAccount
		) AS CalcBaseCostByFieldSummary ON 
		tblCalcTechnicalDriverAssumption.IdModel = CalcBaseCostByFieldSummary.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = CalcBaseCostByFieldSummary.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = CalcBaseCostByFieldSummary.IdField
	WHERE tblCalcTechnicalDriverAssumption.BaseYear=1 AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Update Ziff Base Cost Rates **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		[BCRNormal] = CalcBaseCostByFieldSummaryZiff.BCRNormal,
		[BCRPessimistic] = CalcBaseCostByFieldSummaryZiff.BCRPessimistic,
		[BCROptimistic] = CalcBaseCostByFieldSummaryZiff.BCROptimistic
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(BCRNormal) AS BCRNormal, SUM(BCRPessimistic) AS BCRPessimistic, SUM(BCROptimistic) AS BCROptimistic
		FROM tblCalcBaseCostByFieldZiff AS CBCBFZ
		GROUP BY IdModel, IdField, IdZiffAccount
		) AS CalcBaseCostByFieldSummaryZiff ON 
		tblCalcTechnicalDriverAssumptionZiff.IdModel = CalcBaseCostByFieldSummaryZiff.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = CalcBaseCostByFieldSummaryZiff.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = CalcBaseCostByFieldSummaryZiff.IdField
	WHERE tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	/** Drop Ziff BaseYear with no costs **/
	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE [BaseYear]=1 AND [BCRNormal] IS NULL AND [BCRPessimistic] IS NULL AND [BCROptimistic] IS NULL AND [IdModel]=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- SETUP ADDITIONAL YEARS ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Create Company Additional Years Technical Driver Assumption Lookup **/
	INSERT INTO tblCalcTechnicalDriverAssumption ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff],[SemivarForecast])
	SELECT 2, tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, tblCalcTechnicalDriverAssumption.IdTypeOperation, 
		tblCalcTechnicalDriverAssumption.IdField, tblCalcTechnicalDriverAssumption.IdProject, tblCalcModelYears.Year, 
		tblCalcTechnicalDriverAssumption.Operation, CAST(0 AS BIT) AS BaseYear, tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize, 
		tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalDriverAssumption.CycleValue, 
		tblCalcTechnicalDriverAssumption.ProjectionCriteria, tblCalcTechnicalDriverAssumption.YearClient, tblCalcTechnicalDriverAssumption.YearZiff, tblCalcTechnicalDriverAssumption.SemivarForecast
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		tblCalcModelYears ON tblCalcTechnicalDriverAssumption.IdModel = tblCalcModelYears.IdModel AND 
		tblCalcTechnicalDriverAssumption.Year < tblCalcModelYears.Year
	WHERE tblCalcModelYears.Year<=@intMaxYear AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Create Ziff Additional Years Technical Driver Assumption Lookup **/
	INSERT INTO tblCalcTechnicalDriverAssumptionZiff ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff],[SemivarForecast])
	SELECT 2, tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdTypeOperation, 
		tblCalcTechnicalDriverAssumptionZiff.IdField, tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcModelYears.Year, 
		tblCalcTechnicalDriverAssumptionZiff.Operation, CAST(0 AS BIT) AS BaseYear, tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverSize, 
		tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverPerformance, tblCalcTechnicalDriverAssumptionZiff.CycleValue, 
		tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.YearClient, tblCalcTechnicalDriverAssumptionZiff.YearZiff, tblCalcTechnicalDriverAssumptionZiff.SemivarForecast
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		tblCalcModelYears ON tblCalcTechnicalDriverAssumptionZiff.IdModel = tblCalcModelYears.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.Year < tblCalcModelYears.Year
	WHERE tblCalcModelYears.Year<=@intMaxYear AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	/** Create Company BusinessOpportunity Technical Driver Assumption Lookup **/
	INSERT INTO tblCalcTechnicalDriverAssumption ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff],[SemivarForecast])
	SELECT 3, tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, tblCalcTechnicalDriverAssumption.IdTypeOperation, 
		tblCalcTechnicalDriverAssumption.IdField, tblProjects.IdProject, tblCalcTechnicalDriverAssumption.Year, tblProjects.Operation, 
		CASE WHEN tblCalcTechnicalDriverAssumption.Year = tblProjects.Starts THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS BaseYear, 
		tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize, tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance,  
		tblCalcTechnicalDriverAssumption.CycleValue, tblCalcTechnicalDriverAssumption.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumption.YearClient, tblCalcTechnicalDriverAssumption.YearZiff, tblCalcTechnicalDriverAssumption.SemivarForecast
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumption.IdModel = tblProjects.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdField = tblProjects.IdField AND tblCalcTechnicalDriverAssumption.Year >= tblProjects.Starts
	WHERE tblProjects.Operation=2 AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Remove Company BusinessOpportunity Technical Driver Assumption = Fixed or Semi-Fixed **/
	DELETE FROM tblCalcTechnicalDriverAssumption WHERE ([ProjectionCriteria]=1 OR [ProjectionCriteria]=2) AND [Operation]=2 AND [IdModel]=@IdModel;

	/** Create Ziff BusinessOpportunity Technical Driver Assumption Lookup **/
	/** Note: Modifying SemivarProjection with Fixed=1 to Variable=3 for Forecast **/
	INSERT INTO tblCalcTechnicalDriverAssumptionZiff ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff],[SemivarForecast])
	SELECT 3, tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdTypeOperation, 
		tblCalcTechnicalDriverAssumptionZiff.IdField, tblProjects.IdProject, tblCalcTechnicalDriverAssumptionZiff.Year, tblProjects.Operation, 
		CASE WHEN tblCalcTechnicalDriverAssumptionZiff.Year = tblProjects.Starts THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS BaseYear, 
		tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverSize, tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverPerformance, 
		tblCalcTechnicalDriverAssumptionZiff.CycleValue, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria,
		tblCalcTechnicalDriverAssumptionZiff.YearClient, tblCalcTechnicalDriverAssumptionZiff.YearZiff, tblCalcTechnicalDriverAssumptionZiff.SemivarForecast
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumptionZiff.IdModel = tblProjects.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = tblProjects.IdField AND tblCalcTechnicalDriverAssumptionZiff.Year >= tblProjects.Starts
	WHERE tblProjects.Operation=2 AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	/** Remove Ziff BusinessOpportunity Technical Driver Assumption = Fixed or Semi-Fixed **/
	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE ([ProjectionCriteria]=1 OR [ProjectionCriteria]=2) AND [Operation]=2 AND [IdModel]=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- CLEAN UP ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Remove Base Year Assumptions not in initial Year for Semi-Fixed **/
	UPDATE tblCalcTechnicalDriverAssumption SET [BaseYear]=0 WHERE [ProjectionCriteria]=2 AND [BaseYear]=1 AND [Year]<>@intMinYear AND [IdModel]=@IdModel;
	
	/** Rebase and Remove Excess Years from all Cyclical Assumptions **/
	EXEC calcModuleTechnicalCyclicalRebase @IdModel,@intMinYear,@intMaxYear;
	
	--------------------------------------------------------------------------------------
	-- ADD CLIENT AND ZIFF COST ASSUMPTION START YEARS TO TABLE
	--------------------------------------------------------------------------------------
	
	/** Update Company Client/Ziff Years Lookup **/
	UPDATE tblCalcTechnicalDriverAssumption
	SET YearClient=CTDALookup.YearClient, YearZiff=CTDALookup.YearZiff
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		(
		SELECT CTDA.IdModel, CTDA.IdZiffAccount, CTDA.IdField, CTDA.YearClient, CTDA.YearZiff
		FROM tblCalcTechnicalDriverAssumption AS CTDA INNER JOIN
			tblCalcCostStructureAssumptionsByField AS CSABF ON CTDA.IdModel = CSABF.IdModel AND CTDA.IdField = CSABF.IdField AND CTDA.IdZiffAccount = CSABF.IdZiffAccount
		WHERE CTDA.Operation=1 AND CTDA.BaseYear=1
		GROUP BY CTDA.IdModel, CTDA.IdZiffAccount, CTDA.IdField, CTDA.YearClient, CTDA.YearZiff
		) AS CTDALookup ON 
		tblCalcTechnicalDriverAssumption.IdModel = CTDALookup.IdModel AND tblCalcTechnicalDriverAssumption.IdZiffAccount = CTDALookup.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = CTDALookup.IdField
	WHERE tblCalcTechnicalDriverAssumption.YearClient IS NULL AND tblCalcTechnicalDriverAssumption.YearZiff IS NULL AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Remove Company Years Based on Company/Ziff Start Years **/
	DELETE FROM tblCalcTechnicalDriverAssumption WHERE [YearZiff]>0 AND [Year]>=[YearZiff] AND [IdModel]=@IdModel;
	
	/** Update Ziff Client/Ziff Years Lookup **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff
	SET YearClient=CTDAZLookup.YearClient, YearZiff=CTDAZLookup.YearZiff
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		(
		SELECT CTDAZ.IdModel, CTDAZ.IdZiffAccount, CTDAZ.IdField, CTDAZ.YearClient, CTDAZ.YearZiff
		FROM tblCalcTechnicalDriverAssumptionZiff AS CTDAZ INNER JOIN
			tblCalcCostStructureAssumptionsByField AS CSABF ON CTDAZ.IdModel = CSABF.IdModel AND CTDAZ.IdField = CSABF.IdField AND CTDAZ.IdZiffAccount = CSABF.IdZiffAccount
		WHERE CTDAZ.Operation=1 AND CTDAZ.BaseYear=1
		GROUP BY CTDAZ.IdModel, CTDAZ.IdZiffAccount, CTDAZ.IdField, CTDAZ.YearClient, CTDAZ.YearZiff
		) AS CTDAZLookup ON 
		tblCalcTechnicalDriverAssumptionZiff.IdModel = CTDAZLookup.IdModel AND tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = CTDAZLookup.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = CTDAZLookup.IdField
	WHERE tblCalcTechnicalDriverAssumptionZiff.YearClient IS NULL AND tblCalcTechnicalDriverAssumptionZiff.YearZiff IS NULL AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	/** Remove Ziff Years Based on Company/Ziff Start Years (SHOULD BE NO RECORDS - THIS IS JUST A DOUBLE CHECK) **/
	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE [YearClient]>0 AND [Year]<[YearZiff] AND [IdModel]=@IdModel;
	
	/** Ensure All Ziff BO Items as properly flagged as base year **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET BaseYear=1 WHERE [Operation]=2 AND [BaseYear]=0 AND [Year]=[YearZiff] AND IdModel=@IdModel;	

	--------------------------------------------------------------------------------------
	-- PREP FOR TECHNICAL BASE COST RATE CALCS
	--------------------------------------------------------------------------------------
	
	/** Update Company TF (Base Year Only for BCR Calc) **/
	UPDATE tblCalcTechnicalDriverAssumption SET 
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeNormal], 0) = 0 THEN 0 ELSE [SizeNormal]*(CASE WHEN ISNULL([PerformanceNormal],0)=0 THEN 1 ELSE [PerformanceNormal] END) END ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizePessimistic], 0) = 0 THEN 0 ELSE [SizePessimistic]*(CASE WHEN ISNULL([PerformancePessimistic],0)=0 THEN 1 ELSE [PerformancePessimistic] END) END ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeOptimistic], 0) = 0 THEN 0 ELSE [SizeOptimistic]*(CASE WHEN ISNULL([PerformanceOptimistic],0)=0 THEN 1 ELSE [PerformanceOptimistic] END) END ELSE 0 END
	WHERE [BaseYear]=1 AND [IdModel]=@IdModel;
	
	/** Update Ziff TF  (Base Year Only for BCR Calc) **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeNormal], 0) = 0 THEN 0 ELSE [SizeNormal]*(CASE WHEN ISNULL([PerformanceNormal],0)=0 THEN 1 ELSE [PerformanceNormal] END) END ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizePessimistic], 0) = 0 THEN 0 ELSE [SizePessimistic]*(CASE WHEN ISNULL([PerformancePessimistic],0)=0 THEN 1 ELSE [PerformancePessimistic] END) END ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeOptimistic], 0) = 0 THEN 0 ELSE [SizeOptimistic]*(CASE WHEN ISNULL([PerformanceOptimistic],0)=0 THEN 1 ELSE [PerformanceOptimistic] END) END ELSE 0 END
	WHERE [BaseYear]=1 AND [IdModel]=@IdModel;

	/** Remove Base Year Items without Project Drivers **/
	DELETE FROM tblCalcTechnicalDriverAssumption
	WHERE SizeNormal IS NULL AND SizePessimistic IS NULL AND SizeOptimistic IS NULL AND PerformanceNormal IS NULL AND PerformancePessimistic IS NULL AND PerformanceOptimistic IS NULL AND ProjectionCriteria IN (2,3,4,5) AND BaseYear=1 AND Operation=1 AND IdModel=@IdModel;

	/** Technical Base Cost Rate **/
	EXEC calcModuleTechnicalBaseCostRate @IdModel;

	--------------------------------------------------------------------------------------
	-- POST TECHNICAL BASE COST RATE RESET
	--------------------------------------------------------------------------------------
	
	/** Update All Years Driver Values **/
	EXEC calcModuleTechnicalDriverUpdate @IdModel,1,0;
	EXEC calcModuleTechnicalDriverUpdate @IdModel,2,0;
		
	/** Update Company TF **/
	UPDATE tblCalcTechnicalDriverAssumption SET 
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeNormal], 0) = 0 THEN 0 ELSE [SizeNormal]*(CASE WHEN ISNULL([PerformanceNormal],0)=0 THEN 1 ELSE [PerformanceNormal] END) END ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizePessimistic], 0) = 0 THEN 0 ELSE [SizePessimistic]*(CASE WHEN ISNULL([PerformancePessimistic],0)=0 THEN 1 ELSE [PerformancePessimistic] END) END ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeOptimistic], 0) = 0 THEN 0 ELSE [SizeOptimistic]*(CASE WHEN ISNULL([PerformanceOptimistic],0)=0 THEN 1 ELSE [PerformanceOptimistic] END) END ELSE 0 END
	WHERE [IdModel]=@IdModel;
	
	/** Update Ziff TF **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5  THEN CASE WHEN ISNULL([SizeNormal], 0) = 0 THEN 0 ELSE [SizeNormal]*(CASE WHEN ISNULL([PerformanceNormal],0)=0 THEN 1 ELSE [PerformanceNormal] END) END ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizePessimistic], 0) = 0 THEN 0 ELSE [SizePessimistic]*(CASE WHEN ISNULL([PerformancePessimistic],0)=0 THEN 1 ELSE [PerformancePessimistic] END) END ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeOptimistic], 0) = 0 THEN 0 ELSE [SizeOptimistic]*(CASE WHEN ISNULL([PerformanceOptimistic],0)=0 THEN 1 ELSE [PerformanceOptimistic] END) END ELSE 0 END
	WHERE [IdModel]=@IdModel;

	--/** Remove Base Year Items without Project Drivers **/
	DELETE FROM tblCalcTechnicalDriverAssumption
	WHERE SizeNormal IS NULL AND SizePessimistic IS NULL AND SizeOptimistic IS NULL AND PerformanceNormal IS NULL AND PerformancePessimistic IS NULL AND PerformanceOptimistic IS NULL AND ProjectionCriteria IN (2,3,4,5) AND BaseYear=1 AND Operation=1 AND IdModel=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- UPDATE BASE COST RATES ALL YEARS
	--------------------------------------------------------------------------------------
	
	/** Update Company Base Cost Rates **/
	UPDATE tblCalcTechnicalDriverAssumption SET
		[BCRNormal] = CalcBaseCostByFieldSummary.BCRNormal,
		[BCRPessimistic] = CalcBaseCostByFieldSummary.BCRPessimistic,
		[BCROptimistic] = CalcBaseCostByFieldSummary.BCROptimistic
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(BCRNormal) AS BCRNormal, SUM(BCRPessimistic) AS BCRPessimistic, SUM(BCROptimistic) AS BCROptimistic
		FROM tblCalcBaseCostByField AS CBCBF
		GROUP BY IdModel, IdField, IdZiffAccount
		) AS CalcBaseCostByFieldSummary ON 
		tblCalcTechnicalDriverAssumption.IdModel = CalcBaseCostByFieldSummary.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = CalcBaseCostByFieldSummary.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = CalcBaseCostByFieldSummary.IdField
	WHERE tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Update Ziff Base Cost Rates **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		[BCRNormal] = CalcBaseCostByFieldSummaryZiff.BCRNormal,
		[BCRPessimistic] = CalcBaseCostByFieldSummaryZiff.BCRPessimistic,
		[BCROptimistic] = CalcBaseCostByFieldSummaryZiff.BCROptimistic
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(BCRNormal) AS BCRNormal, SUM(BCRPessimistic) AS BCRPessimistic, SUM(BCROptimistic) AS BCROptimistic
		FROM tblCalcBaseCostByFieldZiff AS CBCBFZ
		GROUP BY IdModel, IdField, IdZiffAccount
		) AS CalcBaseCostByFieldSummaryZiff ON 
		tblCalcTechnicalDriverAssumptionZiff.IdModel = CalcBaseCostByFieldSummaryZiff.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = CalcBaseCostByFieldSummaryZiff.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = CalcBaseCostByFieldSummaryZiff.IdField
	WHERE tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;

	--------------------------------------------------------------------------------------
	-- SEMI-VARIABLE Baseline Reset as Fixed Rates
	--------------------------------------------------------------------------------------

	/** Update Company Base Cost Rates Semi-Variable **/
	UPDATE tblCalcTechnicalDriverAssumption SET
		tblCalcTechnicalDriverAssumption.TFNormal=1,
		tblCalcTechnicalDriverAssumption.TFPessimistic=1,
		tblCalcTechnicalDriverAssumption.TFOptimistic=1,
		tblCalcTechnicalDriverAssumption.BCRNormal=BCRSV.SumAmount,
		tblCalcTechnicalDriverAssumption.BCRPessimistic=BCRSV.SumAmount,
		tblCalcTechnicalDriverAssumption.BCROptimistic=BCRSV.SumAmount
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		(
		SELECT tblCalcTechnicalDriverAssumption_1.IdModel, tblCalcTechnicalDriverAssumption_1.IdZiffAccount, tblCalcTechnicalDriverAssumption_1.IdField, tblCalcTechnicalDriverAssumption_1.IdProject, 
			tblCalcTechnicalDriverAssumption_1.Year, SUM(tblCalcBaseCostByField.Amount) AS SumAmount
		FROM tblCalcTechnicalDriverAssumption AS tblCalcTechnicalDriverAssumption_1 INNER JOIN
			tblCalcBaseCostByField ON tblCalcTechnicalDriverAssumption_1.IdModel = tblCalcBaseCostByField.IdModel AND 
			tblCalcTechnicalDriverAssumption_1.IdField = tblCalcBaseCostByField.IdField AND tblCalcTechnicalDriverAssumption_1.IdZiffAccount = tblCalcBaseCostByField.IdZiffAccount
		WHERE tblCalcTechnicalDriverAssumption_1.Operation=1 AND tblCalcTechnicalDriverAssumption_1.ProjectionCriteria=5
		GROUP BY tblCalcTechnicalDriverAssumption_1.IdModel, tblCalcTechnicalDriverAssumption_1.IdZiffAccount, tblCalcTechnicalDriverAssumption_1.IdField, tblCalcTechnicalDriverAssumption_1.IdProject, 
			tblCalcTechnicalDriverAssumption_1.Year
		) AS BCRSV ON tblCalcTechnicalDriverAssumption.IdModel = BCRSV.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = BCRSV.IdZiffAccount AND tblCalcTechnicalDriverAssumption.IdField = BCRSV.IdField AND 
		tblCalcTechnicalDriverAssumption.IdProject = BCRSV.IdProject AND tblCalcTechnicalDriverAssumption.Year = BCRSV.Year
	WHERE tblCalcTechnicalDriverAssumption.IdModel=@IdModel;

	UPDATE tblCalcTechnicalDriverAssumption SET
		tblCalcTechnicalDriverAssumption.TFNormal=1,
		tblCalcTechnicalDriverAssumption.TFPessimistic=1,
		tblCalcTechnicalDriverAssumption.TFOptimistic=1
	FROM tblCalcTechnicalDriverAssumption
	WHERE tblCalcTechnicalDriverAssumption.Operation=1 AND tblCalcTechnicalDriverAssumption.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;

	/** Update Ziff Base Cost Rates Semi-Variable **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		tblCalcTechnicalDriverAssumptionZiff.TFNormal=1,
		tblCalcTechnicalDriverAssumptionZiff.TFPessimistic=1,
		tblCalcTechnicalDriverAssumptionZiff.TFOptimistic=1,
		tblCalcTechnicalDriverAssumptionZiff.BCRNormal=BCRSVZ.SumAmount,
		tblCalcTechnicalDriverAssumptionZiff.BCRPessimistic=BCRSVZ.SumAmount,
		tblCalcTechnicalDriverAssumptionZiff.BCROptimistic=BCRSVZ.SumAmount
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		(
		SELECT tblCalcTechnicalDriverAssumptionZiff_1.IdModel, tblCalcTechnicalDriverAssumptionZiff_1.IdZiffAccount, tblCalcTechnicalDriverAssumptionZiff_1.IdField, tblCalcTechnicalDriverAssumptionZiff_1.IdProject, 
			tblCalcTechnicalDriverAssumptionZiff_1.Year, SUM(tblCalcBaseCostByFieldZiff.TotalCost) AS SumAmount
		FROM tblCalcTechnicalDriverAssumptionZiff AS tblCalcTechnicalDriverAssumptionZiff_1 INNER JOIN
			tblCalcBaseCostByFieldZiff ON tblCalcTechnicalDriverAssumptionZiff_1.IdModel = tblCalcBaseCostByFieldZiff.IdModel AND 
			tblCalcTechnicalDriverAssumptionZiff_1.IdField = tblCalcBaseCostByFieldZiff.IdField AND tblCalcTechnicalDriverAssumptionZiff_1.IdZiffAccount = tblCalcBaseCostByFieldZiff.IdZiffAccount
		WHERE tblCalcTechnicalDriverAssumptionZiff_1.Operation=1 AND tblCalcTechnicalDriverAssumptionZiff_1.ProjectionCriteria=5
		GROUP BY tblCalcTechnicalDriverAssumptionZiff_1.IdModel, tblCalcTechnicalDriverAssumptionZiff_1.IdZiffAccount, tblCalcTechnicalDriverAssumptionZiff_1.IdField, tblCalcTechnicalDriverAssumptionZiff_1.IdProject, 
			tblCalcTechnicalDriverAssumptionZiff_1.Year
		) AS BCRSVZ ON tblCalcTechnicalDriverAssumptionZiff.IdModel = BCRSVZ.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = BCRSVZ.IdZiffAccount AND tblCalcTechnicalDriverAssumptionZiff.IdField = BCRSVZ.IdField AND 
		tblCalcTechnicalDriverAssumptionZiff.IdProject = BCRSVZ.IdProject AND tblCalcTechnicalDriverAssumptionZiff.Year = BCRSVZ.Year
	WHERE tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;

	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		tblCalcTechnicalDriverAssumptionZiff.TFNormal=1,
		tblCalcTechnicalDriverAssumptionZiff.TFPessimistic=1,
		tblCalcTechnicalDriverAssumptionZiff.TFOptimistic=1
	FROM tblCalcTechnicalDriverAssumptionZiff
	WHERE tblCalcTechnicalDriverAssumptionZiff.Operation=1 AND tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- RUN FORECASTS
	--------------------------------------------------------------------------------------
	
	/** Technical Forecast **/
	EXEC calcModuleTechnicalForecast @IdModel,@intMinYear,@intMaxYear;

	/** Cleanup Calc Tables as Data is not needed anywhere else **/
	DELETE FROM tblCalcModelMultiBaseline WHERE IdModel=@IdModel;
	DELETE FROM tblCalcUnitCostDenominatorSumAmount WHERE IdModel=@IdModel;
	DELETE FROM tblCalcUnitCostDenominatorAmount WHERE IdModel=@IdModel;
	DELETE FROM tblCalcBaselineUnitCostDenominatorSumAmount WHERE IdModel=@IdModel;
	DELETE FROM tblCalcCostStructureAssumptionsByField WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalDriverData WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalAssumption WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalDriverAssumption WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE [IdModel]=@IdModel;
	
END

GO
/****** Object:  StoredProcedure [dbo].[allpListExternalBaseCostReferences]    Script Date: 10/06/2015 14:29:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-06-17
-- Description:	List of External Base Cost References
-- ================================================================================================
-- Modification:	Added field ExternalAccount code for export of data
-- ================================================================================================
		
ALTER PROCEDURE [dbo].[allpListExternalBaseCostReferences]
	-- Add the parameters for the stored procedure here
	@IdModel INT,
	@IdField INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF OBJECT_ID('tempdb..#ExternalCostReferences') IS NOT NULL DROP TABLE #ExternalCostReferences

	CREATE TABLE #ExternalCostReferences (IdModel INT,IdZiffBaseCost INT,IdField INT,IdZiffAccount INT,IdPeerCriteria INT,UnitCost FLOAT,Quantity FLOAT,TotalCost FLOAT,
										  Field VARCHAR(150),CodeZiff VARCHAR(50),ZiffAccount VARCHAR(150),IdRootZiffAccount INT,RootCodeZiff VARCHAR(50),RootZiffAccount VARCHAR(150),
										  PeerCriteria VARCHAR(50),Unit VARCHAR(150),IdUnitPeerCriteria INT,TechApplicable BIT,TFNormal FLOAT,TFPessimistic FLOAT,TFOptimistic FLOAT,
										  BCRNormal FLOAT,BCRPessimistic FLOAT,BCROptimistic FLOAT, SortOrder VARCHAR(50), ExternalAccountCode VARCHAR(50))	
	INSERT INTO #ExternalCostReferences (IdModel,IdZiffBaseCost,IdField,IdZiffAccount,IdPeerCriteria,UnitCost,Quantity,TotalCost,Field,CodeZiff,ZiffAccount,IdRootZiffAccount,
										 RootCodeZiff,RootZiffAccount,PeerCriteria,Unit,IdUnitPeerCriteria,TechApplicable,TFNormal,TFPessimistic,TFOptimistic,BCRNormal,
										 BCRPessimistic,BCROptimistic, SortOrder, ExternalAccountCode)	
	SELECT CBCBFZ.IdModel,CBCBFZ.IdZiffBaseCost,CBCBFZ.IdField,CBCBFZ.IdZiffAccount,CBCBFZ.IdPeerCriteria,CBCBFZ.UnitCost,CBCBFZ.Quantity,CBCBFZ.TotalCost,CBCBFZ.Field,
		   '&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;'+CBCBFZ.CodeZiff as CodeZiff,CBCBFZ.ZiffAccount,CBCBFZ.IdRootZiffAccount,CBCBFZ.RootCodeZiff,CBCBFZ.RootZiffAccount,
		   CBCBFZ.PeerCriteria,CBCBFZ.Unit,CBCBFZ.IdUnitPeerCriteria,CBCBFZ.TechApplicable,CBCBFZ.TFNormal,CBCBFZ.TFPessimistic,CBCBFZ.TFOptimistic,CBCBFZ.BCRNormal,
		   CBCBFZ.BCRPessimistic,CBCBFZ.BCROptimistic,ZA.SortOrder, CBCBFZ.CodeZiff AS ExternalAccountCode 
	FROM tblCalcBaseCostByFieldZiff CBCBFZ left outer join tblZiffAccounts ZA on 
		 ZA.IdModel=CBCBFZ.IdModel and ZA.IdZiffAccount=CBCBFZ.IdZiffAccount 
	WHERE CBCBFZ.IdModel = @IdModel AND CBCBFZ.IdField = @IdField 
	UNION 
	SELECT IdModel,NULL, NULL, IdZiffAccount,NULL,-99,NULL,-99,NULL,Code,Name,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,SortOrder,NULL 
	FROM tblZiffAccounts 
	WHERE IdModel = @IdModel 
	AND Parent IS NULL 

	SELECT * FROM #ExternalCostReferences 	ORDER BY SortOrder 
END

USE [DBCPM]
GO
UPDATE tblLabelsLanguages
SET LabelText = 'Mapping (Company Cost Structure vs. Cost Projection Categories)'
WHERE IdLabel = 124 and IdLanguage = 1

UPDATE tblLabelsLanguages
SET LabelText = 'Mapeo (Estructura Empresa  vs Estructura Proyección)'
WHERE IdLabel = 124 and IdLanguage = 2

UPDATE tblLabelsLanguages
SET LabelText = 'Base Cost by Field (Cost Projection Categories)' 
WHERE IdLabel = 125 and IdLanguage = 1	

UPDATE tblLabelsLanguages
SET LabelText = 'Costo Base por Campo (Estructura Proyección)'
WHERE IdLabel = 125 and IdLanguage = 2	

UPDATE tblLabelsLanguages
SET LabelText = 'Mapping (Company Structure vs. Cost Projection Categories)' 
WHERE IdLabel = 131 and IdLanguage = 1	

UPDATE tblLabelsLanguages
SET LabelText = 'Mapeo (Estructura Empresa vs Estructura Proyección)' 
WHERE IdLabel = 131 and IdLanguage = 2	

UPDATE tblLabelsLanguages
SET LabelText = 'Base Cost By Field (Cost Projection Categories)'
WHERE IdLabel = 132 and IdLanguage = 1	

UPDATE tblLabelsLanguages
SET LabelText = 'Costo Base por Campo (Estructura Proyección)'
WHERE IdLabel = 132 and IdLanguage = 2	
USE [DBCPM]
GO
/****** Object:  Table [dbo].[tblParameters]    Script Date: 06/05/2015 10:52:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
UPDATE [dbo].[tblParameters]
SET	[Name] = N'Oportunidades de Negocio'
WHERE[IdParameters] = 37
/****** Object:  StoredProcedure [dbo].[parpBaseCostTechnicalModule]    Script Date: 06/05/2015 10:52:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gareth Slater
-- Create date: 2014-04-22
-- Description:	Technical Base Cost Rates Lookup
-- =============================================
ALTER PROCEDURE [dbo].[parpBaseCostTechnicalModule](
@IdModel int,
@IdField int,
@IdStage int,
@BaseCostType int,
@IdCurrency int,
@IdLanguage int
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	--Original Code (R Manubens 2015/02/23)
	--SELECT * FROM tblCalcTechnicalBaseCostRate
	--WHERE IdModel=@IdModel AND IdField=@IdField AND IdStage=@IdStage AND BaseCostType=@BaseCostType AND ProjectionCriteria<>98
	--ORDER BY SortOrder;
	
	--New Code to include Currency Conversion(R Manubens 2015/02/23)

	/** DROP Temporary Tables **/
	IF OBJECT_ID('tempdb..#tblCalcTechnicalBaseCostRate') IS NOT NULL DROP TABLE #tblCalcTechnicalBaseCostRate
	IF OBJECT_ID('tempdb..#tblParameters') IS NOT NULL DROP TABLE #tblParameters

	/** Get Currency Factor to use **/
	DECLARE @CurrencyFactor FLOAT;
	SELECT @CurrencyFactor=[Value] FROM tblModelsCurrencies WHERE [IdCurrency]=@IdCurrency AND [IdModel]=@IdModel;
	
	CREATE TABLE #tblCalcTechnicalBaseCostRate ([IdModel] INT, [IdZiffAccount] INT, [CodeZiff] VARCHAR(25), [ZiffAccount] VARCHAR(100), [DisplayCode] VARCHAR(255), 
												[SortOrder] VARCHAR(100), [IdField] INT, [IdStage] INT, [BaseCostType] INT, [ProjectionCriteria] INT, 
												[NameProjectionCriteria] VARCHAR(25), CycleValue INT, [SizeName] VARCHAR(100), [SizeValue] INT, [PerformanceName] VARCHAR(100),
												[PerformanceValue] INT, [BaseCost] FLOAT, [BaseCostRate] FLOAT, [IdRootZiffAccount] INT, [RootCodeZiff] VARCHAR(20),
												[RootZiffAccount] VARCHAR(25)) ON [PRIMARY]
	INSERT INTO #tblCalcTechnicalBaseCostRate ([IdModel], [IdZiffAccount], [CodeZiff], [ZiffAccount], [DisplayCode], [SortOrder], [IdField], [IdStage], [BaseCostType], 
											   [ProjectionCriteria], [NameProjectionCriteria], CycleValue, [SizeName], [SizeValue], [PerformanceName],[PerformanceValue], 
											   [BaseCost], [BaseCostRate], [IdRootZiffAccount], [RootCodeZiff],[RootZiffAccount])
	SELECT	IdModel
			, IdZiffAccount
			, CodeZiff
			, ZiffAccount
			, DisplayCode
			, SortOrder
			, IdField
			, IdStage
			, BaseCostType
			, ProjectionCriteria
			, NameProjectionCriteria
			, CycleValue
			, SizeName
			, "SizeValue" = CASE 
								WHEN NameProjectionCriteria != 'SemiFixed' THEN SUM(SizeValue)
								ELSE NULL
						   END
			, PerformanceName
			, SUM(PerformanceValue) AS PerformanceValue
			, SUM(BaseCost) * @CurrencyFactor  AS BaseCost
			, "BaseCostRate" = CASE
									WHEN NameProjectionCriteria != 'Fixed' THEN (BaseCostRate) * @CurrencyFactor
									ELSE NULL
							  END
			, IdRootZiffAccount
			, RootCodeZiff
			, RootZiffAccount
	FROM	tblCalcTechnicalBaseCostRate 
	WHERE	IdModel=@IdModel AND IdField=@IdField AND IdStage=@IdStage AND BaseCostType=@BaseCostType AND ProjectionCriteria<>98
	GROUP BY IdModel
			, IdZiffAccount
			, CodeZiff
			, ZiffAccount
			, DisplayCode
			, SortOrder
			, IdField
			, IdStage
			, BaseCostType
			, ProjectionCriteria
			, NameProjectionCriteria
			, CycleValue
			, SizeName
			, SizeValue
			, PerformanceName
			, PerformanceValue
			, BaseCost
			, BaseCostRate
			, IdRootZiffAccount
			, RootCodeZiff
			, RootZiffAccount
	ORDER BY SortOrder;
	
	CREATE TABLE #tblParameters ([Code] INT, [Name] VARCHAR(50), [IdLanguage] INT)
	INSERT INTO #tblParameters (Code, Name, IdLanguage)
	SELECT Code, Name, IdLanguage
	FROM   tblParameters
	WHERE (Type = 'ProjectionCriteria')
	
	UPDATE #tblCalcTechnicalBaseCostRate
	SET	   NameProjectionCriteria = #tblParameters.Name
	FROM   #tblCalcTechnicalBaseCostRate INNER JOIN #tblParameters on #tblParameters.Code = (SELECT Code FROM #tblParameters WHERE Name = #tblCalcTechnicalBaseCostRate.NameProjectionCriteria AND IdLanguage = 1)
	WHERE  #tblParameters.IdLanguage=@IdLanguage

	SELECT * FROM #tblCalcTechnicalBaseCostRate ORDER BY SortOrder;
		
END
GO
/****** Object:  StoredProcedure [dbo].[parpBaseRatesByDriversTechnicalModule]    Script Date: 06/05/2015 10:52:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-03-19
-- Description:	Base Rates By Drivers Report
-- =============================================
ALTER PROCEDURE [dbo].[parpBaseRatesByDriversTechnicalModule](
@IdModel int,
@IdAggregationLevel INT,
@IdField int,
@IdCurrency INT,
@IdStage int,
@BaseCostType int
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#tblCalcTechnicalBaseCostRate_Temp') IS NOT NULL DROP TABLE #tblCalcTechnicalBaseCostRate_Temp
	IF OBJECT_ID('tempdb..#FieldList') IS NOT NULL DROP TABLE #FieldList
	IF OBJECT_ID('tempdb..#ProjectionCosts') IS NOT NULL DROP TABLE #ProjectionCosts
	IF OBJECT_ID('tempdb..#SizeDriverList') IS NOT NULL DROP TABLE #SizeDriverList
	IF OBJECT_ID('tempdb..#PerformanceDriverList') IS NOT NULL DROP TABLE #PerformanceDriverList
	IF OBJECT_ID('tempdb..#BaseCostRatePerDriverList') IS NOT NULL DROP TABLE #BaseCostRatePerDriverList

	/** Build Field List **/
	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL, Name NVARCHAR(255)) ON [PRIMARY]
	IF (@IdAggregationLevel>0 AND @IdField>0)
	BEGIN
		INSERT INTO #FieldList SELECT A.[IdField], B.Name FROM tblCalcTechnicalBaseCostRate A INNER JOIN tblFields B ON A.IdField=B.IdField WHERE A.[IdModel]=@IdModel AND A.IdField = @IdField GROUP BY A.[IdField], B.Name; 
	END
	ELSE IF (@IdAggregationLevel>0 AND @IdField=0)
	BEGIN
		INSERT INTO #FieldList SELECT A.[IdField], B.Name FROM tblCalcTechnicalBaseCostRate A INNER JOIN tblFields B ON A.IdField=B.IdField WHERE A.[IdModel]=@IdModel  AND A.IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=@IdAggregationLevel) GROUP BY A.[IdField], B.Name; 
	END
	ELSE IF (@IdAggregationLevel=0 AND @IdField>0) 
	BEGIN
		INSERT INTO #FieldList SELECT A.[IdField], B.Name FROM tblCalcTechnicalBaseCostRate A INNER JOIN tblFields B ON A.IdField=B.IdField WHERE A.[IdModel]=@IdModel  AND A.IdField = @IdField GROUP BY A.[IdField], B.Name; 
	END
	ELSE IF (@IdAggregationLevel=0 AND @IdField=0)
	BEGIN
		INSERT INTO #FieldList SELECT A.[IdField], B.Name FROM tblCalcTechnicalBaseCostRate A INNER JOIN tblFields B ON A.IdField=B.IdField WHERE A.[IdModel]=@IdModel GROUP BY A.[IdField], B.Name;
	END

	/** Build Field Column **/
	DECLARE @FieldColumnList NVARCHAR(MAX)='';
	DECLARE @FieldName NVARCHAR(255);
	DECLARE FieldList CURSOR FOR
	SELECT Name FROM #FieldList;

	OPEN FieldList;
	FETCH NEXT FROM FieldList INTO @FieldName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @FieldColumnList=''
		BEGIN
			SET @FieldColumnList =  @FieldColumnList + '[' + @FieldName  + ']';
		END
		ELSE
		BEGIN
			SET @FieldColumnList =  @FieldColumnList + ',[' + @FieldName  + ']';
		END
		FETCH NEXT FROM FieldList INTO @FieldName;
	END

	CLOSE FieldList;
	DEALLOCATE FieldList;

	/** Get Currency Factor to use **/
	DECLARE @CurrencyFactor FLOAT;
	SELECT @CurrencyFactor=[Value] FROM tblModelsCurrencies WHERE [IdCurrency]=@IdCurrency AND [IdModel]=@IdModel;

	/** Build Temporary tblCalcTechnicalBaseCostRate table **/
	CREATE TABLE #tblCalcTechnicalBaseCostRate_Temp (IdModel INT, IdZiffAccount INT, CodeZiff VARCHAR(50), ZiffAccount VARCHAR(150), 
													 DisplayCode NVARCHAR(MAX), SortOrder NVARCHAR(MAX), IdField INT, IdStage INT,
													 BaseCostType INT, ProjectionCriteria INT, NameProjectionCriteria VARCHAR(100), 
													 CycleValue INT, SizeName VARCHAR(150), SizeValue FLOAT, PerformanceName VARCHAR(150), 
													 PerformanceValue FLOAT, BaseCost FLOAT, BaseCostRate FLOAT, IdRootZiffAccount INT, 
													 RootCodeZiff NVARCHAR(50), RootZiffAccount NVARCHAR(255))
	INSERT INTO #tblCalcTechnicalBaseCostRate_Temp SELECT	IdModel
			, IdZiffAccount
			, CodeZiff
			, ZiffAccount
			, DisplayCode
			, SortOrder
			, IdField
			, IdStage
			, BaseCostType
			, ProjectionCriteria
			, NameProjectionCriteria
			, CycleValue
			, SizeName
			, SizeValue
			, PerformanceName
			, PerformanceValue
			, BaseCost
			, BaseCostRate
			, IdRootZiffAccount
			, RootCodeZiff
			, RootZiffAccount
	FROM	tblCalcTechnicalBaseCostRate INNER JOIN tblParameters on tblParameters.Name = tblCalcTechnicalBaseCostRate.NameProjectionCriteria
	WHERE	IdModel=@IdModel AND IdStage=@IdStage AND BaseCostType=@BaseCostType AND IdLanguage=1
   GROUP BY IdModel
			, IdZiffAccount
			, CodeZiff
			, ZiffAccount
			, DisplayCode
			, SortOrder
			, IdField
			, IdStage
			, BaseCostType
			, ProjectionCriteria
			, NameProjectionCriteria
			, CycleValue
			, SizeName
			, SizeValue
			, PerformanceName
			, PerformanceValue
			, BaseCost
			, BaseCostRate
			, IdRootZiffAccount
			, RootCodeZiff
			, RootZiffAccount
	ORDER BY SortOrder;

	/** Build projection costs list **/
	CREATE TABLE #ProjectionCosts (FieldName NVARCHAR(255), Name NVARCHAR(255), BaseCost FLOAT)
	INSERT INTO #ProjectionCosts SELECT B.Name, A.NameProjectionCriteria + ' Cost', SUM(A.BaseCost) * @CurrencyFactor AS BaseCost
	FROM  #tblCalcTechnicalBaseCostRate_Temp A INNER JOIN #FieldList B ON A.IdField = B.IdField
	WHERE A.BaseCost IS NOT NULL
	GROUP BY B.Name, A.NameProjectionCriteria

	/** Build Size Driver List **/
	CREATE TABLE #SizeDriverList (FieldName NVARCHAR(255), Name NVARCHAR(255), SizeValue FLOAT)
	INSERT INTO #SizeDriverList SELECT B.Name, A.SizeName, A.SizeValue
	FROM  #tblCalcTechnicalBaseCostRate_Temp A INNER JOIN 
		  #FieldList B ON A.IdField = B.IdField
	where A.SizeName is not null and A.Sizevalue is not null
	GROUP BY /*A.IdField,*/ B.Name, A.SizeName, A.SizeValue

	/** Build Performance Driver List **/
	CREATE TABLE #PerformanceDriverList (FieldName NVARCHAR(255), Name NVARCHAR(255), PerformanceValue FLOAT)
	INSERT INTO #PerformanceDriverList SELECT  B.Name, A.PerformanceName, sum(A.PerformanceValue) * @CurrencyFactor
	FROM  #tblCalcTechnicalBaseCostRate_Temp A INNER JOIN 
		  #FieldList B ON A.IdField = B.IdField
	where A.PerformanceName is not null and A.PerformanceValue is not null
	GROUP BY /*A.IdField,*/ B.Name, A.PerformanceName

	/** Build Base Cost Rate per Driver List **/
	CREATE TABLE #BaseCostRatePerDriverList (FieldName NVARCHAR(255), Name NVARCHAR(255), BaseCostRate FLOAT)
	INSERT INTO #BaseCostRatePerDriverList SELECT B.Name, A.SizeName, sum(A.BaseCostRate) * @CurrencyFactor
	FROM  #tblCalcTechnicalBaseCostRate_Temp A INNER JOIN 
		  #FieldList B ON A.IdField = B.IdField
	where A.SizeName is not null and A.SizeValue is not null
	GROUP BY /*A.IdField,*/ B.Name, A.SizeName


	--/** Build Output Table **/
	DECLARE @SQLProjectionCosts NVARCHAR(MAX);
	SET @SQLProjectionCosts = 'SELECT * FROM #ProjectionCosts PIVOT (Sum(BaseCost) FOR FieldName IN (' + @FieldColumnList + ')) AS PivotTable';
	EXEC(@SQLProjectionCosts);

	DECLARE @SQLSizeDriverList NVARCHAR(MAX);
	SET @SQLSizeDriverList = 'SELECT * FROM #SizeDriverList PIVOT (Sum(SizeValue) FOR FieldName IN (' + @FieldColumnList + ')) AS PivotTable';
	EXEC(@SQLSizeDriverList);

	DECLARE @SQLBaseCostRatePerDriverList NVARCHAR(MAX);
	SET @SQLBaseCostRatePerDriverList = 'SELECT * FROM #BaseCostRatePerDriverList PIVOT (Sum(BaseCostRate) FOR FieldName IN (' + @FieldColumnList + ')) AS PivotTable';
	EXEC(@SQLBaseCostRatePerDriverList);

 END
GO
/****** Object:  View [dbo].[vListProjects]    Script Date: 06/05/2015 10:52:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DROP VIEW [dbo].[vListProjects]
GO
CREATE VIEW [dbo].[vListProjects]
AS
SELECT     A.IdProject, A.IdModel, A.Code, A.Name, A.IdField, A.Operation, A.Starts, A.TypeAllocation, A.UserCreation, A.DateCreation, A.UserModification, A.DateModification, 
                      C.Name AS Field, B.Name AS OperationName, C.Code AS FieldCode, B.IdLanguage
FROM         dbo.tblProjects AS A INNER JOIN
                      dbo.tblFields AS C ON C.IdField = A.IdField INNER JOIN
                      dbo.tblParameters AS B ON B.Code = A.Operation AND B.Type = 'OperationProject'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "A"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 207
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "C"
            Begin Extent = 
               Top = 6
               Left = 245
               Bottom = 239
               Right = 428
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "B"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 274
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vListProjects'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vListProjects'
GO
/****** Object:  StoredProcedure [dbo].[calcModuleTechnicalForecast]    Script Date: 06/05/2015 10:52:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gareth Slater
-- Create date: 2014-04-20
-- Description:	Module Technical Forecast
-- =============================================
ALTER PROCEDURE [dbo].[calcModuleTechnicalForecast](
@IdModel int,
@MinYear int,
@MaxYear int
)
AS
BEGIN
	
	/** Cleanup Calc Tables for New Data **/
	DELETE FROM tblCalcTechnicalSemifixedSelections WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalSemifixedSplit WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcProjectionTechnical WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalCostByField WHERE [IdModel]=@IdModel;
	
	/** Set Allowed Combinations for Semi-Fixed Assumptions (Shared) **/
	INSERT INTO tblCalcTechnicalSemifixedSelections ([IdModel],[IdField],[IdZiffAccount],[IdTechnicalDriverSize],[TypeAllocation])
	SELECT tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.IdZiffAccount, 
		tblCalcTechnicalAssumption.IdTechnicalDriverSize, tblCalcBaseCostByField.TypeAllocation
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalAssumption.IdModel AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalAssumption.IdZiffAccount INNER JOIN
		tblFields ON tblCalcBaseCostByField.IdField = tblFields.IdField AND tblCalcBaseCostByField.IdModel = tblFields.IdModel AND 
		tblCalcTechnicalAssumption.IdTypeOperation = tblFields.IdTypeOperation
	WHERE tblCalcTechnicalAssumption.ProjectionCriteria=2
	GROUP BY tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.TypeAllocation, 
		tblCalcTechnicalAssumption.IdTechnicalDriverSize
	HAVING tblCalcBaseCostByField.TypeAllocation=2 AND tblCalcBaseCostByField.IdModel=@IdModel;
	
	/** Set Allowed Combinations for Semi-Fixed Assumptions (Hierarchy) **/
	INSERT INTO tblCalcTechnicalSemifixedSelections ([IdModel],[IdField],[IdZiffAccount],[IdTechnicalDriverSize],[TypeAllocation])
	SELECT tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.IdZiffAccount, 
		tblCalcTechnicalAssumption.IdTechnicalDriverSize, tblCalcBaseCostByField.TypeAllocation
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalAssumption.IdModel AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalAssumption.IdZiffAccount INNER JOIN
		tblFields ON tblCalcBaseCostByField.IdField = tblFields.IdField AND tblCalcBaseCostByField.IdModel = tblFields.IdModel AND 
		tblCalcTechnicalAssumption.IdTypeOperation = tblFields.IdTypeOperation
	WHERE tblCalcTechnicalAssumption.ProjectionCriteria=2
	GROUP BY tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.TypeAllocation, 
		tblCalcTechnicalAssumption.IdTechnicalDriverSize
	HAVING tblCalcBaseCostByField.TypeAllocation=3 AND tblCalcBaseCostByField.IdModel=@IdModel;
	
	/** Update Ziff Semi-Fixed Forecast to Fixed **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET [ProjectionCriteria]=1 WHERE [ProjectionCriteria]=2 AND [IdModel]=@IdModel;
	
	/** Insert Company Base Year Into tblCalcProjectionTechnical **/
	INSERT INTO tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 1, tblCalcBaseCostByField.IdModel,tblCalcBaseCostByField.IdActivity,tblCalcBaseCostByField.CodeAct,tblCalcBaseCostByField.Activity,tblCalcBaseCostByField.IdResources,tblCalcBaseCostByField.CodeRes,tblCalcBaseCostByField.Resource,tblCalcBaseCostByField.IdClientAccount,tblCalcBaseCostByField.Account,tblCalcBaseCostByField.NameAccount,tblCalcBaseCostByField.IdField,tblCalcBaseCostByField.Field,tblCalcBaseCostByField.IdClientCostCenter,tblCalcBaseCostByField.ClientCostCenter,tblCalcBaseCostByField.Amount,tblCalcBaseCostByField.TypeAllocation,tblCalcBaseCostByField.IdZiffAccount,tblCalcBaseCostByField.CodeZiff,tblCalcBaseCostByField.ZiffAccount,tblCalcBaseCostByField.IdRootZiffAccount,[RootCodeZiff],[RootZiffAccount],tblCalcTechnicalDriverAssumption.IdProject,tblCalcTechnicalDriverAssumption.ProjectionCriteria,tblCalcTechnicalDriverAssumption.Year,tblCalcTechnicalDriverAssumption.BaseYear,tblCalcTechnicalDriverAssumption.TFNormal,tblCalcTechnicalDriverAssumption.TFPessimistic,tblCalcTechnicalDriverAssumption.TFOptimistic,tblCalcBaseCostByField.BCRNormal,tblCalcBaseCostByField.BCRPessimistic,tblCalcBaseCostByField.BCROptimistic,1 AS BaseCostType,tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount
	WHERE tblCalcTechnicalDriverAssumption.BaseYear=1 AND tblCalcBaseCostByField.IdModel=@IdModel;
	
	/** Insert Ziff Base Year Into tblCalcProjectionTechnical (If Applicable) **/
	INSERT INTO tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 2, tblCalcBaseCostByFieldZiff.IdModel,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,tblCalcBaseCostByFieldZiff.IdField,tblCalcBaseCostByFieldZiff.Field,NULL,NULL,tblCalcBaseCostByFieldZiff.TotalCost,NULL,tblCalcBaseCostByFieldZiff.IdZiffAccount,tblCalcBaseCostByFieldZiff.CodeZiff,tblCalcBaseCostByFieldZiff.ZiffAccount,tblCalcBaseCostByFieldZiff.IdRootZiffAccount,[RootCodeZiff],[RootZiffAccount],tblCalcTechnicalDriverAssumptionZiff.IdProject,tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria,tblCalcTechnicalDriverAssumptionZiff.Year,tblCalcTechnicalDriverAssumptionZiff.BaseYear,tblCalcTechnicalDriverAssumptionZiff.TFNormal,tblCalcTechnicalDriverAssumptionZiff.TFPessimistic,tblCalcTechnicalDriverAssumptionZiff.TFOptimistic,tblCalcBaseCostByFieldZiff.BCRNormal,tblCalcBaseCostByFieldZiff.BCRPessimistic,tblCalcBaseCostByFieldZiff.BCROptimistic,2 AS BaseCostType,tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount
	WHERE tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 
	AND tblCalcBaseCostByFieldZiff.IdModel=@IdModel;
	
	/** Setup Semi-Fixed Driver Splits (Shared) **/
	INSERT INTO tblCalcTechnicalSemifixedSplit ([IdModel],[Year],[IdZiffAccount],[IdField],[IdProject],[IdTechnicalDriverSize],[TypeAllocation],[SplitNormal],[SplitPessimistic],[SplitOptimistic])
	SELECT DetailsLookup.IdModel, DetailsLookup.Year, DetailsLookup.IdZiffAccount, DetailsLookup.IdField, DetailsLookup.IdProject, DetailsLookup.IdTechnicalDriver, 2 AS TypeAllocation, 
		ISNULL(DetailsLookup.SizeNormal/SummaryLookup.SizeNormal,0) AS SplitNormal, 
		ISNULL(DetailsLookup.SizePessimistic/SummaryLookup.SizePessimistic,0) AS SplitPessimistic, 
		ISNULL(DetailsLookup.SizeOptimistic/SummaryLookup.SizeOptimistic,0) AS SplitOptimistic
	FROM (
		SELECT tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalDriverData.Year, tblCalcTechnicalDriverData.IdField, 
			tblCalcTechnicalDriverData.IdProject, tblCalcTechnicalSemifixedSelections.IdZiffAccount, tblCalcTechnicalDriverData.IdTechnicalDriver, 
			SUM(tblCalcTechnicalDriverData.Normal) AS SizeNormal, SUM(tblCalcTechnicalDriverData.Pessimistic) AS SizePessimistic, 
			SUM(tblCalcTechnicalDriverData.Optimistic) AS SizeOptimistic
		FROM tblCalcTechnicalSemifixedSelections INNER JOIN
			tblCalcTechnicalDriverData ON tblCalcTechnicalSemifixedSelections.IdModel = tblCalcTechnicalDriverData.IdModel AND 
			tblCalcTechnicalSemifixedSelections.IdField = tblCalcTechnicalDriverData.IdField INNER JOIN
			(
			SELECT IdModel, IdTechnicalDriverSize, IdZiffAccount, IdField, IdProject, Year
			FROM tblCalcTechnicalDriverAssumption
			WHERE ProjectionCriteria=2
			GROUP BY IdModel, IdTechnicalDriverSize, IdZiffAccount, IdField, IdProject, Year
			HAVING IdModel=@IdModel
			) AS SelectionList ON tblCalcTechnicalDriverData.IdModel = SelectionList.IdModel AND 
			tblCalcTechnicalDriverData.IdTechnicalDriver = SelectionList.IdTechnicalDriverSize AND tblCalcTechnicalDriverData.IdField = SelectionList.IdField AND 
			tblCalcTechnicalDriverData.IdProject = SelectionList.IdProject AND tblCalcTechnicalDriverData.Year = SelectionList.Year AND 
			tblCalcTechnicalSemifixedSelections.IdZiffAccount = SelectionList.IdZiffAccount
		WHERE tblCalcTechnicalSemifixedSelections.TypeAllocation=2
		GROUP BY tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalDriverData.Year, tblCalcTechnicalSemifixedSelections.IdZiffAccount, 
			tblCalcTechnicalDriverData.IdTechnicalDriver, tblCalcTechnicalDriverData.IdField, tblCalcTechnicalDriverData.IdProject
		HAVING tblCalcTechnicalDriverData.Year<>@MinYear AND tblCalcTechnicalSemifixedSelections.IdModel=@IdModel
		) AS DetailsLookup LEFT OUTER JOIN
		(
		SELECT tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalDriverData.Year, tblCalcTechnicalSemifixedSelections.IdZiffAccount, 
			tblCalcTechnicalDriverData.IdTechnicalDriver, SUM(tblCalcTechnicalDriverData.Normal) AS SizeNormal, 
			SUM(tblCalcTechnicalDriverData.Pessimistic) AS SizePessimistic, SUM(tblCalcTechnicalDriverData.Optimistic) AS SizeOptimistic
		FROM tblCalcTechnicalSemifixedSelections INNER JOIN
			tblCalcTechnicalDriverData ON tblCalcTechnicalSemifixedSelections.IdModel = tblCalcTechnicalDriverData.IdModel AND 
			tblCalcTechnicalSemifixedSelections.IdField = tblCalcTechnicalDriverData.IdField INNER JOIN
			(
			SELECT IdModel, IdTechnicalDriverSize, IdZiffAccount, IdField, IdProject, Year
			FROM tblCalcTechnicalDriverAssumption
			WHERE ProjectionCriteria=2
			GROUP BY IdModel, IdTechnicalDriverSize, IdZiffAccount, IdField, IdProject, Year
			HAVING IdModel=@IdModel
			) AS SelectionList ON tblCalcTechnicalDriverData.IdModel = SelectionList.IdModel AND 
			tblCalcTechnicalDriverData.IdTechnicalDriver = SelectionList.IdTechnicalDriverSize AND tblCalcTechnicalDriverData.IdField = SelectionList.IdField AND 
			tblCalcTechnicalDriverData.IdProject = SelectionList.IdProject AND tblCalcTechnicalDriverData.Year = SelectionList.Year AND 
			tblCalcTechnicalSemifixedSelections.IdZiffAccount = SelectionList.IdZiffAccount
		WHERE tblCalcTechnicalSemifixedSelections.TypeAllocation=2
		GROUP BY tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalDriverData.Year, tblCalcTechnicalSemifixedSelections.IdZiffAccount, 
			tblCalcTechnicalDriverData.IdTechnicalDriver
		HAVING tblCalcTechnicalDriverData.Year<>@MinYear AND tblCalcTechnicalSemifixedSelections.IdModel=@IdModel
		) AS SummaryLookup ON DetailsLookup.IdModel = SummaryLookup.IdModel AND 
		DetailsLookup.Year = SummaryLookup.Year AND DetailsLookup.IdZiffAccount = SummaryLookup.IdZiffAccount AND 
		DetailsLookup.IdTechnicalDriver = SummaryLookup.IdTechnicalDriver
	
	/** Setup Semi-Fixed Driver Splits (Hierarchy) **/
	INSERT INTO tblCalcTechnicalSemifixedSplit ([IdModel],[Year],[IdZiffAccount],[IdField],[IdProject],[IdTechnicalDriverSize],[TypeAllocation],[SplitNormal],[SplitPessimistic],[SplitOptimistic])
	SELECT DetailsLookup.IdModel, DetailsLookup.Year, DetailsLookup.IdZiffAccount, DetailsLookup.IdField, DetailsLookup.IdProject, DetailsLookup.IdTechnicalDriver, 3 AS TypeAllocation, 
		ISNULL(DetailsLookup.SizeNormal/SummaryLookup.SizeNormal,0) AS SplitNormal, 
		ISNULL(DetailsLookup.SizePessimistic/SummaryLookup.SizePessimistic,0) AS SplitPessimistic, 
		ISNULL(DetailsLookup.SizeOptimistic/SummaryLookup.SizeOptimistic,0) AS SplitOptimistic
	FROM (
		SELECT tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalDriverData.Year, tblCalcTechnicalDriverData.IdField, 
			tblCalcTechnicalDriverData.IdProject, tblCalcTechnicalSemifixedSelections.IdZiffAccount, tblCalcTechnicalDriverData.IdTechnicalDriver, 
			SUM(tblCalcTechnicalDriverData.Normal) AS SizeNormal, SUM(tblCalcTechnicalDriverData.Pessimistic) AS SizePessimistic, 
			SUM(tblCalcTechnicalDriverData.Optimistic) AS SizeOptimistic
		FROM tblCalcTechnicalSemifixedSelections INNER JOIN
			tblCalcTechnicalDriverData ON tblCalcTechnicalSemifixedSelections.IdModel = tblCalcTechnicalDriverData.IdModel AND 
			tblCalcTechnicalSemifixedSelections.IdField = tblCalcTechnicalDriverData.IdField INNER JOIN
			(
			SELECT IdModel, IdTechnicalDriverSize, IdZiffAccount, IdField, IdProject, Year
			FROM tblCalcTechnicalDriverAssumption
			WHERE ProjectionCriteria=2
			GROUP BY IdModel, IdTechnicalDriverSize, IdZiffAccount, IdField, IdProject, Year
			HAVING IdModel=@IdModel
			) AS SelectionList ON tblCalcTechnicalDriverData.IdModel = SelectionList.IdModel AND 
			tblCalcTechnicalDriverData.IdTechnicalDriver = SelectionList.IdTechnicalDriverSize AND tblCalcTechnicalDriverData.IdField = SelectionList.IdField AND 
			tblCalcTechnicalDriverData.IdProject = SelectionList.IdProject AND tblCalcTechnicalDriverData.Year = SelectionList.Year AND 
			tblCalcTechnicalSemifixedSelections.IdZiffAccount = SelectionList.IdZiffAccount
		WHERE tblCalcTechnicalSemifixedSelections.TypeAllocation=3
		GROUP BY tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalDriverData.Year, tblCalcTechnicalSemifixedSelections.IdZiffAccount, 
			tblCalcTechnicalDriverData.IdTechnicalDriver, tblCalcTechnicalDriverData.IdField, tblCalcTechnicalDriverData.IdProject
		HAVING tblCalcTechnicalDriverData.Year<>@MinYear AND tblCalcTechnicalSemifixedSelections.IdModel=@IdModel
		) AS DetailsLookup LEFT OUTER JOIN
		(
		SELECT tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalDriverData.Year, tblCalcTechnicalSemifixedSelections.IdZiffAccount, 
			tblCalcTechnicalDriverData.IdTechnicalDriver, SUM(tblCalcTechnicalDriverData.Normal) AS SizeNormal, 
			SUM(tblCalcTechnicalDriverData.Pessimistic) AS SizePessimistic, SUM(tblCalcTechnicalDriverData.Optimistic) AS SizeOptimistic
		FROM tblCalcTechnicalSemifixedSelections INNER JOIN
			tblCalcTechnicalDriverData ON tblCalcTechnicalSemifixedSelections.IdModel = tblCalcTechnicalDriverData.IdModel AND 
			tblCalcTechnicalSemifixedSelections.IdField = tblCalcTechnicalDriverData.IdField INNER JOIN
			(
			SELECT IdModel, IdTechnicalDriverSize, IdZiffAccount, IdField, IdProject, Year
			FROM tblCalcTechnicalDriverAssumption
			WHERE ProjectionCriteria=2
			GROUP BY IdModel, IdTechnicalDriverSize, IdZiffAccount, IdField, IdProject, Year
			HAVING IdModel=@IdModel
			) AS SelectionList ON tblCalcTechnicalDriverData.IdModel = SelectionList.IdModel AND 
			tblCalcTechnicalDriverData.IdTechnicalDriver = SelectionList.IdTechnicalDriverSize AND tblCalcTechnicalDriverData.IdField = SelectionList.IdField AND 
			tblCalcTechnicalDriverData.IdProject = SelectionList.IdProject AND tblCalcTechnicalDriverData.Year = SelectionList.Year AND 
			tblCalcTechnicalSemifixedSelections.IdZiffAccount = SelectionList.IdZiffAccount
		WHERE tblCalcTechnicalSemifixedSelections.TypeAllocation=3
		GROUP BY tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalDriverData.Year, tblCalcTechnicalSemifixedSelections.IdZiffAccount, 
			tblCalcTechnicalDriverData.IdTechnicalDriver
		HAVING tblCalcTechnicalDriverData.Year<>@MinYear AND tblCalcTechnicalSemifixedSelections.IdModel=@IdModel
		) AS SummaryLookup ON DetailsLookup.IdModel = SummaryLookup.IdModel AND 
		DetailsLookup.Year = SummaryLookup.Year AND DetailsLookup.IdZiffAccount = SummaryLookup.IdZiffAccount AND 
		DetailsLookup.IdTechnicalDriver = SummaryLookup.IdTechnicalDriver
	
	/** Mark Semi-Fixed Years to keep **/
	UPDATE tblCalcTechnicalSemifixedSplit
	SET SemifixedForecast=1
	FROM tblCalcTechnicalSemifixedSplit INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcTechnicalSemifixedSplit.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcTechnicalSemifixedSplit.Year = tblCalcTechnicalDriverAssumption.Year AND 
		tblCalcTechnicalSemifixedSplit.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount AND 
		tblCalcTechnicalSemifixedSplit.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
		tblCalcTechnicalSemifixedSplit.IdProject = tblCalcTechnicalDriverAssumption.IdProject AND 
		tblCalcTechnicalSemifixedSplit.IdTechnicalDriverSize = tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize
	WHERE tblCalcTechnicalSemifixedSplit.IdModel=@IdModel;
	
	/** Delete Semi-Fixed Years to drop **/
	DELETE FROM tblCalcTechnicalSemifixedSplit WHERE [SemifixedForecast] IS NULL AND IdModel=@IdModel;
	
	/** Update Company Projection - Semi-Fixed (Direct Portion) **/
	INSERT INTO tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 3, SemifixedSplit.IdModel, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.CodeAct, tblCalcBaseCostByField.Activity, 
		tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.CodeRes, tblCalcBaseCostByField.Resource, 
		tblCalcBaseCostByField.IdClientAccount, tblCalcBaseCostByField.Account, tblCalcBaseCostByField.NameAccount, tblCalcBaseCostByField.IdField, 
		tblCalcBaseCostByField.Field, tblCalcBaseCostByField.IdClientCostCenter, tblCalcBaseCostByField.ClientCostCenter, 
		tblCalcBaseCostByField.Amount, tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.CodeZiff, 
		tblCalcBaseCostByField.ZiffAccount, tblCalcBaseCostByField.IdRootZiffAccount, tblCalcBaseCostByField.RootCodeZiff, 
		tblCalcBaseCostByField.RootZiffAccount, tblProjects.IdProject, 2 AS ProjectionCriteria, SemifixedSplit.Year, 0 AS BaseYear, 1 AS TFNormal, 
		1 AS TFPessimistic, 1 AS TFOptimistic, tblCalcBaseCostByField.Amount AS BCRNormal, tblCalcBaseCostByField.Amount AS BCRPessimistic, 
		tblCalcBaseCostByField.Amount AS BCROptimistic, 1 AS BaseCostType, tblProjects.Operation
	FROM tblCalcBaseCostByField INNER JOIN
		(
		SELECT [IdModel],[Year],[IdField],[IdZiffAccount] FROM tblCalcTechnicalSemifixedSplit WHERE [Year]<>@MinYear AND [IdModel]=@IdModel GROUP BY [IdModel],[Year],[IdField],[IdZiffAccount]
		) AS SemifixedSplit ON tblCalcBaseCostByField.IdModel = SemifixedSplit.IdModel AND 
		tblCalcBaseCostByField.IdZiffAccount = SemifixedSplit.IdZiffAccount AND tblCalcBaseCostByField.IdField = SemifixedSplit.IdField INNER JOIN
		tblProjects ON tblCalcBaseCostByField.IdModel = tblProjects.IdModel AND tblCalcBaseCostByField.IdField = tblProjects.IdField
	WHERE tblCalcBaseCostByField.TypeAllocation=1 AND tblProjects.Operation=1 AND SemifixedSplit.IdModel=@IdModel;
	
	/** Update Company Projection - Semi-Fixed (Shared) **/
	INSERT INTO tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 4, tblCalcTechnicalSemifixedSplit.IdModel, TotalLookup.IdActivity, TotalLookup.CodeAct, TotalLookup.Activity, TotalLookup.IdResources, TotalLookup.CodeRes, 
		TotalLookup.Resource, TotalLookup.IdClientAccount, TotalLookup.Account, TotalLookup.NameAccount, tblCalcTechnicalSemifixedSplit.IdField, 
		tblFields.Name AS Field, TotalLookup.IdClientCostCenter, TotalLookup.ClientCostCenter, TotalLookup.Amount, TotalLookup.TypeAllocation, 
		TotalLookup.IdZiffAccount, TotalLookup.CodeZiff, TotalLookup.ZiffAccount, TotalLookup.IdRootZiffAccount, TotalLookup.RootCodeZiff, TotalLookup.RootZiffAccount, 
		tblCalcTechnicalSemifixedSplit.IdProject, TotalLookup.ProjectionCriteria, tblCalcTechnicalSemifixedSplit.Year, TotalLookup.BaseYear, tblCalcTechnicalSemifixedSplit.SplitNormal AS TFNormal, 
		tblCalcTechnicalSemifixedSplit.SplitPessimistic AS TFPessimistic, tblCalcTechnicalSemifixedSplit.SplitOptimistic AS TFOptimistic, 
		TotalLookup.Amount AS BCRNormal, TotalLookup.Amount AS BCRPessimistic, TotalLookup.Amount AS BCROptimistic, 1 AS BaseCostType, TotalLookup.Operation
	FROM tblCalcTechnicalSemifixedSplit INNER JOIN
		(SELECT IdModel, IdActivity, CodeAct, Activity, IdResources, CodeRes, Resource, IdClientAccount, Account, NameAccount, IdClientCostCenter, ClientCostCenter, 
		SUM(Amount) AS Amount, TypeAllocation, IdZiffAccount, CodeZiff, ZiffAccount, IdRootZiffAccount, RootCodeZiff, RootZiffAccount, ProjectionCriteria, Year, 
		0 AS BaseYear, BaseCostType, Operation
		FROM tblCalcProjectionTechnical
		WHERE IdModel=@IdModel
		GROUP BY IdModel, IdActivity, CodeAct, Activity, IdResources, CodeRes, Resource, IdClientAccount, Account, NameAccount, IdClientCostCenter, ClientCostCenter, 
		TypeAllocation, IdZiffAccount, CodeZiff, ZiffAccount, IdRootZiffAccount, RootCodeZiff, RootZiffAccount, ProjectionCriteria, Year, BaseCostType, Operation
		HAVING [TypeAllocation]=2 AND [Year]=@MinYear AND [ProjectionCriteria]=2
		) AS TotalLookup ON 
		tblCalcTechnicalSemifixedSplit.IdModel = TotalLookup.IdModel AND tblCalcTechnicalSemifixedSplit.IdZiffAccount = TotalLookup.IdZiffAccount INNER JOIN
		tblFields ON tblCalcTechnicalSemifixedSplit.IdField = tblFields.IdField AND 
		tblCalcTechnicalSemifixedSplit.IdModel = tblFields.IdModel INNER JOIN
		tblCalcTechnicalSemifixedSelections ON tblCalcTechnicalSemifixedSplit.IdModel = tblCalcTechnicalSemifixedSelections.IdModel AND 
		tblCalcTechnicalSemifixedSplit.IdField = tblCalcTechnicalSemifixedSelections.IdField AND 
		tblCalcTechnicalSemifixedSplit.IdZiffAccount = tblCalcTechnicalSemifixedSelections.IdZiffAccount AND 
		tblCalcTechnicalSemifixedSplit.IdTechnicalDriverSize = tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize AND
		tblCalcTechnicalSemifixedSplit.TypeAllocation = tblCalcTechnicalSemifixedSelections.TypeAllocation
	WHERE tblCalcTechnicalSemifixedSplit.TypeAllocation=2 AND tblCalcTechnicalSemifixedSplit.Year<>@MinYear AND tblCalcTechnicalSemifixedSplit.IdModel=@IdModel;
	
	/** Update Company Projection - Semi-Fixed (Hierarchy) **/
	INSERT INTO tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 5, tblCalcTechnicalSemifixedSplit.IdModel, TotalLookup.IdActivity, TotalLookup.CodeAct, TotalLookup.Activity, TotalLookup.IdResources, TotalLookup.CodeRes, 
		TotalLookup.Resource, TotalLookup.IdClientAccount, TotalLookup.Account, TotalLookup.NameAccount, tblCalcTechnicalSemifixedSplit.IdField, 
		tblFields.Name AS Field, TotalLookup.IdClientCostCenter, TotalLookup.ClientCostCenter, TotalLookup.Amount, TotalLookup.TypeAllocation, 
		TotalLookup.IdZiffAccount, TotalLookup.CodeZiff, TotalLookup.ZiffAccount, TotalLookup.IdRootZiffAccount, TotalLookup.RootCodeZiff, TotalLookup.RootZiffAccount, 
		tblCalcTechnicalSemifixedSplit.IdProject, TotalLookup.ProjectionCriteria, tblCalcTechnicalSemifixedSplit.Year, TotalLookup.BaseYear, tblCalcTechnicalSemifixedSplit.SplitNormal AS TFNormal, 
		tblCalcTechnicalSemifixedSplit.SplitPessimistic AS TFPessimistic, tblCalcTechnicalSemifixedSplit.SplitOptimistic AS TFOptimistic, 
		TotalLookup.Amount AS BCRNormal, TotalLookup.Amount AS BCRPessimistic, TotalLookup.Amount AS BCROptimistic, 1 AS BaseCostType, TotalLookup.Operation
	FROM tblCalcTechnicalSemifixedSplit INNER JOIN
		(SELECT IdModel, IdActivity, CodeAct, Activity, IdResources, CodeRes, Resource, IdClientAccount, Account, NameAccount, IdClientCostCenter, ClientCostCenter, 
		SUM(Amount) AS Amount, TypeAllocation, IdZiffAccount, CodeZiff, ZiffAccount, IdRootZiffAccount, RootCodeZiff, RootZiffAccount, ProjectionCriteria, Year, 
		0 AS BaseYear, BaseCostType, Operation
		FROM tblCalcProjectionTechnical
		WHERE IdModel=@IdModel
		GROUP BY IdModel, IdActivity, CodeAct, Activity, IdResources, CodeRes, Resource, IdClientAccount, Account, NameAccount, IdClientCostCenter, ClientCostCenter, 
		TypeAllocation, IdZiffAccount, CodeZiff, ZiffAccount, IdRootZiffAccount, RootCodeZiff, RootZiffAccount, ProjectionCriteria, Year, BaseCostType, Operation
		HAVING [TypeAllocation]=3 AND [Year]=@MinYear AND [ProjectionCriteria]=2
		) AS TotalLookup ON 
		tblCalcTechnicalSemifixedSplit.IdModel = TotalLookup.IdModel AND tblCalcTechnicalSemifixedSplit.IdZiffAccount = TotalLookup.IdZiffAccount INNER JOIN
		tblFields ON tblCalcTechnicalSemifixedSplit.IdField = tblFields.IdField AND 
		tblCalcTechnicalSemifixedSplit.IdModel = tblFields.IdModel INNER JOIN
		tblCalcTechnicalSemifixedSelections ON tblCalcTechnicalSemifixedSplit.IdModel = tblCalcTechnicalSemifixedSelections.IdModel AND 
		tblCalcTechnicalSemifixedSplit.IdField = tblCalcTechnicalSemifixedSelections.IdField AND 
		tblCalcTechnicalSemifixedSplit.IdZiffAccount = tblCalcTechnicalSemifixedSelections.IdZiffAccount AND 
		tblCalcTechnicalSemifixedSplit.IdTechnicalDriverSize = tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize AND
		tblCalcTechnicalSemifixedSplit.TypeAllocation = tblCalcTechnicalSemifixedSelections.TypeAllocation
	WHERE tblCalcTechnicalSemifixedSplit.TypeAllocation=3 AND tblCalcTechnicalSemifixedSplit.Year<>@MinYear AND tblCalcTechnicalSemifixedSplit.IdModel=@IdModel;
	
	/** Update Semi-Fixed Business Opportunities to Baseline (Should only be in the baseline project) **/
	UPDATE tblCalcProjectionTechnical
	SET [IdProject]=tblProjects.IdProject, [Project]=tblProjects.Name
	FROM tblFields LEFT OUTER JOIN
		tblProjects ON tblFields.IdField = tblProjects.IdField RIGHT OUTER JOIN
		tblCalcProjectionTechnical ON tblFields.IdField = tblCalcProjectionTechnical.IdField
	WHERE tblProjects.Operation=1 AND tblCalcProjectionTechnical.ProjectionCriteria=2 AND tblCalcProjectionTechnical.IdModel=@IdModel;
	
	/** 1 - Fixed, 2 - SemiFixed, 3 - Variable, 4 - Cyclical, 5 - SemiVariable **/
	/** Update Company Projection - Fixed **/
	INSERT INTO tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT DISTINCT 6, tblCalcProjectionTechnical.IdModel, tblCalcProjectionTechnical.IdActivity, tblCalcProjectionTechnical.CodeAct, tblCalcProjectionTechnical.Activity, 
		tblCalcProjectionTechnical.IdResources, tblCalcProjectionTechnical.CodeRes, tblCalcProjectionTechnical.Resource, 
		tblCalcProjectionTechnical.IdClientAccount, tblCalcProjectionTechnical.Account, tblCalcProjectionTechnical.NameAccount, 
		tblCalcProjectionTechnical.IdField, tblCalcProjectionTechnical.Field, tblCalcProjectionTechnical.IdClientCostCenter, 
		tblCalcProjectionTechnical.ClientCostCenter, tblCalcProjectionTechnical.Amount, tblCalcProjectionTechnical.TypeAllocation, 
		tblCalcProjectionTechnical.IdZiffAccount, tblCalcProjectionTechnical.CodeZiff, tblCalcProjectionTechnical.ZiffAccount, 
		tblCalcProjectionTechnical.IdRootZiffAccount, tblCalcProjectionTechnical.RootCodeZiff, tblCalcProjectionTechnical.RootZiffAccount, 
		tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, tblCalcTechnicalDriverAssumption.Year, CAST(0 AS BIT) AS BaseYear, 
		tblCalcProjectionTechnical.TFNormal, tblCalcProjectionTechnical.TFPessimistic, tblCalcProjectionTechnical.TFOptimistic, 
		tblCalcProjectionTechnical.BCRNormal, tblCalcProjectionTechnical.BCRPessimistic, tblCalcProjectionTechnical.BCROptimistic, 1 AS BaseCostType, tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumption.IdModel = tblCalcProjectionTechnical.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = tblCalcProjectionTechnical.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = tblCalcProjectionTechnical.IdField
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=1) 
	AND tblCalcProjectionTechnical.BaseYear=1 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND tblCalcProjectionTechnical.BaseCostType=1 
	AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Update Ziff Projection - Fixed **/
	INSERT INTO tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT DISTINCT 7, tblCalcProjectionTechnical.IdModel, tblCalcProjectionTechnical.IdActivity, tblCalcProjectionTechnical.CodeAct, tblCalcProjectionTechnical.Activity, 
		tblCalcProjectionTechnical.IdResources, tblCalcProjectionTechnical.CodeRes, tblCalcProjectionTechnical.Resource, 
		tblCalcProjectionTechnical.IdClientAccount, tblCalcProjectionTechnical.Account, tblCalcProjectionTechnical.NameAccount, 
		tblCalcProjectionTechnical.IdField, tblCalcProjectionTechnical.Field, tblCalcProjectionTechnical.IdClientCostCenter, 
		tblCalcProjectionTechnical.ClientCostCenter, tblCalcProjectionTechnical.Amount, tblCalcProjectionTechnical.TypeAllocation, 
		tblCalcProjectionTechnical.IdZiffAccount, tblCalcProjectionTechnical.CodeZiff, tblCalcProjectionTechnical.ZiffAccount, 
		tblCalcProjectionTechnical.IdRootZiffAccount, tblCalcProjectionTechnical.RootCodeZiff, tblCalcProjectionTechnical.RootZiffAccount, 
		tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.Year, CAST(0 AS BIT) AS BaseYear, 
		tblCalcProjectionTechnical.TFNormal, tblCalcProjectionTechnical.TFPessimistic, tblCalcProjectionTechnical.TFOptimistic, 
		tblCalcProjectionTechnical.BCRNormal, tblCalcProjectionTechnical.BCRPessimistic, tblCalcProjectionTechnical.BCROptimistic, 2 AS BaseCostType, tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumptionZiff.IdModel = tblCalcProjectionTechnical.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = tblCalcProjectionTechnical.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = tblCalcProjectionTechnical.IdField		
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=1) 
	AND tblCalcProjectionTechnical.BaseYear=1 
	AND tblCalcProjectionTechnical.BaseCostType=2 
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0 
	AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	/** Update Company Projection - Variable and Cyclical **/
	INSERT INTO tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 8, tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.CodeAct, tblCalcBaseCostByField.Activity, 
		tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.CodeRes, tblCalcBaseCostByField.Resource, 
		tblCalcBaseCostByField.IdClientAccount, tblCalcBaseCostByField.Account, tblCalcBaseCostByField.NameAccount, tblCalcBaseCostByField.IdField, 
		tblCalcBaseCostByField.Field, tblCalcBaseCostByField.IdClientCostCenter, tblCalcBaseCostByField.ClientCostCenter, 
		tblCalcBaseCostByField.Amount, tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.CodeZiff, 
		tblCalcBaseCostByField.ZiffAccount, tblCalcBaseCostByField.IdRootZiffAccount, tblCalcBaseCostByField.RootCodeZiff, 
		tblCalcBaseCostByField.RootZiffAccount, tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumption.Year, tblCalcTechnicalDriverAssumption.BaseYear, tblCalcTechnicalDriverAssumption.TFNormal, 
		tblCalcTechnicalDriverAssumption.TFPessimistic, tblCalcTechnicalDriverAssumption.TFOptimistic, tblCalcBaseCostByField.BCRNormal, 
		tblCalcBaseCostByField.BCRPessimistic, tblCalcBaseCostByField.BCROptimistic, 1 AS BaseCostType, 
		tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=3 OR tblCalcTechnicalDriverAssumption.ProjectionCriteria=4 /*OR tblCalcTechnicalDriverAssumption.ProjectionCriteria=5*/) 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND tblCalcBaseCostByField.IdModel=@IdModel;
	
	/** Update Ziff Projection - Variable and Cyclical **/
	INSERT INTO tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 9, tblCalcBaseCostByFieldZiff.IdModel, NULL, NULL, NULL, NULL, NULL, NULL, 
		NULL, NULL, NULL, tblCalcBaseCostByFieldZiff.IdField, tblCalcBaseCostByFieldZiff.Field, NULL, NULL, 
		tblCalcBaseCostByFieldZiff.TotalCost, NULL, tblCalcBaseCostByFieldZiff.IdZiffAccount, tblCalcBaseCostByFieldZiff.CodeZiff, 
		tblCalcBaseCostByFieldZiff.ZiffAccount, tblCalcBaseCostByFieldZiff.IdRootZiffAccount, tblCalcBaseCostByFieldZiff.RootCodeZiff, 
		tblCalcBaseCostByFieldZiff.RootZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumptionZiff.Year, tblCalcTechnicalDriverAssumptionZiff.BaseYear, tblCalcTechnicalDriverAssumptionZiff.TFNormal, 
		tblCalcTechnicalDriverAssumptionZiff.TFPessimistic, tblCalcTechnicalDriverAssumptionZiff.TFOptimistic, tblCalcBaseCostByFieldZiff.BCRNormal, 
		tblCalcBaseCostByFieldZiff.BCRPessimistic, tblCalcBaseCostByFieldZiff.BCROptimistic, 2 AS BaseCostType, 
		tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=3 OR tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=4 /*OR tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5*/) AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0 AND tblCalcBaseCostByFieldZiff.IdModel=@IdModel;

	/** Update Company Projection - SemiVariable (BaseLine) **/
	INSERT INTO tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT DISTINCT 10, tblCalcProjectionTechnical.IdModel, tblCalcProjectionTechnical.IdActivity, tblCalcProjectionTechnical.CodeAct, tblCalcProjectionTechnical.Activity, 
		tblCalcProjectionTechnical.IdResources, tblCalcProjectionTechnical.CodeRes, tblCalcProjectionTechnical.Resource, 
		tblCalcProjectionTechnical.IdClientAccount, tblCalcProjectionTechnical.Account, tblCalcProjectionTechnical.NameAccount, 
		tblCalcProjectionTechnical.IdField, tblCalcProjectionTechnical.Field, tblCalcProjectionTechnical.IdClientCostCenter, 
		tblCalcProjectionTechnical.ClientCostCenter, tblCalcProjectionTechnical.Amount, tblCalcProjectionTechnical.TypeAllocation, 
		tblCalcProjectionTechnical.IdZiffAccount, tblCalcProjectionTechnical.CodeZiff, tblCalcProjectionTechnical.ZiffAccount, 
		tblCalcProjectionTechnical.IdRootZiffAccount, tblCalcProjectionTechnical.RootCodeZiff, tblCalcProjectionTechnical.RootZiffAccount, 
		tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, tblCalcTechnicalDriverAssumption.Year, CAST(0 AS BIT) AS BaseYear, 
		tblCalcProjectionTechnical.TFNormal, tblCalcProjectionTechnical.TFPessimistic, tblCalcProjectionTechnical.TFOptimistic, 
		tblCalcProjectionTechnical.BCRNormal, tblCalcProjectionTechnical.BCRPessimistic, tblCalcProjectionTechnical.BCROptimistic, 1 AS BaseCostType, tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumption.IdModel = tblCalcProjectionTechnical.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = tblCalcProjectionTechnical.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = tblCalcProjectionTechnical.IdField
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=5) 
	AND tblCalcProjectionTechnical.BaseYear=1 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND tblCalcProjectionTechnical.BaseCostType=1 
	AND tblCalcProjectionTechnical.Operation=1
	AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;

	/** Update Ziff Projection - SemiVariable (BaseLine) **/
	INSERT INTO tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT DISTINCT 11, tblCalcProjectionTechnical.IdModel, tblCalcProjectionTechnical.IdActivity, tblCalcProjectionTechnical.CodeAct, tblCalcProjectionTechnical.Activity, 
		tblCalcProjectionTechnical.IdResources, tblCalcProjectionTechnical.CodeRes, tblCalcProjectionTechnical.Resource, 
		tblCalcProjectionTechnical.IdClientAccount, tblCalcProjectionTechnical.Account, tblCalcProjectionTechnical.NameAccount, 
		tblCalcProjectionTechnical.IdField, tblCalcProjectionTechnical.Field, tblCalcProjectionTechnical.IdClientCostCenter, 
		tblCalcProjectionTechnical.ClientCostCenter, tblCalcProjectionTechnical.Amount, tblCalcProjectionTechnical.TypeAllocation, 
		tblCalcProjectionTechnical.IdZiffAccount, tblCalcProjectionTechnical.CodeZiff, tblCalcProjectionTechnical.ZiffAccount, 
		tblCalcProjectionTechnical.IdRootZiffAccount, tblCalcProjectionTechnical.RootCodeZiff, tblCalcProjectionTechnical.RootZiffAccount, 
		tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.Year, CAST(0 AS BIT) AS BaseYear, 
		/*tblCalcProjectionTechnical.TFNormal, tblCalcProjectionTechnical.TFPessimistic, tblCalcProjectionTechnical.TFOptimistic, 
		tblCalcProjectionTechnical.BCRNormal, tblCalcProjectionTechnical.BCRPessimistic, tblCalcProjectionTechnical.BCROptimistic*/0,0,0,0,0,0, 2 AS BaseCostType, tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumptionZiff.IdModel = tblCalcProjectionTechnical.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = tblCalcProjectionTechnical.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = tblCalcProjectionTechnical.IdField		
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5) 
	AND tblCalcProjectionTechnical.BaseYear=1 
	AND tblCalcProjectionTechnical.BaseCostType=2 
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0
	AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 
	AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;

	/** Update Company Projection - SemiVariable (Business Opportunity) **/
	/** Get Base Year **/
	DECLARE @BaseYear INT
	SELECT @BaseYear = [BaseYear] FROM [DBCPM].[dbo].[tblModels] WHERE IdModel=@IdModel

	/** Build Volume Tables **/
	/** Report (Baseline)**/
	CREATE TABLE #VolumeReportBLRows ([Name] NVARCHAR(255), [YearProjection] INT, [NormalAmount] FLOAT, [OptimisticAmount] FLOAT, [PessimisticAmount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLRows ([Name], [YearProjection], [NormalAmount], [OptimisticAmount], [PessimisticAmount])	
		SELECT	dbo.tblProjects.Name AS Name, dbo.tblTechnicalDriverData.Year AS YearProjection, 
				ISNULL(dbo.tblTechnicalDriverData.Normal,0) AS NormalAmount,
				ISNULL(dbo.tblTechnicalDriverData.Optimistic,0) AS OptimisticAmount,
				ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0) AS PessimisticAmount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON
			dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=1
	ORDER BY dbo.tblTechnicalDriverData.Year
	
	CREATE TABLE #VolumeReportBL ([Name] NVARCHAR(255), [YearProjection] INT, [NormalAmount] FLOAT, [OptimisticAmount] FLOAT, [PessimisticAmount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBL([Name], [YearProjection], [NormalAmount], [OptimisticAmount], [PessimisticAmount])
	SELECT	Name, YearProjection, SUM(NormalAmount), SUM(OptimisticAmount), SUM(PessimisticAmount)
	FROM	#VolumeReportBLRows
	GROUP BY Name, YearProjection
	ORDER BY Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBLList (Name NVARCHAR(255), YearProjection INT, [NormalAmount] FLOAT, [OptimisticAmount] FLOAT, [PessimisticAmount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLList ([Name],[YearProjection], [NormalAmount], [OptimisticAmount], [PessimisticAmount])
	SELECT CNList.Name, CYList.Year, 0, 0, 0 FROM
		(SELECT [Name] FROM #VolumeReportBL WHERE [Name] IS NOT NULL GROUP BY [Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #BLList to Fill Properly **/
	INSERT INTO #VolumeReportBL ([Name],[YearProjection],[NormalAmount],[OptimisticAmount],[PessimisticAmount])
	SELECT	#VolumeReportBLList.Name, #VolumeReportBLList.YearProjection, 0 AS NormalAmount, 0 AS OptimisticAmount,  0 AS PessimisticAmount
	FROM	#VolumeReportBLList LEFT OUTER JOIN
			#VolumeReportBL ON #VolumeReportBLList.Name=#VolumeReportBL.Name AND #VolumeReportBLList.Name=#VolumeReportBL.Name AND #VolumeReportBLList.YearProjection=#VolumeReportBL.YearProjection
	WHERE	#VolumeReportBL.YearProjection IS NULL;

	/** Report (Business Opportunities)**/
	CREATE TABLE #VolumeReportBORows ([Name] NVARCHAR(255), [YearProjection] INT, [NormalAmount] FLOAT, [OptimisticAmount] FLOAT, [PessimisticAmount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBORows ([Name], [YearProjection], [NormalAmount], [OptimisticAmount], [PessimisticAmount])	
		SELECT	dbo.tblProjects.Name AS Name, dbo.tblTechnicalDriverData.Year AS YearProjection, 
				ISNULL(dbo.tblTechnicalDriverData.Normal,0) AS NormalAmount,
				ISNULL(dbo.tblTechnicalDriverData.Optimistic,0) AS OptimisticAmount,
				ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0) AS PessimisticAmount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON
			dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=2
	ORDER BY dbo.tblTechnicalDriverData.Year

	CREATE TABLE #VolumeReportBO ([Name] NVARCHAR(255), [YearProjection] INT, [NormalAmount] FLOAT, [OptimisticAmount] FLOAT, [PessimisticAmount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBO([Name], [YearProjection], [NormalAmount], [OptimisticAmount], [PessimisticAmount])
	SELECT	Name, YearProjection, SUM(NormalAmount), SUM(OptimisticAmount), SUM(PessimisticAmount)
	FROM	#VolumeReportBORows
	GROUP BY Name, YearProjection
	ORDER BY Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBOList (Name NVARCHAR(255), YearProjection INT, [NormalAmount] FLOAT, [OptimisticAmount] FLOAT, [PessimisticAmount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBOList ([Name],[YearProjection], [NormalAmount], [OptimisticAmount], [PessimisticAmount])
	SELECT CNList.Name, CYList.Year, 0, 0, 0 FROM
		(SELECT [Name] FROM #VolumeReportBO WHERE [Name] IS NOT NULL GROUP BY [Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #BOList to Fill Properly **/
	INSERT INTO #VolumeReportBO ([Name],[YearProjection],[NormalAmount],[OptimisticAmount],[PessimisticAmount])
	SELECT	#VolumeReportBOList.Name, #VolumeReportBOList.YearProjection, 0 AS NormalAmount, 0 AS OptimisticAmount,  0 AS PessimisticAmount
	FROM	#VolumeReportBOList LEFT OUTER JOIN
			#VolumeReportBO ON #VolumeReportBOList.Name=#VolumeReportBO.Name AND #VolumeReportBOList.Name=#VolumeReportBO.Name AND #VolumeReportBOList.YearProjection=#VolumeReportBO.YearProjection
	WHERE	#VolumeReportBO.YearProjection IS NULL;

	/** Report (All Fields Grouped)**/
	CREATE TABLE #VolumeReportGroupedBL ([YearProjection] INT, [NormalAmount] FLOAT, [OptimisticAmount] FLOAT, [PessimisticAmount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBL ([YearProjection], [NormalAmount], [OptimisticAmount], [PessimisticAmount])
	SELECT YearProjection,SUM(NormalAmount) AS NormalAmount,SUM(OptimisticAmount) AS OptimisticAmount,SUM(PessimisticAmount) AS PessimisticAmount
	FROM #VolumeReportBL
	GROUP BY YearProjection
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGroupedBO ([YearProjection] INT, [NormalAmount] FLOAT, [OptimisticAmount] FLOAT, [PessimisticAmount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBO ([YearProjection], [NormalAmount], [OptimisticAmount], [PessimisticAmount])
	SELECT YearProjection,SUM(NormalAmount) AS NormalAmount,SUM(OptimisticAmount) AS OptimisticAmount,SUM(PessimisticAmount) AS PessimisticAmount
	FROM #VolumeReportBO
	GROUP BY YearProjection
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGrouped ([YearProjection] INT, [NormalAmount] FLOAT, [OptimisticAmount] FLOAT, [PessimisticAmount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGrouped ([YearProjection], [NormalAmount], [OptimisticAmount], [PessimisticAmount])
	SELECT #VolumeReportGroupedBL.YearProjection,
		   #VolumeReportGroupedBL.NormalAmount + #VolumeReportGroupedBO.NormalAmount AS NormalAmount,
		   #VolumeReportGroupedBL.OptimisticAmount + #VolumeReportGroupedBO.OptimisticAmount AS OptimisticAmount,
		   #VolumeReportGroupedBL.PessimisticAmount + #VolumeReportGroupedBO.PessimisticAmount AS PessimisticAmount
	FROM #VolumeReportGroupedBL INNER JOIN	#VolumeReportGroupedBO ON #VolumeReportGroupedBO.YearProjection = #VolumeReportGroupedBL.YearProjection		   

	DECLARE @TotalCapacityDriverNormal INT = 0;
	DECLARE @TotalCapacityDriverOptimistic INT = 0;
	DECLARE @TotalCapacityDriverPessimistic INT = 0;

	/** Get grouped driver value for Base Year **/
	SELECT @TotalCapacityDriverNormal = NormalAmount FROM #VolumeReportGrouped WHERE YearProjection=@BaseYear
	SELECT @TotalCapacityDriverOptimistic = OptimisticAmount FROM #VolumeReportGrouped WHERE YearProjection=@BaseYear
	SELECT @TotalCapacityDriverPessimistic = PessimisticAmount FROM #VolumeReportGrouped WHERE YearProjection=@BaseYear

	/** Build Utilization Capacity Values **/
	CREATE TABLE #UtilizationCapacityValues ([CodeZiff] NVARCHAR(50), [IdField] INT, [UtilizationValueNormal] FLOAT, [UtilizationValueOptimistic] FLOAT, [UtilizationValuePessimistic] FLOAT) ON [PRIMARY]
	INSERT INTO #UtilizationCapacityValues([CodeZiff], [IdField], [UtilizationValueNormal], [UtilizationValueOptimistic], [UtilizationValuePessimistic])
	SELECT  A.[CodeZiff], tblFields.IdField, @TotalCapacityDriverNormal/A.UtilizationValue*100, @TotalCapacityDriverOptimistic/A.UtilizationValue*100, @TotalCapacityDriverPessimistic/A.UtilizationValue*100
	  FROM tblUtilizationCapacity A INNER JOIN tblFields
			ON A.IdField = tblFields.IdField      
	WHERE  	A.IdModel = @IdModel

	/** WITHOUT VOLUME CALCS **/
--	INSERT INTO tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
--	SELECT 12, tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.CodeAct, tblCalcBaseCostByField.Activity, 
--		tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.CodeRes, tblCalcBaseCostByField.Resource, 
--		tblCalcBaseCostByField.IdClientAccount, tblCalcBaseCostByField.Account, tblCalcBaseCostByField.NameAccount, tblCalcBaseCostByField.IdField, 
--		tblCalcBaseCostByField.Field, tblCalcBaseCostByField.IdClientCostCenter, tblCalcBaseCostByField.ClientCostCenter, 
--		tblCalcBaseCostByField.Amount, tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.CodeZiff, 
--		tblCalcBaseCostByField.ZiffAccount, tblCalcBaseCostByField.IdRootZiffAccount, tblCalcBaseCostByField.RootCodeZiff, 
--		tblCalcBaseCostByField.RootZiffAccount, tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, 
--		tblCalcTechnicalDriverAssumption.Year, tblCalcTechnicalDriverAssumption.BaseYear, tblCalcTechnicalDriverAssumption.TFNormal, 
--		tblCalcTechnicalDriverAssumption.TFPessimistic, tblCalcTechnicalDriverAssumption.TFOptimistic, tblCalcBaseCostByField.BCRNormal, 
--		tblCalcBaseCostByField.BCRPessimistic, tblCalcBaseCostByField.BCROptimistic, 1 AS BaseCostType, 
--		tblCalcTechnicalDriverAssumption.Operation
--	FROM tblCalcBaseCostByField INNER JOIN
--		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
--		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
--		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount
--	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=5) 
--	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
--	AND tblCalcTechnicalDriverAssumption.Operation=2
--	AND tblCalcBaseCostByField.IdModel=@IdModel;

	/** WITH VOLUME CALCS **/
	INSERT INTO tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 12, tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.CodeAct, tblCalcBaseCostByField.Activity, 
		tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.CodeRes, tblCalcBaseCostByField.Resource, 
		tblCalcBaseCostByField.IdClientAccount, tblCalcBaseCostByField.Account, tblCalcBaseCostByField.NameAccount, tblCalcBaseCostByField.IdField, 
		tblCalcBaseCostByField.Field, tblCalcBaseCostByField.IdClientCostCenter, tblCalcBaseCostByField.ClientCostCenter, 
		tblCalcBaseCostByField.Amount, tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.CodeZiff, 
		tblCalcBaseCostByField.ZiffAccount, tblCalcBaseCostByField.IdRootZiffAccount, tblCalcBaseCostByField.RootCodeZiff, 
		tblCalcBaseCostByField.RootZiffAccount, tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumption.Year, tblCalcTechnicalDriverAssumption.BaseYear, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValueNormal < #VolumeReportGrouped.NormalAmount THEN tblCalcTechnicalDriverAssumption.TFNormal ELSE 0 END AS TFNormal, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValuePessimistic < #VolumeReportGrouped.PessimisticAmount THEN tblCalcTechnicalDriverAssumption.TFPessimistic ELSE 0 END AS TFPessimistic, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValueOptimistic < #VolumeReportGrouped.OptimisticAmount THEN tblCalcTechnicalDriverAssumption.TFOptimistic ELSE 0 END AS TFOptimistic, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValueNormal < #VolumeReportGrouped.NormalAmount THEN tblCalcBaseCostByField.BCRNormal ELSE 0 END AS BCRNormal, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValuePessimistic < #VolumeReportGrouped.PessimisticAmount THEN tblCalcBaseCostByField.BCRPessimistic ELSE 0 END AS BCRPessimistic, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValueOptimistic < #VolumeReportGrouped.OptimisticAmount THEN tblCalcBaseCostByField.BCROptimistic ELSE 0 END AS BCROptimistic, 
		1 AS BaseCostType, 
		tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount INNER JOIN
		#UtilizationCapacityValues ON #UtilizationCapacityValues.CodeZiff = tblCalcBaseCostByField.CodeZiff AND
		#UtilizationCapacityValues.IdField = tblCalcBaseCostByField.IdField INNER JOIN
		#VolumeReportGrouped ON #VolumeReportGrouped.YearProjection = tblCalcTechnicalDriverAssumption.Year
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=5) 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND tblCalcTechnicalDriverAssumption.Operation=2
	AND tblCalcBaseCostByField.IdModel=@IdModel;

	/** Update Ziff Projection - SemiVariable (Business Opportunity)  **/
	/** WITHOUT VOLUME CALCS **/
	--INSERT INTO tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	--SELECT 13, tblCalcBaseCostByFieldZiff.IdModel, NULL, NULL, NULL, NULL, NULL, NULL, 
	--	NULL, NULL, NULL, tblCalcBaseCostByFieldZiff.IdField, tblCalcBaseCostByFieldZiff.Field, NULL, NULL, 
	--	tblCalcBaseCostByFieldZiff.TotalCost, NULL, tblCalcBaseCostByFieldZiff.IdZiffAccount, tblCalcBaseCostByFieldZiff.CodeZiff, 
	--	tblCalcBaseCostByFieldZiff.ZiffAccount, tblCalcBaseCostByFieldZiff.IdRootZiffAccount, tblCalcBaseCostByFieldZiff.RootCodeZiff, 
	--	tblCalcBaseCostByFieldZiff.RootZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, 
	--	tblCalcTechnicalDriverAssumptionZiff.Year, tblCalcTechnicalDriverAssumptionZiff.BaseYear, tblCalcTechnicalDriverAssumptionZiff.TFNormal, 
	--	tblCalcTechnicalDriverAssumptionZiff.TFPessimistic, tblCalcTechnicalDriverAssumptionZiff.TFOptimistic, tblCalcBaseCostByFieldZiff.BCRNormal, 
	--	tblCalcBaseCostByFieldZiff.BCRPessimistic, tblCalcBaseCostByFieldZiff.BCROptimistic, 2 AS BaseCostType, 
	--	tblCalcTechnicalDriverAssumptionZiff.Operation
	--FROM tblCalcBaseCostByFieldZiff INNER JOIN
	--	tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
	--	tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND 
	--	tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount
	--WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5) 
	--AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0 
	--AND tblCalcTechnicalDriverAssumptionZiff.Operation=2
	--AND tblCalcBaseCostByFieldZiff.IdModel=@IdModel;

	/** WITH VOLUME CALCS **/
	INSERT INTO tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 13, tblCalcBaseCostByFieldZiff.IdModel, NULL, NULL, NULL, NULL, NULL, NULL, 
		NULL, NULL, NULL, tblCalcBaseCostByFieldZiff.IdField, tblCalcBaseCostByFieldZiff.Field, NULL, NULL, 
		tblCalcBaseCostByFieldZiff.TotalCost, NULL, tblCalcBaseCostByFieldZiff.IdZiffAccount, tblCalcBaseCostByFieldZiff.CodeZiff, 
		tblCalcBaseCostByFieldZiff.ZiffAccount, tblCalcBaseCostByFieldZiff.IdRootZiffAccount, tblCalcBaseCostByFieldZiff.RootCodeZiff, 
		tblCalcBaseCostByFieldZiff.RootZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumptionZiff.Year, tblCalcTechnicalDriverAssumptionZiff.BaseYear, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValueNormal < #VolumeReportGrouped.NormalAmount THEN tblCalcTechnicalDriverAssumptionZiff.TFNormal ELSE 0 END AS TFNormal, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValuePessimistic < #VolumeReportGrouped.PessimisticAmount THEN tblCalcTechnicalDriverAssumptionZiff.TFPessimistic ELSE 0 END AS TFPessimistic, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValueOptimistic < #VolumeReportGrouped.OptimisticAmount THEN tblCalcTechnicalDriverAssumptionZiff.TFOptimistic ELSE 0 END AS TFOptimistic, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValueNormal < #VolumeReportGrouped.NormalAmount THEN tblCalcBaseCostByFieldZiff.BCRNormal ELSE 0 END AS BCRNormal, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValuePessimistic < #VolumeReportGrouped.PessimisticAmount THEN tblCalcBaseCostByFieldZiff.BCRPessimistic ELSE 0 END AS BCRPessimistic, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValueOptimistic < #VolumeReportGrouped.OptimisticAmount THEN tblCalcBaseCostByFieldZiff.BCROptimistic ELSE 0 END AS BCROptimistic, 
		2 AS BaseCostType, 
		tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount INNER JOIN
		#UtilizationCapacityValues ON #UtilizationCapacityValues.CodeZiff = tblCalcBaseCostByFieldZiff.CodeZiff AND
		#UtilizationCapacityValues.IdField = tblCalcBaseCostByFieldZiff.IdField INNER JOIN
		#VolumeReportGrouped ON #VolumeReportGrouped.YearProjection = tblCalcTechnicalDriverAssumptionZiff.Year
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5) 
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0 
	AND tblCalcTechnicalDriverAssumptionZiff.Operation=2
	AND tblCalcBaseCostByFieldZiff.IdModel=@IdModel;

	/** Prep for Reporting **/
	UPDATE tblCalcProjectionTechnical SET [Activity]='Ziff/Third-party Costs',[SortOrder]=2 WHERE [Activity] IS NULL AND [BaseCostType]=2 AND [IdModel]=@IdModel;
	UPDATE tblCalcProjectionTechnical SET [Resource]='Ziff/Third-party Costs',[SortOrder]=2 WHERE [Resource] IS NULL AND [BaseCostType]=2 AND [IdModel]=@IdModel;
	UPDATE tblCalcProjectionTechnical SET [Project]=tblProjects.Name, [Operation]=tblProjects.Operation FROM tblCalcProjectionTechnical INNER JOIN tblProjects ON tblCalcProjectionTechnical.IdProject = tblProjects.IdProject AND tblCalcProjectionTechnical.IdModel = tblProjects.IdModel WHERE tblCalcProjectionTechnical.IdModel=@IdModel;
	UPDATE tblCalcProjectionTechnical SET [Project]='Unknown Project' WHERE [Project] IS NULL AND [IdProject] IS NOT NULL AND [IdModel]=@IdModel;
	
END
GO
/****** Object:  StoredProcedure [dbo].[reppOpexProjectionDetailed]    Script Date: 06/05/2015 10:52:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-03-31
-- Description:	Module Report Opex Projection detailed Baseline and Business Opportunities
-- ===========================================================================================
ALTER PROCEDURE [dbo].[reppOpexProjectionDetailed](
@IdModel INT,
@IdAggregationLevel INT,
@IdField INT,
@IdStructure INT,
@From INT,
@To INT,
@IdTechnicalScenario INT,
@IdEconomicScenario INT,
@IdCurrency INT,
@IdTerm INT,
@IdTechnicalDriver INT,
@IdCostType INT,
@IdTypeOperation INT,
@IdLanguage INT
)
AS
BEGIN

	/** DROP Temporary Tables **/
	IF OBJECT_ID('tempdb..#FieldList') IS NOT NULL DROP TABLE #FieldList
	IF OBJECT_ID('tempdb..#AccountList') IS NOT NULL DROP TABLE #AccountList
	IF OBJECT_ID('tempdb..#TechDriver') IS NOT NULL DROP TABLE #TechDriver
	IF OBJECT_ID('tempdb..#Opex') IS NOT NULL DROP TABLE #Opex
	IF OBJECT_ID('tempdb..#BL') IS NOT NULL DROP TABLE #BL
	IF OBJECT_ID('tempdb..#BO') IS NOT NULL DROP TABLE #BO
	IF OBJECT_ID('tempdb..#BLList') IS NOT NULL DROP TABLE #BLList
	IF OBJECT_ID('tempdb..#BOList') IS NOT NULL DROP TABLE #BOList
	IF OBJECT_ID('tempdb..#ProjectionCriteriaList') IS NOT NULL DROP TABLE #ProjectionCriteriaList
	IF OBJECT_ID('tempdb..#VolumeReportBL') IS NOT NULL DROP TABLE #VolumeReportBL  
	IF OBJECT_ID('tempdb..#VolumeReportBO') IS NOT NULL DROP TABLE #VolumeReportBO
	IF OBJECT_ID('tempdb..#BLVolTotal') IS NOT NULL DROP TABLE #BLVolTotal

	/** Structure Value - ZiffEnergy=1 Client=2 **/
	/** Term Value - Real=1 (Run Economic Model) Nominal=2 (No Economic Influence) **/
	DECLARE @StartYear INT = @From;
	DECLARE @EndYear INT = @To;

	DECLARE @TotalCapacityDriver INT = 0;

	/** Get Base Year **/
	DECLARE @BaseYear INT
	SELECT @BaseYear = [BaseYear] FROM [DBCPM].[dbo].[tblModels] WHERE IdModel=@IdModel
		
	/** Build Year Columns **/
	DECLARE @YearColumnList NVARCHAR(MAX)='';	
	WHILE (@From <= @To)
	BEGIN
		SET @YearColumnList =  @YearColumnList + '[' + CAST(@From AS VARCHAR(4)) + ']';
		SET @From = @From + 1;
		IF (@From <= @To)
		BEGIN
			SET @YearColumnList = @YearColumnList + ',';
		END
	END
	
	/** Get Currency Factor to use **/
	DECLARE @CurrencyFactor FLOAT;
	SELECT @CurrencyFactor=[Value] FROM tblModelsCurrencies WHERE [IdCurrency]=@IdCurrency AND [IdModel]=@IdModel;

	/** Build Field List **/
	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF (@IdAggregationLevel=0 AND @IdField=0 AND @IdTypeOperation=0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0 AND @IdField>0 AND @IdTypeOperation>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdField]=@IdField AND [IdModel]=@IdModel AND [IdTypeOperation]=@IdTypeOperation GROUP BY [IdField];
	END
	ELSE IF (@IdField>0 AND @IdTypeOperation>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdField]=@IdField AND [IdModel]=@IdModel AND [IdTypeOperation]=@IdTypeOperation GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0 AND @IdField>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0 AND @IdTypeOperation>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdModel]=@IdModel AND [IdTypeOperation]=@IdTypeOperation GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdField>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdTypeOperation>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdTypeOperation]=@IdTypeOperation AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	DECLARE @FieldCount INT;
	SELECT @FieldCount=COUNT([IdField]) FROM #FieldList

	/** Build Projection Criteria List **/
	CREATE TABLE #ProjectionCriteriaList (Code INT PRIMARY KEY NOT NULL, [Name] NVARCHAR(20), [SortOrder] INT) ON [PRIMARY]
	INSERT INTO #ProjectionCriteriaList (Code, Name, SortOrder)
	SELECT Code, Name, CASE WHEN Code=1 THEN 1 
			WHEN Code=3 THEN 2
			WHEN Code=4 THEN 3
			WHEN Code=2 THEN 4
			ELSE 5 END  
	FROM tblParameters 
	WHERE [Type]='ProjectionCriteria' and IdLanguage=@IdLanguage AND Code<90

	/** Build Criteria Columns **/
	DECLARE @ProjectionCriteriaList NVARCHAR(MAX)='';	
	DECLARE @ProjectionCriteriaList2 NVARCHAR(MAX)='SUM([Total]) AS Total,';	
	DECLARE @ProjectionCriteriaName NVARCHAR(255);
	DECLARE ProjectionCriteriaList CURSOR FOR
	SELECT Name FROM #ProjectionCriteriaList ORDER BY SortOrder;

	OPEN ProjectionCriteriaList;
	FETCH NEXT FROM ProjectionCriteriaList INTO @ProjectionCriteriaName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @ProjectionCriteriaList=''
		BEGIN
			SET @ProjectionCriteriaList =  @ProjectionCriteriaList + '[' + @ProjectionCriteriaName + ']';
			SET @ProjectionCriteriaList2 =  @ProjectionCriteriaList2 + 'SUM(ISNULL([' + @ProjectionCriteriaName + '],0)) AS ' + @ProjectionCriteriaName;
		END
		ELSE
		BEGIN
			SET @ProjectionCriteriaList =  @ProjectionCriteriaList + ',[' + @ProjectionCriteriaName + ']';
			SET @ProjectionCriteriaList2 =  @ProjectionCriteriaList2 + ',SUM(ISNULL([' + @ProjectionCriteriaName + '],0)) AS ' + @ProjectionCriteriaName;
		END
		FETCH NEXT FROM ProjectionCriteriaList INTO @ProjectionCriteriaName;
	END

	CLOSE ProjectionCriteriaList;
	DEALLOCATE ProjectionCriteriaList;

	/** Build Technical Driver Factors (used for KPI Report) **/
	CREATE TABLE #TechDriver ([Year] INT, [Normal] FLOAT, [Optimistic] FLOAT, [Pessimistic] FLOAT) ON [PRIMARY]
	IF @IdTechnicalDriver=0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, 1 AS [Normal], 1 AS [Optimistic], 1 AS [Pessimistic]
		FROM tblCalcModelYears
		WHERE [IdModel]=@IdModel AND [Year] BETWEEN @StartYear AND @EndYear;
	END
	ELSE IF @IdTechnicalDriver>0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, SUM([Normal]) AS [Normal], SUM([Pessimistic]) AS [Pessimistic], SUM([Optimistic]) AS [Optimistic]
		FROM tblTechnicalDriverData
		WHERE [IdTechnicalDriver]=@IdTechnicalDriver AND [IdModel]=@IdModel AND IdField IN (SELECT [IdField] FROM #FieldList)
		AND	dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
		GROUP BY Year;
	END

	/** Build Volume Tables **/
	/** Report (Baseline)**/
	CREATE TABLE #VolumeReportBLRows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLRows ([ParentName], [Name], [YearProjection], [Amount])	
		SELECT	DISTINCT N'All Cost Accounts' + 
			CASE WHEN @IdTechnicalScenario=1 THEN 'Base' 
				WHEN @IdTechnicalScenario=2 THEN 'Optimistic'
	 			WHEN @IdTechnicalScenario=3 THEN 'Pessimistic'
			END AS ParentName, dbo.tblProjects.Name AS Name, 
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN ISNULL(dbo.tblTechnicalDriverData.Normal,0)
				WHEN @IdTechnicalScenario=2 THEN ISNULL(dbo.tblTechnicalDriverData.Optimistic,0)
				WHEN @IdTechnicalScenario=3 THEN ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0)
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON
			dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=1
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	ORDER BY dbo.tblTechnicalDriverData.Year
	
	CREATE TABLE #VolumeReportBL ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBL([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBLRows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBLList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBL WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #BLList to Fill Properly **/
	INSERT INTO #VolumeReportBL ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBLList.ParentName,#VolumeReportBLList.Name, #VolumeReportBLList.YearProjection,0 AS Amount
	FROM	#VolumeReportBLList LEFT OUTER JOIN
			#VolumeReportBL ON #VolumeReportBLList.ParentName=#VolumeReportBL.ParentName AND #VolumeReportBLList.Name=#VolumeReportBL.Name AND #VolumeReportBLList.YearProjection=#VolumeReportBL.YearProjection
	WHERE	#VolumeReportBL.YearProjection IS NULL;

	/** Report (Business Opportunities)**/
	CREATE TABLE #VolumeReportBORows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBORows ([ParentName], [Name], [YearProjection], [Amount])	
		SELECT	DISTINCT N'All Cost Accounts' + 
			CASE WHEN @IdTechnicalScenario=1 THEN 'Base' 
				WHEN @IdTechnicalScenario=2 THEN 'Optimistic'
	 			WHEN @IdTechnicalScenario=3 THEN 'Pessimistic'
			END AS ParentName, dbo.tblProjects.Name AS Name, 
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN ISNULL(dbo.tblTechnicalDriverData.Normal,0)
				WHEN @IdTechnicalScenario=2 THEN ISNULL(dbo.tblTechnicalDriverData.Optimistic,0)
				WHEN @IdTechnicalScenario=3 THEN ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0)
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON
			dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=2
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	ORDER BY dbo.tblTechnicalDriverData.Year

	CREATE TABLE #VolumeReportBO ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBO([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBORows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBOList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBOList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBO WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #BOList to Fill Properly **/
	INSERT INTO #VolumeReportBO ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBOList.ParentName,#VolumeReportBOList.Name, #VolumeReportBOList.YearProjection,0 AS Amount
	FROM	#VolumeReportBOList LEFT OUTER JOIN
			#VolumeReportBO ON #VolumeReportBOList.ParentName=#VolumeReportBO.ParentName AND #VolumeReportBOList.Name=#VolumeReportBO.Name AND #VolumeReportBOList.YearProjection=#VolumeReportBO.YearProjection
	WHERE	#VolumeReportBO.YearProjection IS NULL;

	/** Report (All Fields Grouped)**/
	CREATE TABLE #VolumeReportGroupedBL ([YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBL ([YearProjection], [Amount])
	SELECT YearProjection,SUM(amount) AS Amount
	FROM #VolumeReportBL
	GROUP BY YearProjection
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGroupedBO ([YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBO ([YearProjection], [Amount])
	SELECT YearProjection,SUM(amount) AS Amount
	FROM #VolumeReportBO
	GROUP BY YearProjection
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGrouped ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255),[YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGrouped ([ParentName], [Name], [YearProjection], [Amount])
	SELECT N'All Cost Accounts' AS ParentName, 
		   CASE WHEN @IdTechnicalScenario=1 THEN 'Base' 
				WHEN @IdTechnicalScenario=2 THEN 'Optimistic'
				WHEN @IdTechnicalScenario=3 THEN 'Pessimistic'
		   END AS Name,
		   #VolumeReportGroupedBL.YearProjection,
		   #VolumeReportGroupedBL.Amount + #VolumeReportGroupedBO.Amount AS AMOUNT
	FROM #VolumeReportGroupedBL INNER JOIN	#VolumeReportGroupedBO ON #VolumeReportGroupedBO.YearProjection = #VolumeReportGroupedBL.YearProjection		   

	/** Get grouped driver value for Base Year **/
	SELECT @TotalCapacityDriver = Amount FROM #VolumeReportGrouped WHERE YearProjection=@BaseYear

	/** Baseline Volume Totals per year **/
	CREATE TABLE #BLVolTotalRows ([YearProjection] INT, [TotalVolume] FLOAT) ON [PRIMARY]
	INSERT INTO #BLVolTotalRows ([YearProjection], [TotalVolume])
	SELECT DISTINCT 
		   dbo.tblTechnicalDriverData.Year,
		   CASE WHEN @IdTechnicalScenario=1 THEN dbo.tblTechnicalDriverData.Normal
				WHEN @IdTechnicalScenario=2 THEN dbo.tblTechnicalDriverData.Optimistic
				WHEN @IdTechnicalScenario=3 THEN dbo.tblTechnicalDriverData.Pessimistic
		   ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON
			dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblProjects.[Operation]=1
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	ORDER BY dbo.tblTechnicalDriverData.Year

	CREATE TABLE #BLVolTotal (YearProjection INT, TotalVolume INT) ON [PRIMARY]
	INSERT INTO #BLVolTotal ([YearProjection],[TotalVolume])
	SELECT YearProjection, SUM(TotalVolume)
	FROM	#BLVolTotalRows
	GROUP BY YearProjection
	ORDER BY YearProjection

	/** Build Utilization Capacity Values **/
	CREATE TABLE #UtilizationCapacityValues ([CodeZiff] NVARCHAR(50), [IdField] INT, [UtilizationValue] FLOAT) ON [PRIMARY]
	INSERT INTO #UtilizationCapacityValues([CodeZiff], [IdField], [UtilizationValue])
	SELECT  A.[CodeZiff], tblFields.IdField, @TotalCapacityDriver/A.UtilizationValue*100
	  FROM tblUtilizationCapacity A INNER JOIN tblFields
			ON A.IdField = tblFields.IdField      
	WHERE  	A.IdModel = @IdModel
	AND		A.IdField = @IdField
	
	/** Build Source Data Table - Baseline and Business Opportunities (No Techical KPI Required) **/
	CREATE TABLE #BL (FieldProject NVARCHAR(255), Field NVARCHAR(255), Project NVARCHAR(255), ProjectionCriteria NVARCHAR(255), YearProjection INT, Total FLOAT, CostProjection FLOAT, SortOrder INT) ON [PRIMARY]
	CREATE TABLE #BO (FieldProject NVARCHAR(255), Field NVARCHAR(255), Project NVARCHAR(255), ProjectionCriteria NVARCHAR(255), YearProjection INT, Total FLOAT, CostProjection FLOAT, SortOrder INT) ON [PRIMARY]

	/** BL **/
	INSERT INTO #BL ([FieldProject],[Field],[Project],[ProjectionCriteria],[YearProjection],[Total],[CostProjection],[SortOrder])
	SELECT CPT.Field + ' ('+ CPT.Project + ')' AS FieldProject, CPT.Field, CPT.Project, P.Name AS ProjectionCriteria, CPT.Year AS YearProjection, 0,
		--CASE WHEN CPT.ProjectionCriteria in (1,5) THEN CASE WHEN CPT.Year < #BLVolTotal.YearProjection THEN CASE WHEN @IdCostType=1 THEN SUM(CPT.[Amount] * @CurrencyFactor) WHEN @IdCostType=2 THEN SUM((CPT.[Amount] * @CurrencyFactor)/#VolumeReportBL.Amount * (#VolumeReportBL.Amount / #BLVolTotal.TotalVolume)) END ELSE CASE WHEN @IdCostType=1 THEN CPT.[Amount] * @CurrencyFactor  WHEN @IdCostType=2 THEN (CPT.[Amount] * @CurrencyFactor)/#VolumeReportBL.Amount  * (#VolumeReportBL.Amount / #BLVolTotal.TotalVolume) END END
		--ELSE
		ISNULL(SUM ( 
			CASE WHEN @IdTechnicalScenario=1 THEN CPT.TFNormal * CPT.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN CPT.TFOptimistic * CPT.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN CPT.TFPessimistic * CPT.BCRPessimistic 
			ELSE 0 END 
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END
			/ CASE WHEN @IdCostType=1 THEN 1
			WHEN @IdCostType=2 THEN #VolumeReportBL.Amount
			ELSE 1 END
			* CASE WHEN @IdCostType=1 THEN 1
			WHEN @IdCostType=2 THEN (#VolumeReportBL.Amount / #BLVolTotal.TotalVolume)
			ELSE 1 END
		),0) /*END*/ AS CostProjection, P.SortOrder
	FROM tblCalcProjectionTechnical CPT  LEFT OUTER JOIN
		#TechDriver ON CPT.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON CPT.Year = tblCalcProjectionEconomic.Year AND 
		CPT.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		CPT.IdModel = tblCalcProjectionEconomic.IdModel LEFT OUTER JOIN #ProjectionCriteriaList P ON
		CPT.ProjectionCriteria = P.Code LEFT OUTER JOIN
		#VolumeReportBL ON #VolumeReportBL.YearProjection = CPT.Year AND
		#VolumeReportBL.Name = CPT.Project LEFT OUTER JOIN
		#BLVolTotal ON CPT.Year=#BLVolTotal.YearProjection
	WHERE CPT.IdModel=@IdModel
	  AND CPT.IdField IN (SELECT [IdField] FROM #FieldList) 
	  --AND CPT.BaseCostType=1
	  AND CPT.Operation = 1 
	  AND ISNULL(#TechDriver.[Normal],0) <> 0
	GROUP BY CPT.Field + ' ('+ CPT.Project + ')',CPT.Field, CPT.Project, P.Name, CPT.Year, P.SortOrder, ProjectionCriteria, CPT.Amount,#BLVolTotal.YearProjection, #VolumeReportBL.Amount,#BLVolTotal.TotalVolume
	
	/** BO **/
	INSERT INTO #BO ([FieldProject],[Field],[Project],[ProjectionCriteria],[YearProjection],[Total],[CostProjection],[SortOrder])
	SELECT CPT.Field + ' ('+ CPT.Project + ')' AS FieldProject, CPT.Field, CPT.Project, P.Name AS ProjectionCriteria, CPT.Year AS YearProjection, 0,
		SUM (
			--CASE WHEN CPT.ProjectionCriteria IN (5) THEN
			--	CASE WHEN @IdTechnicalScenario=1 THEN CASE WHEN #UtilizationCapacityValues.UtilizationValue < #VolumeReportGrouped.Amount THEN (CPT.TFNormal * CPT.BCRNormal) * @CurrencyFactor ELSE 0 END
			--		WHEN @IdTechnicalScenario=2 THEN CASE WHEN #UtilizationCapacityValues.UtilizationValue < #VolumeReportGrouped.Amount THEN (CPT.TFOptimistic * CPT.BCROptimistic) * @CurrencyFactor ELSE 0 END
			--		WHEN @IdTechnicalScenario=3 THEN CASE WHEN #UtilizationCapacityValues.UtilizationValue < #VolumeReportGrouped.Amount THEN (CPT.TFPessimistic * CPT.BCRPessimistic) * @CurrencyFactor ELSE 0 END 
			--		ELSE 0 
			--	END 
			--ELSE
				CASE WHEN @IdTechnicalScenario=1 THEN CPT.TFNormal * CPT.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN CPT.TFOptimistic * CPT.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN CPT.TFPessimistic * CPT.BCRPessimistic 
					ELSE 0 
				END 
			* @CurrencyFactor / 
				CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
					WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
					WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
					ELSE 1 
				END 
			/ CASE WHEN @IdCostType=1 THEN 1
				  WHEN @IdCostType=2 THEN #VolumeReportBO.Amount
				  ELSE 1 
			  END
			--END
		) AS CostProjection, P.SortOrder
	FROM tblCalcProjectionTechnical CPT  LEFT OUTER JOIN
		#TechDriver ON CPT.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON CPT.Year = tblCalcProjectionEconomic.Year AND 
		CPT.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		CPT.IdModel = tblCalcProjectionEconomic.IdModel JOIN #ProjectionCriteriaList P ON
		CPT.ProjectionCriteria = P.Code LEFT OUTER JOIN
		#VolumeReportBO ON #VolumeReportBO.YearProjection = CPT.Year AND
		#VolumeReportBO.Name = CPT.Project LEFT OUTER JOIN
		#UtilizationCapacityValues ON #UtilizationCapacityValues.CodeZiff = CPT.CodeZiff AND
		#UtilizationCapacityValues.IdField=CPT.IdField LEFT OUTER JOIN
		#VolumeReportGrouped ON #VolumeReportGrouped.YearProjection = CPT.Year
	WHERE CPT.IdModel=@IdModel
	  AND CPT.IdField IN (SELECT [IdField] FROM #FieldList) 
	  --AND CPT.BaseCostType=1
	  AND CPT.Operation = 2
	  AND CPT.Year BETWEEN @StartYear AND @EndYear
	GROUP BY CPT.Field + ' ('+ CPT.Project + ')',CPT.Field, CPT.Project, P.Name, CPT.Year, P.SortOrder
	
	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #BLList (FieldProject NVARCHAR(255), Field NVARCHAR(255), Project NVARCHAR(255), ProjectionCriteria NVARCHAR(255), YearProjection INT, Total FLOAT, SortOrder INT) ON [PRIMARY]
	INSERT INTO #BLList ([FieldProject],[Field],[Project],[ProjectionCriteria],[YearProjection],[Total],[SortOrder])
	SELECT CNList.FieldProject, CNList.Field, CNList.Project, CPList.Name, CYList.Year, 0, CPList.SortOrder FROM
		(SELECT [FieldProject],[Field],[Project] FROM #BL GROUP BY [FieldProject],[Field],[Project])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList CROSS JOIN
		(SELECT [NAME], [SortOrder] FROM #ProjectionCriteriaList) AS CPList

	CREATE TABLE #BOList (FieldProject NVARCHAR(255), Field NVARCHAR(255), Project NVARCHAR(255), ProjectionCriteria NVARCHAR(255), YearProjection INT, Total FLOAT, SortOrder INT) ON [PRIMARY]
	INSERT INTO #BOList ([FieldProject],[Field],[Project],[ProjectionCriteria],[YearProjection],[Total],[SortOrder])
	SELECT CNList.FieldProject, CNList.Field, CNList.Project, CPList.Name, CYList.Year, 0, CPList.SortOrder FROM
		(SELECT [FieldProject],[Field],[Project] FROM #BO GROUP BY [FieldProject],[Field],[Project])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList CROSS JOIN
		(SELECT [NAME], [SortOrder] FROM #ProjectionCriteriaList) AS CPList

	/** Add Any Missing Years to Make the Chart Fill Properly (eg. Fields with only Cyclical Drivers) **/
	INSERT INTO #BL([FieldProject],[Field],[Project],[ProjectionCriteria],[YearProjection],[Total],[CostProjection],[SortOrder])
	SELECT #BLList.FieldProject, #BLList.Field, #BLList.Project, #BLList.ProjectionCriteria, #BLList.YearProjection, 0 AS Total, NULL AS CostProjection , #BLList.SortOrder
	FROM #BLList LEFT OUTER JOIN
		 #BL ON #BLList.FieldProject=#BL.FieldProject AND #BLList.Field=#BL.Field AND #BLList.Project=#BL.Project AND #BLList.ProjectionCriteria=#BL.ProjectionCriteria AND #BLList.YearProjection=#BL.YearProjection AND #BLList.SortOrder=#BL.SortOrder
	WHERE #BL.YearProjection IS NULL;	

	INSERT INTO #BO([FieldProject],[Field],[Project],[ProjectionCriteria],[YearProjection],[Total],[CostProjection],[SortOrder])
	SELECT #BOList.FieldProject, #BOList.Field, #BOList.Project, #BOList.ProjectionCriteria, #BOList.YearProjection, 0 AS Total, NULL AS CostProjection , #BOList.SortOrder
	FROM #BOList LEFT OUTER JOIN
		 #BO ON #BOList.FieldProject=#BO.FieldProject AND #BOList.Field=#BO.Field AND #BOList.Project=#BO.Project AND #BOList.ProjectionCriteria=#BO.ProjectionCriteria AND #BOList.YearProjection=#BO.YearProjection AND #BOList.SortOrder=#BO.SortOrder
	WHERE #BO.YearProjection IS NULL;	

	/** Build Output Table **/
	DECLARE @SQLOpexBL NVARCHAR(MAX);
	SET @SQLOpexBL = 'SELECT [FieldProject],[ProjectionCriteria],' + @YearColumnList + ' FROM #BL PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [FieldProject],[SortOrder]';
	EXEC(@SQLOpexBL);

	DECLARE @SQLOpexBO NVARCHAR(MAX);
	SET @SQLOpexBO = 'SELECT [FieldProject],[ProjectionCriteria],' + @YearColumnList + ' FROM #BO PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [FieldProject],[SortOrder]';
	EXEC(@SQLOpexBO);

	DECLARE @SQLOpexBLExport NVARCHAR(MAX);
	SET @SQLOpexBLExport = 'WITH Pivoted AS (SELECT [Field],[Project],[YearProjection],[Total],' + @ProjectionCriteriaList + ' FROM #BL PIVOT (SUM(CostProjection) FOR [ProjectionCriteria] IN (' + @ProjectionCriteriaList + ')) AS PivotTable ) SELECT [Field],[Project],[YearProjection],' + @ProjectionCriteriaList2 + ' FROM Pivoted GROUP BY [Field],[Project],[YearProjection]';
	EXEC(@SQLOpexBLExport);

	DECLARE @SQLOpexBOExport NVARCHAR(MAX);
	SET @SQLOpexBOExport = 'WITH Pivoted AS (SELECT [Field],[Project],[YearProjection],[Total],' + @ProjectionCriteriaList + ' FROM #BO PIVOT (SUM(CostProjection) FOR [ProjectionCriteria] IN (' + @ProjectionCriteriaList + ')) AS PivotTable ) SELECT [Field],[Project],[YearProjection],' + @ProjectionCriteriaList2 + ' FROM Pivoted GROUP BY [Field],[Project],[YearProjection]';
	EXEC(@SQLOpexBOExport);

END
GO
/****** Object:  StoredProcedure [dbo].[reppOpexProjection]    Script Date: 06/05/2015 10:52:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gareth Slater
-- Create date: 2014-05-05
-- Description:	Module Report Opex Projection
-- =============================================
ALTER PROCEDURE [dbo].[reppOpexProjection](


@IdModel INT,
@IdAggregationLevel INT,
@IdField INT,
@IdStructure INT,
@From INT,
@To INT,
@IdTechnicalScenario INT,
@IdEconomicScenario INT,
@IdCurrency INT,
@IdTerm INT,
@IdTechnicalDriver INT,
@IdCostType INT,
@IdTypeOperation INT
)
AS
BEGIN
	DECLARE @StartYear INT = @From;
	DECLARE @EndYear INT = @To;
	
	/** Get Base Year **/
	DECLARE @BaseYear INT
	SELECT @BaseYear = [BaseYear] FROM [DBCPM].[dbo].[tblModels] WHERE IdModel=@IdModel

	/** Build Year Columns **/
	DECLARE @YearColumnList NVARCHAR(MAX)='';	
	WHILE (@From <= @To)
	BEGIN
		SET @YearColumnList =  @YearColumnList + '[' + CAST(@From AS VARCHAR(4)) + ']';
		SET @From = @From + 1;
		IF (@From <= @To)
		BEGIN
			SET @YearColumnList = @YearColumnList + ',';
		END
	END
	
	/** Build Field List **/
	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF (@IdAggregationLevel=0 AND @IdField=0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0 AND @IdField>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdField>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	DECLARE @FieldCount INT;
	SELECT @FieldCount=COUNT([IdField]) FROM #FieldList
	
	/** Get Chart Columns to Temp Table **/
	CREATE TABLE #AccountList (ColumnName NVARCHAR(255) PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF @IdStructure=1
	BEGIN
		INSERT INTO #AccountList SELECT [RootZiffAccount] FROM tblCalcProjectionTechnical WHERE IdField IN (SELECT [IdField] FROM #FieldList) AND [IdModel]=@IdModel GROUP BY [RootZiffAccount] ORDER BY [RootZiffAccount];
	END
	ELSE IF @IdStructure=2
	BEGIN
		INSERT INTO #AccountList SELECT [Activity] FROM tblCalcProjectionTechnical WHERE IdField IN (SELECT [IdField] FROM #FieldList) AND [IdModel]=@IdModel GROUP BY [Activity], [SortOrder] ORDER BY [SortOrder], [Activity];
	END
	
	/** Generate Chart Columns from Temp Table **/
	DECLARE @AccountColumnList NVARCHAR(MAX)='';
	DECLARE @AccountName NVARCHAR(255);
	DECLARE AccountList CURSOR FOR
	SELECT ColumnName FROM #AccountList;

	OPEN AccountList;
	FETCH NEXT FROM AccountList INTO @AccountName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @AccountColumnList=''
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + '[' + @AccountName + ']';
		END
		ELSE
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + ',[' + @AccountName + ']';
		END
		FETCH NEXT FROM AccountList INTO @AccountName;
	END

	CLOSE AccountList;
	DEALLOCATE AccountList;
	
	/** Get Currency Factor to use **/
	DECLARE @CurrencyFactor FLOAT;
	SELECT @CurrencyFactor=[Value] FROM tblModelsCurrencies WHERE [IdCurrency]=@IdCurrency AND [IdModel]=@IdModel;
	
	/** Build Technical Driver Factors (used for KPI Report) **/
	CREATE TABLE #TechDriver ([Year] INT, [Normal] FLOAT, [Optimistic] FLOAT, [Pessimistic] FLOAT) ON [PRIMARY]
	IF @IdTechnicalDriver=0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, 1 AS [Normal], 1 AS [Optimistic], 1 AS [Pessimistic]
		FROM tblCalcModelYears
		WHERE [IdModel]=@IdModel AND [Year] BETWEEN @StartYear AND @EndYear;
	END
	ELSE IF @IdTechnicalDriver>0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, SUM([Normal]) AS [Normal], SUM([Pessimistic]) AS [Pessimistic], SUM([Optimistic]) AS [Optimistic]
		FROM tblTechnicalDriverData
		WHERE [IdTechnicalDriver]=@IdTechnicalDriver AND [IdModel]=@IdModel AND IdField IN (SELECT [IdField] FROM #FieldList)
		GROUP BY Year;
	END
	
	/** Build Volume Tables **/
	/** Report (Baseline)**/
	CREATE TABLE #VolumeReportBLRows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLRows ([ParentName], [Name], [YearProjection], [Amount])	
		SELECT	DISTINCT N'All Cost Accounts' + 
			CASE WHEN @IdTechnicalScenario=1 THEN 'Base' 
				WHEN @IdTechnicalScenario=2 THEN 'Optimistic'
	 			WHEN @IdTechnicalScenario=3 THEN 'Pessimistic'
			END AS ParentName, dbo.tblProjects.Name AS Name, 
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN ISNULL(dbo.tblTechnicalDriverData.Normal,0)
				WHEN @IdTechnicalScenario=2 THEN ISNULL(dbo.tblTechnicalDriverData.Optimistic,0)
				WHEN @IdTechnicalScenario=3 THEN ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0)
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON
			dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=1
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	ORDER BY dbo.tblTechnicalDriverData.Year
	
	CREATE TABLE #VolumeReportBL ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBL([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBLRows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBLList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBL WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #BLList to Fill Properly **/
	INSERT INTO #VolumeReportBL ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBLList.ParentName,#VolumeReportBLList.Name, #VolumeReportBLList.YearProjection,0 AS Amount
	FROM	#VolumeReportBLList LEFT OUTER JOIN
			#VolumeReportBL ON #VolumeReportBLList.ParentName=#VolumeReportBL.ParentName AND #VolumeReportBLList.Name=#VolumeReportBL.Name AND #VolumeReportBLList.YearProjection=#VolumeReportBL.YearProjection
	WHERE	#VolumeReportBL.YearProjection IS NULL;

	/** Report (Business Opportunities)**/
	CREATE TABLE #VolumeReportBORows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBORows ([ParentName], [Name], [YearProjection], [Amount])	
		SELECT	DISTINCT N'All Cost Accounts' + 
			CASE WHEN @IdTechnicalScenario=1 THEN 'Base' 
				WHEN @IdTechnicalScenario=2 THEN 'Optimistic'
	 			WHEN @IdTechnicalScenario=3 THEN 'Pessimistic'
			END AS ParentName, dbo.tblProjects.Name AS Name, 
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN ISNULL(dbo.tblTechnicalDriverData.Normal,0)
				WHEN @IdTechnicalScenario=2 THEN ISNULL(dbo.tblTechnicalDriverData.Optimistic,0)
				WHEN @IdTechnicalScenario=3 THEN ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0)
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON
			dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=2
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	ORDER BY dbo.tblTechnicalDriverData.Year

	CREATE TABLE #VolumeReportBO ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBO([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBORows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBOList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBOList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBO WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #BOList to Fill Properly **/
	INSERT INTO #VolumeReportBO ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBOList.ParentName,#VolumeReportBOList.Name, #VolumeReportBOList.YearProjection,0 AS Amount
	FROM	#VolumeReportBOList LEFT OUTER JOIN
			#VolumeReportBO ON #VolumeReportBOList.ParentName=#VolumeReportBO.ParentName AND #VolumeReportBOList.Name=#VolumeReportBO.Name AND #VolumeReportBOList.YearProjection=#VolumeReportBO.YearProjection
	WHERE	#VolumeReportBO.YearProjection IS NULL;

	/** Report (All Fields Grouped)**/
	CREATE TABLE #VolumeReportGroupedBL ([YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBL ([YearProjection], [Amount])
	SELECT YearProjection,SUM(amount) AS Amount
	FROM #VolumeReportBL
	GROUP BY YearProjection
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGroupedBO ([YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBO ([YearProjection], [Amount])
	SELECT YearProjection,SUM(amount) AS Amount
	FROM #VolumeReportBO
	GROUP BY YearProjection
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGrouped ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255),[YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGrouped ([ParentName], [Name], [YearProjection], [Amount])
	SELECT N'All Cost Accounts' AS ParentName, 
		   CASE WHEN @IdTechnicalScenario=1 THEN 'Base' 
				WHEN @IdTechnicalScenario=2 THEN 'Optimistic'
				WHEN @IdTechnicalScenario=3 THEN 'Pessimistic'
		   END AS Name,
		   #VolumeReportGroupedBL.YearProjection,
		   #VolumeReportGroupedBL.Amount + #VolumeReportGroupedBO.Amount AS AMOUNT
	FROM #VolumeReportGroupedBL INNER JOIN	#VolumeReportGroupedBO ON #VolumeReportGroupedBO.YearProjection = #VolumeReportGroupedBL.YearProjection		   

	DECLARE @TotalCapacityDriver INT = 0;

	/** Get grouped driver value for Base Year **/
	SELECT @TotalCapacityDriver = Amount FROM #VolumeReportGrouped WHERE YearProjection=@BaseYear

	/** Build Utilization Capacity Values **/
	CREATE TABLE #UtilizationCapacityValues ([CodeZiff] NVARCHAR(50), [IdField] INT, [UtilizationValue] FLOAT) ON [PRIMARY]
	INSERT INTO #UtilizationCapacityValues([CodeZiff], [IdField], [UtilizationValue])
	SELECT  A.[CodeZiff], tblFields.IdField, @TotalCapacityDriver/A.UtilizationValue*100
	  FROM tblUtilizationCapacity A INNER JOIN tblFields
			ON A.IdField = tblFields.IdField      
	WHERE  	A.IdModel = @IdModel
	AND		A.IdField = @IdField

	/** Build Source Data Table - Total **/
	CREATE TABLE #Opex (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #Opex ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS ParentName, 
		CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
		tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
		) AS CostProjection
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList)
	GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END, tblCalcProjectionTechnical.Year;
	
	/** Build Source Data Table - Chart **/
	CREATE TABLE #Chart (ParentName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #Chart ([ParentName],[YearProjection],[CostProjection])
	SELECT CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS ParentName, 
		tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
		) AS CostProjection
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, tblCalcProjectionTechnical.Year;
	
	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #ChartList (ParentName NVARCHAR(255), YearProjection INT) ON [PRIMARY]
	INSERT INTO #ChartList ([ParentName],[YearProjection])
	SELECT CNList.ParentName, CYList.Year FROM
		(SELECT [ParentName] FROM #Chart GROUP BY [ParentName])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList
	
	/** Add Any Missing Years to Make the Chart Fill Properly (eg. Fields with only Cyclical Drivers) **/
	INSERT INTO #Chart ([ParentName],[YearProjection],[CostProjection])
	SELECT #ChartList.ParentName, #ChartList.YearProjection, NULL AS CostProjection FROM
		#ChartList LEFT OUTER JOIN
		#Chart ON #ChartList.ParentName=#Chart.ParentName AND #ChartList.YearProjection=#Chart.YearProjection
	WHERE #Chart.YearProjection IS NULL;	
	
	IF @IdTechnicalDriver=0
	BEGIN
		/** Build Source Data Table - Baseline and Business Opportunities (No Techical KPI Required) **/
		CREATE TABLE #BL (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
		CREATE TABLE #BO (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
		IF @FieldCount = 1
		BEGIN
			/** Baseline **/
			INSERT INTO #BL ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=1 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, tblCalcProjectionTechnical.Year;
			
			/** Business Opportunities **/
			INSERT INTO #BO ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=2 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, tblCalcProjectionTechnical.Year;
		END
		ELSE IF @FieldCount <> 1
		BEGIN
			/** Baseline **/
			INSERT INTO #BL ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, Project AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=1 
			GROUP BY tblCalcProjectionTechnical.Project, tblCalcProjectionTechnical.Year;
			
			/** Business Opportunities **/
			INSERT INTO #BO ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, Project Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=2 
			GROUP BY tblCalcProjectionTechnical.Project, tblCalcProjectionTechnical.Year;
		END
		
		/* NEED to do calculations for SEMI-VARIABLE */
		
	END

	/** Add Unit Cost factor, IF NECESSARY **/
	IF @IdCostType = 2
	BEGIN		 
		IF @IdField <>0
		BEGIN		
			UPDATE	#BL
			SET		#BL.CostProjection = #BL.CostProjection/#VolumeReportBL.Amount
			FROM	#BL INNER JOIN #VolumeReportBL ON
					#BL.YearProjection = #VolumeReportBL.YearProjection 

			UPDATE	#BO
			SET		#BO.CostProjection = #BO.CostProjection/#VolumeReportBO.Amount
			FROM	#BO INNER JOIN #VolumeReportBO ON
					#BO.YearProjection = #VolumeReportBO.YearProjection 
		END
		ELSE
		BEGIN
			UPDATE	#BL
			SET		#BL.CostProjection = #BL.CostProjection/#VolumeReportBL.Amount
			FROM	#BL INNER JOIN #VolumeReportBL ON
					#BL.YearProjection = #VolumeReportBL.YearProjection AND #BL.Name=#VolumeReportBL.Name

			UPDATE	#BO
			SET		#BO.CostProjection = #BO.CostProjection/#VolumeReportBO.Amount
			FROM	#BO INNER JOIN #VolumeReportBO ON
					#BO.YearProjection = #VolumeReportBO.YearProjection AND #BO.Name=#VolumeReportBO.Name
		END
		UPDATE	#Opex
		SET		#Opex.CostProjection = #Opex.CostProjection/#VolumeReportGrouped.Amount
		FROM	#Opex INNER JOIN #VolumeReportGrouped ON
				#Opex.YearProjection = #VolumeReportGrouped.YearProjection
	END
		
	


	
	/** Build Output Table **/
	DECLARE @SQLOpex NVARCHAR(MAX);
	SET @SQLOpex = 'SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #Opex PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
	EXEC(@SQLOpex);
	DECLARE @SQLOpexChart NVARCHAR(MAX);
	SET @SQLOpexChart = 'SELECT [YearProjection] AS [Year],' + @AccountColumnList + ' FROM #Chart PIVOT (SUM(CostProjection) FOR ParentName IN (' + @AccountColumnList + ')) AS PivotTable ORDER BY [YearProjection]';
	EXEC(@SQLOpexChart);
	IF @IdTechnicalDriver=0
	BEGIN
		DECLARE @SQLOpexBL NVARCHAR(MAX);
		SET @SQLOpexBL = 'SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #BL PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
		EXEC(@SQLOpexBL);
		DECLARE @SQLOpexBO NVARCHAR(MAX);
		SET @SQLOpexBO = 'SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #BO PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
		EXEC(@SQLOpexBO);
	END

	DECLARE @SQLVolumeTotal NVARCHAR(MAX);
	SET @SQLVolumeTotal = 'SELECT [ParentName], [Name],' + @YearColumnList + ' FROM #VolumeReportGrouped PIVOT (SUM(Amount) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
	EXEC(@SQLVolumeTotal);

	DECLARE @SQLVolumeBL NVARCHAR(MAX);
	SET @SQLVolumeBL = 'SELECT [ParentName], [Name],' + @YearColumnList + ' FROM #VolumeReportBL PIVOT (SUM(Amount) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
	EXEC(@SQLVolumeBL);
	
	DECLARE @SQLVolumeBO NVARCHAR(MAX);
	SET @SQLVolumeBO = 'SELECT [ParentName], [Name],' + @YearColumnList + ' FROM #VolumeReportBO PIVOT (SUM(Amount) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName],[Name]';
	EXEC(@SQLVolumeBO);
	
	/** Output Stats Table for UI **/
	IF @IdTechnicalDriver=0
	BEGIN
		SELECT @FieldCount AS FieldCount;
	END
	
END
GO

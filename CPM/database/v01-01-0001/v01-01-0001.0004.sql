GO
USE [DBCPM]
GO
/****** Object:  StoredProcedure [dbo].[tecpUtilizationCapacity]    Script Date: 06/18/2015 11:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-03-19
-- Description:	Utilization Capacity Report per Field
-- ============================================================
ALTER PROCEDURE [dbo].[tecpUtilizationCapacity](
@IdModel int,
@IdStage int,
@BaseCostType int
)
AS
BEGIN
	
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#FieldList') IS NOT NULL DROP TABLE #FieldList
	IF OBJECT_ID('tempdb..#UtilizationCapacity') IS NOT NULL DROP TABLE #UtilizationCapacity
	IF OBJECT_ID('tempdb..#tblCalcTechnicalBaseCostRate') IS NOT NULL DROP TABLE #tblCalcTechnicalBaseCostRate

	/** Build Field List **/
	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL, Name NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #FieldList SELECT A.[IdField], B.Name FROM tblCalcTechnicalBaseCostRate A INNER JOIN tblFields B ON A.IdField=B.IdField WHERE A.[IdModel]=@IdModel GROUP BY A.[IdField], B.Name;

	/** Build Field Column **/
	DECLARE @FieldColumnList NVARCHAR(MAX)='';
	DECLARE @FieldName NVARCHAR(255);
	DECLARE FieldList CURSOR FOR
	SELECT Name FROM #FieldList;

	OPEN FieldList;
	FETCH NEXT FROM FieldList INTO @FieldName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @FieldColumnList=''
		BEGIN
			SET @FieldColumnList =  @FieldColumnList + '[' + @FieldName  + ']';
		END
		ELSE
		BEGIN
			SET @FieldColumnList =  @FieldColumnList + ',[' + @FieldName  + ']';
		END
		FETCH NEXT FROM FieldList INTO @FieldName;
	END

	CLOSE FieldList;
	DEALLOCATE FieldList;

	CREATE TABLE #UtilizationCapacity (CodeZiff VARCHAR(150), FieldName NVARCHAR(255), SizeValue FLOAT, CodeZiffExport VARCHAR(150), SortOrder NVARCHAR(100))
	INSERT INTO #UtilizationCapacity (CodeZiff,FieldName,SizeValue,CodeZiffExport,SortOrder)
	SELECT  dbo.svfLevelStringSpacer(dbo.tblZiffAccounts.RelationLevel) + dbo.tblZiffAccounts.Code + N': ' + dbo.tblZiffAccounts.Name AS DisplayName
		  , tblFields.Name
		  , A.UtilizationValue
		  , A.[CodeZiff] + ': ' + A.ZiffAccount AS CodeZiffExport
		  , tblZiffAccounts.SortOrder
	  FROM tblUtilizationCapacity A INNER JOIN tblFields
			ON A.IdField = tblFields.IdField LEFT OUTER JOIN tblZiffAccounts 
			ON tblZiffAccounts.Code=A.CodeZiff
	WHERE  	A.IdModel = @IdModel
	--AND		A.IdStage = @IdStage 
	--AND		A.BaseCostType = @BaseCostType
	--GROUP BY A.[CodeZiff] + ': ' + A.ZiffAccount,tblFields.Name,A.UtilizationValue, tblZiffAccounts.Name,tblZiffAccounts.SortOrder
	ORDER BY tblZiffAccounts.SortOrder--A.[CodeZiff] + ': ' + A.ZiffAccount

	INSERT INTO #UtilizationCapacity (CodeZiff,FieldName,SizeValue,CodeZiffExport,SortOrder)
	SELECT  ZA.[Code] AS CodeZiff
		  , #FieldList.Name
		  , '-1'
		  , ZA.[Code] AS CodeZiffExport
		  , ZA.SortOrder
	  FROM tblZiffAccounts ZA , #FieldList
	WHERE  	ZA.IdModel = @IdModel AND Parent IS NULL

	--/** Build Output Table **/
	DECLARE @SQLUtilizationCapacity NVARCHAR(MAX);
	SET @SQLUtilizationCapacity = 'SELECT * FROM #UtilizationCapacity PIVOT (Sum(SizeValue) FOR FieldName IN (' + @FieldColumnList + ')) AS PivotTable ORDER BY SortOrder';
	EXEC(@SQLUtilizationCapacity);

END
GO
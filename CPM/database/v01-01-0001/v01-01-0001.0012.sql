USE [DBCPM]
GO
/****** Object:  StoredProcedure [dbo].[tecpUtilizationCapacity]    Script Date: 08/12/2015 14:57:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- ================================================================================================ 
-- Author:		Rodrigo Manubens
-- Create date: 2015-03-19
-- Description:	Utilization Capacity Report per Field
-- ================================================================================================ 
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 2015-08-04	RM		Added a return of the numbers of rows existing in table for that IdModel
--						used in code to check whether we need to create records or not.
-- ================================================================================================ 
ALTER PROCEDURE [dbo].[tecpUtilizationCapacity](
@IdModel int,
@IdStage int,
@BaseCostType int
)
AS
BEGIN
	
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#FieldList') IS NOT NULL DROP TABLE #FieldList
	IF OBJECT_ID('tempdb..#UtilizationCapacity') IS NOT NULL DROP TABLE #UtilizationCapacity
	IF OBJECT_ID('tempdb..#tblCalcTechnicalBaseCostRate') IS NOT NULL DROP TABLE #tblCalcTechnicalBaseCostRate

	DECLARE @TotalRows INT = 0
	
	/** Build Field List **/
	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL, Name NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #FieldList SELECT A.[IdField], B.Name FROM tblCalcTechnicalBaseCostRate A INNER JOIN tblFields B ON A.IdField=B.IdField WHERE A.[IdModel]=@IdModel GROUP BY A.[IdField], B.Name;

	/** Build Field Column **/
	DECLARE @FieldColumnList NVARCHAR(MAX)='';
	DECLARE @FieldName NVARCHAR(255);
	DECLARE FieldList CURSOR FOR
	SELECT Name FROM #FieldList;

	OPEN FieldList;
	FETCH NEXT FROM FieldList INTO @FieldName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @FieldColumnList=''
		BEGIN
			SET @FieldColumnList =  @FieldColumnList + '[' + @FieldName  + ']';
		END
		ELSE
		BEGIN
			SET @FieldColumnList =  @FieldColumnList + ',[' + @FieldName  + ']';
		END
		FETCH NEXT FROM FieldList INTO @FieldName;
	END

	CLOSE FieldList;
	DEALLOCATE FieldList;

	CREATE TABLE #UtilizationCapacity (CodeZiff VARCHAR(150), FieldName NVARCHAR(255), SizeValue FLOAT, CodeZiffExport VARCHAR(150), SortOrder NVARCHAR(100))
	INSERT INTO #UtilizationCapacity (CodeZiff,FieldName,SizeValue,CodeZiffExport,SortOrder)
	SELECT  dbo.svfLevelStringSpacer(dbo.tblZiffAccounts.RelationLevel) + dbo.tblZiffAccounts.Code + N': ' + dbo.tblZiffAccounts.Name AS DisplayName
		  , tblFields.Name
		  , A.UtilizationValue
		  , A.[CodeZiff] + ': ' + A.ZiffAccount AS CodeZiffExport
		  , tblZiffAccounts.SortOrder
	  FROM tblUtilizationCapacity A INNER JOIN tblFields
			ON A.IdField = tblFields.IdField LEFT OUTER JOIN tblZiffAccounts 
			ON tblZiffAccounts.Code=A.CodeZiff
	WHERE  	A.IdModel = @IdModel
	GROUP BY dbo.svfLevelStringSpacer(dbo.tblZiffAccounts.RelationLevel) + dbo.tblZiffAccounts.Code + N': ' + dbo.tblZiffAccounts.Name,tblFields.Name,A.UtilizationValue, A.[CodeZiff] + ': ' + A.ZiffAccount,tblZiffAccounts.SortOrder
	ORDER BY tblZiffAccounts.SortOrder

	SELECT @TotalRows = COUNT(*) from #UtilizationCapacity

	INSERT INTO #UtilizationCapacity (CodeZiff,FieldName,SizeValue,CodeZiffExport,SortOrder)
	SELECT  dbo.svfLevelStringSpacer(ZA.RelationLevel) + ZA.Code + N': ' + ZA.Name/*ZA.[Code]*/ AS CodeZiff
		  , #FieldList.Name
		  , '-1'
		  , dbo.svfLevelStringSpacer(ZA.RelationLevel) + ZA.Code + N': ' + ZA.Name/*ZA.[Code]*/ AS CodeZiffExport
		  , ZA.SortOrder
	  FROM tblZiffAccounts ZA , #FieldList
	WHERE  	ZA.IdModel = @IdModel AND Parent IS NULL

	--/** Build Output Table **/
	DECLARE @SQLUtilizationCapacity NVARCHAR(MAX);
	SET @SQLUtilizationCapacity = 'SELECT * FROM #UtilizationCapacity PIVOT (Sum(SizeValue) FOR FieldName IN (' + @FieldColumnList + ')) AS PivotTable ORDER BY SortOrder';
	EXEC(@SQLUtilizationCapacity);

	SELECT @TotalRows AS TotalRows
END


GO

USE [DBCPM]
GO

/****** Object:  StoredProcedure [dbo].[calcModuleTechnical]    Script Date: 08/12/2015 15:00:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Gareth Slater
-- Create date: 2014-04-03
-- Description:	Module Technical Calc
-- ================================================================================================ 
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 2015-08-04	RM		Added a new code to poulate table tblCalcBaselineUnitCostDenominatorSumAmount
-- ================================================================================================	
/****** Object:  StoredProcedure [dbo].[calcModuleTechnical]    Script Date: 2015-08-05 10:01:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gareth Slater
-- Create date: 2014-04-03
-- Description:	Module Technical Calc
-- =============================================
ALTER PROCEDURE [dbo].[calcModuleTechnical](
@IdModel int
)
AS
BEGIN
	
	DECLARE @intMinYear INT;
	DECLARE @intMaxYear INT;
	SELECT @intMinYear=BaseYear, @intMaxYear=BaseYear+Projection FROM tblModels WHERE [IdModel]=@IdModel;
	
	DECLARE @intLoop int = 0;
	DECLARE @intMaxLevel int = 0;
	SELECT @intMaxLevel = MAX([RelationLevel])+1 FROM tblZiffAccounts WHERE [IdModel]=@IdModel;
	
	/** Precalc **/
	EXEC calcModuleTechnicalPreCalc @IdModel;
	
	/** Cleanup Calc Tables for New Data **/
	DELETE FROM tblCalcModelMultiBaseline WHERE IdModel=@IdModel;
	DELETE FROM tblCalcUnitCostDenominatorSumAmount WHERE IdModel=@IdModel;
	DELETE FROM tblCalcUnitCostDenominatorAmount WHERE IdModel=@IdModel;
	DELETE FROM tblCalcTechnicalDriverData WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalAssumption WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalDriverAssumption WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcBaselineUnitCostDenominatorSumAmount WHERE IdModel=@IdModel;

	INSERT INTO tblCalcModelMultiBaseline ([IdModel],[IdField])
	SELECT tblFields.IdModel, tblFields.IdField
	FROM tblFields INNER JOIN
		tblProjects ON tblFields.IdField = tblProjects.IdField
	GROUP BY tblFields.IdModel, tblFields.IdField, tblProjects.Operation
	HAVING SUM(1)>1 AND tblProjects.Operation=1 AND tblFields.IdModel=@IdModel;

	INSERT INTO tblCalcUnitCostDenominatorSumAmount ([IdModel],[IdField],[Year],[SumNormal],[SumPessimistic],[SumOptimistic])
	SELECT tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year, SUM(tblTechnicalDriverData.Normal) AS SumNormal, 
		SUM(tblTechnicalDriverData.Pessimistic) AS SumPessimistic, SUM(tblTechnicalDriverData.Optimistic) AS SumOptimistic
	FROM tblTechnicalDriverData INNER JOIN
		tblTechnicalDrivers ON tblTechnicalDriverData.IdTechnicalDriver = tblTechnicalDrivers.IdTechnicalDriver AND 
		tblTechnicalDriverData.IdModel = tblTechnicalDrivers.IdModel INNER JOIN
		tblCalcModelMultiBaseline ON tblTechnicalDriverData.IdModel = tblCalcModelMultiBaseline.IdModel AND 
		tblTechnicalDriverData.IdField = tblCalcModelMultiBaseline.IdField
	WHERE tblTechnicalDrivers.UnitCostDenominator=1
	GROUP BY tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year
	HAVING tblTechnicalDrivers.IdModel=@IdModel;

	INSERT INTO tblCalcUnitCostDenominatorAmount ([IdModel],[IdField],[IdProject],[Year],[SumNormal],[SumPessimistic],[SumOptimistic])
	SELECT tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.IdProject, tblTechnicalDriverData.Year, 
		SUM(tblTechnicalDriverData.Normal) AS SumNormal, SUM(tblTechnicalDriverData.Pessimistic) AS SumPessimistic, 
		SUM(tblTechnicalDriverData.Optimistic) AS SumOptimistic
	FROM tblTechnicalDriverData INNER JOIN
		tblTechnicalDrivers ON tblTechnicalDriverData.IdTechnicalDriver = tblTechnicalDrivers.IdTechnicalDriver AND 
		tblTechnicalDriverData.IdModel = tblTechnicalDrivers.IdModel INNER JOIN
		tblCalcModelMultiBaseline ON tblTechnicalDriverData.IdModel = tblCalcModelMultiBaseline.IdModel AND 
		tblTechnicalDriverData.IdField = tblCalcModelMultiBaseline.IdField
	WHERE tblTechnicalDrivers.UnitCostDenominator=1
	GROUP BY tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year, tblTechnicalDriverData.IdProject
	HAVING tblTechnicalDrivers.IdModel=@IdModel;

	INSERT INTO tblCalcBaselineUnitCostDenominatorSumAmount ([IdModel],[IdField],[Year],[SumNormal],[SumPessimistic],[SumOptimistic])
	SELECT tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year, SUM(tblTechnicalDriverData.Normal) AS SumNormal, 
		SUM(tblTechnicalDriverData.Pessimistic) AS SumPessimistic, SUM(tblTechnicalDriverData.Optimistic) AS SumOptimistic
	FROM tblTechnicalDriverData INNER JOIN
		tblTechnicalDrivers ON tblTechnicalDriverData.IdTechnicalDriver = tblTechnicalDrivers.IdTechnicalDriver AND 
		tblTechnicalDriverData.IdModel = tblTechnicalDrivers.IdModel INNER JOIN
		tblCalcModelMultiBaseline ON tblTechnicalDriverData.IdModel = tblCalcModelMultiBaseline.IdModel AND 
		tblTechnicalDriverData.IdField = tblCalcModelMultiBaseline.IdField right outer join 
		tblProjects  ON tblTechnicalDriverData.IdModel = tblProjects.IdModel AND
		tblTechnicalDriverData.IdField = tblProjects.IdField AND 
		tblTechnicalDriverData.IdProject = tblProjects.IdProject
	WHERE tblTechnicalDrivers.UnitCostDenominator=1
	AND tblProjects.Operation=1
	GROUP BY tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year
	HAVING tblTechnicalDrivers.IdModel=@IdModel;

	/** Create Base for Technical Driver Data **/
	INSERT INTO tblCalcTechnicalDriverData ([IdModel],[IdTechnicalDriver],[IdField],[IdProject],[IdTypeOperation],[Year],[Normal],[Pessimistic],[Optimistic])
	SELECT tblTechnicalDriverData.IdModel, tblTechnicalDriverData.IdTechnicalDriver, tblTechnicalDriverData.IdField, tblTechnicalDriverData.IdProject, 
		tblFields.IdTypeOperation, tblTechnicalDriverData.Year, tblTechnicalDriverData.Normal, tblTechnicalDriverData.Pessimistic, tblTechnicalDriverData.Optimistic
	FROM tblTechnicalDriverData INNER JOIN
		tblFields ON tblTechnicalDriverData.IdField = tblFields.IdField AND tblTechnicalDriverData.IdModel = tblFields.IdModel
	WHERE tblTechnicalDriverData.IdModel=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- SETUP HIERARCHY OF ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Set Hierarchy of Technical Assumptions (Specific TypeOperation) **/
	SET @intLoop = @intMaxLevel;
	WHILE @intLoop > 0 BEGIN
		SET @intLoop = @intLoop - 1
		
		INSERT INTO tblCalcTechnicalAssumption ([IdModel],[IdZiffAccount],[IdTypeOperation],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria])
		SELECT tblTechnicalAssumption.IdModel, tblTechnicalAssumption.IdZiffAccount, tblTechnicalAssumption.IdTypeOperation, 
			tblTechnicalAssumption.IdTechnicalDriverSize, tblTechnicalAssumption.IdTechnicalDriverPerformance, 
			tblTechnicalAssumption.CycleValue, tblTechnicalAssumption.ProjectionCriteria
		FROM tblTechnicalAssumption INNER JOIN
			tblZiffAccounts ON tblTechnicalAssumption.IdZiffAccount = tblZiffAccounts.IdZiffAccount AND 
			tblTechnicalAssumption.IdModel = tblZiffAccounts.IdModel
		WHERE tblTechnicalAssumption.IdTypeOperation<>0 AND tblZiffAccounts.RelationLevel=@intLoop AND tblTechnicalAssumption.IdModel=@IdModel;
	END
	
	SET @intLoop = @intMaxLevel;
	WHILE @intLoop > 0 BEGIN
		SET @intLoop = @intLoop - 1
		
		INSERT INTO tblCalcTechnicalAssumption ([IdModel],[IdZiffAccount],[IdTypeOperation],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria])
		SELECT tblTechnicalAssumption.IdModel, tblZiffAccounts.IdZiffAccount, tblTechnicalAssumption.IdTypeOperation, 
			tblTechnicalAssumption.IdTechnicalDriverSize, tblTechnicalAssumption.IdTechnicalDriverPerformance, 
			tblTechnicalAssumption.CycleValue, tblTechnicalAssumption.ProjectionCriteria
		FROM tblTechnicalAssumption INNER JOIN
			tblCalcZiffAccountParentChild ON tblTechnicalAssumption.IdZiffAccount = tblCalcZiffAccountParentChild.IdParent AND 
			tblTechnicalAssumption.IdModel = tblCalcZiffAccountParentChild.IdModel INNER JOIN
			tblZiffAccounts ON tblCalcZiffAccountParentChild.IdChild = tblZiffAccounts.IdZiffAccount AND 
			tblCalcZiffAccountParentChild.IdModel = tblZiffAccounts.IdModel INNER JOIN
			tblZiffAccounts AS tblZiffAccountsParent ON tblTechnicalAssumption.IdZiffAccount = tblZiffAccountsParent.IdZiffAccount
		WHERE tblTechnicalAssumption.IdTypeOperation<>0 AND tblZiffAccountsParent.RelationLevel=@intLoop AND tblTechnicalAssumption.IdModel=@IdModel AND tblZiffAccounts.IdZiffAccount NOT IN (SELECT CTA.IdZiffAccount FROM tblCalcTechnicalAssumption AS CTA WHERE CTA.IdTypeOperation=tblTechnicalAssumption.IdTypeOperation AND CTA.IdModel=@IdModel);
	END
	
	/** Set Hierarchy of Technical Assumptions (General/All TypeOperation) **/
	SET @intLoop = @intMaxLevel;
	WHILE @intLoop > 0 BEGIN
		SET @intLoop = @intLoop - 1
		
		INSERT INTO tblCalcTechnicalAssumption ([IdModel],[IdZiffAccount],[IdTypeOperation],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria])
		SELECT tblTechnicalAssumption.IdModel, tblTechnicalAssumption.IdZiffAccount, TypeOpList.IdTypeOperation, tblTechnicalAssumption.IdTechnicalDriverSize, 
			tblTechnicalAssumption.IdTechnicalDriverPerformance, 
			tblTechnicalAssumption.CycleValue, tblTechnicalAssumption.ProjectionCriteria
		FROM tblTechnicalAssumption INNER JOIN
			tblZiffAccounts ON tblTechnicalAssumption.IdZiffAccount = tblZiffAccounts.IdZiffAccount AND 
			tblTechnicalAssumption.IdModel = tblZiffAccounts.IdModel CROSS JOIN
			  (SELECT IdTypeOperation FROM tblFields WHERE (IdModel=@IdModel) GROUP BY IdTypeOperation) AS TypeOpList
		WHERE tblTechnicalAssumption.IdTypeOperation=0 AND tblZiffAccounts.RelationLevel=@intLoop AND tblTechnicalAssumption.IdModel=@IdModel AND tblZiffAccounts.IdZiffAccount NOT IN (SELECT IdZiffAccount FROM tblCalcTechnicalAssumption AS CTA WHERE IdTypeOperation=TypeOpList.IdTypeOperation AND IdModel=@IdModel);
	END
	
	SET @intLoop = @intMaxLevel;
	WHILE @intLoop > 0 BEGIN
		SET @intLoop = @intLoop - 1
		
		INSERT INTO tblCalcTechnicalAssumption ([IdModel],[IdZiffAccount],[IdTypeOperation],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria])
		SELECT tblTechnicalAssumption.IdModel, tblZiffAccounts.IdZiffAccount, TypeOpList.IdTypeOperation, tblTechnicalAssumption.IdTechnicalDriverSize, 
			tblTechnicalAssumption.IdTechnicalDriverPerformance, 
			tblTechnicalAssumption.CycleValue, tblTechnicalAssumption.ProjectionCriteria
		FROM tblTechnicalAssumption INNER JOIN
			tblCalcZiffAccountParentChild ON tblTechnicalAssumption.IdZiffAccount = tblCalcZiffAccountParentChild.IdParent AND 
			tblTechnicalAssumption.IdModel = tblCalcZiffAccountParentChild.IdModel INNER JOIN
			tblZiffAccounts ON tblCalcZiffAccountParentChild.IdChild = tblZiffAccounts.IdZiffAccount AND 
			tblCalcZiffAccountParentChild.IdModel = tblZiffAccounts.IdModel INNER JOIN
			tblZiffAccounts AS tblZiffAccountsParent ON tblTechnicalAssumption.IdZiffAccount = tblZiffAccountsParent.IdZiffAccount CROSS JOIN
			  (SELECT IdTypeOperation FROM tblFields WHERE (IdModel=@IdModel) GROUP BY IdTypeOperation) AS TypeOpList
		WHERE tblTechnicalAssumption.IdTypeOperation=0 AND tblZiffAccountsParent.RelationLevel=@intLoop AND tblTechnicalAssumption.IdModel=@IdModel AND tblZiffAccounts.IdZiffAccount NOT IN (SELECT IdZiffAccount FROM tblCalcTechnicalAssumption AS CTA WHERE IdTypeOperation=TypeOpList.IdTypeOperation AND IdModel=@IdModel);
	END
	
	/** Remove all that have been marked as N/A **/
	DELETE FROM tblCalcTechnicalAssumption WHERE [ProjectionCriteria]=98 AND [IdModel]=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- SETUP COMPANY BASE AND FORECAST ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Create Company BaseYear Technical Driver Assumption Lookup **/
	DECLARE @YearBase INT;
	DECLARE YearList CURSOR FOR
	SELECT tblCostStructureAssumptionsByProject.Client AS Year
	FROM tblCostStructureAssumptionsByProject INNER JOIN tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject
	WHERE tblProjects.IdModel=@IdModel
	GROUP BY tblCostStructureAssumptionsByProject.Client
	HAVING tblCostStructureAssumptionsByProject.Client > 0
	ORDER BY Year;

	OPEN YearList;
	FETCH NEXT FROM YearList INTO @YearBase;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		INSERT INTO tblCalcTechnicalDriverAssumption ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff])
		SELECT 1, tblCalcTechnicalAssumption.IdModel, tblCalcTechnicalAssumption.IdZiffAccount, tblCalcTechnicalAssumption.IdTypeOperation, 
			tblFields.IdField, tblProjects.IdProject, @YearBase + CASE WHEN tblCalcTechnicalAssumption.ProjectionCriteria=4 AND tblCostStructureAssumptionsByProject.Client<>@intMinYear THEN tblCalcTechnicalAssumption.CycleValue-1 ELSE 0 END AS [Year] , 
			tblProjects.Operation, CAST(1 AS BIT) AS [BaseYear], tblCalcTechnicalAssumption.IdTechnicalDriverSize, tblCalcTechnicalAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalAssumption.CycleValue, 
			tblCalcTechnicalAssumption.ProjectionCriteria, tblCostStructureAssumptionsByProject.Client, tblCostStructureAssumptionsByProject.Ziff
		FROM tblCalcTechnicalAssumption LEFT OUTER JOIN
			tblProjects LEFT OUTER JOIN
			tblFields ON tblFields.IdField = tblProjects.IdField RIGHT OUTER JOIN
			tblCostStructureAssumptionsByProject ON tblProjects.IdProject = tblCostStructureAssumptionsByProject.IdProject ON 
			tblCostStructureAssumptionsByProject.IdZiffAccount = tblCalcTechnicalAssumption.IdZiffAccount AND 
			tblFields.IdTypeOperation = tblCalcTechnicalAssumption.IdTypeOperation
		WHERE tblCostStructureAssumptionsByProject.Client=@YearBase AND tblCalcTechnicalAssumption.IdModel=@IdModel;

		FETCH NEXT FROM YearList INTO @YearBase;
	END

	CLOSE YearList;
	DEALLOCATE YearList;
	
	/** Update Company BaseYear Driver Values **/
	EXEC calcModuleTechnicalDriverUpdate @IdModel,1,0;
	
	/** Update Company BaseCost Value for BaseYear **/
	UPDATE tblCalcBaseCostByField SET
		[TechApplicable]=1,
		[TFNormal] = 1,
		[TFPessimistic] = 1,
		[TFOptimistic] = 1
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField
	WHERE tblCalcTechnicalDriverAssumption.BaseYear=1 AND tblCalcTechnicalDriverAssumption.Operation=1 AND tblCalcTechnicalDriverAssumption.ProjectionCriteria IN (1,2) AND tblCalcBaseCostByField.IdModel=@IdModel;

	UPDATE tblCalcBaseCostByField SET
		tblCalcBaseCostByField.TechApplicable=1,
		tblCalcBaseCostByField.TFNormal = CBCBF.TFNormal,
		tblCalcBaseCostByField.TFPessimistic = CBCBF.TFPessimistic, 
		tblCalcBaseCostByField.TFOptimistic = CBCBF.TFOptimistic
	FROM tblCalcBaseCostByField INNER JOIN
		(
		SELECT tblCalcBaseCostByField_1.IdModel, tblCalcBaseCostByField_1.IdActivity, tblCalcBaseCostByField_1.IdResources, 
			tblCalcBaseCostByField_1.IdClientAccount, tblCalcBaseCostByField_1.IdField, tblCalcBaseCostByField_1.IdClientCostCenter, 
			tblCalcBaseCostByField_1.IdZiffAccount, SUM(CASE WHEN [SizeNormal] IS NULL 
			THEN 0 ELSE [SizeNormal] * (CASE WHEN ISNULL([PerformanceNormal], 0) = 0 THEN 1 ELSE [PerformanceNormal] END) END) AS TFNormal, 
			SUM(CASE WHEN [SizePessimistic] IS NULL THEN 0 ELSE [SizePessimistic] * (CASE WHEN ISNULL([PerformancePessimistic], 0) 
			= 0 THEN 1 ELSE [PerformancePessimistic] END) END) AS TFPessimistic, SUM(CASE WHEN [SizeOptimistic] IS NULL 
			THEN 0 ELSE [SizeOptimistic] * (CASE WHEN ISNULL([PerformanceOptimistic], 0) = 0 THEN 1 ELSE [PerformanceOptimistic] END) END) 
			AS TFOptimistic
		FROM tblCalcBaseCostByField AS tblCalcBaseCostByField_1 INNER JOIN
			tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField_1.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
			tblCalcBaseCostByField_1.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount AND 
			tblCalcBaseCostByField_1.IdField = tblCalcTechnicalDriverAssumption.IdField
			WHERE tblCalcTechnicalDriverAssumption.BaseYear=1 AND tblCalcTechnicalDriverAssumption.Operation=1 AND 
			tblCalcTechnicalDriverAssumption.ProjectionCriteria IN (3, 4, 5)
		GROUP BY tblCalcBaseCostByField_1.IdModel, tblCalcBaseCostByField_1.IdActivity, tblCalcBaseCostByField_1.IdResources, 
			tblCalcBaseCostByField_1.IdClientAccount, tblCalcBaseCostByField_1.IdField, tblCalcBaseCostByField_1.IdClientCostCenter, 
			tblCalcBaseCostByField_1.IdZiffAccount
		) AS CBCBF ON tblCalcBaseCostByField.IdModel = CBCBF.IdModel AND 
		tblCalcBaseCostByField.IdActivity = CBCBF.IdActivity AND tblCalcBaseCostByField.IdResources = CBCBF.IdResources AND 
		tblCalcBaseCostByField.IdClientAccount = CBCBF.IdClientAccount AND tblCalcBaseCostByField.IdField = CBCBF.IdField AND 
		tblCalcBaseCostByField.IdClientCostCenter = CBCBF.IdClientCostCenter AND tblCalcBaseCostByField.IdZiffAccount = CBCBF.IdZiffAccount
	WHERE tblCalcBaseCostByField.IdModel = @IdModel;
	
	UPDATE tblCalcBaseCostByField SET
		[BCRNormal] = CASE WHEN ISNULL([TFNormal],0)=0 THEN 0 ELSE [Amount]/[TFNormal] END,
		[BCRPessimistic] = CASE WHEN ISNULL([TFPessimistic],0)=0 THEN 0 ELSE [Amount]/[TFPessimistic] END,
		[BCROptimistic] = CASE WHEN ISNULL([TFOptimistic],0)=0 THEN 0 ELSE [Amount]/[TFOptimistic] END
	WHERE TechApplicable=1 AND IdModel=@IdModel;

	UPDATE tblCalcBaseCostByField SET
		[BCRNormal] = 0,
		[BCRPessimistic] = 0,
		[BCROptimistic] = 0
	WHERE TechApplicable=0 AND IdModel=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- SETUP ZIFF BASE AND FORECAST ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Create Ziff BaseYear Technical Driver Assumption Lookup **/
	DECLARE @YearBaseZiff INT;
	DECLARE YearListZiff CURSOR FOR
	SELECT tblCostStructureAssumptionsByProject.Ziff AS Year
	FROM tblCostStructureAssumptionsByProject INNER JOIN tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject
	WHERE tblProjects.IdModel=@IdModel
	GROUP BY tblCostStructureAssumptionsByProject.Ziff
	HAVING tblCostStructureAssumptionsByProject.Ziff > 0
	ORDER BY Year;

	OPEN YearListZiff;
	FETCH NEXT FROM YearListZiff INTO @YearBaseZiff;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		INSERT INTO tblCalcTechnicalDriverAssumptionZiff ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff])
		SELECT 1, tblCalcTechnicalAssumption.IdModel, tblCalcTechnicalAssumption.IdZiffAccount, tblCalcTechnicalAssumption.IdTypeOperation, 
			tblFields.IdField, tblProjects.IdProject, @YearBaseZiff + CASE WHEN tblCalcTechnicalAssumption.ProjectionCriteria=4 AND tblCostStructureAssumptionsByProject.Ziff=0 THEN tblCalcTechnicalAssumption.CycleValue-1 ELSE 0 END AS [Year], 
			tblProjects.Operation, CAST(1 AS BIT) AS [BaseYear], tblCalcTechnicalAssumption.IdTechnicalDriverSize, tblCalcTechnicalAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalAssumption.CycleValue, 
			tblCalcTechnicalAssumption.ProjectionCriteria, tblCostStructureAssumptionsByProject.Client, tblCostStructureAssumptionsByProject.Ziff
		FROM tblCalcTechnicalAssumption LEFT OUTER JOIN
			tblProjects LEFT OUTER JOIN
			tblFields ON tblFields.IdField = tblProjects.IdField RIGHT OUTER JOIN
			tblCostStructureAssumptionsByProject ON tblProjects.IdProject = tblCostStructureAssumptionsByProject.IdProject ON 
			tblCostStructureAssumptionsByProject.IdZiffAccount = tblCalcTechnicalAssumption.IdZiffAccount AND 
			tblFields.IdTypeOperation = tblCalcTechnicalAssumption.IdTypeOperation
		WHERE tblCostStructureAssumptionsByProject.Ziff=@YearBaseZiff AND tblCalcTechnicalAssumption.IdModel=@IdModel;

		FETCH NEXT FROM YearListZiff INTO @YearBaseZiff;
	END

	CLOSE YearListZiff;
	DEALLOCATE YearListZiff;
	
	/** Update Ziff BaseYear Driver Values **/
	EXEC calcModuleTechnicalDriverUpdate @IdModel,2,1;
	
	/** Update Ziff BaseCost Value for BaseYear **/
	UPDATE tblCalcBaseCostByFieldZiff SET
		[TechApplicable]=1,
		[TFNormal] = 1,
		[TFPessimistic] = 1,
		[TFOptimistic] = 1
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField
	WHERE tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 AND tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria IN (1,2) AND tblCalcBaseCostByFieldZiff.IdModel=@IdModel;

	UPDATE tblCalcBaseCostByFieldZiff SET
		tblCalcBaseCostByFieldZiff.TechApplicable=1,
		tblCalcBaseCostByFieldZiff.TFNormal = CBCBFZ.TFNormal,
		tblCalcBaseCostByFieldZiff.TFPessimistic = CBCBFZ.TFPessimistic,
		tblCalcBaseCostByFieldZiff.TFOptimistic = CBCBFZ.TFOptimistic
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		 (
		 SELECT tblCalcBaseCostByFieldZiff_1.IdModel, tblCalcBaseCostByFieldZiff_1.IdField, tblCalcBaseCostByFieldZiff_1.IdZiffAccount, 
			 SUM(CASE WHEN [SizeNormal] IS NULL THEN 0 ELSE [SizeNormal] * (CASE WHEN ISNULL([PerformanceNormal], 0) 
			 = 0 THEN 1 ELSE [PerformanceNormal] END) END) AS TFNormal, SUM(CASE WHEN [SizePessimistic] IS NULL 
			 THEN 0 ELSE [SizePessimistic] * (CASE WHEN ISNULL([PerformancePessimistic], 0) = 0 THEN 1 ELSE [PerformancePessimistic] END) END) 
			 AS TFPessimistic, SUM(CASE WHEN [SizeOptimistic] IS NULL THEN 0 ELSE [SizeOptimistic] * (CASE WHEN ISNULL([PerformanceOptimistic], 0) 
			 = 0 THEN 1 ELSE [PerformanceOptimistic] END) END) AS TFOptimistic
			 FROM tblCalcBaseCostByFieldZiff AS tblCalcBaseCostByFieldZiff_1 INNER JOIN
			 tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff_1.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
			 tblCalcBaseCostByFieldZiff_1.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount AND 
			 tblCalcBaseCostByFieldZiff_1.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField
		 WHERE tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 AND 
			tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria IN (3, 4, 5)
		 GROUP BY tblCalcBaseCostByFieldZiff_1.IdModel, tblCalcBaseCostByFieldZiff_1.IdField, tblCalcBaseCostByFieldZiff_1.IdZiffAccount
		 ) AS CBCBFZ ON 
		 tblCalcBaseCostByFieldZiff.IdModel = CBCBFZ.IdModel AND tblCalcBaseCostByFieldZiff.IdField = CBCBFZ.IdField AND 
		 tblCalcBaseCostByFieldZiff.IdZiffAccount = CBCBFZ.IdZiffAccount
	WHERE tblCalcBaseCostByFieldZiff.IdModel = @IdModel;

	UPDATE tblCalcBaseCostByFieldZiff SET
		[BCRNormal] = CASE WHEN ISNULL([TFNormal],0)=0 THEN 0 ELSE [TotalCost]/[TFNormal] END,
		[BCRPessimistic] = CASE WHEN ISNULL([TFPessimistic],0)=0 THEN 0 ELSE [TotalCost]/[TFPessimistic] END,
		[BCROptimistic] = CASE WHEN ISNULL([TFOptimistic],0)=0 THEN 0 ELSE [TotalCost]/[TFOptimistic] END
	WHERE TechApplicable=1 AND IdModel=@IdModel;
	
	UPDATE tblCalcBaseCostByFieldZiff SET
		[BCRNormal] = 0,
		[BCRPessimistic] = 0,
		[BCROptimistic] = 0
	WHERE TechApplicable=0 AND IdModel=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- UPDATE BASE COST RATES BASE YEAR
	--------------------------------------------------------------------------------------
	
	/** Update Company Base Cost Rates **/
	UPDATE tblCalcTechnicalDriverAssumption SET
		[BCRNormal] = CalcBaseCostByFieldSummary.BCRNormal,
		[BCRPessimistic] = CalcBaseCostByFieldSummary.BCRPessimistic,
		[BCROptimistic] = CalcBaseCostByFieldSummary.BCROptimistic
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(BCRNormal) AS BCRNormal, SUM(BCRPessimistic) AS BCRPessimistic, SUM(BCROptimistic) AS BCROptimistic
		FROM tblCalcBaseCostByField AS CBCBF
		GROUP BY IdModel, IdField, IdZiffAccount
		) AS CalcBaseCostByFieldSummary ON 
		tblCalcTechnicalDriverAssumption.IdModel = CalcBaseCostByFieldSummary.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = CalcBaseCostByFieldSummary.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = CalcBaseCostByFieldSummary.IdField
	WHERE tblCalcTechnicalDriverAssumption.BaseYear=1 AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Update Ziff Base Cost Rates **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		[BCRNormal] = CalcBaseCostByFieldSummaryZiff.BCRNormal,
		[BCRPessimistic] = CalcBaseCostByFieldSummaryZiff.BCRPessimistic,
		[BCROptimistic] = CalcBaseCostByFieldSummaryZiff.BCROptimistic
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(BCRNormal) AS BCRNormal, SUM(BCRPessimistic) AS BCRPessimistic, SUM(BCROptimistic) AS BCROptimistic
		FROM tblCalcBaseCostByFieldZiff AS CBCBFZ
		GROUP BY IdModel, IdField, IdZiffAccount
		) AS CalcBaseCostByFieldSummaryZiff ON 
		tblCalcTechnicalDriverAssumptionZiff.IdModel = CalcBaseCostByFieldSummaryZiff.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = CalcBaseCostByFieldSummaryZiff.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = CalcBaseCostByFieldSummaryZiff.IdField
	WHERE tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	/** Drop Ziff BaseYear with no costs **/
	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE [BaseYear]=1 AND [BCRNormal] IS NULL AND [BCRPessimistic] IS NULL AND [BCROptimistic] IS NULL AND [IdModel]=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- SETUP ADDITIONAL YEARS ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Create Company Additional Years Technical Driver Assumption Lookup **/
	INSERT INTO tblCalcTechnicalDriverAssumption ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff])
	SELECT 2, tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, tblCalcTechnicalDriverAssumption.IdTypeOperation, 
		tblCalcTechnicalDriverAssumption.IdField, tblCalcTechnicalDriverAssumption.IdProject, tblCalcModelYears.Year, 
		tblCalcTechnicalDriverAssumption.Operation, CAST(0 AS BIT) AS BaseYear, tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize, 
		tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalDriverAssumption.CycleValue, 
		tblCalcTechnicalDriverAssumption.ProjectionCriteria, tblCalcTechnicalDriverAssumption.YearClient, tblCalcTechnicalDriverAssumption.YearZiff
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		tblCalcModelYears ON tblCalcTechnicalDriverAssumption.IdModel = tblCalcModelYears.IdModel AND 
		tblCalcTechnicalDriverAssumption.Year < tblCalcModelYears.Year		
	WHERE tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Create Ziff Additional Years Technical Driver Assumption Lookup **/
	INSERT INTO tblCalcTechnicalDriverAssumptionZiff ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff])
	SELECT 2, tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdTypeOperation, 
		tblCalcTechnicalDriverAssumptionZiff.IdField, tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcModelYears.Year, 
		tblCalcTechnicalDriverAssumptionZiff.Operation, CAST(0 AS BIT) AS BaseYear, tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverSize, 
		tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverPerformance, tblCalcTechnicalDriverAssumptionZiff.CycleValue, 
		tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.YearClient, tblCalcTechnicalDriverAssumptionZiff.YearZiff
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		tblCalcModelYears ON tblCalcTechnicalDriverAssumptionZiff.IdModel = tblCalcModelYears.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.Year < tblCalcModelYears.Year
	WHERE tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	/** Create Company BusinessOpportunity Technical Driver Assumption Lookup **/
	INSERT INTO tblCalcTechnicalDriverAssumption ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff])
	SELECT 3, tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, tblCalcTechnicalDriverAssumption.IdTypeOperation, 
		tblCalcTechnicalDriverAssumption.IdField, tblProjects.IdProject, tblCalcTechnicalDriverAssumption.Year, tblProjects.Operation, 
		CASE WHEN tblCalcTechnicalDriverAssumption.Year = tblProjects.Starts THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS BaseYear, 
		tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize, tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance,  
		tblCalcTechnicalDriverAssumption.CycleValue, tblCalcTechnicalDriverAssumption.ProjectionCriteria, tblCalcTechnicalDriverAssumption.YearClient, tblCalcTechnicalDriverAssumption.YearZiff
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumption.IdModel = tblProjects.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdField = tblProjects.IdField AND tblCalcTechnicalDriverAssumption.Year >= tblProjects.Starts
	WHERE tblProjects.Operation=2 AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Remove Company BusinessOpportunity Technical Driver Assumption = Fixed **/
	DELETE FROM tblCalcTechnicalDriverAssumption WHERE ([ProjectionCriteria]=1) AND [Operation]=2 AND [IdModel]=@IdModel;
	
	/** Create Ziff BusinessOpportunity Technical Driver Assumption Lookup **/
	INSERT INTO tblCalcTechnicalDriverAssumptionZiff ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff])
	SELECT 3, tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdTypeOperation, 
		tblCalcTechnicalDriverAssumptionZiff.IdField, tblProjects.IdProject, tblCalcTechnicalDriverAssumptionZiff.Year, tblProjects.Operation, 
		CASE WHEN tblCalcTechnicalDriverAssumptionZiff.Year = tblProjects.Starts THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS BaseYear, 
		tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverSize, tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverPerformance, 
		tblCalcTechnicalDriverAssumptionZiff.CycleValue, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.YearClient, tblCalcTechnicalDriverAssumptionZiff.YearZiff
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumptionZiff.IdModel = tblProjects.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = tblProjects.IdField AND tblCalcTechnicalDriverAssumptionZiff.Year >= tblProjects.Starts
	WHERE tblProjects.Operation=2 AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	/** Remove Ziff BusinessOpportunity Technical Driver Assumption = Fixed or Semi-Fixed **/
	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE ([ProjectionCriteria]=1 OR [ProjectionCriteria]=2) AND [Operation]=2 AND [IdModel]=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- CLEAN UP ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Remove Base Year Assumptions not in initial Year for Semi-Fixed **/
	UPDATE tblCalcTechnicalDriverAssumption SET [BaseYear]=0 WHERE [ProjectionCriteria]=2 AND [BaseYear]=1 AND [Year]<>@intMinYear AND [IdModel]=@IdModel;
	
	/** Rebase and Remove Excess Years from all Cyclical Assumptions **/
	EXEC calcModuleTechnicalCyclicalRebase @IdModel,@intMinYear,@intMaxYear;
	
	--------------------------------------------------------------------------------------
	-- ADD CLIENT AND ZIFF COST ASSUMPTION START YEARS TO TABLE
	--------------------------------------------------------------------------------------
	
	/** Update Company Client/Ziff Years Lookup **/
	UPDATE tblCalcTechnicalDriverAssumption
	SET YearClient=CTDALookup.YearClient, YearZiff=CTDALookup.YearZiff
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		(
		SELECT CTDA.IdModel, CTDA.IdZiffAccount, CTDA.IdField, CTDA.YearClient, CTDA.YearZiff
		FROM tblCalcTechnicalDriverAssumption AS CTDA INNER JOIN
			tblProjects AS P ON CTDA.IdField = P.IdField AND CTDA.IdModel = P.IdModel INNER JOIN
			tblCostStructureAssumptionsByProject AS CSABP ON P.IdProject = CSABP.IdProject
		WHERE CTDA.Operation=1 AND CTDA.BaseYear=1
		GROUP BY CTDA.IdModel, CTDA.IdZiffAccount, CTDA.IdField, CTDA.YearClient, CTDA.YearZiff
		) AS CTDALookup ON 
		tblCalcTechnicalDriverAssumption.IdModel = CTDALookup.IdModel AND tblCalcTechnicalDriverAssumption.IdZiffAccount = CTDALookup.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = CTDALookup.IdField
	WHERE tblCalcTechnicalDriverAssumption.YearClient IS NULL AND tblCalcTechnicalDriverAssumption.YearZiff IS NULL AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Remove Company Years Based on Company/Ziff Start Years **/
	DELETE FROM tblCalcTechnicalDriverAssumption WHERE [YearZiff]>0 AND [Year]>=[YearZiff] AND [IdModel]=@IdModel;
	
	/** Update Ziff Client/Ziff Years Lookup **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff
	SET YearClient=CTDAZLookup.YearClient, YearZiff=CTDAZLookup.YearZiff
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		(
		SELECT CTDAZ.IdModel, CTDAZ.IdZiffAccount, CTDAZ.IdField, CTDAZ.YearClient, CTDAZ.YearZiff
		FROM tblCalcTechnicalDriverAssumptionZiff AS CTDAZ INNER JOIN
			tblProjects AS P ON CTDAZ.IdField = P.IdField AND CTDAZ.IdModel = P.IdModel INNER JOIN
			tblCostStructureAssumptionsByProject AS CSABP ON P.IdProject = CSABP.IdProject
		WHERE CTDAZ.Operation=1 AND CTDAZ.BaseYear=1
		GROUP BY CTDAZ.IdModel, CTDAZ.IdZiffAccount, CTDAZ.IdField, CTDAZ.YearClient, CTDAZ.YearZiff
		) AS CTDAZLookup ON 
		tblCalcTechnicalDriverAssumptionZiff.IdModel = CTDAZLookup.IdModel AND tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = CTDAZLookup.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = CTDAZLookup.IdField
	WHERE tblCalcTechnicalDriverAssumptionZiff.YearClient IS NULL AND tblCalcTechnicalDriverAssumptionZiff.YearZiff IS NULL AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	/** Remove Ziff Years Based on Company/Ziff Start Years (SHOULD BE NO RECORDS - THIS IS JUST A DOUBLE CHECK) **/
	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE [YearClient]>0 AND [Year]<[YearZiff] AND [IdModel]=@IdModel;
	
	/** Ensure All Ziff BO Items as properly flagged as based **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET BaseYear=1 WHERE [Operation]=2 AND [BaseYear]=0 AND [Year]=[YearZiff] AND IdModel=@IdModel;
	

	--------------------------------------------------------------------------------------
	-- PREP FOR TECHNICAL BASE COST RATE CALCS
	--------------------------------------------------------------------------------------
	
	/** Update Company TF (Base Year Only for BCR Calc) **/
	UPDATE tblCalcTechnicalDriverAssumption SET 
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeNormal], 0) = 0 THEN 0 ELSE [SizeNormal]*(CASE WHEN ISNULL([PerformanceNormal],0)=0 THEN 1 ELSE [PerformanceNormal] END) END ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizePessimistic], 0) = 0 THEN 0 ELSE [SizePessimistic]*(CASE WHEN ISNULL([PerformancePessimistic],0)=0 THEN 1 ELSE [PerformancePessimistic] END) END ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeOptimistic], 0) = 0 THEN 0 ELSE [SizeOptimistic]*(CASE WHEN ISNULL([PerformanceOptimistic],0)=0 THEN 1 ELSE [PerformanceOptimistic] END) END ELSE 0 END
	WHERE [BaseYear]=1 AND [IdModel]=@IdModel;
	
	/** Update Ziff TF  (Base Year Only for BCR Calc) **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeNormal], 0) = 0 THEN 0 ELSE [SizeNormal]*(CASE WHEN ISNULL([PerformanceNormal],0)=0 THEN 1 ELSE [PerformanceNormal] END) END ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizePessimistic], 0) = 0 THEN 0 ELSE [SizePessimistic]*(CASE WHEN ISNULL([PerformancePessimistic],0)=0 THEN 1 ELSE [PerformancePessimistic] END) END ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeOptimistic], 0) = 0 THEN 0 ELSE [SizeOptimistic]*(CASE WHEN ISNULL([PerformanceOptimistic],0)=0 THEN 1 ELSE [PerformanceOptimistic] END) END ELSE 0 END
	WHERE [BaseYear]=1 AND [IdModel]=@IdModel;

	UPDATE tblCalcTechnicalDriverAssumption SET 
		tblCalcTechnicalDriverAssumption.TFNormal = 0,
		tblCalcTechnicalDriverAssumption.TFPessimistic = 0,
		tblCalcTechnicalDriverAssumption.TFOptimistic = 0
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		tblCalcModelMultiBaseline ON tblCalcTechnicalDriverAssumption.IdModel = tblCalcModelMultiBaseline.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdField = tblCalcModelMultiBaseline.IdField
	WHERE tblCalcTechnicalDriverAssumption.ProjectionCriteria IN (1, 2) AND tblCalcTechnicalDriverAssumption.Operation=1 AND tblCalcTechnicalDriverAssumption.BaseYear=1 AND tblCalcTechnicalDriverAssumption.Year=tblCalcTechnicalDriverAssumption.YearClient AND
		tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize=0 AND tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance=0 AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	UPDATE tblCalcTechnicalDriverAssumption SET 
		tblCalcTechnicalDriverAssumption.TFNormal = TDUCD.TFNormal,
		tblCalcTechnicalDriverAssumption.TFPessimistic = TDUCD.TFPessimistic,
		tblCalcTechnicalDriverAssumption.TFOptimistic = TDUCD.TFOptimistic
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		(
		SELECT tblCalcUnitCostDenominatorAmount.IdModel, tblCalcUnitCostDenominatorAmount.IdField, tblCalcUnitCostDenominatorAmount.IdProject, 
		tblCalcUnitCostDenominatorAmount.Year, 
		tblCalcUnitCostDenominatorAmount.SumNormal / tblCalcUnitCostDenominatorSumAmount.SumNormal AS TFNormal, 
		tblCalcUnitCostDenominatorAmount.SumPessimistic / tblCalcUnitCostDenominatorSumAmount.SumPessimistic AS TFPessimistic, 
		tblCalcUnitCostDenominatorAmount.SumOptimistic / tblCalcUnitCostDenominatorSumAmount.SumOptimistic AS TFOptimistic
		FROM tblCalcUnitCostDenominatorAmount INNER JOIN
		tblCalcUnitCostDenominatorSumAmount ON 
		tblCalcUnitCostDenominatorAmount.IdModel = tblCalcUnitCostDenominatorSumAmount.IdModel AND 
		tblCalcUnitCostDenominatorAmount.IdField = tblCalcUnitCostDenominatorSumAmount.IdField AND 
		tblCalcUnitCostDenominatorAmount.Year = tblCalcUnitCostDenominatorSumAmount.Year
		) AS TDUCD ON 
		tblCalcTechnicalDriverAssumption.IdModel = TDUCD.IdModel AND tblCalcTechnicalDriverAssumption.IdField = TDUCD.IdField AND 
		tblCalcTechnicalDriverAssumption.IdProject = TDUCD.IdProject AND tblCalcTechnicalDriverAssumption.Year = TDUCD.Year
	WHERE tblCalcTechnicalDriverAssumption.ProjectionCriteria IN (1, 2) AND tblCalcTechnicalDriverAssumption.Operation=1 AND tblCalcTechnicalDriverAssumption.BaseYear=1 AND tblCalcTechnicalDriverAssumption.Year=tblCalcTechnicalDriverAssumption.YearClient AND
		tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize=0 AND tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance=0 AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;

	/** Remove Base Year Items without Project Drivers **/
	DELETE FROM tblCalcTechnicalDriverAssumption
	WHERE SizeNormal IS NULL AND SizePessimistic IS NULL AND SizeOptimistic IS NULL AND PerformanceNormal IS NULL AND PerformancePessimistic IS NULL AND PerformanceOptimistic IS NULL AND ProjectionCriteria IN (2,3,4,5) AND BaseYear=1 AND Operation=1 AND IdModel=@IdModel;

	/** Technical Base Cost Rate **/
	EXEC calcModuleTechnicalBaseCostRate @IdModel;

	--------------------------------------------------------------------------------------
	-- POST TECHNICAL BASE COST RATE RESET
	--------------------------------------------------------------------------------------
	
	/** Update All Years Driver Values **/
	EXEC calcModuleTechnicalDriverUpdate @IdModel,1,0;
	EXEC calcModuleTechnicalDriverUpdate @IdModel,2,0;
		
	/** Update Company TF **/
	UPDATE tblCalcTechnicalDriverAssumption SET 
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeNormal], 0) = 0 THEN 0 ELSE [SizeNormal]*(CASE WHEN ISNULL([PerformanceNormal],0)=0 THEN 1 ELSE [PerformanceNormal] END) END ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizePessimistic], 0) = 0 THEN 0 ELSE [SizePessimistic]*(CASE WHEN ISNULL([PerformancePessimistic],0)=0 THEN 1 ELSE [PerformancePessimistic] END) END ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeOptimistic], 0) = 0 THEN 0 ELSE [SizeOptimistic]*(CASE WHEN ISNULL([PerformanceOptimistic],0)=0 THEN 1 ELSE [PerformanceOptimistic] END) END ELSE 0 END
	WHERE [IdModel]=@IdModel;
	
	/** Update Ziff TF **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5  THEN CASE WHEN ISNULL([SizeNormal], 0) = 0 THEN 0 ELSE [SizeNormal]*(CASE WHEN ISNULL([PerformanceNormal],0)=0 THEN 1 ELSE [PerformanceNormal] END) END ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizePessimistic], 0) = 0 THEN 0 ELSE [SizePessimistic]*(CASE WHEN ISNULL([PerformancePessimistic],0)=0 THEN 1 ELSE [PerformancePessimistic] END) END ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeOptimistic], 0) = 0 THEN 0 ELSE [SizeOptimistic]*(CASE WHEN ISNULL([PerformanceOptimistic],0)=0 THEN 1 ELSE [PerformanceOptimistic] END) END ELSE 0 END
	WHERE [IdModel]=@IdModel;

	--TODO: Need to add for ZIFF Side Too!!!

	UPDATE tblCalcTechnicalDriverAssumption SET 
		tblCalcTechnicalDriverAssumption.TFNormal = 0,
		tblCalcTechnicalDriverAssumption.TFPessimistic = 0,
		tblCalcTechnicalDriverAssumption.TFOptimistic = 0
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		tblCalcModelMultiBaseline ON tblCalcTechnicalDriverAssumption.IdModel = tblCalcModelMultiBaseline.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdField = tblCalcModelMultiBaseline.IdField
	WHERE tblCalcTechnicalDriverAssumption.ProjectionCriteria IN (1, 2) AND tblCalcTechnicalDriverAssumption.Operation=1 AND 
		tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize=0 AND tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance=0 AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	UPDATE tblCalcTechnicalDriverAssumption SET 
		tblCalcTechnicalDriverAssumption.TFNormal = TDUCD.TFNormal,
		tblCalcTechnicalDriverAssumption.TFPessimistic = TDUCD.TFPessimistic,
		tblCalcTechnicalDriverAssumption.TFOptimistic = TDUCD.TFOptimistic
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		(
		SELECT tblCalcUnitCostDenominatorAmount.IdModel, tblCalcUnitCostDenominatorAmount.IdField, tblCalcUnitCostDenominatorAmount.IdProject, 
		tblCalcUnitCostDenominatorAmount.Year, 
		tblCalcUnitCostDenominatorAmount.SumNormal / tblCalcUnitCostDenominatorSumAmount.SumNormal AS TFNormal, 
		tblCalcUnitCostDenominatorAmount.SumPessimistic / tblCalcUnitCostDenominatorSumAmount.SumPessimistic AS TFPessimistic, 
		tblCalcUnitCostDenominatorAmount.SumOptimistic / tblCalcUnitCostDenominatorSumAmount.SumOptimistic AS TFOptimistic
		FROM tblCalcUnitCostDenominatorAmount INNER JOIN
		tblCalcUnitCostDenominatorSumAmount ON 
		tblCalcUnitCostDenominatorAmount.IdModel = tblCalcUnitCostDenominatorSumAmount.IdModel AND 
		tblCalcUnitCostDenominatorAmount.IdField = tblCalcUnitCostDenominatorSumAmount.IdField AND 
		tblCalcUnitCostDenominatorAmount.Year = tblCalcUnitCostDenominatorSumAmount.Year
		) AS TDUCD ON 
		tblCalcTechnicalDriverAssumption.IdModel = TDUCD.IdModel AND tblCalcTechnicalDriverAssumption.IdField = TDUCD.IdField AND 
		tblCalcTechnicalDriverAssumption.IdProject = TDUCD.IdProject AND tblCalcTechnicalDriverAssumption.Year = TDUCD.Year
	WHERE tblCalcTechnicalDriverAssumption.ProjectionCriteria IN (1, 2) AND tblCalcTechnicalDriverAssumption.Operation=1 AND
		tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize=0 AND tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance=0 AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;

	--/** Remove Base Year Items without Project Drivers **/
	DELETE FROM tblCalcTechnicalDriverAssumption
	WHERE SizeNormal IS NULL AND SizePessimistic IS NULL AND SizeOptimistic IS NULL AND PerformanceNormal IS NULL AND PerformancePessimistic IS NULL AND PerformanceOptimistic IS NULL AND ProjectionCriteria IN (2,3,4,5) AND BaseYear=1 AND Operation=1 AND IdModel=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- UPDATE BASE COST RATES ALL YEARS
	--------------------------------------------------------------------------------------
	
	/** Update Company Base Cost Rates **/
	UPDATE tblCalcTechnicalDriverAssumption SET
		[BCRNormal] = CalcBaseCostByFieldSummary.BCRNormal,
		[BCRPessimistic] = CalcBaseCostByFieldSummary.BCRPessimistic,
		[BCROptimistic] = CalcBaseCostByFieldSummary.BCROptimistic
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(BCRNormal) AS BCRNormal, SUM(BCRPessimistic) AS BCRPessimistic, SUM(BCROptimistic) AS BCROptimistic
		FROM tblCalcBaseCostByField AS CBCBF
		GROUP BY IdModel, IdField, IdZiffAccount
		) AS CalcBaseCostByFieldSummary ON 
		tblCalcTechnicalDriverAssumption.IdModel = CalcBaseCostByFieldSummary.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = CalcBaseCostByFieldSummary.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = CalcBaseCostByFieldSummary.IdField
	WHERE tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Update Ziff Base Cost Rates **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		[BCRNormal] = CalcBaseCostByFieldSummaryZiff.BCRNormal,
		[BCRPessimistic] = CalcBaseCostByFieldSummaryZiff.BCRPessimistic,
		[BCROptimistic] = CalcBaseCostByFieldSummaryZiff.BCROptimistic
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(BCRNormal) AS BCRNormal, SUM(BCRPessimistic) AS BCRPessimistic, SUM(BCROptimistic) AS BCROptimistic
		FROM tblCalcBaseCostByFieldZiff AS CBCBFZ
		GROUP BY IdModel, IdField, IdZiffAccount
		) AS CalcBaseCostByFieldSummaryZiff ON 
		tblCalcTechnicalDriverAssumptionZiff.IdModel = CalcBaseCostByFieldSummaryZiff.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = CalcBaseCostByFieldSummaryZiff.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = CalcBaseCostByFieldSummaryZiff.IdField
	WHERE tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- RUN FORECASTS
	--------------------------------------------------------------------------------------
	
	/** Technical Forecast **/
	EXEC calcModuleTechnicalForecast @IdModel,@intMinYear,@intMaxYear;
END
GO

USE [DBCPM]
GO

/****** Object:  StoredProcedure [dbo].[calcModuleTechnicalForecast]    Script Date: 08/12/2015 15:03:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Gareth Slater
-- Create date: 2014-04-20
-- Description:	Module Technical Forecast
-- ================================================================================================ 
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 2015-08-04	RM		Added local variables, made temporary tables in order to speed up execution. 
--						Added split of baselines when necessary
-- ================================================================================================ 
ALTER PROCEDURE [dbo].[calcModuleTechnicalForecast](
@IdModel int,
@MinYear int,
@MaxYear int
) WITH RECOMPILE 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/** Declare local variables to reduce processing time **/
	DECLARE @LocIdModel INT = @IdModel
	DECLARE @LocMinYear INT = @MinYear
	DECLARE @LocMaxYear INT = @MaxYear

	/** Cleanup Calc Tables for New Data **/
	IF OBJECT_ID('tempdb..#tblCalcProjectionTechnical') IS NOT NULL DROP TABLE #tblCalcProjectionTechnical
	IF OBJECT_ID('tempdb..#tblCalcTechnicalSemifixedSelections') IS NOT NULL DROP TABLE #tblCalcTechnicalSemifixedSelections
	IF OBJECT_ID('tempdb..#tblCalcTechnicalSemifixedSplit') IS NOT NULL DROP TABLE #tblCalcTechnicalSemifixedSplit
	IF OBJECT_ID('tempdb..#UtilizationCapacityValues') IS NOT NULL DROP TABLE #UtilizationCapacityValues
	IF OBJECT_ID('tempdb..#VolumeReportGrouped') IS NOT NULL DROP TABLE #VolumeReportGrouped
	IF OBJECT_ID('tempdb..#VolumeReportGroupedBO') IS NOT NULL DROP TABLE #VolumeReportGroupedBO
	IF OBJECT_ID('tempdb..#VolumeReportGroupedBL') IS NOT NULL DROP TABLE #VolumeReportGroupedBL
	IF OBJECT_ID('tempdb..#VolumeReportBOList') IS NOT NULL DROP TABLE #VolumeReportBOList
	IF OBJECT_ID('tempdb..#VolumeReportBO') IS NOT NULL DROP TABLE #VolumeReportBO
	IF OBJECT_ID('tempdb..#VolumeReportBORows') IS NOT NULL DROP TABLE #VolumeReportBORows
	IF OBJECT_ID('tempdb..#VolumeReportBLList') IS NOT NULL DROP TABLE #VolumeReportBLList
	IF OBJECT_ID('tempdb..#VolumeReportBL') IS NOT NULL DROP TABLE #VolumeReportBL
	IF OBJECT_ID('tempdb..#VolumeReportBLRows') IS NOT NULL DROP TABLE #VolumeReportBLRows
	
	CREATE TABLE [dbo].[#tblCalcProjectionTechnical](
		[SourceDebug] [int] NULL,
		[IdModel] [int] NOT NULL,
		[IdActivity] [int] NULL,
		[CodeAct] [nvarchar](50) NULL,
		[Activity] [nvarchar](255) NULL,
		[IdResources] [int] NULL,
		[CodeRes] [nvarchar](50) NULL,
		[Resource] [nvarchar](255) NULL,
		[IdClientAccount] [int] NULL,
		[Account] [nvarchar](50) NULL,
		[NameAccount] [nvarchar](255) NULL,
		[IdField] [int] NOT NULL,
		[Field] [nvarchar](255) NULL,
		[IdClientCostCenter] [int] NULL,
		[ClientCostCenter] [nvarchar](255) NULL,
		[Amount] [float] NULL,
		[TypeAllocation] [int] NULL,
		[IdZiffAccount] [int] NULL,
		[CodeZiff] [nvarchar](50) NULL,
		[ZiffAccount] [nvarchar](255) NULL,
		[IdRootZiffAccount] [int] NULL,
		[RootCodeZiff] [nvarchar](50) NULL,
		[RootZiffAccount] [nvarchar](255) NULL,
		[IdProject] [int] NULL,
		[Project] [nvarchar](255) NULL,
		[ProjectionCriteria] [int] NULL,
		[Year] [int] NULL,
		[BaseYear] [bit] NULL,
		[TFNormal] [float] NULL,
		[TFPessimistic] [float] NULL,
		[TFOptimistic] [float] NULL,
		[BCRNormal] [float] NULL,
		[BCRPessimistic] [float] NULL,
		[BCROptimistic] [float] NULL,
		[BaseCostType] [int] NULL,
		[Operation] [int] NULL,
		[SortOrder] [int] NULL
	) ON [PRIMARY]

	CREATE TABLE [dbo].[#tblCalcTechnicalSemifixedSelections](
		[IdModel] [int],
		[IdField] [int],
		[IdZiffAccount] [int],
		[IdTechnicalDriverSize] [int],
		[TypeAllocation] [int],
	) ON [PRIMARY]

	CREATE TABLE [dbo].[#tblCalcTechnicalSemifixedSplit](
		[IdModel] [int] NOT NULL,
		[Year] [int] NOT NULL,
		[IdZiffAccount] [int] NOT NULL,
		[IdField] [int] NOT NULL,
		[IdProject] [int] NOT NULL,
		[IdTechnicalDriverSize] [int] NULL,
		[TypeAllocation] [int] NOT NULL,
		[SemifixedForecast] [bit] NULL,
		[SplitNormal] [float] NULL,
		[SplitPessimistic] [float] NULL,
		[SplitOptimistic] [float] NULL,
	) ON [PRIMARY]
	
	/** Set Allowed Combinations for Semi-Fixed Assumptions (Shared) **/
	INSERT INTO #tblCalcTechnicalSemifixedSelections ([IdModel],[IdField],[IdZiffAccount],[IdTechnicalDriverSize],[TypeAllocation])
	SELECT tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.IdZiffAccount, 
		tblCalcTechnicalAssumption.IdTechnicalDriverSize, tblCalcBaseCostByField.TypeAllocation
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalAssumption.IdModel AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalAssumption.IdZiffAccount INNER JOIN
		tblFields ON tblCalcBaseCostByField.IdField = tblFields.IdField AND tblCalcBaseCostByField.IdModel = tblFields.IdModel AND 
		tblCalcTechnicalAssumption.IdTypeOperation = tblFields.IdTypeOperation
	WHERE tblCalcTechnicalAssumption.ProjectionCriteria=2
	GROUP BY tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.TypeAllocation, 
		tblCalcTechnicalAssumption.IdTechnicalDriverSize
	HAVING tblCalcBaseCostByField.TypeAllocation=2 AND tblCalcBaseCostByField.IdModel=@LocIdModel;

	/** Set Allowed Combinations for Semi-Fixed Assumptions (Hierarchy) **/
	INSERT INTO #tblCalcTechnicalSemifixedSelections ([IdModel],[IdField],[IdZiffAccount],[IdTechnicalDriverSize],[TypeAllocation])
	SELECT tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.IdZiffAccount, 
		tblCalcTechnicalAssumption.IdTechnicalDriverSize, tblCalcBaseCostByField.TypeAllocation
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalAssumption.IdModel AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalAssumption.IdZiffAccount INNER JOIN
		tblFields ON tblCalcBaseCostByField.IdField = tblFields.IdField AND tblCalcBaseCostByField.IdModel = tblFields.IdModel AND 
		tblCalcTechnicalAssumption.IdTypeOperation = tblFields.IdTypeOperation
	WHERE tblCalcTechnicalAssumption.ProjectionCriteria=2
	GROUP BY tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.TypeAllocation, 
		tblCalcTechnicalAssumption.IdTechnicalDriverSize
	HAVING tblCalcBaseCostByField.TypeAllocation=3 AND tblCalcBaseCostByField.IdModel=@LocIdModel;
	
	/** Update Ziff Semi-Fixed Forecast to Fixed **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET [ProjectionCriteria]=1 WHERE [ProjectionCriteria]=2 AND [IdModel]=@LocIdModel;
	
	/** (1) Insert Company Base Year Into #tblCalcProjectionTechnical **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 1, tblCalcBaseCostByField.IdModel,tblCalcBaseCostByField.IdActivity,tblCalcBaseCostByField.CodeAct,tblCalcBaseCostByField.Activity,tblCalcBaseCostByField.IdResources,tblCalcBaseCostByField.CodeRes,tblCalcBaseCostByField.Resource,tblCalcBaseCostByField.IdClientAccount,tblCalcBaseCostByField.Account,tblCalcBaseCostByField.NameAccount,tblCalcBaseCostByField.IdField,tblCalcBaseCostByField.Field,tblCalcBaseCostByField.IdClientCostCenter,tblCalcBaseCostByField.ClientCostCenter,tblCalcBaseCostByField.Amount,tblCalcBaseCostByField.TypeAllocation,tblCalcBaseCostByField.IdZiffAccount,tblCalcBaseCostByField.CodeZiff,tblCalcBaseCostByField.ZiffAccount,tblCalcBaseCostByField.IdRootZiffAccount,[RootCodeZiff],[RootZiffAccount],tblCalcTechnicalDriverAssumption.IdProject,tblCalcTechnicalDriverAssumption.ProjectionCriteria,tblCalcTechnicalDriverAssumption.Year,tblCalcTechnicalDriverAssumption.BaseYear,tblCalcTechnicalDriverAssumption.TFNormal,tblCalcTechnicalDriverAssumption.TFPessimistic,tblCalcTechnicalDriverAssumption.TFOptimistic,tblCalcBaseCostByField.BCRNormal,tblCalcBaseCostByField.BCRPessimistic,tblCalcBaseCostByField.BCROptimistic,1 AS BaseCostType,tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount
	WHERE tblCalcTechnicalDriverAssumption.BaseYear=1 AND tblCalcBaseCostByField.IdModel=@LocIdModel;
	
	/** (2) Insert Ziff Base Year Into #tblCalcProjectionTechnical (If Applicable) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 2, tblCalcBaseCostByFieldZiff.IdModel,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,tblCalcBaseCostByFieldZiff.IdField,tblCalcBaseCostByFieldZiff.Field,NULL,NULL,tblCalcBaseCostByFieldZiff.TotalCost,NULL,tblCalcBaseCostByFieldZiff.IdZiffAccount,tblCalcBaseCostByFieldZiff.CodeZiff,tblCalcBaseCostByFieldZiff.ZiffAccount,tblCalcBaseCostByFieldZiff.IdRootZiffAccount,[RootCodeZiff],[RootZiffAccount],tblCalcTechnicalDriverAssumptionZiff.IdProject,tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria,tblCalcTechnicalDriverAssumptionZiff.Year,tblCalcTechnicalDriverAssumptionZiff.BaseYear,tblCalcTechnicalDriverAssumptionZiff.TFNormal,tblCalcTechnicalDriverAssumptionZiff.TFPessimistic,tblCalcTechnicalDriverAssumptionZiff.TFOptimistic,tblCalcBaseCostByFieldZiff.BCRNormal,tblCalcBaseCostByFieldZiff.BCRPessimistic,tblCalcBaseCostByFieldZiff.BCROptimistic,2 AS BaseCostType,tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount
	WHERE tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 
	AND tblCalcBaseCostByFieldZiff.IdModel=@LocIdModel;
	
	/** Setup Semi-Fixed Driver Splits (Shared) **/
	INSERT INTO #tblCalcTechnicalSemifixedSplit ([IdModel],[Year],[IdZiffAccount],[IdField],[IdProject],[IdTechnicalDriverSize],[TypeAllocation],[SplitNormal],[SplitPessimistic],[SplitOptimistic])
	SELECT DetailsLookup.IdModel, DetailsLookup.Year, DetailsLookup.IdZiffAccount, DetailsLookup.IdField, DetailsLookup.IdProject, DetailsLookup.IdTechnicalDriver, 2 AS TypeAllocation, 
		ISNULL(DetailsLookup.SizeNormal/SummaryLookup.SizeNormal,0) AS SplitNormal, 
		ISNULL(DetailsLookup.SizePessimistic/SummaryLookup.SizePessimistic,0) AS SplitPessimistic, 
		ISNULL(DetailsLookup.SizeOptimistic/SummaryLookup.SizeOptimistic,0) AS SplitOptimistic
	FROM (
		SELECT #tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalDriverData.Year, tblCalcTechnicalDriverData.IdField, 
			tblCalcTechnicalDriverData.IdProject, #tblCalcTechnicalSemifixedSelections.IdZiffAccount, tblCalcTechnicalDriverData.IdTechnicalDriver, 
			SUM(tblCalcTechnicalDriverData.Normal) AS SizeNormal, SUM(tblCalcTechnicalDriverData.Pessimistic) AS SizePessimistic, 
			SUM(tblCalcTechnicalDriverData.Optimistic) AS SizeOptimistic
		FROM #tblCalcTechnicalSemifixedSelections INNER JOIN
			tblCalcTechnicalDriverData ON #tblCalcTechnicalSemifixedSelections.IdModel = tblCalcTechnicalDriverData.IdModel AND 
			#tblCalcTechnicalSemifixedSelections.IdField = tblCalcTechnicalDriverData.IdField INNER JOIN
			(
			SELECT IdModel, IdTechnicalDriverSize, IdZiffAccount, IdField, IdProject, Year
			FROM tblCalcTechnicalDriverAssumption
			WHERE ProjectionCriteria=2
			GROUP BY IdModel, IdTechnicalDriverSize, IdZiffAccount, IdField, IdProject, Year
			HAVING IdModel=@LocIdModel
			) AS SelectionList ON tblCalcTechnicalDriverData.IdModel = SelectionList.IdModel AND 
			tblCalcTechnicalDriverData.IdTechnicalDriver = SelectionList.IdTechnicalDriverSize AND tblCalcTechnicalDriverData.IdField = SelectionList.IdField AND 
			tblCalcTechnicalDriverData.IdProject = SelectionList.IdProject AND tblCalcTechnicalDriverData.Year = SelectionList.Year AND 
			#tblCalcTechnicalSemifixedSelections.IdZiffAccount = SelectionList.IdZiffAccount
		WHERE #tblCalcTechnicalSemifixedSelections.TypeAllocation=2
		GROUP BY #tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalDriverData.Year, #tblCalcTechnicalSemifixedSelections.IdZiffAccount, 
			tblCalcTechnicalDriverData.IdTechnicalDriver, tblCalcTechnicalDriverData.IdField, tblCalcTechnicalDriverData.IdProject
		HAVING tblCalcTechnicalDriverData.Year<>@LocMinYear AND #tblCalcTechnicalSemifixedSelections.IdModel=@LocIdModel
		) AS DetailsLookup LEFT OUTER JOIN
		(
		SELECT #tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalDriverData.Year, #tblCalcTechnicalSemifixedSelections.IdZiffAccount, 
			tblCalcTechnicalDriverData.IdTechnicalDriver, SUM(tblCalcTechnicalDriverData.Normal) AS SizeNormal, 
			SUM(tblCalcTechnicalDriverData.Pessimistic) AS SizePessimistic, SUM(tblCalcTechnicalDriverData.Optimistic) AS SizeOptimistic
		FROM #tblCalcTechnicalSemifixedSelections INNER JOIN
			tblCalcTechnicalDriverData ON #tblCalcTechnicalSemifixedSelections.IdModel = tblCalcTechnicalDriverData.IdModel AND 
			#tblCalcTechnicalSemifixedSelections.IdField = tblCalcTechnicalDriverData.IdField INNER JOIN
			(
			SELECT IdModel, IdTechnicalDriverSize, IdZiffAccount, IdField, IdProject, Year
			FROM tblCalcTechnicalDriverAssumption
			WHERE ProjectionCriteria=2
			GROUP BY IdModel, IdTechnicalDriverSize, IdZiffAccount, IdField, IdProject, Year
			HAVING IdModel=@LocIdModel
			) AS SelectionList ON tblCalcTechnicalDriverData.IdModel = SelectionList.IdModel AND 
			tblCalcTechnicalDriverData.IdTechnicalDriver = SelectionList.IdTechnicalDriverSize AND tblCalcTechnicalDriverData.IdField = SelectionList.IdField AND 
			tblCalcTechnicalDriverData.IdProject = SelectionList.IdProject AND tblCalcTechnicalDriverData.Year = SelectionList.Year AND 
			#tblCalcTechnicalSemifixedSelections.IdZiffAccount = SelectionList.IdZiffAccount
		WHERE #tblCalcTechnicalSemifixedSelections.TypeAllocation=2
		GROUP BY #tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalDriverData.Year, #tblCalcTechnicalSemifixedSelections.IdZiffAccount, 
			tblCalcTechnicalDriverData.IdTechnicalDriver
		HAVING tblCalcTechnicalDriverData.Year<>@LocMinYear AND #tblCalcTechnicalSemifixedSelections.IdModel=@LocIdModel
		) AS SummaryLookup ON DetailsLookup.IdModel = SummaryLookup.IdModel AND 
		DetailsLookup.Year = SummaryLookup.Year AND DetailsLookup.IdZiffAccount = SummaryLookup.IdZiffAccount AND 
		DetailsLookup.IdTechnicalDriver = SummaryLookup.IdTechnicalDriver
	
	/** Setup Semi-Fixed Driver Splits (Hierarchy) **/
	INSERT INTO #tblCalcTechnicalSemifixedSplit ([IdModel],[Year],[IdZiffAccount],[IdField],[IdProject],[IdTechnicalDriverSize],[TypeAllocation],[SplitNormal],[SplitPessimistic],[SplitOptimistic])
	SELECT DetailsLookup.IdModel, DetailsLookup.Year, DetailsLookup.IdZiffAccount, DetailsLookup.IdField, DetailsLookup.IdProject, DetailsLookup.IdTechnicalDriver, 3 AS TypeAllocation, 
		ISNULL(DetailsLookup.SizeNormal/SummaryLookup.SizeNormal,0) AS SplitNormal, 
		ISNULL(DetailsLookup.SizePessimistic/SummaryLookup.SizePessimistic,0) AS SplitPessimistic, 
		ISNULL(DetailsLookup.SizeOptimistic/SummaryLookup.SizeOptimistic,0) AS SplitOptimistic
	FROM (
		SELECT #tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalDriverData.Year, tblCalcTechnicalDriverData.IdField, 
			tblCalcTechnicalDriverData.IdProject, #tblCalcTechnicalSemifixedSelections.IdZiffAccount, tblCalcTechnicalDriverData.IdTechnicalDriver, 
			SUM(tblCalcTechnicalDriverData.Normal) AS SizeNormal, SUM(tblCalcTechnicalDriverData.Pessimistic) AS SizePessimistic, 
			SUM(tblCalcTechnicalDriverData.Optimistic) AS SizeOptimistic
		FROM #tblCalcTechnicalSemifixedSelections INNER JOIN
			tblCalcTechnicalDriverData ON #tblCalcTechnicalSemifixedSelections.IdModel = tblCalcTechnicalDriverData.IdModel AND 
			#tblCalcTechnicalSemifixedSelections.IdField = tblCalcTechnicalDriverData.IdField INNER JOIN
			(
			SELECT IdModel, IdTechnicalDriverSize, IdZiffAccount, IdField, IdProject, Year
			FROM tblCalcTechnicalDriverAssumption
			WHERE ProjectionCriteria=2
			GROUP BY IdModel, IdTechnicalDriverSize, IdZiffAccount, IdField, IdProject, Year
			HAVING IdModel=@LocIdModel
			) AS SelectionList ON tblCalcTechnicalDriverData.IdModel = SelectionList.IdModel AND 
			tblCalcTechnicalDriverData.IdTechnicalDriver = SelectionList.IdTechnicalDriverSize AND tblCalcTechnicalDriverData.IdField = SelectionList.IdField AND 
			tblCalcTechnicalDriverData.IdProject = SelectionList.IdProject AND tblCalcTechnicalDriverData.Year = SelectionList.Year AND 
			#tblCalcTechnicalSemifixedSelections.IdZiffAccount = SelectionList.IdZiffAccount
		WHERE #tblCalcTechnicalSemifixedSelections.TypeAllocation=3
		GROUP BY #tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalDriverData.Year, #tblCalcTechnicalSemifixedSelections.IdZiffAccount, 
			tblCalcTechnicalDriverData.IdTechnicalDriver, tblCalcTechnicalDriverData.IdField, tblCalcTechnicalDriverData.IdProject
		HAVING tblCalcTechnicalDriverData.Year<>@LocMinYear AND #tblCalcTechnicalSemifixedSelections.IdModel=@LocIdModel
		) AS DetailsLookup LEFT OUTER JOIN
		(
		SELECT #tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalDriverData.Year, #tblCalcTechnicalSemifixedSelections.IdZiffAccount, 
			tblCalcTechnicalDriverData.IdTechnicalDriver, SUM(tblCalcTechnicalDriverData.Normal) AS SizeNormal, 
			SUM(tblCalcTechnicalDriverData.Pessimistic) AS SizePessimistic, SUM(tblCalcTechnicalDriverData.Optimistic) AS SizeOptimistic
		FROM #tblCalcTechnicalSemifixedSelections INNER JOIN
			tblCalcTechnicalDriverData ON #tblCalcTechnicalSemifixedSelections.IdModel = tblCalcTechnicalDriverData.IdModel AND 
			#tblCalcTechnicalSemifixedSelections.IdField = tblCalcTechnicalDriverData.IdField INNER JOIN
			(
			SELECT IdModel, IdTechnicalDriverSize, IdZiffAccount, IdField, IdProject, Year
			FROM tblCalcTechnicalDriverAssumption
			WHERE ProjectionCriteria=2
			GROUP BY IdModel, IdTechnicalDriverSize, IdZiffAccount, IdField, IdProject, Year
			HAVING IdModel=@LocIdModel
			) AS SelectionList ON tblCalcTechnicalDriverData.IdModel = SelectionList.IdModel AND 
			tblCalcTechnicalDriverData.IdTechnicalDriver = SelectionList.IdTechnicalDriverSize AND tblCalcTechnicalDriverData.IdField = SelectionList.IdField AND 
			tblCalcTechnicalDriverData.IdProject = SelectionList.IdProject AND tblCalcTechnicalDriverData.Year = SelectionList.Year AND 
			#tblCalcTechnicalSemifixedSelections.IdZiffAccount = SelectionList.IdZiffAccount
		WHERE #tblCalcTechnicalSemifixedSelections.TypeAllocation=3
		GROUP BY #tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalDriverData.Year, #tblCalcTechnicalSemifixedSelections.IdZiffAccount, 
			tblCalcTechnicalDriverData.IdTechnicalDriver
		HAVING tblCalcTechnicalDriverData.Year<>@LocMinYear AND #tblCalcTechnicalSemifixedSelections.IdModel=@LocIdModel
		) AS SummaryLookup ON DetailsLookup.IdModel = SummaryLookup.IdModel AND 
		DetailsLookup.Year = SummaryLookup.Year AND DetailsLookup.IdZiffAccount = SummaryLookup.IdZiffAccount AND 
		DetailsLookup.IdTechnicalDriver = SummaryLookup.IdTechnicalDriver
	
	/** Mark Semi-Fixed Years to keep **/
	UPDATE #tblCalcTechnicalSemifixedSplit
	SET SemifixedForecast=1
	FROM #tblCalcTechnicalSemifixedSplit INNER JOIN
		tblCalcTechnicalDriverAssumption ON #tblCalcTechnicalSemifixedSplit.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		#tblCalcTechnicalSemifixedSplit.Year = tblCalcTechnicalDriverAssumption.Year AND 
		#tblCalcTechnicalSemifixedSplit.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount AND 
		#tblCalcTechnicalSemifixedSplit.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
		#tblCalcTechnicalSemifixedSplit.IdProject = tblCalcTechnicalDriverAssumption.IdProject AND 
		#tblCalcTechnicalSemifixedSplit.IdTechnicalDriverSize = tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize
	WHERE #tblCalcTechnicalSemifixedSplit.IdModel=@LocIdModel;
	
	/** Delete Semi-Fixed Years to drop **/
	DELETE FROM #tblCalcTechnicalSemifixedSplit WHERE [SemifixedForecast] IS NULL AND IdModel=@LocIdModel;
	
	/** (3) Update Company Projection - Semi-Fixed (Direct Portion) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 3, SemifixedSplit.IdModel, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.CodeAct, tblCalcBaseCostByField.Activity, 
		tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.CodeRes, tblCalcBaseCostByField.Resource, 
		tblCalcBaseCostByField.IdClientAccount, tblCalcBaseCostByField.Account, tblCalcBaseCostByField.NameAccount, tblCalcBaseCostByField.IdField, 
		tblCalcBaseCostByField.Field, tblCalcBaseCostByField.IdClientCostCenter, tblCalcBaseCostByField.ClientCostCenter, 
		tblCalcBaseCostByField.Amount, tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.CodeZiff, 
		tblCalcBaseCostByField.ZiffAccount, tblCalcBaseCostByField.IdRootZiffAccount, tblCalcBaseCostByField.RootCodeZiff, 
		tblCalcBaseCostByField.RootZiffAccount, tblProjects.IdProject, 2 AS ProjectionCriteria, SemifixedSplit.Year, 0 AS BaseYear, 1 AS TFNormal, 
		1 AS TFPessimistic, 1 AS TFOptimistic, tblCalcBaseCostByField.Amount AS BCRNormal, tblCalcBaseCostByField.Amount AS BCRPessimistic, 
		tblCalcBaseCostByField.Amount AS BCROptimistic, 1 AS BaseCostType, tblProjects.Operation
	FROM tblCalcBaseCostByField INNER JOIN
		(
		SELECT [IdModel],[Year],[IdField],[IdZiffAccount] FROM #tblCalcTechnicalSemifixedSplit WHERE [Year]<>@LocMinYear AND [IdModel]=@LocIdModel GROUP BY [IdModel],[Year],[IdField],[IdZiffAccount]
		) AS SemifixedSplit ON tblCalcBaseCostByField.IdModel = SemifixedSplit.IdModel AND 
		tblCalcBaseCostByField.IdZiffAccount = SemifixedSplit.IdZiffAccount AND tblCalcBaseCostByField.IdField = SemifixedSplit.IdField INNER JOIN
		tblProjects ON tblCalcBaseCostByField.IdModel = tblProjects.IdModel AND tblCalcBaseCostByField.IdField = tblProjects.IdField
	WHERE tblCalcBaseCostByField.TypeAllocation=1 AND tblProjects.Operation=1 AND SemifixedSplit.IdModel=@LocIdModel;
	
	/** (4) Update Company Projection - Semi-Fixed (Shared) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 4, #tblCalcTechnicalSemifixedSplit.IdModel, TotalLookup.IdActivity, TotalLookup.CodeAct, TotalLookup.Activity, TotalLookup.IdResources, TotalLookup.CodeRes, 
		TotalLookup.Resource, TotalLookup.IdClientAccount, TotalLookup.Account, TotalLookup.NameAccount, #tblCalcTechnicalSemifixedSplit.IdField, 
		tblFields.Name AS Field, TotalLookup.IdClientCostCenter, TotalLookup.ClientCostCenter, TotalLookup.Amount, TotalLookup.TypeAllocation, 
		TotalLookup.IdZiffAccount, TotalLookup.CodeZiff, TotalLookup.ZiffAccount, TotalLookup.IdRootZiffAccount, TotalLookup.RootCodeZiff, TotalLookup.RootZiffAccount, 
		#tblCalcTechnicalSemifixedSplit.IdProject, TotalLookup.ProjectionCriteria, #tblCalcTechnicalSemifixedSplit.Year, TotalLookup.BaseYear, #tblCalcTechnicalSemifixedSplit.SplitNormal AS TFNormal, 
		#tblCalcTechnicalSemifixedSplit.SplitPessimistic AS TFPessimistic, #tblCalcTechnicalSemifixedSplit.SplitOptimistic AS TFOptimistic, 
		TotalLookup.Amount AS BCRNormal, TotalLookup.Amount AS BCRPessimistic, TotalLookup.Amount AS BCROptimistic, 1 AS BaseCostType, TotalLookup.Operation
	FROM #tblCalcTechnicalSemifixedSplit INNER JOIN
		(SELECT IdModel, IdActivity, CodeAct, Activity, IdResources, CodeRes, Resource, IdClientAccount, Account, NameAccount, IdClientCostCenter, ClientCostCenter, 
		SUM(Amount) AS Amount, TypeAllocation, IdZiffAccount, CodeZiff, ZiffAccount, IdRootZiffAccount, RootCodeZiff, RootZiffAccount, ProjectionCriteria, Year, 
		0 AS BaseYear, BaseCostType, Operation
		FROM #tblCalcProjectionTechnical
		WHERE IdModel=@LocIdModel
		GROUP BY IdModel, IdActivity, CodeAct, Activity, IdResources, CodeRes, Resource, IdClientAccount, Account, NameAccount, IdClientCostCenter, ClientCostCenter, 
		TypeAllocation, IdZiffAccount, CodeZiff, ZiffAccount, IdRootZiffAccount, RootCodeZiff, RootZiffAccount, ProjectionCriteria, Year, BaseCostType, Operation
		HAVING [TypeAllocation]=2 AND [Year]=@LocMinYear AND [ProjectionCriteria]=2
		) AS TotalLookup ON 
		#tblCalcTechnicalSemifixedSplit.IdModel = TotalLookup.IdModel AND #tblCalcTechnicalSemifixedSplit.IdZiffAccount = TotalLookup.IdZiffAccount INNER JOIN
		tblFields ON #tblCalcTechnicalSemifixedSplit.IdField = tblFields.IdField AND 
		#tblCalcTechnicalSemifixedSplit.IdModel = tblFields.IdModel INNER JOIN
		#tblCalcTechnicalSemifixedSelections ON #tblCalcTechnicalSemifixedSplit.IdModel = #tblCalcTechnicalSemifixedSelections.IdModel AND 
		#tblCalcTechnicalSemifixedSplit.IdField = #tblCalcTechnicalSemifixedSelections.IdField AND 
		#tblCalcTechnicalSemifixedSplit.IdZiffAccount = #tblCalcTechnicalSemifixedSelections.IdZiffAccount AND 
		#tblCalcTechnicalSemifixedSplit.IdTechnicalDriverSize = #tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize AND
		#tblCalcTechnicalSemifixedSplit.TypeAllocation = #tblCalcTechnicalSemifixedSelections.TypeAllocation
	WHERE #tblCalcTechnicalSemifixedSplit.TypeAllocation=2 AND #tblCalcTechnicalSemifixedSplit.Year<>@LocMinYear AND #tblCalcTechnicalSemifixedSplit.IdModel=@LocIdModel;
	
	/** (5) Update Company Projection - Semi-Fixed (Hierarchy) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 5, #tblCalcTechnicalSemifixedSplit.IdModel, TotalLookup.IdActivity, TotalLookup.CodeAct, TotalLookup.Activity, TotalLookup.IdResources, TotalLookup.CodeRes, 
		TotalLookup.Resource, TotalLookup.IdClientAccount, TotalLookup.Account, TotalLookup.NameAccount, #tblCalcTechnicalSemifixedSplit.IdField, 
		tblFields.Name AS Field, TotalLookup.IdClientCostCenter, TotalLookup.ClientCostCenter, TotalLookup.Amount, TotalLookup.TypeAllocation, 
		TotalLookup.IdZiffAccount, TotalLookup.CodeZiff, TotalLookup.ZiffAccount, TotalLookup.IdRootZiffAccount, TotalLookup.RootCodeZiff, TotalLookup.RootZiffAccount, 
		#tblCalcTechnicalSemifixedSplit.IdProject, TotalLookup.ProjectionCriteria, #tblCalcTechnicalSemifixedSplit.Year, TotalLookup.BaseYear, #tblCalcTechnicalSemifixedSplit.SplitNormal AS TFNormal, 
		#tblCalcTechnicalSemifixedSplit.SplitPessimistic AS TFPessimistic, #tblCalcTechnicalSemifixedSplit.SplitOptimistic AS TFOptimistic, 
		TotalLookup.Amount AS BCRNormal, TotalLookup.Amount AS BCRPessimistic, TotalLookup.Amount AS BCROptimistic, 1 AS BaseCostType, TotalLookup.Operation
	FROM #tblCalcTechnicalSemifixedSplit INNER JOIN
		(SELECT IdModel, IdActivity, CodeAct, Activity, IdResources, CodeRes, Resource, IdClientAccount, Account, NameAccount, IdClientCostCenter, ClientCostCenter, 
		SUM(Amount) AS Amount, TypeAllocation, IdZiffAccount, CodeZiff, ZiffAccount, IdRootZiffAccount, RootCodeZiff, RootZiffAccount, ProjectionCriteria, Year, 
		0 AS BaseYear, BaseCostType, Operation
		FROM #tblCalcProjectionTechnical
		WHERE IdModel=@LocIdModel
		GROUP BY IdModel, IdActivity, CodeAct, Activity, IdResources, CodeRes, Resource, IdClientAccount, Account, NameAccount, IdClientCostCenter, ClientCostCenter, 
		TypeAllocation, IdZiffAccount, CodeZiff, ZiffAccount, IdRootZiffAccount, RootCodeZiff, RootZiffAccount, ProjectionCriteria, Year, BaseCostType, Operation
		HAVING [TypeAllocation]=3 AND [Year]=@LocMinYear AND [ProjectionCriteria]=2
		) AS TotalLookup ON 
		#tblCalcTechnicalSemifixedSplit.IdModel = TotalLookup.IdModel AND #tblCalcTechnicalSemifixedSplit.IdZiffAccount = TotalLookup.IdZiffAccount INNER JOIN
		tblFields ON #tblCalcTechnicalSemifixedSplit.IdField = tblFields.IdField AND 
		#tblCalcTechnicalSemifixedSplit.IdModel = tblFields.IdModel INNER JOIN
		#tblCalcTechnicalSemifixedSelections ON #tblCalcTechnicalSemifixedSplit.IdModel = #tblCalcTechnicalSemifixedSelections.IdModel AND 
		#tblCalcTechnicalSemifixedSplit.IdField = #tblCalcTechnicalSemifixedSelections.IdField AND 
		#tblCalcTechnicalSemifixedSplit.IdZiffAccount = #tblCalcTechnicalSemifixedSelections.IdZiffAccount AND 
		#tblCalcTechnicalSemifixedSplit.IdTechnicalDriverSize = #tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize AND
		#tblCalcTechnicalSemifixedSplit.TypeAllocation = #tblCalcTechnicalSemifixedSelections.TypeAllocation
	WHERE #tblCalcTechnicalSemifixedSplit.TypeAllocation=3 AND #tblCalcTechnicalSemifixedSplit.Year<>@LocMinYear AND #tblCalcTechnicalSemifixedSplit.IdModel=@LocIdModel;
	
	/** Update Semi-Fixed Business Opportunities to Baseline (Should only be in the baseline project) **/
	UPDATE #tblCalcProjectionTechnical
	SET [IdProject]=tblProjects.IdProject, [Project]=tblProjects.Name
	FROM tblFields LEFT OUTER JOIN
		tblProjects ON tblFields.IdField = tblProjects.IdField RIGHT OUTER JOIN
		#tblCalcProjectionTechnical ON tblFields.IdField = #tblCalcProjectionTechnical.IdField
	WHERE tblProjects.Operation=1 AND #tblCalcProjectionTechnical.ProjectionCriteria=2 AND #tblCalcProjectionTechnical.IdModel=@LocIdModel;
	
	/** 1 - Fixed, 2 - SemiFixed, 3 - Variable, 4 - Cyclical, 5 - SemiVariable **/
	/** (6) Update Company Projection - Fixed **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT DISTINCT 6, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, 
		#tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, #tblCalcProjectionTechnical.NameAccount, 
		#tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, tblCalcTechnicalDriverAssumption.Year, CAST(0 AS BIT) AS BaseYear, 
		#tblCalcProjectionTechnical.TFNormal,#tblCalcProjectionTechnical.TFPessimistic,#tblCalcProjectionTechnical.TFOptimistic,#tblCalcProjectionTechnical.BCRNormal, #tblCalcProjectionTechnical.BCRPessimistic, 
		#tblCalcProjectionTechnical.BCROptimistic, 1 AS BaseCostType, tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		#tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumption.IdModel = #tblCalcProjectionTechnical.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = #tblCalcProjectionTechnical.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = #tblCalcProjectionTechnical.IdField
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=1) 
	AND #tblCalcProjectionTechnical.BaseYear=1 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND #tblCalcProjectionTechnical.BaseCostType=1 
	AND tblCalcTechnicalDriverAssumption.IdModel=@LocIdModel;
		
	/** (7) Update Ziff Projection - Fixed **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT DISTINCT 7, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, 
		#tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, #tblCalcProjectionTechnical.NameAccount, 
		#tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.Year, CAST(0 AS BIT) AS BaseYear, 
		#tblCalcProjectionTechnical.TFNormal, #tblCalcProjectionTechnical.TFPessimistic, #tblCalcProjectionTechnical.TFOptimistic, 
		#tblCalcProjectionTechnical.BCRNormal, #tblCalcProjectionTechnical.BCRPessimistic, #tblCalcProjectionTechnical.BCROptimistic, 2 AS BaseCostType, tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		#tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumptionZiff.IdModel = #tblCalcProjectionTechnical.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = #tblCalcProjectionTechnical.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = #tblCalcProjectionTechnical.IdField
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=1) 
	AND #tblCalcProjectionTechnical.BaseYear=1 
	AND #tblCalcProjectionTechnical.BaseCostType=2 
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0 
	AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@LocIdModel;
		
	/** (8) Update Company Projection - Variable and Cyclical **/
	/** Baselines **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 8, tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.CodeAct, tblCalcBaseCostByField.Activity, 
		tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.CodeRes, tblCalcBaseCostByField.Resource, 
		tblCalcBaseCostByField.IdClientAccount, tblCalcBaseCostByField.Account, tblCalcBaseCostByField.NameAccount, tblCalcBaseCostByField.IdField, 
		tblCalcBaseCostByField.Field, tblCalcBaseCostByField.IdClientCostCenter, tblCalcBaseCostByField.ClientCostCenter, 
		tblCalcBaseCostByField.Amount, tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.CodeZiff, 
		tblCalcBaseCostByField.ZiffAccount, tblCalcBaseCostByField.IdRootZiffAccount, tblCalcBaseCostByField.RootCodeZiff, 
		tblCalcBaseCostByField.RootZiffAccount, tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumption.Year, tblCalcTechnicalDriverAssumption.BaseYear, 
		tblCalcBaseCostByField.TFNormal,tblCalcBaseCostByField.TFPessimistic,tblCalcBaseCostByField.TFOptimistic, 
		tblCalcBaseCostByField.BCRNormal, tblCalcBaseCostByField.BCRPessimistic, tblCalcBaseCostByField.BCROptimistic, 1 AS BaseCostType, 
		tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=3 OR tblCalcTechnicalDriverAssumption.ProjectionCriteria=4) 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND tblCalcBaseCostByField.IdModel=@LocIdModel
	AND tblCalcTechnicalDriverAssumption.Operation = 1;
	
	/** Business Opportunities **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 8, tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.CodeAct, tblCalcBaseCostByField.Activity, 
		tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.CodeRes, tblCalcBaseCostByField.Resource, 
		tblCalcBaseCostByField.IdClientAccount, tblCalcBaseCostByField.Account, tblCalcBaseCostByField.NameAccount, tblCalcBaseCostByField.IdField, 
		tblCalcBaseCostByField.Field, tblCalcBaseCostByField.IdClientCostCenter, tblCalcBaseCostByField.ClientCostCenter, 
		tblCalcBaseCostByField.Amount, tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.CodeZiff, 
		tblCalcBaseCostByField.ZiffAccount, tblCalcBaseCostByField.IdRootZiffAccount, tblCalcBaseCostByField.RootCodeZiff, 
		tblCalcBaseCostByField.RootZiffAccount, tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumption.Year, tblCalcTechnicalDriverAssumption.BaseYear, tblCalcTechnicalDriverAssumption.TFNormal, 
		tblCalcTechnicalDriverAssumption.TFPessimistic, tblCalcTechnicalDriverAssumption.TFOptimistic, tblCalcBaseCostByField.BCRNormal, 
		tblCalcBaseCostByField.BCRPessimistic, tblCalcBaseCostByField.BCROptimistic, 1 AS BaseCostType, 
		tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=3 OR tblCalcTechnicalDriverAssumption.ProjectionCriteria=4) 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND tblCalcBaseCostByField.IdModel=@LocIdModel
	AND tblCalcTechnicalDriverAssumption.Operation <> 1;

	/** (9) Update Ziff Projection - Variable and Cyclical **/
	/** Baselines **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 9, tblCalcBaseCostByFieldZiff.IdModel, NULL, NULL, NULL, NULL, NULL, NULL, 
		NULL, NULL, NULL, tblCalcBaseCostByFieldZiff.IdField, tblCalcBaseCostByFieldZiff.Field, NULL, NULL, 
		tblCalcBaseCostByFieldZiff.TotalCost, NULL, tblCalcBaseCostByFieldZiff.IdZiffAccount, tblCalcBaseCostByFieldZiff.CodeZiff, 
		tblCalcBaseCostByFieldZiff.ZiffAccount, tblCalcBaseCostByFieldZiff.IdRootZiffAccount, tblCalcBaseCostByFieldZiff.RootCodeZiff, 
		tblCalcBaseCostByFieldZiff.RootZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumptionZiff.Year, tblCalcTechnicalDriverAssumptionZiff.BaseYear, tblCalcBaseCostByFieldZiff.TFNormal,tblCalcBaseCostByFieldZiff.TFPessimistic,tblCalcBaseCostByFieldZiff.TFOptimistic, 
		tblCalcBaseCostByFieldZiff.BCRNormal, tblCalcBaseCostByFieldZiff.BCRPessimistic, tblCalcBaseCostByFieldZiff.BCROptimistic, 2 AS BaseCostType, 
		tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=3 OR tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=4) 
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0 
	AND tblCalcBaseCostByFieldZiff.IdModel=@LocIdModel
	AND tblCalcTechnicalDriverAssumptionZiff.Operation=1;

	/** Business Opportunities **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 9, tblCalcBaseCostByFieldZiff.IdModel, NULL, NULL, NULL, NULL, NULL, NULL, 
		NULL, NULL, NULL, tblCalcBaseCostByFieldZiff.IdField, tblCalcBaseCostByFieldZiff.Field, NULL, NULL, 
		tblCalcBaseCostByFieldZiff.TotalCost, NULL, tblCalcBaseCostByFieldZiff.IdZiffAccount, tblCalcBaseCostByFieldZiff.CodeZiff, 
		tblCalcBaseCostByFieldZiff.ZiffAccount, tblCalcBaseCostByFieldZiff.IdRootZiffAccount, tblCalcBaseCostByFieldZiff.RootCodeZiff, 
		tblCalcBaseCostByFieldZiff.RootZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumptionZiff.Year, tblCalcTechnicalDriverAssumptionZiff.BaseYear, tblCalcTechnicalDriverAssumptionZiff.TFNormal, 
		tblCalcTechnicalDriverAssumptionZiff.TFPessimistic, tblCalcTechnicalDriverAssumptionZiff.TFOptimistic, tblCalcBaseCostByFieldZiff.BCRNormal, 
		tblCalcBaseCostByFieldZiff.BCRPessimistic, tblCalcBaseCostByFieldZiff.BCROptimistic, 2 AS BaseCostType, 
		tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=3 OR tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=4)
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0 AND tblCalcBaseCostByFieldZiff.IdModel=@LocIdModel
	AND tblCalcTechnicalDriverAssumptionZiff.Operation<>1;

	/** (10) Update Company Projection - SemiVariable (BaseLine) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT DISTINCT 10, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, 
		#tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, #tblCalcProjectionTechnical.NameAccount, 
		#tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, tblCalcTechnicalDriverAssumption.Year, CAST(0 AS BIT) AS BaseYear, 
		#tblCalcProjectionTechnical.TFNormal, #tblCalcProjectionTechnical.TFPessimistic, #tblCalcProjectionTechnical.TFOptimistic, 
		#tblCalcProjectionTechnical.BCRNormal, #tblCalcProjectionTechnical.BCRPessimistic, #tblCalcProjectionTechnical.BCROptimistic, 1 AS BaseCostType, tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		#tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumption.IdModel = #tblCalcProjectionTechnical.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = #tblCalcProjectionTechnical.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = #tblCalcProjectionTechnical.IdField
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=5) 
	AND #tblCalcProjectionTechnical.BaseYear=1 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND #tblCalcProjectionTechnical.BaseCostType=1 
	AND #tblCalcProjectionTechnical.Operation=1
	AND tblCalcTechnicalDriverAssumption.IdModel=@LocIdModel;

	/** (11) Update Ziff Projection - SemiVariable (BaseLine) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT DISTINCT 11, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, 
		#tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, #tblCalcProjectionTechnical.NameAccount, 
		#tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.Year, CAST(0 AS BIT) AS BaseYear, 
		0,0,0,0,0,0, 2 AS BaseCostType, tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		#tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumptionZiff.IdModel = #tblCalcProjectionTechnical.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = #tblCalcProjectionTechnical.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = #tblCalcProjectionTechnical.IdField		
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5) 
	AND #tblCalcProjectionTechnical.BaseYear=1 
	AND #tblCalcProjectionTechnical.BaseCostType=2 
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0
	AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 
	AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@LocIdModel;

	/** Volume and Utilization preparation tables **/
	/** Get Base Year **/
	DECLARE @BaseYear INT
	SELECT @BaseYear = [BaseYear] FROM [DBCPM].[dbo].[tblModels] WHERE IdModel=@LocIdModel

	/** Build Volume Tables **/
	/** Report (Baseline)**/
	CREATE TABLE #VolumeReportBLRows ([Name] NVARCHAR(255), [YearProjection] INT, [NormalAmount] FLOAT, [OptimisticAmount] FLOAT, [PessimisticAmount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLRows ([Name], [YearProjection], [NormalAmount], [OptimisticAmount], [PessimisticAmount])	
	SELECT	dbo.tblProjects.Name AS Name, dbo.tblTechnicalDriverData.Year AS YearProjection, 
				ISNULL(dbo.tblTechnicalDriverData.Normal,0) AS NormalAmount,
				ISNULL(dbo.tblTechnicalDriverData.Optimistic,0) AS OptimisticAmount,
				ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0) AS PessimisticAmount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON
			dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject
	WHERE	dbo.tblTechnicalDriverData.IdModel = @LocIdModel
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=1
	ORDER BY dbo.tblTechnicalDriverData.Year
	
	CREATE TABLE #VolumeReportBL ([Name] NVARCHAR(255), [YearProjection] INT, [NormalAmount] FLOAT, [OptimisticAmount] FLOAT, [PessimisticAmount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBL([Name], [YearProjection], [NormalAmount], [OptimisticAmount], [PessimisticAmount])
	SELECT	Name, YearProjection, SUM(NormalAmount), SUM(OptimisticAmount), SUM(PessimisticAmount)
	FROM	#VolumeReportBLRows
	GROUP BY Name, YearProjection
	ORDER BY Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBLList (Name NVARCHAR(255), YearProjection INT, [NormalAmount] FLOAT, [OptimisticAmount] FLOAT, [PessimisticAmount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLList ([Name],[YearProjection], [NormalAmount], [OptimisticAmount], [PessimisticAmount])
	SELECT CNList.Name, CYList.Year, 0, 0, 0 FROM
		(SELECT [Name] FROM #VolumeReportBL WHERE [Name] IS NOT NULL GROUP BY [Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@LocIdModel) AS CYList 

	/** Add Any Missing Years to #BLList to Fill Properly **/
	INSERT INTO #VolumeReportBL ([Name],[YearProjection],[NormalAmount],[OptimisticAmount],[PessimisticAmount])
	SELECT	#VolumeReportBLList.Name, #VolumeReportBLList.YearProjection, 0 AS NormalAmount, 0 AS OptimisticAmount,  0 AS PessimisticAmount
	FROM	#VolumeReportBLList LEFT OUTER JOIN
			#VolumeReportBL ON #VolumeReportBLList.Name=#VolumeReportBL.Name AND #VolumeReportBLList.Name=#VolumeReportBL.Name AND #VolumeReportBLList.YearProjection=#VolumeReportBL.YearProjection
	WHERE	#VolumeReportBL.YearProjection IS NULL;

	/** Report (Business Opportunities)**/
	CREATE TABLE #VolumeReportBORows ([Name] NVARCHAR(255), [YearProjection] INT, [NormalAmount] FLOAT, [OptimisticAmount] FLOAT, [PessimisticAmount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBORows ([Name], [YearProjection], [NormalAmount], [OptimisticAmount], [PessimisticAmount])	
		SELECT	dbo.tblProjects.Name AS Name, dbo.tblTechnicalDriverData.Year AS YearProjection, 
				ISNULL(dbo.tblTechnicalDriverData.Normal,0) AS NormalAmount,
				ISNULL(dbo.tblTechnicalDriverData.Optimistic,0) AS OptimisticAmount,
				ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0) AS PessimisticAmount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON
			dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject
	WHERE	dbo.tblTechnicalDriverData.IdModel = @LocIdModel
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=2
	ORDER BY dbo.tblTechnicalDriverData.Year

	CREATE TABLE #VolumeReportBO ([Name] NVARCHAR(255), [YearProjection] INT, [NormalAmount] FLOAT, [OptimisticAmount] FLOAT, [PessimisticAmount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBO([Name], [YearProjection], [NormalAmount], [OptimisticAmount], [PessimisticAmount])
	SELECT	Name, YearProjection, SUM(NormalAmount), SUM(OptimisticAmount), SUM(PessimisticAmount)
	FROM	#VolumeReportBORows
	GROUP BY Name, YearProjection
	ORDER BY Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBOList (Name NVARCHAR(255), YearProjection INT, [NormalAmount] FLOAT, [OptimisticAmount] FLOAT, [PessimisticAmount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBOList ([Name],[YearProjection], [NormalAmount], [OptimisticAmount], [PessimisticAmount])
	SELECT CNList.Name, CYList.Year, 0, 0, 0 FROM
		(SELECT [Name] FROM #VolumeReportBO WHERE [Name] IS NOT NULL GROUP BY [Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@LocIdModel) AS CYList 

	/** Add Any Missing Years to #BOList to Fill Properly **/
	INSERT INTO #VolumeReportBO ([Name],[YearProjection],[NormalAmount],[OptimisticAmount],[PessimisticAmount])
	SELECT	#VolumeReportBOList.Name, #VolumeReportBOList.YearProjection, 0 AS NormalAmount, 0 AS OptimisticAmount,  0 AS PessimisticAmount
	FROM	#VolumeReportBOList LEFT OUTER JOIN
			#VolumeReportBO ON #VolumeReportBOList.Name=#VolumeReportBO.Name AND #VolumeReportBOList.Name=#VolumeReportBO.Name AND #VolumeReportBOList.YearProjection=#VolumeReportBO.YearProjection
	WHERE	#VolumeReportBO.YearProjection IS NULL;

	/** Report (All Fields Grouped)**/
	CREATE TABLE #VolumeReportGroupedBL ([YearProjection] INT, [NormalAmount] FLOAT, [OptimisticAmount] FLOAT, [PessimisticAmount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBL ([YearProjection], [NormalAmount], [OptimisticAmount], [PessimisticAmount])
	SELECT YearProjection,SUM(NormalAmount) AS NormalAmount,SUM(OptimisticAmount) AS OptimisticAmount,SUM(PessimisticAmount) AS PessimisticAmount
	FROM #VolumeReportBL
	GROUP BY YearProjection
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGroupedBO ([YearProjection] INT, [NormalAmount] FLOAT, [OptimisticAmount] FLOAT, [PessimisticAmount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBO ([YearProjection], [NormalAmount], [OptimisticAmount], [PessimisticAmount])
	SELECT YearProjection,SUM(NormalAmount) AS NormalAmount,SUM(OptimisticAmount) AS OptimisticAmount,SUM(PessimisticAmount) AS PessimisticAmount
	FROM #VolumeReportBO
	GROUP BY YearProjection
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGrouped ([YearProjection] INT, [NormalAmount] FLOAT, [OptimisticAmount] FLOAT, [PessimisticAmount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGrouped ([YearProjection], [NormalAmount], [OptimisticAmount], [PessimisticAmount])
	SELECT #VolumeReportGroupedBL.YearProjection,
		   #VolumeReportGroupedBL.NormalAmount + #VolumeReportGroupedBO.NormalAmount AS NormalAmount,
		   #VolumeReportGroupedBL.OptimisticAmount + #VolumeReportGroupedBO.OptimisticAmount AS OptimisticAmount,
		   #VolumeReportGroupedBL.PessimisticAmount + #VolumeReportGroupedBO.PessimisticAmount AS PessimisticAmount
	FROM #VolumeReportGroupedBL INNER JOIN	#VolumeReportGroupedBO ON #VolumeReportGroupedBO.YearProjection = #VolumeReportGroupedBL.YearProjection		   

	DECLARE @TotalCapacityDriverNormal INT = 0;
	DECLARE @TotalCapacityDriverOptimistic INT = 0;
	DECLARE @TotalCapacityDriverPessimistic INT = 0;

	/** Get grouped driver value for Base Year **/
	SELECT @TotalCapacityDriverNormal = NormalAmount FROM #VolumeReportGrouped WHERE YearProjection=@BaseYear
	SELECT @TotalCapacityDriverOptimistic = OptimisticAmount FROM #VolumeReportGrouped WHERE YearProjection=@BaseYear
	SELECT @TotalCapacityDriverPessimistic = PessimisticAmount FROM #VolumeReportGrouped WHERE YearProjection=@BaseYear

	/** Build Utilization Capacity Values **/
	CREATE TABLE #UtilizationCapacityValues ([CodeZiff] NVARCHAR(50), [IdField] INT, [UtilizationValueNormal] FLOAT, [UtilizationValueOptimistic] FLOAT, [UtilizationValuePessimistic] FLOAT) ON [PRIMARY]
	INSERT INTO #UtilizationCapacityValues([CodeZiff], [IdField], [UtilizationValueNormal], [UtilizationValueOptimistic], [UtilizationValuePessimistic])
	SELECT  A.[CodeZiff], tblFields.IdField, @TotalCapacityDriverNormal/A.UtilizationValue*100, @TotalCapacityDriverOptimistic/A.UtilizationValue*100, @TotalCapacityDriverPessimistic/A.UtilizationValue*100
	  FROM tblUtilizationCapacity A INNER JOIN tblFields
			ON A.IdField = tblFields.IdField      
	WHERE  	A.IdModel = @LocIdModel

	/** (12) Update Company Projection - SemiVariable (Business Opportunity) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 12, tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.CodeAct, tblCalcBaseCostByField.Activity, 
		tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.CodeRes, tblCalcBaseCostByField.Resource, 
		tblCalcBaseCostByField.IdClientAccount, tblCalcBaseCostByField.Account, tblCalcBaseCostByField.NameAccount, tblCalcBaseCostByField.IdField, 
		tblCalcBaseCostByField.Field, tblCalcBaseCostByField.IdClientCostCenter, tblCalcBaseCostByField.ClientCostCenter, 
		tblCalcBaseCostByField.Amount, tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.CodeZiff, 
		tblCalcBaseCostByField.ZiffAccount, tblCalcBaseCostByField.IdRootZiffAccount, tblCalcBaseCostByField.RootCodeZiff, 
		tblCalcBaseCostByField.RootZiffAccount, tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumption.Year, tblCalcTechnicalDriverAssumption.BaseYear, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValueNormal < #VolumeReportGrouped.NormalAmount THEN tblCalcTechnicalDriverAssumption.TFNormal ELSE 0 END AS TFNormal, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValuePessimistic < #VolumeReportGrouped.PessimisticAmount THEN tblCalcTechnicalDriverAssumption.TFPessimistic ELSE 0 END AS TFPessimistic, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValueOptimistic < #VolumeReportGrouped.OptimisticAmount THEN tblCalcTechnicalDriverAssumption.TFOptimistic ELSE 0 END AS TFOptimistic, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValueNormal < #VolumeReportGrouped.NormalAmount THEN tblCalcBaseCostByField.BCRNormal ELSE 0 END AS BCRNormal, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValuePessimistic < #VolumeReportGrouped.PessimisticAmount THEN tblCalcBaseCostByField.BCRPessimistic ELSE 0 END AS BCRPessimistic, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValueOptimistic < #VolumeReportGrouped.OptimisticAmount THEN tblCalcBaseCostByField.BCROptimistic ELSE 0 END AS BCROptimistic, 
		1 AS BaseCostType, 
		tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount INNER JOIN
		#UtilizationCapacityValues ON #UtilizationCapacityValues.CodeZiff = tblCalcBaseCostByField.CodeZiff AND
		#UtilizationCapacityValues.IdField = tblCalcBaseCostByField.IdField INNER JOIN
		#VolumeReportGrouped ON #VolumeReportGrouped.YearProjection = tblCalcTechnicalDriverAssumption.Year
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=5) 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND tblCalcTechnicalDriverAssumption.Operation=2
	AND tblCalcBaseCostByField.IdModel=@LocIdModel;

	/** (13) Update Ziff Projection - SemiVariable (Business Opportunity)  **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 13, tblCalcBaseCostByFieldZiff.IdModel, NULL, NULL, NULL, NULL, NULL, NULL, 
		NULL, NULL, NULL, tblCalcBaseCostByFieldZiff.IdField, tblCalcBaseCostByFieldZiff.Field, NULL, NULL, 
		tblCalcBaseCostByFieldZiff.TotalCost, NULL, tblCalcBaseCostByFieldZiff.IdZiffAccount, tblCalcBaseCostByFieldZiff.CodeZiff, 
		tblCalcBaseCostByFieldZiff.ZiffAccount, tblCalcBaseCostByFieldZiff.IdRootZiffAccount, tblCalcBaseCostByFieldZiff.RootCodeZiff, 
		tblCalcBaseCostByFieldZiff.RootZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumptionZiff.Year, tblCalcTechnicalDriverAssumptionZiff.BaseYear, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValueNormal < #VolumeReportGrouped.NormalAmount THEN tblCalcTechnicalDriverAssumptionZiff.TFNormal ELSE 0 END AS TFNormal, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValuePessimistic < #VolumeReportGrouped.PessimisticAmount THEN tblCalcTechnicalDriverAssumptionZiff.TFPessimistic ELSE 0 END AS TFPessimistic, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValueOptimistic < #VolumeReportGrouped.OptimisticAmount THEN tblCalcTechnicalDriverAssumptionZiff.TFOptimistic ELSE 0 END AS TFOptimistic, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValueNormal < #VolumeReportGrouped.NormalAmount THEN tblCalcBaseCostByFieldZiff.BCRNormal ELSE 0 END AS BCRNormal, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValuePessimistic < #VolumeReportGrouped.PessimisticAmount THEN tblCalcBaseCostByFieldZiff.BCRPessimistic ELSE 0 END AS BCRPessimistic, 
		CASE WHEN #UtilizationCapacityValues.UtilizationValueOptimistic < #VolumeReportGrouped.OptimisticAmount THEN tblCalcBaseCostByFieldZiff.BCROptimistic ELSE 0 END AS BCROptimistic, 
		2 AS BaseCostType, 
		tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount INNER JOIN
		#UtilizationCapacityValues ON #UtilizationCapacityValues.CodeZiff = tblCalcBaseCostByFieldZiff.CodeZiff AND
		#UtilizationCapacityValues.IdField = tblCalcBaseCostByFieldZiff.IdField INNER JOIN
		#VolumeReportGrouped ON #VolumeReportGrouped.YearProjection = tblCalcTechnicalDriverAssumptionZiff.Year
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5) 
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0 
	AND tblCalcTechnicalDriverAssumptionZiff.Operation=2
	AND tblCalcBaseCostByFieldZiff.IdModel=@LocIdModel;

	/** Apply denominator if applicable **/
	/** Create temporary table to house changes **/
	IF OBJECT_ID('tempdb..#HousingDataTable') IS NOT NULL DROP TABLE #HousingDataTable
	SELECT 
		#tblCalcProjectionTechnical.SourceDebug, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, #tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, 
		#tblCalcProjectionTechnical.NameAccount, #tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		TDUCD.IdProject, #tblCalcProjectionTechnical.ProjectionCriteria, #tblCalcProjectionTechnical.Year, #tblCalcProjectionTechnical.BaseYear, 
		#tblCalcProjectionTechnical.TFNormal * TDUCD.TFNormal AS TFNormal,
		#tblCalcProjectionTechnical.TFPessimistic * TDUCD.TFPessimistic AS TFPessimistic,
		#tblCalcProjectionTechnical.TFOptimistic * TDUCD.TFOptimistic AS TFOptimistic,
		#tblCalcProjectionTechnical.BCRNormal, #tblCalcProjectionTechnical.BCRPessimistic, 
		#tblCalcProjectionTechnical.BCROptimistic, #tblCalcProjectionTechnical.BaseCostType, #tblCalcProjectionTechnical.Operation,#tblCalcProjectionTechnical.SortOrder
	INTO #HousingDataTable	
	FROM #tblCalcProjectionTechnical INNER JOIN
		(
		SELECT tblCalcUnitCostDenominatorAmount.IdModel, tblCalcUnitCostDenominatorAmount.IdField, tblCalcUnitCostDenominatorAmount.IdProject, 
		tblCalcUnitCostDenominatorAmount.Year, 
		tblCalcUnitCostDenominatorAmount.SumNormal / tblCalcBaselineUnitCostDenominatorSumAmount.SumNormal AS TFNormal, 
		tblCalcUnitCostDenominatorAmount.SumPessimistic / tblCalcBaselineUnitCostDenominatorSumAmount.SumPessimistic AS TFPessimistic, 
		tblCalcUnitCostDenominatorAmount.SumOptimistic / tblCalcBaselineUnitCostDenominatorSumAmount.SumOptimistic AS TFOptimistic
		FROM tblCalcUnitCostDenominatorAmount INNER JOIN
		tblCalcBaselineUnitCostDenominatorSumAmount ON 
		tblCalcUnitCostDenominatorAmount.IdModel = tblCalcBaselineUnitCostDenominatorSumAmount.IdModel AND 
		tblCalcUnitCostDenominatorAmount.IdField = tblCalcBaselineUnitCostDenominatorSumAmount.IdField AND 
		tblCalcUnitCostDenominatorAmount.Year = tblCalcBaselineUnitCostDenominatorSumAmount.Year RIGHT OUTER JOIN 
		tblProjects  ON tblCalcUnitCostDenominatorAmount.IdModel = tblProjects.IdModel AND
		tblCalcUnitCostDenominatorAmount.IdField = tblProjects.IdField AND 
		tblCalcUnitCostDenominatorAmount.IdProject = tblProjects.IdProject
		WHERE tblProjects.Operation=1
		AND tblCalcUnitCostDenominatorAmount.IdModel=@LocIdModel) AS TDUCD ON 
		#tblCalcProjectionTechnical.IdModel = TDUCD.IdModel AND #tblCalcProjectionTechnical.IdField = TDUCD.IdField AND #tblCalcProjectionTechnical.Year = TDUCD.Year
	WHERE #tblCalcProjectionTechnical.IdModel=@LocIdModel AND #tblCalcProjectionTechnical.BaseCostType = 1;

	/** Delete data from #tblCalcProjectionTechnical **/
	DECLARE @DeleteField INT
	DECLARE MultiBaselineFields CURSOR 
	FOR SELECT IdField--, COUNT(IdProject)  
		FROM tblProjects 
		WHERE IdModel=@LocIdModel AND Operation=1
		GROUP BY idfield
		HAVING COUNT(IdProject) > 1
		ORDER BY IdField
		
	OPEN MultiBaselineFields
	
	FETCH NEXT FROM MultiBaselineFields
	INTO @DeleteField

	WHILE @@FETCH_STATUS = 0
	BEGIN
		DELETE #tblCalcProjectionTechnical 
		WHERE IdModel=@LocIdModel 
		AND BaseCostType = 1 
		AND IdField = @DeleteField
		AND YEAR IN (SELECT tblCalcUnitCostDenominatorAmount.Year
					 FROM tblCalcUnitCostDenominatorAmount INNER JOIN
						 tblCalcBaselineUnitCostDenominatorSumAmount ON 
						 tblCalcUnitCostDenominatorAmount.IdModel = tblCalcBaselineUnitCostDenominatorSumAmount.IdModel AND 
						 tblCalcUnitCostDenominatorAmount.IdField = tblCalcBaselineUnitCostDenominatorSumAmount.IdField AND 
						 tblCalcUnitCostDenominatorAmount.Year = tblCalcBaselineUnitCostDenominatorSumAmount.Year RIGHT OUTER JOIN 
						 tblProjects  ON tblCalcUnitCostDenominatorAmount.IdModel = tblProjects.IdModel AND
						 tblCalcUnitCostDenominatorAmount.IdField = tblProjects.IdField AND 
						 tblCalcUnitCostDenominatorAmount.IdProject = tblProjects.IdProject
					 WHERE tblProjects.Operation=1
					 AND tblCalcUnitCostDenominatorAmount.IdModel=@LocIdModel 
					 AND tblCalcBaselineUnitCostDenominatorSumAmount.IdField=@DeleteField 
					 GROUP BY tblCalcUnitCostDenominatorAmount.Year)

		/** Insert data from #HousingDataTable into #tblCalcProjectionTechnical  for that field**/
		INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[SortOrder])
		SELECT SourceDebug, IdModel, IdActivity, CodeAct, Activity,IdResources, CodeRes, Resource, IdClientAccount, Account, NameAccount, IdField, Field, IdClientCostCenter, 
			ClientCostCenter, Amount, TypeAllocation, IdZiffAccount, CodeZiff, ZiffAccount, IdRootZiffAccount, RootCodeZiff, RootZiffAccount, IdProject, ProjectionCriteria, Year, BaseYear, 
			TFNormal, TFPessimistic, TFOptimistic,BCRNormal, BCRPessimistic, BCROptimistic, BaseCostType, Operation,SortOrder
		FROM #HousingDataTable	
		WHERE IdField = @DeleteField
	
		FETCH NEXT FROM MultiBaselineFields
		INTO @DeleteField
	END
	CLOSE MultiBaselineFields
	DEALLOCATE MultiBaselineFields
	
	/** Prep for Reporting **/
	UPDATE #tblCalcProjectionTechnical SET [Activity]='Ziff/Third-party Costs',[SortOrder]=2 WHERE [Activity] IS NULL AND [BaseCostType]=2 AND [IdModel]=@LocIdModel;
	UPDATE #tblCalcProjectionTechnical SET [Resource]='Ziff/Third-party Costs',[SortOrder]=2 WHERE [Resource] IS NULL AND [BaseCostType]=2 AND [IdModel]=@LocIdModel;
	UPDATE #tblCalcProjectionTechnical SET [Project]=tblProjects.Name, [Operation]=tblProjects.Operation FROM #tblCalcProjectionTechnical INNER JOIN tblProjects ON #tblCalcProjectionTechnical.IdProject = tblProjects.IdProject AND #tblCalcProjectionTechnical.IdModel = tblProjects.IdModel WHERE #tblCalcProjectionTechnical.IdModel=@LocIdModel;
	UPDATE #tblCalcProjectionTechnical SET [Project]='Unknown Project' WHERE [Project] IS NULL AND [IdProject] IS NOT NULL AND [IdModel]=@LocIdModel;

	/** Move data from temp table #tblCalcProjectionTecnical to permanent table tblCalcProjectionTechnical **/
	DELETE FROM tblCalcProjectionTechnical where IdModel=@LocIdModel
	INSERT INTO tblCalcProjectionTechnical 
	SELECT * FROM #tblCalcProjectionTechnical
	
	/** Clean up temporary tables **/
	DROP TABLE #tblCalcProjectionTechnical
	DROP TABLE #tblCalcTechnicalSemifixedSelections
	DROP TABLE #tblCalcTechnicalSemifixedSplit
	DROP TABLE #UtilizationCapacityValues
	DROP TABLE #VolumeReportGrouped
	DROP TABLE #VolumeReportGroupedBO
	DROP TABLE #VolumeReportGroupedBL
	DROP TABLE #VolumeReportBOList
	DROP TABLE #VolumeReportBO
	DROP TABLE #VolumeReportBORows
	DROP TABLE #VolumeReportBLList
	DROP TABLE #VolumeReportBL
	DROP TABLE #VolumeReportBLRows
	
END
GO

USE [DBCPM]
GO

/****** Object:  Table [dbo].[tblCalcBaselineUnitCostDenominatorSumAmount]    Script Date: 08/12/2015 15:06:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[tblCalcBaselineUnitCostDenominatorSumAmount]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[tblCalcBaselineUnitCostDenominatorSumAmount](
		[IdModel] [int] NOT NULL,
		[IdField] [int] NOT NULL,
		[Year] [int] NOT NULL,
		[SumNormal] [float] NULL,
		[SumPessimistic] [float] NULL,
		[SumOptimistic] [float] NULL
	) ON [PRIMARY]
END
GO

USE [DBCPM]
GO

/****** Object:  StoredProcedure [dbo].[reppOpexProjection]    Script Date: 08/12/2015 15:09:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:		Gareth Slater
-- Create date: 2014-05-05
-- Description:	Module Report Opex Projection
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 6/10/2015	RM		Added @Factor parameter
-- 8/5/2015		RM		Cleaned up unnecessary code in stored procedure
-- ===========================================================================================
ALTER PROCEDURE [dbo].[reppOpexProjection](
@IdModel INT,
@IdAggregationLevel INT,
@IdField INT,
@IdStructure INT,
@From INT,
@To INT,
@IdTechnicalScenario INT,
@IdEconomicScenario INT,
@IdCurrency INT,
@IdTerm INT,
@IdTechnicalDriver INT,
@IdCostType INT,
@IdTypeOperation INT,
@Factor INT
)
AS
BEGIN
	DECLARE @StartYear INT = @From;
	DECLARE @EndYear INT = @To;
	
	/** Get Base Year **/
	DECLARE @BaseYear INT
	SELECT @BaseYear = [BaseYear] FROM [DBCPM].[dbo].[tblModels] WHERE IdModel=@IdModel

	/** Build Year Columns **/
	DECLARE @YearColumnList NVARCHAR(MAX)='';	
	WHILE (@From <= @To)
	BEGIN
		SET @YearColumnList =  @YearColumnList + '[' + CAST(@From AS VARCHAR(4)) + ']';
		SET @From = @From + 1;
		IF (@From <= @To)
		BEGIN
			SET @YearColumnList = @YearColumnList + ',';
		END
	END
	
	/** Build Field List **/
	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF (@IdAggregationLevel=0 AND @IdField=0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0 AND @IdField>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdField>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	DECLARE @FieldCount INT;
	SELECT @FieldCount=COUNT([IdField]) FROM #FieldList
	
	/** Get Chart Columns to Temp Table **/
	CREATE TABLE #AccountList (ColumnName NVARCHAR(255) PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF @IdStructure=1
	BEGIN
		INSERT INTO #AccountList SELECT [RootZiffAccount] FROM tblCalcProjectionTechnical WHERE IdField IN (SELECT [IdField] FROM #FieldList) AND [IdModel]=@IdModel GROUP BY [RootZiffAccount] ORDER BY [RootZiffAccount];
	END
	ELSE IF @IdStructure=2
	BEGIN
		INSERT INTO #AccountList SELECT [Activity] FROM tblCalcProjectionTechnical WHERE IdField IN (SELECT [IdField] FROM #FieldList) AND [IdModel]=@IdModel GROUP BY [Activity], [SortOrder] ORDER BY [SortOrder], [Activity];
	END
	
	/** Generate Chart Columns from Temp Table **/
	DECLARE @AccountColumnList NVARCHAR(MAX)='';
	DECLARE @AccountName NVARCHAR(255);
	DECLARE AccountList CURSOR FOR
	SELECT ColumnName FROM #AccountList;

	OPEN AccountList;
	FETCH NEXT FROM AccountList INTO @AccountName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @AccountColumnList=''
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + '[' + @AccountName + ']';
		END
		ELSE
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + ',[' + @AccountName + ']';
		END
		FETCH NEXT FROM AccountList INTO @AccountName;
	END

	CLOSE AccountList;
	DEALLOCATE AccountList;
	
	/** Get Currency Factor to use **/
	DECLARE @CurrencyFactor FLOAT;
	SELECT @CurrencyFactor=[Value] FROM tblModelsCurrencies WHERE [IdCurrency]=@IdCurrency AND [IdModel]=@IdModel;
	
	/** Build Technical Driver Factors (used for KPI Report) **/
	CREATE TABLE #TechDriver ([Year] INT, [Normal] FLOAT, [Optimistic] FLOAT, [Pessimistic] FLOAT) ON [PRIMARY]
	IF @IdTechnicalDriver=0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, 1 AS [Normal], 1 AS [Optimistic], 1 AS [Pessimistic]
		FROM tblCalcModelYears
		WHERE [IdModel]=@IdModel AND [Year] BETWEEN @StartYear AND @EndYear;
	END
	ELSE IF @IdTechnicalDriver>0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, SUM([Normal]) AS [Normal], SUM([Pessimistic]) AS [Pessimistic], SUM([Optimistic]) AS [Optimistic]
		FROM tblTechnicalDriverData
		WHERE [IdTechnicalDriver]=@IdTechnicalDriver AND [IdModel]=@IdModel AND IdField IN (SELECT [IdField] FROM #FieldList)
		GROUP BY Year;
	END
	
	/** Build Volume Tables **/
	/** Report (Baseline)**/
	CREATE TABLE #VolumeReportBLRows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLRows ([ParentName], [Name], [YearProjection], [Amount])	
		SELECT	DISTINCT N'All Cost Accounts' + 
			CASE WHEN @IdTechnicalScenario=1 THEN 'Base' 
				WHEN @IdTechnicalScenario=2 THEN 'Optimistic'
	 			WHEN @IdTechnicalScenario=3 THEN 'Pessimistic'
			END AS ParentName, dbo.tblProjects.Name AS Name, 
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN ISNULL(dbo.tblTechnicalDriverData.Normal,0)
				WHEN @IdTechnicalScenario=2 THEN ISNULL(dbo.tblTechnicalDriverData.Optimistic,0)
				WHEN @IdTechnicalScenario=3 THEN ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0)
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON
			dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=1
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	ORDER BY dbo.tblTechnicalDriverData.Year
	
	CREATE TABLE #VolumeReportBL ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBL([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBLRows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBLList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBL WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #BLList to Fill Properly **/
	INSERT INTO #VolumeReportBL ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBLList.ParentName,#VolumeReportBLList.Name, #VolumeReportBLList.YearProjection,0 AS Amount
	FROM	#VolumeReportBLList LEFT OUTER JOIN
			#VolumeReportBL ON #VolumeReportBLList.ParentName=#VolumeReportBL.ParentName AND #VolumeReportBLList.Name=#VolumeReportBL.Name AND #VolumeReportBLList.YearProjection=#VolumeReportBL.YearProjection
	WHERE	#VolumeReportBL.YearProjection IS NULL;

	/** Report (Business Opportunities)**/
	CREATE TABLE #VolumeReportBORows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBORows ([ParentName], [Name], [YearProjection], [Amount])	
		SELECT	DISTINCT N'All Cost Accounts' + 
			CASE WHEN @IdTechnicalScenario=1 THEN 'Base' 
				WHEN @IdTechnicalScenario=2 THEN 'Optimistic'
	 			WHEN @IdTechnicalScenario=3 THEN 'Pessimistic'
			END AS ParentName, dbo.tblProjects.Name AS Name, 
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN ISNULL(dbo.tblTechnicalDriverData.Normal,0)
				WHEN @IdTechnicalScenario=2 THEN ISNULL(dbo.tblTechnicalDriverData.Optimistic,0)
				WHEN @IdTechnicalScenario=3 THEN ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0)
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON
			dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=2
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	ORDER BY dbo.tblTechnicalDriverData.Year

	CREATE TABLE #VolumeReportBO ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBO([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBORows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBOList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBOList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBO WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #BOList to Fill Properly **/
	INSERT INTO #VolumeReportBO ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBOList.ParentName,#VolumeReportBOList.Name, #VolumeReportBOList.YearProjection,0 AS Amount
	FROM	#VolumeReportBOList LEFT OUTER JOIN
			#VolumeReportBO ON #VolumeReportBOList.ParentName=#VolumeReportBO.ParentName AND #VolumeReportBOList.Name=#VolumeReportBO.Name AND #VolumeReportBOList.YearProjection=#VolumeReportBO.YearProjection
	WHERE	#VolumeReportBO.YearProjection IS NULL;

	/** Report (All Fields Grouped)**/
	CREATE TABLE #VolumeReportGroupedBL ([YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBL ([YearProjection], [Amount])
	SELECT YearProjection,SUM(amount) AS Amount
	FROM #VolumeReportBL
	GROUP BY YearProjection
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGroupedBO ([YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBO ([YearProjection], [Amount])
	SELECT YearProjection,SUM(amount) AS Amount
	FROM #VolumeReportBO
	GROUP BY YearProjection
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGrouped ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255),[YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGrouped ([ParentName], [Name], [YearProjection], [Amount])
	SELECT N'All Cost Accounts' AS ParentName, 
		   CASE WHEN @IdTechnicalScenario=1 THEN 'Base' 
				WHEN @IdTechnicalScenario=2 THEN 'Optimistic'
				WHEN @IdTechnicalScenario=3 THEN 'Pessimistic'
		   END AS Name,
		   #VolumeReportGroupedBL.YearProjection,
		   #VolumeReportGroupedBL.Amount + #VolumeReportGroupedBO.Amount AS AMOUNT
	FROM #VolumeReportGroupedBL INNER JOIN	#VolumeReportGroupedBO ON #VolumeReportGroupedBO.YearProjection = #VolumeReportGroupedBL.YearProjection		   

	DECLARE @TotalCapacityDriver INT = 0;

	/** Get grouped driver value for Base Year **/
	SELECT @TotalCapacityDriver = Amount FROM #VolumeReportGrouped WHERE YearProjection=@BaseYear

	/** Build Utilization Capacity Values **/
	CREATE TABLE #UtilizationCapacityValues ([CodeZiff] NVARCHAR(50), [IdField] INT, [UtilizationValue] FLOAT) ON [PRIMARY]
	INSERT INTO #UtilizationCapacityValues([CodeZiff], [IdField], [UtilizationValue])
	SELECT  A.[CodeZiff], tblFields.IdField, @TotalCapacityDriver/A.UtilizationValue*100
	  FROM tblUtilizationCapacity A INNER JOIN tblFields
			ON A.IdField = tblFields.IdField      
	WHERE  	A.IdModel = @IdModel
	AND		A.IdField = @IdField

	/** Build Source Data Table - Total **/
	CREATE TABLE #Opex (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #Opex ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS ParentName, 
		CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
		tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
		) AS CostProjection
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList)
	GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END, tblCalcProjectionTechnical.Year;
	
	/** Build Source Data Table - Chart **/
	CREATE TABLE #Chart (ParentName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #Chart ([ParentName],[YearProjection],[CostProjection])
	SELECT CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS ParentName, 
		tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
		) AS CostProjection
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, tblCalcProjectionTechnical.Year;
	
	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #ChartList (ParentName NVARCHAR(255), YearProjection INT) ON [PRIMARY]
	INSERT INTO #ChartList ([ParentName],[YearProjection])
	SELECT CNList.ParentName, CYList.Year FROM
		(SELECT [ParentName] FROM #Chart GROUP BY [ParentName])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList
	
	/** Add Any Missing Years to Make the Chart Fill Properly (eg. Fields with only Cyclical Drivers) **/
	INSERT INTO #Chart ([ParentName],[YearProjection],[CostProjection])
	SELECT #ChartList.ParentName, #ChartList.YearProjection, NULL AS CostProjection FROM
		#ChartList LEFT OUTER JOIN
		#Chart ON #ChartList.ParentName=#Chart.ParentName AND #ChartList.YearProjection=#Chart.YearProjection
	WHERE #Chart.YearProjection IS NULL;	
	
	IF @IdTechnicalDriver=0
	BEGIN
		/** Build Source Data Table - Baseline and Business Opportunities (No Techical KPI Required) **/
		CREATE TABLE #BL (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
		CREATE TABLE #BO (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
		IF @FieldCount = 1
		BEGIN
			/** Baseline **/
			INSERT INTO #BL ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=1 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, tblCalcProjectionTechnical.Year;
			
			/** Business Opportunities **/
			INSERT INTO #BO ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=2 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, tblCalcProjectionTechnical.Year;
		END
		ELSE IF @FieldCount <> 1
		BEGIN
			/** Baseline **/
			INSERT INTO #BL ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, Project AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=1 
			GROUP BY tblCalcProjectionTechnical.Project, tblCalcProjectionTechnical.Year;
			
			/** Business Opportunities **/
			INSERT INTO #BO ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, Project Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=2 
			GROUP BY tblCalcProjectionTechnical.Project, tblCalcProjectionTechnical.Year;
		END
		
		/* NEED to do calculations for SEMI-VARIABLE */
		
	END

	/** Add Unit Cost factor, IF NECESSARY **/
	IF @IdCostType = 2
	BEGIN		 
		IF @IdField <>0
		BEGIN		
			UPDATE	#BL
			SET		#BL.CostProjection = #BL.CostProjection/#VolumeReportBL.Amount
			FROM	#BL INNER JOIN #VolumeReportBL ON
					#BL.YearProjection = #VolumeReportBL.YearProjection 

			UPDATE	#BO
			SET		#BO.CostProjection = #BO.CostProjection/#VolumeReportBO.Amount
			FROM	#BO INNER JOIN #VolumeReportBO ON
					#BO.YearProjection = #VolumeReportBO.YearProjection 
		END
		ELSE
		BEGIN
			UPDATE	#BL
			SET		#BL.CostProjection = #BL.CostProjection/#VolumeReportBL.Amount
			FROM	#BL INNER JOIN #VolumeReportBL ON
					#BL.YearProjection = #VolumeReportBL.YearProjection AND #BL.Name=#VolumeReportBL.Name

			UPDATE	#BO
			SET		#BO.CostProjection = #BO.CostProjection/#VolumeReportBO.Amount
			FROM	#BO INNER JOIN #VolumeReportBO ON
					#BO.YearProjection = #VolumeReportBO.YearProjection AND #BO.Name=#VolumeReportBO.Name
		END
		UPDATE	#Opex
		SET		#Opex.CostProjection = #Opex.CostProjection/#VolumeReportGrouped.Amount
		FROM	#Opex INNER JOIN #VolumeReportGrouped ON
				#Opex.YearProjection = #VolumeReportGrouped.YearProjection
	END
		
	/** Build Output Table **/
	DECLARE @SQLOpex NVARCHAR(MAX);
	SET @SQLOpex = 'SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #Opex PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
	EXEC(@SQLOpex);
	DECLARE @SQLOpexChart NVARCHAR(MAX);
	SET @SQLOpexChart = 'SELECT [YearProjection] AS [Year],' + @AccountColumnList + ' FROM #Chart PIVOT (SUM(CostProjection) FOR ParentName IN (' + @AccountColumnList + ')) AS PivotTable ORDER BY [YearProjection]';
	EXEC(@SQLOpexChart);
	IF @IdTechnicalDriver=0
	BEGIN
		DECLARE @SQLOpexBL NVARCHAR(MAX);
		SET @SQLOpexBL = 'SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #BL PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
		EXEC(@SQLOpexBL);
		DECLARE @SQLOpexBO NVARCHAR(MAX);
		SET @SQLOpexBO = 'SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #BO PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
		EXEC(@SQLOpexBO);
	END

	DECLARE @SQLVolumeTotal NVARCHAR(MAX);
	SET @SQLVolumeTotal = 'SELECT [ParentName], [Name],' + @YearColumnList + ' FROM #VolumeReportGrouped PIVOT (SUM(Amount) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
	EXEC(@SQLVolumeTotal);

	DECLARE @SQLVolumeBL NVARCHAR(MAX);
	SET @SQLVolumeBL = 'SELECT [ParentName], [Name],' + @YearColumnList + ' FROM #VolumeReportBL PIVOT (SUM(Amount) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
	EXEC(@SQLVolumeBL);
	
	DECLARE @SQLVolumeBO NVARCHAR(MAX);
	SET @SQLVolumeBO = 'SELECT [ParentName], [Name],' + @YearColumnList + ' FROM #VolumeReportBO PIVOT (SUM(Amount) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName],[Name]';
	EXEC(@SQLVolumeBO);
	
	/** Output Stats Table for UI **/
	IF @IdTechnicalDriver=0
	BEGIN
		SELECT @FieldCount AS FieldCount;
	END
	
END
GO

USE [DBCPM]
GO

/****** Object:  StoredProcedure [dbo].[reppOpexProjectionDetailed]    Script Date: 08/12/2015 15:12:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-03-31
-- Description:	Module Report Opex Projection detailed Baseline and Business Opportunities
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 6/10/2015	RM		Added @Factor parameter
-- 8/5/2015		RM		Cleaned up code, removed what wasn't being used.
-- ===========================================================================================

ALTER PROCEDURE [dbo].[reppOpexProjectionDetailed](
@IdModel INT,
@IdAggregationLevel INT,
@IdField INT,
@IdStructure INT,
@From INT,
@To INT,
@IdTechnicalScenario INT,
@IdEconomicScenario INT,
@IdCurrency INT,
@IdTerm INT,
@IdTechnicalDriver INT,
@IdCostType INT,
@IdTypeOperation INT,
@IdLanguage INT,
@Factor INT
)
AS
BEGIN

	/** DROP Temporary Tables **/
	IF OBJECT_ID('tempdb..#FieldList') IS NOT NULL DROP TABLE #FieldList
	IF OBJECT_ID('tempdb..#AccountList') IS NOT NULL DROP TABLE #AccountList
	IF OBJECT_ID('tempdb..#TechDriver') IS NOT NULL DROP TABLE #TechDriver
	IF OBJECT_ID('tempdb..#Opex') IS NOT NULL DROP TABLE #Opex
	IF OBJECT_ID('tempdb..#BL') IS NOT NULL DROP TABLE #BL
	IF OBJECT_ID('tempdb..#BO') IS NOT NULL DROP TABLE #BO
	IF OBJECT_ID('tempdb..#BLList') IS NOT NULL DROP TABLE #BLList
	IF OBJECT_ID('tempdb..#BOList') IS NOT NULL DROP TABLE #BOList
	IF OBJECT_ID('tempdb..#ProjectionCriteriaList') IS NOT NULL DROP TABLE #ProjectionCriteriaList

	/** Structure Value - ZiffEnergy=1 Client=2 **/
	/** Term Value - Real=1 (Run Economic Model) Nominal=2 (No Economic Influence) **/
	DECLARE @StartYear INT = @From;
	DECLARE @EndYear INT = @To;

	DECLARE @TotalCapacityDriver INT = 0;

	/** Get Base Year **/
	DECLARE @BaseYear INT
	SELECT @BaseYear = [BaseYear] FROM [DBCPM].[dbo].[tblModels] WHERE IdModel=@IdModel
		
	/** Build Year Columns **/
	DECLARE @YearColumnList NVARCHAR(MAX)='';	
	WHILE (@From <= @To)
	BEGIN
		SET @YearColumnList =  @YearColumnList + '[' + CAST(@From AS VARCHAR(4)) + ']';
		SET @From = @From + 1;
		IF (@From <= @To)
		BEGIN
			SET @YearColumnList = @YearColumnList + ',';
		END
	END
	
	/** Get Currency Factor to use **/
	DECLARE @CurrencyFactor FLOAT;
	SELECT @CurrencyFactor=[Value] FROM tblModelsCurrencies WHERE [IdCurrency]=@IdCurrency AND [IdModel]=@IdModel;

	/** Build Field List **/
	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF (@IdAggregationLevel=0 AND @IdField=0 AND @IdTypeOperation=0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0 AND @IdField>0 AND @IdTypeOperation>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdField]=@IdField AND [IdModel]=@IdModel AND [IdTypeOperation]=@IdTypeOperation GROUP BY [IdField];
	END
	ELSE IF (@IdField>0 AND @IdTypeOperation>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdField]=@IdField AND [IdModel]=@IdModel AND [IdTypeOperation]=@IdTypeOperation GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0 AND @IdField>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0 AND @IdTypeOperation>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdModel]=@IdModel AND [IdTypeOperation]=@IdTypeOperation GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdField>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdTypeOperation>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdTypeOperation]=@IdTypeOperation AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	DECLARE @FieldCount INT;
	SELECT @FieldCount=COUNT([IdField]) FROM #FieldList

	/** Build Projection Criteria List **/
	CREATE TABLE #ProjectionCriteriaList (Code INT PRIMARY KEY NOT NULL, [Name] NVARCHAR(20), [SortOrder] INT) ON [PRIMARY]
	INSERT INTO #ProjectionCriteriaList (Code, Name, SortOrder)
	SELECT Code, Name, CASE WHEN Code=1 THEN 1 
			WHEN Code=3 THEN 2
			WHEN Code=4 THEN 3
			WHEN Code=2 THEN 4
			ELSE 5 END  
	FROM tblParameters 
	WHERE [Type]='ProjectionCriteria' and IdLanguage=@IdLanguage AND Code<90

	/** Build Criteria Columns **/
	DECLARE @ProjectionCriteriaList NVARCHAR(MAX)='';	
	DECLARE @ProjectionCriteriaList2 NVARCHAR(MAX)='SUM([Total]) AS Total,';	
	DECLARE @ProjectionCriteriaName NVARCHAR(255);
	DECLARE ProjectionCriteriaList CURSOR FOR
	SELECT Name FROM #ProjectionCriteriaList ORDER BY SortOrder;

	OPEN ProjectionCriteriaList;
	FETCH NEXT FROM ProjectionCriteriaList INTO @ProjectionCriteriaName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @ProjectionCriteriaList=''
		BEGIN
			SET @ProjectionCriteriaList =  @ProjectionCriteriaList + '[' + @ProjectionCriteriaName + ']';
			SET @ProjectionCriteriaList2 =  @ProjectionCriteriaList2 + 'SUM(ISNULL([' + @ProjectionCriteriaName + '],0)) AS ' + @ProjectionCriteriaName;
		END
		ELSE
		BEGIN
			SET @ProjectionCriteriaList =  @ProjectionCriteriaList + ',[' + @ProjectionCriteriaName + ']';
			SET @ProjectionCriteriaList2 =  @ProjectionCriteriaList2 + ',SUM(ISNULL([' + @ProjectionCriteriaName + '],0)) AS ' + @ProjectionCriteriaName;
		END
		FETCH NEXT FROM ProjectionCriteriaList INTO @ProjectionCriteriaName;
	END

	CLOSE ProjectionCriteriaList;
	DEALLOCATE ProjectionCriteriaList;

	/** Build Technical Driver Factors (used for KPI Report) **/
	CREATE TABLE #TechDriver ([Year] INT, [Normal] FLOAT, [Optimistic] FLOAT, [Pessimistic] FLOAT) ON [PRIMARY]
	IF @IdTechnicalDriver=0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, 1 AS [Normal], 1 AS [Optimistic], 1 AS [Pessimistic]
		FROM tblCalcModelYears
		WHERE [IdModel]=@IdModel AND [Year] BETWEEN @StartYear AND @EndYear;
	END
	ELSE IF @IdTechnicalDriver>0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, SUM([Normal]) AS [Normal], SUM([Pessimistic]) AS [Pessimistic], SUM([Optimistic]) AS [Optimistic]
		FROM tblTechnicalDriverData
		WHERE [IdTechnicalDriver]=@IdTechnicalDriver AND [IdModel]=@IdModel AND IdField IN (SELECT [IdField] FROM #FieldList)
		AND	dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
		GROUP BY Year;
	END

	/** Build Source Data Table - Baseline and Business Opportunities (No Techical KPI Required) **/
	CREATE TABLE #BL (FieldProject NVARCHAR(255), Field NVARCHAR(255), Project NVARCHAR(255), ProjectionCriteria NVARCHAR(255), YearProjection INT, Total FLOAT, CostProjection FLOAT, SortOrder INT) ON [PRIMARY]
	CREATE TABLE #BO (FieldProject NVARCHAR(255), Field NVARCHAR(255), Project NVARCHAR(255), ProjectionCriteria NVARCHAR(255), YearProjection INT, Total FLOAT, CostProjection FLOAT, SortOrder INT) ON [PRIMARY]

	/** BL **/
	INSERT INTO #BL ([FieldProject],[Field],[Project],[ProjectionCriteria],[YearProjection],[Total],[CostProjection],[SortOrder])
	SELECT CPT.Field + ' ('+ CPT.Project + ')' AS FieldProject, CPT.Field, CPT.Project, P.Name AS ProjectionCriteria, CPT.Year AS YearProjection, 0,
		ISNULL(SUM ( 
			CASE WHEN @IdTechnicalScenario=1 THEN CPT.TFNormal * CPT.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN CPT.TFOptimistic * CPT.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN CPT.TFPessimistic * CPT.BCRPessimistic 
			ELSE 0 END 
			* @CurrencyFactor
			/ @Factor 
		),0) AS CostProjection, P.SortOrder
	FROM tblCalcProjectionTechnical CPT  LEFT OUTER JOIN
		tblCalcProjectionEconomic ON CPT.Year = tblCalcProjectionEconomic.Year AND 
		CPT.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		CPT.IdModel = tblCalcProjectionEconomic.IdModel LEFT OUTER JOIN #ProjectionCriteriaList P ON
		CPT.ProjectionCriteria = P.Code
	WHERE CPT.IdModel=@IdModel
	  AND CPT.IdField IN (SELECT [IdField] FROM #FieldList) 
	  AND CPT.Operation = 1 
	GROUP BY CPT.Field + ' ('+ CPT.Project + ')',CPT.Field, CPT.Project, P.Name, CPT.Year, P.SortOrder, ProjectionCriteria, CPT.Amount
	
	/** BO **/
	INSERT INTO #BO ([FieldProject],[Field],[Project],[ProjectionCriteria],[YearProjection],[Total],[CostProjection],[SortOrder])
	SELECT CPT.Field + ' ('+ CPT.Project + ')' AS FieldProject, CPT.Field, CPT.Project, P.Name AS ProjectionCriteria, CPT.Year AS YearProjection, 0,
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN CPT.TFNormal * CPT.BCRNormal 
				WHEN @IdTechnicalScenario=2 THEN CPT.TFOptimistic * CPT.BCROptimistic 
				WHEN @IdTechnicalScenario=3 THEN CPT.TFPessimistic * CPT.BCRPessimistic 
				ELSE 0 
			END 
			* @CurrencyFactor
			/ @Factor 
		) AS CostProjection, P.SortOrder
	FROM tblCalcProjectionTechnical CPT LEFT OUTER JOIN
		tblCalcProjectionEconomic ON CPT.Year = tblCalcProjectionEconomic.Year AND 
		CPT.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		CPT.IdModel = tblCalcProjectionEconomic.IdModel JOIN #ProjectionCriteriaList P ON
		CPT.ProjectionCriteria = P.Code
	WHERE CPT.IdModel=@IdModel
	  AND CPT.IdField IN (SELECT [IdField] FROM #FieldList) 
	  AND CPT.Operation = 2
	  AND CPT.Year BETWEEN @StartYear AND @EndYear
	GROUP BY CPT.Field + ' ('+ CPT.Project + ')',CPT.Field, CPT.Project, P.Name, CPT.Year, P.SortOrder
	
	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #BLList (FieldProject NVARCHAR(255), Field NVARCHAR(255), Project NVARCHAR(255), ProjectionCriteria NVARCHAR(255), YearProjection INT, Total FLOAT, SortOrder INT) ON [PRIMARY]
	INSERT INTO #BLList ([FieldProject],[Field],[Project],[ProjectionCriteria],[YearProjection],[Total],[SortOrder])
	SELECT CNList.FieldProject, CNList.Field, CNList.Project, CPList.Name, CYList.Year, 0, CPList.SortOrder FROM
		(SELECT [FieldProject],[Field],[Project] FROM #BL GROUP BY [FieldProject],[Field],[Project])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList CROSS JOIN
		(SELECT [NAME], [SortOrder] FROM #ProjectionCriteriaList) AS CPList

	CREATE TABLE #BOList (FieldProject NVARCHAR(255), Field NVARCHAR(255), Project NVARCHAR(255), ProjectionCriteria NVARCHAR(255), YearProjection INT, Total FLOAT, SortOrder INT) ON [PRIMARY]
	INSERT INTO #BOList ([FieldProject],[Field],[Project],[ProjectionCriteria],[YearProjection],[Total],[SortOrder])
	SELECT CNList.FieldProject, CNList.Field, CNList.Project, CPList.Name, CYList.Year, 0, CPList.SortOrder FROM
		(SELECT [FieldProject],[Field],[Project] FROM #BO GROUP BY [FieldProject],[Field],[Project])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList CROSS JOIN
		(SELECT [NAME], [SortOrder] FROM #ProjectionCriteriaList) AS CPList

	/** Add Any Missing Years to Make the Chart Fill Properly (eg. Fields with only Cyclical Drivers) **/
	INSERT INTO #BL([FieldProject],[Field],[Project],[ProjectionCriteria],[YearProjection],[Total],[CostProjection],[SortOrder])
	SELECT #BLList.FieldProject, #BLList.Field, #BLList.Project, #BLList.ProjectionCriteria, #BLList.YearProjection, 0 AS Total, NULL AS CostProjection , #BLList.SortOrder
	FROM #BLList LEFT OUTER JOIN
		 #BL ON #BLList.FieldProject=#BL.FieldProject AND #BLList.Field=#BL.Field AND #BLList.Project=#BL.Project AND #BLList.ProjectionCriteria=#BL.ProjectionCriteria AND #BLList.YearProjection=#BL.YearProjection AND #BLList.SortOrder=#BL.SortOrder
	WHERE #BL.YearProjection IS NULL;	

	INSERT INTO #BO([FieldProject],[Field],[Project],[ProjectionCriteria],[YearProjection],[Total],[CostProjection],[SortOrder])
	SELECT #BOList.FieldProject, #BOList.Field, #BOList.Project, #BOList.ProjectionCriteria, #BOList.YearProjection, 0 AS Total, NULL AS CostProjection , #BOList.SortOrder
	FROM #BOList LEFT OUTER JOIN
		 #BO ON #BOList.FieldProject=#BO.FieldProject AND #BOList.Field=#BO.Field AND #BOList.Project=#BO.Project AND #BOList.ProjectionCriteria=#BO.ProjectionCriteria AND #BOList.YearProjection=#BO.YearProjection AND #BOList.SortOrder=#BO.SortOrder
	WHERE #BO.YearProjection IS NULL;	

	/** Build Output Table **/
	DECLARE @SQLOpexBL NVARCHAR(MAX);
	SET @SQLOpexBL = 'SELECT [FieldProject],[ProjectionCriteria],' + @YearColumnList + ' FROM #BL PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [FieldProject],[SortOrder]';
	EXEC(@SQLOpexBL);

	DECLARE @SQLOpexBO NVARCHAR(MAX);
	SET @SQLOpexBO = 'SELECT [FieldProject],[ProjectionCriteria],' + @YearColumnList + ' FROM #BO PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [FieldProject],[SortOrder]';
	EXEC(@SQLOpexBO);

	DECLARE @SQLOpexBLExport NVARCHAR(MAX);
	SET @SQLOpexBLExport = 'WITH Pivoted AS (SELECT [Field],[Project],[YearProjection],[Total],' + @ProjectionCriteriaList + ' FROM #BL PIVOT (SUM(CostProjection) FOR [ProjectionCriteria] IN (' + @ProjectionCriteriaList + ')) AS PivotTable ) SELECT [Field],[Project],[YearProjection],' + @ProjectionCriteriaList2 + ' FROM Pivoted GROUP BY [Field],[Project],[YearProjection]';
	EXEC(@SQLOpexBLExport);

	DECLARE @SQLOpexBOExport NVARCHAR(MAX);
	SET @SQLOpexBOExport = 'WITH Pivoted AS (SELECT [Field],[Project],[YearProjection],[Total],' + @ProjectionCriteriaList + ' FROM #BO PIVOT (SUM(CostProjection) FOR [ProjectionCriteria] IN (' + @ProjectionCriteriaList + ')) AS PivotTable ) SELECT [Field],[Project],[YearProjection],' + @ProjectionCriteriaList2 + ' FROM Pivoted GROUP BY [Field],[Project],[YearProjection]';
	EXEC(@SQLOpexBOExport);

END
GO


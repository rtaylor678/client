--USE [DBCPM]
--GO
/****** Object:  StoredProcedure [dbo].[usrpInsertModelBase]    Script Date: 12/09/2015 17:05:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gareth Slater
-- Create date: 2014-03-14
-- Description:	Create a copy of an existing model/case
-- =============================================
ALTER PROC [dbo].[usrpInsertModelBase](
@IdModel numeric output,
@IdModelBase int,
@Name varchar(100),
@BaseYear int,
@Projection int,
@Status int,
@Notes varchar(255),
@UserCreation int,
@DateCreation datetime,
@UserModification int,
@DateModification datetime,
@Level int)
AS
BEGIN

	SET NOCOUNT ON;  

	-- INSERT NEW MODEL
	INSERT INTO tblModels ([Name],[BaseYear],[Projection],[Status],[Notes],[UserCreation],[DateCreation],[UserModification],[DateModification],[Level])
	VALUES(@Name,@BaseYear,@Projection,@Status,@Notes,@UserCreation,@DateCreation,@UserModification,@DateModification,@Level);

	-- PRIMARY KEY
	SET @IdModel = SCOPE_IDENTITY();

	DECLARE @Code varchar(100)
	DECLARE @Parent int
	DECLARE @IdAggregationLevel int
	DECLARE @IdAggregationLevelAnt int

	BEGIN /* Parameters Module **/
	--------------------------------------------------------------------------------------
	-------------------------- PARAMETERS MODULE -----------------------------------------
	--------------------------------------------------------------------------------------

	--------------------------------------------------------------------------------------
	-- AGGREGATION LEVELS
	--------------------------------------------------------------------------------------
	DECLARE @tblAggregationLevels TABLE (IdAggregationLevel int, IdAggregationLevelAnt int)
	DECLARE CurAggregationLevels CURSOR FOR
		SELECT [IdAggregationLevel],[Code],[Name],[Parent]
		FROM [dbo].[tblAggregationLevels]
		WHERE IdModel = @IdModelBase AND Parent IS NULL
		UNION ALL
		SELECT [IdAggregationLevel],[Code],[Name],[Parent]
		FROM [dbo].[tblAggregationLevels]
		WHERE IdModel = @IdModelBase AND Parent IS NOT NULL
	OPEN CurAggregationLevels

	FETCH NEXT FROM CurAggregationLevels INTO @IdAggregationLevelAnt, @Code, @Name, @Parent
	WHILE @@Fetch_status = 0
	BEGIN
		IF (@Parent IS NULL)
		BEGIN
			INSERT INTO tblAggregationLevels ([IdModel],[Code],[Name],[Parent],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES (@IdModel,@Code,@Name,NULL,@UserCreation,@DateCreation,@UserModification,@DateModification)
			SET @IdAggregationLevel = SCOPE_IDENTITY()
			INSERT INTO @tblAggregationLevels VALUES (@IdAggregationLevel, @IdAggregationLevelAnt)
		END
		ELSE
		BEGIN
			INSERT INTO tblAggregationLevels ([IdModel],[Code],[Name],[Parent],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES (@IdModel,@Code,@Name,(SELECT IdAggregationLevel FROM tblAggregationLevels WHERE IdModel = @IdModel AND Name = (SELECT Name FROM tblAggregationLevels WHERE IdAggregationLevel = @Parent)),@UserCreation,@DateCreation,@UserModification,@DateModification)
			SET @IdAggregationLevel = SCOPE_IDENTITY()
			INSERT INTO @tblAggregationLevels VALUES (@IdAggregationLevel,@IdAggregationLevelAnt)
		END
		-----------
		FETCH NEXT FROM CurAggregationLevels INTO @IdAggregationLevelAnt, @Code, @Name, @Parent
	END
	CLOSE CurAggregationLevels
	DEALLOCATE CurAggregationLevels
	
	--------------------------------------------------------------------------------------
	-- TYPE OF OPERATION
	--------------------------------------------------------------------------------------
	DECLARE @IdTypeOperation int
	DECLARE CurModelsTypesOperation CURSOR FOR
		SELECT [IdTypeOperation]
		FROM [dbo].[tblModelsTypesOperation]
		WHERE IdModel = @IdModelBase
	OPEN CurModelsTypesOperation

	FETCH NEXT FROM CurModelsTypesOperation INTO @IdTypeOperation
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblModelsTypesOperation ([IdModel],[IdTypeOperation]) VALUES (@IdModel,@IdTypeOperation)
		-----------
		FETCH NEXT FROM CurModelsTypesOperation INTO @IdTypeOperation
	END
	CLOSE CurModelsTypesOperation
	DEALLOCATE CurModelsTypesOperation
	
	--------------------------------------------------------------------------------------
	-- FIELDS
	--------------------------------------------------------------------------------------
	DECLARE @IdField int
	DECLARE @IdFieldAnt int
	DECLARE @Starts int
	DECLARE @tblFields TABLE (IdField int, IdFieldAnt int)
	DECLARE CurFields CURSOR FOR
		SELECT IdField, Code, Name, IdAggregationLevel, IdTypeOperation, Starts 
		FROM [dbo].[tblFields]
		WHERE IdModel = @IdModelBase
	OPEN CurFields

	FETCH NEXT FROM CurFields INTO @IdFieldAnt, @Code, @Name, @IdAggregationLevel, @IdTypeOperation, @Starts
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblFields ([IdModel],[Code],[Name],[IdAggregationLevel],[IdTypeOperation],[UserCreation],[DateCreation],[UserModification],[DateModification],[Starts]) VALUES (@IdModel,@Code,@Name,(SELECT IdAggregationLevel FROM @tblAggregationLevels WHERE IdAggregationLevelAnt = @IdAggregationLevel),@IdTypeOperation,@UserCreation,@DateCreation,@UserModification,@DateModification,@Starts)
		SET @IdField = SCOPE_IDENTITY()
		INSERT INTO @tblFields VALUES (@IdField, @IdFieldAnt)
		-----------
		FETCH NEXT FROM CurFields INTO @IdFieldAnt, @Code, @Name, @IdAggregationLevel, @IdTypeOperation, @Starts
	END
	CLOSE CurFields
	DEALLOCATE CurFields
	
	--------------------------------------------------------------------------------------
	-- PROJECTS now CASES
	--------------------------------------------------------------------------------------
	DECLARE @IdProject int
	DECLARE @IdProjectAnt int
	DECLARE @Operation int 
	DECLARE @TypeAllocation int
	DECLARE @CaseSelection bit
	DECLARE @tblProjects TABLE (IdProject int, IdProjectAnt int)
	DECLARE CurProjects CURSOR FOR
		SELECT IdProject, Code, Name, IdField, Operation, Starts, TypeAllocation, CaseSelection 
		FROM [dbo].[tblProjects]
		WHERE IdModel = @IdModelBase
	OPEN CurProjects

	FETCH NEXT FROM CurProjects INTO @IdProjectAnt, @Code, @Name, @IdField, @Operation, @Starts, @TypeAllocation, @CaseSelection
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblProjects ([IdModel],[Code],[Name],[IdField],[Operation],[Starts],[TypeAllocation],[UserCreation],[DateCreation],[UserModification],[DateModification],[CaseSelection]) VALUES (@IdModel,@Code,@Name,(SELECT IdField FROM @tblFields WHERE IdFieldAnt = @IdField),@Operation,@Starts,@TypeAllocation,@UserCreation,@DateCreation,@UserModification,@DateModification,@CaseSelection)
		SET @IdProject = SCOPE_IDENTITY()
		INSERT INTO @tblProjects VALUES (@IdProject, @IdProjectAnt)
		-----------
		FETCH NEXT FROM CurProjects INTO @IdProjectAnt, @Code, @Name, @IdField, @Operation, @Starts, @TypeAllocation,@CaseSelection
	END
	CLOSE CurProjects
	DEALLOCATE CurProjects
	
	--------------------------------------------------------------------------------------
	-- ZIFF ACCOUNT (Cost Projection Categories)
	--------------------------------------------------------------------------------------
	DECLARE @IdZiffAccount int
	DECLARE @IdZiffAccountAnt int
	DECLARE @ThirdParty int
	DECLARE @SortOrder NVARCHAR(MAX)
	DECLARE @RelationLevel INT
	DECLARE @HasChild INT
	DECLARE @tblZiffAccount TABLE (IdZiffAccount int, IdZiffAccountAnt int)
	DECLARE CurZiffAccount CURSOR FOR
		SELECT IdZiffAccount, Code, Name, Parent, RelationLevel, ThirdParty, HasChild, SortOrder
		FROM [dbo].[tblZiffAccounts]
		WHERE IdModel = @IdModelBase
	OPEN CurZiffAccount

	FETCH NEXT FROM CurZiffAccount INTO @IdZiffAccountAnt, @Code, @Name, @Parent, @RelationLevel, @ThirdParty, @HasChild, @SortOrder
	WHILE @@Fetch_status = 0
	BEGIN
		IF (@Parent IS NULL)
		BEGIN
			INSERT INTO tblZiffAccounts ([Code],[Name],[Parent],[RelationLevel], [UserCreation],[DateCreation],[UserModification],[DateModification],[IdModel],[ThirdParty],[HasChild],[SortOrder]) VALUES (@Code,@Name,NULL,@RelationLevel,@UserCreation,@DateCreation,@UserModification,@DateModification,@IdModel,@ThirdParty,@HasChild,@SortOrder)
			SET @IdZiffAccount = SCOPE_IDENTITY()
			INSERT INTO @tblZiffAccount VALUES (@IdZiffAccount, @IdZiffAccountAnt)
		END
		ELSE
		BEGIN
			INSERT INTO tblZiffAccounts ([Code],[Name],[Parent],[RootParent],[RelationLevel],[UserCreation],[DateCreation],[UserModification],[DateModification],[IdModel],[ThirdParty],[HasChild],[SortOrder]) VALUES (@Code,@Name,(SELECT IdZiffAccount FROM tblZiffAccounts WHERE IdModel = @IdModel AND Name = (SELECT Name FROM tblZiffAccounts WHERE IdZiffAccount = @Parent)),(SELECT IdZiffAccount FROM tblZiffAccounts WHERE IdModel = @IdModel AND Name = (SELECT Name FROM tblZiffAccounts WHERE IdZiffAccount = @Parent)),@RelationLevel,@UserCreation,@DateCreation,@UserModification,@DateModification,@IdModel,@ThirdParty,@HasChild,@SortOrder)
			SET @IdZiffAccount = SCOPE_IDENTITY()
			INSERT INTO @tblZiffAccount VALUES (@IdZiffAccount, @IdZiffAccountAnt)
		END
		-----------
		FETCH NEXT FROM CurZiffAccount INTO @IdZiffAccountAnt, @Code, @Name, @Parent, @RelationLevel, @ThirdParty, @HasChild, @SortOrder
	END
	CLOSE CurZiffAccount
	DEALLOCATE CurZiffAccount
	
	--------------------------------------------------------------------------------------
	-- CURRENCIES
	--------------------------------------------------------------------------------------
	INSERT INTO dbo.tblModelsCurrencies ([IdCurrency],[IdModel],[Value],[BaseCurrency],[Output],[UserCreation],[DateCreation],[UserModification],[DateModification])
	SELECT IdCurrency, @IdModel AS IdModel, Value, BaseCurrency, [Output], @UserCreation AS UserCreation, @DateCreation AS DateCreation, @UserModification AS UserModification, @DateModification AS DateModification
	FROM tblModelsCurrencies
	WHERE IdModel = @IdModelBase

	END  /* Parameters Module **/
	
	BEGIN /** Cost Benchmarking Module **/
	--------------------------------------------------------------------------------------
	---------------- ALLOCATION MODULE (Cost Benchmarking) -------------------------------
	--------------------------------------------------------------------------------------

	--------------------------------------------------------------------------------------
	-- INTERNAL COST REFERENCES
	--------------------------------------------------------------------------------------

	--------------------------------------------------------------------------------------
	-- ACTIVITIES
	--------------------------------------------------------------------------------------
	DECLARE @IdActivity int
	DECLARE @IdActivityAnt int
	DECLARE @tblActivities TABLE (IdActivity int, IdActivityAnt int)
	DECLARE CurActivities CURSOR FOR
		SELECT IdActivity, Code, Name, IdTypeOperation
		FROM [dbo].[tblActivities]
		WHERE IdModel = @IdModelBase
	OPEN CurActivities

	FETCH NEXT FROM CurActivities INTO @IdActivityAnt, @Code, @Name, @IdTypeOperation
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblActivities ([IdModel],[Code],[Name],[IdTypeOperation],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES (@IdModel,@Code,@Name,@IdTypeOperation,@UserCreation,@DateCreation,@UserModification,@DateModification)
		SET @IdActivity = SCOPE_IDENTITY()
		INSERT INTO @tblActivities VALUES (@IdActivity, @IdActivityAnt)
		-----------
		FETCH NEXT FROM CurActivities INTO @IdActivityAnt, @Code, @Name, @IdTypeOperation
	END
	CLOSE CurActivities
	DEALLOCATE CurActivities
	
	--------------------------------------------------------------------------------------
	-- RESOURCES
	--------------------------------------------------------------------------------------
	DECLARE @IdResource int
	DECLARE @IdResourceAnt int
	DECLARE @tblResources TABLE (IdResources int, IdResourcesAnt int)
	DECLARE CurResources CURSOR FOR
		SELECT IdResources, Code, Name, IdActivity
		FROM [dbo].[tblResources]
		WHERE IdModel = @IdModelBase
	OPEN CurResources

	FETCH NEXT FROM CurResources INTO @IdResourceAnt, @Code, @Name, @IdActivity
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblResources ([IdModel],[Code],[Name],[Parent],[TypeAllocation],[IdActivity],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES (@IdModel,@Code,@Name,NULL,NULL,(SELECT IdActivity FROM @tblActivities WHERE IdActivityAnt = @IdActivity),@UserCreation,@DateCreation,@UserModification,@DateModification)
		SET @IdResource = SCOPE_IDENTITY()
		INSERT INTO @tblResources VALUES (@IdResource, @IdResourceAnt)
		-----------
		FETCH NEXT FROM CurResources INTO @IdResourceAnt, @Code, @Name, @IdActivity
	END
	CLOSE CurResources
	DEALLOCATE CurResources
	
	--------------------------------------------------------------------------------------
	-- CLIENT COST CENTER (Cost Objects)
	--------------------------------------------------------------------------------------
	DECLARE @IdClientCostCenter int
	DECLARE @IdClientCostCenterAnt int
	DECLARE @IdTypeAllocation int
	DECLARE @tblClientCostCenter TABLE (IdClientCostCenter int, IdClientCostCenterAnt int)
	DECLARE CurClientCostCenter CURSOR FOR
		SELECT IdClientCostCenter, Code, Name, IdActivity, TypeAllocation, Notes
		FROM [dbo].[tblClientCostCenters]
		WHERE IdModel = @IdModelBase
	OPEN CurClientCostCenter

	FETCH NEXT FROM CurClientCostCenter INTO @IdClientCostCenterAnt, @Code, @Name, @IdActivity, @IdTypeAllocation, @Notes
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblClientCostCenters ([IdModel],[Code],[Name],[IdActivity],[TypeAllocation],[Notes],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES (@IdModel,@Code,@Name,(SELECT IdActivity FROM @tblActivities WHERE IdActivityAnt = @IdActivity),@IdTypeAllocation,@Notes,@UserCreation,@DateCreation,@UserModification,@DateModification)
		SET @IdClientCostCenter = SCOPE_IDENTITY()
		INSERT INTO @tblClientCostCenter VALUES (@IdClientCostCenter, @IdClientCostCenterAnt)
		-----------
		FETCH NEXT FROM CurClientCostCenter INTO @IdClientCostCenterAnt, @Code, @Name, @IdActivity, @IdTypeAllocation, @Notes
	END
	CLOSE CurClientCostCenter
	DEALLOCATE CurClientCostCenter
	
	--------------------------------------------------------------------------------------
	-- CLIENT ACCOUNT (Chart of Accounts)
	--------------------------------------------------------------------------------------
	DECLARE @IdClientAccount int
	DECLARE @IdClientAccountAnt int
	DECLARE @Account varchar(200)
	DECLARE @tblClientAccount TABLE (IdClientAccount int, IdClientAccountAnt int)
	DECLARE CurClientAccount CURSOR FOR
		SELECT IdClientAccount, Account, Name, IdResource
		FROM [dbo].[tblClientAccounts]
		WHERE IdModel = @IdModelBase
	OPEN CurClientAccount

	FETCH NEXT FROM CurClientAccount INTO @IdClientAccountAnt, @Account, @Name, @IdResource
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblClientAccounts ([IdModel],[Account],[Name],[IdResource],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES (@IdModel,@Account,@Name,(SELECT IdResources FROM @tblResources WHERE IdResourcesAnt = @IdResource),@UserCreation,@DateCreation,@UserModification,@DateModification)
		SET @IdClientAccount = SCOPE_IDENTITY()
		INSERT INTO @tblClientAccount VALUES (@IdClientAccount, @IdClientAccountAnt)
		-----------
		FETCH NEXT FROM CurClientAccount INTO @IdClientAccountAnt, @Account, @Name, @IdResource
	END
	CLOSE CurClientAccount
	DEALLOCATE CurClientAccount

	--------------------------------------------------------------------------------------
	-- CLIENT COST DATA (Base Cost Data)
	--------------------------------------------------------------------------------------
	DECLARE @IdClientCostData int
	DECLARE @IdClientCostDataAnt int
	DECLARE @Amount float
	DECLARE @tblClientCostData TABLE (IdClientCostData int, IdClientCostDataAnt int)
	DECLARE CurClientCostData CURSOR FOR
		SELECT IdClientCostData, IdClientCostCenter, IdClientAccount, Amount
		FROM [dbo].[tblClientCostData]
		WHERE IdModel = @IdModelBase
	OPEN CurClientCostData

	FETCH NEXT FROM CurClientCostData INTO @IdClientCostDataAnt, @IdClientCostCenter, @IdClientAccount, @Amount
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblClientCostData ([IdModel],[IdClientCostCenter],[IdClientAccount],[Amount],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES (@IdModel,(SELECT IdClientCostCenter FROM @tblClientCostCenter WHERE IdClientCostCenterAnt = @IdClientCostCenter ),(SELECT IdClientAccount FROM @tblClientAccount WHERE IdClientAccountAnt = @IdClientAccount ),@Amount,@UserCreation,@DateCreation,@UserModification,@DateModification)
		SET @IdClientCostData = SCOPE_IDENTITY()
		INSERT INTO @tblClientCostData VALUES (@IdClientCostData, @IdClientCostDataAnt)
		-----------
		FETCH NEXT FROM CurClientCostData INTO @IdClientCostDataAnt, @IdClientCostCenter, @IdClientAccount, @Amount
	END
	CLOSE CurClientCostData
	DEALLOCATE CurClientCostData

	--------------------------------------------------------------------------------------
	-- DIRECT COST ALLOCATION
	--------------------------------------------------------------------------------------
	DECLARE @IdDirectCostAllocation int
	DECLARE @IdDirectCostAllocationAnt int
	DECLARE @Cost float
	DECLARE @tblDirectCostAllocation TABLE (IdDirectCostAllocation int, IdDirectCostAllocationAnt int)
	DECLARE CurDirectCostAllocation CURSOR FOR
		SELECT IdDirectCostAllocation, IdClientCostCenter, Cost, IdField
		FROM [dbo].[tblDirectCostAllocation]
		WHERE IdModel = @IdModelBase
	OPEN CurDirectCostAllocation

	FETCH NEXT FROM CurDirectCostAllocation INTO @IdDirectCostAllocationAnt, @IdClientCostCenter, @Cost, @IdField
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblDirectCostAllocation ([IdModel],[IdClientCostCenter],[Cost],[IdField],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES (@IdModel, (SELECT IdClientCostCenter FROM @tblClientCostCenter WHERE IdClientCostCenterAnt = @IdClientCostCenter ), @Cost, (SELECT IdField FROM @tblFields WHERE IdFieldAnt = @IdField ), @UserCreation, @DateCreation, @UserModification, @DateModification)
		SET @IdDirectCostAllocation = SCOPE_IDENTITY()
		INSERT INTO @tblDirectCostAllocation VALUES (@IdDirectCostAllocation, @IdDirectCostAllocationAnt)
		-----------
		FETCH NEXT FROM CurDirectCostAllocation INTO @IdDirectCostAllocationAnt, @IdClientCostCenter, @Cost, @IdField
	END
	CLOSE CurDirectCostAllocation
	DEALLOCATE CurDirectCostAllocation

	--------------------------------------------------------------------------------------
	-- SHARED COST ALLOCATION
	--------------------------------------------------------------------------------------
	DECLARE @IdSharedCostAllocation int
	DECLARE @IdSharedCostAllocationAnt int
	DECLARE @tblSharedCostAllocation TABLE (IdShareCostAllocation int, IdShareCostAllocationAnt int)
	DECLARE CurShareCostAllocation CURSOR FOR
		SELECT IdSharedCostAllocation, IdClientCostCenter
		FROM [dbo].[tblSharedCostAllocation]
		WHERE IdModel = @IdModelBase
	OPEN CurShareCostAllocation

	FETCH NEXT FROM CurShareCostAllocation INTO @IdSharedCostAllocationAnt, @IdClientCostCenter
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblSharedCostAllocation ([IdModel],[IdClientCostCenter],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES (@IdModel, (SELECT IdClientCostCenter FROM @tblClientCostCenter WHERE IdClientCostCenterAnt = @IdClientCostCenter ), @UserCreation, @DateCreation, @UserModification, @DateModification)
		SET @IdSharedCostAllocation = SCOPE_IDENTITY()
		INSERT INTO @tblSharedCostAllocation VALUES (@IdSharedCostAllocation, @IdSharedCostAllocationAnt)
		
		-- SHARE COST FIELDS
		DECLARE @IdSharedCostField int
		DECLARE @IdSharedCostFieldAnt int

		DECLARE @tblSharedCostField TABLE (IdShareCostField int, IdShareCostFieldAnt int)
		DECLARE CurShareCostField CURSOR FOR
			SELECT IdSharedCostField, IdField
			FROM [dbo].[tblSharedCostField]
			WHERE IdSharedCostAllocation = @IdSharedCostAllocationAnt
		OPEN CurShareCostField

		FETCH NEXT FROM CurShareCostField INTO @IdSharedCostFieldAnt, @IdField
		WHILE @@Fetch_status = 0
		BEGIN
			INSERT INTO tblSharedCostField ([IdSharedCostAllocation],[IdField]) VALUES (@IdSharedCostAllocation, (SELECT IdField FROM @tblFields WHERE IdFieldAnt = @IdField ))
			SET @IdSharedCostField = SCOPE_IDENTITY()
			INSERT INTO @tblSharedCostField VALUES (@IdSharedCostField, @IdSharedCostFieldAnt)
			-----------
			FETCH NEXT FROM CurShareCostField INTO @IdSharedCostFieldAnt, @IdField
		END
		CLOSE CurShareCostField
		DEALLOCATE CurShareCostField
		-----------
		FETCH NEXT FROM CurShareCostAllocation INTO @IdSharedCostAllocationAnt, @IdClientCostCenter
	END
	CLOSE CurShareCostAllocation
	DEALLOCATE CurShareCostAllocation

	--------------------------------------------------------------------------------------
	-- HIERARCHY COST ALLOCATION
	--------------------------------------------------------------------------------------
	DECLARE @IdHierarchyCostAllocation int
	DECLARE @IdHierarchyCostAllocationAnt int
	DECLARE @tblHierarchyCostAllocation TABLE (IdHierarchyCostAllocation int, IdHierarchyCostAllocationAnt int)
	DECLARE CurHierarchyCostAllocation CURSOR FOR
		SELECT IdHierarchyCostAllocation, IdClientCostCenter, IdAggregationLevel
		FROM [dbo].[tblHierarchyCostAllocation]
		WHERE IdModel = @IdModelBase
	OPEN CurHierarchyCostAllocation

	FETCH NEXT FROM CurHierarchyCostAllocation INTO @IdHierarchyCostAllocationAnt, @IdClientCostCenter, @IdAggregationLevel
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblHierarchyCostAllocation ([IdModel],[IdClientCostCenter],[IdAggregationLevel],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES (@IdModel, (SELECT IdClientCostCenter FROM @tblClientCostCenter WHERE IdClientCostCenterAnt = @IdClientCostCenter ), (SELECT IdAggregationLevel FROM @tblAggregationLevels WHERE IdAggregationLevelAnt = @IdAggregationLevel ), @UserCreation, @DateCreation, @UserModification, @DateModification)
		SET @IdHierarchyCostAllocation = SCOPE_IDENTITY()
		INSERT INTO @tblHierarchyCostAllocation VALUES (@IdHierarchyCostAllocation, @IdHierarchyCostAllocationAnt)
		-----------
		FETCH NEXT FROM CurHierarchyCostAllocation INTO @IdHierarchyCostAllocationAnt, @IdClientCostCenter, @IdAggregationLevel
	END
	CLOSE CurHierarchyCostAllocation
	DEALLOCATE CurHierarchyCostAllocation

	--------------------------------------------------------------------------------------	
	-- ALLOCATION LIST DRIVERS 
	--------------------------------------------------------------------------------------
	DECLARE @IdAllocationListDrivers int
	DECLARE @IdAllocationListDriversAnt int
	DECLARE @IdUnit int
	DECLARE @UnitCostDenominator bit
	DECLARE @tblAllocationListDrivers TABLE (IdAllocationListDrivers int, IdAllocationListDriversAnt int)
	DECLARE CurAllocationListDrivers CURSOR FOR
		SELECT IdAllocationListDriver, Name, IdUnit, UnitCostDenominator
		FROM [dbo].[tblAllocationListDrivers]
		WHERE IdModel = @IdModelBase
	OPEN CurAllocationListDrivers

	FETCH NEXT FROM CurAllocationListDrivers INTO @IdAllocationListDriversAnt, @Name, @IdUnit, @UnitCostDenominator
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblAllocationListDrivers ([IdModel],[Name],[IdUnit],[UserCreation],[DateCreation],[UserModification],[DateModification],[UnitCostDenominator] ) VALUES (@IdModel, @Name, @IdUnit, @UserCreation, @DateCreation, @UserModification, @DateModification, @UnitCostDenominator)
		SET @IdAllocationListDrivers = SCOPE_IDENTITY()
		INSERT INTO @tblAllocationListDrivers VALUES (@IdAllocationListDrivers, @IdAllocationListDriversAnt)
		-----------
		FETCH NEXT FROM CurAllocationListDrivers INTO @IdAllocationListDriversAnt, @Name, @IdUnit, @UnitCostDenominator
	END
	CLOSE CurAllocationListDrivers
	DEALLOCATE CurAllocationListDrivers

	------------------------------------------------------------------------------------
	-- ALLOCATION DRIVER BY COST CENTER
	--------------------------------------------------------------------------------------
	DECLARE @IdAllocationDriverData int
	DECLARE @IdAllocationDriverDataAnt int
	DECLARE @tblAllocationDriverData TABLE (IdAllocationDriverData int, IdAllocationDriverDataAnt int)
	DECLARE CurAllocationDriverData CURSOR FOR
		SELECT A.IdAllocationDriverData, A.IdAllocationListDriver, IdClientCostCenter
		FROM [dbo].[tblAllocationDriverByCostCenter] A
		INNER JOIN [dbo].[tblAllocationListDrivers] D ON D.IdAllocationListDriver = A.IdAllocationListDriver
		WHERE D.IdModel = @IdModelBase
	OPEN CurAllocationDriverData

	FETCH NEXT FROM CurAllocationDriverData INTO @IdAllocationDriverDataAnt, @IdAllocationListDrivers, @IdClientCostCenter
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblAllocationDriverByCostCenter ([IdAllocationListDriver],[IdClientCostCenter],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES ((SELECT IdAllocationListDrivers FROM @tblAllocationListDrivers WHERE IdAllocationListDriversAnt = @IdAllocationListDrivers ) , (SELECT IdClientCostCenter FROM @tblClientCostCenter WHERE IdClientCostCenterAnt = @IdClientCostCenter ), @UserCreation, @DateCreation, @UserModification, @DateModification)
		SET @IdAllocationDriverData = SCOPE_IDENTITY()
		INSERT INTO @tblAllocationDriverData VALUES (@IdAllocationDriverData, @IdAllocationDriverDataAnt)
		-----------
		FETCH NEXT FROM CurAllocationDriverData INTO @IdAllocationDriverDataAnt, @IdAllocationListDrivers, @IdClientCostCenter
	END
	CLOSE CurAllocationDriverData
	DEALLOCATE CurAllocationDriverData

	--------------------------------------------------------------------------------------
	-- ALLOCATION DRIVER BY FIELD
	--------------------------------------------------------------------------------------
	DECLARE @IdAllocationDriverByField int
	DECLARE @IdAllocationDriverByFieldAnt int
	DECLARE @tblAllocationDriverByField TABLE (IdAllocationDriverByField int, IdAllocationDriverByFieldAnt int)
	DECLARE CurAllocationDriverByField CURSOR FOR
		SELECT A.IdAllocationDriversByField, A.IdAllocationListDriver, A.IdField, A.Amount
		FROM [dbo].[tblAllocationDriversByField] A
		INNER JOIN [dbo].[tblAllocationListDrivers] D ON D.IdAllocationListDriver = A.IdAllocationListDriver
		WHERE D.IdModel = @IdModelBase
	OPEN CurAllocationDriverByField

	FETCH NEXT FROM CurAllocationDriverByField INTO @IdAllocationDriverByFieldAnt, @IdAllocationListDrivers, @IdField, @Amount
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblAllocationDriversByField ([IdAllocationListDriver],[IdField],[Amount],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES ((SELECT IdAllocationListDrivers FROM @tblAllocationListDrivers WHERE IdAllocationListDriversAnt = @IdAllocationListDrivers ) , (SELECT IdField FROM @tblFields WHERE IdFieldAnt = @IdField ), @Amount, @UserCreation, @DateCreation, @UserModification, @DateModification)
		SET @IdAllocationDriverByField = SCOPE_IDENTITY()
		INSERT INTO @tblAllocationDriverByField VALUES (@IdAllocationDriverByField, @IdAllocationDriverByFieldAnt)
		-----------
		FETCH NEXT FROM CurAllocationDriverByField INTO @IdAllocationDriverByFieldAnt, @IdAllocationListDrivers, @IdField, @Amount
	END
	CLOSE CurAllocationDriverByField
	DEALLOCATE CurAllocationDriverByField

	--------------------------------------------------------------------------------------
	-- MAPPING ZIFF VS CLIENT
	--------------------------------------------------------------------------------------
	DECLARE @tbltblZiffClientMap TABLE (IdZiffAccount int, IdZiffAccountAnt int)
	DECLARE CurtblZiffClientMap CURSOR FOR
		SELECT A.IdZiffAccount, A.IdClientAccount, A.IdClientCostCenter
		FROM [dbo].[tblZiffClientMap] A
		INNER JOIN [dbo].[tblClientAccounts] C ON C.IdClientAccount = A.IdClientAccount
		WHERE C.IdModel = @IdModelBase
	OPEN CurtblZiffClientMap

	FETCH NEXT FROM CurtblZiffClientMap INTO @IdZiffAccount, @IdClientAccount, @IdClientCostCenter
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblZiffClientMap ([IdZiffAccount],[IdClientAccount],[IdClientCostCenter]) VALUES ((SELECT IdZiffAccount FROM @tblZiffAccount WHERE IdZiffAccountAnt=@IdZiffAccount),(SELECT IdClientAccount FROM @tblClientAccount WHERE IdClientAccountAnt = @IdClientAccount ),(SELECT IdClientCostCenter FROM @tblClientCostCenter WHERE IdClientCostCenterAnt = @IdClientCostCenter ))
		SET @IdZiffAccount = SCOPE_IDENTITY()
		INSERT INTO @tbltblZiffClientMap VALUES (@IdZiffAccount, @IdZiffAccountAnt)
		-----------
		FETCH NEXT FROM CurtblZiffClientMap INTO @IdZiffAccount, @IdClientAccount, @IdClientCostCenter
	END
	CLOSE CurtblZiffClientMap
	DEALLOCATE CurtblZiffClientMap

	--------------------------------------------------------------------------------------
	-- EXTERNAL COST REFERENCES
	--------------------------------------------------------------------------------------

	--------------------------------------------------------------------------------------
	-- PEER GROUP
	--------------------------------------------------------------------------------------
	DECLARE @IdPeerGroup int
	DECLARE @IdPeerGroupAnt int 
	DECLARE @tblPeerGroup TABLE (IdPeerGroup int, IdPeerGroupAnt int)
	DECLARE @PeerGroupCode VARCHAR(20)
	DECLARE @PeerGroupDescription VARCHAR(100)
	DECLARE @FullBenchmark BIT
	DECLARE @Comments VARCHAR(250)
	DECLARE GenericCursor CURSOR FOR
		SELECT A.[IdPeerGroup],A.[PeerGroupCode],A.[PeerGroupDescription],A.[FullBenchmark],A.[Comments]
		FROM [dbo].[tblPeerGroup] A
		WHERE A.IdModel = @IdModelBase
	OPEN GenericCursor

	FETCH NEXT FROM GenericCursor INTO @IdPeerGroupAnt, @PeerGroupCode, @PeerGroupDescription, @FullBenchmark, @Comments
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblPeerGroup([IdModel],[PeerGroupCode],[PeerGroupDescription],[FullBenchmark],[Comments]) VALUES (@IdModel,@PeerGroupCode,@PeerGroupDescription,@FullBenchmark,@Comments)
		SET @IdPeerGroup = SCOPE_IDENTITY()
		INSERT INTO @tblPeerGroup VALUES(@IdPeerGroup,@IdPeerGroupAnt)
		-----------
		FETCH NEXT FROM GenericCursor INTO @IdPeerGroupAnt, @PeerGroupCode, @PeerGroupDescription, @FullBenchmark, @Comments
	END
	CLOSE GenericCursor
	DEALLOCATE GenericCursor

	--------------------------------------------------------------------------------------
	-- PEER GROUP FIELD
	--------------------------------------------------------------------------------------
	DECLARE @IdPeerGroupField int
	DECLARE @IdPeerGroupFieldAnt int 
	DECLARE @tblPeerGroupField TABLE (IdPeerGroupField int, IdPeerGroupFieldAnt int)
	DECLARE GenericCursor CURSOR FOR
		SELECT A.[IdPeerGroupField],A.[IdPeerGroup],A.[IdField]
		FROM [dbo].[tblPeerGroupField] A
		WHERE A.IdPeerGroup IN (SELECT [IdPeerGroup] FROM [dbo].[tblPeerGroup] WHERE IdModel = @IdModelBase)
	OPEN GenericCursor

	FETCH NEXT FROM GenericCursor INTO @IdPeerGroupFieldAnt, @IdPeerGroupAnt, @IdFieldAnt
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblPeerGroupField([IdPeerGroup],[IdField]) VALUES ((SELECT IdPeerGroup FROM @tblPeerGroup WHERE IdPeerGroupAnt = @IdPeerGroupAnt),(SELECT IdField FROM @tblFields WHERE IdFieldAnt = @IdFieldAnt ))
		SET @IdPeerGroupField = SCOPE_IDENTITY()
		INSERT INTO @tblPeerGroupField VALUES(@IdPeerGroupField,@IdPeerGroupFieldAnt)
		-----------
		FETCH NEXT FROM GenericCursor INTO @IdPeerGroupFieldAnt, @IdPeerGroupAnt, @IdFieldAnt
	END
	CLOSE GenericCursor
	DEALLOCATE GenericCursor

	--------------------------------------------------------------------------------------
	-- PEER CRITERIA
	--------------------------------------------------------------------------------------
	DECLARE @IdPeerCriteria int
	DECLARE @IdPeerCriteriaAnt int
	DECLARE @IdUnitPeerCriteria int
	DECLARE @TotalUnitCostDenominator int
	DECLARE @tblPeerCriteria TABLE (IdPeerCriteria int, IdPeerCriteriaAnt int)
	DECLARE CurPeerCriteriaCost CURSOR FOR
		SELECT IdPeerCriteria, Description, IdUnitPeerCriteria, TotalUnitCostDenominator
		FROM   tblPeerCriteria
		WHERE IdModel = @IdModelBase
	OPEN CurPeerCriteriaCost

	FETCH NEXT FROM CurPeerCriteriaCost INTO @IdPeerCriteriaAnt, @Name, @IdUnitPeerCriteria, @TotalUnitCostDenominator
	WHILE @@Fetch_status = 0
	BEGIN		
		INSERT INTO tblPeerCriteria ([Description],[IdUnitPeerCriteria],[IdModel],[TotalUnitCostDenominator]) VALUES (@Name, @IdUnitPeerCriteria, @IdModel, @TotalUnitCostDenominator)
		SET @IdPeerCriteria = SCOPE_IDENTITY()
		INSERT INTO @tblPeerCriteria VALUES (@IdPeerCriteria, @IdPeerCriteriaAnt)
		-----------
		FETCH NEXT FROM CurPeerCriteriaCost INTO @IdPeerCriteriaAnt, @Name, @IdUnitPeerCriteria, @TotalUnitCostDenominator
	END
	CLOSE CurPeerCriteriaCost
	DEALLOCATE CurPeerCriteriaCost

	--------------------------------------------------------------------------------------
	-- PEER CRITERIA DATA
	--------------------------------------------------------------------------------------
	DECLARE @Quantity float
	DECLARE CurPeerCriteriaCost CURSOR FOR
		SELECT P.IdPeerCriteria, P.IdPeerGroup, P.Quantity
		FROM [tblPeerCriteriaCost] P
		WHERE P.IdPeerGroup IN (SELECT [IdPeerGroup] FROM [dbo].[tblPeerGroup] WHERE IdModel = @IdModelBase)
	OPEN CurPeerCriteriaCost

	FETCH NEXT FROM CurPeerCriteriaCost INTO @IdPeerCriteria, @IdPeerGroup, @Quantity
	WHILE @@Fetch_status = 0
	BEGIN		
		INSERT INTO tblPeerCriteriaCost ([IdPeerCriteria],[IdPeerGroup],[Quantity]) VALUES ((SELECT IdPeerCriteria FROM @tblPeerCriteria WHERE IdPeerCriteriaAnt = @IdPeerCriteria ),(SELECT IdPeerGroup FROM @tblPeerGroup WHERE IdPeerGroupAnt = @IdPeerGroup),@Quantity)
		-----------
		FETCH NEXT FROM CurPeerCriteriaCost INTO @IdPeerCriteria, @IdPeerGroup, @Quantity
	END
	CLOSE CurPeerCriteriaCost
	DEALLOCATE CurPeerCriteriaCost

	--------------------------------------------------------------------------------------
	-- ZIFF BASE COST
	--------------------------------------------------------------------------------------
	DECLARE @IdZiffBaseCost int
	DECLARE @IdZiffBaseCostAnt int
	DECLARE @UnitCost float
	DECLARE @TotalCost float
	DECLARE @tblZiffBaseCost TABLE (IdZiffAccount int, IdZiffAccountAnt int)
	DECLARE CurZiffBaseCost CURSOR FOR
		SELECT Z.IdZiffBaseCost, Z.IdField, Z.IdZiffAccount, Z.IdPeerCriteria, Z.UnitCost, Z.Quantity, Z.TotalCost, Z.IdPeerGroup
		FROM tblZiffBaseCost Z
			INNER JOIN tblFields F ON F.IdField = Z.IdField
		WHERE F.IdModel = @IdModelBase
	OPEN CurZiffBaseCost

	FETCH NEXT FROM CurZiffBaseCost INTO @IdZiffBaseCostAnt, @IdField, @IdZiffAccount, @IdPeerCriteria, @UnitCost, @Quantity, @TotalCost, @IdPeerGroup
	WHILE @@Fetch_status = 0
	BEGIN		
		INSERT INTO tblZiffBaseCost ([IdField],[IdZiffAccount],[IdPeerCriteria],[UnitCost],[Quantity],[TotalCost],[IdPeerGroup]) 
		VALUES ((SELECT IdField FROM @tblFields WHERE IdFieldAnt = @IdField ), 
		(SELECT IdZiffAccount FROM @tblZiffAccount WHERE IdZiffAccountAnt=@IdZiffAccount), 
		CASE WHEN @IdPeerCriteria = 0 THEN 0 ELSE (SELECT ISNULL(IdPeerCriteria,0) FROM @tblPeerCriteria WHERE IdPeerCriteriaAnt = @IdPeerCriteria ) END, 
		@UnitCost, 
		@Quantity, 
		@TotalCost, 
		CASE WHEN @IdPeerGroup = NULL THEN 0 WHEN @IdPeerGroup = 0 THEN 0 ELSE (SELECT ISNULL(IdPeerGroup,0) FROM @tblPeerGroup WHERE IdPeerGroupAnt = @IdPeerGroup) END)
		SET @IdZiffBaseCost = SCOPE_IDENTITY()
		INSERT INTO @tblZiffBaseCost VALUES (@IdZiffBaseCost, @IdZiffBaseCostAnt)
		-----------
		FETCH NEXT FROM CurZiffBaseCost INTO @IdZiffBaseCostAnt, @IdField, @IdZiffAccount, @IdPeerCriteria, @UnitCost, @Quantity, @TotalCost, @IdPeerGroup
	END
	CLOSE CurZiffBaseCost
	DEALLOCATE CurZiffBaseCost

	END /** Cost Benchmarking Module **/

	BEGIN /** Cost Projection Module **/
	--------------------------------------------------------------------------------------
	----------------------- COST PROJECTION MODULE ---------------------------------------
	--------------------------------------------------------------------------------------
	
	--------------------------------------------------------------------------------------
	-- TECHNICAL DRIVERS
	--------------------------------------------------------------------------------------
	DECLARE @IdTechnicalDriver int
	DECLARE @IdTechnicalDriverAnt INT
	DECLARE @TypeDriver int
	DECLARE @tblTechnicalDrivers TABLE (IdTechnicalDriver int, IdTechnicalDriverAnt int)
	DECLARE @BaselineAllocation INT
	DECLARE GenericCursor CURSOR FOR
		SELECT A.IdTechnicalDriver, A.Name, A.IdUnit, A.IdTypeOperation, A.TypeDriver, UnitCostDenominator, BaselineAllocation
		FROM [dbo].[tblTechnicalDrivers] A
		WHERE A.IdModel = @IdModelBase
	OPEN GenericCursor

	FETCH NEXT FROM GenericCursor INTO @IdTechnicalDriverAnt, @Name, @IdUnit, @IdTypeOperation, @TypeDriver, @UnitCostDenominator, @BaselineAllocation
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblTechnicalDrivers ([IdModel],[Name],[IdUnit],[IdTypeOperation],[TypeDriver],[UserCreation],[DateCreation],[UserModification],[DateModification],[UnitCostDenominator],[BaselineAllocation]) VALUES (@IdModel, @Name, @IdUnit, @IdTypeOperation, @TypeDriver, @UserCreation,@DateCreation,@UserModification,@DateModification,@UnitCostDenominator, @BaselineAllocation)
		SET @IdTechnicalDriver = SCOPE_IDENTITY()
		INSERT INTO @tblTechnicalDrivers VALUES (@IdTechnicalDriver, @IdTechnicalDriverAnt)
		-----------
		FETCH NEXT FROM GenericCursor INTO @IdTechnicalDriverAnt, @Name, @IdUnit, @IdTypeOperation, @TypeDriver, @UnitCostDenominator, @BaselineAllocation
	END
	CLOSE GenericCursor
	DEALLOCATE GenericCursor

	--------------------------------------------------------------------------------------
	-- TECHNICAL DRIVER DATA
	--------------------------------------------------------------------------------------
	DECLARE @IdTechnicalDriverData int
	DECLARE @IdTechnicalDriverDataAnt INT		
	DECLARE @tblTechnicalDriverData TABLE (IdTechnicalDriverData int, IdTechnicalDriverDataAnt int)
	DECLARE @Year INT
	DECLARE @Normal INT
	DECLARE @Optimistic INT
	DECLARE @Pessimistic INT
	DECLARE GenericCursor CURSOR FOR
		SELECT A.IdTechnicalDriverData, A.IdTechnicalDriver, A.IdField, A.IdProject, A.[Year], A.Normal, A.Pessimistic, A.[Optimistic]
		FROM [dbo].[tblTechnicalDriverData] A
		WHERE A.IdModel = @IdModelBase
	OPEN GenericCursor

	FETCH NEXT FROM GenericCursor INTO @IdTechnicalDriverDataAnt, @IdTechnicalDriver,@IdField,@Idproject,@Year, @Normal, @Pessimistic, @Optimistic
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblTechnicalDriverData ([IdModel],[IdTechnicalDriver],[IdField],[IdProject],[Year],[Normal],[Pessimistic],[Optimistic],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES (@IdModel, (SELECT IdTechnicalDriver FROM @tblTechnicalDrivers WHERE IdTechnicalDriverAnt=@IdTechnicalDriver), (SELECT IdField FROM @tblFields WHERE IdFieldAnt=@IdField), (SELECT IdProject FROM @tblProjects WHERE IdProjectAnt=@IdProject), @Year, @Normal, @Pessimistic, @Optimistic, @UserCreation,@DateCreation,@UserModification,@DateModification)
		SET @IdTechnicalDriverData = SCOPE_IDENTITY()
		INSERT INTO @tblTechnicalDriverData VALUES (@IdTechnicalDriverData, @IdTechnicalDriverDataAnt)
		-----------
		FETCH NEXT FROM GenericCursor INTO @IdTechnicalDriverDataAnt, @IdTechnicalDriver,@IdField,@Idproject,@Year, @Normal, @Pessimistic, @Optimistic
	END
	CLOSE GenericCursor
	DEALLOCATE GenericCursor

	--------------------------------------------------------------------------------------
	-- TECHNICAL ASSUMPTION
	--------------------------------------------------------------------------------------
	DECLARE @IdTechnicalAssumption int
	DECLARE @IdTechnicalAssumptionAnt INT
	DECLARE @IdTechnicalDriverSize INT
	DECLARE @IdTechnicalDriverPerformance INT
	DECLARE @CycleValue INT
	DECLARE @ProjectionCriteriaTechnical int
	DECLARE @tblTechnicalAssumption TABLE (IdTechnicalAssumption int, IdTechnicalAssumptionAnt int)
	DECLARE GenericCursor CURSOR FOR
		SELECT A.IdTechnicalAssumption, A.IdTechnicalDriverSize, A.IdTechnicalDriverPerformance, A.IdZiffAccount, A.IdTypeOperation, A.CycleValue, A.ProjectionCriteria
		FROM [dbo].[tblTechnicalAssumption] A
		WHERE A.IdModel = @IdModelBase
	OPEN GenericCursor

	FETCH NEXT FROM GenericCursor INTO @IdTechnicalAssumptionAnt, @IdTechnicalDriverSize, @IdTechnicalDriverPerformance, @IdZiffAccount, @IdTypeOperation, @CycleValue, @ProjectionCriteriaTechnical
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblTechnicalAssumption ([IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[IdZiffAccount],[IdModel],[IdTypeOperation],[CycleValue],[ProjectionCriteria],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES (ISNULL((SELECT IdTechnicalDriver FROM @tblTechnicalDrivers WHERE IdTechnicalDriverAnt=@IdTechnicalDriverSize),0), ISNULL((SELECT IdTechnicalDriver FROM @tblTechnicalDrivers WHERE IdTechnicalDriverAnt=@IdTechnicalDriverPerformance),0), (SELECT IdZiffAccount FROM @tblZiffAccount WHERE IdZiffAccountAnt=@IdZiffAccount), @IdModel, @IdTypeOperation, @CycleValue, @ProjectionCriteriaTechnical, @UserCreation,@DateCreation,@UserModification,@DateModification)
		SET @IdTechnicalAssumption = SCOPE_IDENTITY()
		INSERT INTO @tblTechnicalAssumption VALUES (@IdTechnicalAssumption, @IdTechnicalAssumptionAnt)
		-----------
		FETCH NEXT FROM GenericCursor INTO @IdTechnicalAssumptionAnt, @IdTechnicalDriverSize, @IdTechnicalDriverPerformance, @IdZiffAccount, @IdTypeOperation, @CycleValue, @ProjectionCriteriaTechnical
	END
	CLOSE GenericCursor
	DEALLOCATE GenericCursor

	----------------------------------------------------------------------------------------
	---- UTILIZATION CAPACITY
	----------------------------------------------------------------------------------------
	--DECLARE @IdUtilization INT
	--DECLARE @IdUtilizationAnt INT
	--DECLARE @CodeZiff VARCHAR(50)
	--DECLARE @ZiffAccount VARCHAR(150)
	--DECLARE @BaseCostType INT
	--DECLARE @UtilizationValue FLOAT
	--DECLARE @IdStage INT
	--DECLARE @tblUtilizationCapacity TABLE (IdUtilization int, IdUtilizationAnt int) 	
	--DECLARE GenericCursor CURSOR FOR
	--	SELECT A.IdUtilization, A.IdZiffAccount, A.CodeZiff, A.ZiffAccount, A.SortOrder, A.IdField, A.IdStage, A.BaseCostType, A.UtilizationValue
	--	FROM [dbo].[tblUtilizationCapacity] A
	--	WHERE A.IdModel = @IdModelBase
	--OPEN GenericCursor

	--FETCH NEXT FROM GenericCursor INTO @IdUtilizationAnt, @IdZiffAccount, @CodeZiff, @ZiffAccount, @SortOrder, @IdField, @IdStage, @BaseCostType, @UtilizationValue
	--WHILE @@Fetch_status = 0
	--BEGIN
	--	INSERT INTO tblUtilizationCapacity ([IdModel],[IdZiffAccount],[CodeZiff],[ZiffAccount],[SortOrder],[IdField],[IdStage],[BaseCostType],[UtilizationValue],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES (@IdModel,(SELECT IdZiffAccount FROM @tblZiffAccount WHERE IdZiffAccountAnt=@IdZiffAccount),@CodeZiff,@ZiffAccount,@SortOrder,(SELECT IdField FROM @tblFields WHERE IdFieldAnt=@IdField),@IdStage,@BaseCostType,@UtilizationValue,@UserCreation,@DateCreation,@UserModification,@DateModification)
	--	SET @IdUtilization = SCOPE_IDENTITY()
	--	INSERT INTO @tblUtilizationCapacity VALUES (@IdUtilization,@IdUtilizationAnt)
	--	-----------
	--	FETCH NEXT FROM GenericCursor INTO @IdUtilizationAnt, @IdZiffAccount, @CodeZiff, @ZiffAccount, @SortOrder, @IdField, @IdStage, @BaseCostType, @UtilizationValue
	--END
	--CLOSE GenericCursor
	--DEALLOCATE GenericCursor
		
	--------------------------------------------------------------------------------------
	-- COST STRUCTURE ASSUMPTIONS
	--------------------------------------------------------------------------------------
--print 'starting COST STRUCTURE ASSUMPTIONS'
	DECLARE @IdCostStructureAssumptions int
	DECLARE @IdCostStructureAssumptionsAnt int
	DECLARE @Client int
	DECLARE @Ziff int
	DECLARE @tblCostStructureAssumptionsByProject TABLE (IdCostStructureAssumptions int, IdCostStructureAssumptionsAnt int)
	DECLARE CurtblCostStructureAssumptionsByProject CURSOR FOR
		SELECT A.IdCostStructureAssumptions, A.IdProject, A.IdZiffAccount, A.Client, A.Ziff, A.IdPeerGroup 
		FROM [dbo].[tblCostStructureAssumptionsByProject] A
			INNER JOIN tblProjects P ON P.IdProject = A.IdProject
		WHERE P.IdModel = @IdModelBase
	OPEN CurtblCostStructureAssumptionsByProject

--SELECT A.IdCostStructureAssumptions, A.IdProject, A.IdZiffAccount, A.Client, A.Ziff, A.IdPeerGroup 
--FROM [dbo].[tblCostStructureAssumptionsByProject] A
--	INNER JOIN tblProjects P ON P.IdProject = A.IdProject
--WHERE P.IdModel = @IdModelBase

--SELECT * FROM @tblPeerGroup
 
--DECLARE @NewIdPeerGroup INT

	FETCH NEXT FROM CurtblCostStructureAssumptionsByProject INTO @IdCostStructureAssumptions, @IdProject, @IdZiffAccount, @Client, @Ziff, @IdPeerGroup
	WHILE @@Fetch_status = 0
	BEGIN
--IF @IdPeerGroup <> 0
--BEGIN	
--	SELECT @NewIdPeerGroup = IdPeerGroup FROM @tblPeerGroup WHERE IdPeerGroupAnt=@IdPeerGroup
--	print '@IdCostStructureAssumptions=' + cast(@IdCostStructureAssumptions as nvarchar(10)) + ' : @IdProject='+ cast(@IdProject as nvarchar(10))+' : @IdZiffAccount='+cast(@IdZiffAccount as nvarchar(10))+' : @Client='+cast(@Client as nvarchar(10))+' : @Ziff='+cast(@Ziff as nvarchar(10))+' : @IdPeerGroup='+cast(@IdPeerGroup as nvarchar(10))+' : @NewIdPeerGroup='+cast(@NewIdPeerGroup as nvarchar(10))
--END
		INSERT INTO tblCostStructureAssumptionsByProject ([IdProject],[IdZiffAccount],[Client],[Ziff],[IdPeerGroup]) 
		VALUES ((SELECT IdProject FROM @tblProjects WHERE IdProjectAnt=@IdProject), 
		(SELECT IdZiffAccount FROM @tblZiffAccount WHERE IdZiffAccountAnt=@IdZiffAccount), 
		@Client, 
		@Ziff, 
		CASE WHEN @IdPeerGroup = 0 THEN 0 ELSE (SELECT IdPeerGroup FROM @tblPeerGroup WHERE IdPeerGroupAnt=@IdPeerGroup) END)
		SET @IdCostStructureAssumptions = SCOPE_IDENTITY()
		INSERT INTO @tblCostStructureAssumptionsByProject VALUES (@IdCostStructureAssumptions, @IdCostStructureAssumptionsAnt)
		-----------
		FETCH NEXT FROM CurtblCostStructureAssumptionsByProject INTO @IdCostStructureAssumptions, @IdProject, @IdZiffAccount, @Client, @Ziff, @IdPeerGroup 
	END
	CLOSE CurtblCostStructureAssumptionsByProject
	DEALLOCATE CurtblCostStructureAssumptionsByProject
print 'finishing COST STRUCTURE ASSUMPTIONS'

	--------------------------------------------------------------------------------------
	-- PRICE SCENARIOS
	--------------------------------------------------------------------------------------
	DECLARE @IdPriceScenario int
	DECLARE @IdPriceScenarioAnt int 
	DECLARE @tblPriceScenarios TABLE (IdPriceScenario int, IdPriceScenarioAnt int)
	DECLARE @ScenarioName NVARCHAR(50)
	DECLARE GenericCursor CURSOR FOR
		SELECT A.[IdPriceScenario],A.[ScenarioName]
		FROM [dbo].[tblPriceScenarios] A
		WHERE IdModel = @IdModelBase
	OPEN GenericCursor

	FETCH NEXT FROM GenericCursor INTO @IdPriceScenarioAnt, @ScenarioName
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblPriceScenarios([IdModel],[ScenarioName]) VALUES (@IdModel,@ScenarioName)
		SET @IdPriceScenario = SCOPE_IDENTITY()
		INSERT INTO @tblPriceScenarios VALUES(@IdPriceScenario,@IdPriceScenarioAnt)
		-----------
		FETCH NEXT FROM GenericCursor INTO @IdPriceScenarioAnt, @ScenarioName
	END
	CLOSE GenericCursor
	DEALLOCATE GenericCursor

	--------------------------------------------------------------------------------------
	-- MODELS PRICES
	--------------------------------------------------------------------------------------
	DECLARE @IdModelPrice int
	DECLARE @IdModelPriceAnt int
	DECLARE @tblModelsPrices TABLE (IdModelPrice int, IdModelPriceAnt int)
	DECLARE @IdCurrency int
	DECLARE @PriceYear int
	DECLARE @PriceValue float
	DECLARE @PriceOffset int
	DECLARE GenericCursor CURSOR FOR
		SELECT A.[IdModelPrice],A.[IdCurrency],A.[IdField],A.[PriceYear],A.[IdTechnicalDriver],A.[IdPriceScenario],A.[PriceValue],A.[PriceOffset]
		FROM [dbo].[tblModelsPrices] A
		WHERE A.IdModel = @IdModelBase
	OPEN GenericCursor

	FETCH NEXT FROM GenericCursor INTO @IdModelPriceAnt,@IdCurrency,@IdField,@PriceYear,@IdTechnicalDriver,@IdPriceScenario,@PriceValue,@PriceOffset
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblModelsPrices([IdModel],[IdCurrency],[IdField],[PriceYear],[IdTechnicalDriver],[IdPriceScenario],[PriceValue],[PriceOffset]) VALUES (@IdModel,@IdCurrency,CASE WHEN @Idfield = 0 THEN 0 ELSE (SELECT IdField FROM @tblFields WHERE IdFieldAnt = @IdField ) END,@PriceYear,(SELECT IdTechnicalDriver FROM @tblTechnicalDrivers WHERE IdTechnicalDriverAnt = @IdTechnicalDriver),(SELECT IdPriceScenario FROM @tblPriceScenarios WHERE IdPriceScenarioAnt = @IdPriceScenario),@PriceValue,@PriceOffset)
		SET @IdModelPrice = SCOPE_IDENTITY()
		INSERT INTO @tblModelsPrices VALUES(@IdModelPrice,@IdModelPriceAnt)
		-----------
		FETCH NEXT FROM GenericCursor INTO @IdModelPriceAnt,@IdCurrency,@IdField,@PriceYear,@IdTechnicalDriver,@IdPriceScenario,@PriceValue,@PriceOffset
	END
	CLOSE GenericCursor
	DEALLOCATE GenericCursor
	
	--------------------------------------------------------------------------------------
	-- ECONOMIC DRIVER GROUPS
	--------------------------------------------------------------------------------------
	DECLARE @IdDriverGroup int
	DECLARE @IdDriverGroupAnt int
	DECLARE @NameDriverGroup varchar(250)
	DECLARE @tblDriverGroups TABLE (IdDriverGroup int, IdDriverGroupAnt int)
	DECLARE GenericCursor CURSOR FOR
		SELECT A.IdDriverGroup, A.Name
		FROM [dbo].[tblDriverGroups] A
		WHERE A.IdModel = @IdModelBase
	OPEN GenericCursor

	FETCH NEXT FROM GenericCursor INTO @IdDriverGroupAnt, @NameDriverGroup
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblDriverGroups ([IdModel],[Name],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES (@IdModel,@NameDriverGroup,@UserCreation,@DateCreation,@UserModification,@DateModification)
		SET @IdDriverGroup = SCOPE_IDENTITY()
		INSERT INTO @tblDriverGroups VALUES (@IdDriverGroup, @IdDriverGroupAnt)
		-----------
		FETCH NEXT FROM GenericCursor INTO @IdDriverGroupAnt, @NameDriverGroup
	END
	CLOSE GenericCursor
	DEALLOCATE GenericCursor

	--------------------------------------------------------------------------------------	
	-- ECONOMIC DRIVERS
	--------------------------------------------------------------------------------------
	DECLARE @IdEconomicDriver int
	DECLARE @IdEconomicDriverAnt int
	DECLARE @tblEconomicDrivers TABLE (IdEconomicDriver int, IdEconomicDriverAnt int)
	DECLARE GenericCursor CURSOR FOR
		SELECT A.IdEconomicDriver, A.Name, A.IdDriverGroup
		FROM [dbo].[tblEconomicDrivers] A
		WHERE A.IdModel = @IdModelBase
	OPEN GenericCursor

	FETCH NEXT FROM GenericCursor INTO @IdEconomicDriverAnt, @Name, @IdDriverGroup
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblEconomicDrivers ([IdModel],[Name],[IdDriverGroup],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES (@IdModel,@Name,(SELECT IdDriverGroup FROM @tblDriverGroups WHERE IdDriverGroupAnt=@IdDriverGroup),@UserCreation,@DateCreation,@UserModification,@DateModification)
		SET @IdEconomicDriver = SCOPE_IDENTITY()
		INSERT INTO @tblEconomicDrivers VALUES (@IdEconomicDriver, @IdEconomicDriverAnt)
		-----------
		FETCH NEXT FROM GenericCursor INTO @IdEconomicDriverAnt, @Name, @IdDriverGroup
	END
	CLOSE GenericCursor
	DEALLOCATE GenericCursor

	--------------------------------------------------------------------------------------	
	-- ECONOMIC DRIVER DATA
	--------------------------------------------------------------------------------------
	DECLARE @IdEconomicDriverData int
	DECLARE @IdEconomicDriverDataAnt INT
	DECLARE @NormalElasticity INT
	DECLARE @NormalWeight INT
	DECLARE @OptimisticElasticity INT
	DECLARE @OptimisticWeight INT
	DECLARE @PessimisticElasticity INT
	DECLARE @PessimisticWeight INT
	DECLARE @tblEconomicDriverData TABLE (IdEconomicDriverData int, IdEconomicDriverDataAnt int)
	DECLARE GenericCursor CURSOR FOR
		SELECT A.IdEconomicDriverData, A.IdEconomicDriver, A.[Year], A.Normal, A.NormalElasticity, A.NormalWeight
		,A.[Optimistic],A.OptimisticElasticity,A.OptimisticWeight,Pessimistic,PessimisticElasticity,PessimisticWeight 
		FROM [dbo].[tblEconomicDriverData] A
		WHERE A.IdModel = @IdModelBase
	OPEN GenericCursor

	FETCH NEXT FROM GenericCursor INTO @IdEconomicDriverDataAnt,@IdEconomicDriver, @Year, @Normal,@NormalElasticity,@NormalWeight,@Optimistic,@OptimisticElasticity,@OptimisticWeight,@Pessimistic,@PessimisticElasticity,@PessimisticWeight
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblEconomicDriverData ([IdEconomicDriver],[IdModel],[Year],[Normal],[NormalElasticity],[NormalWeight],[Optimistic],[OptimisticElasticity],[OptimisticWeight],[Pessimistic],[PessimisticElasticity],[PessimisticWeight],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES ((SELECT IdEconomicDriver FROM @tblEconomicDrivers WHERE IdEconomicDriverAnt=@IdEconomicDriver),@IdModel,@Year,@Normal,@NormalElasticity,@NormalWeight,@Optimistic,@OptimisticElasticity,@OptimisticWeight,@Pessimistic,@PessimisticElasticity,@PessimisticWeight,@UserCreation,@DateCreation,@UserModification,@DateModification)
		SET @IdEconomicDriver = SCOPE_IDENTITY()
		INSERT INTO @tblEconomicDriverData VALUES (@IdEconomicDriverData, @IdEconomicDriverDataAnt)
		-----------
		FETCH NEXT FROM GenericCursor INTO @IdEconomicDriverDataAnt,@IdEconomicDriver, @Year, @Normal,@NormalElasticity,@NormalWeight,@Optimistic,@OptimisticElasticity,@OptimisticWeight,@Pessimistic,@PessimisticElasticity,@PessimisticWeight
	END
	CLOSE GenericCursor
	DEALLOCATE GenericCursor

	--------------------------------------------------------------------------------------	
	-- PRICE COMPOSITION
	--------------------------------------------------------------------------------------
	DECLARE @IdPriceComposition int
	DECLARE @IdPriceCompositionAnt INT
	DECLARE @Tradable float
	DECLARE @NonTradable float
	DECLARE @tblPriceComposition TABLE (IdPriceComposition int, IdPriceCompositionAnt int)
	DECLARE GenericCursor CURSOR FOR
		SELECT A.IdPriceComposition, A.IdZiffAccount, A.Tradable, A.NonTradable
		FROM [dbo].[tblPriceComposition] A
		WHERE A.IdModel = @IdModelBase
	OPEN GenericCursor

	FETCH NEXT FROM GenericCursor INTO @IdPriceCompositionAnt, @IdZiffAccount, @Tradable, @NonTradable
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblPriceComposition ([IdZiffAccount],[IdModel],[Tradable],[NonTradable],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES ((SELECT IdZiffAccount FROM @tblZiffAccount WHERE IdZiffAccountAnt=@IdZiffAccount), @IdModel, @Tradable, @NonTradable, @UserCreation,@DateCreation,@UserModification,@DateModification)
		SET @IdPriceComposition = SCOPE_IDENTITY()
		INSERT INTO @tblPriceComposition VALUES (@IdPriceComposition, @IdPriceCompositionAnt)
		-----------
		FETCH NEXT FROM GenericCursor INTO @IdPriceCompositionAnt, @IdZiffAccount, @Tradable, @NonTradable
	END
	CLOSE GenericCursor
	DEALLOCATE GenericCursor

	--------------------------------------------------------------------------------------	
	-- PRICE FORECAST
	--------------------------------------------------------------------------------------
	DECLARE @IdPriceForecast int
	DECLARE @IdPriceForecastAnt INT
	DECLARE @ValueStage float
	DECLARE @IdStage INT
	DECLARE @tblPriceForecast TABLE (IdPriceForecast int, IdPriceForecastAnt int)
	DECLARE GenericCursor CURSOR FOR
		SELECT A.IdPriceForecast, A.IdZiffAccount, A.[YEAR], A.IdStage, A.ValueStage
		FROM [dbo].[tblPriceForecast] A
		WHERE A.IdModel = @IdModelBase
	OPEN GenericCursor

	FETCH NEXT FROM GenericCursor INTO @IdPriceForecastAnt, @IdZiffAccount, @Year, @IdStage, @ValueStage
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblPriceForecast ([IdZiffAccount],[IdModel],[Year],[IdStage],[ValueStage],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES ((SELECT IdZiffAccount FROM @tblZiffAccount WHERE IdZiffAccountAnt=@IdZiffAccount), @IdModel, @Year, @IdStage, @ValueStage, @UserCreation,@DateCreation,@UserModification,@DateModification)
		SET @IdPriceForecast = SCOPE_IDENTITY()
		INSERT INTO @tblPriceForecast VALUES (@IdPriceForecast, @IdPriceForecastAnt)
		-----------
		FETCH NEXT FROM GenericCursor INTO @IdPriceForecastAnt, @IdZiffAccount, @Year, @IdStage, @ValueStage
	END
	CLOSE GenericCursor
	DEALLOCATE GenericCursor

	--------------------------------------------------------------------------------------
	-- TYPE PRICE ECONOMIC
	--------------------------------------------------------------------------------------
	DECLARE @IdTypePriceEconomic int
	DECLARE @IdTypePriceEconomicAnt INT
	DECLARE @ProjectionCriteriaEconomic INT		
	DECLARE @tblTypePriceEconomic TABLE (IdTypePriceEconomic int, IdTypePriceEconomicAnt int)
	DECLARE GenericCursor CURSOR FOR
		SELECT A.IdTypePriceEconomic, A.IdZiffAccount, A.ProjectionCriteria
		FROM [dbo].[tblTypePriceEconomic] A
		WHERE A.IdModel = @IdModelBase
	OPEN GenericCursor

	FETCH NEXT FROM GenericCursor INTO @IdTypePriceEconomicAnt, @IdZiffAccount,@ProjectionCriteriaEconomic
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblTypePriceEconomic ([IdZiffAccount],[IdModel],[ProjectionCriteria],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES ((SELECT IdZiffAccount FROM @tblZiffAccount WHERE IdZiffAccountAnt=@IdZiffAccount),@IdModel, @ProjectionCriteriaEconomic, @UserCreation,@DateCreation,@UserModification,@DateModification)
		SET @IdTypePriceEconomic = SCOPE_IDENTITY()
		INSERT INTO @tblTypePriceEconomic VALUES (@IdTypePriceEconomic, @IdTypePriceEconomicAnt)
		
		-- TYPE PRICE WEIGHT
		DECLARE @Weight float
		DECLARE CurTypePriceWeight CURSOR FOR
			SELECT IdEconomicDriver, Weight
			FROM [dbo].[tblTypePriceWeight]
			WHERE IdTypePriceEconomic = @IdTypePriceEconomicAnt
		OPEN CurTypePriceWeight

		FETCH NEXT FROM CurTypePriceWeight INTO @IdEconomicDriver, @Weight
		WHILE @@Fetch_status = 0
		BEGIN
			INSERT INTO tblTypePriceWeight ([IdTypePriceEconomic],[IdEconomicDriver],[Weight]) VALUES (@IdTypePriceEconomic, (SELECT IdEconomicDriver FROM @tblEconomicDrivers WHERE IdEconomicDriverAnt=@IdEconomicDriver), @Weight)
			-----------
			FETCH NEXT FROM CurTypePriceWeight INTO @IdEconomicDriver, @Weight
		END
		CLOSE CurTypePriceWeight
		DEALLOCATE CurTypePriceWeight
		-----------
		FETCH NEXT FROM GenericCursor INTO @IdTypePriceEconomicAnt, @IdZiffAccount,@ProjectionCriteriaEconomic
	END
	CLOSE GenericCursor
	DEALLOCATE GenericCursor

	--------------------------------------------------------------------------------------
	-- ZIFF ECONOMIC ANALYSIS
	--------------------------------------------------------------------------------------
	DECLARE @IdZiffEconomicAnalysis int
	DECLARE @IdZiffEconomicAnalysisAnt INT
	DECLARE @AttachFile VARBINARY(MAX)
	DECLARE @FileName varchar(200)		
	DECLARE @tblZiffEconomicAnalysis TABLE (IdZiffEconomicAnalysis int, IdZiffEconomicAnalysisAnt int)
	DECLARE GenericCursor CURSOR FOR
		SELECT A.IdZiffEconomicAnalysis, A.IdZiffAccount, A.AttachFile, A.[FileName]
		FROM [dbo].[tblZiffEconomicAnalysis] A
		WHERE A.IdModel = @IdModelBase
	OPEN GenericCursor

	FETCH NEXT FROM GenericCursor INTO @IdZiffEconomicAnalysisAnt, @IdZiffAccount,@AttachFile,@FileName
	WHILE @@Fetch_status = 0
	BEGIN
		INSERT INTO tblZiffEconomicAnalysis ([IdModel],[IdZiffAccount],[AttachFile],[FileName],[UserCreation],[DateCreation],[UserModification],[DateModification]) VALUES (@IdModel, (SELECT IdZiffAccount FROM @tblZiffAccount WHERE IdZiffAccountAnt=@IdZiffAccount), @AttachFile, @FileName, @UserCreation,@DateCreation,@UserModification,@DateModification)
		SET @IdZiffEconomicAnalysis = SCOPE_IDENTITY()
		INSERT INTO @tblZiffEconomicAnalysis VALUES (@IdZiffEconomicAnalysis, @IdZiffEconomicAnalysisAnt)
		-----------
		FETCH NEXT FROM GenericCursor INTO @IdZiffEconomicAnalysisAnt, @IdZiffAccount,@AttachFile,@FileName
	END
	CLOSE GenericCursor
	DEALLOCATE GenericCursor
	END /** Cost Projection Module **/
print 'finished ZIFF ECONOMIC ANALYSIS'
	
END

GO
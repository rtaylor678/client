USE [DBCPM]
GO
/****** Object:  StoredProcedure [dbo].[allpListCostStructureAssumptionsByProject]    Script Date: 07/27/2015 15:53:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-06-11
-- Description:	List Cost Structure Assumptions By Project as the view will no longer work as we are adding a row to display the parent
-- ========================================================================================================================================
-- 2015-07-27	Added the Case name to the query as it will be displayed in the Cost Structure Assumptions page of the tool
-- ========================================================================================================================================

ALTER PROCEDURE [dbo].[allpListCostStructureAssumptionsByProject](
 @IdModel int,
 @IdField int
)
AS
BEGIN
	
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#CostStructureAssumptions') IS NOT NULL DROP TABLE #CostStructureAssumptions

	CREATE TABLE #CostStructureAssumptions (IdModel INT, IdCostStructureAssumptions INT, IdProject INT, ProjectName NVARCHAR(255), IdZiffAccount INT, Code NVARCHAR(50), Name NVARCHAR(255), ParentCode NVARCHAR(50), ParentName NVARCHAR(255), Client INT, Ziff INT , IdField INT, SortOrder NVARCHAR(100))
	INSERT INTO #CostStructureAssumptions (IdModel, IdCostStructureAssumptions, IdProject, ProjectName, IdZiffAccount, Code, Name, ParentCode, ParentName, Client, Ziff, IdField, SortOrder)
	SELECT	tblZiffAccountsParent.IdModel, tblCostStructureAssumptionsByProject.IdCostStructureAssumptions, tblCostStructureAssumptionsByProject.IdProject, tblProjects.Name,
			tblCostStructureAssumptionsByProject.IdZiffAccount, '&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;' + tblZiffAccounts.Code as Code, tblZiffAccounts.Name, tblZiffAccounts.Code AS ParentCode, 
			tblZiffAccountsParent.Name AS ParentName, tblCostStructureAssumptionsByProject.Client, tblCostStructureAssumptionsByProject.Ziff, 
			tblProjects.IdField, tblZiffAccounts.SortOrder
	FROM    tblCostStructureAssumptionsByProject LEFT OUTER JOIN
			tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject LEFT OUTER JOIN
			tblZiffAccounts ON tblCostStructureAssumptionsByProject.IdZiffAccount = tblZiffAccounts.IdZiffAccount LEFT OUTER JOIN
			tblZiffAccounts AS tblZiffAccountsParent ON tblZiffAccounts.Parent = tblZiffAccountsParent.IdZiffAccount
	WHERE  	tblProjects.IdModel = @IdModel
	AND		tblProjects.IdField = @IdField 

	INSERT INTO #CostStructureAssumptions (IdModel, IdCostStructureAssumptions, IdProject, ProjectName, IdZiffAccount, Code, Name, ParentCode, ParentName, Client, Ziff, IdField, SortOrder)
	SELECT NULL, NULL, NULL, NULL, NULL, dbo.tblZiffAccounts.Code, Name, NULL, NULL, 9999, 9999, NULL, dbo.tblZiffAccounts.SortOrder
	FROM tblZiffAccounts
	WHERE  	IdModel = @IdModel AND Parent IS NULL

	/** Build Output Table **/
	SELECT IdModel, IdCostStructureAssumptions, IdProject, ProjectName, IdZiffAccount, Code, Name, ParentCode, ParentName, Client, Ziff, IdField, SortOrder 
	FROM #CostStructureAssumptions 
	ORDER BY SortOrder
END
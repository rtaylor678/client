/*************************/
/***   TABLE CHANGES   ***/
/*************************/

/****** Object:  Table [dbo].[tblProjects]    Script Date: 12/03/2015 09:51:40 ******/
IF COL_LENGTH('tblProjects','CaseSelection') IS NULL
BEGIN
	ALTER TABLE [dbo].[tblProjects]
	ADD [CaseSelection] [bit] NOT NULL DEFAULT 0;
END
GO

/***** Delete FK constraint in tblPeerCriteriaCost to table tblPeerGroup *******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblPeerCriteriaCost_tblPeerGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblPeerCriteriaCost]'))
	ALTER TABLE [dbo].[tblPeerCriteriaCost] DROP CONSTRAINT [FK_tblPeerCriteriaCost_tblPeerGroup]
GO

/***** Delete FK constraint in tblPeerGroupField to table tblPeerGroup *******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblPeerGroupField_tblPeerGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblPeerGroupField]'))
	ALTER TABLE [dbo].[tblPeerGroupField] DROP CONSTRAINT [FK_tblPeerGroupField_tblPeerGroup]
GO

/****** Object:  Table [dbo].[tblPeerGroup]    Script Date: 01/22/2016 08:17:55 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblPeerGroup_tblModels]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblPeerGroup]'))
	ALTER TABLE [dbo].[tblPeerGroup] DROP CONSTRAINT [FK_tblPeerGroup_tblModels]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tblPeerGroup_FullBenchmark]') AND type = 'D')
	ALTER TABLE [dbo].[tblPeerGroup] DROP CONSTRAINT [DF_tblPeerGroup_FullBenchmark]
GO

/****** Object:  Table [dbo].[tblPeerGroup]    Script Date: 04/25/2016 10:49:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPeerGroup]') AND type in (N'U'))
	DROP TABLE [dbo].[tblPeerGroup]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblPeerGroup](
	[IdPeerGroup] [int] IDENTITY(1,1) NOT NULL,
	[IdModel] [int] NOT NULL,
	[PeerGroupCode] [varchar](20) NOT NULL,
	[PeerGroupDescription] [varchar](100) NOT NULL,
	[FullBenchmark] [bit] NOT NULL,
	[Comments] [varchar](250) NULL,
 CONSTRAINT [PK_tblPeerGroup] PRIMARY KEY CLUSTERED 
(
	[IdPeerGroup] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tblPeerGroup]  WITH CHECK ADD  CONSTRAINT [FK_tblPeerGroup_tblModels] FOREIGN KEY([IdModel])
REFERENCES [dbo].[tblModels] ([IdModel])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tblPeerGroup] CHECK CONSTRAINT [FK_tblPeerGroup_tblModels]
GO

ALTER TABLE [dbo].[tblPeerGroup] ADD  CONSTRAINT [DF_tblPeerGroup_FullBenchmark]  DEFAULT ((0)) FOR [FullBenchmark]
GO

/***** Add constraint back to table tblPeerCriteriaCost *******/
ALTER TABLE [dbo].[tblPeerCriteriaCost]  WITH CHECK ADD CONSTRAINT [FK_tblPeerCriteriaCost_tblPeerGroup] FOREIGN KEY([IdPeerGroup])
REFERENCES [dbo].[tblPeerGroup] ([IdPeerGroup])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tblPeerCriteriaCost] CHECK CONSTRAINT [FK_tblPeerCriteriaCost_tblPeerGroup]
GO

/***** Add constraint back to table tblPeerGroupField *******/
ALTER TABLE [dbo].[tblPeerGroupField]  WITH CHECK ADD  CONSTRAINT [FK_tblPeerGroupField_tblPeerGroup] FOREIGN KEY([IdPeerGroup])
REFERENCES [dbo].[tblPeerGroup] ([IdPeerGroup])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tblPeerGroupField] CHECK CONSTRAINT [FK_tblPeerGroupField_tblPeerGroup]
GO

/****** Object:  Table [dbo].[tblPeerGroupField]    Script Date: 12/07/2015 10:34:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblPeerGroupField_tblPeerGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblPeerGroupField]'))
ALTER TABLE [dbo].[tblPeerGroupField] DROP CONSTRAINT [FK_tblPeerGroupField_tblPeerGroup]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPeerGroupField]') AND type in (N'U'))
DROP TABLE [dbo].[tblPeerGroupField]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblPeerGroupField](
	[IdPeerGroupField] [int] IDENTITY(1,1) NOT NULL,
	[IdPeerGroup] [int] NOT NULL,
	[IdField] [int] NOT NULL,
 CONSTRAINT [PK_tblPeerGroupField] PRIMARY KEY CLUSTERED 
(
	[IdPeerGroupField] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblPeerGroupField]  WITH CHECK ADD  CONSTRAINT [FK_tblPeerGroupField_tblPeerGroup] FOREIGN KEY([IdPeerGroup])
REFERENCES [dbo].[tblPeerGroup] ([IdPeerGroup])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tblPeerGroupField] CHECK CONSTRAINT [FK_tblPeerGroupField_tblPeerGroup]
GO

/****** Object:  Table [dbo].[tblPeerCriteriaCost]    Script Date: 12/10/2015 11:30:33 ******/

/** Create a backup copy of table tblPeerCriteriaCost as new structure will be used. **/
IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[tblPeerCriteriaCost_OLD]') AND type in (N'U'))
BEGIN
	SELECT * INTO [dbo].[tblPeerCriteriaCost_OLD] FROM [dbo].[tblPeerCriteriaCost]
END

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblPeerCriteriaCost_tblFields]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblPeerCriteriaCost]'))
ALTER TABLE [dbo].[tblPeerCriteriaCost] DROP CONSTRAINT [FK_tblPeerCriteriaCost_tblFields]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPeerCriteriaCost]') AND type in (N'U'))
DROP TABLE [dbo].[tblPeerCriteriaCost]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[tblPeerCriteriaCost]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[tblPeerCriteriaCost](
		[IdPeerCriteria] [int] NOT NULL,
		[IdPeerGroup] [int] NOT NULL,
		[Quantity] [float] NOT NULL,
	 CONSTRAINT [PK_tblPeerCriteriaCost] PRIMARY KEY CLUSTERED 
	(
		[IdPeerCriteria] ASC,
		[IdPeerGroup] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
END
GO

ALTER TABLE [dbo].[tblPeerCriteriaCost]  WITH CHECK ADD  CONSTRAINT [FK_tblPeerCriteriaCost_tblPeerGroup] FOREIGN KEY([IdPeerGroup])
REFERENCES [dbo].[tblPeerGroup] ([IdPeerGroup])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tblPeerCriteriaCost] CHECK CONSTRAINT [FK_tblPeerCriteriaCost_tblPeerGroup]
GO

/****** Object:  Table [dbo].[tblCalcExternalCostReferences]    Script Date: 01/14/2016 10:55:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[tblCalcExternalCostReferences]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[tblCalcExternalCostReferences](
		[IdModel] [int] NOT NULL,
		[IdZiffBaseCost] [int] NOT NULL,
		[IdZiffAccount] [int] NOT NULL,
		[IdPeerGroup] [int] NULL,
		[IdPeerCriteria] [int] NULL,
		[UnitCost] [float] NULL,
		[Quantity] [float] NULL,
		[TotalCost] [float] NULL,
		[CodeZiff] [varchar](50) NULL,
		[ZiffAccount] [varchar](150) NULL,
		[IdRootZiffAccount] [int] NULL,
		[RootCodeZiff] [varchar](50) NULL,
		[RootZiffAccount] [varchar](150) NULL,
		[PeerCriteria] [varchar](50) NULL,
		[Unit] [varchar](150) NULL,
		[IdUnitPeerCriteria] [int] NULL,
		[TechApplicable] [bit] NULL,
		[TFNormal] [float] NULL,
		[TFPessimistic] [float] NULL,
		[TFOptimistic] [float] NULL,
		[BCRNormal] [float] NULL,
		[BCRPessimistic] [float] NULL,
		[BCROptimistic] [float] NULL,
		[SortOrder] [varchar](50) NULL,
		[ExternalAccountCode] [varchar](50) NULL,
		[PeerGroupDescription] [varchar](100) NULL,
	 CONSTRAINT [PK_tblCalcExternalCostReferences] PRIMARY KEY CLUSTERED 
	(
		[IdModel] ASC,
		[IdZiffBaseCost] ASC,
		[IdZiffAccount] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[tblModelsPrices]    Script Date: 01/14/2016 10:55:16 ******/
IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[tblModelsPrices]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[tblModelsPrices](
		[IdModelPrice] [int] IDENTITY(1,1) NOT NULL,
		[IdModel] [int] NOT NULL,
		[IdCurrency] [int] NOT NULL,
		[IdField] [int] NOT NULL,
		[PriceYear] [int] NOT NULL,
		[IdTechnicalDriver] [int] NOT NULL,
		[IdPriceScenario] [int] NOT NULL,
		[PriceValue] [float] NOT NULL,
		[PriceOffset] [int] NOT NULL,
	 CONSTRAINT [PK_tblModelsPrices] PRIMARY KEY CLUSTERED 
	(
		[IdModelPrice] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[tblModelsPrices]') AND type in (N'U'))
BEGIN
	ALTER TABLE [dbo].[tblModelsPrices]  WITH CHECK ADD  CONSTRAINT [FK_tblModelsPrices_tblCurrencies] FOREIGN KEY([IdCurrency])
	REFERENCES [dbo].[tblCurrencies] ([IdCurrency])
END	
GO

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[tblModelsPrices]') AND type in (N'U'))
BEGIN
	ALTER TABLE [dbo].[tblModelsPrices] CHECK CONSTRAINT [FK_tblModelsPrices_tblCurrencies]
END	
GO

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[tblModelsPrices]') AND type in (N'U'))
BEGIN
	ALTER TABLE [dbo].[tblModelsPrices]  WITH CHECK ADD  CONSTRAINT [FK_tblModelsPrices_tblModels] FOREIGN KEY([IdModel])
	REFERENCES [dbo].[tblModels] ([IdModel])
	ON DELETE CASCADE
END
GO

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[tblModelsPrices]') AND type in (N'U'))
BEGIN
	ALTER TABLE [dbo].[tblModelsPrices] CHECK CONSTRAINT [FK_tblModelsPrices_tblModels]
END
GO

/****** Object:  Table [dbo].[tblPriceScenarios]    Script Date: 12/18/2015 13:38:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[tblPriceScenarios]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[tblPriceScenarios](
		[IdPriceScenario] [int] IDENTITY(1,1) NOT NULL,
		[IdModel] [int] NOT NULL,
		[ScenarioName] [nvarchar](50) NOT NULL,
	 CONSTRAINT [PK_tblPriceScenarios] PRIMARY KEY CLUSTERED 
	(
		[IdPriceScenario] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[tblPriceScenarios]') AND type in (N'U'))
BEGIN
	ALTER TABLE [dbo].[tblPriceScenarios]  WITH CHECK ADD  CONSTRAINT [FK_tblPriceScenarios_tblModels] FOREIGN KEY([IdModel])
	REFERENCES [dbo].[tblModels] ([IdModel])
	ON DELETE CASCADE
END
GO

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[tblPriceScenarios]') AND type in (N'U'))
BEGIN
	ALTER TABLE [dbo].[tblPriceScenarios] CHECK CONSTRAINT [FK_tblPriceScenarios_tblModels]
END
GO

/****** Object:  Table [dbo].[tblTechnicalDrivers]    Script Date: 01/06/2016 09:00:19 ******/
IF COL_LENGTH('tblTechnicalDrivers','BaselineAllocation') IS NULL
BEGIN
	ALTER TABLE [dbo].[tblTechnicalDrivers]
	ADD [BaselineAllocation] [bit] NULL DEFAULT 0;
END
GO

/****** Object:  Table [dbo].[tblCalcBaseCostByFieldZiff]    Script Date: 01/06/2016 14:51:16 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblCalcBaseCostByFieldZiff_tblModels]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblCalcBaseCostByFieldZiff]'))
ALTER TABLE [dbo].[tblCalcBaseCostByFieldZiff] DROP CONSTRAINT [FK_tblCalcBaseCostByFieldZiff_tblModels]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tblCalcBaseCostByFieldZiff_TechApplicable]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCalcBaseCostByFieldZiff] DROP CONSTRAINT [DF_tblCalcBaseCostByFieldZiff_TechApplicable]
END

GO

/****** Object:  Table [dbo].[tblCalcBaseCostByFieldZiff]    Script Date: 04/25/2016 10:46:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCalcBaseCostByFieldZiff]') AND type in (N'U'))
DROP TABLE [dbo].[tblCalcBaseCostByFieldZiff]
GO

/****** Object:  Table [dbo].[tblCalcBaseCostByFieldZiff]    Script Date: 04/25/2016 10:46:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblCalcBaseCostByFieldZiff](
	[IdModel] [int] NOT NULL,
	[IdZiffBaseCost] [int] NULL,
	[IdField] [int] NOT NULL,
	[IdZiffAccount] [int] NOT NULL,
	[IdPeerCriteria] [int] NULL,
	[UnitCost] [float] NULL,
	[Quantity] [float] NULL,
	[TotalCost] [float] NULL,
	[Field] [varchar](150) NULL,
	[CodeZiff] [varchar](50) NULL,
	[ZiffAccount] [varchar](150) NULL,
	[IdRootZiffAccount] [int] NULL,
	[RootCodeZiff] [varchar](50) NULL,
	[RootZiffAccount] [varchar](150) NULL,
	[PeerCriteria] [varchar](50) NULL,
	[Unit] [varchar](150) NULL,
	[IdUnitPeerCriteria] [int] NULL,
	[TechApplicable] [bit] NOT NULL,
	[QuantityNormal] [float] NULL,
	[QuantityPessimistic] [float] NULL,
	[QuantityOptimistic] [float] NULL,
	[TotalCostNormal] [float] NULL,
	[TotalCostPessimistic] [float] NULL,
	[TotalCostOptimistic] [float] NULL,
	[TFNormal] [float] NULL,
	[TFPessimistic] [float] NULL,
	[TFOptimistic] [float] NULL,
	[BCRNormal] [float] NULL,
	[BCRPessimistic] [float] NULL,
	[BCROptimistic] [float] NULL,
	[IdPeerGroup] [int] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tblCalcBaseCostByFieldZiff]  WITH CHECK ADD  CONSTRAINT [FK_tblCalcBaseCostByFieldZiff_tblModels] FOREIGN KEY([IdModel])
REFERENCES [dbo].[tblModels] ([IdModel])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tblCalcBaseCostByFieldZiff] CHECK CONSTRAINT [FK_tblCalcBaseCostByFieldZiff_tblModels]
GO

ALTER TABLE [dbo].[tblCalcBaseCostByFieldZiff] ADD  CONSTRAINT [DF_tblCalcBaseCostByFieldZiff_TechApplicable]  DEFAULT ((0)) FOR [TechApplicable]
GO

/****** Object:  Table [dbo].[tblCalcProjectionTechnical] ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tblCalcProjectionTechnical_SortOrder]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCalcProjectionTechnical] DROP CONSTRAINT [DF_tblCalcProjectionTechnical_SortOrder]
END

GO
  
ALTER TABLE [dbo].[tblCalcProjectionTechnical]
DROP COLUMN [SortOrder];

GO

IF COL_LENGTH('tblCalcProjectionTechnical','CaseSelection') IS NULL
BEGIN
	ALTER TABLE [dbo].[tblCalcProjectionTechnical]
	ADD [CaseSelection] [int] NULL;
END
GO

ALTER TABLE [dbo].[tblCalcProjectionTechnical]
ADD [SortOrder] [int] NULL;

GO

ALTER TABLE [dbo].[tblCalcProjectionTechnical] ADD  CONSTRAINT [DF_tblCalcProjectionTechnical_SortOrder]  DEFAULT ((1)) FOR [SortOrder]
GO

ALTER TABLE [dbo].[tblCalcTechnicalDriverAssumptionZiff]  WITH CHECK ADD  CONSTRAINT [FK_tblCalcTechnicalDriverAssumptionZiff_tblModels] FOREIGN KEY([IdModel])
REFERENCES [dbo].[tblModels] ([IdModel])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tblCalcTechnicalDriverAssumptionZiff] CHECK CONSTRAINT [FK_tblCalcTechnicalDriverAssumptionZiff_tblModels]
GO

/****** Object:  Table [dbo].[tblZiffBaseCost]    Script Date: 02/02/2016 09:01:49 ******/
IF COL_LENGTH('tblZiffBaseCost','QuantityNormal') IS NULL AND 
   COL_LENGTH('tblZiffBaseCost','QuantityPessimistic') IS NULL AND
   COL_LENGTH('tblZiffBaseCost','QuantityOptimistic') IS NULL AND
   COL_LENGTH('tblZiffBaseCost','TotalCostNormal') IS NULL AND
   COL_LENGTH('tblZiffBaseCost','TotalCostPessimistic') IS NULL AND
   COL_LENGTH('tblZiffBaseCost','TotalCostOptimistic') IS NULL AND
   COL_LENGTH('tblZiffBaseCost','IdPeerGroup') IS NULL
BEGIN
	ALTER TABLE [dbo].[tblZiffBaseCost] 
	ADD [QuantityNormal] [float] NOT NULL DEFAULT ((0)),
		[QuantityPessimistic] [float] NOT NULL DEFAULT ((0)),
		[QuantityOptimistic] [float] NOT NULL DEFAULT ((0)),
		[TotalCostNormal] [float] NOT NULL DEFAULT ((0)),
		[TotalCostPessimistic] [float] NOT NULL DEFAULT ((0)),
		[TotalCostOptimistic] [float] NOT NULL DEFAULT ((0)),
		[IdPeerGroup] [int] NOT NULL DEFAULT ((0))
END
GO

/****** Object:  Table [dbo].[tblPeerCriteria]    Script Date: 12/07/2015 15:35:40 ******/
IF COL_LENGTH('tblPeerCriteria','TotalUnitCostDenominator') IS NULL
BEGIN
	ALTER TABLE [dbo].[tblPeerCriteria]
	ADD [TotalUnitCostDenominator] [bit] NOT NULL DEFAULT 0;
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCalcTechnicalBaseCostRate]') AND type in (N'U'))
DROP TABLE [dbo].[tblCalcTechnicalBaseCostRate]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

/****** Object:  Table [dbo].[tblCalcTechnicalBaseCostRate]    Script Date: 12/07/2015 15:35:40 ******/
CREATE TABLE [dbo].[tblCalcTechnicalBaseCostRate](
	[IdModel] [int] NOT NULL,
	[IdZiffAccount] [int] NOT NULL,
	[CodeZiff] [varchar](50) NULL,
	[ZiffAccount] [varchar](150) NULL,
	[DisplayCode] [nvarchar](max) NULL,
	[SortOrder] [nvarchar](max) NULL,
	[IdField] [int] NOT NULL,
	[IdStage] [int] NOT NULL,
	[BaseCostType] [int] NOT NULL,
	[ProjectionCriteria] [int] NULL,
	[NameProjectionCriteria] [varchar](100) NULL,
	[CycleValue] [int] NULL,
	[SizeName] [varchar](150) NULL,
	[SizeValue] [float] NULL,
	[PerformanceName] [varchar](150) NULL,
	[PerformanceValue] [float] NULL,
	[BaseCost] [float] NULL,
	[BaseCostRate] [float] NULL,
	[IdRootZiffAccount] [int] NULL,
	[RootCodeZiff] [nvarchar](50) NULL,
	[RootZiffAccount] [nvarchar](255) NULL,
	[BaselineAllocation] [int] NULL,
	[IdPeerGroup] [int] NOT NULL,
 CONSTRAINT [PK_tblCalcTechnicalBaseCostRate] PRIMARY KEY CLUSTERED 
(
	[IdModel] ASC,
	[IdZiffAccount] ASC,
	[IdField] ASC,
	[IdStage] ASC,
	[BaseCostType] ASC,
	[IdPeerGroup] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tblCalcTechnicalBaseCostRate]  WITH CHECK ADD  CONSTRAINT [FK_tblCalcTechnicalBaseCostRate_tblModels] FOREIGN KEY([IdModel])
REFERENCES [dbo].[tblModels] ([IdModel])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tblCalcTechnicalBaseCostRate] CHECK CONSTRAINT [FK_tblCalcTechnicalBaseCostRate_tblModels]
GO

/****** Object:  Table [dbo].[tblCostStructureAssumptionsByProject]    Script Date: 02/02/2016 08:41:55 ******/
IF COL_LENGTH('tblCostStructureAssumptionsByProject','IdPeerGroup') IS NULL
BEGIN
	ALTER TABLE [dbo].[tblCostStructureAssumptionsByProject]
	ADD [IdPeerGroup] [int] NULL;
END
GO

/****** Object:  Table [dbo].[tblCalcTechnicalDriverAssumptionZiff]    Script Date: 02/02/2016 08:52:32 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblCalcTechnicalDriverAssumptionZiff_tblModels]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblCalcTechnicalDriverAssumptionZiff]'))
ALTER TABLE [dbo].[tblCalcTechnicalDriverAssumptionZiff] DROP CONSTRAINT [FK_tblCalcTechnicalDriverAssumptionZiff_tblModels]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCalcTechnicalDriverAssumptionZiff]') AND type in (N'U'))
DROP TABLE [dbo].[tblCalcTechnicalDriverAssumptionZiff]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblCalcTechnicalDriverAssumptionZiff](
	[SourceDebug] [int] NULL,
	[IdModel] [int] NOT NULL,
	[IdZiffAccount] [int] NOT NULL,
	[IdTypeOperation] [int] NULL,
	[IdField] [int] NOT NULL,
	[IdProject] [int] NOT NULL,
	[Year] [int] NOT NULL,
	[Operation] [int] NULL,
	[BaseYear] [bit] NULL,
	[IdTechnicalDriverSize] [int] NULL,
	[IdTechnicalDriverPerformance] [int] NULL,
	[CycleValue] [int] NULL,
	[ProjectionCriteria] [int] NULL,
	[SizeNormal] [float] NULL,
	[SizePessimistic] [float] NULL,
	[SizeOptimistic] [float] NULL,
	[PerformanceNormal] [float] NULL,
	[PerformancePessimistic] [float] NULL,
	[PerformanceOptimistic] [float] NULL,
	[YearClient] [int] NULL,
	[YearZiff] [int] NULL,
	[TFNormal] [float] NULL,
	[TFPessimistic] [float] NULL,
	[TFOptimistic] [float] NULL,
	[BCRNormal] [float] NULL,
	[BCRPessimistic] [float] NULL,
	[BCROptimistic] [float] NULL,
	[SemifixedForecast] [bit] NULL,
	[SemivarForecast] [bit] NULL,
	[IdPeerGroup] [int] NOT NULL,
 CONSTRAINT [PK_tblCalcTechnicalDriverAssumptionZiff] PRIMARY KEY CLUSTERED 
(
	[IdModel] ASC,
	[IdZiffAccount] ASC,
	[IdField] ASC,
	[IdProject] ASC,
	[Year] ASC,
	[IdPeerGroup] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[tblFilesRepository]    Script Date: 05/31/2016 15:17:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFilesRepository]') AND type in (N'U'))
	DROP TABLE [dbo].[tblFilesRepository]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblFilesRepository](
	[IdFileRepository] [int] IDENTITY(1,1) NOT NULL,
	[IdModel] [int] NULL,
	[AttachFile] [varbinary](max) NULL,
	[FileName] [varchar](200) NULL,
	[UserCreation] [int] NULL,
	[DateCreation] [datetime] NULL,
	[UserModification] [int] NULL,
	[DateModification] [datetime] NULL,
 	[Comments] [varchar](250) NULL,
CONSTRAINT [PK_tblFilesRepository] PRIMARY KEY CLUSTERED 
(
	[IdFileRepository] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblFilesRepository_tblModels]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblFilesRepository]'))
ALTER TABLE [dbo].[tblFilesRepository] DROP CONSTRAINT [FK_tblFilesRepository_tblModels]
GO

ALTER TABLE [dbo].[tblFilesRepository]  WITH CHECK ADD  CONSTRAINT [FK_tblFilesRepository_tblModels] FOREIGN KEY([IdModel])
REFERENCES [dbo].[tblModels] ([IdModel])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tblFilesRepository] CHECK CONSTRAINT [FK_tblFilesRepository_tblModels]
GO

/*************************/
/*** VIEWS ***/
/*************************/

/****** Object:  View [dbo].[vListPeerCriteriaCost]    Script Date: 12/10/2015 11:44:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vListPeerCriteriaCost]
AS
SELECT	dbo.tblPeerGroup.IdModel, dbo.tblPeerCriteria.IdPeerCriteria, dbo.tblPeerGroup.IdPeerGroup, ISNULL(dbo.tblPeerCriteriaCost.Quantity, 0) AS Quantity, 
		dbo.tblPeerCriteria.Description AS PeerCriteria, dbo.tblPeerGroup.PeerGroupCode, dbo.tblUnits.Name AS Unit, dbo.tblTypeUnit.Name AS TypeUnit, 
		dbo.tblPeerGroup.PeerGroupDescription
FROM    dbo.tblUnits LEFT OUTER JOIN
		dbo.tblTypeUnit ON dbo.tblUnits.IdTypeUnit = dbo.tblTypeUnit.IdTypeUnit RIGHT OUTER JOIN
		dbo.tblPeerCriteriaCost RIGHT OUTER JOIN
		dbo.tblPeerCriteria INNER JOIN
        dbo.tblPeerGroup ON dbo.tblPeerCriteria.IdModel = dbo.tblPeerGroup.IdModel ON dbo.tblPeerCriteriaCost.IdPeerGroup = dbo.tblPeerGroup.IdPeerGroup AND 
        dbo.tblPeerCriteriaCost.IdPeerCriteria = dbo.tblPeerCriteria.IdPeerCriteria ON dbo.tblUnits.IdUnit = dbo.tblPeerCriteria.IdUnitPeerCriteria

GO

/****** Object:  View [dbo].[vListPeerGroup]    Script Date: 12/07/2015 14:39:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
IF  OBJECT_ID('dbo.vListPeerGroup', 'V') IS NOT NULL
	DROP VIEW [dbo].[vListPeerGroup]
GO

CREATE VIEW [dbo].[vListPeerGroup]
AS
SELECT	S.*, M.Name AS Model,
		(SELECT STUFF ((SELECT     ',' + CC2.Name FROM [dbo].[tblPeerGroupField] A2 INNER JOIN [dbo].[tblFields] CC2 ON CC2.IdField = A2.IdField WHERE A2.IdPeerGroup = S.IdPeerGroup FOR XML PATH('')), 1, 1, '')) AS Fields
FROM	dbo.tblPeerGroup S INNER JOIN
		dbo.tblModels M ON M.IdModel = S.IdModel

GO

/****** Object:  View [dbo].[vListPrices]    Script Date: 12/18/2015 16:00:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vListPrices]'))
DROP VIEW [dbo].[vListPrices]
GO

CREATE VIEW [dbo].[vListPrices]
AS
SELECT	DISTINCT 
		dbo.tblModelsPrices.IdModelPrice, dbo.tblModelsPrices.IdModel, dbo.tblCurrencies.IdCurrency, dbo.tblCurrencies.Code, dbo.tblCurrencies.Name AS CurrencyName, 
		dbo.tblModelsPrices.IdField, dbo.tblTechnicalDrivers.IdTechnicalDriver, dbo.tblTechnicalDrivers.Name AS Description, dbo.tblModelsPrices.IdPriceScenario, 
		dbo.tblPriceScenarios.ScenarioName, dbo.tblModelsPrices.PriceYear, dbo.tblModelsPrices.PriceValue, dbo.tblModelsPrices.PriceOffset, 
		dbo.tblModelsPrices.PriceValue - dbo.tblModelsPrices.PriceOffset AS NetPrice, dbo.tblModelsPrices.IdField AS FieldId
FROM dbo.tblModelsPrices INNER JOIN
		dbo.tblCurrencies ON dbo.tblModelsPrices.IdCurrency = dbo.tblCurrencies.IdCurrency INNER JOIN
		dbo.tblTechnicalDrivers ON dbo.tblModelsPrices.IdModel = dbo.tblTechnicalDrivers.IdModel AND 
		dbo.tblModelsPrices.IdTechnicalDriver = dbo.tblTechnicalDrivers.IdTechnicalDriver INNER JOIN
		dbo.tblPriceScenarios ON dbo.tblModelsPrices.IdPriceScenario = dbo.tblPriceScenarios.IdPriceScenario

GO

/****** Object:  View [dbo].[vListPeerCriteria]    Script Date: 12/07/2015 15:39:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vListPeerCriteria]
AS
SELECT	dbo.tblPeerCriteria.IdPeerCriteria, dbo.tblPeerCriteria.Description, dbo.tblPeerCriteria.IdUnitPeerCriteria, dbo.tblPeerCriteria.IdModel, dbo.tblUnits.Name AS Unit, 
        dbo.tblTypeUnit.IdTypeUnit, dbo.tblTypeUnit.Name AS TypeUnit, dbo.tblPeerCriteria.TotalUnitCostDenominator
FROM    dbo.tblTypeUnit RIGHT OUTER JOIN
        dbo.tblUnits ON dbo.tblTypeUnit.IdTypeUnit = dbo.tblUnits.IdTypeUnit RIGHT OUTER JOIN
        dbo.tblPeerCriteria ON dbo.tblUnits.IdUnit = dbo.tblPeerCriteria.IdUnitPeerCriteria

GO

/****** Object:  View [dbo].[vListProjects]    Script Date: 12/03/2015 14:04:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vListProjects]
AS
SELECT	A.IdProject, A.IdModel, A.Code, A.Name, A.IdField, A.Operation, A.Starts, A.TypeAllocation, A.UserCreation, A.DateCreation, A.UserModification, A.DateModification, 
        C.Name AS Field, B.Name AS OperationName, C.Code AS FieldCode, B.IdLanguage, A.CaseSelection
FROM    dbo.tblProjects AS A INNER JOIN
        dbo.tblFields AS C ON C.IdField = A.IdField INNER JOIN
        dbo.tblParameters AS B ON B.Code = A.Operation AND B.Type = 'OperationProject'

GO

/****** Object:  View [dbo].[vListTechnicalDrivers]    Script Date: 01/06/2016 09:22:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vListTechnicalDrivers]
AS
SELECT	T.Name AS NameTypeDriver, dbo.tblUnits.Name AS NameUnit, 
        CASE WHEN dbo.tblTechnicalDrivers.IdTypeOperation = 0 THEN dbo.vListTypesOperationWithGeneral.Name ELSE dbo.vListTypesOperationWithGeneral.ParentName +
        ' - ' + dbo.vListTypesOperationWithGeneral.Name END AS NameTypeOperation, dbo.tblTypeUnit.Name AS NameTypeUnit, dbo.tblTechnicalDrivers.IdTechnicalDriver, 
        dbo.tblTechnicalDrivers.IdModel, dbo.tblTechnicalDrivers.Name, dbo.tblTechnicalDrivers.IdUnit, dbo.tblTechnicalDrivers.IdTypeOperation, 
        dbo.tblTechnicalDrivers.TypeDriver, dbo.tblTechnicalDrivers.UserCreation, dbo.tblTechnicalDrivers.DateCreation, dbo.tblTechnicalDrivers.UserModification, 
        dbo.tblTechnicalDrivers.DateModification, dbo.tblTypeUnit.IdTypeUnit, dbo.vListTypesOperationWithGeneral.Shore, 
        ISNULL(dbo.tblTechnicalDrivers.UnitCostDenominator, 0) AS UnitCostDenominator, T.IdLanguage, ISNULL(dbo.tblTechnicalDrivers.BaselineAllocation, 0) AS BaselineAllocation
FROM    dbo.tblUnits LEFT OUTER JOIN
        dbo.tblTypeUnit ON dbo.tblUnits.IdTypeUnit = dbo.tblTypeUnit.IdTypeUnit RIGHT OUTER JOIN
        dbo.vListTypesOperationWithGeneral RIGHT OUTER JOIN
        dbo.tblTechnicalDrivers ON dbo.vListTypesOperationWithGeneral.IdModel = dbo.tblTechnicalDrivers.IdModel AND 
        dbo.vListTypesOperationWithGeneral.IdTypeOperation = dbo.tblTechnicalDrivers.IdTypeOperation LEFT OUTER JOIN
        (SELECT	TOP (200) Code, Name, IdLanguage
         FROM   dbo.tblParameters
         WHERE  (Type = 'TypeDriver')) AS T ON dbo.tblTechnicalDrivers.TypeDriver = T.Code ON dbo.tblUnits.IdUnit = dbo.tblTechnicalDrivers.IdUnit

GO

/****** Object:  View [dbo].[vListFilesRepository]    Script Date: 05/31/2016 16:04:13 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vListFilesRepository]'))
	DROP VIEW [dbo].[vListFilesRepository]
GO	

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vListFilesRepository]
AS
SELECT	dbo.tblFilesRepository.IdFileRepository, dbo.tblFilesRepository.IdModel, dbo.tblFilesRepository.AttachFile, dbo.tblFilesRepository.FileName, 
        dbo.tblFilesRepository.UserCreation, dbo.tblFilesRepository.DateCreation, dbo.tblFilesRepository.UserModification, dbo.tblFilesRepository.DateModification, 
        dbo.tblUsers.Name,dbo.tblFilesRepository.Comments
FROM    dbo.tblFilesRepository INNER JOIN
        dbo.tblUsers ON dbo.tblFilesRepository.UserCreation = dbo.tblUsers.IdUser

GO

/****** Object:  View [dbo].[vListFieldsPerPeerGroup]    Script Date: 05/05/2016 15:29:21 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vListFieldsPerPeerGroup]'))
	DROP VIEW [dbo].[vListFieldsPerPeerGroup]
GO	

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].vListFieldsPerPeerGroup
AS

SELECT	dbo.tblFields.IdModel, dbo.tblFields.IdField, dbo.tblFields.Code, dbo.tblFields.Name, dbo.tblPeerGroup.IdPeerGroup, dbo.tblPeerGroup.PeerGroupCode, 
        dbo.tblPeerGroup.PeerGroupDescription
FROM    dbo.tblPeerGroupField INNER JOIN
        dbo.tblPeerGroup ON dbo.tblPeerGroupField.IdPeerGroup = dbo.tblPeerGroup.IdPeerGroup INNER JOIN
        dbo.tblFields ON dbo.tblPeerGroupField.IdField = dbo.tblFields.IdField
GO

/****** Object:  View [dbo].[vListExportAggregationLevels]    Script Date: 05/27/2016 13:49:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vListExportAggregationLevels]
AS
SELECT	A.IdModel, A.Code, A.Name, ISNULL(B.Code, '') AS ParentCode, A.RelationLevel
FROM    dbo.tblAggregationLevels AS A LEFT OUTER JOIN
        dbo.tblAggregationLevels AS B ON B.IdAggregationLevel = A.Parent

GO

/****** Object:  View [dbo].[vListTypePriceEconomicWithChildren]    Script Date: 06/28/2016 11:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vListTypePriceEconomicWithChildren]
AS
SELECT DISTINCT 
                      TOP (100) PERCENT dbo.tblTypePriceEconomic.IdTypePriceEconomic, dbo.tblZiffAccounts.IdZiffAccount, dbo.tblZiffAccounts.IdModel, 
                      dbo.tblTypePriceEconomic.ProjectionCriteria, dbo.tblZiffAccounts.Code AS CodeZiffAccount, dbo.tblZiffAccounts.Name AS NameZiffAccount, dbo.tblZiffAccounts.Parent, 
                      dbo.tblZiffAccounts.SortOrder
FROM         dbo.tblZiffAccounts LEFT OUTER JOIN
                      dbo.tblTypePriceEconomic ON dbo.tblZiffAccounts.IdZiffAccount = dbo.tblTypePriceEconomic.IdZiffAccount LEFT OUTER JOIN
                          (SELECT     IdParameters, Type, Code, Name, IdLanguage
                            FROM          dbo.tblParameters
                            WHERE      (Type = 'EconomicProjectionCriteria')) AS P ON P.Code = dbo.tblTypePriceEconomic.ProjectionCriteria
ORDER BY dbo.tblZiffAccounts.SortOrder

GO

/*************************/
/*** STORED PROCEDURES ***/
/*************************/

/*** NEW ***/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[parpIsCaseSelectionChecked]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[parpIsCaseSelectionChecked]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-----------------------------------------------------------------------------------------
Autor   		: Rodrigo Manubens
Fecha Creacion	: 12/03/2015
Descripcion     : Check for CaseSelection field selected for a given field's baseline(s)
===========================================================================================
Modifications:	
Date		Author	Description
---------	------	-----------------------------------------------------------------------

-------------------------------------------------------------------------------------------*/
CREATE PROC [dbo].[parpIsCaseSelectionChecked](
@IdModel int,
@IdField int)
AS
BEGIN
	SELECT * FROM [dbo].tblProjects
	 WHERE IdModel = @IdModel
	   AND IdField = @IdField
	   AND Operation = 1 -- Baseline = 1
	   AND CaseSelection=1 -- True = 1
END  

GO

/****** Object:  StoredProcedure [dbo].[allpInsertPeerGroup]    Script Date: 03/23/2016 10:28:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[allpInsertPeerGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[allpInsertPeerGroup]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------
Autor   		: Rodrigo Manubens
Fecha Creacion	: 12/07/2015
Descripcion     : Add a record to table tblPeerGroups
===========================================================================================
Modifications:	
Date		Author	Description
---------	------	-----------------------------------------------------------------------

-------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[allpInsertPeerGroup](
@IdPeerGroup numeric output,
@IdModel int,
@PeerGroupCode varchar(20),
@PeerGroupdescription varchar(100),
@FullBenchmark bit,
@Comments varchar(250))
AS
BEGIN

	INSERT INTO [tblPeerGroup] ([IdModel],[PeerGroupCode],[PeerGroupDescription],[FullBenchmark],[Comments])
    VALUES (@IdModel,@PeerGroupCode,@PeerGroupDescription,@FullBenchmark,@Comments);
     
	SET @IdPeerGroup = SCOPE_IDENTITY();
	
END

GO

/****** Object:  StoredProcedure [dbo].[allpDeletePeerGroup]    Script Date: 03/23/2016 10:28:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[allpDeletePeerGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[allpDeletePeerGroup]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------
Autor   		: Rodrigo Manubens
Fecha Creacion	: 12/07/2015
Descripcion     : Delete a record from table tblPeerGroup
===========================================================================================
Modifications:	
Date		Author	Description
---------	------	-----------------------------------------------------------------------

-------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[allpDeletePeerGroup](
@IdPeerGroup int
)
AS
BEGIN

	DELETE FROM [tblPeerGroup] WHERE IdPeerGroup = @IdPeerGroup;
	
END

GO

/****** Object:  StoredProcedure [dbo].[allpUpdatePeerGroup]    Script Date: 03/23/2016 10:28:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[allpUpdatePeerGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[allpUpdatePeerGroup]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------
Autor   		: Rodrigo Manubens
Fecha Creacion	: 12/07/2015
Descripcion     : Update a record in table tblPeerGroup
===========================================================================================
Modifications:	
Date		Author	Description
---------	------	-----------------------------------------------------------------------

-------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[allpUpdatePeerGroup](
@IdPeerGroup int,
@IdModel int,
@PeerGroupCode varchar(20),
@PeerGroupdescription varchar(100),
@FullBenchmark bit,
@Comments varchar(250))
AS
BEGIN

	UPDATE [tblPeerGroup] SET [PeerGroupCode] = @PeerGroupCode,[PeerGroupDescription] = @PeerGroupDescription,[FullBenchmark] = @FullBenchmark,[Comments] = @Comments
    WHERE IdPeerGroup = @IdPeerGroup AND IdModel = @IdModel;
     
END

GO

/****** Object:  StoredProcedure [dbo].[allpInsertPeerGroupField]    Script Date: 03/23/2016 10:28:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[allpInsertPeerGroupField]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[allpInsertPeerGroupField]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------
Autor   		: Rodrigo Manubens
Fecha Creacion	: 12/07/2015
Descripcion     : Insert a record into table tblPeerGroupField
===========================================================================================
Modifications:	
Date		Author	Description
---------	------	-----------------------------------------------------------------------
07-29-2016	RM		Added code to add the peer group to accounts in table 
					tblCostStructureAssumptionsByProject that belong to that field
-------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[allpInsertPeerGroupField](
@IdPeerGroupField numeric output,
@IdPeerGroup int,
@IdField int)
AS
BEGIN

	INSERT INTO tblPeerGroupField (IdPeerGroup, IdField)
	VALUES (@IdPeerGroup, @IdField);

	UPDATE tblCostStructureAssumptionsByProject
	SET IdPeerGroup = @IdPeerGroup
	WHERE IdProject IN (SELECT IdProject FROM tblProjects WHERE IdField = @IdField) and Client <>0
	
	SET @IdPeerGroupField = SCOPE_IDENTITY();     
END

GO

/****** Object:  StoredProcedure [dbo].[allpDeletePeerGroupField]    Script Date: 03/23/2016 10:28:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[allpDeletePeerGroupField]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].allpDeletePeerGroupField
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------
Autor   		: Rodrigo Manubens
Fecha Creacion	: 12/07/2015
Descripcion     : Delete a record from table tblPeerGroupField
===========================================================================================
Modifications:	
Date		Author	Description
---------	------	-----------------------------------------------------------------------

-------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[allpDeletePeerGroupField](
@IdPeerGroupField int)
AS
BEGIN

	DELETE FROM tblPeerGroupField WHERE IdPeerGroupField = @IdPeerGroupField;
	
END

GO

/****** Object:  StoredProcedure [dbo].[allpDeletePeerGroupFieldAll]    Script Date: 03/23/2016 10:28:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[allpDeletePeerGroupFieldAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].allpDeletePeerGroupFieldAll
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------
Autor   		: Rodrigo Manubens
Fecha Creacion	: 12/07/2015
Descripcion     : Delete a record from table tblPeerGroupField
===========================================================================================
Modifications:	
Date		Author	Description
---------	------	-----------------------------------------------------------------------

-------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[allpDeletePeerGroupFieldAll](
@IdPeerGroup int)
AS
BEGIN

	DELETE FROM tblPeerGroupField WHERE IdPeerGroup = @IdPeerGroup;
	
END

GO

/****** Object:  StoredProcedure [dbo].[allpUpdatePeerGroupField]    Script Date: 03/23/2016 10:28:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[allpUpdatePeerGroupField]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].allpUpdatePeerGroupField
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------
Autor   		: Rodrigo Manubens
Fecha Creacion	: 12/07/2015
Descripcion     : Update a record in table tblPeerGroupField
===========================================================================================
Modifications:	
Date		Author	Description
---------	------	-----------------------------------------------------------------------

-------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[allpUpdatePeerGroupField](
@IdPeerGroupField int,
@IdPeerGroup int,
@IdField int)
AS
BEGIN

	UPDATE tblPeerGroupField SET IdPeerGroup = @IdPeerGroup, IdField = @IdField
	WHERE IdPeerGroupField = @IdPeerGroupField;
	
END

GO

/****** Object:  StoredProcedure [dbo].[allpListPeerGroupField]    Script Date: 03/23/2016 10:28:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[allpListPeerGroupField]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].allpListPeerGroupField
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------------------------------
Autor   		: Rodrigo Manubens
Fecha Creacion	: 12/07/2015
Descripcion     : List records in table tblPeerGroupField
===========================================================================================
Modifications:	
Date		Author	Description
---------	------	-----------------------------------------------------------------------

-------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[allpListPeerGroupField](
@IdModel int,
@IdPeerGroup int)
AS
BEGIN	
	IF (@IdPeerGroup <>0)
		SELECT * FROM	
		(
			SELECT C.IdField, C.IdModel, C.Code, C.Name, MC.IdPeerGroupField, SC.IdPeerGroup, 'true' AS Selected
			FROM [dbo].[tblFields] C
				INNER JOIN [dbo].tblPeerGroupField MC ON MC.IdField = C.IdField
				INNER JOIN [dbo].[tblPeerGroup] SC ON SC.IdPeerGroup = MC.IdPeerGroup
			WHERE C.IdModel = @IdModel AND SC.IdPeerGroup = @IdPeerGroup
			
			UNION ALL
			
			SELECT C.IdField, C.IdModel, C.Code, C.Name, 0 AS IdPeerGroupField,  0 AS IdPeerGroup, 'false' AS Selected
			FROM [dbo].[tblFields] C
			WHERE C.IdModel = @IdModel 
			AND C.IdField NOT IN 
				(
					SELECT IdField
					FROM [dbo].tblPeerGroupField
					WHERE IdModel = @IdModel
					AND IdPeerGroup = @IdPeerGroup
					
				)
		) A
		ORDER BY A.Name
	ELSE
		SELECT * FROM	
		(
			SELECT C.IdField, C.IdModel, C.Code, C.Name, 0 AS IdPeerGroupField, 0 AS IdPeerGroup, 'false' AS Selected
			FROM [dbo].[tblFields] C
			WHERE C.IdModel = @IdModel
		) A
		ORDER BY A.Name	
END

GO

/****** Object:  StoredProcedure [dbo].[allpUpdateZiffBaseCostUnitCost]    Script Date: 03/23/2016 10:28:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[allpUpdateZiffBaseCostUnitCost]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].allpUpdateZiffBaseCostUnitCost
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================================
-- Author:Rodrigo Manubens		
-- Create date: 2016-01-15
-- Description:	Update record into tblZiffBaseCost for a given Peer Group and External Base 
--				Code
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
--						
-- ===========================================================================================
CREATE PROCEDURE [dbo].[allpUpdateZiffBaseCostUnitCost](
@IdModel INT,
@ExternalAccountCode VARCHAR(50),
@PeerGroupDescription VARCHAR(50),
@UnitCost FLOAT
)
AS
BEGIN
	UPDATE tblZiffBaseCost 
	SET tblZiffBaseCost.UnitCost = @UnitCost
	FROM tblZiffBaseCost 
	WHERE IdZiffBaseCost IN (SELECT IdZiffBaseCost 
							 FROM tblCalcExternalCostReferences 
							 WHERE IdModel = @IdModel 
							 AND ExternalAccountCode = @ExternalAccountCode 
							 AND PeerGroupDescription = @PeerGroupDescription)	
END

GO

/****** Object:  StoredProcedure [dbo].[parpCheckCurrencies]    Script Date: 03/23/2016 10:28:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[parpCheckCurrencies]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].parpCheckCurrencies
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-12-14
-- Description:	Code to check if a currency already exists in tblCurrencies
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- ===========================================================================================
CREATE PROCEDURE [dbo].[parpCheckCurrencies](
	@IdModel INT,
	@IdCurrency INT)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * 
	FROM tblModelsCurrencies
	WHERE IdCurrency = @IdCurrency
	AND IdModel= @IdModel
END
GO

/****** Object:  StoredProcedure [dbo].[parpInsertModelsPrices]    Script Date: 12/15/2015 13:18:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[parpInsertModelsPrices]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[parpInsertModelsPrices]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-12-15
-- Description:	Inserts a record into tblModelsPrices
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- ===========================================================================================
CREATE PROC [dbo].[parpInsertModelsPrices](
@IdModelPrice numeric output,
@IdModel int,
@IdCurrency int,
@IdField int,
@PriceYear int,
@IdTechnicalDriver int,
@IdPriceScenario int,
@PriceValue float,
@PriceOffset int)
AS
BEGIN
	
	IF (EXISTS(
		SELECT [IdModelPrice]
		FROM tblModelsPrices
		WHERE [IdModel]=@IdModel AND [IdCurrency]=@IdCurrency AND [IdField]=@IdField AND [PriceYear]=@PriceYear AND [IdTechnicalDriver]=@IdTechnicalDriver AND [IdPriceScenario]=@IdPriceScenario))
	BEGIN
		UPDATE tblModelsPrices SET [PriceValue]=@PriceValue, [PriceOffset]=@PriceOffset
		WHERE [IdModel]=@IdModel AND [IdCurrency]=@IdCurrency AND [IdField]=@IdField AND [PriceYear]=@PriceYear AND [IdTechnicalDriver]=@IdTechnicalDriver AND [IdPriceScenario]=@IdPriceScenario
	END
	ELSE
	BEGIN
		INSERT INTO tblModelsPrices ([IdModel],[IdCurrency],[IdField],[PriceYear],[IdTechnicalDriver],[IdPriceScenario],[PriceValue],[PriceOffset])
		VALUES (@IdModel,@IdCurrency,@IdField,@PriceYear,@IdTechnicalDriver,@IdPriceScenario,@PriceValue,@PriceOffset)
	END
	
	SET @IdModelPrice = SCOPE_IDENTITY();
	
END

GO

/****** Object:  StoredProcedure [dbo].[parpUpdateModelsPrices]    Script Date: 01/05/2016 16:10:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[parpUpdateModelsPrices]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[parpUpdateModelsPrices]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-12-15
-- Description:	Inserts a record into tblModelsPrices
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- ===========================================================================================
CREATE PROC [dbo].[parpUpdateModelsPrices](
@IdModelPrice int,
@PriceValue float,
@PriceOffset int)
AS 
BEGIN
  UPDATE tblModelsPrices SET [PriceValue]=@PriceValue, [PriceOffset]=@PriceOffset
      WHERE IdModelPrice =@IdModelPrice
END

GO

/****** Object:  StoredProcedure [dbo].[parpInsertPriceScenarios]    Script Date: 12/18/2015 14:30:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[parpInsertPriceScenarios]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[parpInsertPriceScenarios]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-12-18
-- Description:	Inserts a record into tblPriceScenarios
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- ===========================================================================================
Create PROC [dbo].[parpInsertPriceScenarios](
@IdPriceScenario numeric output,
@IdModel int,
@ScenarioName varchar(50))
AS
BEGIN
	
	INSERT INTO tblPriceScenarios ([IdModel],[ScenarioName])
	VALUES(@IdModel,@ScenarioName);
	
	SET @IdPriceScenario = SCOPE_IDENTITY();
	
END

GO

/****** Object:  StoredProcedure [dbo].[parpUpdatePriceScenarios]    Script Date: 12/18/2015 14:30:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[parpUpdatePriceScenarios]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[parpUpdatePriceScenarios]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-12-18
-- Description:	Updates a record in tblPriceScenarios
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- ===========================================================================================
Create PROC [dbo].[parpUpdatePriceScenarios](
@IdPriceScenario numeric output,
@IdModel int,
@ScenarioName varchar(50))
AS
BEGIN
	
	UPDATE tblPriceScenarios SET [ScenarioName]=@ScenarioName
	WHERE IdPriceScenario=@IdPriceScenario
	
	SET @IdPriceScenario = SCOPE_IDENTITY();
	
END

GO

/****** Object:  StoredProcedure [dbo].[parpDeletePriceScenarios]    Script Date: 12/18/2015 14:30:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[parpDeletePriceScenarios]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[parpDeletePriceScenarios]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-12-18
-- Description:	Deletes a record from tblPriceScenarios
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- ===========================================================================================
Create PROC [dbo].[parpDeletePriceScenarios](
@IdPriceScenario int
)
AS
BEGIN
	
	DELETE tblPriceScenarios WHERE IdPriceScenario=@IdPriceScenario
	
END

GO

/****** Object:  StoredProcedure [dbo].[allpListPeerCriteria]    Script Date: 03/23/2016 10:28:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[allpListPeerCriteria]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].allpListPeerCriteria
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2016-01-15
-- Description:	List of Peer Criteria for External Base Cost References
-- ================================================================================================
-- Modification:	
-- ================================================================================================

CREATE PROCEDURE [dbo].[allpListPeerCriteria](
@IdModel INT,
@IdPeerGroup INT
)
AS
BEGIN
	--SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	DECLARE @SQL NVARCHAR(MAX);
	DECLARE @InnerIdPeerGroup NVARCHAR(50)
	IF (@IdPeerGroup=0)
		SELECT @InnerIdPeerGroup = COALESCE(@InnerIdPeerGroup + ', ', '') + PeerGroupDescription FROM tblPeerGroup WHERE IdModel=@IdModel
	ELSE
		SELECT @InnerIdPeerGroup = PeerGroupDescription FROM tblPeerGroup WHERE IdModel=@IdModel AND IdPeerGroup=@IdPeerGroup
		
	/** Generate Peer Group columns to be used for output of data **/
	DECLARE @PeerGroupsList NVARCHAR(MAX)='';
	DECLARE @PeerGroupName NVARCHAR(255);
	IF (@IdPeerGroup=0)
		DECLARE PeerGroupsList CURSOR FOR
		SELECT PeerGroupCode FROM tblPeerGroup WHERE IdModel=@IdModel;
	ELSE
		DECLARE PeerGroupsList CURSOR FOR
		SELECT PeerGroupCode FROM tblPeerGroup WHERE IdModel=@IdModel AND IdPeerGroup=@IdPeerGroup;

	OPEN PeerGroupsList;
	FETCH NEXT FROM PeerGroupsList INTO @PeerGroupName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @PeerGroupsList=''
		BEGIN
			SET @PeerGroupsList =  @PeerGroupsList + '[' + @PeerGroupName + ']';
		END
		ELSE
		BEGIN
			SET @PeerGroupsList =  @PeerGroupsList + ',[' + @PeerGroupName + ']';
		END
		FETCH NEXT FROM PeerGroupsList INTO @PeerGroupName;
	END

	CLOSE PeerGroupsList;
	DEALLOCATE PeerGroupsList;

	IF OBJECT_ID('tempdb..#PeerCriteriaCost') IS NOT NULL DROP TABLE #PeerCriteriaCost	

	CREATE TABLE #PeerCriteriaCost (Quantity INT, PeerCriteria VARCHAR(50), Unit VARCHAR(20),PeerGroupDescription VARCHAR(50))
	IF (@IdPeerGroup=0)
		INSERT INTO #PeerCriteriaCost (Quantity,PeerCriteria,Unit,PeerGroupDescription)
		SELECT Quantity,PeerCriteria,Unit,PeerGroupCode
		FROM vListPeerCriteriaCost 
		WHERE IdModel=@IdModel 
		AND IdPeerGroup IN (SELECT IdPeerGroup FROM tblPeerGroup WHERE IdModel = @IdModel) 
		--GROUP BY PeerCriteria,Unit,PeerGroupDescription
	ELSE
		INSERT INTO #PeerCriteriaCost (Quantity,PeerCriteria,Unit,PeerGroupDescription)
		SELECT Quantity,PeerCriteria,Unit,PeerGroupCode
		FROM vListPeerCriteriaCost 
		WHERE IdModel=@IdModel 
		AND IdPeerGroup =@IdPeerGroup
		--GROUP BY PeerCriteria,Unit,PeerGroupDescription

	IF exists(SELECT 1 FROM #PeerCriteriaCost)
	BEGIN
		DECLARE @SQLOutput NVARCHAR(MAX);
		SET @SQLOutput = 'SELECT [PeerCriteria] AS "Peer Criteria", [Unit],' + @PeerGroupsList + ' FROM #PeerCriteriaCost PIVOT (SUM(Quantity) FOR PeerGroupDescription IN (' + @PeerGroupsList + ')) AS PivotTable ';
		EXEC(@SQLOutput);
	END	
	ELSE
	BEGIN
		SELECT * FROM #PeerCriteriaCost WHERE PeerCriteria = 'Nothing Found'
	END
END

GO

/****** Object:  StoredProcedure [dbo].[allpListExportExternalBaseCostReferences]    Script Date: 03/23/2016 10:28:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[allpListExportExternalBaseCostReferences]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].allpListExportExternalBaseCostReferences
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================ 
-- Author:		Rodrigo Manubens
-- Create date: 2016-01-18
-- Description:	Cretaes a list of external base cost references for the purpose of creating export
--				file
-- ================================================================================================ 
-- Modifications:	
-- Date			Author	Description
-- ----------	------	----------------------------------------------------------------------
-- 
-- ================================================================================================ 
CREATE PROCEDURE [dbo].[allpListExportExternalBaseCostReferences](
@IdModel INT,
@IdPeerGroup INT
)
AS
BEGIN
	--SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (@IdPeerGroup=0)
	BEGIN
		SELECT	tblPeerGroup.PeerGroupCode, CodeZiff AS 'Cost Category Code', UnitCost 
		FROM	tblCalcBaseCostByFieldZiff INNER JOIN
				tblPeerGroup ON tblCalcBaseCostByFieldZiff.IdPeerGroup = tblPeerGroup.IdPeerGroup
		WHERE	tblCalcBaseCostByFieldZiff.IdModel = @IdModel 
		AND		tblCalcBaseCostByFieldZiff.IdPeerGroup IN (SELECT IdPeerGroup FROM tblPeerGroup WHERE IdModel = @IdModel)
		AND		UnitCost IS NOT NULL 
		GROUP BY tblPeerGroup.PeerGroupCode, CodeZiff, UnitCost
	END
	ELSE
	BEGIN
		SELECT	tblPeerGroup.PeerGroupCode, CodeZiff AS 'Cost Category Code', UnitCost 
		FROM	tblCalcBaseCostByFieldZiff INNER JOIN
				tblPeerGroup ON tblCalcBaseCostByFieldZiff.IdPeerGroup = tblPeerGroup.IdPeerGroup
		WHERE	tblCalcBaseCostByFieldZiff.IdModel = @IdModel 
		AND		tblCalcBaseCostByFieldZiff.IdPeerGroup = @IdPeerGroup
		AND		UnitCost IS NOT NULL 
		GROUP BY tblPeerGroup.PeerGroupCode, CodeZiff, UnitCost
	END
END

GO

/****** Object:  StoredProcedure [dbo].[tecpListExportTechnicalAssumptions]    Script Date: 03/23/2016 10:28:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tecpListExportTechnicalAssumptions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].tecpListExportTechnicalAssumptions
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================ 
-- Author:		Rodrigo Manubens
-- Create date: 2016-01-18
-- Description:	Creates a list of technical assumptions for the purpose of creating export file
-- ================================================================================================ 
-- Modifications:	
-- Date			Author	Description
-- ----------	------	----------------------------------------------------------------------
-- 
-- ================================================================================================ 
CREATE PROCEDURE [dbo].[tecpListExportTechnicalAssumptions](
@IdModel INT,
@IdTypeOperation INT
)
AS
BEGIN
	--SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT CASE WHEN tTA.IdTypeOperation = 0 THEN 'All' ELSE tTO.Name END AS 'Type of Operation', tZA.Code AS 'Cost Category', tP.Name AS 'Projection Criteria', 
			CASE WHEN tTA.IdTechnicalDriverSize = 0 THEN '' ELSE tTDS.Name END AS 'Size Drivers', CASE WHEN tTA.IdTechnicalDriverPerformance = 0 THEN '' ELSE tTDP.Name END AS 'Performance Drivers'
			,tTA.CycleValue AS 'Cycle Term'
	FROM tblTechnicalAssumption tTA LEFT OUTER JOIN tblTypesOperation tTO ON
			tTA.IdTypeOperation = tTO.IdTypeOperation INNER JOIN tblZiffAccounts tZA ON
			tTA.IdZiffAccount = tZA.IdZiffAccount INNER JOIN tblParameters tP ON
			tTA.ProjectionCriteria = tP.Code AND tp.Type = 'ProjectionCriteria' AND tp.IdLanguage = 1 LEFT OUTER JOIN tblTechnicalDrivers tTDS ON
			tTA.IdTechnicalDriverSize=tTDS.IdTechnicalDriver AND tTDS.TypeDriver=1  LEFT OUTER JOIN tblTechnicalDrivers tTDP ON
			tTA.IdTechnicalDriverPerformance=tTDP.IdTechnicalDriver AND tTDP.TypeDriver=2
	WHERE tTA.IdModel = @IdModel
	AND   tTA.IdTypeOperation = @IdTypeOperation
	ORDER BY 2

END

GO

/****** Object:  StoredProcedure [dbo].[allpInsertFilesToRepository]    Script Date: 06/1/2016 14:27:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[allpInsertFilesToRepository]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[allpInsertFilesToRepository]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*---------------------------------------------------------------
Autor   		: Rodrigo Manubens
Fecha Creacion	: 06/01/2016
Descripcion     : Add files to tblFilesRepository
---------------------------------------------------------------*/
CREATE PROC [dbo].[allpInsertFilesToRepository](
@IdFileRepository numeric output,
@IdModel int,
@AttachFile varbinary(Max),
@FileName varchar(200),
@UserCreation int,
@DateCreation datetime,
@UserModification int,
@DateModification datetime,
@Comments varchar(250))
AS
BEGIN
	
	INSERT INTO tblFilesRepository ([IdModel],[AttachFile],[FileName],[UserCreation],[DateCreation],[UserModification],[DateModification],[Comments])
	VALUES(@IdModel,@AttachFile,@FileName,@UserCreation,@DateCreation,@UserModification,@DateModification,@Comments);
	
	SET @IdFileRepository = SCOPE_IDENTITY();
	
END

GO

/****** Object:  StoredProcedure [dbo].[allpUpdateFilesInRepository]    Script Date: 06/01/2016 09:53:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[allpUpdateFilesInRepository]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[allpUpdateFilesInRepository]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------
Autor   		: Rodrigo Manubens
Fecha Creacion	: 06/01/2016
Descripcion     : Update file in table tblFilesRepository
-----------------------------------------------------------------*/
CREATE PROC [dbo].[allpUpdateFilesInRepository](
@IdFileRepository numeric output,
@IdModel int,
@AttachFile varbinary(Max),
@FileName varchar(200),
@UserCreation int,
@DateCreation datetime,
@UserModification int,
@DateModification datetime,
@Comments varchar(250))
AS 
BEGIN
  UPDATE tblFilesRepository SET IdModel=@IdModel, AttachFile=@AttachFile, [FileName]=@FileName, UserCreation=@UserCreation, DateCreation=@DateCreation, UserModification=@UserModification, DateModification=@DateModification, Comments = @Comments
      WHERE IdFileRepository =@IdFileRepository
END

GO

/****** Object:  StoredProcedure [dbo].[allpDeleteFilesInRepository]    Script Date: 06/01/2016 09:57:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[allpDeleteFilesInRepository]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[allpDeleteFilesInRepository]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------
Autor   		: Rodrigo Manubens
Fecha Creacion	: 06/01/2016
Descripcion     : Delete file in table tblFilesRepository
-----------------------------------------------------------------*/
CREATE PROC [dbo].[allpDeleteFilesInRepository](
@IdFileRepository int
)
AS 
BEGIN 
   DELETE FROM tblFilesRepository WHERE IdFileRepository =@IdFileRepository
END

GO

/****** Object:  StoredProcedure [dbo].[parpDeleteModelsCurrenciesAll]    Script Date: 06/03/2016 14:29:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[parpDeleteModelsCurrenciesAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[parpDeleteModelsCurrenciesAll]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*--------------------------------------------------------------------------------------
Autor   		: Rodrigo Manubens
Fecha Creacion	: 06/03/2016
Descripcion     : Delete all currencies for a given model
---------------------------------------------------------------------------------------*/
CREATE PROC [dbo].[parpDeleteModelsCurrenciesAll](
@IdModel int
)
AS 
BEGIN 
   DELETE FROM tblModelsCurrencies   WHERE IdModel = @IdModel
END

GO

/*** ALTER ***/

/****** Object:  StoredProcedure [dbo].[allpInsertZiffBaseCost]    Script Date: 02/02/2016 09:11:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:		
-- Create date: 
-- Description:	Insert record into tblZiffBaseCost
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 12/10/15		RM		Changed @IdField as it now uses peer group so new input field is 
--						@IdPeerGroup
-- ===========================================================================================
ALTER PROCEDURE [dbo].[allpInsertZiffBaseCost](
@IdZiffBaseCost numeric output,
@IdPeerGroup int,
@IdZiffAccount int,
@IdPeerCriteria int,
@UnitCost float,
@Quantity float,
@TotalCost float)
AS
BEGIN

	DECLARE @IdField int
	DECLARE	CurPeerGroupFields CURSOR FOR
		SELECT tblPeerGroupField.IdField
		FROM tblPeerCriteriaCost INNER JOIN
			tblPeerGroup ON tblPeerCriteriaCost.IdPeerGroup = tblPeerGroup.IdPeerGroup INNER JOIN
			tblPeerGroupField ON tblPeerGroup.IdPeerGroup = tblPeerGroupField.IdPeerGroup
		WHERE tblPeerGroup.IdPeerGroup = @IdPeerGroup
		GROUP BY tblPeerGroupField.IdField
	OPEN CurPeerGroupFields
	
	FETCH NEXT FROM CurPeerGroupFields INTO @IdField
	
	WHILE @@Fetch_status = 0
	BEGIN
		IF (EXISTS (
			SELECT [Quantity]
			FROM tblZiffBaseCost
			WHERE [IdField]=@IdField AND [IdZiffAccount]=@IdZiffAccount AND [IdPeerCriteria]=@IdPeerCriteria AND [IdPeerGroup]=@IdPeerGroup))
		BEGIN
			UPDATE tblZiffBaseCost SET [UnitCost]= @UnitCost
			WHERE [IdField]=@IdField AND [IdZiffAccount]=@IdZiffAccount AND [IdPeerCriteria]=@IdPeerCriteria AND [IdPeerGroup]=@IdPeerGroup;
		END
		ELSE
		BEGIN
			INSERT INTO tblZiffBaseCost ([IdField],[IdZiffAccount],[IdPeerCriteria],[UnitCost],[Quantity],[TotalCost],[IdPeerGroup],[QuantityNormal],[QuantityPessimistic],[QuantityOptimistic],[TotalCostNormal],[TotalCostPessimistic],[TotalCostOptimistic])
			VALUES(@IdField,@IdZiffAccount,@IdPeerCriteria,@UnitCost,@Quantity,@TotalCost,@IdPeerGroup,0.0,0.0,0.0,0.0,0.0,0.0);
			
			SET @IdZiffBaseCost = SCOPE_IDENTITY();
		END
		
		FETCH NEXT FROM CurPeerGroupFields INTO @IdField
	END
	CLOSE CurPeerGroupFields
	DEALLOCATE CurPeerGroupFields
	
END

GO

/****** Object:  StoredProcedure [dbo].[allpUpdateZiffBaseCost]    Script Date: 12/14/2015 15:11:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================================
-- Author:		
-- Create date: 
-- Description:	Update existing record in tblZiffBaseCost
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 12/14/15		RM		Changed @IdField as it now uses peer group so new input field is 
--						@IdPeerGroup
-- ===========================================================================================
ALTER PROCEDURE [dbo].[allpUpdateZiffBaseCost](
@IdZiffBaseCost int,
@IdPeerGroup int,
@IdZiffAccount int,
@IdPeerCriteria int,
@UnitCost float,
@Quantity float,
@TotalCost float)
AS
BEGIN

	DECLARE @IdField int

	DECLARE	CurPeerGroupFields CURSOR FOR
		SELECT tblPeerGroupField.IdField
		FROM tblPeerCriteriaCost INNER JOIN
			tblPeerGroup ON tblPeerCriteriaCost.IdPeerGroup = tblPeerGroup.IdPeerGroup INNER JOIN
			tblPeerGroupField ON tblPeerGroup.IdPeerGroup = tblPeerGroupField.IdPeerGroup
		WHERE tblPeerGroup.IdPeerGroup = @IdPeerGroup
		GROUP BY tblPeerGroupField.IdField
	OPEN CurPeerGroupFields
	
	FETCH NEXT FROM CurPeerGroupFields INTO @IdField
	
	WHILE @@Fetch_status = 0
	BEGIN
		IF (EXISTS(
			SELECT [Quantity]
			FROM tblZiffBaseCost
			WHERE [IdField]=@IdField AND [IdZiffAccount]=@IdZiffAccount AND [IdPeerCriteria]=@IdPeerCriteria AND [IdPeerGroup]=@IdPeerGroup))
		BEGIN
			UPDATE tblZiffBaseCost SET [UnitCost]= @UnitCost
			WHERE [IdField]=@IdField AND [IdZiffAccount]=@IdZiffAccount AND [IdPeerCriteria]=@IdPeerCriteria AND [IdPeerGroup]=@IdPeerGroup;
		END
		ELSE
		BEGIN
			INSERT INTO tblZiffBaseCost ([IdField],[IdZiffAccount],[IdPeerCriteria],[UnitCost],[Quantity],[TotalCost],[IdPeerGroup])
			VALUES(@IdField,@IdZiffAccount,@IdPeerCriteria,@UnitCost,@Quantity,@TotalCost,@IdPeerGroup);
		END
		
		FETCH NEXT FROM CurPeerGroupFields INTO @IdField
	END
	CLOSE CurPeerGroupFields
	DEALLOCATE CurPeerGroupFields
    	
END

GO

/****** Object:  StoredProcedure [dbo].[allpListExternalBaseCostReferences]    Script Date: 01/13/2016 13:14:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-06-17
-- Description:	List of External Base Cost References
-- ================================================================================================
-- Modification:	Added field ExternalAccount code for export of data
--					Replaced @IdIdField with @IdPeerGroup, subsequent select to get fields in Peer 
--					Group.
-- ================================================================================================

ALTER PROCEDURE [dbo].[allpListExternalBaseCostReferences](
@IdModel INT,
@IdPeerGroup INT
)
AS
BEGIN
	--SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @tblZiffBaseCostCount INT
	IF (@IdPeerGroup!=0)
		SELECT @tblZiffBaseCostCount = COUNT(*) FROM tblZiffBaseCost WHERE IdPeerGroup = @IdPeerGroup
	ELSE
		SELECT @tblZiffBaseCostCount = COUNT(*) FROM tblZiffBaseCost WHERE IdPeerGroup IN (SELECT IdPeerGroup FROM tblPeerGroup WHERE IdModel = @IdModel)

	DECLARE @SQL NVARCHAR(MAX);
	DECLARE @InnerIdPeerGroup NVARCHAR(50)
	IF (@IdPeerGroup=0)
		SELECT @InnerIdPeerGroup = COALESCE(@InnerIdPeerGroup + ', ', '') + CAST(IdPeerGroup AS NVARCHAR(50)) FROM tblPeerGroup WHERE IdModel=@IdModel
	ELSE
		SELECT @InnerIdPeerGroup = IdPeerGroup FROM tblPeerGroup WHERE IdModel=@IdModel AND IdPeerGroup=@IdPeerGroup

    -- Insert statements for procedure here
   	IF OBJECT_ID('tempdb..#ExternalCostReferences') IS NOT NULL DROP TABLE #ExternalCostReferences	
   	IF OBJECT_ID('tempdb..#ExternalCostReferencesFiller') IS NOT NULL DROP TABLE #ExternalCostReferencesFiller

	/** Cleanup Calc Tables for New Data **/
	DELETE FROM tblCalcExternalCostReferences WHERE IdModel=@IdModel
	
	/** Generate Peer Group columns to be used for output of data **/
	DECLARE @PeerGroupsList NVARCHAR(MAX)='';
	DECLARE @PeerGroupName NVARCHAR(255);
	IF (@IdPeerGroup=0)
		DECLARE PeerGroupsList CURSOR FOR
		SELECT PeerGroupCode FROM tblPeerGroup WHERE IdModel=@IdModel;
	ELSE
		DECLARE PeerGroupsList CURSOR FOR
		SELECT PeerGroupCode FROM tblPeerGroup WHERE IdModel=@IdModel AND IdPeerGroup=@IdPeerGroup;

	OPEN PeerGroupsList;
	FETCH NEXT FROM PeerGroupsList INTO @PeerGroupName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @PeerGroupsList=''
		BEGIN
			SET @PeerGroupsList =  @PeerGroupsList + '[' + @PeerGroupName + ']';
		END
		ELSE
		BEGIN
			SET @PeerGroupsList =  @PeerGroupsList + ',[' + @PeerGroupName + ']';
		END
		FETCH NEXT FROM PeerGroupsList INTO @PeerGroupName;
	END

	CLOSE PeerGroupsList;
	DEALLOCATE PeerGroupsList;

	/** Create a FILLER Table to add cost categories that have not been imported **/
	
	CREATE TABLE #ExternalCostReferencesFiller (IdModel INT,IdZiffAccount INT,PeerGroupDescription VARCHAR(100),CodeZiff VARCHAR(50),ZiffAccount VARCHAR(150),UnitCost FLOAT, SortOrder VARCHAR(20))	
	INSERT INTO #ExternalCostReferencesFiller (IdModel,IdZiffAccount,PeerGroupDescription,CodeZiff,ZiffAccount,UnitCost,SortOrder)
	SELECT CBCBFZ.IdModel,CBCBFZ.IdZiffAccount,NULL,'&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;'+CBCBFZ.CodeZiff as CodeZiff,CBCBFZ.ZiffAccount,0,CBCBFZ.RootCodeZiff+'_'+CBCBFZ.CodeZiff
	FROM tblCalcBaseCostByFieldZiff CBCBFZ left outer join tblZiffAccounts ZA on 
		 ZA.IdModel=CBCBFZ.IdModel and ZA.IdZiffAccount=CBCBFZ.IdZiffAccount 
	WHERE CBCBFZ.IdModel = @IdModel
	UNION 
	SELECT IdModel,IdZiffAccount,0,Code,Name,NULL,SortOrder
	FROM tblZiffAccounts 
	WHERE IdModel = @IdModel 
	AND Parent IS NULL
	ORDER BY 7 

	CREATE TABLE #ExternalCostReferences (IdModel INT,IdZiffAccount INT,PeerGroupDescription VARCHAR(100),CodeZiff VARCHAR(50),ZiffAccount VARCHAR(150),UnitCost FLOAT, SortOrder VARCHAR(20))	
		
	IF (@tblZiffBaseCostCount = 0)
	BEGIN
		INSERT INTO #ExternalCostReferences (IdModel,IdZiffAccount,PeerGroupDescription,CodeZiff,ZiffAccount,UnitCost,SortOrder)
		SELECT CBCBFZ.IdModel,CBCBFZ.IdZiffAccount,NULL,'&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;'+CBCBFZ.CodeZiff as CodeZiff,CBCBFZ.ZiffAccount,CBCBFZ.UnitCost,CBCBFZ.RootCodeZiff+'_'+CBCBFZ.CodeZiff
		FROM tblCalcBaseCostByFieldZiff CBCBFZ left outer join tblZiffAccounts ZA on 
			 ZA.IdModel=CBCBFZ.IdModel and ZA.IdZiffAccount=CBCBFZ.IdZiffAccount 
		WHERE CBCBFZ.IdModel = @IdModel
		UNION 
		SELECT IdModel,IdZiffAccount,0,Code,Name,NULL,SortOrder
		FROM tblZiffAccounts 
		WHERE IdModel = @IdModel 
		AND Parent IS NULL
		ORDER BY 7 
	END
	ELSE
	BEGIN
		INSERT INTO tblCalcExternalCostReferences (IdModel,IdZiffBaseCost,IdZiffAccount,IdPeerGroup,IdPeerCriteria,UnitCost,Quantity,TotalCost,CodeZiff,ZiffAccount,IdRootZiffAccount,
											 RootCodeZiff,RootZiffAccount,PeerCriteria,Unit,IdUnitPeerCriteria,TechApplicable,TFNormal,TFPessimistic,TFOptimistic,BCRNormal,
											 BCRPessimistic,BCROptimistic, SortOrder, ExternalAccountCode, PeerGroupDescription)	
		SELECT CBCBFZ.IdModel,CBCBFZ.IdZiffBaseCost,CBCBFZ.IdZiffAccount,CBCBFZ.IdPeerGroup,CBCBFZ.IdPeerCriteria,CBCBFZ.UnitCost,CBCBFZ.Quantity,CBCBFZ.TotalCost,
			   '&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;'+CBCBFZ.CodeZiff as CodeZiff,CBCBFZ.ZiffAccount,CBCBFZ.IdRootZiffAccount,CBCBFZ.RootCodeZiff,CBCBFZ.RootZiffAccount,
			   CBCBFZ.PeerCriteria,CBCBFZ.Unit,CBCBFZ.IdUnitPeerCriteria,CBCBFZ.TechApplicable,CBCBFZ.TFNormal,CBCBFZ.TFPessimistic,CBCBFZ.TFOptimistic,CBCBFZ.BCRNormal,
			   CBCBFZ.BCRPessimistic,CBCBFZ.BCROptimistic,ZA.SortOrder, CBCBFZ.CodeZiff AS ExternalAccountCode, PG.PeerGroupCode
		FROM tblCalcBaseCostByFieldZiff CBCBFZ LEFT OUTER JOIN tblZiffAccounts ZA on 
			 ZA.IdModel=CBCBFZ.IdModel and ZA.IdZiffAccount=CBCBFZ.IdZiffAccount INNER JOIN tblPeerGroup PG ON
			 CBCBFZ.IdPeerGroup = PG.IdPeerGroup
		WHERE CBCBFZ.IdModel = @IdModel AND CBCBFZ.IdField IN (SELECT tblPeerGroupField.IdField
																FROM tblPeerCriteriaCost INNER JOIN
																	tblPeerGroup ON tblPeerCriteriaCost.IdPeerGroup = tblPeerGroup.IdPeerGroup INNER JOIN
																	tblPeerGroupField ON tblPeerGroup.IdPeerGroup = tblPeerGroupField.IdPeerGroup
																GROUP BY tblPeerGroupField.IdField) 

		IF (@IdPeerGroup=0)
		BEGIN
			SET @SQL = 'INSERT INTO #ExternalCostReferences (IdModel,IdZiffAccount,PeerGroupDescription,CodeZiff,ZiffAccount,UnitCost,SortOrder)	
			SELECT IdModel,IdZiffAccount,PeerGroupDescription,CodeZiff,ZiffAccount,UnitCost,SortOrder
			FROM tblCalcExternalCostReferences
			WHERE IdModel= ' + cast(@IdModel AS NVARCHAR(MAX)) + '
			AND IdPeerGroup IN (' + @InnerIdPeerGroup + ')
			GROUP BY IdModel,IdZiffAccount,PeerGroupDescription,CodeZiff,ZiffAccount,UnitCost,SortOrder
			UNION 
			SELECT ZA.IdModel,ZA.IdZiffAccount,A.PeerGroupCode,ZA.Code,ZA.Name,SUM(A.UnitCost),SortOrder
			FROM tblZiffAccounts ZA INNER JOIN
			(SELECT CBC.RootCodeZiff,CBC.UnitCost, PG.PeerGroupCode 
			 FROM tblCalcBaseCostByFieldZiff CBC INNER JOIN tblPeerGroup PG 
			 ON CBC.IdPeerGroup=PG.IdPeerGroup 
			 WHERE CBC.IdModel = ' + cast(@IdModel AS NVARCHAR(MAX)) + '
			 AND CBC.IdPeerGroup  IN (' + @InnerIdPeerGroup + ')
			 GROUP BY CBC.RootCodeZiff,CBC.UnitCost,PG.PeerGroupCode) AS A 
			ON ZA.Code = A.RootCodeZiff
			WHERE ZA.IdModel = ' + cast(@IdModel AS NVARCHAR(MAX)) + '
			AND ZA.Parent IS NULL
			GROUP BY ZA.IdModel,ZA.IdZiffAccount,A.PeerGroupCode,ZA.Code,ZA.Name,ZA.SortOrder'
		END
		ELSE
		BEGIN
			SET @SQL = 'INSERT INTO #ExternalCostReferences (IdModel,IdZiffAccount,PeerGroupDescription,CodeZiff,ZiffAccount,UnitCost,SortOrder)	
			SELECT IdModel,IdZiffAccount,PeerGroupDescription,CodeZiff,ZiffAccount,UnitCost,SortOrder
			FROM tblCalcExternalCostReferences
			WHERE IdModel=' + cast(@IdModel AS NVARCHAR(MAX)) + '
			AND   IdPeerGroup=' + cast(@IdPeerGroup AS NVARCHAR(MAX)) + '
			GROUP BY IdModel,IdZiffAccount,PeerGroupDescription,CodeZiff,ZiffAccount,UnitCost,SortOrder
			UNION 
			SELECT ZA.IdModel,ZA.IdZiffAccount,A.PeerGroupCode,ZA.Code,ZA.Name,SUM(A.UnitCost),ZA.SortOrder
			FROM tblZiffAccounts ZA INNER JOIN
			(SELECT CBC.RootCodeZiff,CBC.UnitCost, PG.PeerGroupCode 
			 FROM tblCalcBaseCostByFieldZiff CBC INNER JOIN tblPeerGroup PG 
			 ON CBC.IdPeerGroup=PG.IdPeerGroup 
			 WHERE CBC.IdModel = ' + cast(@IdModel AS NVARCHAR(MAX)) + '
			 AND CBC.IdPeerGroup = ' + cast(@IdPeerGroup AS NVARCHAR(MAX)) + ' 
			 GROUP BY CBC.RootCodeZiff,CBC.UnitCost,PG.PeerGroupCode) AS A 
			ON ZA.Code = A.RootCodeZiff
			WHERE ZA.IdModel = ' + cast(@IdModel AS NVARCHAR(MAX)) + '
			AND ZA.Parent IS NULL
			GROUP BY ZA.IdModel,ZA.IdZiffAccount,A.PeerGroupCode,ZA.Code,ZA.Name,ZA.SortOrder'
		END
		EXEC(@SQL)
	END

	/** Add any missing Cost Categoriess to #ExternalCostReferences to fill properly for display **/
	INSERT INTO #ExternalCostReferences (IdModel,IdZiffAccount,PeerGroupDescription,CodeZiff,ZiffAccount,UnitCost,SortOrder)
	SELECT ECRF.IdModel,ECRF.IdZiffAccount,ECRF.PeerGroupDescription,ECRF.CodeZiff,ECRF.ZiffAccount, ECRF.UnitCost,ECRF.SortOrder
	FROM	#ExternalCostReferencesFiller ECRF LEFT OUTER JOIN
			#ExternalCostReferences ECR ON ECRF.IdModel = ECR.IdModel AND ECRF.IdZiffAccount = ECR.IdZiffAccount
	WHERE	ECR.UnitCost IS NULL

	/** Add SubTotal **/
	SET @SQL = 'INSERT INTO #ExternalCostReferences (IdModel,IdZiffAccount,PeerGroupDescription,CodeZiff,ZiffAccount,UnitCost,SortOrder)
	SELECT ZA.IdModel,NULL,A.PeerGroupCode,''Grand Total'',NULL,SUM(A.UnitCost),''ZZZ-1''
	FROM tblZiffAccounts ZA INNER JOIN
	(SELECT CBC.RootCodeZiff,CBC.UnitCost, PG.PeerGroupCode 
	 FROM tblCalcBaseCostByFieldZiff CBC INNER JOIN tblPeerGroup PG 
	 ON CBC.IdPeerGroup=PG.IdPeerGroup 
	 WHERE CBC.IdModel = ' + cast(@IdModel AS NVARCHAR(MAX)) + '
	 AND CBC.IdPeerGroup  IN (' + @InnerIdPeerGroup + ')
	 GROUP BY CBC.RootCodeZiff,CBC.UnitCost,PG.PeerGroupCode) AS A 
	ON ZA.Code = A.RootCodeZiff
	WHERE ZA.IdModel = ' + cast(@IdModel AS NVARCHAR(MAX)) + '
	AND ZA.Parent IS NULL
	GROUP BY ZA.IdModel,A.PeerGroupCode'

	EXEC(@SQL)

	DECLARE @SQLOutput NVARCHAR(MAX);
	SET @SQLOutput = 'SELECT [CodeZiff] AS Code, [ZiffAccount] AS Description,' + @PeerGroupsList + ' FROM #ExternalCostReferences PIVOT (SUM(UnitCost) FOR PeerGroupDescription IN (' + @PeerGroupsList + ')) AS PivotTable ORDER BY [SortOrder]';
	EXEC(@SQLOutput);
		
END

GO

/****** Object:  StoredProcedure [dbo].[parpListModelsCurrencies]    Script Date: 01/11/2016 14:41:40 ******/
-- ===========================================================================================
-- Author:		
-- Create date: 
-- Description:	List the currencies in a given model
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 2016-1-12	RM		Added a default currency when none exist in the model.
-- ===========================================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[parpListModelsCurrencies] 
    @IdModel INT
AS 
	DECLARE @TotalCurrencies INT = 0
	
	SELECT @TotalCurrencies = COUNT(*) FROM tblModelsCurrencies WHERE IdModel = @IdModel
	
	IF (@TotalCurrencies = 0)
	BEGIN
		INSERT [dbo].[tblModelsCurrencies]
           ([IdCurrency],[IdModel],[Value],[BaseCurrency],[Output],[UserCreation],[DateCreation],[UserModification],[DateModification])
        VALUES (17,@IdModel,1,1,1,1,GetDate(),NULL,NULL)
	END

	SELECT * FROM 
	(
		SELECT C.*
					, MC.IdModelCurrency
					, MC.IdModel
					, MC.Value
					, MC.Output
					, MC.BaseCurrency
					, MC.UserCreation
					, MC.DateCreation
					, MC.UserModification
					, MC.DateModification
				FROM [dbo].[tblCurrencies] C
					INNER JOIN [dbo].[tblModelsCurrencies] MC ON MC.IdCurrency = C.IdCurrency
				WHERE MC.IdModel = @IdModel

		) A
	ORDER BY A.Name
GO

/****** Object:  StoredProcedure [dbo].[calcModuleTechnicalBaseCostRate]    Script Date: 02/02/2016 09:12:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:		Gareth Slater
-- Create date: 2014-04-20
-- Description:	Module Technical Base Cost Rate
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 2016-02-02	RM		Added a peer group id to differentiate external data as well as the 
--						retrieval of external data					
-- ===========================================================================================
ALTER PROCEDURE [dbo].[calcModuleTechnicalBaseCostRate](
@IdModel int
)
AS
BEGIN
	
	/** Set Variables **/
	DECLARE @BaseYear int = 0;
	SELECT @BaseYear=BaseYear FROM tblModels WHERE IdModel=@IdModel;
	
	/** Cleanup Calc Tables for New Data **/
	DELETE FROM tblCalcTechnicalBaseCostRateSelections WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalBaseCostRate WHERE [IdModel]=@IdModel;
	
	/** Add All Possible Combinations for Report Table **/
	INSERT INTO tblCalcTechnicalBaseCostRateSelections ([IdModel],[IdZiffAccount],[CodeZiff],[ZiffAccount],[DisplayCode],[SortOrder],[IdField],[IdStage],[BaseCostType],[ProjectionCriteria],[NameProjectionCriteria],[BaseCost],[BaseCostRate])
	SELECT vListZiffAccounts.IdModel, vListZiffAccounts.IdZiffAccount, vListZiffAccounts.Code AS CodeZiff, vListZiffAccounts.Name AS ZiffAccount, 
		vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder, tblFields.IdField, tblStage.IdStage, BCLookup.Code AS BaseCostType, 
		98 AS ProjectionCriteria, 'N/A' AS NameProjectionCriteria, NULL AS BaseCost, NULL AS BaseCostRate
	FROM vListZiffAccounts INNER JOIN
		tblModels ON vListZiffAccounts.IdModel = tblModels.IdModel INNER JOIN
		tblFields ON tblModels.IdModel = tblFields.IdModel CROSS JOIN
		tblStage CROSS JOIN
		(SELECT Code FROM tblParameters WHERE Type = N'BaseCost' AND IdLanguage=1) AS BCLookup
	WHERE vListZiffAccounts.Parent IS NOT NULL AND vListZiffAccounts.IdModel=@IdModel;

	/** Company Normal BaseCostRate **/
	INSERT INTO tblCalcTechnicalBaseCostRate ([IdModel],[IdZiffAccount],[CodeZiff],[ZiffAccount],[DisplayCode],[SortOrder],[IdField],[IdStage],[BaseCostType],[ProjectionCriteria],[NameProjectionCriteria],[CycleValue],
											  [SizeName],[SizeValue],[PerformanceName],[PerformanceValue],[BaseCost],[BaseCostRate],[BaselineAllocation],[IdPeerGroup])
	SELECT tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, vListZiffAccounts.Code AS CodeZiff, vListZiffAccounts.Name AS ZiffAccount, vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder,
		tblCalcTechnicalDriverAssumption.IdField, 1 AS IdStage, 1 AS BaseCostType,
		tblCalcTechnicalDriverAssumption.ProjectionCriteria, PCLookup.Name AS NameProjectionCriteria, tblCalcTechnicalDriverAssumption.CycleValue, TDS.Name AS SizeName,
		SUM(ISNULL(tblCalcTechnicalDriverAssumption.SizeNormal,0)) AS SizeValue, TDP.Name AS PerformanceName, 
		SUM(ISNULL(tblCalcTechnicalDriverAssumption.PerformanceNormal,0)) AS PerformanceValue, 
		SUM(ISNULL(tblCalcTechnicalDriverAssumption.TFNormal,0) * ISNULL(tblCalcTechnicalDriverAssumption.BCRNormal,0)) AS BaseCost, 
		AVG(ISNULL(tblCalcTechnicalDriverAssumption.BCRNormal,0)) AS BaseCostRate, ISNULL(TDS.BaselineAllocation, 0) AS BaselineAllocation, 0 AS IdPeerGroup
	FROM tblCalcTechnicalDriverAssumption LEFT OUTER JOIN
		vListZiffAccounts ON tblCalcTechnicalDriverAssumption.IdZiffAccount = vListZiffAccounts.IdZiffAccount LEFT OUTER JOIN
		(
		SELECT IdParameters, Type, Code, Name, IdLanguage
		FROM tblParameters
		WHERE Type = N'ProjectionCriteria'
		) AS PCLookup ON tblCalcTechnicalDriverAssumption.ProjectionCriteria = PCLookup.Code LEFT OUTER JOIN
		tblTechnicalDrivers AS TDP ON tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance = TDP.IdTechnicalDriver LEFT OUTER JOIN
		tblTechnicalDrivers AS TDS ON tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize = TDS.IdTechnicalDriver
	WHERE tblCalcTechnicalDriverAssumption.Operation=1 AND tblCalcTechnicalDriverAssumption.BaseYear=1 AND PCLookup.IdLanguage=1
	GROUP BY tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, vListZiffAccounts.Code, vListZiffAccounts.Name, 
		vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder, tblCalcTechnicalDriverAssumption.IdField, 
		tblCalcTechnicalDriverAssumption.ProjectionCriteria, PCLookup.Name, tblCalcTechnicalDriverAssumption.CycleValue, TDS.Name, TDP.Name,TDS.BaselineAllocation
	HAVING tblCalcTechnicalDriverAssumption.IdModel=@IdModel;

	/** Company Optimistic BaseCostRate **/
	INSERT INTO tblCalcTechnicalBaseCostRate ([IdModel],[IdZiffAccount],[CodeZiff],[ZiffAccount],[DisplayCode],[SortOrder],[IdField],[IdStage],[BaseCostType],[ProjectionCriteria],[NameProjectionCriteria],[CycleValue],
											  [SizeName],[SizeValue],[PerformanceName],[PerformanceValue],[BaseCost],[BaseCostRate],[BaselineAllocation],[IdPeerGroup])
	SELECT tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, vListZiffAccounts.Code AS CodeZiff, vListZiffAccounts.Name AS ZiffAccount, vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder,
		tblCalcTechnicalDriverAssumption.IdField, 2 AS IdStage, 1 AS BaseCostType,
		tblCalcTechnicalDriverAssumption.ProjectionCriteria, PCLookup.Name AS NameProjectionCriteria, tblCalcTechnicalDriverAssumption.CycleValue, TDS.Name AS SizeName,
		SUM(ISNULL(tblCalcTechnicalDriverAssumption.SizeOptimistic,0)) AS SizeValue, TDP.Name AS PerformanceName, 
		SUM(ISNULL(tblCalcTechnicalDriverAssumption.PerformanceOptimistic,0)) AS PerformanceValue, 
		SUM(ISNULL(tblCalcTechnicalDriverAssumption.TFOptimistic,0) * ISNULL(tblCalcTechnicalDriverAssumption.BCROptimistic,0)) AS BaseCost, 
		AVG(ISNULL(tblCalcTechnicalDriverAssumption.BCROptimistic,0)) AS BaseCostRate, ISNULL(TDS.BaselineAllocation, 0) AS BaselineAllocation, 0 AS IdPeerGroup
	FROM tblCalcTechnicalDriverAssumption LEFT OUTER JOIN
		vListZiffAccounts ON tblCalcTechnicalDriverAssumption.IdZiffAccount = vListZiffAccounts.IdZiffAccount LEFT OUTER JOIN
		(
		SELECT IdParameters, Type, Code, Name, IdLanguage
		FROM tblParameters
		WHERE Type = N'ProjectionCriteria'
		) AS PCLookup ON tblCalcTechnicalDriverAssumption.ProjectionCriteria = PCLookup.Code LEFT OUTER JOIN
		tblTechnicalDrivers AS TDP ON tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance = TDP.IdTechnicalDriver LEFT OUTER JOIN
		tblTechnicalDrivers AS TDS ON tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize = TDS.IdTechnicalDriver
	WHERE tblCalcTechnicalDriverAssumption.Operation=1 AND tblCalcTechnicalDriverAssumption.BaseYear=1 AND PCLookup.IdLanguage=1
	GROUP BY tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, vListZiffAccounts.Code, vListZiffAccounts.Name, 
		vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder, tblCalcTechnicalDriverAssumption.IdField, 
		tblCalcTechnicalDriverAssumption.ProjectionCriteria, PCLookup.Name, tblCalcTechnicalDriverAssumption.CycleValue, TDS.Name, TDP.Name,TDS.BaselineAllocation
	HAVING tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
		
	/** Company Pessimistic BaseCostRate **/
	INSERT INTO tblCalcTechnicalBaseCostRate ([IdModel],[IdZiffAccount],[CodeZiff],[ZiffAccount],[DisplayCode],[SortOrder],[IdField],[IdStage],[BaseCostType],[ProjectionCriteria],[NameProjectionCriteria],[CycleValue],
											  [SizeName],[SizeValue],[PerformanceName],[PerformanceValue],[BaseCost],[BaseCostRate],[BaselineAllocation],[IdPeerGroup])
	SELECT tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, vListZiffAccounts.Code AS CodeZiff, vListZiffAccounts.Name AS ZiffAccount, vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder,
		tblCalcTechnicalDriverAssumption.IdField, 3 AS IdStage, 1 AS BaseCostType,
		tblCalcTechnicalDriverAssumption.ProjectionCriteria, PCLookup.Name AS NameProjectionCriteria, tblCalcTechnicalDriverAssumption.CycleValue, TDS.Name AS SizeName,
		SUM(ISNULL(tblCalcTechnicalDriverAssumption.SizePessimistic,0)) AS SizeValue, TDP.Name AS PerformanceName, 
		SUM(ISNULL(tblCalcTechnicalDriverAssumption.PerformancePessimistic,0)) AS PerformanceValue, 
		SUM(ISNULL(tblCalcTechnicalDriverAssumption.TFPessimistic,0) * ISNULL(tblCalcTechnicalDriverAssumption.BCRPessimistic,0)) AS BaseCost, 
		AVG(ISNULL(tblCalcTechnicalDriverAssumption.BCRPessimistic,0)) AS BaseCostRate, ISNULL(TDS.BaselineAllocation, 0) AS BaselineAllocation, 0 AS IdPeerGroup
	FROM tblCalcTechnicalDriverAssumption LEFT OUTER JOIN
		vListZiffAccounts ON tblCalcTechnicalDriverAssumption.IdZiffAccount = vListZiffAccounts.IdZiffAccount LEFT OUTER JOIN
		(
		SELECT IdParameters, Type, Code, Name, IdLanguage
		FROM tblParameters
		WHERE Type = N'ProjectionCriteria'
		) AS PCLookup ON tblCalcTechnicalDriverAssumption.ProjectionCriteria = PCLookup.Code LEFT OUTER JOIN
		tblTechnicalDrivers AS TDP ON tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance = TDP.IdTechnicalDriver LEFT OUTER JOIN
		tblTechnicalDrivers AS TDS ON tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize = TDS.IdTechnicalDriver
	WHERE tblCalcTechnicalDriverAssumption.Operation=1 AND tblCalcTechnicalDriverAssumption.BaseYear=1 AND PCLookup.IdLanguage=1
	GROUP BY tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, vListZiffAccounts.Code, vListZiffAccounts.Name, 
		vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder, tblCalcTechnicalDriverAssumption.IdField, 
		tblCalcTechnicalDriverAssumption.ProjectionCriteria, PCLookup.Name, tblCalcTechnicalDriverAssumption.CycleValue, TDS.Name, TDP.Name,TDS.BaselineAllocation
	HAVING tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Ziff Normal BaseCostRate **/
	INSERT INTO tblCalcTechnicalBaseCostRate ([IdModel],[IdZiffAccount],[CodeZiff],[ZiffAccount],[DisplayCode],[SortOrder],[IdField],[IdStage],[BaseCostType],[ProjectionCriteria],[NameProjectionCriteria],[CycleValue],
											  [SizeName],[SizeValue],[PerformanceName],[PerformanceValue],[BaseCost],[BaseCostRate],[BaselineAllocation],[IdPeerGroup])
	SELECT tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, vListZiffAccounts.Code AS CodeZiff, vListZiffAccounts.Name AS ZiffAccount, vListZiffAccounts.DisplayCode, 
		vListZiffAccounts.SortOrder, tblCalcTechnicalDriverAssumptionZiff.IdField, 1 AS IdStage, 2 AS BaseCostType, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, PCLookup.Name AS NameProjectionCriteria, 
		tblCalcTechnicalDriverAssumptionZiff.CycleValue, TDS.Name AS SizeName, 
		SUM(ISNULL(tblCalcTechnicalDriverAssumptionZiff.SizeNormal,0)) AS SizeValue, TDP.Name AS PerformanceName, 
		SUM(ISNULL(tblCalcTechnicalDriverAssumptionZiff.PerformanceNormal,0)) AS PerformanceValue, 
		CBCBFZ.Normal * UnitCost AS BaseCost, 
	    (CBCBFZ.Normal * UnitCost) / tblCalcTechnicalDriverAssumptionZiff.SizeNormal / ISNULL(tblCalcTechnicalDriverAssumptionZiff.PerformanceNormal,1) AS BaseCostRate,
	    ISNULL(TDS.BaselineAllocation, 0) AS BaselineAllocation, ISNULL(tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup,0)
	FROM tblCalcTechnicalDriverAssumptionZiff LEFT OUTER JOIN
		vListZiffAccounts ON tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = vListZiffAccounts.IdZiffAccount LEFT OUTER JOIN
		(
		SELECT IdParameters, Type, Code, Name, IdLanguage
		FROM tblParameters
		WHERE Type = N'ProjectionCriteria'
		) AS PCLookup ON tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria = PCLookup.Code LEFT OUTER JOIN
		tblTechnicalDrivers AS TDP ON tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverPerformance = TDP.IdTechnicalDriver LEFT OUTER JOIN
		tblTechnicalDrivers AS TDS ON tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverSize = TDS.IdTechnicalDriver LEFT OUTER JOIN
		tblCalcBaseCostByFieldZiff ON tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = tblCalcBaseCostByFieldZiff.IdZiffAccount AND
		tblCalcTechnicalDriverAssumptionZiff.IdField = tblCalcBaseCostByFieldZiff.IdField AND
		tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup = tblCalcBaseCostByFieldZiff.IdPeerGroup INNER JOIN
		 ( SELECT  tblProjects.IdModel,tblProjects.IdField,tblCostStructureAssumptionsByProject.IdZiffAccount,tblCostStructureAssumptionsByProject.IdPeerGroup,
					tblCostStructureAssumptionsByProject.Client,tblCostStructureAssumptionsByProject.Ziff,CalcBaseCostByFieldSummaryZiff.Normal,
					CalcBaseCostByFieldSummaryZiff.Pessimistic,CalcBaseCostByFieldSummaryZiff.Optimistic
			FROM    tblCostStructureAssumptionsByProject LEFT OUTER JOIN
					tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject LEFT OUTER JOIN
					tblZiffAccounts ON tblCostStructureAssumptionsByProject.IdZiffAccount = tblZiffAccounts.IdZiffAccount LEFT OUTER JOIN
					tblZiffAccounts AS tblZiffAccountsParent ON tblZiffAccounts.Parent = tblZiffAccountsParent.IdZiffAccount INNER JOIN
					tblPeerGroup ON tblCostStructureAssumptionsByProject.IdPeerGroup = tblPeerGroup.IdPeerGroup INNER JOIN
					(
					 SELECT tblCalcTechnicalDriverData.IdModel,tblCalcTechnicalDriverData.IdField,tblCalcTechnicalDriverData.Year,
							SUM(tblCalcTechnicalDriverData.Normal) AS Normal,
							SUM(tblCalcTechnicalDriverData.Pessimistic) AS Pessimistic,
							SUM(tblCalcTechnicalDriverData.Optimistic) AS Optimistic
					 FROM tblCalcTechnicalDriverData INNER JOIN
							tblTechnicalDrivers ON tblCalcTechnicalDriverData.IdTechnicalDriver = tblTechnicalDrivers.IdTechnicalDriver
					 WHERE tblTechnicalDrivers.UnitCostDenominator=1 
					 GROUP BY tblCalcTechnicalDriverData.IdModel,tblCalcTechnicalDriverData.IdField,tblCalcTechnicalDriverData.Year
					) AS CalcBaseCostByFieldSummaryZiff ON tblProjects.IdModel = CalcBaseCostByFieldSummaryZiff.IdModel AND
					tblProjects.IdField = CalcBaseCostByFieldSummaryZiff.IdField AND
					tblCostStructureAssumptionsByProject.Ziff = CalcBaseCostByFieldSummaryZiff.Year
			GROUP BY tblProjects.IdModel,tblProjects.IdField,tblCostStructureAssumptionsByProject.IdZiffAccount,tblCostStructureAssumptionsByProject.IdPeerGroup,
					tblCostStructureAssumptionsByProject.Client,tblCostStructureAssumptionsByProject.Ziff,CalcBaseCostByFieldSummaryZiff.Normal,
					CalcBaseCostByFieldSummaryZiff.Pessimistic,CalcBaseCostByFieldSummaryZiff.Optimistic
		) AS CBCBFZ ON
		tblCalcTechnicalDriverAssumptionZiff.IdModel = CBCBFZ.IdModel AND
		tblCalcTechnicalDriverAssumptionZiff.IdField = CBCBFZ.IdField AND
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = CBCBFZ.IdZiffAccount AND
		tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup = CBCBFZ.IdPeerGroup
	WHERE tblCalcTechnicalDriverAssumptionZiff.Operation=1 AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 AND PCLookup.IdLanguage=1
	GROUP BY tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, vListZiffAccounts.Code, vListZiffAccounts.Name, 
		vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder, tblCalcTechnicalDriverAssumptionZiff.IdField, 
		tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, PCLookup.Name, tblCalcTechnicalDriverAssumptionZiff.CycleValue, TDS.Name, TDP.Name,TDS.BaselineAllocation, tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup,
		CBCBFZ.Normal * UnitCost,(CBCBFZ.Normal * UnitCost) / tblCalcTechnicalDriverAssumptionZiff.SizeNormal / ISNULL(tblCalcTechnicalDriverAssumptionZiff.PerformanceNormal,1), tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup
	HAVING tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel
	ORDER BY ProjectionCriteria
		
	/** Ziff Optimistic BaseCostRate **/
	INSERT INTO tblCalcTechnicalBaseCostRate ([IdModel],[IdZiffAccount],[CodeZiff],[ZiffAccount],[DisplayCode],[SortOrder],[IdField],[IdStage],[BaseCostType],[ProjectionCriteria],[NameProjectionCriteria],[CycleValue],
											  [SizeName],[SizeValue],[PerformanceName],[PerformanceValue],[BaseCost],[BaseCostRate],[BaselineAllocation], [IdPeerGroup])
	SELECT tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, vListZiffAccounts.Code AS CodeZiff, vListZiffAccounts.Name AS ZiffAccount, vListZiffAccounts.DisplayCode, 
		vListZiffAccounts.SortOrder, tblCalcTechnicalDriverAssumptionZiff.IdField, 2 AS IdStage, 2 AS BaseCostType, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, PCLookup.Name AS NameProjectionCriteria, 
		tblCalcTechnicalDriverAssumptionZiff.CycleValue, TDS.Name AS SizeName, 
		SUM(ISNULL(tblCalcTechnicalDriverAssumptionZiff.SizeOptimistic,0)) AS SizeValue, TDP.Name AS PerformanceName, 
		SUM(ISNULL(tblCalcTechnicalDriverAssumptionZiff.PerformanceOptimistic,0)) AS PerformanceValue, 
		CBCBFZ.Optimistic * UnitCost AS BaseCost, 
	    (CBCBFZ.Optimistic * UnitCost) / tblCalcTechnicalDriverAssumptionZiff.SizeOptimistic / ISNULL(tblCalcTechnicalDriverAssumptionZiff.PerformanceOptimistic,1) AS BaseCostRate,
	    ISNULL(TDS.BaselineAllocation, 0) AS BaselineAllocation, ISNULL(tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup,0)
	FROM tblCalcTechnicalDriverAssumptionZiff LEFT OUTER JOIN
		vListZiffAccounts ON tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = vListZiffAccounts.IdZiffAccount LEFT OUTER JOIN
		(
		SELECT IdParameters, Type, Code, Name, IdLanguage
		FROM tblParameters
		WHERE Type = N'ProjectionCriteria'
		) AS PCLookup ON tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria = PCLookup.Code LEFT OUTER JOIN
		tblTechnicalDrivers AS TDP ON tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverPerformance = TDP.IdTechnicalDriver LEFT OUTER JOIN
		tblTechnicalDrivers AS TDS ON tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverSize = TDS.IdTechnicalDriver LEFT OUTER JOIN
		tblCalcBaseCostByFieldZiff ON tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = tblCalcBaseCostByFieldZiff.IdZiffAccount AND
		tblCalcTechnicalDriverAssumptionZiff.IdField = tblCalcBaseCostByFieldZiff.IdField AND
		tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup = tblCalcBaseCostByFieldZiff.IdPeerGroup INNER JOIN
		 ( SELECT  tblProjects.IdModel,tblProjects.IdField,tblCostStructureAssumptionsByProject.IdZiffAccount,tblCostStructureAssumptionsByProject.IdPeerGroup,
					tblCostStructureAssumptionsByProject.Client,tblCostStructureAssumptionsByProject.Ziff,CalcBaseCostByFieldSummaryZiff.Normal,
					CalcBaseCostByFieldSummaryZiff.Pessimistic,CalcBaseCostByFieldSummaryZiff.Optimistic
			FROM    tblCostStructureAssumptionsByProject LEFT OUTER JOIN
					tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject LEFT OUTER JOIN
					tblZiffAccounts ON tblCostStructureAssumptionsByProject.IdZiffAccount = tblZiffAccounts.IdZiffAccount LEFT OUTER JOIN
					tblZiffAccounts AS tblZiffAccountsParent ON tblZiffAccounts.Parent = tblZiffAccountsParent.IdZiffAccount INNER JOIN
					tblPeerGroup ON tblCostStructureAssumptionsByProject.IdPeerGroup = tblPeerGroup.IdPeerGroup INNER JOIN
					(
					 SELECT tblCalcTechnicalDriverData.IdModel,tblCalcTechnicalDriverData.IdField,tblCalcTechnicalDriverData.Year,
							SUM(tblCalcTechnicalDriverData.Normal) AS Normal,
							SUM(tblCalcTechnicalDriverData.Pessimistic) AS Pessimistic,
							SUM(tblCalcTechnicalDriverData.Optimistic) AS Optimistic
					 FROM tblCalcTechnicalDriverData INNER JOIN
							tblTechnicalDrivers ON tblCalcTechnicalDriverData.IdTechnicalDriver = tblTechnicalDrivers.IdTechnicalDriver
					 WHERE tblTechnicalDrivers.UnitCostDenominator=1 
					 GROUP BY tblCalcTechnicalDriverData.IdModel,tblCalcTechnicalDriverData.IdField,tblCalcTechnicalDriverData.Year
					) AS CalcBaseCostByFieldSummaryZiff ON tblProjects.IdModel = CalcBaseCostByFieldSummaryZiff.IdModel AND
					tblProjects.IdField = CalcBaseCostByFieldSummaryZiff.IdField AND
					tblCostStructureAssumptionsByProject.Ziff = CalcBaseCostByFieldSummaryZiff.Year
			GROUP BY tblProjects.IdModel,tblProjects.IdField,tblCostStructureAssumptionsByProject.IdZiffAccount,tblCostStructureAssumptionsByProject.IdPeerGroup,
					tblCostStructureAssumptionsByProject.Client,tblCostStructureAssumptionsByProject.Ziff,CalcBaseCostByFieldSummaryZiff.Normal,
					CalcBaseCostByFieldSummaryZiff.Pessimistic,CalcBaseCostByFieldSummaryZiff.Optimistic
		) AS CBCBFZ ON
		tblCalcTechnicalDriverAssumptionZiff.IdModel = CBCBFZ.IdModel AND
		tblCalcTechnicalDriverAssumptionZiff.IdField = CBCBFZ.IdField AND
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = CBCBFZ.IdZiffAccount AND
		tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup = CBCBFZ.IdPeerGroup
	WHERE tblCalcTechnicalDriverAssumptionZiff.Operation=1 AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 AND PCLookup.IdLanguage=1 
	GROUP BY tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, vListZiffAccounts.Code, vListZiffAccounts.Name, 
		vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder, tblCalcTechnicalDriverAssumptionZiff.IdField, 
		tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, PCLookup.Name, tblCalcTechnicalDriverAssumptionZiff.CycleValue, TDS.Name, TDP.Name,TDS.BaselineAllocation, tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup,
		CBCBFZ.Optimistic * UnitCost,(CBCBFZ.Optimistic * UnitCost) / tblCalcTechnicalDriverAssumptionZiff.SizeOptimistic / ISNULL(tblCalcTechnicalDriverAssumptionZiff.PerformanceOptimistic,1), tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup
	HAVING tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel
		
	/** Ziff Pessimistic BaseCostRate **/
	INSERT INTO tblCalcTechnicalBaseCostRate ([IdModel],[IdZiffAccount],[CodeZiff],[ZiffAccount],[DisplayCode],[SortOrder],[IdField],[IdStage],[BaseCostType],[ProjectionCriteria],[NameProjectionCriteria],[CycleValue],
											  [SizeName],[SizeValue],[PerformanceName],[PerformanceValue],[BaseCost],[BaseCostRate],[BaselineAllocation],[IdPeerGroup])
	SELECT tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, vListZiffAccounts.Code AS CodeZiff, vListZiffAccounts.Name AS ZiffAccount, vListZiffAccounts.DisplayCode, 
		vListZiffAccounts.SortOrder, tblCalcTechnicalDriverAssumptionZiff.IdField, 3 AS IdStage, 2 AS BaseCostType, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, PCLookup.Name AS NameProjectionCriteria, 
		tblCalcTechnicalDriverAssumptionZiff.CycleValue, TDS.Name AS SizeName, 
		SUM(ISNULL(tblCalcTechnicalDriverAssumptionZiff.SizePessimistic,0)) AS SizeValue, TDP.Name AS PerformanceName, 
		SUM(ISNULL(tblCalcTechnicalDriverAssumptionZiff.PerformancePessimistic,0)) AS PerformanceValue, 
		CBCBFZ.Pessimistic * UnitCost AS BaseCost, 
	    (CBCBFZ.Pessimistic * UnitCost) / tblCalcTechnicalDriverAssumptionZiff.SizePessimistic / ISNULL(tblCalcTechnicalDriverAssumptionZiff.PerformancePessimistic,1) AS BaseCostRate,
	    ISNULL(TDS.BaselineAllocation, 0) AS BaselineAllocation, ISNULL(tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup,0)
	FROM tblCalcTechnicalDriverAssumptionZiff LEFT OUTER JOIN
		vListZiffAccounts ON tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = vListZiffAccounts.IdZiffAccount LEFT OUTER JOIN
		(
		SELECT IdParameters, Type, Code, Name, IdLanguage
		FROM tblParameters
		WHERE Type = N'ProjectionCriteria'
		) AS PCLookup ON tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria = PCLookup.Code LEFT OUTER JOIN
		tblTechnicalDrivers AS TDP ON tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverPerformance = TDP.IdTechnicalDriver LEFT OUTER JOIN
		tblTechnicalDrivers AS TDS ON tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverSize = TDS.IdTechnicalDriver LEFT OUTER JOIN
		tblCalcBaseCostByFieldZiff ON tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = tblCalcBaseCostByFieldZiff.IdZiffAccount AND
		tblCalcTechnicalDriverAssumptionZiff.IdField = tblCalcBaseCostByFieldZiff.IdField AND
		tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup = tblCalcBaseCostByFieldZiff.IdPeerGroup INNER JOIN
		 ( SELECT  tblProjects.IdModel,tblProjects.IdField,tblCostStructureAssumptionsByProject.IdZiffAccount,tblCostStructureAssumptionsByProject.IdPeerGroup,
					tblCostStructureAssumptionsByProject.Client,tblCostStructureAssumptionsByProject.Ziff,CalcBaseCostByFieldSummaryZiff.Normal,
					CalcBaseCostByFieldSummaryZiff.Pessimistic,CalcBaseCostByFieldSummaryZiff.Optimistic
			FROM    tblCostStructureAssumptionsByProject LEFT OUTER JOIN
					tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject LEFT OUTER JOIN
					tblZiffAccounts ON tblCostStructureAssumptionsByProject.IdZiffAccount = tblZiffAccounts.IdZiffAccount LEFT OUTER JOIN
					tblZiffAccounts AS tblZiffAccountsParent ON tblZiffAccounts.Parent = tblZiffAccountsParent.IdZiffAccount INNER JOIN
					tblPeerGroup ON tblCostStructureAssumptionsByProject.IdPeerGroup = tblPeerGroup.IdPeerGroup INNER JOIN
					(
					 SELECT tblCalcTechnicalDriverData.IdModel,tblCalcTechnicalDriverData.IdField,tblCalcTechnicalDriverData.Year,
							SUM(tblCalcTechnicalDriverData.Normal) AS Normal,
							SUM(tblCalcTechnicalDriverData.Pessimistic) AS Pessimistic,
							SUM(tblCalcTechnicalDriverData.Optimistic) AS Optimistic
					 FROM tblCalcTechnicalDriverData INNER JOIN
							tblTechnicalDrivers ON tblCalcTechnicalDriverData.IdTechnicalDriver = tblTechnicalDrivers.IdTechnicalDriver
					 WHERE tblTechnicalDrivers.UnitCostDenominator=1 
					 GROUP BY tblCalcTechnicalDriverData.IdModel,tblCalcTechnicalDriverData.IdField,tblCalcTechnicalDriverData.Year
					) AS CalcBaseCostByFieldSummaryZiff ON tblProjects.IdModel = CalcBaseCostByFieldSummaryZiff.IdModel AND
					tblProjects.IdField = CalcBaseCostByFieldSummaryZiff.IdField AND
					tblCostStructureAssumptionsByProject.Ziff = CalcBaseCostByFieldSummaryZiff.Year
			GROUP BY tblProjects.IdModel,tblProjects.IdField,tblCostStructureAssumptionsByProject.IdZiffAccount,tblCostStructureAssumptionsByProject.IdPeerGroup,
					tblCostStructureAssumptionsByProject.Client,tblCostStructureAssumptionsByProject.Ziff,CalcBaseCostByFieldSummaryZiff.Normal,
					CalcBaseCostByFieldSummaryZiff.Pessimistic,CalcBaseCostByFieldSummaryZiff.Optimistic
		) AS CBCBFZ ON
		tblCalcTechnicalDriverAssumptionZiff.IdModel = CBCBFZ.IdModel AND
		tblCalcTechnicalDriverAssumptionZiff.IdField = CBCBFZ.IdField AND
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = CBCBFZ.IdZiffAccount AND
		tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup = CBCBFZ.IdPeerGroup
	WHERE tblCalcTechnicalDriverAssumptionZiff.Operation=1 AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 AND PCLookup.IdLanguage=1
	GROUP BY tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, vListZiffAccounts.Code, vListZiffAccounts.Name, 
		vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder, tblCalcTechnicalDriverAssumptionZiff.IdField, 
		tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, PCLookup.Name, tblCalcTechnicalDriverAssumptionZiff.CycleValue, TDS.Name, TDP.Name,TDS.BaselineAllocation, tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup,
		CBCBFZ.Pessimistic * UnitCost,(CBCBFZ.Pessimistic * UnitCost) / tblCalcTechnicalDriverAssumptionZiff.SizePessimistic / ISNULL(tblCalcTechnicalDriverAssumptionZiff.PerformancePessimistic,1), tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup
	HAVING tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel
	ORDER BY ProjectionCriteria
	
	/** Update Missing Values **/
	UPDATE tblCalcTechnicalBaseCostRate SET [BaseCost]=0 WHERE [BaseCost] IS NULL AND [IdModel]=@IdModel;
	UPDATE tblCalcTechnicalBaseCostRate SET [BaseCostRate]=0 WHERE [BaseCostRate] IS NULL AND [IdModel]=@IdModel;
	
	/** Update Fixed and SemiFixed **/
	UPDATE tblCalcTechnicalBaseCostRate SET [BaseCostRate]=[BaseCost] WHERE [ProjectionCriteria]=1 AND [IdModel]=@IdModel;
	UPDATE tblCalcTechnicalBaseCostRate SET [BaseCostRate]=NULL WHERE [ProjectionCriteria]=2 AND [IdModel]=@IdModel;
	
	/** Add any missing codes for the Report **/
	INSERT INTO tblCalcTechnicalBaseCostRate ([IdModel],[IdZiffAccount],[CodeZiff],[ZiffAccount],[DisplayCode],[SortOrder],[IdField],[IdStage],[BaseCostType],[ProjectionCriteria],[NameProjectionCriteria],[BaseCost],
											  [BaseCostRate],[IdPeerGroup])
	SELECT tblCalcTechnicalBaseCostRateSelections.IdModel, tblCalcTechnicalBaseCostRateSelections.IdZiffAccount, 
		tblCalcTechnicalBaseCostRateSelections.CodeZiff, tblCalcTechnicalBaseCostRateSelections.ZiffAccount, 
		tblCalcTechnicalBaseCostRateSelections.DisplayCode, tblCalcTechnicalBaseCostRateSelections.SortOrder, 
		tblCalcTechnicalBaseCostRateSelections.IdField, tblCalcTechnicalBaseCostRateSelections.IdStage, 
		tblCalcTechnicalBaseCostRateSelections.BaseCostType, tblCalcTechnicalBaseCostRateSelections.ProjectionCriteria, 
		tblCalcTechnicalBaseCostRateSelections.NameProjectionCriteria, tblCalcTechnicalBaseCostRateSelections.BaseCost, 
		tblCalcTechnicalBaseCostRateSelections.BaseCostRate, 0 AS IdPeerGroup
	FROM tblCalcTechnicalBaseCostRateSelections LEFT OUTER JOIN
		tblCalcTechnicalBaseCostRate ON tblCalcTechnicalBaseCostRateSelections.IdModel = tblCalcTechnicalBaseCostRate.IdModel AND 
		tblCalcTechnicalBaseCostRateSelections.IdZiffAccount = tblCalcTechnicalBaseCostRate.IdZiffAccount AND 
		tblCalcTechnicalBaseCostRateSelections.IdField = tblCalcTechnicalBaseCostRate.IdField AND 
		tblCalcTechnicalBaseCostRateSelections.IdStage = tblCalcTechnicalBaseCostRate.IdStage AND 
		tblCalcTechnicalBaseCostRateSelections.BaseCostType = tblCalcTechnicalBaseCostRate.BaseCostType
	WHERE tblCalcTechnicalBaseCostRate.IdModel IS NULL AND tblCalcTechnicalBaseCostRateSelections.IdModel=@IdModel;
	
	/** Set RootParent Columns **/
	UPDATE tblCalcTechnicalBaseCostRate
	SET [IdRootZiffAccount]=tblZiffAccountsRoot.IdZiffAccount, [RootCodeZiff]=tblZiffAccountsRoot.Code, [RootZiffAccount]=tblZiffAccountsRoot.Name
	FROM tblCalcTechnicalBaseCostRate LEFT OUTER JOIN tblZiffAccounts ON tblCalcTechnicalBaseCostRate.IdModel = tblZiffAccounts.IdModel AND tblCalcTechnicalBaseCostRate.IdZiffAccount = tblZiffAccounts.IdZiffAccount LEFT OUTER JOIN tblZiffAccounts AS tblZiffAccountsRoot ON tblZiffAccounts.RootParent = tblZiffAccountsRoot.IdZiffAccount
	WHERE tblCalcTechnicalBaseCostRate.[IdModel]=@IdModel;

END

GO

/****** Object:  StoredProcedure [dbo].[allpDeletePeerCriteriaCost]    Script Date: 12/10/2015 13:41:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[allpDeletePeerCriteriaCost](
@IdPeerCriteria int,
@IdPeerGroup int)
AS
BEGIN
	
	DELETE FROM tblPeerCriteriaCost WHERE [IdPeerCriteria]=@IdPeerCriteria AND [IdPeerGroup]=@IdPeerGroup;
	
END

GO

/****** Object:  StoredProcedure [dbo].[allpUpdatePeerCriteriaCost]    Script Date: 12/10/2015 11:56:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[allpUpdatePeerCriteriaCost](
@IdPeerCriteria int,
@IdPeerGroup int,
@Quantity float)
AS
BEGIN

	IF (EXISTS(
		SELECT [Quantity]
		FROM tblPeerCriteriaCost 
		WHERE [IdPeerCriteria]=@IdPeerCriteria AND [IdPeerGroup]=@IdPeerGroup))
	BEGIN
		UPDATE tblPeerCriteriaCost SET [Quantity]=@Quantity
		WHERE [IdPeerCriteria]=@IdPeerCriteria AND [IdPeerGroup]=@IdPeerGroup;
	END
	ELSE
	BEGIN
		INSERT INTO tblPeerCriteriaCost ([IdPeerCriteria],[IdPeerGroup],[Quantity])
		VALUES(@IdPeerCriteria,@IdPeerGroup,@Quantity);
	END
	
END

GO

/****** Object:  StoredProcedure [dbo].[allpInsertPeerCriteriaCost]    Script Date: 12/10/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[allpInsertPeerCriteriaCost](
@IdPeerCriteria int,
@IdPeerGroup int,
@Quantity float)
AS
BEGIN
	
	INSERT INTO tblPeerCriteriaCost ([IdPeerCriteria],[IdPeerGroup],[Quantity])
	VALUES(@IdPeerCriteria,@IdPeerGroup,@Quantity);
	
END

GO

/****** Object:  StoredProcedure [dbo].[allpUpdatePeerCriteria]    Script Date: 12/08/2015 08:13:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[allpUpdatePeerCriteria](
@IdPeerCriteria int,
@IdModel int,
@Description varchar(50),
@IdUnitPeerCriteria int,
@TotalUnitCostDenominator bit)
AS
BEGIN

	UPDATE tblPeerCriteria SET [IdModel]=@IdModel,[Description]=@Description,[IdUnitPeerCriteria]=@IdUnitPeerCriteria,[TotalUnitCostDenominator]=@TotalUnitCostDenominator
    WHERE [IdPeerCriteria]=@IdPeerCriteria;
END

GO

/****** Object:  StoredProcedure [dbo].[allpInsertPeerCriteria]    Script Date: 12/08/2015 08:10:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[allpInsertPeerCriteria](
@IdPeerCriteria numeric output,
@IdModel int,
@Description varchar(50),
@IdUnitPeerCriteria int,
@TotalUnitCostDenominator bit)
AS
BEGIN
	
	INSERT INTO tblPeerCriteria ([IdModel],[Description],[IdUnitPeerCriteria],[TotalUnitCostDenominator])
	VALUES(@IdModel,@Description,@IdUnitPeerCriteria,@TotalUnitCostDenominator);
	
	SET @IdPeerCriteria = SCOPE_IDENTITY();
	
END

GO

/****** Object:  StoredProcedure [dbo].[parpUpdateProjects]    Script Date: 12/03/2015 10:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-----------------------------------------------------------------------------------------
Autor   		:Rodrigo Lopez Robles
Fecha Creacion	:09/10/2013
Descripcion     : Modifica un registro en la tabla tblProjects
===========================================================================================
Modifications:	
Date		Author	Description
---------	------	-----------------------------------------------------------------------
12/3/2015	RM		Added field CaseSelection 

-------------------------------------------------------------------------------------------*/
ALTER PROC [dbo].[parpUpdateProjects](
@IdProject int,
@IdModel int,
@Code varchar(150),
@Name varchar(255),
@IdField int,
@Operation int,
@Starts int,
@TypeAllocation Int,
@UserCreation int,
@DateCreation datetime,
@UserModification int,
@DateModification datetime,
@CaseSelection bit)
AS 
BEGIN
  UPDATE tblProjects SET IdModel=@IdModel,Code=@Code,Name=@Name,IdField=@IdField,Operation=@Operation,Starts=@Starts,TypeAllocation=@TypeAllocation,UserCreation=@UserCreation,DateCreation=@DateCreation,UserModification=@UserModification,DateModification=@DateModification,CaseSelection=@CaseSelection
      WHERE IdProject =@IdProject
END

GO

/****** Object:  StoredProcedure [dbo].[parpInsertProjects]    Script Date: 12/03/2015 10:09:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-----------------------------------------------------------------------------------------
Autor   		:Rodrigo Lopez Robles
Fecha Creacion	:09/10/2013
Descripcion     : Adiciona un registra en la tabla tblProjects
===========================================================================================
Modifications:	
Date		Author	Description
---------	------	-----------------------------------------------------------------------
12/3/2015	RM		Added field CaseSelection 

-------------------------------------------------------------------------------------------*/
ALTER PROC [dbo].[parpInsertProjects](
@IdProject numeric output,
@IdModel int,
@Code varchar(150),
@Name varchar(255),
@IdField int,
@Operation int,
@Starts int,
@TypeAllocation Int,
@UserCreation int,
@DateCreation datetime,
@UserModification int,
@DateModification datetime,
@CaseSelection bit)
AS
BEGIN
	
	INSERT INTO tblProjects ([IdModel],[Code],[Name],[IdField],[Operation],[Starts],[TypeAllocation],[UserCreation],[DateCreation],[UserModification],[DateModification],[CaseSelection] )
	VALUES(@IdModel,@Code,@Name,@IdField,@Operation,@Starts,@TypeAllocation,@UserCreation,@DateCreation,@UserModification,@DateModification,@CaseSelection);
	
	SET @IdProject = SCOPE_IDENTITY();
	
END

GO

/****** Object:  StoredProcedure [dbo].[parpInsertTechnicalDriver]    Script Date: 01/06/2016 09:28:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------
Autor   		:Pedro Valverde
Fecha Creacion	:03/10/2013
Descripcion     : Adiciona un registra en la tabla tblTechnicalDrivers
--------------------------------------------*/
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 16-01-06		RM		Added field BaselineAllocationfor v1.2 Req #4
-- ===========================================================================================
ALTER PROC [dbo].[parpInsertTechnicalDriver](
@IdTechnicalDriver numeric output,
@IdModel int,
@Name varchar(150),
@IdUnit int,
@IdTypeOperation int,
@TypeDriver int,
@UserCreation int,
@DateCreation datetime,
@UserModification int,
@DateModification datetime,
@UnitCostDenominator bit,
@BaselineAllocation bit)
AS
BEGIN
	
	INSERT INTO tblTechnicalDrivers ([IdModel],[Name],[IdUnit],[IdTypeOperation],[TypeDriver],[UserCreation],[DateCreation],[UserModification],[DateModification], [UnitCostDenominator], [BaselineAllocation])
	VALUES(@IdModel,@Name,@IdUnit,@IdTypeOperation,@TypeDriver,@UserCreation,@DateCreation,@UserModification,@DateModification,@UnitCostDenominator,@BaselineAllocation);
	
	SET @IdTechnicalDriver = SCOPE_IDENTITY();
	
END

GO

/*-------------------------------------------
Autor   		:Pedro Valverde
Fecha Creacion	:17/10/2013
Descripcion     : Modifica un registro en la tabla tblTechnicalDrivers
--------------------------------------------*/
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 16-01-06		RM		Added field BaselineAllocationfor v1.2 Req #4
-- ===========================================================================================
ALTER PROC [dbo].[parpUpdateTechnicalDriver](
@IdTechnicalDriver int,
@IdModel int,
@Name varchar(100),
@IdUnit int,
@TypeDriver int,
@IdTypeOperation int,
@UserCreation int,
@DateCreation datetime,
@UserModification int,
@DateModification datetime,
@UnitCostDenominator bit,
@BaselineAllocation bit)
AS 
BEGIN
  UPDATE tblTechnicalDrivers SET IdModel=@IdModel,Name=@Name, IdUnit=@IdUnit, TypeDriver=@TypeDriver, IdTypeOperation=@IdTypeOperation, UserCreation=@UserCreation,DateCreation=@DateCreation,UserModification=@UserModification,DateModification=@DateModification,UnitCostDenominator=@UnitCostDenominator,BaselineAllocation=@BaselineAllocation
      WHERE IdTechnicalDriver =@IdTechnicalDriver
END

GO

/****** Object:  StoredProcedure [dbo].[calcModuleAllocation]    Script Date: 01/22/2016 09:42:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[calcModuleAllocation](
@IdModel int
)
AS
BEGIN
	
	/** Precalc **/
	EXEC calcModuleAllocationPreCalc @IdModel;

	/** Cleanup Calc Tables for New Data **/
	DELETE FROM tblCalcAggregationLevels WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcAggregationLevelsField WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcBaseCostByField WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcBaseCostByFieldZiff WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcHierarchyAllocationAmount WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcHierarchyAllocationSumAmount WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcSharedAllocationAmount WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcSharedAllocationSumAmount WHERE [IdModel]=@IdModel;

	/** Aggregation Level Hierarchy **/
	;WITH AggregationLevelsParents AS
	(
	SELECT IdModel, Parent AS IdParent, IdAggregationLevel FROM tblAggregationLevels AS ALMain WHERE Parent IS NOT NULL AND IdModel=@IdModel
	UNION ALL
	SELECT AL.IdModel, AL.Parent AS IdParent, AL2.IdAggregationLevel FROM tblAggregationLevels AS AL INNER JOIN AggregationLevelsParents AS AL2 ON AL.IdAggregationLevel = AL2.IdParent WHERE AL.Parent IS NOT NULL AND AL.IdModel=@IdModel
	)
	INSERT INTO tblCalcAggregationLevels ([IdModel], [IdParent], [IdAggregationLevel])
	SELECT [IdModel], [IdParent], [IdAggregationLevel] FROM AggregationLevelsParents;
	
	/** Aggregation Level Hierarchy Related Fields **/
	INSERT INTO tblCalcAggregationLevelsField ([IdModel], [IdField], [IdAggregationLevel], [IdTypeOperation])
	SELECT IdModel, IdField, IdAggregationLevel, IdTypeOperation FROM tblFields WHERE [IdModel]=@IdModel
	UNION ALL
	SELECT tblCalcAggregationLevels.IdModel, tblFields.IdField, tblCalcAggregationLevels.IdParent AS IdAggregationLevel, tblFields.IdTypeOperation
	FROM tblCalcAggregationLevels INNER JOIN
		  tblFields ON tblCalcAggregationLevels.IdModel = tblFields.IdModel AND 
		  tblCalcAggregationLevels.IdAggregationLevel = tblFields.IdAggregationLevel
	WHERE tblCalcAggregationLevels.IdModel=@IdModel;
    
    /** Company BaseCost Direct Cost Allocations **/
    /** (Initial Insert into table tblCalcBaseCostByField) **/
	INSERT INTO tblCalcBaseCostByField ([IdModel],[IdActivity],[CodeAct],[Activity],[IdClientAccount],[Account],[NameAccount],[IdResources],[CodeRes],[Resource],[IdField],[Field],[Amount],[IdClientCostCenter],
										[ClientCostCenter],[TypeAllocation])
	SELECT D.IdModel, A.IdActivity, A.Code AS CodeAct, A.Name AS Activity, CA.IdClientAccount, CA.Account, CA.Name AS NameAccount, R.IdResources, R.Code AS CodeRes, R.Name AS Resource, F.IdField, F.Name AS Field, 
		ROUND(CCD.Amount, 2) AS Amount, CC.IdClientCostCenter, CC.Name AS ClientCostCenter, 1 AS TypeAllocation
	FROM tblDirectCostAllocation AS D INNER JOIN
		tblClientCostData AS CCD ON CCD.IdClientCostCenter = D.IdClientCostCenter INNER JOIN
		tblClientAccounts AS CA ON CA.IdClientAccount = CCD.IdClientAccount INNER JOIN
		tblClientCostCenters AS CC ON CC.IdClientCostCenter = D.IdClientCostCenter AND CC.TypeAllocation = 1 INNER JOIN
		tblFields AS F ON F.IdField = D.IdField INNER JOIN
		tblActivities AS A ON A.IdActivity = CC.IdActivity INNER JOIN
		tblResources AS R ON R.IdResources = CA.IdResource --INNER JOIN
		--tblAllocationDriverByCostCenter AS ADCC ON CC.IdClientCostCenter = ADCC.IdClientCostCenter INNER JOIN
		--tblAllocationDriversByField AS ADF ON ADF.IdAllocationListDriver = ADCC.IdAllocationListDriver INNER JOIN
		--tblAllocationListDrivers AS ALD ON ADCC.IdAllocationListDriver = ALD.IdAllocationListDriver
	WHERE D.IdModel=@IdModel;

	/** Company BaseCost Shared Cost Allocations **/
	INSERT INTO tblCalcSharedAllocationAmount ([IdModel],[IdActivity],[CodeAct],[Activity],[IdClientAccount],[Account],[NameAccount],[IdResources],[CodeRes],[Resource],[IdField],[Field],
											   [Amount],[IdClientCostCenter],[ClientCostCenter],[TypeAllocation])
	SELECT DISTINCT S.IdModel, A.IdActivity, A.Code AS CodeAct, A.Name AS Activity, CA.IdClientAccount, CA.Account, CA.Name AS NameAccount, R.IdResources, R.Code AS CodeRes, 
		R.Name AS Resource, F.IdField, F.Name AS Field, 
		ISNULL(ROUND(CCD.Amount, 2), 0) AS Amount, CC.IdClientCostCenter, CC.Name AS ClientCostCenter, 2 AS TypeAllocation
	FROM tblSharedCostAllocation AS S INNER JOIN
		tblSharedCostField AS SF ON S.IdSharedCostAllocation = SF.IdSharedCostAllocation INNER JOIN
		tblClientCostData AS CCD ON CCD.IdClientCostCenter = S.IdClientCostCenter INNER JOIN
		tblClientAccounts AS CA ON CA.IdClientAccount = CCD.IdClientAccount INNER JOIN
		tblClientCostCenters AS CC ON CC.IdClientCostCenter = S.IdClientCostCenter AND CC.TypeAllocation = 2 INNER JOIN
		tblFields AS F ON F.IdField = SF.IdField INNER JOIN
		tblAllocationDriverByCostCenter AS ADCC ON ADCC.IdClientCostCenter = S.IdClientCostCenter INNER JOIN
		tblAllocationListDrivers AS ALD ON ALD.IdAllocationListDriver = ADCC.IdAllocationListDriver INNER JOIN
		tblAllocationDriversByField AS ADF ON ADF.IdAllocationListDriver = ALD.IdAllocationListDriver AND ADF.IdField = F.IdField INNER JOIN
		tblActivities AS A ON A.IdActivity = CC.IdActivity INNER JOIN
		tblResources AS R ON R.IdResources = CA.IdResource
    WHERE S.IdModel=@IdModel;

	INSERT INTO tblCalcSharedAllocationSumAmount ([IdModel], [IdResources], [IdField],[SumAmount],[IdClientCostCenter])
	SELECT DISTINCT S.IdModel, R.IdResources, F.IdField, 
		0 + (SELECT (SELECT SUM(ADF2.Amount) AS SumAmount
					 FROM tblSharedCostAllocation AS S2 INNER JOIN
					 	  tblSharedCostField AS SF2 ON S2.IdSharedCostAllocation = SF2.IdSharedCostAllocation INNER JOIN
						  tblClientCostData AS CCD2 ON CCD2.IdClientCostCenter = S2.IdClientCostCenter INNER JOIN
						  tblClientAccounts AS CA2 ON CA2.IdClientAccount = CCD2.IdClientAccount INNER JOIN
						  tblClientCostCenters AS CC2 ON CC2.IdClientCostCenter = S2.IdClientCostCenter AND CC2.TypeAllocation = 2 INNER JOIN
						  tblFields AS F2 ON F2.IdField = SF2.IdField INNER JOIN
						  tblAllocationDriverByCostCenter AS ADCC2 ON ADCC2.IdClientCostCenter = S2.IdClientCostCenter INNER JOIN
						  tblAllocationListDrivers AS ALD2 ON ALD2.IdAllocationListDriver = ADCC2.IdAllocationListDriver INNER JOIN
						  tblAllocationDriversByField AS ADF2 ON ADF2.IdAllocationListDriver = ALD2.IdAllocationListDriver AND 
						  ADF2.IdField = F2.IdField INNER JOIN
						  tblActivities AS A2 ON A2.IdActivity = CC2.IdActivity INNER JOIN
						  tblResources AS R2 ON R2.IdResources = CA2.IdResource
					 WHERE (CA2.IdResource = R.IdResources) AND (CC2.IdClientCostCenter = CC.IdClientCostCenter) AND (F2.IdField = F.IdField)AND (S2.IdModel=@IdModel)
					)
			) AS SumAmount, CC.IdClientCostCenter
	FROM tblSharedCostAllocation AS S INNER JOIN
		tblSharedCostField AS SF ON S.IdSharedCostAllocation = SF.IdSharedCostAllocation INNER JOIN
		tblClientCostData AS CCD ON CCD.IdClientCostCenter = S.IdClientCostCenter INNER JOIN
		tblClientAccounts AS CA ON CA.IdClientAccount = CCD.IdClientAccount INNER JOIN
		tblClientCostCenters AS CC ON CC.IdClientCostCenter = S.IdClientCostCenter AND CC.TypeAllocation = 2 INNER JOIN
		tblFields AS F ON F.IdField = SF.IdField INNER JOIN
		tblAllocationDriverByCostCenter AS ADCC ON ADCC.IdClientCostCenter = S.IdClientCostCenter INNER JOIN
		tblAllocationListDrivers AS ALD ON ALD.IdAllocationListDriver = ADCC.IdAllocationListDriver INNER JOIN
		tblAllocationDriversByField AS ADF ON ADF.IdAllocationListDriver = ALD.IdAllocationListDriver AND ADF.IdField = F.IdField INNER JOIN
		tblActivities AS A ON A.IdActivity = CC.IdActivity INNER JOIN
		tblResources AS R ON R.IdResources = CA.IdResource
    WHERE S.IdModel=@IdModel;
    
	INSERT INTO tblCalcBaseCostByField ([IdModel],[IdActivity],[CodeAct],[Activity],[IdClientAccount],[Account],[NameAccount],[IdResources],[CodeRes],[Resource],[IdField],[Field],[Amount],[IdClientCostCenter],
										[ClientCostCenter],[TypeAllocation])
	SELECT	tt1.IdModel, tt1.IdActivity, tt1.CodeAct, tt1.Activity, tt1.IdClientAccount, tt1.Account, tt1.NameAccount, tt1.IdResources, tt1.CodeRes, 
		tt1.Resource, tt1.IdField, tt1.Field, 
		ISNULL(ROUND(tt1.Amount *
			(SELECT SUM(tt2.SumAmount) AS SumAmmount
			FROM tblCalcSharedAllocationSumAmount tt2
			WHERE (tt2.IdResources = tt1.IdResources) AND (tt2.IdClientCostCenter = tt1.IdClientCostCenter) AND (tt2.IdField = tt1.IdField)) /
					(SELECT SUM(tt3.SumAmount) AS SumAmmountTotal
					FROM tblCalcSharedAllocationSumAmount tt3
					WHERE (tt3.IdResources = tt1.IdResources) AND (tt3.IdClientCostCenter = tt1.IdClientCostCenter)), 2), 0
			) AS Amount, tt1.IdClientCostCenter, tt1.ClientCostCenter, 2 AS TypeAllocation
	FROM tblCalcSharedAllocationAmount tt1
	WHERE tt1.IdModel=@IdModel
	
	/** Company BaseCost Hierarchy Cost Allocations **/
	INSERT 	INTO tblCalcHierarchyAllocationAmount ([IdModel],[IdActivity],[CodeAct],[Activity],[IdClientAccount],[Account],[NameAccount],[IdResources],[CodeRes],[Resource],[IdField],[Field],[Amount],[IdClientCostCenter],
												   [ClientCostCenter],[TypeAllocation],[IdTypeOperation])
	SELECT DISTINCT H.IdModel, CC.IdActivity, A.Code AS CodeAct, A.Name AS Activity, CCD.IdClientAccount, CA.Account, CA.Name AS NameAccount, R.IdResources, R.Code AS CodeRes, 
			R.Name AS Resource, ADF.IdField, F.Name AS Field, CCD.Amount, H.IdClientCostCenter, CC.Name AS ClientCostCenter, 3 AS TypeAllocation, A.IdTypeOperation
	FROM    dbo.tblHierarchyCostAllocation AS H INNER JOIN
			dbo.tblClientCostData AS CCD ON CCD.IdClientCostCenter = H.IdClientCostCenter INNER JOIN
			dbo.tblClientAccounts AS CA ON CA.IdClientAccount = CCD.IdClientAccount INNER JOIN
			dbo.tblClientCostCenters AS CC ON CC.IdClientCostCenter = H.IdClientCostCenter AND CC.TypeAllocation = 3 INNER JOIN
			dbo.tblAllocationDriverByCostCenter AS ADCC ON ADCC.IdClientCostCenter = H.IdClientCostCenter INNER JOIN
			dbo.tblAllocationListDrivers AS ALD ON ALD.IdAllocationListDriver = ADCC.IdAllocationListDriver INNER JOIN
			dbo.tblAllocationDriversByField AS ADF ON ADF.IdAllocationListDriver = ALD.IdAllocationListDriver INNER JOIN
			dbo.tblFields AS F ON F.IdField = ADF.IdField INNER JOIN
			dbo.tblActivities AS A ON A.IdActivity = CC.IdActivity INNER JOIN
			dbo.tblResources AS R ON R.IdResources = CA.IdResource	
	WHERE H.IdModel=@IdModel;

	INSERT INTO tblCalcHierarchyAllocationSumAmount ([IdModel],[IdResources],[IdField],[SumAmount],[IdClientCostCenter])
	SELECT DISTINCT H.IdModel, R.IdResources, F.IdField, 
		0 + (SELECT SUM(ADF2.Amount) AS SumAmount
			FROM tblHierarchyCostAllocation AS H2 INNER JOIN
				tblClientCostData AS CCD2 ON CCD2.IdClientCostCenter = H2.IdClientCostCenter INNER JOIN
				tblClientAccounts AS CA2 ON CA2.IdClientAccount = CCD2.IdClientAccount INNER JOIN
				tblClientCostCenters AS CC2 ON CC2.IdClientCostCenter = H2.IdClientCostCenter AND CC2.TypeAllocation = 3 INNER JOIN
				tblFields AS F2 ON F2.IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=H2.IdAggregationLevel AND (CASE WHEN A.IdTypeOperation=0 THEN 1 ELSE CASE WHEN IdTypeOperation=A.IdTypeOperation THEN 1 ELSE 0 END END = 1)) INNER JOIN
				tblAllocationDriverByCostCenter AS ADCC2 ON ADCC2.IdClientCostCenter = H2.IdClientCostCenter INNER JOIN
				tblAllocationListDrivers AS ALD2 ON ALD2.IdAllocationListDriver = ADCC2.IdAllocationListDriver INNER JOIN
				tblAllocationDriversByField AS ADF2 ON ADF2.IdAllocationListDriver = ALD2.IdAllocationListDriver AND 
				ADF2.IdField = F2.IdField INNER JOIN
				tblActivities AS A2 ON A2.IdActivity = CC2.IdActivity INNER JOIN
				tblResources AS R2 ON R2.IdResources = CA2.IdResource
			WHERE (CA2.IdResource = R.IdResources) AND (CC2.IdClientCostCenter = CC.IdClientCostCenter) AND (F2.IdField = F.IdField) AND (H2.IdModel=@IdModel)
		) AS SumAmount, CC.IdClientCostCenter
	FROM tblHierarchyCostAllocation AS H INNER JOIN
		 tblClientCostData AS CCD ON CCD.IdClientCostCenter = H.IdClientCostCenter INNER JOIN
		 tblClientAccounts AS CA ON CA.IdClientAccount = CCD.IdClientAccount INNER JOIN
		 tblClientCostCenters AS CC ON CC.IdClientCostCenter = H.IdClientCostCenter AND CC.TypeAllocation = 3 INNER JOIN
		 tblAllocationDriverByCostCenter AS ADCC ON ADCC.IdClientCostCenter = H.IdClientCostCenter INNER JOIN
		 tblAllocationListDrivers AS ALD ON ALD.IdAllocationListDriver = ADCC.IdAllocationListDriver INNER JOIN
		 tblAllocationDriversByField AS ADF ON ADF.IdAllocationListDriver = ALD.IdAllocationListDriver INNER JOIN
		 tblFields AS F ON F.IdField = ADF.IdField INNER JOIN
		 tblActivities AS A ON A.IdActivity = CC.IdActivity INNER JOIN
		 tblResources AS R ON R.IdResources = CA.IdResource
	WHERE H.IdModel=@IdModel

	INSERT INTO tblCalcBaseCostByField ([IdModel],[IdActivity],[CodeAct],[Activity],[IdClientAccount],[Account],[NameAccount],[IdResources],[CodeRes],[Resource],[IdField],[Field],[Amount],[IdClientCostCenter],
										[ClientCostCenter],[TypeAllocation])
	SELECT	tt1.IdModel, tt1.IdActivity, tt1.CodeAct, tt1.Activity, tt1.IdClientAccount, tt1.Account, tt1.NameAccount, tt1.IdResources, tt1.CodeRes, tt1.Resource, tt1.IdField, tt1.Field, 
		ISNULL(ROUND(tt1.Amount * (SELECT SUM(tt2.SumAmount) AS SumAmmount
								   FROM tblCalcHierarchyAllocationSumAmount tt2
								   WHERE (tt2.IdResources = tt1.IdResources) AND (tt2.IdClientCostCenter = tt1.IdClientCostCenter) AND (tt2.IdField = tt1.IdField)
								  )
							    / (SELECT SUM(tt3.SumAmount) AS SumAmmountTotal
								   FROM tblCalcHierarchyAllocationSumAmount tt3
								   WHERE (tt3.IdResources = tt1.IdResources) AND (tt3.IdClientCostCenter = tt1.IdClientCostCenter)
								  ), 2), 0) AS Amount, tt1.IdClientCostCenter, tt1.ClientCostCenter, 3 AS TypeAllocation
	FROM tblCalcHierarchyAllocationAmount tt1
	WHERE tt1.IdModel=@IdModel;

	/** Update IdZiffAccount **/
	UPDATE tblCalcBaseCostByField
	SET [IdZiffAccount]=(SELECT [IdZiffAccount] 
						 FROM tblZiffClientMap 
						 WHERE tblZiffClientMap.IdClientAccount=tblCalcBaseCostByField.IdClientAccount AND tblZiffClientMap.IdClientCostCenter=tblCalcBaseCostByField.IdClientCostCenter
						)
	WHERE [IdModel]=@IdModel;
	
	/** Update IdZiffAccount Related Items **/
	UPDATE tblCalcBaseCostByField
	SET [CodeZiff]=tblZiffAccounts.[Code], [ZiffAccount]=tblZiffAccounts.[Name], [IdRootZiffAccount]=tblZiffAccountsRoot.IdZiffAccount, [RootCodeZiff]=tblZiffAccountsRoot.Code, [RootZiffAccount]=tblZiffAccountsRoot.Name
	FROM tblCalcBaseCostByField LEFT OUTER JOIN 
	     tblZiffAccounts ON tblCalcBaseCostByField.IdModel = tblZiffAccounts.IdModel 
	     AND tblCalcBaseCostByField.IdZiffAccount = tblZiffAccounts.IdZiffAccount LEFT OUTER JOIN 
	     tblZiffAccounts AS tblZiffAccountsRoot ON tblZiffAccounts.RootParent = tblZiffAccountsRoot.IdZiffAccount
	WHERE tblCalcBaseCostByField.[IdModel]=@IdModel;
	
	/** Ziff BaseCost **/
	INSERT INTO tblCalcBaseCostByFieldZiff ([IdModel],[IdZiffBaseCost],[IdField],[IdZiffAccount],[IdPeerCriteria],[UnitCost],[Quantity],[TotalCost],[Field],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],
											[RootZiffAccount],[PeerCriteria],[Unit],[IdUnitPeerCriteria],[QuantityNormal],[QuantityPessimistic],[QuantityOptimistic],[TotalCostNormal],[TotalCostPessimistic],
											[TotalCostOptimistic],[IdPeerGroup])
	/** BEGIN OLD CODE **/
	--SELECT vListZiffBaseCostSelections.IdModel, tblZiffBaseCost.IdZiffBaseCost, vListZiffBaseCostSelections.IdField, 
	--	vListZiffBaseCostSelections.IdZiffAccount, tblZiffBaseCost.IdPeerCriteria, tblZiffBaseCost.UnitCost, tblZiffBaseCost.Quantity, tblZiffBaseCost.TotalCost, 
	--	vListZiffBaseCostSelections.Field, vListZiffBaseCostSelections.CodeZiff, vListZiffBaseCostSelections.ZiffAccount, vListZiffBaseCostSelections.IdRootZiffAccount, vListZiffBaseCostSelections.RootCodeZiff, vListZiffBaseCostSelections.RootZiffAccount, 
	--	tblPeerCriteria.Description AS PeerCriteria, tblUnits.Name AS Unit, tblPeerCriteria.IdUnitPeerCriteria, ISNULL(tblZiffBaseCost.IdPeerGroup,0) AS IdPeerGroup
	--FROM tblUnits RIGHT OUTER JOIN
	--	tblPeerCriteria ON tblUnits.IdUnit = tblPeerCriteria.IdUnitPeerCriteria RIGHT OUTER JOIN
	--	tblZiffBaseCost RIGHT OUTER JOIN
	--	vListZiffBaseCostSelections ON tblZiffBaseCost.IdField = vListZiffBaseCostSelections.IdField AND 
	--	tblZiffBaseCost.IdZiffAccount = dbo.vListZiffBaseCostSelections.IdZiffAccount ON dbo.tblPeerCriteria.IdPeerCriteria = dbo.tblZiffBaseCost.IdPeerCriteria
	--WHERE vListZiffBaseCostSelections.[IdModel]=@IdModel;
	/** END OLD CODE **/
	
	/** BEGIN NEW CODE **/
	SELECT vListZiffBaseCostSelections.IdModel, tblZiffBaseCost.IdZiffBaseCost, vListZiffBaseCostSelections.IdField, vListZiffBaseCostSelections.IdZiffAccount, tblZiffBaseCost.IdPeerCriteria, tblZiffBaseCost.UnitCost, 
		tblZiffBaseCost.Quantity, tblZiffBaseCost.TotalCost, vListZiffBaseCostSelections.Field, vListZiffBaseCostSelections.CodeZiff, vListZiffBaseCostSelections.ZiffAccount, vListZiffBaseCostSelections.IdRootZiffAccount, 
		vListZiffBaseCostSelections.RootCodeZiff, vListZiffBaseCostSelections.RootZiffAccount, NULL, NULL, NULL, tblZiffBaseCost.QuantityNormal, tblZiffBaseCost.QuantityPessimistic, tblZiffBaseCost.QuantityOptimistic,
		tblZiffBaseCost.TotalCostNormal, tblZiffBaseCost.TotalCostPessimistic, tblZiffBaseCost.TotalCostOptimistic, ISNULL(tblZiffBaseCost.IdPeerGroup,0) AS IdPeerGroup
	FROM tblZiffBaseCost RIGHT OUTER JOIN
		vListZiffBaseCostSelections ON tblZiffBaseCost.IdField = vListZiffBaseCostSelections.IdField AND 
		tblZiffBaseCost.IdZiffAccount = dbo.vListZiffBaseCostSelections.IdZiffAccount
	WHERE vListZiffBaseCostSelections.[IdModel]=@IdModel;
	/** END NEW CODE **/

END

GO

/****** Object:  StoredProcedure [dbo].[calcModuleAllocationPreCalc]    Script Date: 02/02/2016 08:46:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gareth Slater
-- Create date: 2014-04-03
-- Description:	Module Allocation PreCalc
-- =============================================
ALTER PROCEDURE [dbo].[calcModuleAllocationPreCalc](
@IdModel int
)
AS
BEGIN
	
	/** Update any Missing Cost Structure Assumptions **/
	----INSERT INTO tblCostStructureAssumptionsByProject ([IdProject], [IdZiffAccount], [Client], [Ziff])
	----SELECT CSAALL.IdProject, CSAALL.IdZiffAccount, CSAALL.Client, CSAALL.Ziff
	----FROM vListCostStructureAssumptionsByProjectAll AS CSAALL LEFT OUTER JOIN
	----	  tblCostStructureAssumptionsByProject AS CSA ON CSAALL.IdProject = CSA.IdProject AND 
	----	  CSAALL.IdZiffAccount = CSA.IdZiffAccount
	----WHERE CSA.IdProject IS NULL AND CSA.IdZiffAccount IS NULL AND CSAALL.IdModel=@IdModel;

	--INSERT INTO tblCostStructureAssumptionsByProject ([IdProject], [IdZiffAccount], [Client], [Ziff],[IdPeerGroup])
	--SELECT CSAALL.IdProject, CSAALL.IdZiffAccount, CSAALL.Client, CSAALL.Ziff,PGF.IdPeerGroup
	--FROM vListCostStructureAssumptionsByProjectAll AS CSAALL LEFT OUTER JOIN
	--	  tblCostStructureAssumptionsByProject AS CSA ON CSAALL.IdProject = CSA.IdProject AND 
	--	  CSAALL.IdZiffAccount = CSA.IdZiffAccount INNER JOIN
	--	  tblProjects AS P ON CSAALL.IdProject = P.IdProject INNER JOIN
	--	  tblPeerGroupField AS PGF ON P.IdField = PGF.IdField
	--WHERE CSA.IdProject IS NULL AND CSA.IdZiffAccount IS NULL AND CSAALL.IdModel=@IdModel;
	
	/** Need to check if there are any peer groups **/
	DECLARE @HasPeerGroups INT = 0

	SELECT @HasPeerGroups = COUNT(*) FROM tblPeerGroupField WHERE IdField IN (SELECT IdField FROM tblFields WHERE IdModel = @IdModel)

	IF (@HasPeerGroups=0)
	BEGIN
		INSERT INTO tblCostStructureAssumptionsByProject ([IdProject], [IdZiffAccount], [Client], [Ziff],[IdPeerGroup])
		SELECT CSAALL.IdProject, CSAALL.IdZiffAccount, CSAALL.Client, CSAALL.Ziff, 0
		FROM vListCostStructureAssumptionsByProjectAll AS CSAALL LEFT OUTER JOIN
				  tblCostStructureAssumptionsByProject AS CSA ON CSAALL.IdProject = CSA.IdProject AND 
				  CSAALL.IdZiffAccount = CSA.IdZiffAccount INNER JOIN
				  tblProjects AS P ON CSAALL.IdProject = P.IdProject 
		where CSA.IdProject IS NULL AND CSA.IdZiffAccount IS NULL AND CSAALL.IdModel=@IdModel
	END
	ELSE
	BEGIN
		INSERT INTO tblCostStructureAssumptionsByProject ([IdProject], [IdZiffAccount], [Client], [Ziff],[IdPeerGroup])
		SELECT CSAALL.IdProject, CSAALL.IdZiffAccount, CSAALL.Client, CSAALL.Ziff,PGF.IdPeerGroup
		FROM vListCostStructureAssumptionsByProjectAll AS CSAALL LEFT OUTER JOIN
			  tblCostStructureAssumptionsByProject AS CSA ON CSAALL.IdProject = CSA.IdProject AND 
			  CSAALL.IdZiffAccount = CSA.IdZiffAccount INNER JOIN
			  tblProjects AS P ON CSAALL.IdProject = P.IdProject INNER JOIN
			  tblPeerGroupField AS PGF ON P.IdField = PGF.IdField
		WHERE CSA.IdProject IS NULL AND CSA.IdZiffAccount IS NULL AND CSAALL.IdModel=@IdModel;
	END	

	/** Cleanup Cost Structure Assumptions **/
	DELETE FROM tblCostStructureAssumptionsByProject WHERE IdProject IN (SELECT [IdProject] FROM tblProjects WHERE [Operation]<>1 AND [IdModel]=@IdModel);
	DELETE FROM tblCostStructureAssumptionsByProject WHERE IdZiffAccount IN (SELECT [IdZiffAccount] FROM tblZiffAccounts WHERE [Parent] IS NULL AND [IdModel]=@IdModel);
	
	/** Reset Quantities and Costs for Base Costs **/
	EXEC allpUpdateZiffBaseCostQuantity @IdModel;
		
END

GO

/****** Object:  StoredProcedure [dbo].[calcModuleTechnicalForecast]    Script Date: 01/19/2016 15:13:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---- ================================================================================================
---- Author:		Gareth Slater
---- Create date: 2014-04-20
---- Description:	Module Technical Forecast
---- ================================================================================================ 
---- Modifications:	
---- Date			Author	Description
---- ---------	------	----------------------------------------------------------------------
---- 2015-08-04	RM		Added local variables, made temporary tables in order to speed up execution. 
----						Added split of baselines when necessary
---- ================================================================================================ 
ALTER PROCEDURE [dbo].[calcModuleTechnicalForecast](
@IdModel int,
@MinYear int,
@MaxYear int
) WITH RECOMPILE 
AS
BEGIN

	--SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/** Declare local variables to reduce processing time **/
	DECLARE @LocIdModel INT = @IdModel
	DECLARE @LocMinYear INT = @MinYear
	DECLARE @LocMaxYear INT = @MaxYear
	
	/** Cleanup Calc Tables for New Data **/
	DELETE FROM tblCalcTechnicalSemifixedSelections WHERE [IdModel]=@LocIdModel;
	DELETE FROM tblCalcTechnicalSemifixedAmount WHERE [IdModel]=@LocIdModel;
	DELETE FROM tblCalcTechnicalSemifixedSumAmount WHERE [IdModel]=@LocIdModel;
	DELETE FROM tblCalcTechnicalSemifixedSplit WHERE [IdModel]=@LocIdModel;

	/** Cleanup Calc Tables for New Data **/
	IF OBJECT_ID('tempdb..#tblCalcProjectionTechnical') IS NOT NULL DROP TABLE #tblCalcProjectionTechnical
	IF OBJECT_ID('tempdb..#VolumePerField') IS NOT NULL DROP TABLE #VolumePerField
	IF OBJECT_ID('tempdb..#VolumeJoiner') IS NOT NULL DROP TABLE #VolumeJoiner
	
	/** Temporary table to store Projection data **/
	CREATE TABLE [dbo].[#tblCalcProjectionTechnical](
		[SourceDebug] [int] NULL,
		[IdModel] [int] NOT NULL,
		[IdActivity] [int] NULL,
		[CodeAct] [nvarchar](50) NULL,
		[Activity] [nvarchar](255) NULL,
		[IdResources] [int] NULL,
		[CodeRes] [nvarchar](50) NULL,
		[Resource] [nvarchar](255) NULL,
		[IdClientAccount] [int] NULL,
		[Account] [nvarchar](50) NULL,
		[NameAccount] [nvarchar](255) NULL,
		[IdField] [int] NOT NULL,
		[Field] [nvarchar](255) NULL,
		[IdClientCostCenter] [int] NULL,
		[ClientCostCenter] [nvarchar](255) NULL,
		[Amount] [float] NULL,
		[TypeAllocation] [int] NULL,
		[IdZiffAccount] [int] NULL,
		[CodeZiff] [nvarchar](50) NULL,
		[ZiffAccount] [nvarchar](255) NULL,
		[IdRootZiffAccount] [int] NULL,
		[RootCodeZiff] [nvarchar](50) NULL,
		[RootZiffAccount] [nvarchar](255) NULL,
		[IdProject] [int] NULL,
		[Project] [nvarchar](255) NULL,
		[ProjectionCriteria] [int] NULL,
		[Year] [int] NULL,
		[BaseYear] [bit] NULL,
		[TFNormal] [float] NULL,
		[TFPessimistic] [float] NULL,
		[TFOptimistic] [float] NULL,
		[BCRNormal] [float] NULL,
		[BCRPessimistic] [float] NULL,
		[BCROptimistic] [float] NULL,
		[BaseCostType] [int] NULL,
		[Operation] [int] NULL,
		[CaseSelection] [int] NULL,
		[SortOrder] [int] NULL,
		[IdPeerGroup] [int] NULL
	) ON [PRIMARY]
	
	/** Set Allowed Combinations for Semi-Fixed Assumptions (Shared) **/	
	INSERT INTO tblCalcTechnicalSemifixedSelections ([IdModel],[IdField],[IdZiffAccount],[IdTechnicalDriverSize],[TypeAllocation],[IdActivity],[IdResources],[IdClientAccount],[IdClientCostCenter])
	SELECT tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.IdZiffAccount, tblCalcTechnicalAssumption.IdTechnicalDriverSize, 
		tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.IdClientAccount, 
		tblCalcBaseCostByField.IdClientCostCenter
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalAssumption.IdModel AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalAssumption.IdZiffAccount INNER JOIN
		tblFields ON tblCalcBaseCostByField.IdField = tblFields.IdField AND tblCalcBaseCostByField.IdModel = tblFields.IdModel AND 
		tblCalcTechnicalAssumption.IdTypeOperation = tblFields.IdTypeOperation
	WHERE tblCalcTechnicalAssumption.ProjectionCriteria=2
	GROUP BY tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.TypeAllocation, 
		tblCalcTechnicalAssumption.IdTechnicalDriverSize, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.IdClientAccount, 
		tblCalcBaseCostByField.IdClientCostCenter
	HAVING tblCalcBaseCostByField.TypeAllocation=2 AND tblCalcBaseCostByField.IdModel=@LocIdModel;
	
	/** Set Allowed Combinations for Semi-Fixed Assumptions (Hierarchy) **/
	INSERT INTO tblCalcTechnicalSemifixedSelections ([IdModel],[IdField],[IdZiffAccount],[IdTechnicalDriverSize],[TypeAllocation],[IdActivity],[IdResources],[IdClientAccount],[IdClientCostCenter])
	SELECT tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.IdZiffAccount, tblCalcTechnicalAssumption.IdTechnicalDriverSize, 
		tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.IdClientAccount, 
		tblCalcBaseCostByField.IdClientCostCenter
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalAssumption.IdModel AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalAssumption.IdZiffAccount INNER JOIN
		tblFields ON tblCalcBaseCostByField.IdField = tblFields.IdField AND tblCalcBaseCostByField.IdModel = tblFields.IdModel AND 
		tblCalcTechnicalAssumption.IdTypeOperation = tblFields.IdTypeOperation
	WHERE tblCalcTechnicalAssumption.ProjectionCriteria=2 AND tblCalcBaseCostByField.BCRNormal<>0 AND tblCalcBaseCostByField.BCRPessimistic<>0 AND tblCalcBaseCostByField.BCROptimistic<>0
	GROUP BY tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.TypeAllocation, 
		tblCalcTechnicalAssumption.IdTechnicalDriverSize, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.IdClientAccount, 
		tblCalcBaseCostByField.IdClientCostCenter
	HAVING tblCalcBaseCostByField.TypeAllocation=3 AND tblCalcBaseCostByField.IdModel=@LocIdModel;

	/** Get Tech Driver Totals by Field for Semi-Fixed **/
	INSERT INTO tblCalcTechnicalSemifixedAmount ([IdModel],[IdField],[IdZiffAccount],[IdTechnicalDriverSize],[TypeAllocation],[IdActivity],[IdResources],[IdClientAccount],[IdClientCostCenter],[Year],[Normal],[Pessimistic],[Optimistic])
	SELECT tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalSemifixedSelections.IdField, tblCalcTechnicalSemifixedSelections.IdZiffAccount, 
		tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize, tblCalcTechnicalSemifixedSelections.TypeAllocation, tblCalcTechnicalSemifixedSelections.IdActivity, 
		tblCalcTechnicalSemifixedSelections.IdResources, tblCalcTechnicalSemifixedSelections.IdClientAccount, tblCalcTechnicalSemifixedSelections.IdClientCostCenter, 
		tblCalcTechnicalDriverData.Year, SUM(tblCalcTechnicalDriverData.Normal) AS Normal, SUM(tblCalcTechnicalDriverData.Pessimistic) AS Pessimistic, SUM(tblCalcTechnicalDriverData.Optimistic) 
		AS Optimistic
	FROM tblCalcTechnicalSemifixedSelections INNER JOIN
		tblCalcTechnicalDriverData ON tblCalcTechnicalSemifixedSelections.IdModel = tblCalcTechnicalDriverData.IdModel AND 
		tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize = tblCalcTechnicalDriverData.IdTechnicalDriver AND 
		tblCalcTechnicalSemifixedSelections.IdField = tblCalcTechnicalDriverData.IdField
	GROUP BY tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalSemifixedSelections.IdField, tblCalcTechnicalSemifixedSelections.IdZiffAccount, 
		tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize, tblCalcTechnicalSemifixedSelections.TypeAllocation, tblCalcTechnicalSemifixedSelections.IdActivity, 
		tblCalcTechnicalSemifixedSelections.IdResources, tblCalcTechnicalSemifixedSelections.IdClientAccount, tblCalcTechnicalSemifixedSelections.IdClientCostCenter, 
		tblCalcTechnicalDriverData.Year
	HAVING tblCalcTechnicalDriverData.Year<>@LocMinYear AND tblCalcTechnicalSemifixedSelections.IdModel=@LocIdModel;

	/** Get Tech Driver Totals by Set of Applicable Fields for Semi-Fixed **/
	INSERT INTO tblCalcTechnicalSemifixedSumAmount ([IdModel],[IdZiffAccount],[IdTechnicalDriverSize],[TypeAllocation],[IdActivity],[IdResources],[IdClientAccount],[IdClientCostCenter],[Year],[Normal],[Pessimistic],[Optimistic])
	SELECT tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalSemifixedSelections.IdZiffAccount, tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize, 
		tblCalcTechnicalSemifixedSelections.TypeAllocation, tblCalcTechnicalSemifixedSelections.IdActivity, tblCalcTechnicalSemifixedSelections.IdResources, 
		tblCalcTechnicalSemifixedSelections.IdClientAccount, tblCalcTechnicalSemifixedSelections.IdClientCostCenter, tblCalcTechnicalDriverData.Year, SUM(tblCalcTechnicalDriverData.Normal) 
		AS Normal, SUM(tblCalcTechnicalDriverData.Pessimistic) AS Pessimistic, SUM(tblCalcTechnicalDriverData.Optimistic) AS Optimistic
	FROM tblCalcTechnicalSemifixedSelections INNER JOIN
		tblCalcTechnicalDriverData ON tblCalcTechnicalSemifixedSelections.IdModel = tblCalcTechnicalDriverData.IdModel AND 
		tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize = tblCalcTechnicalDriverData.IdTechnicalDriver AND 
		tblCalcTechnicalSemifixedSelections.IdField = tblCalcTechnicalDriverData.IdField
	GROUP BY tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalSemifixedSelections.IdZiffAccount, tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize, 
		tblCalcTechnicalSemifixedSelections.TypeAllocation, tblCalcTechnicalSemifixedSelections.IdActivity, tblCalcTechnicalSemifixedSelections.IdResources, 
		tblCalcTechnicalSemifixedSelections.IdClientAccount, tblCalcTechnicalSemifixedSelections.IdClientCostCenter, tblCalcTechnicalDriverData.Year
	HAVING tblCalcTechnicalDriverData.Year<>@LocMinYear AND tblCalcTechnicalSemifixedSelections.IdModel=@LocIdModel;

	/** Set Tech Driver Splits for Semi-Fixed **/
	INSERT INTO tblCalcTechnicalSemifixedSplit ([IdModel],[IdField],[IdZiffAccount],[IdTechnicalDriverSize],[TypeAllocation],[IdActivity],[IdResources],[IdClientAccount],[IdClientCostCenter],[Year],[SplitNormal],[SplitPessimistic],[SplitOptimistic])
	SELECT tblCalcTechnicalSemifixedAmount.IdModel, tblCalcTechnicalSemifixedAmount.IdField, tblCalcTechnicalSemifixedAmount.IdZiffAccount, tblCalcTechnicalSemifixedAmount.IdTechnicalDriverSize, 
		tblCalcTechnicalSemifixedAmount.TypeAllocation, tblCalcTechnicalSemifixedAmount.IdActivity, tblCalcTechnicalSemifixedAmount.IdResources, tblCalcTechnicalSemifixedAmount.IdClientAccount, 
		tblCalcTechnicalSemifixedAmount.IdClientCostCenter, tblCalcTechnicalSemifixedAmount.Year, 
		CASE WHEN ISNULL(tblCalcTechnicalSemifixedSumAmount.Normal, 0) = 0 THEN 0 ELSE tblCalcTechnicalSemifixedAmount.Normal / tblCalcTechnicalSemifixedSumAmount.Normal END AS SplitNormal, 
		CASE WHEN ISNULL(tblCalcTechnicalSemifixedSumAmount.Pessimistic, 0) = 0 THEN 0 ELSE tblCalcTechnicalSemifixedAmount.Pessimistic / tblCalcTechnicalSemifixedSumAmount.Pessimistic END AS SplitPessimistic, 
		CASE WHEN ISNULL(tblCalcTechnicalSemifixedSumAmount.Optimistic, 0) = 0 THEN 0 ELSE tblCalcTechnicalSemifixedAmount.Optimistic / tblCalcTechnicalSemifixedSumAmount.Optimistic END AS SplitOptimistic
	FROM tblCalcTechnicalSemifixedAmount INNER JOIN
		tblCalcTechnicalSemifixedSumAmount ON tblCalcTechnicalSemifixedAmount.IdModel = tblCalcTechnicalSemifixedSumAmount.IdModel AND 
		tblCalcTechnicalSemifixedAmount.IdZiffAccount = tblCalcTechnicalSemifixedSumAmount.IdZiffAccount AND 
		tblCalcTechnicalSemifixedAmount.IdTechnicalDriverSize = tblCalcTechnicalSemifixedSumAmount.IdTechnicalDriverSize AND 
		tblCalcTechnicalSemifixedAmount.TypeAllocation = tblCalcTechnicalSemifixedSumAmount.TypeAllocation AND 
		tblCalcTechnicalSemifixedAmount.IdActivity = tblCalcTechnicalSemifixedSumAmount.IdActivity AND 
		tblCalcTechnicalSemifixedAmount.IdResources = tblCalcTechnicalSemifixedSumAmount.IdResources AND 
		tblCalcTechnicalSemifixedAmount.IdClientAccount = tblCalcTechnicalSemifixedSumAmount.IdClientAccount AND 
		tblCalcTechnicalSemifixedAmount.IdClientCostCenter = tblCalcTechnicalSemifixedSumAmount.IdClientCostCenter AND 
		tblCalcTechnicalSemifixedAmount.Year = tblCalcTechnicalSemifixedSumAmount.Year
	WHERE tblCalcTechnicalSemifixedAmount.Year<>@LocMinYear AND tblCalcTechnicalSemifixedAmount.IdModel=@LocIdModel;
	
	/** Update Ziff Semi-Fixed Forecast to Fixed **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET [ProjectionCriteria]=1 WHERE [ProjectionCriteria]=2 AND [IdModel]=@LocIdModel;

	/** (1) Insert Company Base Year Into #tblCalcProjectionTechnical **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],
											 [ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],
											 [Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[CaseSelection])
	SELECT 1, tblCalcBaseCostByField.IdModel,tblCalcBaseCostByField.IdActivity,tblCalcBaseCostByField.CodeAct,tblCalcBaseCostByField.Activity,tblCalcBaseCostByField.IdResources,tblCalcBaseCostByField.CodeRes,
		tblCalcBaseCostByField.Resource,tblCalcBaseCostByField.IdClientAccount,tblCalcBaseCostByField.Account,tblCalcBaseCostByField.NameAccount,tblCalcBaseCostByField.IdField,tblCalcBaseCostByField.Field,
		tblCalcBaseCostByField.IdClientCostCenter,tblCalcBaseCostByField.ClientCostCenter,tblCalcBaseCostByField.Amount,tblCalcBaseCostByField.TypeAllocation,tblCalcBaseCostByField.IdZiffAccount,
		tblCalcBaseCostByField.CodeZiff,tblCalcBaseCostByField.ZiffAccount,tblCalcBaseCostByField.IdRootZiffAccount,[RootCodeZiff],[RootZiffAccount],tblCalcTechnicalDriverAssumption.IdProject,
		tblCalcTechnicalDriverAssumption.ProjectionCriteria,tblCalcTechnicalDriverAssumption.Year,tblCalcTechnicalDriverAssumption.BaseYear,
		tblCalcTechnicalDriverAssumption.TFNormal,tblCalcTechnicalDriverAssumption.TFPessimistic,tblCalcTechnicalDriverAssumption.TFOptimistic,
		CASE WHEN tblCalcTechnicalDriverAssumption.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumption.Operation=1 THEN tblCalcBaseCostByField.BCRNormal * tblCalcBaseCostByField.TFNormal ELSE tblCalcBaseCostByField.BCRNormal END,
		CASE WHEN tblCalcTechnicalDriverAssumption.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumption.Operation=1 THEN tblCalcBaseCostByField.BCRPessimistic * tblCalcBaseCostByField.TFPessimistic ELSE tblCalcBaseCostByField.BCRPessimistic END,
		CASE WHEN tblCalcTechnicalDriverAssumption.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumption.Operation=1 THEN tblCalcBaseCostByField.BCROptimistic * tblCalcBaseCostByField.TFOptimistic ELSE tblCalcBaseCostByField.BCROptimistic END,
		1 AS BaseCostType,tblCalcTechnicalDriverAssumption.Operation, ISNULL(tblProjects.CaseSelection,0) AS CaseSelection
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount LEFT OUTER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumption.IdProject = tblProjects.IdProject
	WHERE tblCalcTechnicalDriverAssumption.BaseYear=1 AND tblCalcBaseCostByField.IdModel=@LocIdModel;
	
	/** (2) Insert Ziff Base Year Into #tblCalcProjectionTechnical (If Applicable) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],
											 [ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],
											 [Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[CaseSelection],[IdPeerGroup])
	SELECT 2, tblCalcBaseCostByFieldZiff.IdModel,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,tblCalcBaseCostByFieldZiff.IdField,tblCalcBaseCostByFieldZiff.Field,NULL,NULL,tblCalcBaseCostByFieldZiff.TotalCost,NULL,
		tblCalcBaseCostByFieldZiff.IdZiffAccount,tblCalcBaseCostByFieldZiff.CodeZiff,tblCalcBaseCostByFieldZiff.ZiffAccount,tblCalcBaseCostByFieldZiff.IdRootZiffAccount,[RootCodeZiff],[RootZiffAccount],
		tblCalcTechnicalDriverAssumptionZiff.IdProject,tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria,tblCalcTechnicalDriverAssumptionZiff.Year,tblCalcTechnicalDriverAssumptionZiff.BaseYear,
		tblCalcTechnicalDriverAssumptionZiff.TFNormal,tblCalcTechnicalDriverAssumptionZiff.TFPessimistic,tblCalcTechnicalDriverAssumptionZiff.TFOptimistic,
		--CASE WHEN tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 THEN tblCalcBaseCostByFieldZiff.BCRNormal * tblCalcBaseCostByFieldZiff.TFNormal ELSE tblCalcBaseCostByFieldZiff.TotalCostNormal END,--CASE WHEN tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=1 THEN  tblCalcBaseCostByFieldZiff.TotalCostNormal ELSE tblCalcBaseCostByFieldZiff.BCRNormal END END,
		--CASE WHEN tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 THEN tblCalcBaseCostByFieldZiff.BCRPessimistic * tblCalcBaseCostByFieldZiff.TFPessimistic ELSE tblCalcBaseCostByFieldZiff.TotalCostPessimistic END,-- CASE WHEN tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=1 THEN  tblCalcBaseCostByFieldZiff.TotalCostPessimistic ELSE tblCalcBaseCostByFieldZiff.BCRPessimistic END END,
		--CASE WHEN tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 THEN tblCalcBaseCostByFieldZiff.BCROptimistic * tblCalcBaseCostByFieldZiff.TFOptimistic ELSE tblCalcBaseCostByFieldZiff.BCROptimistic END,-- tblCalcBaseCostByFieldZiff.TotalCostOptimistic END,--CASE WHEN tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=1 THEN  tblCalcBaseCostByFieldZiff.TotalCostOptimistic ELSE tblCalcBaseCostByFieldZiff.BCROptimistic END END,
		CASE WHEN tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=1 THEN  tblCalcBaseCostByFieldZiff.TotalCostNormal WHEN tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=3 THEN tblCalcBaseCostByFieldZiff.BCRNormal ELSE tblCalcTechnicalDriverAssumptionZiff.BCRNormal END,
		CASE WHEN tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=1 THEN  tblCalcBaseCostByFieldZiff.TotalCostPessimistic WHEN tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=3 THEN tblCalcBaseCostByFieldZiff.BCRPessimistic ELSE tblCalcTechnicalDriverAssumptionZiff.BCRPessimistic END,
		CASE WHEN tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=1 THEN  tblCalcBaseCostByFieldZiff.TotalCostOptimistic WHEN tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=3 THEN tblCalcBaseCostByFieldZiff.BCROptimistic ELSE tblCalcTechnicalDriverAssumptionZiff.BCROptimistic END, 
		2 AS BaseCostType,tblCalcTechnicalDriverAssumptionZiff.Operation, ISNULL(tblProjects.CaseSelection,0) AS CaseSelection, tblCalcBaseCostByFieldZiff.IdPeerGroup
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount  AND
	tblCalcBaseCostByFieldZiff.IdPeerGroup = tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup LEFT OUTER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumptionZiff.IdProject = tblProjects.IdProject
	WHERE tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 
	AND tblCalcBaseCostByFieldZiff.IdModel=@LocIdModel;
	
	/** (3) Update Company Projection - Semi-Fixed (Direct Portion) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],
											 [ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],
											 [Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[CaseSelection])
	SELECT 3, tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.CodeAct, tblCalcBaseCostByField.Activity, tblCalcBaseCostByField.IdResources, 
		tblCalcBaseCostByField.CodeRes, tblCalcBaseCostByField.Resource, tblCalcBaseCostByField.IdClientAccount, tblCalcBaseCostByField.Account, tblCalcBaseCostByField.NameAccount, 
		tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.Field, tblCalcBaseCostByField.IdClientCostCenter, tblCalcBaseCostByField.ClientCostCenter, tblCalcBaseCostByField.Amount, 
		tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.CodeZiff, tblCalcBaseCostByField.ZiffAccount, 
		tblCalcBaseCostByField.IdRootZiffAccount, tblCalcBaseCostByField.RootCodeZiff, tblCalcBaseCostByField.RootZiffAccount, 0 AS IdProject, 2 AS ProjectionCriteria, tblCalcModelYears.Year, 
		CAST(0 AS BIT) AS BaseYear, tblCalcBaseCostByField.TFNormal, tblCalcBaseCostByField.TFPessimistic, tblCalcBaseCostByField.TFOptimistic, tblCalcBaseCostByField.BCRNormal, 
		tblCalcBaseCostByField.BCRPessimistic, tblCalcBaseCostByField.BCROptimistic, 1 AS BaseCostType, 1 AS Operation, 0 AS CaseSelection
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcModelYears ON tblCalcBaseCostByField.IdModel = tblCalcModelYears.IdModel INNER JOIN
		tblCalcTechnicalAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalAssumption.IdModel AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalAssumption.IdZiffAccount
	WHERE tblCalcBaseCostByField.TypeAllocation=1 AND tblCalcModelYears.Year<=@LocMaxYear AND tblCalcTechnicalAssumption.ProjectionCriteria=2 AND tblCalcBaseCostByField.IdModel=@LocIdModel;
	
	/** (4) Update Company Projection - Semi-Fixed (Shared) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],
											 [ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],
											 [Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[CaseSelection])
	SELECT 4, CBCSUM.IdModel, CBCSUM.IdActivity, CBCSUM.CodeAct, CBCSUM.Activity, CBCSUM.IdResources, CBCSUM.CodeRes, CBCSUM.Resource, CBCSUM.IdClientAccount, CBCSUM.Account, CBCSUM.NameAccount, 
		tblCalcTechnicalSemifixedSplit.IdField, NULL AS Field, CBCSUM.IdClientCostCenter, CBCSUM.ClientCostCenter, CBCSUM.Amount, CBCSUM.TypeAllocation, CBCSUM.IdZiffAccount, CBCSUM.CodeZiff, 
		CBCSUM.ZiffAccount, CBCSUM.IdRootZiffAccount, CBCSUM.RootCodeZiff, CBCSUM.RootZiffAccount, 0 AS IdProject, 2 AS ProjectionCriteria, tblCalcTechnicalSemifixedSplit.Year, 
		CAST(0 AS BIT) AS BaseYear, 1 AS TFNormal, 1 AS TFPessimistic, 1 AS TFOptimistic, CBCSUM.Amount * tblCalcTechnicalSemifixedSplit.SplitNormal AS BCRNormal, 
		CBCSUM.Amount * tblCalcTechnicalSemifixedSplit.SplitPessimistic AS BCRPessimistic, CBCSUM.Amount * tblCalcTechnicalSemifixedSplit.SplitOptimistic AS BCROptimistic,
		1 AS BaseCostType, 1 AS Operation, 0 AS CaseSelection
	FROM
	(
		SELECT IdModel, IdActivity, CodeAct, Activity, IdResources, CodeRes, Resource, IdClientAccount, Account, NameAccount, IdClientCostCenter, ClientCostCenter, SUM(Amount) AS Amount, TypeAllocation, 
			IdZiffAccount, CodeZiff, ZiffAccount, IdRootZiffAccount, RootCodeZiff, RootZiffAccount
		FROM tblCalcBaseCostByField
		GROUP BY IdModel, IdActivity, CodeAct, Activity, IdResources, CodeRes, Resource, IdClientAccount, Account, NameAccount, IdClientCostCenter, ClientCostCenter, TypeAllocation, IdZiffAccount, CodeZiff, 
			ZiffAccount, IdRootZiffAccount, RootCodeZiff, RootZiffAccount
		HAVING TypeAllocation<>1 AND IdModel=@LocIdModel
	) AS CBCSUM INNER JOIN
		tblCalcTechnicalSemifixedSplit ON CBCSUM.IdModel = tblCalcTechnicalSemifixedSplit.IdModel AND CBCSUM.IdActivity = tblCalcTechnicalSemifixedSplit.IdActivity AND 
		CBCSUM.IdResources = tblCalcTechnicalSemifixedSplit.IdResources AND CBCSUM.IdClientAccount = tblCalcTechnicalSemifixedSplit.IdClientAccount AND 
		CBCSUM.IdClientCostCenter = tblCalcTechnicalSemifixedSplit.IdClientCostCenter AND CBCSUM.IdZiffAccount = tblCalcTechnicalSemifixedSplit.IdZiffAccount AND 
		CBCSUM.TypeAllocation = tblCalcTechnicalSemifixedSplit.TypeAllocation;
	
	/** 1 - Fixed, 2 - SemiFixed, 3 - Variable, 4 - Cyclical, 5 - SemiVariable **/
	/** (5) Update Company Projection - Fixed **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],
											 [ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],
											 [Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[CaseSelection],[IdPeerGroup])
	SELECT DISTINCT 5, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, 
		#tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, #tblCalcProjectionTechnical.NameAccount, 
		#tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, tblCalcTechnicalDriverAssumption.Year, CAST(0 AS BIT) AS BaseYear, 
		#tblCalcProjectionTechnical.TFNormal,#tblCalcProjectionTechnical.TFPessimistic,#tblCalcProjectionTechnical.TFOptimistic,#tblCalcProjectionTechnical.BCRNormal, #tblCalcProjectionTechnical.BCRPessimistic, 
		#tblCalcProjectionTechnical.BCROptimistic, 1 AS BaseCostType, tblCalcTechnicalDriverAssumption.Operation, ISNULL(tblProjects.CaseSelection,0) AS CaseSelection,#tblCalcProjectionTechnical.IdPeerGroup
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		#tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumption.IdModel = #tblCalcProjectionTechnical.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = #tblCalcProjectionTechnical.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = #tblCalcProjectionTechnical.IdField LEFT OUTER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumption.IdProject = tblProjects.IdProject
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=1) 
	AND #tblCalcProjectionTechnical.BaseYear=1 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND #tblCalcProjectionTechnical.BaseCostType=1 
	AND tblCalcTechnicalDriverAssumption.IdModel=@LocIdModel;
		
	/** (6) Update Ziff Projection - Fixed **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],
											 [ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],
											 [Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[CaseSelection],[IdPeerGroup])
	SELECT DISTINCT 6, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, 
		#tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, #tblCalcProjectionTechnical.NameAccount, 
		#tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.Year, CAST(0 AS BIT) AS BaseYear, 
		#tblCalcProjectionTechnical.TFNormal, #tblCalcProjectionTechnical.TFPessimistic, #tblCalcProjectionTechnical.TFOptimistic, 
		#tblCalcProjectionTechnical.BCRNormal, #tblCalcProjectionTechnical.BCRPessimistic, #tblCalcProjectionTechnical.BCROptimistic, 2 AS BaseCostType, tblCalcTechnicalDriverAssumptionZiff.Operation, 
		ISNULL(tblProjects.CaseSelection,0) AS CaseSelection,#tblCalcProjectionTechnical.IdPeerGroup
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		#tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumptionZiff.IdModel = #tblCalcProjectionTechnical.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = #tblCalcProjectionTechnical.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = #tblCalcProjectionTechnical.IdField  AND
		tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup = #tblCalcProjectionTechnical.IdPeerGroup LEFT OUTER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumptionZiff.IdProject = tblProjects.IdProject
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=1) 
	AND #tblCalcProjectionTechnical.BaseYear=1 
	AND #tblCalcProjectionTechnical.BaseCostType=2 
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0 
	AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@LocIdModel;

	/** (7) Update Company Projection - Variable and Cyclical **/
	/** Baselines **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],
											 [ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],
											 [Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[CaseSelection])
	SELECT 7, tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.CodeAct, tblCalcBaseCostByField.Activity, 
		tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.CodeRes, tblCalcBaseCostByField.Resource, 
		tblCalcBaseCostByField.IdClientAccount, tblCalcBaseCostByField.Account, tblCalcBaseCostByField.NameAccount, tblCalcBaseCostByField.IdField, 
		tblCalcBaseCostByField.Field, tblCalcBaseCostByField.IdClientCostCenter, tblCalcBaseCostByField.ClientCostCenter, 
		tblCalcBaseCostByField.Amount, tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.CodeZiff, 
		tblCalcBaseCostByField.ZiffAccount, tblCalcBaseCostByField.IdRootZiffAccount, tblCalcBaseCostByField.RootCodeZiff, 
		tblCalcBaseCostByField.RootZiffAccount, tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumption.Year, tblCalcTechnicalDriverAssumption.BaseYear, 
		tblCalcTechnicalDriverAssumption.TFNormal,tblCalcTechnicalDriverAssumption.TFPessimistic,tblCalcTechnicalDriverAssumption.TFOptimistic, 
		/** ORIGINAL CODE tblCalcBaseCostByField.BCRNormal, tblCalcBaseCostByField.BCRPessimistic, tblCalcBaseCostByField.BCROptimistic, 1 AS BaseCostType, **/
		--CASE WHEN tblCalcTechnicalDriverAssumption.ProjectionCriteria=4 THEN 0 ELSE tblCalcBaseCostByField.BCRNormal END, 
		--CASE WHEN tblCalcTechnicalDriverAssumption.ProjectionCriteria=4 THEN 0 ELSE tblCalcBaseCostByField.BCRPessimistic END, 
		--CASE WHEN tblCalcTechnicalDriverAssumption.ProjectionCriteria=4 THEN 0 ELSE tblCalcBaseCostByField.BCROptimistic END, 1 AS BaseCostType, 
		tblCalcBaseCostByField.BCRNormal, tblCalcBaseCostByField.BCRPessimistic, tblCalcBaseCostByField.BCROptimistic, 1 AS BaseCostType,
		tblCalcTechnicalDriverAssumption.Operation, ISNULL(tblProjects.CaseSelection,0) AS CaseSelection
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount LEFT OUTER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumption.IdProject = tblProjects.IdProject
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=3 OR tblCalcTechnicalDriverAssumption.ProjectionCriteria=4) 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND tblCalcBaseCostByField.IdModel=@LocIdModel
	AND tblCalcTechnicalDriverAssumption.Operation = 1;

	/** (8) Business Opportunities **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],
											 [ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],
											 [Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[CaseSelection])
	SELECT 8, tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.CodeAct, tblCalcBaseCostByField.Activity, 
		tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.CodeRes, tblCalcBaseCostByField.Resource, 
		tblCalcBaseCostByField.IdClientAccount, tblCalcBaseCostByField.Account, tblCalcBaseCostByField.NameAccount, tblCalcBaseCostByField.IdField, 
		tblCalcBaseCostByField.Field, tblCalcBaseCostByField.IdClientCostCenter, tblCalcBaseCostByField.ClientCostCenter, 
		tblCalcBaseCostByField.Amount, tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.CodeZiff, 
		tblCalcBaseCostByField.ZiffAccount, tblCalcBaseCostByField.IdRootZiffAccount, tblCalcBaseCostByField.RootCodeZiff, 
		tblCalcBaseCostByField.RootZiffAccount, tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumption.Year, tblCalcTechnicalDriverAssumption.BaseYear, 
		tblCalcTechnicalDriverAssumption.TFNormal, tblCalcTechnicalDriverAssumption.TFPessimistic, tblCalcTechnicalDriverAssumption.TFOptimistic, 
		tblCalcBaseCostByField.BCRNormal, tblCalcBaseCostByField.BCRPessimistic, tblCalcBaseCostByField.BCROptimistic, 1 AS BaseCostType, 
		tblCalcTechnicalDriverAssumption.Operation, ISNULL(tblProjects.CaseSelection,0) AS CaseSelection
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount LEFT OUTER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumption.IdProject = tblProjects.IdProject
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=3 OR tblCalcTechnicalDriverAssumption.ProjectionCriteria=4) 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND tblCalcBaseCostByField.IdModel=@LocIdModel
	AND tblCalcTechnicalDriverAssumption.Operation <> 1;

	/** (9) Update Ziff Projection - Variable and Cyclical **/
	/** Baselines **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],
											 [ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],
											 [Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[CaseSelection],[IdPeerGroup])
	SELECT 9, tblCalcBaseCostByFieldZiff.IdModel, NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, tblCalcBaseCostByFieldZiff.IdField, tblCalcBaseCostByFieldZiff.Field, NULL, NULL, tblCalcBaseCostByFieldZiff.TotalCost,
		NULL, tblCalcBaseCostByFieldZiff.IdZiffAccount, tblCalcBaseCostByFieldZiff.CodeZiff, tblCalcBaseCostByFieldZiff.ZiffAccount, tblCalcBaseCostByFieldZiff.IdRootZiffAccount, tblCalcBaseCostByFieldZiff.RootCodeZiff, 
		tblCalcBaseCostByFieldZiff.RootZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.Year, 
		tblCalcTechnicalDriverAssumptionZiff.BaseYear, 
		tblCalcTechnicalDriverAssumptionZiff.TFNormal, tblCalcTechnicalDriverAssumptionZiff.TFPessimistic, tblCalcTechnicalDriverAssumptionZiff.TFOptimistic,/*tblCalcBaseCostByFieldZiff.TFNormal,tblCalcBaseCostByFieldZiff.TFPessimistic,tblCalcBaseCostByFieldZiff.TFOptimistic,*/ 
		tblCalcBaseCostByFieldZiff.BCRNormal, tblCalcBaseCostByFieldZiff.BCRPessimistic, tblCalcBaseCostByFieldZiff.BCROptimistic, 
		2 AS BaseCostType, tblCalcTechnicalDriverAssumptionZiff.Operation, ISNULL(tblProjects.CaseSelection,0) AS CaseSelection,
		tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount  AND
		tblCalcBaseCostByFieldZiff.IdPeerGroup = tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup LEFT OUTER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumptionZiff.IdProject = tblProjects.IdProject
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=3 OR tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=4) 
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0 
	AND tblCalcBaseCostByFieldZiff.IdModel=@LocIdModel
	AND tblCalcTechnicalDriverAssumptionZiff.Operation=1;

	/** (10) Business Opportunities **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],
											 [ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],
											 [Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[CaseSelection],[IdPeerGroup])
	SELECT 10, tblCalcBaseCostByFieldZiff.IdModel, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, tblCalcBaseCostByFieldZiff.IdField, tblCalcBaseCostByFieldZiff.Field, NULL, NULL, tblCalcBaseCostByFieldZiff.TotalCost, 
		NULL, tblCalcBaseCostByFieldZiff.IdZiffAccount, tblCalcBaseCostByFieldZiff.CodeZiff, tblCalcBaseCostByFieldZiff.ZiffAccount, tblCalcBaseCostByFieldZiff.IdRootZiffAccount, tblCalcBaseCostByFieldZiff.RootCodeZiff, 
		tblCalcBaseCostByFieldZiff.RootZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.Year, 
		tblCalcTechnicalDriverAssumptionZiff.BaseYear, 
		tblCalcTechnicalDriverAssumptionZiff.TFNormal, tblCalcTechnicalDriverAssumptionZiff.TFPessimistic, tblCalcTechnicalDriverAssumptionZiff.TFOptimistic, 
		tblCalcBaseCostByFieldZiff.BCRNormal, tblCalcBaseCostByFieldZiff.BCRPessimistic, tblCalcBaseCostByFieldZiff.BCROptimistic, 
		2 AS BaseCostType, tblCalcTechnicalDriverAssumptionZiff.Operation, 
		ISNULL(tblProjects.CaseSelection,0) AS CaseSelection,tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount  AND
		tblCalcBaseCostByFieldZiff.IdPeerGroup = tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup LEFT OUTER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumptionZiff.IdProject = tblProjects.IdProject
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=3 OR tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=4)
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0 
	AND tblCalcBaseCostByFieldZiff.IdModel=@LocIdModel
	AND tblCalcTechnicalDriverAssumptionZiff.Operation<>1;

	/** (11) Update Company Projection - SemiVariable (BaseLine) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],
											 [ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],
											 [Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[CaseSelection],[IdPeerGroup])
	SELECT DISTINCT 11, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, 
		#tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, #tblCalcProjectionTechnical.NameAccount, 
		#tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, tblCalcTechnicalDriverAssumption.Year, CAST(0 AS BIT) AS BaseYear, 
		#tblCalcProjectionTechnical.TFNormal, #tblCalcProjectionTechnical.TFPessimistic, #tblCalcProjectionTechnical.TFOptimistic, 
		#tblCalcProjectionTechnical.BCRNormal, #tblCalcProjectionTechnical.BCRPessimistic, #tblCalcProjectionTechnical.BCROptimistic, 1 AS BaseCostType, tblCalcTechnicalDriverAssumption.Operation, 
		ISNULL(tblProjects.CaseSelection,0) AS CaseSelection,#tblCalcProjectionTechnical.IdPeerGroup
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		#tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumption.IdModel = #tblCalcProjectionTechnical.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = #tblCalcProjectionTechnical.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = #tblCalcProjectionTechnical.IdField AND
		tblCalcTechnicalDriverAssumption.IdProject = #tblCalcProjectionTechnical.IdProject LEFT OUTER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumption.IdProject = tblProjects.IdProject
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=5) 
	AND #tblCalcProjectionTechnical.BaseYear=1 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND #tblCalcProjectionTechnical.BaseCostType=1 
	AND #tblCalcProjectionTechnical.Operation=1
	AND tblCalcTechnicalDriverAssumption.IdModel=@LocIdModel;

	/** (12) Update Ziff Projection - SemiVariable (BaseLine) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],
											 [ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],
											 [Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[CaseSelection],[IdPeerGroup])
	SELECT DISTINCT 12, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, 
		#tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, #tblCalcProjectionTechnical.NameAccount, 
		#tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.Year, CAST(0 AS BIT) AS BaseYear, 
		tblCalcTechnicalDriverAssumptionZiff.TFNormal,tblCalcTechnicalDriverAssumptionZiff.TFPessimistic,tblCalcTechnicalDriverAssumptionZiff.TFOptimistic,
		tblCalcTechnicalDriverAssumptionZiff.BCRNormal,tblCalcTechnicalDriverAssumptionZiff.BCRPessimistic,tblCalcTechnicalDriverAssumptionZiff.BCROptimistic,
		/*0,0,0,0,0,0,*/ 2 AS BaseCostType, tblCalcTechnicalDriverAssumptionZiff.Operation, 
		ISNULL(tblProjects.CaseSelection,0) AS CaseSelection,#tblCalcProjectionTechnical.IdPeerGroup
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		#tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumptionZiff.IdModel = #tblCalcProjectionTechnical.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = #tblCalcProjectionTechnical.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = #tblCalcProjectionTechnical.IdField  AND
		tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup = #tblCalcProjectionTechnical.IdPeerGroup LEFT OUTER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumptionZiff.IdProject = tblProjects.IdProject		
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5) 
	AND #tblCalcProjectionTechnical.BaseYear=1 
	AND #tblCalcProjectionTechnical.BaseCostType=2 
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0
	AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 
	AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@LocIdModel;

	/** (13) Update Company Projection - SemiVariable (Business Opportunity) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],
											 [ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],
											 [Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[CaseSelection])
	SELECT 13, tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.CodeAct, tblCalcBaseCostByField.Activity, 
		tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.CodeRes, tblCalcBaseCostByField.Resource, 
		tblCalcBaseCostByField.IdClientAccount, tblCalcBaseCostByField.Account, tblCalcBaseCostByField.NameAccount, tblCalcBaseCostByField.IdField, 
		tblCalcBaseCostByField.Field, tblCalcBaseCostByField.IdClientCostCenter, tblCalcBaseCostByField.ClientCostCenter, 
		tblCalcBaseCostByField.Amount, tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.CodeZiff, 
		tblCalcBaseCostByField.ZiffAccount, tblCalcBaseCostByField.IdRootZiffAccount, tblCalcBaseCostByField.RootCodeZiff, 
		tblCalcBaseCostByField.RootZiffAccount, tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumption.Year, tblCalcTechnicalDriverAssumption.BaseYear, 
		tblCalcTechnicalDriverAssumption.TFNormal, tblCalcTechnicalDriverAssumption.TFPessimistic, tblCalcTechnicalDriverAssumption.TFOptimistic, 
		tblCalcBaseCostByField.BCRNormal, tblCalcBaseCostByField.BCRPessimistic,tblCalcBaseCostByField.BCROptimistic, 
		1 AS BaseCostType, tblCalcTechnicalDriverAssumption.Operation, ISNULL(tblProjects.CaseSelection,0) AS CaseSelection
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount LEFT OUTER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumption.IdProject = tblProjects.IdProject
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=5) 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND tblCalcTechnicalDriverAssumption.Operation=2
	AND tblCalcBaseCostByField.IdModel=@LocIdModel;

	/** (14) Update Ziff Projection - SemiVariable (Business Opportunity)  **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],
											 [ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],
											 [Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[CaseSelection],[IdPeerGroup])
	SELECT 14, tblCalcBaseCostByFieldZiff.IdModel, NULL, NULL, NULL, NULL, NULL, NULL, 
		NULL, NULL, NULL, tblCalcBaseCostByFieldZiff.IdField, tblCalcBaseCostByFieldZiff.Field, NULL, NULL, 
		tblCalcBaseCostByFieldZiff.TotalCost, NULL, tblCalcBaseCostByFieldZiff.IdZiffAccount, tblCalcBaseCostByFieldZiff.CodeZiff, 
		tblCalcBaseCostByFieldZiff.ZiffAccount, tblCalcBaseCostByFieldZiff.IdRootZiffAccount, tblCalcBaseCostByFieldZiff.RootCodeZiff, 
		tblCalcBaseCostByFieldZiff.RootZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumptionZiff.Year, tblCalcTechnicalDriverAssumptionZiff.BaseYear, 
		tblCalcTechnicalDriverAssumptionZiff.TFNormal, tblCalcTechnicalDriverAssumptionZiff.TFPessimistic, tblCalcTechnicalDriverAssumptionZiff.TFOptimistic, 
		tblCalcTechnicalDriverAssumptionZiff.BCRNormal, tblCalcTechnicalDriverAssumptionZiff.BCRPessimistic, tblCalcTechnicalDriverAssumptionZiff.BCROptimistic, 
		--tblCalcBaseCostByFieldZiff.BCRNormal, tblCalcBaseCostByFieldZiff.BCRPessimistic, tblCalcBaseCostByFieldZiff.BCROptimistic, 
		----tblCalcBaseCostByFieldZiff.TotalCostNormal,tblCalcBaseCostByFieldZiff.TotalCostPessimistic,tblCalcBaseCostByFieldZiff.TotalCostOptimistic,
		2 AS BaseCostType, tblCalcTechnicalDriverAssumptionZiff.Operation, ISNULL(tblProjects.CaseSelection,0) AS CaseSelection,tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount  AND
		tblCalcBaseCostByFieldZiff.IdPeerGroup = tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup LEFT OUTER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumptionZiff.IdProject = tblProjects.IdProject
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5) 
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0 
	AND tblCalcTechnicalDriverAssumptionZiff.Operation=2
	AND tblCalcBaseCostByFieldZiff.IdModel=@LocIdModel;

	/** Volume preparation tables **/
	/** Get Base Year **/
	DECLARE @BaseYear INT
	SELECT @BaseYear = [BaseYear] FROM [dbo].[tblModels] WHERE IdModel=@LocIdModel

	/** Volume per driver **/
	CREATE TABLE #VolumePerField ([IdModel] INT, [IdTechnicalDriver] INT, [IdField] INT, [DriverName] NVARCHAR(255), [Year] INT, [NormalAmount] FLOAT, [PessimisticAmount] FLOAT, [OptimisticAmount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumePerField ([IdModel], [IdTechnicalDriver], [IdField], [DriverName], [Year], [NormalAmount], [PessimisticAmount], [OptimisticAmount])
	SELECT tblTechnicalDrivers.IdModel, tblTechnicalDrivers.IdTechnicalDriver, tblTechnicalDriverData.IdField, tblTechnicalDrivers.Name AS DriverName, tblTechnicalDriverData.Year, SUM(tblTechnicalDriverData.Normal), SUM(tblTechnicalDriverData.Pessimistic), SUM(tblTechnicalDriverData.Optimistic)
	FROM tblTechnicalDrivers INNER JOIN tblTechnicalDriverData ON tblTechnicalDriverData.IdModel=tblTechnicalDrivers.IdModel AND tblTechnicalDriverData.IdTechnicalDriver=tblTechnicalDrivers.IdTechnicalDriver
	WHERE tblTechnicalDrivers.IdModel=@LocIdModel
	GROUP BY  tblTechnicalDrivers.IdModel, tblTechnicalDrivers.IdTechnicalDriver, tblTechnicalDriverData.IdField, tblTechnicalDrivers.Name, tblTechnicalDriverData.Year
	ORDER BY 2,3,5

	/** Volume joiner **/
	CREATE TABLE #VolumeJoiner ([IdModel] INT, [IdTechnicalDriver] INT, [IdField] INT, [Year] INT, [IdZiffAccount] INT) ON [PRIMARY]
	INSERT INTO #VolumeJoiner ([IdModel], [IdTechnicalDriver], [IdField], [Year], [IdZiffAccount])
	SELECT #tblCalcProjectionTechnical.IdModel, tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize, #tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Year, #tblCalcProjectionTechnical.IdZiffAccount
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
         #tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumption.IdModel = #tblCalcProjectionTechnical.IdModel AND 
         tblCalcTechnicalDriverAssumption.IdField = #tblCalcProjectionTechnical.IdField AND tblCalcTechnicalDriverAssumption.Year = #tblCalcProjectionTechnical.Year AND 
         tblCalcTechnicalDriverAssumption.IdZiffAccount = #tblCalcProjectionTechnical.IdZiffAccount
	WHERE (#tblCalcProjectionTechnical.ProjectionCriteria = 5) AND (#tblCalcProjectionTechnical.Operation = 2)
	GROUP BY #tblCalcProjectionTechnical.IdModel, tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize, #tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Year, #tblCalcProjectionTechnical.IdZiffAccount
	HAVING (#tblCalcProjectionTechnical.IdModel = @LocIdModel)
	UNION
	SELECT #tblCalcProjectionTechnical.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverSize, #tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Year, #tblCalcProjectionTechnical.IdZiffAccount
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
         #tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumptionZiff.IdModel = #tblCalcProjectionTechnical.IdModel AND 
         tblCalcTechnicalDriverAssumptionZiff.IdField = #tblCalcProjectionTechnical.IdField AND tblCalcTechnicalDriverAssumptionZiff.Year = #tblCalcProjectionTechnical.Year AND 
         tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = #tblCalcProjectionTechnical.IdZiffAccount
	WHERE (#tblCalcProjectionTechnical.ProjectionCriteria = 5) AND (#tblCalcProjectionTechnical.Operation = 2)
	GROUP BY #tblCalcProjectionTechnical.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverSize, #tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Year, #tblCalcProjectionTechnical.IdZiffAccount
	HAVING (#tblCalcProjectionTechnical.IdModel = @LocIdModel)
    ORDER BY 2,3,4

	/** Update the SemiVariable Factor of Business Oportunites depending on Volume versus Driver Capacity**/
	UPDATE CPT
	SET CPT.BCRNormal =	CASE WHEN #VolumePerField.NormalAmount > VPF.BaseNormalAmount/(A.UtilizationValue/100) THEN CPT.BCRNormal ELSE 0 END, 
		CPT.BCRPessimistic = CASE WHEN #VolumePerField.PessimisticAmount > VPF.BasePessimisticAmount/(A.UtilizationValue/100) THEN CPT.BCRPessimistic ELSE 0 END, 
		CPT.BCROptimistic = CASE WHEN #VolumePerField.OptimisticAmount > VPF.BaseOptimisticAmount/(A.UtilizationValue/100) THEN CPT.BCROptimistic ELSE 0 END 
	FROM #tblCalcProjectionTechnical CPT INNER JOIN
		 #VolumeJoiner ON #VolumeJoiner.IdModel = CPT.IdModel AND #VolumeJoiner.IdField = CPT.IdField AND #VolumeJoiner.Year = CPT.Year AND 
		 #VolumeJoiner.IdZiffAccount = CPT.IdZiffAccount INNER JOIN
		 #VolumePerField ON #VolumePerField.IdModel = #VolumeJoiner.IdModel AND #VolumePerField.IdField = #VolumeJoiner.IdField AND 
		 #VolumePerField.Year = #VolumeJoiner.Year AND #VolumePerField.IdTechnicalDriver = #VolumeJoiner.IdTechnicalDriver INNER JOIN
		(
		SELECT     IdField, IdTechnicalDriver, NormalAmount AS BaseNormalAmount, PessimisticAmount AS BasePessimisticAmount, OptimisticAmount AS BaseOptimisticAmount
		FROM         #VolumePerField
		WHERE     (IdModel = @LocIdModel) AND (Year = @BaseYear)
		) AS VPF ON
		 VPF.IdTechnicalDriver=#VolumeJoiner.IdTechnicalDriver AND
		 VPF.IdField=#VolumeJoiner.IdField INNER JOIN
		 tblUtilizationCapacity A ON A.IdModel = #VolumeJoiner.IdModel AND A.IdField = #VolumeJoiner.IdField AND 
		 A.IdZiffAccount = #VolumeJoiner.IdZiffAccount --INNER JOIN
		 --tblFields ON A.IdField = tblFields.IdField
	WHERE (CPT.ProjectionCriteria=5) AND (CPT.Operation = 2)

	/** Apply denominator if applicable **/
	/** Create temporary table to house changes **/
	IF OBJECT_ID('tempdb..#HousingDataTable') IS NOT NULL DROP TABLE #HousingDataTable
	IF OBJECT_ID('tempdb..#HousingDataTableZiff') IS NOT NULL DROP TABLE #HousingDataTableZiff

	SELECT 
		#tblCalcProjectionTechnical.SourceDebug, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, #tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, 
		#tblCalcProjectionTechnical.NameAccount, #tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, #tblCalcProjectionTechnical.ClientCostCenter, 
		#tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, #tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, TDUCD.IdProject, #tblCalcProjectionTechnical.ProjectionCriteria, 
		#tblCalcProjectionTechnical.Year, #tblCalcProjectionTechnical.BaseYear, #tblCalcProjectionTechnical.TFNormal * TDUCD.TFNormal AS TFNormal,
		#tblCalcProjectionTechnical.TFPessimistic * TDUCD.TFPessimistic AS TFPessimistic, #tblCalcProjectionTechnical.TFOptimistic * TDUCD.TFOptimistic AS TFOptimistic,
		#tblCalcProjectionTechnical.BCRNormal, #tblCalcProjectionTechnical.BCRPessimistic, #tblCalcProjectionTechnical.BCROptimistic, #tblCalcProjectionTechnical.BaseCostType, 
		#tblCalcProjectionTechnical.Operation, TDUCD.CaseSelection, #tblCalcProjectionTechnical.SortOrder
	INTO #HousingDataTable	
	FROM #tblCalcProjectionTechnical INNER JOIN
		(
		SELECT tblCalcUnitCostDenominatorAmount.IdModel, tblCalcUnitCostDenominatorAmount.IdField, tblCalcUnitCostDenominatorAmount.IdProject, 
			tblCalcUnitCostDenominatorAmount.Year, 
			CASE WHEN ISNULL(tblCalcBaselineUnitCostDenominatorSumAmount.SumNormal, 0) = 0 THEN 0 ELSE tblCalcUnitCostDenominatorAmount.SumNormal / tblCalcBaselineUnitCostDenominatorSumAmount.SumNormal END AS TFNormal, 
			CASE WHEN ISNULL(tblCalcBaselineUnitCostDenominatorSumAmount.SumPessimistic, 0) = 0 THEN 0 ELSE tblCalcUnitCostDenominatorAmount.SumPessimistic / tblCalcBaselineUnitCostDenominatorSumAmount.SumPessimistic END AS TFPessimistic, 
			CASE WHEN ISNULL(tblCalcBaselineUnitCostDenominatorSumAmount.SumOptimistic, 0) = 0 THEN 0 ELSE tblCalcUnitCostDenominatorAmount.SumOptimistic / tblCalcBaselineUnitCostDenominatorSumAmount.SumOptimistic END AS TFOptimistic,
			tblProjects.CaseSelection
		FROM tblCalcUnitCostDenominatorAmount INNER JOIN
			tblCalcBaselineUnitCostDenominatorSumAmount ON 
			tblCalcUnitCostDenominatorAmount.IdModel = tblCalcBaselineUnitCostDenominatorSumAmount.IdModel AND 
			tblCalcUnitCostDenominatorAmount.IdField = tblCalcBaselineUnitCostDenominatorSumAmount.IdField AND 
			tblCalcUnitCostDenominatorAmount.Year = tblCalcBaselineUnitCostDenominatorSumAmount.Year RIGHT OUTER JOIN 
			tblProjects  ON tblCalcUnitCostDenominatorAmount.IdModel = tblProjects.IdModel AND
			tblCalcUnitCostDenominatorAmount.IdField = tblProjects.IdField AND 
			tblCalcUnitCostDenominatorAmount.IdProject = tblProjects.IdProject
		WHERE tblProjects.Operation=1
			AND tblCalcUnitCostDenominatorAmount.IdModel=@LocIdModel) AS TDUCD ON 
		#tblCalcProjectionTechnical.IdModel = TDUCD.IdModel AND #tblCalcProjectionTechnical.IdField = TDUCD.IdField AND #tblCalcProjectionTechnical.Year = TDUCD.Year
	WHERE #tblCalcProjectionTechnical.IdModel=@LocIdModel AND #tblCalcProjectionTechnical.BaseCostType = 1 AND #tblCalcProjectionTechnical.Operation = 1;

	SELECT 
		#tblCalcProjectionTechnical.SourceDebug, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, #tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, 
		#tblCalcProjectionTechnical.NameAccount, #tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		TDUCD.IdProject, #tblCalcProjectionTechnical.ProjectionCriteria, #tblCalcProjectionTechnical.Year, #tblCalcProjectionTechnical.BaseYear, 
		#tblCalcProjectionTechnical.TFNormal * TDUCD.TFNormal AS TFNormal,
		#tblCalcProjectionTechnical.TFPessimistic * TDUCD.TFPessimistic AS TFPessimistic,
		#tblCalcProjectionTechnical.TFOptimistic * TDUCD.TFOptimistic AS TFOptimistic,
		#tblCalcProjectionTechnical.BCRNormal, #tblCalcProjectionTechnical.BCRPessimistic, 
		#tblCalcProjectionTechnical.BCROptimistic, #tblCalcProjectionTechnical.BaseCostType, #tblCalcProjectionTechnical.Operation, TDUCD.CaseSelection, #tblCalcProjectionTechnical.SortOrder
	INTO #HousingDataTableZiff	
	FROM #tblCalcProjectionTechnical INNER JOIN
		(
		SELECT tblCalcUnitCostDenominatorAmount.IdModel, tblCalcUnitCostDenominatorAmount.IdField, tblCalcUnitCostDenominatorAmount.IdProject, 
			tblCalcUnitCostDenominatorAmount.Year, 
			CASE WHEN ISNULL(tblCalcBaselineUnitCostDenominatorSumAmount.SumNormal, 0) = 0 THEN 0 ELSE tblCalcUnitCostDenominatorAmount.SumNormal / tblCalcBaselineUnitCostDenominatorSumAmount.SumNormal END AS TFNormal, 
			CASE WHEN ISNULL(tblCalcBaselineUnitCostDenominatorSumAmount.SumPessimistic, 0) = 0 THEN 0 ELSE tblCalcUnitCostDenominatorAmount.SumPessimistic / tblCalcBaselineUnitCostDenominatorSumAmount.SumPessimistic END AS TFPessimistic, 
			CASE WHEN ISNULL(tblCalcBaselineUnitCostDenominatorSumAmount.SumOptimistic, 0) = 0 THEN 0 ELSE tblCalcUnitCostDenominatorAmount.SumOptimistic / tblCalcBaselineUnitCostDenominatorSumAmount.SumOptimistic END AS TFOptimistic,
			tblProjects.CaseSelection
		FROM tblCalcUnitCostDenominatorAmount INNER JOIN
			tblCalcBaselineUnitCostDenominatorSumAmount ON 
			tblCalcUnitCostDenominatorAmount.IdModel = tblCalcBaselineUnitCostDenominatorSumAmount.IdModel AND 
			tblCalcUnitCostDenominatorAmount.IdField = tblCalcBaselineUnitCostDenominatorSumAmount.IdField AND 
			tblCalcUnitCostDenominatorAmount.Year = tblCalcBaselineUnitCostDenominatorSumAmount.Year RIGHT OUTER JOIN 
			tblProjects  ON tblCalcUnitCostDenominatorAmount.IdModel = tblProjects.IdModel AND
			tblCalcUnitCostDenominatorAmount.IdField = tblProjects.IdField AND 
			tblCalcUnitCostDenominatorAmount.IdProject = tblProjects.IdProject
			WHERE tblProjects.Operation=1
			AND tblCalcUnitCostDenominatorAmount.IdModel=@LocIdModel) AS TDUCD ON 
		#tblCalcProjectionTechnical.IdModel = TDUCD.IdModel AND #tblCalcProjectionTechnical.IdField = TDUCD.IdField AND #tblCalcProjectionTechnical.Year = TDUCD.Year
	WHERE #tblCalcProjectionTechnical.IdModel=@LocIdModel AND #tblCalcProjectionTechnical.BaseCostType = 2 AND #tblCalcProjectionTechnical.Operation = 1;

	/** Delete all data prior to Base Year for multiple baselines **/
	DELETE  CPT
	FROM #tblCalcProjectionTechnical CPT INNER JOIN tblProjects ON tblProjects.IdModel = CPT.IdModel 
	WHERE CPT.IdModel = @LocIdModel
	AND tblProjects.Operation=1
	AND CPT.BaseYear=0
	AND CPT.Year = tblProjects.Starts

	/** Delete data from #tblCalcProjectionTechnical **/
	DELETE FROM #tblCalcProjectionTechnical WHERE IdModel=@LocIdModel AND IdProject=0

	/** Insert data from #HousingDataTable into #tblCalcProjectionTechnical  for that field**/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[CaseSelection],[SortOrder])
	SELECT SourceDebug, IdModel, IdActivity, CodeAct, Activity,IdResources, CodeRes, Resource, IdClientAccount, Account, NameAccount, IdField, Field, IdClientCostCenter, 
		ClientCostCenter, Amount, TypeAllocation, IdZiffAccount, CodeZiff, ZiffAccount, IdRootZiffAccount, RootCodeZiff, RootZiffAccount, IdProject, ProjectionCriteria, Year, BaseYear, 
		TFNormal, TFPessimistic, TFOptimistic,BCRNormal, BCRPessimistic, BCROptimistic, BaseCostType, Operation, CaseSelection, SortOrder
	FROM #HousingDataTable	

	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[CaseSelection],[SortOrder])
	SELECT SourceDebug, IdModel, IdActivity, CodeAct, Activity,IdResources, CodeRes, Resource, IdClientAccount, Account, NameAccount, IdField, Field, IdClientCostCenter, 
		ClientCostCenter, Amount, TypeAllocation, IdZiffAccount, CodeZiff, ZiffAccount, IdRootZiffAccount, RootCodeZiff, RootZiffAccount, IdProject, ProjectionCriteria, Year, BaseYear, 
		TFNormal, TFPessimistic, TFOptimistic,BCRNormal, BCRPessimistic, BCROptimistic, BaseCostType, Operation, CaseSelection, SortOrder
	FROM #HousingDataTableZiff	

	DELETE FROM #tblCalcProjectionTechnical WHERE IdModel=@LocIdModel AND BCRNormal =0 AND BCROptimistic=0 AND BCRPessimistic=0 
	DELETE FROM #tblCalcProjectionTechnical WHERE IdModel=@LocIdModel AND TFNormal=0 AND TFOptimistic=0 AND TFNormal=0
	
	/** Prep for Reporting **/
	UPDATE #tblCalcProjectionTechnical SET [Activity]='Ziff/Third-party Costs',[SortOrder]=2 WHERE [Activity] IS NULL AND [BaseCostType]=2 AND [IdModel]=@LocIdModel;
	UPDATE #tblCalcProjectionTechnical SET [Resource]='Ziff/Third-party Costs',[SortOrder]=2 WHERE [Resource] IS NULL AND [BaseCostType]=2 AND [IdModel]=@LocIdModel;
	UPDATE #tblCalcProjectionTechnical SET [Project]=tblProjects.Name, [Operation]=tblProjects.Operation FROM #tblCalcProjectionTechnical INNER JOIN tblProjects ON #tblCalcProjectionTechnical.IdProject = tblProjects.IdProject AND #tblCalcProjectionTechnical.IdModel = tblProjects.IdModel WHERE #tblCalcProjectionTechnical.IdModel=@LocIdModel;
	UPDATE #tblCalcProjectionTechnical SET [Project]='Unknown Project' WHERE [Project] IS NULL AND [IdProject] IS NOT NULL AND [IdModel]=@LocIdModel;
	UPDATE #tblCalcProjectionTechnical SET [Field]=tblFields.Name FROM #tblCalcProjectionTechnical INNER JOIN tblFields ON #tblCalcProjectionTechnical.IdField = tblFields.IdField AND #tblCalcProjectionTechnical.IdModel = tblFields.IdModel WHERE #tblCalcProjectionTechnical.IdModel=@LocIdModel;

	/** Move data from temp table tblCalcProjectionTecnical to permanent table #tblCalcProjectionTechnical **/
	DELETE FROM tblCalcProjectionTechnical where IdModel=@LocIdModel
	INSERT INTO tblCalcProjectionTechnical([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[Project],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[CaseSelection],[SortOrder])
	SELECT DISTINCT [SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[Project],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[CaseSelection],[SortOrder] FROM #tblCalcProjectionTechnical

	/** Clean up temporary tables **/
	DROP TABLE #tblCalcProjectionTechnical
	DROP TABLE #VolumePerField
	DROP TABLE #VolumeJoiner

END

GO

/****** Object:  StoredProcedure [dbo].[tecpUtilizationCapacity]    Script Date: 01/13/2016 13:48:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================ 
-- Author:		Rodrigo Manubens
-- Create date: 2015-03-19
-- Description:	Utilization Capacity Report per Field
-- ================================================================================================ 
-- Modifications:	
-- Date			Author	Description
-- ----------	------	----------------------------------------------------------------------
-- 2015-08-04	RM		Added a return of the numbers of rows existing in table for that IdModel
--						used in code to check whether we need to create records or not.
-- 2016-01-13	RM		Replaced Code with IdZiffAccount in query 
-- ================================================================================================ 
ALTER PROCEDURE [dbo].[tecpUtilizationCapacity](
@IdModel int,
@IdStage int,
@BaseCostType int
)
AS
BEGIN
	
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#FieldList') IS NOT NULL DROP TABLE #FieldList
	IF OBJECT_ID('tempdb..#UtilizationCapacity') IS NOT NULL DROP TABLE #UtilizationCapacity
	IF OBJECT_ID('tempdb..#tblCalcTechnicalBaseCostRate') IS NOT NULL DROP TABLE #tblCalcTechnicalBaseCostRate

	DECLARE @TotalRows INT = 0
	
	/** Build Field List **/
	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL, Name NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #FieldList SELECT A.[IdField], B.Name FROM tblCalcTechnicalBaseCostRate A INNER JOIN tblFields B ON A.IdField=B.IdField WHERE A.[IdModel]=@IdModel GROUP BY A.[IdField], B.Name;

	/** Build Field Column **/
	DECLARE @FieldColumnList NVARCHAR(MAX)='';
	DECLARE @FieldName NVARCHAR(255);
	DECLARE FieldList CURSOR FOR
	SELECT Name FROM #FieldList;

	OPEN FieldList;
	FETCH NEXT FROM FieldList INTO @FieldName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @FieldColumnList=''
		BEGIN
			SET @FieldColumnList =  @FieldColumnList + '[' + @FieldName  + ']';
		END
		ELSE
		BEGIN
			SET @FieldColumnList =  @FieldColumnList + ',[' + @FieldName  + ']';
		END
		FETCH NEXT FROM FieldList INTO @FieldName;
	END

	CLOSE FieldList;
	DEALLOCATE FieldList;

	CREATE TABLE #UtilizationCapacity (CodeZiff VARCHAR(150), FieldName NVARCHAR(255), SizeValue FLOAT, CodeZiffExport VARCHAR(150), SortOrder NVARCHAR(100))
	INSERT INTO #UtilizationCapacity (CodeZiff,FieldName,SizeValue,CodeZiffExport,SortOrder)
	SELECT  dbo.svfLevelStringSpacer(dbo.tblZiffAccounts.RelationLevel) + dbo.tblZiffAccounts.Code + N': ' + dbo.tblZiffAccounts.Name AS DisplayName
		  , tblFields.Name
		  , A.UtilizationValue
		  , A.[CodeZiff] + ': ' + A.ZiffAccount AS CodeZiffExport
		  , tblZiffAccounts.SortOrder
	  FROM tblUtilizationCapacity A INNER JOIN tblFields
			ON A.IdField = tblFields.IdField LEFT OUTER JOIN tblZiffAccounts 
			ON tblZiffAccounts.IdZiffAccount=A.IdZiffAccount
	WHERE  	A.IdModel = @IdModel
	GROUP BY dbo.svfLevelStringSpacer(dbo.tblZiffAccounts.RelationLevel) + dbo.tblZiffAccounts.Code + N': ' + dbo.tblZiffAccounts.Name,tblFields.Name,A.UtilizationValue, A.[CodeZiff] + ': ' + A.ZiffAccount,tblZiffAccounts.SortOrder
	ORDER BY tblZiffAccounts.SortOrder

	SELECT @TotalRows = COUNT(*) from #UtilizationCapacity

	INSERT INTO #UtilizationCapacity (CodeZiff,FieldName,SizeValue,CodeZiffExport,SortOrder)
	SELECT  dbo.svfLevelStringSpacer(ZA.RelationLevel) + ZA.Code + N': ' + ZA.Name/*ZA.[Code]*/ AS CodeZiff
		  , #FieldList.Name
		  , '-1'
		  , dbo.svfLevelStringSpacer(ZA.RelationLevel) + ZA.Code + N': ' + ZA.Name/*ZA.[Code]*/ AS CodeZiffExport
		  , ZA.SortOrder
	  FROM tblZiffAccounts ZA , #FieldList
	WHERE  	ZA.IdModel = @IdModel AND Parent IS NULL

	--/** Build Output Table **/
	DECLARE @SQLUtilizationCapacity NVARCHAR(MAX);
	SET @SQLUtilizationCapacity = 'SELECT * FROM #UtilizationCapacity PIVOT (Sum(SizeValue) FOR FieldName IN (' + @FieldColumnList + ')) AS PivotTable ORDER BY SortOrder';
	EXEC(@SQLUtilizationCapacity);

	SELECT @TotalRows AS TotalRows
END

GO

/****** Object:  StoredProcedure [dbo].[calcModuleTechnicalCyclicalRebase]    Script Date: 01/20/2016 14:22:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:		Gareth Slater
-- Create date: 2014-04-20
-- Description:	Module Technical Rebase Cyclical Assumptions
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 2016-04-12	RM		Updated process to include different start years for peer groups.
-- ===========================================================================================
ALTER PROCEDURE [dbo].[calcModuleTechnicalCyclicalRebase](
@IdModel int,
@MinYear int,
@MaxYear int
)
AS
BEGIN


	DECLARE @intLoop int = 1;
	DECLARE @intLoopYear int;
	DECLARE @intRowCount int;
	
	/** DROP Temporary Tables **/
	IF OBJECT_ID('tempdb..#CycleYears') IS NOT NULL DROP TABLE #CycleYears

	/** Cleanup Calc Tables for New Data **/
	DELETE FROM tblCalcTechnicalCycleYears WHERE [IdModel]=@IdModel;
	
	/** Correct any Cyclical Assumptions that may be accidentally marked as 0 Years Cycle (Set to 1 to avoic issues) **/
	UPDATE tblTechnicalAssumption SET CycleValue=1 WHERE [ProjectionCriteria]=4 AND [CycleValue]=0 AND [IdModel]=@IdModel;
		
	/** Setup Lookup Table for Removal of Excess Year from Cycle **/
	CREATE TABLE #CycleYears ([IdModel] int null,[IdField] int null,[IdProject] int null,[IdZiffAccount] int null,[Starts] int null,[Client] int null,[Ziff] int null,[CycleValue] int null,[LoopID] int null) ON [PRIMARY];

	INSERT INTO #CycleYears ([IdModel],[IdField],[IdProject],[IdZiffAccount],[Starts],[Client],[Ziff],[CycleValue],[LoopID])
	SELECT tblProjects.IdModel, tblProjects.IdField, tblProjects.IdProject, tblTechnicalAssumption.IdZiffAccount, tblProjects.Starts, CSABPLookup.Client, CSABPLookup.Ziff, tblTechnicalAssumption.CycleValue, ROW_NUMBER() OVER(ORDER BY tblProjects.IdModel,tblProjects.IdField,tblProjects.IdProject,tblTechnicalAssumption.IdZiffAccount,tblProjects.Starts ASC, CSABPLookup.Client DESC) AS LoopID
	FROM tblProjects INNER JOIN
		(
		SELECT P.IdModel, P.IdField, CSABP.IdZiffAccount, CSABP.Client, max(CSABP.Ziff) AS Ziff
		FROM tblCostStructureAssumptionsByProject AS CSABP INNER JOIN
		tblProjects AS P ON CSABP.IdProject = P.IdProject
		GROUP BY P.IdModel, P.IdField, CSABP.IdZiffAccount, CSABP.Client
		) AS CSABPLookup ON tblProjects.IdModel = CSABPLookup.IdModel AND 
		tblProjects.IdField = CSABPLookup.IdField INNER JOIN
		tblTechnicalAssumption ON CSABPLookup.IdZiffAccount = tblTechnicalAssumption.IdZiffAccount AND 
		CSABPLookup.IdModel = tblTechnicalAssumption.IdModel
	WHERE tblTechnicalAssumption.ProjectionCriteria=4
	GROUP BY tblProjects.IdModel, tblProjects.IdProject, tblProjects.Starts, tblTechnicalAssumption.CycleValue, tblTechnicalAssumption.IdZiffAccount, tblProjects.IdField, CSABPLookup.Client, CSABPLookup.Ziff
	HAVING tblProjects.IdModel=@IdModel;

	/** Set RelationLevel **/
	SELECT @intRowCount=COUNT([LoopID]) FROM #CycleYears WHERE [IdModel]=@IdModel;
    
    DECLARE @IdField INT;
    DECLARE @IdProject INT;
    DECLARE @IdZiffAccount INT;
    DECLARE @Starts INT;
    DECLARE @Client INT;
    DECLARE @Ziff INT;
    DECLARE @CycleValue INT;
    DECLARE @MinYearLoop INT;
    DECLARE @BaseZiff BIT = 0;
    DECLARE @RowBaseZiff BIT = 0;
	DECLARE @AddedAlready INT = 0;    

    WHILE @intLoop <= @intRowCount BEGIN	
		SELECT @IdField=[IdField],@IdProject=[IdProject],@IdZiffAccount=[IdZiffAccount],@Starts=[Starts],@Client=[Client],@Ziff=[Ziff],@CycleValue=[CycleValue] FROM #CycleYears WHERE [LoopID]=@intLoop AND [IdModel]=@IdModel;

		IF @Starts=@MinYear
			SET @MinYearLoop = @Starts;
		ELSE
			SET @MinYearLoop = @Starts + (@CycleValue - (@Starts - @MinYear))
			--SET @MinYearLoop = @Starts + @CycleValue - 1;
		
		SET @intLoopYear=@MinYearLoop;
		WHILE @intLoopYear <= @MaxYear BEGIN
			SET @RowBaseZiff = 0;
			IF @BaseZiff=0 AND @Client > 0 AND @Ziff > 0 AND @intLoopYear >= @Ziff
				SET @RowBaseZiff = 1;
			IF @RowBaseZiff = 1
				SET @BaseZiff = 1;
			
			/* Need to Check that the entry isin't already in the table as it will cause a duplicate key error */
			SELECT @AddedAlready = ISNULL(COUNT(*),0) FROM tblCalcTechnicalCycleYears WHERE @IdField=[IdField] AND @IdProject=[IdProject] AND @IdZiffAccount=[IdZiffAccount] AND @intLoopYear=Year
			IF @AddedAlready = 0
				INSERT INTO tblCalcTechnicalCycleYears ([IdModel],[IdZiffAccount],[IdField],[IdProject],[Year],[BaseYearClient],[BaseYearZiff]) VALUES (@IdModel,@IdZiffAccount,@IdField,@IdProject,@intLoopYear,CASE WHEN @intLoopYear=@MinYearLoop THEN 1 ELSE 0 END, @RowBaseZiff);
			
			SET @intLoopYear = @intLoopYear + @CycleValue;
		END

		SET @BaseZiff = 0;
		SET @intLoop = @intLoop + 1;
	END

	/** Set ProjectionCriteria to 97 for all projects that have ID = 0 **/
	UPDATE CTDA
	SET ProjectionCriteria = 97
	FROM tblCalcTechnicalDriverAssumption CTDA
	WHERE CTDA.IdModel=@IdModel AND CTDA.IdProject=0 AND CTDA.ProjectionCriteria=4

	UPDATE CTDAZ
	SET ProjectionCriteria = 97
	FROM tblCalcTechnicalDriverAssumptionZiff CTDAZ
	WHERE CTDAZ.IdModel=@IdModel AND CTDAZ.IdProject=0 AND CTDAZ.ProjectionCriteria=4

	UPDATE tblCalcTechnicalDriverAssumption SET BaseYear=tblCalcTechnicalCycleYears.BaseYearClient
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		tblCalcTechnicalCycleYears ON tblCalcTechnicalDriverAssumption.IdModel = tblCalcTechnicalCycleYears.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = tblCalcTechnicalCycleYears.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = tblCalcTechnicalCycleYears.IdField AND 
		tblCalcTechnicalDriverAssumption.IdProject = tblCalcTechnicalCycleYears.IdProject AND 
		tblCalcTechnicalDriverAssumption.Year = tblCalcTechnicalCycleYears.Year
	WHERE tblCalcTechnicalDriverAssumption.ProjectionCriteria=4 AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	UPDATE tblCalcTechnicalDriverAssumption SET ProjectionCriteria=98
	FROM tblCalcTechnicalDriverAssumption LEFT OUTER JOIN
		tblCalcTechnicalCycleYears ON tblCalcTechnicalDriverAssumption.IdModel = tblCalcTechnicalCycleYears.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = tblCalcTechnicalCycleYears.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = tblCalcTechnicalCycleYears.IdField AND 
		tblCalcTechnicalDriverAssumption.IdProject = tblCalcTechnicalCycleYears.IdProject AND 
		tblCalcTechnicalDriverAssumption.Year = tblCalcTechnicalCycleYears.Year
	WHERE tblCalcTechnicalCycleYears.IdModel IS NULL AND tblCalcTechnicalDriverAssumption.ProjectionCriteria=4 AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Mark all non cycle years to 98 for deletion **/
	UPDATE tblCalcTechnicalDriverAssumption SET ProjectionCriteria=98
	FROM tblCalcTechnicalDriverAssumption LEFT OUTER JOIN
		tblCalcTechnicalCycleYears ON tblCalcTechnicalDriverAssumption.IdModel = tblCalcTechnicalCycleYears.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = tblCalcTechnicalCycleYears.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = tblCalcTechnicalCycleYears.IdField AND 
		tblCalcTechnicalDriverAssumption.Year = tblCalcTechnicalCycleYears.Year
	WHERE tblCalcTechnicalCycleYears.IdModel IS NULL AND tblCalcTechnicalDriverAssumption.ProjectionCriteria=97 AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;

	DELETE FROM tblCalcTechnicalDriverAssumption WHERE [ProjectionCriteria]=98 AND [IdModel]=@IdModel;

	UPDATE tblCalcTechnicalDriverAssumptionZiff SET BaseYear=tblCalcTechnicalCycleYears.BaseYearZiff
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		tblCalcTechnicalCycleYears ON tblCalcTechnicalDriverAssumptionZiff.IdModel = tblCalcTechnicalCycleYears.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = tblCalcTechnicalCycleYears.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = tblCalcTechnicalCycleYears.IdField AND 
		tblCalcTechnicalDriverAssumptionZiff.IdProject = tblCalcTechnicalCycleYears.IdProject AND 
		tblCalcTechnicalDriverAssumptionZiff.Year = tblCalcTechnicalCycleYears.Year
	WHERE tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=4 AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET ProjectionCriteria=98
	FROM tblCalcTechnicalDriverAssumptionZiff LEFT OUTER JOIN
		tblCalcTechnicalCycleYears ON tblCalcTechnicalDriverAssumptionZiff.IdModel = tblCalcTechnicalCycleYears.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = tblCalcTechnicalCycleYears.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = tblCalcTechnicalCycleYears.IdField AND 
		tblCalcTechnicalDriverAssumptionZiff.IdProject = tblCalcTechnicalCycleYears.IdProject AND 
		tblCalcTechnicalDriverAssumptionZiff.Year = tblCalcTechnicalCycleYears.Year
	WHERE tblCalcTechnicalCycleYears.IdModel IS NULL AND tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=4 AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	/** Mark all non cycle years to 98 for deletion **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET ProjectionCriteria=98
	FROM tblCalcTechnicalDriverAssumptionZiff LEFT OUTER JOIN
		tblCalcTechnicalCycleYears ON tblCalcTechnicalDriverAssumptionZiff.IdModel = tblCalcTechnicalCycleYears.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = tblCalcTechnicalCycleYears.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = tblCalcTechnicalCycleYears.IdField AND 
		tblCalcTechnicalDriverAssumptionZiff.Year = tblCalcTechnicalCycleYears.Year
	WHERE tblCalcTechnicalCycleYears.IdModel IS NULL AND tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=97 AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;

	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE [ProjectionCriteria]=98 AND [IdModel]=@IdModel;

	/** Set ProjectionCriteria back to 4 for all rows remaining that have value 97 **/
	UPDATE tblCalcTechnicalDriverAssumption SET ProjectionCriteria=4
	FROM tblCalcTechnicalDriverAssumption 
	WHERE tblCalcTechnicalDriverAssumption.ProjectionCriteria=97 AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;

	UPDATE tblCalcTechnicalDriverAssumptionZiff SET ProjectionCriteria=4
	FROM tblCalcTechnicalDriverAssumptionZiff 
	WHERE tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=97 AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;

END

GO

/****** Object:  StoredProcedure [dbo].[allpUpdateCostStrucAssumptionByProj]    Script Date: 01/25/2016 16:17:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[allpUpdateCostStrucAssumptionByProj]
 @pIdCostStructureAssumptions int,
 @pIdProject int,
 @pIdZiffAcnt int,
 @pClient int,
 @pZiff int, 
 @pIdPeerGroup int,
 @pIdField int
AS
BEGIN
	IF (@pIdCostStructureAssumptions > 0)
	BEGIN
		IF @pClient = 0
		BEGIN	
			UPDATE tblCostStructureAssumptionsByProject
			SET	Ziff = @pZiff, IdPeerGroup = @pIdPeerGroup
			WHERE IdCostStructureAssumptions = @pIdCostStructureAssumptions
		END
		ELSE
		BEGIN
			UPDATE tblCostStructureAssumptionsByProject
			SET	Client = @pClient, Ziff = @pZiff, IdPeerGroup = @pIdPeerGroup
			WHERE IdCostStructureAssumptions = @pIdCostStructureAssumptions
		END
	END
	ELSE
	BEGIN
		INSERT INTO tblCostStructureAssumptionsByProject VALUES(@pIdProject,@pIdZiffAcnt,@pClient,@pZiff, @pIdPeerGroup,@pIdField)
	END
END



GO

/****** Object:  StoredProcedure [dbo].[allpListCostStructureAssumptionsByProject]    Script Date: 01/25/2016 09:10:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-06-11
-- Description:	List Cost Structure Assumptions By Project as the view will no longer work as we are adding a row to display the parent
-- ========================================================================================================================================
-- 2015-07-27	Added the Case name to the query as it will be displayed in the Cost Structure Assumptions page of the tool
-- ========================================================================================================================================

ALTER PROCEDURE [dbo].[allpListCostStructureAssumptionsByProject](
 @IdModel int,
 @IdField int
)
AS
BEGIN
	
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#CostStructureAssumptions') IS NOT NULL DROP TABLE #CostStructureAssumptions
	IF OBJECT_ID('tempdb..#PeerGroupList') IS NOT NULL DROP TABLE #PeerGroupList

	/** Build Field List **/
	CREATE TABLE #PeerGroupList (IdPeerGroup INT PRIMARY KEY NOT NULL, PeerGroupDescription NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #PeerGroupList SELECT IdPeerGroup, PeerGroupCode FROM tblPeerGroup WHERE IdPeerGroup IN (SELECT IdPeerGroup FROM tblPeerGroupField WHERE IdField=@IdField);

	/** Build Peer Column **/
	DECLARE @numPeerGroups INT
	SELECT @numPeerGroups = ISNULL(COUNT(*),0) FROM #PeerGroupList
	
	DECLARE @PeerGroupColumnList NVARCHAR(MAX)='[N/A]';
	DECLARE @PeerGroupName NVARCHAR(255);
	DECLARE PeerGroupList CURSOR FOR
	SELECT PeerGroupDescription FROM #PeerGroupList;

	IF (@numPeerGroups > 0)
		SET @PeerGroupColumnList = ''
		
	OPEN PeerGroupList;
	FETCH NEXT FROM PeerGroupList INTO @PeerGroupName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @PeerGroupColumnList=''
		BEGIN
			SET @PeerGroupColumnList =  @PeerGroupColumnList + '[' + @PeerGroupName  + ']';
		END
		ELSE
		BEGIN
			SET @PeerGroupColumnList =  @PeerGroupColumnList + ',[' + @PeerGroupName  + ']';
		END
		FETCH NEXT FROM PeerGroupList INTO @PeerGroupName;
	END

	CLOSE PeerGroupList;
	DEALLOCATE PeerGroupList;

	DECLARE @CostExists INT
	SELECT @CostExists = ISNULL(COUNT(*),0) 
	FROM tblCostStructureAssumptionsByProject  LEFT OUTER JOIN
		tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject 
	WHERE IdPeerGroup IN (SELECT IdPeerGroup FROM #PeerGroupList) AND tblCostStructureAssumptionsByProject.IdField= @IdField

	--CREATE TABLE #CostStructureAssumptions (IdModel INT, IdCostStructureAssumptions INT, IdProject INT, ProjectName NVARCHAR(255), IdZiffAccount INT, Code NVARCHAR(50), Name NVARCHAR(255), ParentCode NVARCHAR(50), ParentName NVARCHAR(255), Client INT, Ziff INT , IdField INT, SortOrder NVARCHAR(100), SeqOrder INT)
	CREATE TABLE #CostStructureAssumptions (IdModel INT, IdZiffAccount INT, Code NVARCHAR(50), Name NVARCHAR(255), ParentCode NVARCHAR(50), ParentName NVARCHAR(255), 
											Client INT, Ziff INT , IdField INT, SortOrder NVARCHAR(100), PeerGroupDescription NVARCHAR(255))
	IF (@numPeerGroups = 0)
	BEGIN
		INSERT INTO #CostStructureAssumptions (IdModel, IdZiffAccount, Code, Name, ParentCode, ParentName, Client, Ziff, IdField, SortOrder, PeerGroupDescription)
		SELECT	tblZiffAccountsParent.IdModel, tblCostStructureAssumptionsByProject.IdZiffAccount, '&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;' + tblZiffAccounts.Code as Code, tblZiffAccounts.Name, 
				tblZiffAccounts.Code AS ParentCode, tblZiffAccountsParent.Name AS ParentName, tblCostStructureAssumptionsByProject.Client, tblCostStructureAssumptionsByProject.Ziff, 
				tblProjects.IdField, tblZiffAccounts.SortOrder, ''
		FROM    tblCostStructureAssumptionsByProject LEFT OUTER JOIN
				tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject LEFT OUTER JOIN
				tblZiffAccounts ON tblCostStructureAssumptionsByProject.IdZiffAccount = tblZiffAccounts.IdZiffAccount LEFT OUTER JOIN
				tblZiffAccounts AS tblZiffAccountsParent ON tblZiffAccounts.Parent = tblZiffAccountsParent.IdZiffAccount
		WHERE  	tblProjects.IdModel = @IdModel
		AND		tblProjects.IdField = @IdField 
		UNION
		SELECT	NULL, NULL, dbo.tblZiffAccounts.Code, Name, NULL, NULL, 9999, 9999, NULL, dbo.tblZiffAccounts.SortOrder, ''
		FROM tblZiffAccounts
		WHERE  	IdModel = @IdModel AND Parent IS NULL
		ORDER BY SortOrder
	END		
	ELSE
	BEGIN
		IF (@CostExists = 0)
		BEGIN
			INSERT INTO #CostStructureAssumptions (IdModel, IdZiffAccount, Code, Name, ParentCode, ParentName, Client, Ziff, IdField, SortOrder, PeerGroupDescription)
			SELECT	tblZiffAccountsParent.IdModel, tblCostStructureAssumptionsByProject.IdZiffAccount, '&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;' + tblZiffAccounts.Code as Code, tblZiffAccounts.Name, 
					tblZiffAccounts.Code AS ParentCode, tblZiffAccountsParent.Name AS ParentName, tblCostStructureAssumptionsByProject.Client, tblCostStructureAssumptionsByProject.Ziff, 
					tblProjects.IdField, tblZiffAccounts.SortOrder, ''
			FROM    tblCostStructureAssumptionsByProject LEFT OUTER JOIN
					tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject LEFT OUTER JOIN
					tblZiffAccounts ON tblCostStructureAssumptionsByProject.IdZiffAccount = tblZiffAccounts.IdZiffAccount LEFT OUTER JOIN
					tblZiffAccounts AS tblZiffAccountsParent ON tblZiffAccounts.Parent = tblZiffAccountsParent.IdZiffAccount
			WHERE  	tblProjects.IdModel = @IdModel
			AND		tblProjects.IdField = @IdField 
			UNION
			SELECT	NULL, NULL, dbo.tblZiffAccounts.Code, Name, NULL, NULL, 9999, 9999, NULL, dbo.tblZiffAccounts.SortOrder, ''
			FROM tblZiffAccounts
			WHERE  	IdModel = @IdModel AND Parent IS NULL
			ORDER BY SortOrder
		END				
		ELSE
		BEGIN
			INSERT INTO #CostStructureAssumptions (IdModel, IdZiffAccount, Code, Name, ParentCode, ParentName, Client, Ziff, IdField, SortOrder, PeerGroupDescription)
			SELECT	DISTINCT tblZiffAccountsParent.IdModel, tblCostStructureAssumptionsByProject.IdZiffAccount, '&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;' + tblZiffAccounts.Code as Code, 
					tblZiffAccounts.Name, tblZiffAccounts.Code AS ParentCode, tblZiffAccountsParent.Name AS ParentName, tblCostStructureAssumptionsByProject.Client, 
					tblCostStructureAssumptionsByProject.Ziff, tblProjects.IdField, tblZiffAccounts.SortOrder, tblPeerGroup.PeerGroupCode
			FROM    tblCostStructureAssumptionsByProject LEFT OUTER JOIN
					tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject LEFT OUTER JOIN
					tblZiffAccounts ON tblCostStructureAssumptionsByProject.IdZiffAccount = tblZiffAccounts.IdZiffAccount LEFT OUTER JOIN
					tblZiffAccounts AS tblZiffAccountsParent ON tblZiffAccounts.Parent = tblZiffAccountsParent.IdZiffAccount INNER JOIN
					tblPeerGroup ON tblCostStructureAssumptionsByProject.IdPeerGroup = tblPeerGroup.IdPeerGroup
			WHERE  	tblProjects.IdModel = @IdModel
			AND		tblProjects.IdField = @IdField 
			UNION
			SELECT NULL, NULL, dbo.tblZiffAccounts.Code, Name, NULL, NULL, 9999, 9999, NULL, dbo.tblZiffAccounts.SortOrder, ''
			FROM tblZiffAccounts
			WHERE  	IdModel = @IdModel AND Parent IS NULL
			ORDER BY SortOrder
		END
	END	
	--/** Add Sequence(ROW_NUMBER) to SeqOrder **/
	--UPDATE x 
	--SET x.SeqOrder = x.New_SeqOrder
	--FROM (
	--		SELECT SeqOrder, ROW_NUMBER() OVER(ORDER BY SortOrder ASC) AS New_SeqOrder
	--		FROM #CostStructureAssumptions
	--	 ) x
	
	/** Build Output Table **/
	--SELECT IdModel, IdCostStructureAssumptions, IdProject, ProjectName, IdZiffAccount, Code, Name, ParentCode, ParentName, Client, Ziff, IdField, SortOrder + N' ' + ISNULL(ProjectName,'') AS SortOrder, SeqOrder
	--FROM #CostStructureAssumptions 
	--ORDER BY SeqOrder

	DECLARE @SQLCostStructureAssumptionsByProject NVARCHAR(MAX)
	IF (@numPeerGroups = 0)
		SET @SQLCostStructureAssumptionsByProject = 'SELECT IdModel, IdField, IdZiffAccount, Code, Name, Client, PeerGroupDescription FROM #CostStructureAssumptions Order by SortOrder'
	ELSE
		SET @SQLCostStructureAssumptionsByProject = 'SELECT IdModel, IdField, IdZiffAccount, Code, Name, Client, ' + @PeerGroupColumnList + ' FROM #CostStructureAssumptions PIVOT (SUM(Ziff) FOR PeerGroupDescription IN (' + @PeerGroupColumnList + ')) AS PivotTable Order by SortOrder'
	EXEC(@SQLCostStructureAssumptionsByProject)

END

GO

/****** Object:  StoredProcedure [dbo].[parpBaseCostTechnicalModule]    Script Date: 01/27/2016 13:28:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================================================
-- Author:		Gareth Slater
-- Create date: 2014-04-22
-- Description:	Technical Base Cost Rates Lookup
-- ========================================================================================================================================
-- 2016-02-01	Added a condition for Internal or External Base Cost Type, as a peer group id is now used in tblCalcTechnicalBaseCostRate
--				to differentiate inernal and external data.
-- ========================================================================================================================================
ALTER PROCEDURE [dbo].[parpBaseCostTechnicalModule](
@IdModel int,
@IdField int,
@IdStage int,
@BaseCostType int,
@IdCurrency int,
@IdLanguage int
)
AS
BEGIN

	SET NOCOUNT ON;
	
	--Original Code (R Manubens 2015/02/23)
	--SELECT * FROM tblCalcTechnicalBaseCostRate
	--WHERE IdModel=@IdModel AND IdField=@IdField AND IdStage=@IdStage AND BaseCostType=@BaseCostType AND ProjectionCriteria<>98
	--ORDER BY SortOrder;
	
	--New Code to include Currency Conversion(R Manubens 2015/02/23)

	/** Get Currency Factor to use **/
	DECLARE @CurrencyFactor FLOAT;
	SELECT @CurrencyFactor=[Value] FROM tblModelsCurrencies WHERE [IdCurrency]=@IdCurrency AND [IdModel]=@IdModel;
	
	IF (@BaseCostType = 1)												
	BEGIN
		SELECT	IdModel, IdZiffAccount, CodeZiff, ZiffAccount, DisplayCode, SortOrder, IdField, IdStage, BaseCostType, ProjectionCriteria, Name AS NameProjectionCriteria, CycleValue, SizeName
				, "SizeValue" = CASE 
									WHEN NameProjectionCriteria != 'SemiFixed' THEN SUM(SizeValue)
									ELSE NULL
							   END
				, PerformanceName
				, SUM(PerformanceValue) AS PerformanceValue
				, SUM(BaseCost) * @CurrencyFactor  AS BaseCost
				, "BaseCostRate" = CASE
										WHEN NameProjectionCriteria != 'Fixed' THEN (BaseCostRate) * @CurrencyFactor
										ELSE NULL
								  END
				, IdRootZiffAccount, RootCodeZiff, RootZiffAccount, BaselineAllocation
		FROM	tblCalcTechnicalBaseCostRate INNER JOIN tblParameters ON tblCalcTechnicalBaseCostRate.ProjectionCriteria = tblParameters.Code
		WHERE	IdModel=@IdModel AND IdField=@IdField AND IdStage=@IdStage AND BaseCostType=@BaseCostType AND ProjectionCriteria<>98 AND Type = 'ProjectionCriteria' AND IdLanguage=@IdLanguage
		GROUP BY IdModel, IdZiffAccount, CodeZiff, ZiffAccount, DisplayCode, SortOrder, IdField, IdStage, BaseCostType, ProjectionCriteria, NameProjectionCriteria, CycleValue, SizeName
				, SizeValue, PerformanceName, PerformanceValue, BaseCost, BaseCostRate, IdRootZiffAccount, RootCodeZiff, RootZiffAccount, BaselineAllocation, Name
		ORDER BY SortOrder;
	END
	ELSE
	BEGIN
		SELECT	IdModel, IdZiffAccount, CodeZiff, ZiffAccount, DisplayCode, SortOrder, tblCalcTechnicalBaseCostRate.IdField, IdStage, BaseCostType, ProjectionCriteria, Name AS NameProjectionCriteria, CycleValue, SizeName
				, "SizeValue" = CASE 
									WHEN NameProjectionCriteria != 'SemiFixed' THEN SUM(SizeValue)
									ELSE NULL
							   END
				, PerformanceName
				, SUM(PerformanceValue) AS PerformanceValue
				, SUM(BaseCost) * @CurrencyFactor  AS BaseCost
				, "BaseCostRate" = CASE
										WHEN NameProjectionCriteria != 'Fixed' THEN (BaseCostRate) * @CurrencyFactor
										ELSE NULL
								  END
				, IdRootZiffAccount, RootCodeZiff, RootZiffAccount, BaselineAllocation, tblCalcTechnicalBaseCostRate.IdPeerGroup
		FROM	tblCalcTechnicalBaseCostRate INNER JOIN tblParameters ON tblCalcTechnicalBaseCostRate.ProjectionCriteria = tblParameters.Code
		WHERE	tblCalcTechnicalBaseCostRate.IdModel = @IdModel AND tblCalcTechnicalBaseCostRate.IdField = @IdField AND IdStage = @IdStage AND ProjectionCriteria <> 98 
		AND		tblCalcTechnicalBaseCostRate.IdPeerGroup=@BaseCostType AND Type = 'ProjectionCriteria' AND IdLanguage=@IdLanguage
		GROUP BY IdModel, IdZiffAccount, CodeZiff, ZiffAccount, DisplayCode, SortOrder, tblCalcTechnicalBaseCostRate.IdField, IdStage, BaseCostType, ProjectionCriteria, NameProjectionCriteria, CycleValue, SizeName
				, SizeValue, PerformanceName, PerformanceValue, BaseCost, BaseCostRate, IdRootZiffAccount, RootCodeZiff, RootZiffAccount, BaselineAllocation,tblCalcTechnicalBaseCostRate.IdPeerGroup, Name
		ORDER BY SortOrder;	
	END
			
END

GO

/****** Object:  StoredProcedure [dbo].[calcModuleEconomicPreCalc]    Script Date: 02/03/2016 10:54:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================================================================================================
-- Author:		Gareth Slater
-- Create date: 2014-04-03
-- Description:	Module Economic PreCalc
-- ========================================================================================================================================
-- 2016-02-01	Commented out the delete portion as it was deleting all informationin table
-- ========================================================================================================================================
ALTER PROCEDURE [dbo].[calcModuleEconomicPreCalc](
@IdModel int
)
AS
BEGIN
	
	/** Update any Missing Economic Assumptions **/
	INSERT INTO tblTypePriceEconomic ([IdZiffAccount],[IdModel],[ProjectionCriteria],[UserCreation],[DateCreation])
	SELECT TPEALL.IdZiffAccount, TPEALL.IdModel, TPEALL.ProjectionCriteria, 0 AS UserCreation, GETDATE() AS DateCreation
	FROM vListTypePriceEconomicAll AS TPEALL LEFT OUTER JOIN
		  tblTypePriceEconomic AS TPE ON TPEALL.IdZiffAccount = TPE.IdZiffAccount AND 
		  TPEALL.IdModel = TPE.IdModel
	WHERE TPE.IdZiffAccount IS NULL AND TPE.IdModel IS NULL AND TPEALL.IdModel=@IdModel;
	
	--/** Cleanup Economic Assumptions **/
	--DELETE FROM tblTypePriceEconomic WHERE IdZiffAccount NOT IN (SELECT [IdZiffAccount] FROM tblZiffAccounts WHERE [Parent] IS NULL AND [IdModel]=@IdModel);
	----DELETE FROM tblTypePriceEconomic WHERE IdZiffAccount IN (SELECT [IdZiffAccount] FROM tblZiffAccounts WHERE [Parent] IS NOT NULL AND [IdModel]=@IdModel);
	
END

GO

/****** Object:  StoredProcedure [dbo].[parpListTypePriceEconomic]    Script Date: 02/03/2016 09:57:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[parpListTypePriceEconomic] 
    @IdModel int
AS 
BEGIN
	
	--SELECT * FROM vListTypePriceEconomic WHERE IdModel=@IdModel;
	SELECT * FROM vListTypePriceEconomicWithChildren WHERE IdModel=@IdModel ORDER BY SortOrder;
	
END

GO

/****** Object:  StoredProcedure [dbo].[parpUpdateTypePriceEconomic]    Script Date: 02/03/2016 11:24:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[parpUpdateTypePriceEconomic](
@IdTypePriceEconomic int,
@IdZiffAccount int,
@IdModel int,
@ProjectionCriteria int,
@UserModification int,
@DateModification datetime)
AS 
BEGIN
  UPDATE tblTypePriceEconomic SET  IdZiffAccount=@IdZiffAccount, IdModel=@IdModel, ProjectionCriteria=@ProjectionCriteria, UserModification=@UserModification,DateModification=@DateModification
		 
      WHERE IdTypePriceEconomic =@IdTypePriceEconomic
END

GO

/****** Object:  StoredProcedure [dbo].[calcModuleTechnical]    Script Date: 02/17/2016 16:30:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Gareth Slater
-- Create date: 2014-04-03
-- Description:	Module Technical Calc
-- ================================================================================================ 
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 2015-08-04	RM		Added a new code to poulate table tblCalcBaselineUnitCostDenominatorSumAmount
-- 2015-08-15	GS		Updated to work with tblCalcCostStructureAssumptionsByField and Handling Semi-Variable Assumptions
-- 2015-10-02	RM		Added a cleanup at end of procedure to remove data from calc tables as data
--						is not needed anywhere else
-- ================================================================================================	
ALTER PROCEDURE [dbo].[calcModuleTechnical](
@IdModel INT
)
AS
BEGIN

	BEGIN /** SETUP **/
	DECLARE @intMinYear INT;
	DECLARE @intMaxYear INT;
	SELECT @intMinYear=BaseYear, @intMaxYear=BaseYear+Projection FROM tblModels WHERE [IdModel]=@IdModel;

	DECLARE @intLoop int = 0;
	DECLARE @intMaxLevel int = 0;
	SELECT @intMaxLevel = MAX([RelationLevel])+1 FROM tblZiffAccounts WHERE [IdModel]=@IdModel;

	/** Precalc **/
	EXEC calcModuleTechnicalPreCalc @IdModel;
	
	/** Cleanup Calc Tables for New Data **/
	DELETE FROM tblCalcModelMultiBaseline WHERE IdModel=@IdModel;
	DELETE FROM tblCalcUnitCostDenominatorSumAmount WHERE IdModel=@IdModel;
	DELETE FROM tblCalcUnitCostDenominatorAmount WHERE IdModel=@IdModel;
	DELETE FROM tblCalcBaselineUnitCostDenominatorSumAmount WHERE IdModel=@IdModel;
	DELETE FROM tblCalcCostStructureAssumptionsByField WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalDriverData WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalAssumption WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalDriverAssumption WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE [IdModel]=@IdModel;
	
	/** Base table records - which fields have multiple baselines **/
	INSERT INTO tblCalcModelMultiBaseline ([IdModel],[IdField])
	SELECT tblFields.IdModel, tblFields.IdField
	FROM tblFields INNER JOIN
		tblProjects ON tblFields.IdField = tblProjects.IdField
	GROUP BY tblFields.IdModel, tblFields.IdField, tblProjects.Operation
	HAVING SUM(1)>1 AND tblProjects.Operation=1 AND tblFields.IdModel=@IdModel;

	/** Base table records - Total Value of Denominator Drivers by Field **/
	INSERT INTO tblCalcUnitCostDenominatorSumAmount ([IdModel],[IdField],[Year],[SumNormal],[SumPessimistic],[SumOptimistic])
	SELECT tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year, SUM(tblTechnicalDriverData.Normal) AS SumNormal, SUM(tblTechnicalDriverData.Pessimistic) 
		AS SumPessimistic, SUM(tblTechnicalDriverData.Optimistic) AS SumOptimistic
	FROM tblTechnicalDriverData INNER JOIN
		tblTechnicalDrivers ON tblTechnicalDriverData.IdTechnicalDriver = tblTechnicalDrivers.IdTechnicalDriver AND tblTechnicalDriverData.IdModel = tblTechnicalDrivers.IdModel INNER JOIN
		tblProjects ON tblTechnicalDriverData.IdProject = tblProjects.IdProject
	WHERE (tblTechnicalDrivers.UnitCostDenominator=1 OR tblTechnicalDrivers.BaselineAllocation = 1) AND tblProjects.Operation=1 
	GROUP BY tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year
	HAVING tblTechnicalDrivers.IdModel=@IdModel;

	/** Base table records - Total Value of Denominator Drivers by Field/Project **/
	INSERT INTO tblCalcUnitCostDenominatorAmount ([IdModel],[IdField],[IdProject],[Year],[SumNormal],[SumPessimistic],[SumOptimistic])
	SELECT tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.IdProject, tblTechnicalDriverData.Year, SUM(tblTechnicalDriverData.Normal) AS SumNormal, 
		SUM(tblTechnicalDriverData.Pessimistic) AS SumPessimistic, SUM(tblTechnicalDriverData.Optimistic) AS SumOptimistic
	FROM tblTechnicalDriverData INNER JOIN
		tblTechnicalDrivers ON tblTechnicalDriverData.IdTechnicalDriver = tblTechnicalDrivers.IdTechnicalDriver AND tblTechnicalDriverData.IdModel = tblTechnicalDrivers.IdModel INNER JOIN
		tblProjects ON tblTechnicalDriverData.IdProject = tblProjects.IdProject AND tblTechnicalDriverData.IdModel = tblProjects.IdModel
	WHERE (tblTechnicalDrivers.UnitCostDenominator=1 OR tblTechnicalDrivers.BaselineAllocation = 1) AND tblProjects.Operation=1 
	GROUP BY tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year, tblTechnicalDriverData.IdProject
	HAVING tblTechnicalDrivers.IdModel=@IdModel;

	/** Base table records - Baseline Unit Cost Denominator **/
	DECLARE @MultiBaseline INT
	SELECT @MultiBaseline = COUNT(*) FROM tblCalcModelMultiBaseline WHERE IdModel=@IdModel
	
	IF (@MultiBaseline <> 0)
	BEGIN
		INSERT INTO tblCalcBaselineUnitCostDenominatorSumAmount ([IdModel],[IdField],[Year],[SumNormal],[SumPessimistic],[SumOptimistic])
		SELECT tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year, SUM(tblTechnicalDriverData.Normal) AS SumNormal, 
			SUM(tblTechnicalDriverData.Pessimistic) AS SumPessimistic, SUM(tblTechnicalDriverData.Optimistic) AS SumOptimistic
		FROM tblTechnicalDriverData INNER JOIN
			tblTechnicalDrivers ON tblTechnicalDriverData.IdTechnicalDriver = tblTechnicalDrivers.IdTechnicalDriver AND 
			tblTechnicalDriverData.IdModel = tblTechnicalDrivers.IdModel INNER JOIN
			tblCalcModelMultiBaseline ON tblTechnicalDriverData.IdModel = tblCalcModelMultiBaseline.IdModel AND 
			tblTechnicalDriverData.IdField = tblCalcModelMultiBaseline.IdField RIGHT OUTER JOIN 
			tblProjects  ON tblTechnicalDriverData.IdModel = tblProjects.IdModel AND
			tblTechnicalDriverData.IdField = tblProjects.IdField AND 
			tblTechnicalDriverData.IdProject = tblProjects.IdProject
		WHERE (tblTechnicalDrivers.UnitCostDenominator=1 OR tblTechnicalDrivers.BaselineAllocation = 1) AND tblProjects.Operation=1 
		GROUP BY tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year
		HAVING tblTechnicalDrivers.IdModel=@IdModel;
	END
	ELSE
	BEGIN
		INSERT INTO tblCalcBaselineUnitCostDenominatorSumAmount ([IdModel],[IdField],[Year],[SumNormal],[SumPessimistic],[SumOptimistic])
		SELECT tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year, SUM(tblTechnicalDriverData.Normal) AS SumNormal, 
			SUM(tblTechnicalDriverData.Pessimistic) AS SumPessimistic, SUM(tblTechnicalDriverData.Optimistic) AS SumOptimistic
		FROM tblTechnicalDriverData INNER JOIN
			tblTechnicalDrivers ON tblTechnicalDriverData.IdTechnicalDriver = tblTechnicalDrivers.IdTechnicalDriver AND 
			tblTechnicalDriverData.IdModel = tblTechnicalDrivers.IdModel /*INNER JOIN
			tblCalcModelMultiBaseline ON tblTechnicalDriverData.IdModel = tblCalcModelMultiBaseline.IdModel AND 
			tblTechnicalDriverData.IdField = tblCalcModelMultiBaseline.IdField*/ RIGHT OUTER JOIN 
			tblProjects  ON tblTechnicalDriverData.IdModel = tblProjects.IdModel AND
			tblTechnicalDriverData.IdField = tblProjects.IdField AND 
			tblTechnicalDriverData.IdProject = tblProjects.IdProject
		WHERE (tblTechnicalDrivers.UnitCostDenominator=1 OR tblTechnicalDrivers.BaselineAllocation = 1) AND tblProjects.Operation=1 
		GROUP BY tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year
		HAVING tblTechnicalDrivers.IdModel=@IdModel;	
	END

	/** Check if @intMaxYear (projection years) are all required (based on Denominator Totals) **/
	DECLARE @intMaxYearDenominator INT;
	SELECT @intMaxYearDenominator=MAX([Year]) FROM tblCalcUnitCostDenominatorSumAmount WHERE [IdModel]=@IdModel AND ([SumNormal]>0 OR [SumPessimistic]>0 OR [SumOptimistic]>0) GROUP BY [IdModel];

	IF (@intMaxYearDenominator<@intMaxYear)
	BEGIN
		SET @intMaxYear = @intMaxYearDenominator;
	END
	END /** SETUP **/
	
	BEGIN /** Base table records - Baseline Cost Structure Assumptions By Field **/
	/** Need to include Peer Group ID so a temporary table will be created to house data **/
	
	/*** BEGIN OLD CODE ***/
	--INSERT INTO tblCalcCostStructureAssumptionsByField ([IdModel],[IdField],[IdZiffAccount],[Client],[Ziff])
	--SELECT tblProjects.IdModel, tblProjects.IdField, tblCostStructureAssumptionsByProject.IdZiffAccount, tblCostStructureAssumptionsByProject.Client, 0--tblCostStructureAssumptionsByProject.Ziff
	--FROM tblCostStructureAssumptionsByProject INNER JOIN
	--	tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject
	--GROUP BY tblCostStructureAssumptionsByProject.IdZiffAccount, tblCostStructureAssumptionsByProject.Client, /*tblCostStructureAssumptionsByProject.Ziff,*/ tblProjects.IdField, tblProjects.IdModel
	--HAVING tblProjects.IdModel=@IdModel;
	/*** END OLD CODE ***/

	IF OBJECT_ID('tempdb..#tblCalcCostStructureAssumptionsByField') IS NOT NULL DROP TABLE #tblCalcCostStructureAssumptionsByField
	CREATE TABLE #tblCalcCostStructureAssumptionsByField ([IdModel] INT,[IdField] INT,[IdZiffAccount] INT,[Client] INT,[Ziff] INT, [ZiffEndYear] INT, [IdPeerGroup] INT) ON [PRIMARY]
	INSERT INTO #tblCalcCostStructureAssumptionsByField ([IdModel],[IdField],[IdZiffAccount],[Client],[Ziff],[IdPeerGroup])
	SELECT tblProjects.IdModel, tblProjects.IdField, tblCostStructureAssumptionsByProject.IdZiffAccount, tblCostStructureAssumptionsByProject.Client, tblCostStructureAssumptionsByProject.Ziff, tblCostStructureAssumptionsByProject.IdPeerGroup
	FROM tblCostStructureAssumptionsByProject INNER JOIN
		tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject
	GROUP BY tblCostStructureAssumptionsByProject.IdZiffAccount, tblCostStructureAssumptionsByProject.Client, tblCostStructureAssumptionsByProject.Ziff, tblProjects.IdField, tblProjects.IdModel,tblCostStructureAssumptionsByProject.IdPeerGroup
	HAVING tblProjects.IdModel=@IdModel;

	UPDATE #tblCalcCostStructureAssumptionsByField
	SET ZiffEndYear = @intMaxYear
	WHERE Ziff > 0

	/** Need to set up the end year of each peer group **/
	UPDATE T1
	SET T1.ZiffEndYear = T2.Ziff - 1
	FROM #tblCalcCostStructureAssumptionsByField T1 CROSS APPLY
	(SELECT TOP 1 T2.* 
	 FROM #tblCalcCostStructureAssumptionsByField T2 
	 WHERE T2.IdModel = T1.IdModel AND
		   T2.IdField = T1.IdField AND
		   T2.IdZiffAccount = T1.IdZiffAccount AND
		   T2.Ziff > T1.Ziff AND 
		   T1.Ziff > 0
	 ORDER by IdModel,IdField,IdZiffAccount,Client
	) T2
	END /** Base table records - Baseline Cost Structure Assumptions By Field **/
			
	BEGIN /** Create Base for Technical Driver Data **/
	INSERT INTO tblCalcTechnicalDriverData ([IdModel],[IdTechnicalDriver],[IdField],[IdProject],[IdTypeOperation],[Year],[Normal],[Pessimistic],[Optimistic])
	SELECT tblTechnicalDriverData.IdModel, tblTechnicalDriverData.IdTechnicalDriver, tblTechnicalDriverData.IdField, CASE WHEN tblProjects.Operation = 1 THEN 0 ELSE tblProjects.IdProject END AS IdProject, 
		tblFields.IdTypeOperation, tblTechnicalDriverData.Year, SUM(tblTechnicalDriverData.Normal) AS Normal, SUM(tblTechnicalDriverData.Pessimistic) AS Pessimistic, 
		SUM(tblTechnicalDriverData.Optimistic) AS Optimistic
	FROM tblTechnicalDriverData INNER JOIN
		tblFields ON tblTechnicalDriverData.IdField = tblFields.IdField AND tblTechnicalDriverData.IdModel = tblFields.IdModel INNER JOIN
		tblProjects ON tblTechnicalDriverData.IdModel = tblProjects.IdModel AND tblTechnicalDriverData.IdProject = tblProjects.IdProject
	GROUP BY tblTechnicalDriverData.IdModel, tblTechnicalDriverData.IdTechnicalDriver, tblTechnicalDriverData.IdField, tblFields.IdTypeOperation, tblTechnicalDriverData.Year, 
		CASE WHEN tblProjects.Operation = 1 THEN 0 ELSE tblProjects.IdProject END
	HAVING tblTechnicalDriverData.IdModel=@IdModel;
	END /** Create Base for Technical Driver Data **/

	BEGIN /** SETUP HIERARCHY OF ASSUMPTIONS **/
	--------------------------------------------------------------------------------------
	-- SETUP HIERARCHY OF ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Set Hierarchy of Technical Assumptions (Specific TypeOperation) **/
	SET @intLoop = @intMaxLevel;
	WHILE @intLoop > 0 BEGIN
		SET @intLoop = @intLoop - 1
		
		INSERT INTO tblCalcTechnicalAssumption ([IdModel],[IdZiffAccount],[IdTypeOperation],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria])
		SELECT tblTechnicalAssumption.IdModel, tblTechnicalAssumption.IdZiffAccount, tblTechnicalAssumption.IdTypeOperation, 
			tblTechnicalAssumption.IdTechnicalDriverSize, tblTechnicalAssumption.IdTechnicalDriverPerformance, 
			tblTechnicalAssumption.CycleValue, tblTechnicalAssumption.ProjectionCriteria
		FROM tblTechnicalAssumption INNER JOIN
			tblZiffAccounts ON tblTechnicalAssumption.IdZiffAccount = tblZiffAccounts.IdZiffAccount AND 
			tblTechnicalAssumption.IdModel = tblZiffAccounts.IdModel
		WHERE tblTechnicalAssumption.IdTypeOperation<>0 AND tblZiffAccounts.RelationLevel=@intLoop AND tblTechnicalAssumption.IdModel=@IdModel;
	END
	
	SET @intLoop = @intMaxLevel;
	WHILE @intLoop > 0 BEGIN
		SET @intLoop = @intLoop - 1
		
		INSERT INTO tblCalcTechnicalAssumption ([IdModel],[IdZiffAccount],[IdTypeOperation],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria])
		SELECT tblTechnicalAssumption.IdModel, tblZiffAccounts.IdZiffAccount, tblTechnicalAssumption.IdTypeOperation, 
			tblTechnicalAssumption.IdTechnicalDriverSize, tblTechnicalAssumption.IdTechnicalDriverPerformance, 
			tblTechnicalAssumption.CycleValue, tblTechnicalAssumption.ProjectionCriteria
		FROM tblTechnicalAssumption INNER JOIN
			tblCalcZiffAccountParentChild ON tblTechnicalAssumption.IdZiffAccount = tblCalcZiffAccountParentChild.IdParent AND 
			tblTechnicalAssumption.IdModel = tblCalcZiffAccountParentChild.IdModel INNER JOIN
			tblZiffAccounts ON tblCalcZiffAccountParentChild.IdChild = tblZiffAccounts.IdZiffAccount AND 
			tblCalcZiffAccountParentChild.IdModel = tblZiffAccounts.IdModel INNER JOIN
			tblZiffAccounts AS tblZiffAccountsParent ON tblTechnicalAssumption.IdZiffAccount = tblZiffAccountsParent.IdZiffAccount
		WHERE tblTechnicalAssumption.IdTypeOperation<>0 AND tblZiffAccountsParent.RelationLevel=@intLoop AND tblTechnicalAssumption.IdModel=@IdModel AND tblZiffAccounts.IdZiffAccount NOT IN (SELECT CTA.IdZiffAccount FROM tblCalcTechnicalAssumption AS CTA WHERE CTA.IdTypeOperation=tblTechnicalAssumption.IdTypeOperation AND CTA.IdModel=@IdModel);
	END
	
	/** Set Hierarchy of Technical Assumptions (General/All TypeOperation) **/
	SET @intLoop = @intMaxLevel;
	WHILE @intLoop > 0 BEGIN
		SET @intLoop = @intLoop - 1
		
		INSERT INTO tblCalcTechnicalAssumption ([IdModel],[IdZiffAccount],[IdTypeOperation],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria])
		SELECT tblTechnicalAssumption.IdModel, tblTechnicalAssumption.IdZiffAccount, TypeOpList.IdTypeOperation, tblTechnicalAssumption.IdTechnicalDriverSize, 
			tblTechnicalAssumption.IdTechnicalDriverPerformance, 
			tblTechnicalAssumption.CycleValue, tblTechnicalAssumption.ProjectionCriteria
		FROM tblTechnicalAssumption INNER JOIN
			tblZiffAccounts ON tblTechnicalAssumption.IdZiffAccount = tblZiffAccounts.IdZiffAccount AND 
			tblTechnicalAssumption.IdModel = tblZiffAccounts.IdModel CROSS JOIN
			  (SELECT IdTypeOperation FROM tblFields WHERE (IdModel=@IdModel) GROUP BY IdTypeOperation) AS TypeOpList
		WHERE tblTechnicalAssumption.IdTypeOperation=0 AND tblZiffAccounts.RelationLevel=@intLoop AND tblTechnicalAssumption.IdModel=@IdModel AND tblZiffAccounts.IdZiffAccount NOT IN (SELECT IdZiffAccount FROM tblCalcTechnicalAssumption AS CTA WHERE IdTypeOperation=TypeOpList.IdTypeOperation AND IdModel=@IdModel);
	END
	
	SET @intLoop = @intMaxLevel;
	WHILE @intLoop > 0 BEGIN
		SET @intLoop = @intLoop - 1
		
		INSERT INTO tblCalcTechnicalAssumption ([IdModel],[IdZiffAccount],[IdTypeOperation],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria])
		SELECT tblTechnicalAssumption.IdModel, tblZiffAccounts.IdZiffAccount, TypeOpList.IdTypeOperation, tblTechnicalAssumption.IdTechnicalDriverSize, 
			tblTechnicalAssumption.IdTechnicalDriverPerformance, 
			tblTechnicalAssumption.CycleValue, tblTechnicalAssumption.ProjectionCriteria
		FROM tblTechnicalAssumption INNER JOIN
			tblCalcZiffAccountParentChild ON tblTechnicalAssumption.IdZiffAccount = tblCalcZiffAccountParentChild.IdParent AND 
			tblTechnicalAssumption.IdModel = tblCalcZiffAccountParentChild.IdModel INNER JOIN
			tblZiffAccounts ON tblCalcZiffAccountParentChild.IdChild = tblZiffAccounts.IdZiffAccount AND 
			tblCalcZiffAccountParentChild.IdModel = tblZiffAccounts.IdModel INNER JOIN
			tblZiffAccounts AS tblZiffAccountsParent ON tblTechnicalAssumption.IdZiffAccount = tblZiffAccountsParent.IdZiffAccount CROSS JOIN
			  (SELECT IdTypeOperation FROM tblFields WHERE (IdModel=@IdModel) GROUP BY IdTypeOperation) AS TypeOpList
		WHERE tblTechnicalAssumption.IdTypeOperation=0 AND tblZiffAccountsParent.RelationLevel=@intLoop AND tblTechnicalAssumption.IdModel=@IdModel AND tblZiffAccounts.IdZiffAccount NOT IN (SELECT IdZiffAccount FROM tblCalcTechnicalAssumption AS CTA WHERE IdTypeOperation=TypeOpList.IdTypeOperation AND IdModel=@IdModel);
	END
	
	/** Remove all that have been marked as N/A **/
	DELETE FROM tblCalcTechnicalAssumption WHERE [ProjectionCriteria]=98 AND [IdModel]=@IdModel;
	END  /** SETUP HIERARCHY OF ASSUMPTIONS **/
	
	--------------------------------------------------------------------------------------
	-- SETUP COMPANY BASE AND FORECAST ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Create Company BaseYear Technical Driver Assumption Lookup **/
	DECLARE @YearBase INT;
	DECLARE YearList CURSOR FOR
	SELECT Client AS Year
	FROM #tblCalcCostStructureAssumptionsByField
	WHERE IdModel=@IdModel
	GROUP BY Client
	HAVING Client > 0
	ORDER BY Year;
	/*** BEGIN OLD CODE ***/
	--SELECT tblCostStructureAssumptionsByProject.Client AS Year
	--FROM tblCostStructureAssumptionsByProject INNER JOIN
	--	tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject
	--WHERE tblProjects.IdModel=@IdModel
	--GROUP BY tblCostStructureAssumptionsByProject.Client
	--HAVING tblCostStructureAssumptionsByProject.Client > 0
	--ORDER BY 1
	/*** END OLD CODE ***/

	OPEN YearList;
	FETCH NEXT FROM YearList INTO @YearBase;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO tblCalcTechnicalDriverAssumption ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],
													  [CycleValue],[ProjectionCriteria],[YearClient],[YearZiff])
		SELECT 1, tblCalcTechnicalAssumption.IdModel, tblCalcTechnicalAssumption.IdZiffAccount, tblCalcTechnicalAssumption.IdTypeOperation, tblFields.IdField, 
			CASE WHEN tblProjects.Operation = 1 THEN 0 ELSE tblProjects.IdProject END AS IdProject, @YearBase + CASE WHEN tblCalcTechnicalAssumption.ProjectionCriteria = 4 AND 
			tblCalcCostStructureAssumptionsByField.Client<>@intMinYear THEN tblCalcTechnicalAssumption.CycleValue - 1 ELSE 0 END AS [Year], tblProjects.Operation, CAST(1 AS BIT) AS BaseYear, 
			tblCalcTechnicalAssumption.IdTechnicalDriverSize, tblCalcTechnicalAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalAssumption.CycleValue, 
			tblCalcTechnicalAssumption.ProjectionCriteria, tblCalcCostStructureAssumptionsByField.Client AS [YearClient], MIN(NULLIF(tblCalcCostStructureAssumptionsByField.Ziff,0)) AS [YearZiff]
		FROM tblCalcTechnicalAssumption LEFT OUTER JOIN
			#tblCalcCostStructureAssumptionsByField AS tblCalcCostStructureAssumptionsByField LEFT OUTER JOIN
			tblFields ON tblCalcCostStructureAssumptionsByField.IdField = tblFields.IdField ON tblCalcTechnicalAssumption.IdZiffAccount = tblCalcCostStructureAssumptionsByField.IdZiffAccount AND 
			tblFields.IdTypeOperation = tblCalcTechnicalAssumption.IdTypeOperation LEFT OUTER JOIN
			tblProjects ON tblFields.IdField = tblProjects.IdField
		GROUP BY tblCalcTechnicalAssumption.IdModel, tblCalcTechnicalAssumption.IdZiffAccount, tblCalcTechnicalAssumption.IdTypeOperation, tblFields.IdField, 
			CASE WHEN tblProjects.Operation = 1 THEN 0 ELSE tblProjects.IdProject END, tblProjects.Operation, tblCalcTechnicalAssumption.IdTechnicalDriverSize, 
			tblCalcTechnicalAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalAssumption.CycleValue, tblCalcTechnicalAssumption.ProjectionCriteria, 
			tblCalcCostStructureAssumptionsByField.Client
		HAVING tblCalcCostStructureAssumptionsByField.Client=@YearBase AND tblProjects.Operation=1 AND tblCalcTechnicalAssumption.IdModel=@IdModel

		FETCH NEXT FROM YearList INTO @YearBase;
	END

	CLOSE YearList;
	DEALLOCATE YearList;

	/** Set Semi-Variable Flag **/
	UPDATE tblCalcTechnicalDriverAssumption SET [SemivarForecast]=1 WHERE ProjectionCriteria=5 AND Operation=1 AND IdModel=@IdModel;

	/** Update Company BaseYear Driver Values **/
	EXEC calcModuleTechnicalDriverUpdate @IdModel,1,0;
	
	/** Update Company BaseCost Value for BaseYear **/
	/** Fixed and SemiFixed **/
	UPDATE tblCalcBaseCostByField SET
		[TechApplicable]=1,
		[TFNormal] = 1,
		[TFPessimistic] = 1,
		[TFOptimistic] = 1
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField
	WHERE tblCalcTechnicalDriverAssumption.BaseYear=1 AND tblCalcTechnicalDriverAssumption.Operation=1 AND tblCalcTechnicalDriverAssumption.ProjectionCriteria IN (1,2) AND tblCalcBaseCostByField.IdModel=@IdModel;

	/** Variable, Cyclical and SemiVariable **/
	UPDATE tblCalcBaseCostByField SET
		tblCalcBaseCostByField.TechApplicable=1,
		tblCalcBaseCostByField.TFNormal = CBCBF.TFNormal,
		tblCalcBaseCostByField.TFPessimistic = CBCBF.TFPessimistic, 
		tblCalcBaseCostByField.TFOptimistic = CBCBF.TFOptimistic
	FROM tblCalcBaseCostByField INNER JOIN
		(
		SELECT tblCalcBaseCostByField_1.IdModel, tblCalcBaseCostByField_1.IdActivity, tblCalcBaseCostByField_1.IdResources, 
			tblCalcBaseCostByField_1.IdClientAccount, tblCalcBaseCostByField_1.IdField, tblCalcBaseCostByField_1.IdClientCostCenter, 
			tblCalcBaseCostByField_1.IdZiffAccount, SUM(CASE WHEN [SizeNormal] IS NULL 
			THEN 0 ELSE [SizeNormal] * (CASE WHEN ISNULL([PerformanceNormal], 0) = 0 THEN 1 ELSE [PerformanceNormal] END) END) AS TFNormal, 
			SUM(CASE WHEN [SizePessimistic] IS NULL THEN 0 ELSE [SizePessimistic] * (CASE WHEN ISNULL([PerformancePessimistic], 0) 
			= 0 THEN 1 ELSE [PerformancePessimistic] END) END) AS TFPessimistic, SUM(CASE WHEN [SizeOptimistic] IS NULL 
			THEN 0 ELSE [SizeOptimistic] * (CASE WHEN ISNULL([PerformanceOptimistic], 0) = 0 THEN 1 ELSE [PerformanceOptimistic] END) END) 
			AS TFOptimistic
		FROM tblCalcBaseCostByField AS tblCalcBaseCostByField_1 INNER JOIN
			tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField_1.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
			tblCalcBaseCostByField_1.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount AND 
			tblCalcBaseCostByField_1.IdField = tblCalcTechnicalDriverAssumption.IdField
			WHERE tblCalcTechnicalDriverAssumption.BaseYear=1 AND tblCalcTechnicalDriverAssumption.Operation=1 AND 
			tblCalcTechnicalDriverAssumption.ProjectionCriteria IN (3, 4, 5)
		GROUP BY tblCalcBaseCostByField_1.IdModel, tblCalcBaseCostByField_1.IdActivity, tblCalcBaseCostByField_1.IdResources, 
			tblCalcBaseCostByField_1.IdClientAccount, tblCalcBaseCostByField_1.IdField, tblCalcBaseCostByField_1.IdClientCostCenter, 
			tblCalcBaseCostByField_1.IdZiffAccount
		) AS CBCBF ON tblCalcBaseCostByField.IdModel = CBCBF.IdModel AND 
		tblCalcBaseCostByField.IdActivity = CBCBF.IdActivity AND tblCalcBaseCostByField.IdResources = CBCBF.IdResources AND 
		tblCalcBaseCostByField.IdClientAccount = CBCBF.IdClientAccount AND tblCalcBaseCostByField.IdField = CBCBF.IdField AND 
		tblCalcBaseCostByField.IdClientCostCenter = CBCBF.IdClientCostCenter AND tblCalcBaseCostByField.IdZiffAccount = CBCBF.IdZiffAccount
	WHERE tblCalcBaseCostByField.IdModel = @IdModel;

	UPDATE tblCalcBaseCostByField SET
		[BCRNormal] = CASE WHEN ISNULL([TFNormal],0)=0 THEN 0 ELSE [Amount]/[TFNormal] END,
		[BCRPessimistic] = CASE WHEN ISNULL([TFPessimistic],0)=0 THEN 0 ELSE [Amount]/[TFPessimistic] END,
		[BCROptimistic] = CASE WHEN ISNULL([TFOptimistic],0)=0 THEN 0 ELSE [Amount]/[TFOptimistic] END
	WHERE TechApplicable=1 AND IdModel=@IdModel;

	UPDATE tblCalcBaseCostByField SET
		[BCRNormal] = 0,
		[BCRPessimistic] = 0,
		[BCROptimistic] = 0
	WHERE TechApplicable=0 AND IdModel=@IdModel;

	--------------------------------------------------------------------------------------
	-- SETUP ZIFF BASE AND FORECAST ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Create Ziff BaseYear Technical Driver Assumption Lookup **/
	DECLARE @YearBaseZiff INT;
	DECLARE @IdPeerGroup INT;
	DECLARE YearListZiff CURSOR FOR
	SELECT Ziff AS Year
	FROM #tblCalcCostStructureAssumptionsByField--tblCalcCostStructureAssumptionsByField
	WHERE IdModel=@IdModel
	GROUP BY Ziff
	HAVING Ziff > 0
	ORDER BY Year;
	/*** BEGIN OLD CODE ***/
	--SELECT tblCostStructureAssumptionsByProject.Ziff AS Year, tblCostStructureAssumptionsByProject.IdPeerGroup
	--FROM tblCostStructureAssumptionsByProject INNER JOIN
	--	tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject
	--WHERE tblProjects.IdModel=@IdModel
	--GROUP BY Ziff, IdPeerGroup
	--HAVING tblCostStructureAssumptionsByProject.Ziff > 0
	--order by Year
	/*** END OLD CODE ***/

	OPEN YearListZiff;
	FETCH NEXT FROM YearListZiff INTO @YearBaseZiff;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		INSERT INTO tblCalcTechnicalDriverAssumptionZiff ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],
														  [CycleValue],[ProjectionCriteria],[YearClient],[YearZiff],[IdPeerGroup])
		SELECT 1, tblCalcTechnicalAssumption.IdModel, tblCalcTechnicalAssumption.IdZiffAccount, tblCalcTechnicalAssumption.IdTypeOperation, tblFields.IdField, 
			CASE WHEN tblProjects.Operation = 1 THEN 0 ELSE tblProjects.IdProject END AS IdProject, @YearBaseZiff + CASE WHEN tblCalcTechnicalAssumption.ProjectionCriteria = 4 AND 
			tblCalcCostStructureAssumptionsByField.Ziff = 0 THEN tblCalcTechnicalAssumption.CycleValue - 1 ELSE 0 END AS [Year], tblProjects.Operation, CAST(1 AS BIT) AS BaseYear, 
			tblCalcTechnicalAssumption.IdTechnicalDriverSize, tblCalcTechnicalAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalAssumption.CycleValue, 
			tblCalcTechnicalAssumption.ProjectionCriteria, tblCalcCostStructureAssumptionsByField.Client, tblCalcCostStructureAssumptionsByField.Ziff, tblCalcCostStructureAssumptionsByField.IdPeerGroup
		FROM tblCalcTechnicalAssumption LEFT OUTER JOIN
			#tblCalcCostStructureAssumptionsByField AS tblCalcCostStructureAssumptionsByField LEFT OUTER JOIN
			tblFields ON tblCalcCostStructureAssumptionsByField.IdField = tblFields.IdField ON tblCalcTechnicalAssumption.IdZiffAccount = tblCalcCostStructureAssumptionsByField.IdZiffAccount AND 
			tblFields.IdTypeOperation = tblCalcTechnicalAssumption.IdTypeOperation LEFT OUTER JOIN
			tblProjects ON tblFields.IdField = tblProjects.IdField
		GROUP BY tblCalcTechnicalAssumption.IdModel, tblCalcTechnicalAssumption.IdZiffAccount, tblCalcTechnicalAssumption.IdTypeOperation, tblFields.IdField, 
			CASE WHEN tblProjects.Operation = 1 THEN 0 ELSE tblProjects.IdProject END, tblProjects.Operation, tblCalcTechnicalAssumption.IdTechnicalDriverSize, 
			tblCalcTechnicalAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalAssumption.CycleValue, tblCalcTechnicalAssumption.ProjectionCriteria, 
			tblCalcCostStructureAssumptionsByField.Client, tblCalcCostStructureAssumptionsByField.Ziff, tblCalcCostStructureAssumptionsByField.IdPeerGroup
		HAVING tblCalcCostStructureAssumptionsByField.Ziff=@YearBaseZiff AND tblProjects.Operation=1 AND tblCalcTechnicalAssumption.IdModel=@IdModel;

		FETCH NEXT FROM YearListZiff INTO @YearBaseZiff;
	END
	CLOSE YearListZiff;
	DEALLOCATE YearListZiff;

	/** Set Semi-Variable Flag **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET [SemivarForecast]=1 WHERE ProjectionCriteria=5 AND Operation=1 AND IdModel=@IdModel;
	
	/** Update Ziff BaseYear Driver Values **/
	EXEC calcModuleTechnicalDriverUpdate @IdModel,2,1;

	/** Update Ziff BaseCost Value for BaseYear **/
	UPDATE tblCalcBaseCostByFieldZiff SET
		[TechApplicable]=1,
		[TFNormal] = 1,
		[TFPessimistic] = 1,
		[TFOptimistic] = 1
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND
		tblCalcBaseCostByFieldZiff.IdPeerGroup = tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup
	WHERE tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 AND tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria IN (1,2) AND tblCalcBaseCostByFieldZiff.IdModel=@IdModel;

	UPDATE tblCalcBaseCostByFieldZiff SET
		tblCalcBaseCostByFieldZiff.TechApplicable=1,
		tblCalcBaseCostByFieldZiff.TFNormal = CBCBFZ.TFNormal,
		tblCalcBaseCostByFieldZiff.TFPessimistic = CBCBFZ.TFPessimistic,
		tblCalcBaseCostByFieldZiff.TFOptimistic = CBCBFZ.TFOptimistic
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		 (
		 SELECT tblCalcBaseCostByFieldZiff_1.IdModel, tblCalcBaseCostByFieldZiff_1.IdField, tblCalcBaseCostByFieldZiff_1.IdZiffAccount, tblCalcBaseCostByFieldZiff_1.IdPeerGroup,
			 SUM(CASE WHEN [SizeNormal] IS NULL THEN 0 ELSE [SizeNormal] * (CASE WHEN ISNULL([PerformanceNormal], 0) = 0 THEN 1 ELSE [PerformanceNormal] END) END) AS TFNormal, 
			 SUM(CASE WHEN [SizePessimistic] IS NULL THEN 0 ELSE [SizePessimistic] * (CASE WHEN ISNULL([PerformancePessimistic], 0) = 0 THEN 1 ELSE [PerformancePessimistic] END) END) AS TFPessimistic, 
			 SUM(CASE WHEN [SizeOptimistic] IS NULL THEN 0 ELSE [SizeOptimistic] * (CASE WHEN ISNULL([PerformanceOptimistic], 0) = 0 THEN 1 ELSE [PerformanceOptimistic] END) END) AS TFOptimistic
		 FROM tblCalcBaseCostByFieldZiff AS tblCalcBaseCostByFieldZiff_1 INNER JOIN
			 tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff_1.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
			 tblCalcBaseCostByFieldZiff_1.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount AND 
			 tblCalcBaseCostByFieldZiff_1.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND 
			 tblCalcBaseCostByFieldZiff_1.IdPeerGroup  = tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup
		 WHERE tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 AND 
			tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria IN (3, 4, 5)
		 GROUP BY tblCalcBaseCostByFieldZiff_1.IdModel, tblCalcBaseCostByFieldZiff_1.IdField, tblCalcBaseCostByFieldZiff_1.IdZiffAccount, tblCalcBaseCostByFieldZiff_1.IdPeerGroup
		 ) AS CBCBFZ ON 
		 tblCalcBaseCostByFieldZiff.IdField = CBCBFZ.IdField AND 
		 tblCalcBaseCostByFieldZiff.IdZiffAccount = CBCBFZ.IdZiffAccount AND
		 tblCalcBaseCostByFieldZiff.IdPeerGroup = CBCBFZ.IdPeerGroup
	WHERE tblCalcBaseCostByFieldZiff.IdModel = @IdModel;

	/** BEGIN OLD CODE **/	
	UPDATE tblCalcBaseCostByFieldZiff SET
		[BCRNormal] = CASE WHEN ISNULL([TFNormal],0)=0 THEN 0 ELSE CBCBFZ.Normal/tblCalcBaseCostByFieldZiff.[TFNormal] END,
		[BCRPessimistic] = CASE WHEN ISNULL([TFPessimistic],0)=0 THEN 0 ELSE CBCBFZ.Pessimistic/tblCalcBaseCostByFieldZiff.[TFPessimistic] END,
		[BCROptimistic] = CASE WHEN ISNULL([TFOptimistic],0)=0 THEN 0 ELSE CBCBFZ.Optimistic/tblCalcBaseCostByFieldZiff.[TFOptimistic] END
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		 ( SELECT  tblProjects.IdModel,tblProjects.IdField,tblCostStructureAssumptionsByProject.IdZiffAccount,tblCostStructureAssumptionsByProject.IdPeerGroup,
					tblCostStructureAssumptionsByProject.Client,tblCostStructureAssumptionsByProject.Ziff,CalcBaseCostByFieldSummaryZiff.Normal,
					CalcBaseCostByFieldSummaryZiff.Pessimistic,CalcBaseCostByFieldSummaryZiff.Optimistic
			FROM    tblCostStructureAssumptionsByProject LEFT OUTER JOIN
					tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject LEFT OUTER JOIN
					tblZiffAccounts ON tblCostStructureAssumptionsByProject.IdZiffAccount = tblZiffAccounts.IdZiffAccount LEFT OUTER JOIN
					tblZiffAccounts AS tblZiffAccountsParent ON tblZiffAccounts.Parent = tblZiffAccountsParent.IdZiffAccount INNER JOIN
					tblPeerGroup ON tblCostStructureAssumptionsByProject.IdPeerGroup = tblPeerGroup.IdPeerGroup INNER JOIN
					(
					 SELECT tblCalcTechnicalDriverData.IdModel,tblCalcTechnicalDriverData.IdField,tblCalcTechnicalDriverData.Year,
							SUM(tblCalcTechnicalDriverData.Normal) AS Normal,
							SUM(tblCalcTechnicalDriverData.Pessimistic) AS Pessimistic,
							SUM(tblCalcTechnicalDriverData.Optimistic) AS Optimistic
					 FROM tblCalcTechnicalDriverData INNER JOIN
							tblTechnicalDrivers ON tblCalcTechnicalDriverData.IdTechnicalDriver = tblTechnicalDrivers.IdTechnicalDriver
					 WHERE tblTechnicalDrivers.UnitCostDenominator=1 
					 GROUP BY tblCalcTechnicalDriverData.IdModel,tblCalcTechnicalDriverData.IdField,tblCalcTechnicalDriverData.Year
					) AS CalcBaseCostByFieldSummaryZiff ON tblProjects.IdModel = CalcBaseCostByFieldSummaryZiff.IdModel AND
					tblProjects.IdField = CalcBaseCostByFieldSummaryZiff.IdField AND
					tblCostStructureAssumptionsByProject.Ziff = CalcBaseCostByFieldSummaryZiff.Year
			GROUP BY tblProjects.IdModel,tblProjects.IdField,tblCostStructureAssumptionsByProject.IdZiffAccount,tblCostStructureAssumptionsByProject.IdPeerGroup,
					tblCostStructureAssumptionsByProject.Client,tblCostStructureAssumptionsByProject.Ziff,CalcBaseCostByFieldSummaryZiff.Normal,
					CalcBaseCostByFieldSummaryZiff.Pessimistic,CalcBaseCostByFieldSummaryZiff.Optimistic
		) AS CBCBFZ ON
		tblCalcBaseCostByFieldZiff.IdModel = CBCBFZ.IdModel AND
		tblCalcBaseCostByFieldZiff.IdField = CBCBFZ.IdField AND
		tblCalcBaseCostByFieldZiff.IdZiffAccount = CBCBFZ.IdZiffAccount AND
		tblCalcBaseCostByFieldZiff.IdPeerGroup = CBCBFZ.IdPeerGroup
	WHERE TechApplicable=1 AND tblCalcBaseCostByFieldZiff.IdModel=@IdModel;
	/** END OLD CODE **/

	/** BEGIN NEW CODE **/
	UPDATE tblCalcBaseCostByFieldZiff SET
		[BCRNormal] = CASE WHEN ISNULL([TFNormal],0)=0 THEN 0 ELSE tblCalcBaseCostByFieldZiff.[TotalCostNormal]/tblCalcBaseCostByFieldZiff.[TFNormal] END /*CASE WHEN ISNULL([TFNormal],0)=0 THEN 0 ELSE tblCalcBaseCostByFieldZiff.[TotalCostNormal] END */,
		[BCRPessimistic] = CASE WHEN ISNULL([TFPessimistic],0)=0 THEN 0 ELSE tblCalcBaseCostByFieldZiff.[TotalCostPessimistic]/tblCalcBaseCostByFieldZiff.[TFPessimistic] END /*CASE WHEN ISNULL([TFPessimistic],0)=0 THEN 0 ELSE tblCalcBaseCostByFieldZiff.[TotalCostPessimistic] END*/,
		[BCROptimistic] = CASE WHEN ISNULL([TFOptimistic],0)=0 THEN 0 ELSE tblCalcBaseCostByFieldZiff.[TotalCostOptimistic]/tblCalcBaseCostByFieldZiff.[TFOptimistic] END/*CASE WHEN ISNULL([TFOptimistic],0)=0 THEN 0 ELSE  tblCalcBaseCostByFieldZiff.[TotalCostOptimistic] END*/ 
	FROM tblCalcBaseCostByFieldZiff
	WHERE TechApplicable=1 AND tblCalcBaseCostByFieldZiff.IdModel=@IdModel;
	/** END NEW CODE **/
	
	UPDATE tblCalcBaseCostByFieldZiff SET
		[BCRNormal] = 0,
		[BCRPessimistic] = 0,
		[BCROptimistic] = 0
	WHERE TechApplicable=0 AND IdModel=@IdModel;

	--------------------------------------------------------------------------------------
	-- UPDATE BASE COST RATES BASE YEAR
	--------------------------------------------------------------------------------------
	/** Update Company Base Cost Rates **/
	UPDATE tblCalcTechnicalDriverAssumption SET
		[BCRNormal] = CalcBaseCostByFieldSummary.BCRNormal,
		[BCRPessimistic] = CalcBaseCostByFieldSummary.BCRPessimistic,
		[BCROptimistic] = CalcBaseCostByFieldSummary.BCROptimistic
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(BCRNormal) AS BCRNormal, SUM(BCRPessimistic) AS BCRPessimistic, SUM(BCROptimistic) AS BCROptimistic
		FROM tblCalcBaseCostByField AS CBCBF
		GROUP BY IdModel, IdField, IdZiffAccount
		) AS CalcBaseCostByFieldSummary ON 
		tblCalcTechnicalDriverAssumption.IdModel = CalcBaseCostByFieldSummary.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = CalcBaseCostByFieldSummary.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = CalcBaseCostByFieldSummary.IdField
	WHERE tblCalcTechnicalDriverAssumption.BaseYear=1 AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Update Ziff Base Cost Rates **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		[BCRNormal] = CalcBaseCostByFieldSummaryZiff.BCRNormal,
		[BCRPessimistic] = CalcBaseCostByFieldSummaryZiff.BCRPessimistic,
		[BCROptimistic] = CalcBaseCostByFieldSummaryZiff.BCROptimistic
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(TotalCostNormal) AS BCRNormal, SUM(TotalCostPessimistic) AS BCRPessimistic, SUM(TotalCostOptimistic) AS BCROptimistic, IdPeerGroup
		--SUM(BCRNormal) AS BCRNormal, SUM(BCRPessimistic) AS BCRPessimistic, SUM(BCROptimistic) AS BCROptimistic, IdPeerGroup
		FROM tblCalcBaseCostByFieldZiff AS CBCBFZ
		GROUP BY IdModel, IdField, IdZiffAccount, IdPeerGroup
		) AS CalcBaseCostByFieldSummaryZiff ON 
		tblCalcTechnicalDriverAssumptionZiff.IdModel = CalcBaseCostByFieldSummaryZiff.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = CalcBaseCostByFieldSummaryZiff.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = CalcBaseCostByFieldSummaryZiff.IdField AND
		tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup = CalcBaseCostByFieldSummaryZiff.IdPeerGroup
	WHERE tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;

	/** Drop Ziff BaseYear with no costs **/
	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE [BaseYear]=1 AND [BCRNormal] IS NULL AND [BCRPessimistic] IS NULL AND [BCROptimistic] IS NULL AND [IdModel]=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- SETUP ADDITIONAL YEARS ASSUMPTIONS
	--------------------------------------------------------------------------------------

	/** Create Company Additional Years Technical Driver Assumption Lookup **/
	INSERT INTO tblCalcTechnicalDriverAssumption ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff],[SemivarForecast])
	SELECT 2, tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, tblCalcTechnicalDriverAssumption.IdTypeOperation, 
		tblCalcTechnicalDriverAssumption.IdField, tblCalcTechnicalDriverAssumption.IdProject, tblCalcModelYears.Year, 
		tblCalcTechnicalDriverAssumption.Operation, CAST(0 AS BIT) AS BaseYear, tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize, 
		tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalDriverAssumption.CycleValue, 
		tblCalcTechnicalDriverAssumption.ProjectionCriteria, tblCalcTechnicalDriverAssumption.YearClient, tblCalcTechnicalDriverAssumption.YearZiff, 
		tblCalcTechnicalDriverAssumption.SemivarForecast
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		tblCalcModelYears ON tblCalcTechnicalDriverAssumption.IdModel = tblCalcModelYears.IdModel AND 
		tblCalcTechnicalDriverAssumption.Year < tblCalcModelYears.Year
	WHERE tblCalcModelYears.Year<=@intMaxYear AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel
	GROUP BY tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, tblCalcTechnicalDriverAssumption.IdTypeOperation, 
		tblCalcTechnicalDriverAssumption.IdField, tblCalcTechnicalDriverAssumption.IdProject, tblCalcModelYears.Year, 
		tblCalcTechnicalDriverAssumption.Operation, tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize, 
		tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalDriverAssumption.CycleValue, 
		tblCalcTechnicalDriverAssumption.ProjectionCriteria, tblCalcTechnicalDriverAssumption.YearClient, tblCalcTechnicalDriverAssumption.YearZiff, 
		tblCalcTechnicalDriverAssumption.SemivarForecast;
	
	/** Create Ziff Additional Years Technical Driver Assumption Lookup **/
	INSERT INTO tblCalcTechnicalDriverAssumptionZiff ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],
													  [CycleValue],[ProjectionCriteria],[YearClient],[YearZiff],[SemivarForecast],[IdPeerGroup])
	SELECT 2, tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdTypeOperation, 
		tblCalcTechnicalDriverAssumptionZiff.IdField, tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcModelYears.Year, 
		tblCalcTechnicalDriverAssumptionZiff.Operation, CAST(0 AS BIT) AS BaseYear, tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverSize, 
		tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverPerformance, tblCalcTechnicalDriverAssumptionZiff.CycleValue, 
		tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.YearClient, tblCalcTechnicalDriverAssumptionZiff.YearZiff, 
		tblCalcTechnicalDriverAssumptionZiff.SemivarForecast, tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		tblCalcModelYears ON tblCalcTechnicalDriverAssumptionZiff.IdModel = tblCalcModelYears.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.Year < tblCalcModelYears.Year
	WHERE tblCalcModelYears.Year<=@intMaxYear AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel
	GROUP BY tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdTypeOperation, 
		tblCalcTechnicalDriverAssumptionZiff.IdField, tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcModelYears.Year, 
		tblCalcTechnicalDriverAssumptionZiff.Operation, tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverSize, 
		tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverPerformance, tblCalcTechnicalDriverAssumptionZiff.CycleValue, 
		tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.YearClient, 
		tblCalcTechnicalDriverAssumptionZiff.YearZiff, tblCalcTechnicalDriverAssumptionZiff.SemivarForecast, tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup
	ORDER BY tblCalcTechnicalDriverAssumptionZiff.IdField,tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount,tblCalcTechnicalDriverAssumptionZiff.YearClient,tblCalcTechnicalDriverAssumptionZiff.YearZiff--, tblPeerGroupField.IdPeerGroup

	/** Create Company BusinessOpportunity Technical Driver Assumption Lookup **/
	INSERT INTO tblCalcTechnicalDriverAssumption ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff],[SemivarForecast])
	SELECT 3, tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, tblCalcTechnicalDriverAssumption.IdTypeOperation, 
		tblCalcTechnicalDriverAssumption.IdField, tblProjects.IdProject, tblCalcTechnicalDriverAssumption.Year, tblProjects.Operation, 
		CASE WHEN tblCalcTechnicalDriverAssumption.Year = tblProjects.Starts THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS BaseYear, 
		tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize, tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance,  
		tblCalcTechnicalDriverAssumption.CycleValue, tblCalcTechnicalDriverAssumption.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumption.YearClient, tblCalcTechnicalDriverAssumption.YearZiff, tblCalcTechnicalDriverAssumption.SemivarForecast
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumption.IdModel = tblProjects.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdField = tblProjects.IdField AND tblCalcTechnicalDriverAssumption.Year >= tblProjects.Starts
	WHERE tblProjects.Operation=2 AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel
	GROUP BY tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, tblCalcTechnicalDriverAssumption.IdTypeOperation, 
		tblCalcTechnicalDriverAssumption.IdField, tblProjects.IdProject, tblCalcTechnicalDriverAssumption.Year, tblProjects.Operation, 
		CASE WHEN tblCalcTechnicalDriverAssumption.Year = tblProjects.Starts THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END, 
		tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize, tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance,  
		tblCalcTechnicalDriverAssumption.CycleValue, tblCalcTechnicalDriverAssumption.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumption.YearClient, tblCalcTechnicalDriverAssumption.YearZiff, tblCalcTechnicalDriverAssumption.SemivarForecast
	
	/** Remove Company BusinessOpportunity Technical Driver Assumption = Fixed or Semi-Fixed **/
	DELETE FROM tblCalcTechnicalDriverAssumption WHERE ([ProjectionCriteria]=1 OR [ProjectionCriteria]=2) AND [Operation]=2 AND [IdModel]=@IdModel;

	/** Create Ziff BusinessOpportunity Technical Driver Assumption Lookup **/
	/** Note: Modifying SemivarProjection with Fixed=1 to Variable=3 for Forecast **/
	INSERT INTO tblCalcTechnicalDriverAssumptionZiff ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],
													  [CycleValue],[ProjectionCriteria],[YearClient],[YearZiff],[SemivarForecast],[IdPeerGroup])
	SELECT 3, tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdTypeOperation, 
		tblCalcTechnicalDriverAssumptionZiff.IdField, tblProjects.IdProject, tblCalcTechnicalDriverAssumptionZiff.Year, tblProjects.Operation, 
		CASE WHEN tblCalcTechnicalDriverAssumptionZiff.Year = tblProjects.Starts THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS BaseYear, 
		tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverSize, tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverPerformance, 
		tblCalcTechnicalDriverAssumptionZiff.CycleValue, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria,
		tblCalcTechnicalDriverAssumptionZiff.YearClient, tblCalcTechnicalDriverAssumptionZiff.YearZiff, tblCalcTechnicalDriverAssumptionZiff.SemivarForecast, tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumptionZiff.IdModel = tblProjects.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = tblProjects.IdField AND tblCalcTechnicalDriverAssumptionZiff.Year >= tblProjects.Starts
	WHERE tblProjects.Operation=2 AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel
	GROUP BY tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdTypeOperation, 
		tblCalcTechnicalDriverAssumptionZiff.IdField, tblProjects.IdProject, tblCalcTechnicalDriverAssumptionZiff.Year, tblProjects.Operation, 
		CASE WHEN tblCalcTechnicalDriverAssumptionZiff.Year = tblProjects.Starts THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END, 
		tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverSize, tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverPerformance, 
		tblCalcTechnicalDriverAssumptionZiff.CycleValue, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria,
		tblCalcTechnicalDriverAssumptionZiff.YearClient, tblCalcTechnicalDriverAssumptionZiff.YearZiff, tblCalcTechnicalDriverAssumptionZiff.SemivarForecast,
		tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup

	/** Remove Ziff BusinessOpportunity Technical Driver Assumption = Fixed or Semi-Fixed **/
	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE ([ProjectionCriteria]=1 OR [ProjectionCriteria]=2) AND [Operation]=2 AND [IdModel]=@IdModel;

	/** Remove overlapping years for the peer groups **/
	DELETE CTDA
	FROM tblCalcTechnicalDriverAssumptionZiff CTDA INNER JOIN 
		#tblCalcCostStructureAssumptionsByField ON CTDA.IdModel=#tblCalcCostStructureAssumptionsByField.IdModel AND
		CTDA.IdField=#tblCalcCostStructureAssumptionsByField.IdField AND
		CTDA.IdZiffAccount=#tblCalcCostStructureAssumptionsByField.IdZiffAccount AND
		CTDA.IdPeerGroup=#tblCalcCostStructureAssumptionsByField.IdPeerGroup
	WHERE CTDA.Year NOT BETWEEN #tblCalcCostStructureAssumptionsByField.Client AND #tblCalcCostStructureAssumptionsByField.ZiffEndYear 

	------------------------------------------------------------------------------------
	-- CLEAN UP ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Remove Base Year Assumptions not in initial Year for Semi-Fixed **/
	UPDATE tblCalcTechnicalDriverAssumption SET [BaseYear]=0 WHERE [ProjectionCriteria]=2 AND [BaseYear]=1 AND [Year]<>@intMinYear AND [IdModel]=@IdModel;

	/** Rebase and Remove Excess Years from all Cyclical Assumptions **/
	EXEC calcModuleTechnicalCyclicalRebase @IdModel,@intMinYear,@intMaxYear;
	
	--------------------------------------------------------------------------------------
	-- ADD CLIENT AND ZIFF COST ASSUMPTION START YEARS TO TABLE
	--------------------------------------------------------------------------------------
	
	/** Update Company Client/Ziff Years Lookup **/
	UPDATE tblCalcTechnicalDriverAssumption
	SET YearClient=CTDALookup.YearClient, YearZiff=CTDALookup.YearZiff
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		(
		SELECT CTDA.IdModel, CTDA.IdZiffAccount, CTDA.IdField, CTDA.YearClient, CTDA.YearZiff
		FROM tblCalcTechnicalDriverAssumption AS CTDA INNER JOIN
			#tblCalcCostStructureAssumptionsByField AS CSABF ON CTDA.IdModel = CSABF.IdModel AND CTDA.IdField = CSABF.IdField AND CTDA.IdZiffAccount = CSABF.IdZiffAccount
		WHERE CTDA.Operation=1 AND CTDA.BaseYear=1
		GROUP BY CTDA.IdModel, CTDA.IdZiffAccount, CTDA.IdField, CTDA.YearClient, CTDA.YearZiff
		) AS CTDALookup ON 
		tblCalcTechnicalDriverAssumption.IdModel = CTDALookup.IdModel AND tblCalcTechnicalDriverAssumption.IdZiffAccount = CTDALookup.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = CTDALookup.IdField
	WHERE tblCalcTechnicalDriverAssumption.YearClient IS NULL AND tblCalcTechnicalDriverAssumption.YearZiff IS NULL AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Remove Company Years Based on Company/Ziff Start Years **/
	DELETE FROM tblCalcTechnicalDriverAssumption WHERE [YearZiff]>0 AND [Year]>=[YearZiff] AND [IdModel]=@IdModel;

	/** Update Ziff Client/Ziff Years Lookup **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff
	SET YearClient=CTDAZLookup.YearClient, YearZiff=CTDAZLookup.YearZiff
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		(
		SELECT CTDAZ.IdModel, CTDAZ.IdZiffAccount, CTDAZ.IdField, CTDAZ.YearClient, CTDAZ.YearZiff
		FROM tblCalcTechnicalDriverAssumptionZiff AS CTDAZ INNER JOIN
			#tblCalcCostStructureAssumptionsByField AS CSABF ON CTDAZ.IdModel = CSABF.IdModel AND CTDAZ.IdField = CSABF.IdField AND CTDAZ.IdZiffAccount = CSABF.IdZiffAccount
		WHERE CTDAZ.Operation=1 AND CTDAZ.BaseYear=1
		GROUP BY CTDAZ.IdModel, CTDAZ.IdZiffAccount, CTDAZ.IdField, CTDAZ.YearClient, CTDAZ.YearZiff
		) AS CTDAZLookup ON 
		tblCalcTechnicalDriverAssumptionZiff.IdModel = CTDAZLookup.IdModel AND tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = CTDAZLookup.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = CTDAZLookup.IdField
	WHERE tblCalcTechnicalDriverAssumptionZiff.YearClient IS NULL AND tblCalcTechnicalDriverAssumptionZiff.YearZiff IS NULL AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;

	/** Remove Ziff Years Based on Company/Ziff Start Years (SHOULD BE NO RECORDS - THIS IS JUST A DOUBLE CHECK) **/
	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE [YearClient]>0 AND [Year]<[YearZiff] AND [IdModel]=@IdModel;
	
	/** Ensure All Ziff BO Items as properly flagged as base year **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET BaseYear=1 WHERE [Operation]=2 AND [BaseYear]=0 AND [Year]=[YearZiff] AND IdModel=@IdModel;	

	--------------------------------------------------------------------------------------
	-- PREP FOR TECHNICAL BASE COST RATE CALCS
	--------------------------------------------------------------------------------------
	/** Update Company TF (Base Year Only for BCR Calc) **/
	UPDATE tblCalcTechnicalDriverAssumption SET 
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN ISNULL([SizeNormal], 1) * ISNULL([PerformanceNormal],1) ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN ISNULL([SizePessimistic], 1) * ISNULL([PerformancePessimistic],1) ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN ISNULL([SizeOptimistic], 1) * ISNULL([PerformanceOptimistic],1) ELSE 0 END
	WHERE [BaseYear]=1 AND [IdModel]=@IdModel;
	
	/** Update Ziff TF  (Base Year Only for BCR Calc) **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN ISNULL([SizeNormal], 1) *ISNULL([PerformanceNormal],1) ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN ISNULL([SizePessimistic], 1) *ISNULL([PerformancePessimistic],1) ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN ISNULL([SizeOptimistic], 1) *ISNULL([PerformanceOptimistic],1) ELSE 0 END
	WHERE [BaseYear]=1 AND [IdModel]=@IdModel;

	/** Remove Base Year Items without Project Drivers **/
	DELETE FROM tblCalcTechnicalDriverAssumption
	WHERE SizeNormal IS NULL AND SizePessimistic IS NULL AND SizeOptimistic IS NULL AND PerformanceNormal IS NULL AND PerformancePessimistic IS NULL AND PerformanceOptimistic IS NULL AND ProjectionCriteria IN (2,3,4,5) AND BaseYear=1 AND Operation=1 AND IdModel=@IdModel;

	/** Technical Base Cost Rate **/
	EXEC calcModuleTechnicalBaseCostRate @IdModel;

	--------------------------------------------------------------------------------------
	-- POST TECHNICAL BASE COST RATE RESET
	--------------------------------------------------------------------------------------

	/** Update All Years Driver Values **/
	EXEC calcModuleTechnicalDriverUpdate @IdModel,1,0;
	EXEC calcModuleTechnicalDriverUpdate @IdModel,2,0;
		
	/** Update Company TF **/
	UPDATE tblCalcTechnicalDriverAssumption SET 
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN ISNULL([SizeNormal], 1) * ISNULL([PerformanceNormal],1) ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN ISNULL([SizePessimistic], 1) * ISNULL([PerformancePessimistic],1) ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN ISNULL([SizeOptimistic], 1) * ISNULL([PerformanceOptimistic],1) ELSE 0 END
	WHERE [IdModel]=@IdModel;
	
	/** Update Ziff TF **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5  THEN ISNULL([SizeNormal], 1) * ISNULL([PerformanceNormal],1) ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN ISNULL([SizePessimistic], 1) * ISNULL([PerformancePessimistic],1) ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN ISNULL([SizeOptimistic], 1) * ISNULL([PerformanceOptimistic],1)ELSE 0 END
	WHERE [IdModel]=@IdModel;

	/** Remove Base Year Items without Project Drivers **/
	DELETE FROM tblCalcTechnicalDriverAssumption
	WHERE SizeNormal IS NULL AND SizePessimistic IS NULL AND SizeOptimistic IS NULL AND PerformanceNormal IS NULL AND PerformancePessimistic IS NULL AND PerformanceOptimistic IS NULL AND ProjectionCriteria IN (2,3,4,5) AND BaseYear=1 AND Operation=1 AND IdModel=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- UPDATE BASE COST RATES ALL YEARS
	--------------------------------------------------------------------------------------
	
	/** Update Company Base Cost Rates **/
	UPDATE tblCalcTechnicalDriverAssumption SET
		[BCRNormal] = CalcBaseCostByFieldSummary.BCRNormal,
		[BCRPessimistic] = CalcBaseCostByFieldSummary.BCRPessimistic,
		[BCROptimistic] = CalcBaseCostByFieldSummary.BCROptimistic
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(BCRNormal) AS BCRNormal, SUM(BCRPessimistic) AS BCRPessimistic, SUM(BCROptimistic) AS BCROptimistic
		FROM tblCalcBaseCostByField AS CBCBF
		GROUP BY IdModel, IdField, IdZiffAccount
		) AS CalcBaseCostByFieldSummary ON 
		tblCalcTechnicalDriverAssumption.IdModel = CalcBaseCostByFieldSummary.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = CalcBaseCostByFieldSummary.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = CalcBaseCostByFieldSummary.IdField
	WHERE tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Update Ziff Base Cost Rates **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		[BCRNormal] = CalcBaseCostByFieldSummaryZiff.BCRNormal,
		[BCRPessimistic] = CalcBaseCostByFieldSummaryZiff.BCRPessimistic,
		[BCROptimistic] = CalcBaseCostByFieldSummaryZiff.BCROptimistic
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(BCRNormal) AS BCRNormal, SUM(BCRPessimistic) AS BCRPessimistic, SUM(BCROptimistic) AS BCROptimistic
		FROM tblCalcBaseCostByFieldZiff AS CBCBFZ
		GROUP BY IdModel, IdField, IdZiffAccount
		) AS CalcBaseCostByFieldSummaryZiff ON 
		tblCalcTechnicalDriverAssumptionZiff.IdModel = CalcBaseCostByFieldSummaryZiff.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = CalcBaseCostByFieldSummaryZiff.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = CalcBaseCostByFieldSummaryZiff.IdField
	WHERE tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;

	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		[BCRNormal] = CalcBaseCostByFieldSummaryZiff.BCRNormal,
		[BCRPessimistic] = CalcBaseCostByFieldSummaryZiff.BCRPessimistic,
		[BCROptimistic] = CalcBaseCostByFieldSummaryZiff.BCROptimistic
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(TotalCostNormal) AS BCRNormal, SUM(TotalCostPessimistic) AS BCRPessimistic, SUM(TotalCostOptimistic) AS BCROptimistic, IdPeerGroup
		FROM tblCalcBaseCostByFieldZiff AS CBCBFZ
		GROUP BY IdModel, IdField, IdZiffAccount, IdPeerGroup
		) AS CalcBaseCostByFieldSummaryZiff ON 
		tblCalcTechnicalDriverAssumptionZiff.IdModel = CalcBaseCostByFieldSummaryZiff.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = CalcBaseCostByFieldSummaryZiff.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = CalcBaseCostByFieldSummaryZiff.IdField
		AND tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup = CalcBaseCostByFieldSummaryZiff.IdPeerGroup
	WHERE tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel AND tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=3;

	--------------------------------------------------------------------------------------
	-- SEMI-VARIABLE Baseline Reset as Fixed Rates
	--------------------------------------------------------------------------------------

	/** Update Company Base Cost Rates Semi-Variable **/
	UPDATE tblCalcTechnicalDriverAssumption SET
		tblCalcTechnicalDriverAssumption.TFNormal=1,
		tblCalcTechnicalDriverAssumption.TFPessimistic=1,
		tblCalcTechnicalDriverAssumption.TFOptimistic=1,
		tblCalcTechnicalDriverAssumption.BCRNormal=BCRSV.SumAmount,
		tblCalcTechnicalDriverAssumption.BCRPessimistic=BCRSV.SumAmount,
		tblCalcTechnicalDriverAssumption.BCROptimistic=BCRSV.SumAmount
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		(
		SELECT tblCalcTechnicalDriverAssumption_1.IdModel, tblCalcTechnicalDriverAssumption_1.IdZiffAccount, tblCalcTechnicalDriverAssumption_1.IdField, tblCalcTechnicalDriverAssumption_1.IdProject, 
			tblCalcTechnicalDriverAssumption_1.Year, SUM(tblCalcBaseCostByField.Amount) AS SumAmount
		FROM tblCalcTechnicalDriverAssumption AS tblCalcTechnicalDriverAssumption_1 INNER JOIN
			tblCalcBaseCostByField ON tblCalcTechnicalDriverAssumption_1.IdModel = tblCalcBaseCostByField.IdModel AND 
			tblCalcTechnicalDriverAssumption_1.IdField = tblCalcBaseCostByField.IdField AND tblCalcTechnicalDriverAssumption_1.IdZiffAccount = tblCalcBaseCostByField.IdZiffAccount
		WHERE tblCalcTechnicalDriverAssumption_1.Operation=1 AND tblCalcTechnicalDriverAssumption_1.ProjectionCriteria=5
		GROUP BY tblCalcTechnicalDriverAssumption_1.IdModel, tblCalcTechnicalDriverAssumption_1.IdZiffAccount, tblCalcTechnicalDriverAssumption_1.IdField, tblCalcTechnicalDriverAssumption_1.IdProject, 
			tblCalcTechnicalDriverAssumption_1.Year
		) AS BCRSV ON tblCalcTechnicalDriverAssumption.IdModel = BCRSV.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = BCRSV.IdZiffAccount AND tblCalcTechnicalDriverAssumption.IdField = BCRSV.IdField AND 
		tblCalcTechnicalDriverAssumption.IdProject = BCRSV.IdProject AND tblCalcTechnicalDriverAssumption.Year = BCRSV.Year
	WHERE tblCalcTechnicalDriverAssumption.IdModel=@IdModel;

	UPDATE tblCalcTechnicalDriverAssumption SET
		tblCalcTechnicalDriverAssumption.TFNormal=1,
		tblCalcTechnicalDriverAssumption.TFPessimistic=1,
		tblCalcTechnicalDriverAssumption.TFOptimistic=1
	FROM tblCalcTechnicalDriverAssumption
	WHERE tblCalcTechnicalDriverAssumption.Operation=1 AND tblCalcTechnicalDriverAssumption.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;

	/** Update Ziff Base Cost Rates Semi-Variable **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		tblCalcTechnicalDriverAssumptionZiff.TFNormal=1,
		tblCalcTechnicalDriverAssumptionZiff.TFPessimistic=1,
		tblCalcTechnicalDriverAssumptionZiff.TFOptimistic=1,
		tblCalcTechnicalDriverAssumptionZiff.BCRNormal=BCRSVZ.SumAmount,
		tblCalcTechnicalDriverAssumptionZiff.BCRPessimistic=BCRSVZ.SumAmount,
		tblCalcTechnicalDriverAssumptionZiff.BCROptimistic=BCRSVZ.SumAmount
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		(
		SELECT tblCalcTechnicalDriverAssumptionZiff_1.IdModel, tblCalcTechnicalDriverAssumptionZiff_1.IdZiffAccount, tblCalcTechnicalDriverAssumptionZiff_1.IdField, tblCalcTechnicalDriverAssumptionZiff_1.IdProject, 
			tblCalcTechnicalDriverAssumptionZiff_1.Year, SUM(tblCalcBaseCostByFieldZiff.TotalCost) AS SumAmount
		FROM tblCalcTechnicalDriverAssumptionZiff AS tblCalcTechnicalDriverAssumptionZiff_1 INNER JOIN
			tblCalcBaseCostByFieldZiff ON tblCalcTechnicalDriverAssumptionZiff_1.IdModel = tblCalcBaseCostByFieldZiff.IdModel AND 
			tblCalcTechnicalDriverAssumptionZiff_1.IdField = tblCalcBaseCostByFieldZiff.IdField AND tblCalcTechnicalDriverAssumptionZiff_1.IdZiffAccount = tblCalcBaseCostByFieldZiff.IdZiffAccount
		WHERE tblCalcTechnicalDriverAssumptionZiff_1.Operation=1 AND tblCalcTechnicalDriverAssumptionZiff_1.ProjectionCriteria=5
		GROUP BY tblCalcTechnicalDriverAssumptionZiff_1.IdModel, tblCalcTechnicalDriverAssumptionZiff_1.IdZiffAccount, tblCalcTechnicalDriverAssumptionZiff_1.IdField, tblCalcTechnicalDriverAssumptionZiff_1.IdProject, 
			tblCalcTechnicalDriverAssumptionZiff_1.Year
		) AS BCRSVZ ON tblCalcTechnicalDriverAssumptionZiff.IdModel = BCRSVZ.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = BCRSVZ.IdZiffAccount AND tblCalcTechnicalDriverAssumptionZiff.IdField = BCRSVZ.IdField AND 
		tblCalcTechnicalDriverAssumptionZiff.IdProject = BCRSVZ.IdProject AND tblCalcTechnicalDriverAssumptionZiff.Year = BCRSVZ.Year
	WHERE tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;

	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		tblCalcTechnicalDriverAssumptionZiff.TFNormal=1,
		tblCalcTechnicalDriverAssumptionZiff.TFPessimistic=1,
		tblCalcTechnicalDriverAssumptionZiff.TFOptimistic=1,
		tblCalcTechnicalDriverAssumptionZiff.BCRNormal=BCRSVZ.BCRNormal,
		tblCalcTechnicalDriverAssumptionZiff.BCRPessimistic=BCRSVZ.BCRPessimistic,
		tblCalcTechnicalDriverAssumptionZiff.BCROptimistic=BCRSVZ.BCROptimistic
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(TotalCostNormal) AS BCRNormal, SUM(TotalCostPessimistic) AS BCRPessimistic, SUM(TotalCostOptimistic) AS BCROptimistic, IdPeerGroup
		FROM tblCalcBaseCostByFieldZiff AS CBCBFZ
		GROUP BY IdModel, IdField, IdZiffAccount, IdPeerGroup
		) AS BCRSVZ ON tblCalcTechnicalDriverAssumptionZiff.IdModel = BCRSVZ.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = BCRSVZ.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = BCRSVZ.IdField AND 
		tblCalcTechnicalDriverAssumptionZiff.IdPeerGroup = BCRSVZ.IdPeerGroup
	WHERE tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel AND
		  tblCalcTechnicalDriverAssumptionZiff.Operation=1  AND tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5;

	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		tblCalcTechnicalDriverAssumptionZiff.TFNormal=1,
		tblCalcTechnicalDriverAssumptionZiff.TFPessimistic=1,
		tblCalcTechnicalDriverAssumptionZiff.TFOptimistic=1
	FROM tblCalcTechnicalDriverAssumptionZiff
	WHERE tblCalcTechnicalDriverAssumptionZiff.Operation=1 AND tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;

	/** CLEAN UP tblCalcTechnicalDriverAssumption table **/
	DELETE tblCalcTechnicalDriverAssumption WHERE IdModel=@IdModel AND SizeNormal IS NULL AND SizeOptimistic IS NULL AND SizePessimistic IS NULL AND ProjectionCriteria <> 1

	/** CLEAN UP tblCalcTechnicalDriverAssumptionZiff table **/
	DELETE tblCalcTechnicalDriverAssumptionZiff WHERE IdModel=@IdModel AND SizeNormal IS NULL AND SizeOptimistic IS NULL and SizePessimistic IS NULL AND ProjectionCriteria <> 1

	--------------------------------------------------------------------------------------
	-- RUN FORECASTS
	--------------------------------------------------------------------------------------
	
	/** Technical Forecast **/
	EXEC calcModuleTechnicalForecast @IdModel,@intMinYear,@intMaxYear;

	/** Cleanup Calc Tables as Data is not needed anywhere else **/
	DELETE FROM tblCalcModelMultiBaseline WHERE IdModel=@IdModel;
	DELETE FROM tblCalcUnitCostDenominatorSumAmount WHERE IdModel=@IdModel;
	--DELETE FROM tblCalcUnitCostDenominatorAmount WHERE IdModel=@IdModel;
	--DELETE FROM tblCalcBaselineUnitCostDenominatorSumAmount WHERE IdModel=@IdModel;
	DELETE FROM tblCalcCostStructureAssumptionsByField WHERE [IdModel]=@IdModel;
	--DELETE FROM tblCalcTechnicalDriverData WHERE [IdModel]=@IdModel;
	--DELETE FROM tblCalcTechnicalAssumption WHERE [IdModel]=@IdModel;
	--DELETE FROM tblCalcTechnicalDriverAssumption WHERE [IdModel]=@IdModel;
	--DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE [IdModel]=@IdModel;

END

GO

/****** Object:  StoredProcedure [dbo].[allpUpdateZiffBaseCostQuantity]    Script Date: 03/29/2016 14:12:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 2016-02-02	RM		Quantities and Total Costs were added for each technical scenario as
--						it is needed with the introduction of peer groups.					
-- ===========================================================================================
ALTER PROCEDURE [dbo].[allpUpdateZiffBaseCostQuantity](
@IdModel int)
AS
BEGIN

	--UPDATE tblZiffBaseCost 
	--SET tblZiffBaseCost.Quantity = tblPeerCriteriaCost.Quantity, 
	--	tblZiffBaseCost.TotalCost = tblPeerCriteriaCost.Quantity * tblZiffBaseCost.UnitCost
	--FROM tblZiffBaseCost INNER JOIN tblPeerCriteriaCost 
	--	ON tblZiffBaseCost.IdPeerCriteria = tblPeerCriteriaCost.IdPeerCriteria AND tblZiffBaseCost.IdField = tblPeerCriteriaCost.IdField
 --   WHERE tblZiffBaseCost.IdField IN (SELECT FF.IdField FROM tblFields AS FF WHERE FF.IdModel=@IdModel);

	--UPDATE tblZiffBaseCost 
	--SET tblZiffBaseCost.Quantity = tblPeerCriteriaCost.Quantity, 
	--	tblZiffBaseCost.TotalCost = tblPeerCriteriaCost.Quantity * tblZiffBaseCost.UnitCost
	--FROM tblZiffBaseCost INNER JOIN tblPeerCriteriaCost 
	--	ON tblZiffBaseCost.IdPeerCriteria = tblPeerCriteriaCost.IdPeerCriteria --AND tblZiffBaseCost.IdField = tblPeerCriteriaCost.IdField
 --   WHERE tblZiffBaseCost.IdField IN (SELECT tblPeerGroupField.IdField
	--									FROM tblPeerCriteriaCost INNER JOIN
	--										 tblPeerGroup ON tblPeerCriteriaCost.IdPeerGroup = tblPeerGroup.IdPeerGroup INNER JOIN
	--										 tblPeerGroupField ON tblPeerGroup.IdPeerGroup = tblPeerGroupField.IdPeerGroup
	--								  GROUP BY tblPeerGroupField.IdField)
	UPDATE tblZiffBaseCost
	SET tblZiffBaseCost.QuantityNormal = CSABP.Normal,
		tblZiffBaseCost.TotalCostNormal = CSABP.Normal * tblZiffBaseCost.UnitCost,
		tblZiffBaseCost.QuantityPessimistic = CSABP.Pessimistic,
		tblZiffBaseCost.TotalCostPessimistic = CSABP.Pessimistic * tblZiffBaseCost.UnitCost,
		tblZiffBaseCost.QuantityOptimistic = CSABP.Optimistic,
		tblZiffBaseCost.TotalCostOptimistic = CSABP.Optimistic * tblZiffBaseCost.UnitCost
	FROM tblZiffBaseCost INNER JOIN
		(
			SELECT tblProjects.IdModel,tblProjects.IdField,tblCostStructureAssumptionsByProject.IdZiffAccount,tblCostStructureAssumptionsByProject.IdPeerGroup,
			CalcBaseCostByFieldSummaryZiff.Normal,CalcBaseCostByFieldSummaryZiff.Pessimistic,CalcBaseCostByFieldSummaryZiff.Optimistic
			FROM tblCostStructureAssumptionsByProject LEFT OUTER JOIN
			tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject LEFT OUTER JOIN
			tblZiffAccounts ON tblCostStructureAssumptionsByProject.IdZiffAccount = tblZiffAccounts.IdZiffAccount LEFT OUTER JOIN
			tblZiffAccounts AS tblZiffAccountsParent ON tblZiffAccounts.Parent = tblZiffAccountsParent.IdZiffAccount INNER JOIN
			tblPeerGroup ON tblCostStructureAssumptionsByProject.IdPeerGroup = tblPeerGroup.IdPeerGroup INNER JOIN
			(
			 SELECT tblCalcTechnicalDriverData.IdModel,tblCalcTechnicalDriverData.IdField,tblCalcTechnicalDriverData.Year,
					SUM(tblCalcTechnicalDriverData.Normal) AS Normal,
					SUM(tblCalcTechnicalDriverData.Pessimistic) AS Pessimistic,
					SUM(tblCalcTechnicalDriverData.Optimistic) AS Optimistic
			 FROM tblCalcTechnicalDriverData INNER JOIN
					tblTechnicalDrivers ON tblCalcTechnicalDriverData.IdTechnicalDriver = tblTechnicalDrivers.IdTechnicalDriver
			 WHERE tblTechnicalDrivers.UnitCostDenominator=1 
			 GROUP BY tblCalcTechnicalDriverData.IdModel,tblCalcTechnicalDriverData.IdField,tblCalcTechnicalDriverData.Year
			) AS CalcBaseCostByFieldSummaryZiff ON tblProjects.IdModel = CalcBaseCostByFieldSummaryZiff.IdModel AND
			tblProjects.IdField = CalcBaseCostByFieldSummaryZiff.IdField AND
			tblCostStructureAssumptionsByProject.Ziff = CalcBaseCostByFieldSummaryZiff.Year
			GROUP BY tblProjects.IdModel,tblProjects.IdField,tblCostStructureAssumptionsByProject.IdZiffAccount,tblCostStructureAssumptionsByProject.IdPeerGroup,
			tblCostStructureAssumptionsByProject.Client,tblCostStructureAssumptionsByProject.Ziff,CalcBaseCostByFieldSummaryZiff.Normal,
			CalcBaseCostByFieldSummaryZiff.Pessimistic,CalcBaseCostByFieldSummaryZiff.Optimistic
		) AS CSABP ON tblZiffBaseCost.IdField = CSABP.IdField AND
			tblZiffBaseCost.IdPeerGroup = CSABP.IdPeerGroup AND
			tblZiffBaseCost.IdZiffAccount = CSABP.IdZiffAccount
END

GO

/****** Object:  StoredProcedure [dbo].[usrpListProfileRoles]    Script Date: 06/20/2016 14:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usrpListProfileRoles] 
    @IdModel INT, 
    @IdProfile INT,
    @IdLanguage INT
AS 
BEGIN

    SET NOCOUNT ON;
    
	SELECT 0 AS IdProfileRoles
		, 0 IdModel
		, 0 IdProfile
		, tblRoles.IdRole
		, CASE WHEN SubSortOrder IS NULL THEN N'' ELSE N'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' END + CASE WHEN SortOrder IS NULL THEN N'' ELSE CAST(SortOrder AS nvarchar) END + CASE WHEN SubSortOrder IS NULL THEN N'' ELSE N'.' + CAST(SubSortOrder AS nvarchar) END + N' - ' + tblRolesLanguages.RoleText AS RoleName
		, 0 AS DenyAccess
		, 0 AS GrantAccess
		, SortOrder
		, SubSortOrder
	FROM tblRoles INNER JOIN
		tblRolesLanguages ON tblRoles.IdRole = tblRolesLanguages.IdRole
	WHERE tblRoles.IdRole NOT IN
	(
	SELECT tblRoles.IdRole
	FROM tblProfilesRoles 
		INNER JOIN tblProfiles ON tblProfilesRoles.IdProfile = tblProfiles.IdProfile 
		INNER JOIN tblRoles ON tblProfilesRoles.IdRole = tblRoles.IdRole
	WHERE tblProfilesRoles.IdModel = @IdModel AND tblProfilesRoles.IdProfile = @IdProfile
	) AND tblRolesLanguages.IdLanguage = @IdLanguage
	AND tblRoles.Enabled=1
	UNION ALL
	SELECT tblProfilesRoles.IdProfileRoles
		, tblProfilesRoles.IdModel	
		, tblProfiles.IdProfile
		, tblRoles.IdRole
		, CASE WHEN SubSortOrder IS NULL THEN N'' ELSE N'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' END + CASE WHEN SortOrder IS NULL THEN N'' ELSE CAST(SortOrder AS nvarchar) END + CASE WHEN SubSortOrder IS NULL THEN N'' ELSE N'.' + CAST(SubSortOrder AS nvarchar) END + N' - ' + tblRolesLanguages.RoleText AS RoleName
		, ISNULL(tblProfilesRoles.DenyAccess,0) AS DenyAccess
		, ISNULL(tblProfilesRoles.GrantAccess,0) AS GrantAccess
		, SortOrder
		, SubSortOrder
	FROM tblProfilesRoles INNER JOIN
		tblProfiles ON tblProfilesRoles.IdProfile = tblProfiles.IdProfile INNER JOIN
		tblRoles ON tblProfilesRoles.IdRole = tblRoles.IdRole INNER JOIN
		tblRolesLanguages ON tblRoles.IdRole = tblRolesLanguages.IdRole
	WHERE tblProfilesRoles.IdModel = @IdModel AND tblProfilesRoles.IdProfile = @IdProfile AND tblRolesLanguages.IdLanguage = @IdLanguage AND tblRoles.Enabled=1
	ORDER BY SortOrder, SubSortOrder
	
END
GO

/****** Object:  StoredProcedure [dbo].[usrpListProfilePermissions]    Script Date: 06/21/2016 15:12:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gareth Slater
-- Create date: 2014-04-04
-- Description:	Get list of applicable Permissions
-- =============================================
ALTER PROC [dbo].[usrpListProfilePermissions](
@IdModel int,
@IdProfile int)
AS
BEGIN
	
	--SELECT * FROM (SELECT tblProfilesRoles.*, ROW_NUMBER() OVER (PARTITION BY [IdProfile],[IdRole] ORDER BY [IdModel] DESC) AS RN
	--FROM tblProfilesRoles WHERE ([IdModel]=0 OR [IdModel]=@IdModel) AND [IdProfile]=@IdProfile AND GrantAccess=1 AND DenyAccess=0) AS RolesSelect
	--WHERE RN = 1

	DECLARE @NumPermissions INT = 0

	SELECT @NumPermissions = ISNULL(COUNT(*),0)
	FROM (SELECT tblProfilesRoles.*, ROW_NUMBER() OVER (PARTITION BY [IdProfile],[IdRole] ORDER BY [IdModel] DESC) AS RN
		  FROM tblProfilesRoles 
		  WHERE ([IdModel]=@IdModel) AND [IdProfile]=@IdProfile AND GrantAccess=1 AND DenyAccess=0) AS RolesSelect
	WHERE RN = 1
	
	IF (@NumPermissions = 0)
		SET @IdModel = 0

	IF (@IdModel=0)
		SELECT * 
		FROM (SELECT tblProfilesRoles.*, ROW_NUMBER() OVER (PARTITION BY [IdProfile],[IdRole] ORDER BY [IdModel] DESC) AS RN
			  FROM tblProfilesRoles 
			  WHERE ([IdModel]=0) AND [IdProfile]=@IdProfile AND GrantAccess=1 AND DenyAccess=0) AS RolesSelect
		WHERE RN = 1
	ELSE	
		SELECT * 
		FROM (SELECT tblProfilesRoles.*, ROW_NUMBER() OVER (PARTITION BY [IdProfile],[IdRole] ORDER BY [IdModel] DESC) AS RN
			  FROM tblProfilesRoles 
			  WHERE ([IdModel]=@IdModel) AND [IdProfile]=@IdProfile AND GrantAccess=1 AND DenyAccess=0) AS RolesSelect
		WHERE RN = 1
	
END

/****** Object:  StoredProcedure [dbo].[parpBaseRatesByDriversTechnicalModule]    Script Date: 07/19/2016 13:33:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-03-19
-- Description:	Base Rates By Drivers Report
-- =============================================
ALTER PROCEDURE [dbo].[parpBaseRatesByDriversTechnicalModule](
@IdModel INT,
@IdAggregationLevel INT,
@IdField INT,
@IdCurrency INT,
@IdStage INT,
@BaseCostType INT,
@IdLanguage INT
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#tblCalcTechnicalBaseCostRate_Temp') IS NOT NULL DROP TABLE #tblCalcTechnicalBaseCostRate_Temp
	IF OBJECT_ID('tempdb..#FieldList') IS NOT NULL DROP TABLE #FieldList
	IF OBJECT_ID('tempdb..#ProjectionCosts') IS NOT NULL DROP TABLE #ProjectionCosts
	IF OBJECT_ID('tempdb..#SizeDriverList') IS NOT NULL DROP TABLE #SizeDriverList
	IF OBJECT_ID('tempdb..#PerformanceDriverList') IS NOT NULL DROP TABLE #PerformanceDriverList
	IF OBJECT_ID('tempdb..#BaseCostRatePerDriverList') IS NOT NULL DROP TABLE #BaseCostRatePerDriverList

	/** Build Field List **/
	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL, Name NVARCHAR(255)) ON [PRIMARY]
	IF (@IdAggregationLevel>0 AND @IdField>0)
	BEGIN
		INSERT INTO #FieldList SELECT A.[IdField], B.Name FROM tblCalcTechnicalBaseCostRate A INNER JOIN tblFields B ON A.IdField=B.IdField WHERE A.[IdModel]=@IdModel AND A.IdField = @IdField GROUP BY A.[IdField], B.Name; 
	END
	ELSE IF (@IdAggregationLevel>0 AND @IdField=0)
	BEGIN
		INSERT INTO #FieldList SELECT A.[IdField], B.Name FROM tblCalcTechnicalBaseCostRate A INNER JOIN tblFields B ON A.IdField=B.IdField WHERE A.[IdModel]=@IdModel  AND A.IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=@IdAggregationLevel) GROUP BY A.[IdField], B.Name; 
	END
	ELSE IF (@IdAggregationLevel=0 AND @IdField>0) 
	BEGIN
		INSERT INTO #FieldList SELECT A.[IdField], B.Name FROM tblCalcTechnicalBaseCostRate A INNER JOIN tblFields B ON A.IdField=B.IdField WHERE A.[IdModel]=@IdModel  AND A.IdField = @IdField GROUP BY A.[IdField], B.Name; 
	END
	ELSE IF (@IdAggregationLevel=0 AND @IdField=0)
	BEGIN
		INSERT INTO #FieldList SELECT A.[IdField], B.Name FROM tblCalcTechnicalBaseCostRate A INNER JOIN tblFields B ON A.IdField=B.IdField WHERE A.[IdModel]=@IdModel GROUP BY A.[IdField], B.Name;
	END

	/** Build Field Column **/
	DECLARE @FieldColumnList NVARCHAR(MAX)='';
	DECLARE @FieldName NVARCHAR(255);
	DECLARE FieldList CURSOR FOR
	SELECT Name FROM #FieldList;

	OPEN FieldList;
	FETCH NEXT FROM FieldList INTO @FieldName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @FieldColumnList=''
		BEGIN
			SET @FieldColumnList =  @FieldColumnList + '[' + @FieldName  + ']';
		END
		ELSE
		BEGIN
			SET @FieldColumnList =  @FieldColumnList + ',[' + @FieldName  + ']';
		END
		FETCH NEXT FROM FieldList INTO @FieldName;
	END

	CLOSE FieldList;
	DEALLOCATE FieldList;

	/** Get Currency Factor to use **/
	DECLARE @CurrencyFactor FLOAT;
	SELECT @CurrencyFactor=[Value] FROM tblModelsCurrencies WHERE [IdCurrency]=@IdCurrency AND [IdModel]=@IdModel;

	/** Build Temporary tblCalcTechnicalBaseCostRate table **/
	CREATE TABLE #tblCalcTechnicalBaseCostRate_Temp (IdModel INT, IdZiffAccount INT, CodeZiff VARCHAR(50), ZiffAccount VARCHAR(150), 
													 DisplayCode NVARCHAR(MAX), SortOrder NVARCHAR(MAX), IdField INT, IdStage INT,
													 BaseCostType INT, ProjectionCriteria INT, NameProjectionCriteria VARCHAR(100), 
													 CycleValue INT, SizeName VARCHAR(150), SizeValue FLOAT, PerformanceName VARCHAR(150), 
													 PerformanceValue FLOAT, BaseCost FLOAT, BaseCostRate FLOAT, IdRootZiffAccount INT, 
													 RootCodeZiff NVARCHAR(50), RootZiffAccount NVARCHAR(255))
	INSERT INTO #tblCalcTechnicalBaseCostRate_Temp 
	SELECT	IdModel
			, IdZiffAccount
			, CodeZiff
			, ZiffAccount
			, DisplayCode
			, SortOrder
			, IdField
			, IdStage
			, BaseCostType
			, ProjectionCriteria
			, NameProjectionCriteria
			, CycleValue
			, SizeName
			, SizeValue
			, PerformanceName
			, PerformanceValue
			, BaseCost
			, BaseCostRate
			, IdRootZiffAccount
			, RootCodeZiff
			, RootZiffAccount
	FROM	tblCalcTechnicalBaseCostRate INNER JOIN tblParameters on tblParameters.Name = tblCalcTechnicalBaseCostRate.NameProjectionCriteria
	WHERE	IdModel=@IdModel AND IdStage=@IdStage AND BaseCostType=@BaseCostType
   GROUP BY IdModel
			, IdZiffAccount
			, CodeZiff
			, ZiffAccount
			, DisplayCode
			, SortOrder
			, IdField
			, IdStage
			, BaseCostType
			, ProjectionCriteria
			, NameProjectionCriteria
			, CycleValue
			, SizeName
			, SizeValue
			, PerformanceName
			, PerformanceValue
			, BaseCost
			, BaseCostRate
			, IdRootZiffAccount
			, RootCodeZiff
			, RootZiffAccount
	ORDER BY SortOrder;

	/** Build projection costs list **/
	CREATE TABLE #ProjectionCosts (FieldName NVARCHAR(255), Name NVARCHAR(255), BaseCost FLOAT)
	INSERT INTO #ProjectionCosts 
	--SELECT B.Name, A.NameProjectionCriteria + ' Cost', SUM(A.BaseCost) * @CurrencyFactor AS BaseCost
	--FROM  #tblCalcTechnicalBaseCostRate_Temp A INNER JOIN #FieldList B ON A.IdField = B.IdField
	--WHERE A.BaseCost IS NOT NULL --AND A.PerformanceValue = 0
	--GROUP BY B.Name, A.NameProjectionCriteria, A.SizeName, A.PerformanceName
	SELECT B.Name, CASE WHEN @IdLanguage=1 THEN P.Name + ' Cost' ELSE 'Costo ' + P.Name END, SUM(A.BaseCost) * @CurrencyFactor AS BaseCost
	FROM  #tblCalcTechnicalBaseCostRate_Temp A INNER JOIN #FieldList B 
		  ON A.IdField = B.IdField INNER JOIN tblParameters P
		  ON A.ProjectionCriteria = P.Code
	WHERE A.BaseCost IS NOT NULL AND P.Type = 'ProjectionCriteria' AND P.IdLanguage=@IdLanguage
	GROUP BY B.Name, P.Name, A.SizeName, A.PerformanceName

	/** Build Size Driver List **/
	CREATE TABLE #SizeDriverList (FieldName NVARCHAR(255), Name NVARCHAR(255), SizeValue FLOAT)
	INSERT INTO #SizeDriverList SELECT B.Name, CASE WHEN A.PerformanceName IS NULL THEN A.SizeName ELSE A.SizeName + ' / ' + ISNULL(A.PerformanceName,'') END, CASE WHEN A.PerformanceName IS NULL THEN A.SizeValue ELSE A.PerformanceValue END
	FROM  #tblCalcTechnicalBaseCostRate_Temp A INNER JOIN 
		  #FieldList B ON A.IdField = B.IdField
	where A.SizeName is not null and A.Sizevalue is not null
	GROUP BY /*A.IdField,*/ B.Name, CASE WHEN A.PerformanceName IS NULL THEN A.SizeName ELSE A.SizeName + ' / ' + ISNULL(A.PerformanceName,'') END, CASE WHEN A.PerformanceName IS NULL THEN A.SizeValue ELSE A.PerformanceValue END

	/** Build Performance Driver List **/
	CREATE TABLE #PerformanceDriverList (FieldName NVARCHAR(255), Name NVARCHAR(255), PerformanceValue FLOAT)
	INSERT INTO #PerformanceDriverList SELECT  B.Name, A.PerformanceName, sum(A.PerformanceValue) * @CurrencyFactor
	FROM  #tblCalcTechnicalBaseCostRate_Temp A INNER JOIN 
		  #FieldList B ON A.IdField = B.IdField
	where A.PerformanceName is not null and A.PerformanceValue is not null
	GROUP BY /*A.IdField,*/ B.Name, A.PerformanceName

	/** Build Base Cost Rate per Driver List **/
	CREATE TABLE #BaseCostRatePerDriverList (FieldName NVARCHAR(255), Name NVARCHAR(255), BaseCostRate FLOAT)
	INSERT INTO #BaseCostRatePerDriverList SELECT B.Name, CASE WHEN A.PerformanceName IS NULL THEN A.SizeName ELSE A.SizeName + ' / ' + ISNULL(A.PerformanceName,'') END, sum(A.BaseCostRate) * @CurrencyFactor
	FROM  #tblCalcTechnicalBaseCostRate_Temp A INNER JOIN 
		  #FieldList B ON A.IdField = B.IdField
	where A.SizeName is not null and A.SizeValue is not null
	GROUP BY /*A.IdField,*/ B.Name, CASE WHEN A.PerformanceName IS NULL THEN A.SizeName ELSE A.SizeName + ' / ' + ISNULL(A.PerformanceName,'') END
	
	--/** Build Output Table **/
	DECLARE @SQLProjectionCosts NVARCHAR(MAX);
	SET @SQLProjectionCosts = 'SELECT * FROM #ProjectionCosts PIVOT (Sum(BaseCost) FOR FieldName IN (' + @FieldColumnList + ')) AS PivotTable';
	EXEC(@SQLProjectionCosts);

	DECLARE @SQLSizeDriverList NVARCHAR(MAX);
	SET @SQLSizeDriverList = 'SELECT * FROM #SizeDriverList PIVOT (Sum(SizeValue) FOR FieldName IN (' + @FieldColumnList + ')) AS PivotTable';
	EXEC(@SQLSizeDriverList);

	DECLARE @SQLBaseCostRatePerDriverList NVARCHAR(MAX);
	SET @SQLBaseCostRatePerDriverList = 'SELECT * FROM #BaseCostRatePerDriverList PIVOT (Sum(BaseCostRate) FOR FieldName IN (' + @FieldColumnList + ')) AS PivotTable';
	EXEC(@SQLBaseCostRatePerDriverList);

 END
GO

/****** Object:  StoredProcedure [dbo].[allpUpdateCostStrucAssumptionFieldPeerGroup]    Script Date: 08/098/2016 07:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[allpUpdateCostStrucAssumptionFieldPeerGroup]
@pIdField INT,
@pIdPeerGroup INT
AS
BEGIN
			UPDATE tblCostStructureAssumptionsByProject
			SET	IdPeerGroup = @pIdPeerGroup
			WHERE idproject IN (SELECT IdProject FROM tblProjects WHERE IdField = @pIdField AND Operation = 1)
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================================
-- Author:		Rodrigo Manubens
-- Create date:	2016-08-094
-- Description:	Used for checking if a module is running or not. If it hasn't finished 
--				within 10 minutes of execution it will be time stamped as finished.
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ----------	------	----------------------------------------------------------------------
-- ===========================================================================================
CREATE PROCEDURE [dbo].[usrpCheckProcessRunning] (
	@IdModel INT
)
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/** is the process still running after 10 minutes of it's execution start **/
	DECLARE @IdUserLogRunning INT 
	SELECT @IdUserLogRunning = ISNULL(MAX(IdUserLog),0) FROM tblUserLog WHERE DATEADD(MINUTE,10,LogDate) < GETDATE() AND Note = 'calcModuleTechnical' AND IdModel = @IdModel AND LogDateFinished IS NULL GROUP BY LogDate

	IF @IdUserLogRunning > 0 -- Finish the process
		UPDATE tblUserLog SET LogDateFinished = GETUTCDATE() WHERE IdUserLog = @IdUserLogRunning
		
	/** is the process still active **/
	SELECT ISNULL(MAX(IdUserLog),0) AS 'IdUserLog' FROM tblUserLog WHERE Note = 'calcModuleTechnical' AND IdModel = @IdModel and LogDateFinished IS NULL
	
END
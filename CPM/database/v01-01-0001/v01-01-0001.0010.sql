USE [DBCPM]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblCalcModelMultiBaseline](
	[IdModel] [int] NOT NULL,
	[IdField] [int] NOT NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[tblCalcUnitCostDenominatorAmount](
	[IdModel] [int] NOT NULL,
	[IdField] [int] NOT NULL,
	[IdProject] [int] NOT NULL,
	[Year] [int] NOT NULL,
	[SumNormal] [float] NULL,
	[SumPessimistic] [float] NULL,
	[SumOptimistic] [float] NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[tblCalcUnitCostDenominatorSumAmount](
	[IdModel] [int] NOT NULL,
	[IdField] [int] NOT NULL,
	[Year] [int] NOT NULL,
	[SumNormal] [float] NULL,
	[SumPessimistic] [float] NULL,
	[SumOptimistic] [float] NULL
) ON [PRIMARY]

GO

-- =============================================
-- Author:		Gareth Slater
-- Create date: 2014-04-03
-- Description:	Module Technical Calc
-- =============================================
ALTER PROCEDURE [dbo].[calcModuleTechnical](
@IdModel int
)
AS
BEGIN
	
	DECLARE @intMinYear INT;
	DECLARE @intMaxYear INT;
	SELECT @intMinYear=BaseYear, @intMaxYear=BaseYear+Projection FROM tblModels WHERE [IdModel]=@IdModel;
	
	DECLARE @intLoop int = 0;
	DECLARE @intMaxLevel int = 0;
	SELECT @intMaxLevel = MAX([RelationLevel])+1 FROM tblZiffAccounts WHERE [IdModel]=@IdModel;
	
	/** Precalc **/
	EXEC calcModuleTechnicalPreCalc @IdModel;
	
	/** Cleanup Calc Tables for New Data **/
	DELETE FROM tblCalcTechnicalDriverData WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalAssumption WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalDriverAssumption WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE [IdModel]=@IdModel;
	
	/** Create Base for Technical Driver Data **/
	INSERT INTO tblCalcTechnicalDriverData ([IdModel],[IdTechnicalDriver],[IdField],[IdProject],[IdTypeOperation],[Year],[Normal],[Pessimistic],[Optimistic])
	SELECT tblTechnicalDriverData.IdModel, tblTechnicalDriverData.IdTechnicalDriver, tblTechnicalDriverData.IdField, tblTechnicalDriverData.IdProject, 
		tblFields.IdTypeOperation, tblTechnicalDriverData.Year, tblTechnicalDriverData.Normal, tblTechnicalDriverData.Pessimistic, tblTechnicalDriverData.Optimistic
	FROM tblTechnicalDriverData INNER JOIN
		tblFields ON tblTechnicalDriverData.IdField = tblFields.IdField AND tblTechnicalDriverData.IdModel = tblFields.IdModel
	WHERE tblTechnicalDriverData.IdModel=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- SETUP HIERARCHY OF ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Set Hierarchy of Technical Assumptions (Specific TypeOperation) **/
	SET @intLoop = @intMaxLevel;
	WHILE @intLoop > 0 BEGIN
		SET @intLoop = @intLoop - 1
		
		INSERT INTO tblCalcTechnicalAssumption ([IdModel],[IdZiffAccount],[IdTypeOperation],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria])
		SELECT tblTechnicalAssumption.IdModel, tblTechnicalAssumption.IdZiffAccount, tblTechnicalAssumption.IdTypeOperation, 
			tblTechnicalAssumption.IdTechnicalDriverSize, tblTechnicalAssumption.IdTechnicalDriverPerformance, 
			tblTechnicalAssumption.CycleValue, tblTechnicalAssumption.ProjectionCriteria
		FROM tblTechnicalAssumption INNER JOIN
			tblZiffAccounts ON tblTechnicalAssumption.IdZiffAccount = tblZiffAccounts.IdZiffAccount AND 
			tblTechnicalAssumption.IdModel = tblZiffAccounts.IdModel
		WHERE tblTechnicalAssumption.IdTypeOperation<>0 AND tblZiffAccounts.RelationLevel=@intLoop AND tblTechnicalAssumption.IdModel=@IdModel;
	END
	
	SET @intLoop = @intMaxLevel;
	WHILE @intLoop > 0 BEGIN
		SET @intLoop = @intLoop - 1
		
		INSERT INTO tblCalcTechnicalAssumption ([IdModel],[IdZiffAccount],[IdTypeOperation],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria])
		SELECT tblTechnicalAssumption.IdModel, tblZiffAccounts.IdZiffAccount, tblTechnicalAssumption.IdTypeOperation, 
			tblTechnicalAssumption.IdTechnicalDriverSize, tblTechnicalAssumption.IdTechnicalDriverPerformance, 
			tblTechnicalAssumption.CycleValue, tblTechnicalAssumption.ProjectionCriteria
		FROM tblTechnicalAssumption INNER JOIN
			tblCalcZiffAccountParentChild ON tblTechnicalAssumption.IdZiffAccount = tblCalcZiffAccountParentChild.IdParent AND 
			tblTechnicalAssumption.IdModel = tblCalcZiffAccountParentChild.IdModel INNER JOIN
			tblZiffAccounts ON tblCalcZiffAccountParentChild.IdChild = tblZiffAccounts.IdZiffAccount AND 
			tblCalcZiffAccountParentChild.IdModel = tblZiffAccounts.IdModel INNER JOIN
			tblZiffAccounts AS tblZiffAccountsParent ON tblTechnicalAssumption.IdZiffAccount = tblZiffAccountsParent.IdZiffAccount
		WHERE tblTechnicalAssumption.IdTypeOperation<>0 AND tblZiffAccountsParent.RelationLevel=@intLoop AND tblTechnicalAssumption.IdModel=@IdModel AND tblZiffAccounts.IdZiffAccount NOT IN (SELECT CTA.IdZiffAccount FROM tblCalcTechnicalAssumption AS CTA WHERE CTA.IdTypeOperation=tblTechnicalAssumption.IdTypeOperation AND CTA.IdModel=@IdModel);
	END
	
	/** Set Hierarchy of Technical Assumptions (General/All TypeOperation) **/
	SET @intLoop = @intMaxLevel;
	WHILE @intLoop > 0 BEGIN
		SET @intLoop = @intLoop - 1
		
		INSERT INTO tblCalcTechnicalAssumption ([IdModel],[IdZiffAccount],[IdTypeOperation],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria])
		SELECT tblTechnicalAssumption.IdModel, tblTechnicalAssumption.IdZiffAccount, TypeOpList.IdTypeOperation, tblTechnicalAssumption.IdTechnicalDriverSize, 
			tblTechnicalAssumption.IdTechnicalDriverPerformance, 
			tblTechnicalAssumption.CycleValue, tblTechnicalAssumption.ProjectionCriteria
		FROM tblTechnicalAssumption INNER JOIN
			tblZiffAccounts ON tblTechnicalAssumption.IdZiffAccount = tblZiffAccounts.IdZiffAccount AND 
			tblTechnicalAssumption.IdModel = tblZiffAccounts.IdModel CROSS JOIN
			  (SELECT IdTypeOperation FROM tblFields WHERE (IdModel=@IdModel) GROUP BY IdTypeOperation) AS TypeOpList
		WHERE tblTechnicalAssumption.IdTypeOperation=0 AND tblZiffAccounts.RelationLevel=@intLoop AND tblTechnicalAssumption.IdModel=@IdModel AND tblZiffAccounts.IdZiffAccount NOT IN (SELECT IdZiffAccount FROM tblCalcTechnicalAssumption AS CTA WHERE IdTypeOperation=TypeOpList.IdTypeOperation AND IdModel=@IdModel);
	END
	
	SET @intLoop = @intMaxLevel;
	WHILE @intLoop > 0 BEGIN
		SET @intLoop = @intLoop - 1
		
		INSERT INTO tblCalcTechnicalAssumption ([IdModel],[IdZiffAccount],[IdTypeOperation],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria])
		SELECT tblTechnicalAssumption.IdModel, tblZiffAccounts.IdZiffAccount, TypeOpList.IdTypeOperation, tblTechnicalAssumption.IdTechnicalDriverSize, 
			tblTechnicalAssumption.IdTechnicalDriverPerformance, 
			tblTechnicalAssumption.CycleValue, tblTechnicalAssumption.ProjectionCriteria
		FROM tblTechnicalAssumption INNER JOIN
			tblCalcZiffAccountParentChild ON tblTechnicalAssumption.IdZiffAccount = tblCalcZiffAccountParentChild.IdParent AND 
			tblTechnicalAssumption.IdModel = tblCalcZiffAccountParentChild.IdModel INNER JOIN
			tblZiffAccounts ON tblCalcZiffAccountParentChild.IdChild = tblZiffAccounts.IdZiffAccount AND 
			tblCalcZiffAccountParentChild.IdModel = tblZiffAccounts.IdModel INNER JOIN
			tblZiffAccounts AS tblZiffAccountsParent ON tblTechnicalAssumption.IdZiffAccount = tblZiffAccountsParent.IdZiffAccount CROSS JOIN
			  (SELECT IdTypeOperation FROM tblFields WHERE (IdModel=@IdModel) GROUP BY IdTypeOperation) AS TypeOpList
		WHERE tblTechnicalAssumption.IdTypeOperation=0 AND tblZiffAccountsParent.RelationLevel=@intLoop AND tblTechnicalAssumption.IdModel=@IdModel AND tblZiffAccounts.IdZiffAccount NOT IN (SELECT IdZiffAccount FROM tblCalcTechnicalAssumption AS CTA WHERE IdTypeOperation=TypeOpList.IdTypeOperation AND IdModel=@IdModel);
	END
	
	/** Remove all that have been marked as N/A **/
	DELETE FROM tblCalcTechnicalAssumption WHERE [ProjectionCriteria]=98 AND [IdModel]=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- SETUP COMPANY BASE AND FORECAST ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Create Company BaseYear Technical Driver Assumption Lookup **/
	DECLARE @YearBase INT;
	DECLARE YearList CURSOR FOR
	SELECT tblCostStructureAssumptionsByProject.Client AS Year
	FROM tblCostStructureAssumptionsByProject INNER JOIN tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject
	WHERE tblProjects.IdModel=@IdModel
	GROUP BY tblCostStructureAssumptionsByProject.Client
	HAVING tblCostStructureAssumptionsByProject.Client > 0
	ORDER BY Year;

	OPEN YearList;
	FETCH NEXT FROM YearList INTO @YearBase;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		INSERT INTO tblCalcTechnicalDriverAssumption ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff])
		SELECT 1, tblCalcTechnicalAssumption.IdModel, tblCalcTechnicalAssumption.IdZiffAccount, tblCalcTechnicalAssumption.IdTypeOperation, 
			tblFields.IdField, tblProjects.IdProject, @YearBase + CASE WHEN tblCalcTechnicalAssumption.ProjectionCriteria=4 AND tblCostStructureAssumptionsByProject.Client<>@intMinYear THEN tblCalcTechnicalAssumption.CycleValue-1 ELSE 0 END AS [Year] , 
			tblProjects.Operation, CAST(1 AS BIT) AS [BaseYear], tblCalcTechnicalAssumption.IdTechnicalDriverSize, tblCalcTechnicalAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalAssumption.CycleValue, 
			tblCalcTechnicalAssumption.ProjectionCriteria, tblCostStructureAssumptionsByProject.Client, tblCostStructureAssumptionsByProject.Ziff
		FROM tblCalcTechnicalAssumption LEFT OUTER JOIN
			tblProjects LEFT OUTER JOIN
			tblFields ON tblFields.IdField = tblProjects.IdField RIGHT OUTER JOIN
			tblCostStructureAssumptionsByProject ON tblProjects.IdProject = tblCostStructureAssumptionsByProject.IdProject ON 
			tblCostStructureAssumptionsByProject.IdZiffAccount = tblCalcTechnicalAssumption.IdZiffAccount AND 
			tblFields.IdTypeOperation = tblCalcTechnicalAssumption.IdTypeOperation
		WHERE tblCostStructureAssumptionsByProject.Client=@YearBase AND tblCalcTechnicalAssumption.IdModel=@IdModel;

		FETCH NEXT FROM YearList INTO @YearBase;
	END

	CLOSE YearList;
	DEALLOCATE YearList;
	
	/** Update Company BaseYear Driver Values **/
	EXEC calcModuleTechnicalDriverUpdate @IdModel,1,0;
	
	/** Update Company BaseCost Value for BaseYear **/
	UPDATE tblCalcBaseCostByField SET
		[TechApplicable]=1,
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN [SizeNormal] IS NULL THEN 0 ELSE [SizeNormal]*(CASE WHEN ISNULL([PerformanceNormal],0)=0 THEN 1 ELSE [PerformanceNormal] END) END ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN [SizePessimistic] IS NULL THEN 0 ELSE [SizePessimistic]*(CASE WHEN ISNULL([PerformancePessimistic],0)=0 THEN 1 ELSE [PerformancePessimistic] END) END ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN [SizeOptimistic] IS NULL THEN 0 ELSE [SizeOptimistic]*(CASE WHEN ISNULL([PerformanceOptimistic],0)=0 THEN 1 ELSE [PerformanceOptimistic] END) END ELSE 0 END
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField
	WHERE tblCalcTechnicalDriverAssumption.BaseYear=1 AND tblCalcTechnicalDriverAssumption.Operation=1 AND tblCalcBaseCostByField.IdModel=@IdModel;
	
	UPDATE tblCalcBaseCostByField SET
		[BCRNormal] = CASE WHEN ISNULL([TFNormal],0)=0 THEN 0 ELSE [Amount]/[TFNormal] END,
		[BCRPessimistic] = CASE WHEN ISNULL([TFPessimistic],0)=0 THEN 0 ELSE [Amount]/[TFPessimistic] END,
		[BCROptimistic] = CASE WHEN ISNULL([TFOptimistic],0)=0 THEN 0 ELSE [Amount]/[TFOptimistic] END
	WHERE TechApplicable=1 AND IdModel=@IdModel;

	UPDATE tblCalcBaseCostByField SET
		[BCRNormal] = 0,
		[BCRPessimistic] = 0,
		[BCROptimistic] = 0
	WHERE TechApplicable=0 AND IdModel=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- SETUP ZIFF BASE AND FORECAST ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Create Ziff BaseYear Technical Driver Assumption Lookup **/
	DECLARE @YearBaseZiff INT;
	DECLARE YearListZiff CURSOR FOR
	SELECT tblCostStructureAssumptionsByProject.Ziff AS Year
	FROM tblCostStructureAssumptionsByProject INNER JOIN tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject
	WHERE tblProjects.IdModel=@IdModel
	GROUP BY tblCostStructureAssumptionsByProject.Ziff
	HAVING tblCostStructureAssumptionsByProject.Ziff > 0
	ORDER BY Year;

	OPEN YearListZiff;
	FETCH NEXT FROM YearListZiff INTO @YearBaseZiff;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		INSERT INTO tblCalcTechnicalDriverAssumptionZiff ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff])
		SELECT 1, tblCalcTechnicalAssumption.IdModel, tblCalcTechnicalAssumption.IdZiffAccount, tblCalcTechnicalAssumption.IdTypeOperation, 
			tblFields.IdField, tblProjects.IdProject, @YearBaseZiff + CASE WHEN tblCalcTechnicalAssumption.ProjectionCriteria=4 AND tblCostStructureAssumptionsByProject.Ziff=0 THEN tblCalcTechnicalAssumption.CycleValue-1 ELSE 0 END AS [Year], 
			tblProjects.Operation, CAST(1 AS BIT) AS [BaseYear], tblCalcTechnicalAssumption.IdTechnicalDriverSize, tblCalcTechnicalAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalAssumption.CycleValue, 
			tblCalcTechnicalAssumption.ProjectionCriteria, tblCostStructureAssumptionsByProject.Client, tblCostStructureAssumptionsByProject.Ziff
		FROM tblCalcTechnicalAssumption LEFT OUTER JOIN
			tblProjects LEFT OUTER JOIN
			tblFields ON tblFields.IdField = tblProjects.IdField RIGHT OUTER JOIN
			tblCostStructureAssumptionsByProject ON tblProjects.IdProject = tblCostStructureAssumptionsByProject.IdProject ON 
			tblCostStructureAssumptionsByProject.IdZiffAccount = tblCalcTechnicalAssumption.IdZiffAccount AND 
			tblFields.IdTypeOperation = tblCalcTechnicalAssumption.IdTypeOperation
		WHERE tblCostStructureAssumptionsByProject.Ziff=@YearBaseZiff AND tblCalcTechnicalAssumption.IdModel=@IdModel;

		FETCH NEXT FROM YearListZiff INTO @YearBaseZiff;
	END

	CLOSE YearListZiff;
	DEALLOCATE YearListZiff;
	
	/** Update Ziff BaseYear Driver Values **/
	EXEC calcModuleTechnicalDriverUpdate @IdModel,2,1;
	
	/** Update Ziff BaseCost Value for BaseYear **/
	UPDATE tblCalcBaseCostByFieldZiff SET
		[TechApplicable]=1,
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN [SizeNormal] IS NULL THEN 0 ELSE [SizeNormal]*(CASE WHEN ISNULL([PerformanceNormal],0)=0 THEN 1 ELSE [PerformanceNormal] END) END ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN [SizePessimistic] IS NULL THEN 0 ELSE [SizePessimistic]*(CASE WHEN ISNULL([PerformancePessimistic],0)=0 THEN 1 ELSE [PerformancePessimistic] END) END ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN [SizeOptimistic] IS NULL THEN 0 ELSE [SizeOptimistic]*(CASE WHEN ISNULL([PerformanceOptimistic],0)=0 THEN 1 ELSE [PerformanceOptimistic] END) END ELSE 0 END
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField
	WHERE tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 AND tblCalcBaseCostByFieldZiff.IdModel=@IdModel;

	UPDATE tblCalcBaseCostByFieldZiff SET
		[BCRNormal] = CASE WHEN ISNULL([TFNormal],0)=0 THEN 0 ELSE [TotalCost]/[TFNormal] END,
		[BCRPessimistic] = CASE WHEN ISNULL([TFPessimistic],0)=0 THEN 0 ELSE [TotalCost]/[TFPessimistic] END,
		[BCROptimistic] = CASE WHEN ISNULL([TFOptimistic],0)=0 THEN 0 ELSE [TotalCost]/[TFOptimistic] END
	WHERE TechApplicable=1 AND IdModel=@IdModel;
	
	UPDATE tblCalcBaseCostByFieldZiff SET
		[BCRNormal] = 0,
		[BCRPessimistic] = 0,
		[BCROptimistic] = 0
	WHERE TechApplicable=0 AND IdModel=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- UPDATE BASE COST RATES BASE YEAR
	--------------------------------------------------------------------------------------
	
	/** Update Company Base Cost Rates **/
	UPDATE tblCalcTechnicalDriverAssumption SET
		[BCRNormal] = CalcBaseCostByFieldSummary.BCRNormal,
		[BCRPessimistic] = CalcBaseCostByFieldSummary.BCRPessimistic,
		[BCROptimistic] = CalcBaseCostByFieldSummary.BCROptimistic
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(BCRNormal) AS BCRNormal, SUM(BCRPessimistic) AS BCRPessimistic, SUM(BCROptimistic) AS BCROptimistic
		FROM tblCalcBaseCostByField AS CBCBF
		GROUP BY IdModel, IdField, IdZiffAccount
		) AS CalcBaseCostByFieldSummary ON 
		tblCalcTechnicalDriverAssumption.IdModel = CalcBaseCostByFieldSummary.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = CalcBaseCostByFieldSummary.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = CalcBaseCostByFieldSummary.IdField
	WHERE tblCalcTechnicalDriverAssumption.BaseYear=1 AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Update Ziff Base Cost Rates **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		[BCRNormal] = CalcBaseCostByFieldSummaryZiff.BCRNormal,
		[BCRPessimistic] = CalcBaseCostByFieldSummaryZiff.BCRPessimistic,
		[BCROptimistic] = CalcBaseCostByFieldSummaryZiff.BCROptimistic
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(BCRNormal) AS BCRNormal, SUM(BCRPessimistic) AS BCRPessimistic, SUM(BCROptimistic) AS BCROptimistic
		FROM tblCalcBaseCostByFieldZiff AS CBCBFZ
		GROUP BY IdModel, IdField, IdZiffAccount
		) AS CalcBaseCostByFieldSummaryZiff ON 
		tblCalcTechnicalDriverAssumptionZiff.IdModel = CalcBaseCostByFieldSummaryZiff.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = CalcBaseCostByFieldSummaryZiff.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = CalcBaseCostByFieldSummaryZiff.IdField
	WHERE tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	/** Drop Ziff BaseYear with no costs **/
	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE [BaseYear]=1 AND [BCRNormal] IS NULL AND [BCRPessimistic] IS NULL AND [BCROptimistic] IS NULL AND [IdModel]=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- SETUP ADDITIONAL YEARS ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Create Company Additional Years Technical Driver Assumption Lookup **/
	INSERT INTO tblCalcTechnicalDriverAssumption ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff])
	SELECT 2, tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, tblCalcTechnicalDriverAssumption.IdTypeOperation, 
		tblCalcTechnicalDriverAssumption.IdField, tblCalcTechnicalDriverAssumption.IdProject, tblCalcModelYears.Year, 
		tblCalcTechnicalDriverAssumption.Operation, CAST(0 AS BIT) AS BaseYear, tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize, 
		tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalDriverAssumption.CycleValue, 
		tblCalcTechnicalDriverAssumption.ProjectionCriteria, tblCalcTechnicalDriverAssumption.YearClient, tblCalcTechnicalDriverAssumption.YearZiff
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		tblCalcModelYears ON tblCalcTechnicalDriverAssumption.IdModel = tblCalcModelYears.IdModel AND 
		tblCalcTechnicalDriverAssumption.Year < tblCalcModelYears.Year		
	WHERE tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Create Ziff Additional Years Technical Driver Assumption Lookup **/
	INSERT INTO tblCalcTechnicalDriverAssumptionZiff ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff])
	SELECT 2, tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdTypeOperation, 
		tblCalcTechnicalDriverAssumptionZiff.IdField, tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcModelYears.Year, 
		tblCalcTechnicalDriverAssumptionZiff.Operation, CAST(0 AS BIT) AS BaseYear, tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverSize, 
		tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverPerformance, tblCalcTechnicalDriverAssumptionZiff.CycleValue, 
		tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.YearClient, tblCalcTechnicalDriverAssumptionZiff.YearZiff
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		tblCalcModelYears ON tblCalcTechnicalDriverAssumptionZiff.IdModel = tblCalcModelYears.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.Year < tblCalcModelYears.Year
	WHERE tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	/** Create Company BusinessOpportunity Technical Driver Assumption Lookup **/
	INSERT INTO tblCalcTechnicalDriverAssumption ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff])
	SELECT 3, tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, tblCalcTechnicalDriverAssumption.IdTypeOperation, 
		tblCalcTechnicalDriverAssumption.IdField, tblProjects.IdProject, tblCalcTechnicalDriverAssumption.Year, tblProjects.Operation, 
		CASE WHEN tblCalcTechnicalDriverAssumption.Year = tblProjects.Starts THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS BaseYear, 
		tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize, tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance,  
		tblCalcTechnicalDriverAssumption.CycleValue, tblCalcTechnicalDriverAssumption.ProjectionCriteria, tblCalcTechnicalDriverAssumption.YearClient, tblCalcTechnicalDriverAssumption.YearZiff
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumption.IdModel = tblProjects.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdField = tblProjects.IdField AND tblCalcTechnicalDriverAssumption.Year >= tblProjects.Starts
	WHERE tblProjects.Operation=2 AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Remove Company BusinessOpportunity Technical Driver Assumption = Fixed **/
	DELETE FROM tblCalcTechnicalDriverAssumption WHERE ([ProjectionCriteria]=1) AND [Operation]=2 AND [IdModel]=@IdModel;
	
	/** Create Ziff BusinessOpportunity Technical Driver Assumption Lookup **/
	INSERT INTO tblCalcTechnicalDriverAssumptionZiff ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff])
	SELECT 3, tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdTypeOperation, 
		tblCalcTechnicalDriverAssumptionZiff.IdField, tblProjects.IdProject, tblCalcTechnicalDriverAssumptionZiff.Year, tblProjects.Operation, 
		CASE WHEN tblCalcTechnicalDriverAssumptionZiff.Year = tblProjects.Starts THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS BaseYear, 
		tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverSize, tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverPerformance, 
		tblCalcTechnicalDriverAssumptionZiff.CycleValue, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.YearClient, tblCalcTechnicalDriverAssumptionZiff.YearZiff
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumptionZiff.IdModel = tblProjects.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = tblProjects.IdField AND tblCalcTechnicalDriverAssumptionZiff.Year >= tblProjects.Starts
	WHERE tblProjects.Operation=2 AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	/** Remove Ziff BusinessOpportunity Technical Driver Assumption = Fixed or Semi-Fixed **/
	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE ([ProjectionCriteria]=1 OR [ProjectionCriteria]=2) AND [Operation]=2 AND [IdModel]=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- CLEAN UP ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Remove Base Year Assumptions not in initial Year for Semi-Fixed **/
	UPDATE tblCalcTechnicalDriverAssumption SET [BaseYear]=0 WHERE [ProjectionCriteria]=2 AND [BaseYear]=1 AND [Year]<>@intMinYear AND [IdModel]=@IdModel;
	
	/** Rebase and Remove Excess Years from all Cyclical Assumptions **/
	EXEC calcModuleTechnicalCyclicalRebase @IdModel,@intMinYear,@intMaxYear;
	
	--------------------------------------------------------------------------------------
	-- ADD CLIENT AND ZIFF COST ASSUMPTION START YEARS TO TABLE
	--------------------------------------------------------------------------------------
	
	/** Update Company Client/Ziff Years Lookup **/
	UPDATE tblCalcTechnicalDriverAssumption
	SET YearClient=CTDALookup.YearClient, YearZiff=CTDALookup.YearZiff
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		(
		SELECT CTDA.IdModel, CTDA.IdZiffAccount, CTDA.IdField, CTDA.YearClient, CTDA.YearZiff
		FROM tblCalcTechnicalDriverAssumption AS CTDA INNER JOIN
			tblProjects AS P ON CTDA.IdField = P.IdField AND CTDA.IdModel = P.IdModel INNER JOIN
			tblCostStructureAssumptionsByProject AS CSABP ON P.IdProject = CSABP.IdProject
		WHERE CTDA.Operation=1 AND CTDA.BaseYear=1
		GROUP BY CTDA.IdModel, CTDA.IdZiffAccount, CTDA.IdField, CTDA.YearClient, CTDA.YearZiff
		) AS CTDALookup ON 
		tblCalcTechnicalDriverAssumption.IdModel = CTDALookup.IdModel AND tblCalcTechnicalDriverAssumption.IdZiffAccount = CTDALookup.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = CTDALookup.IdField
	WHERE tblCalcTechnicalDriverAssumption.YearClient IS NULL AND tblCalcTechnicalDriverAssumption.YearZiff IS NULL AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Remove Company Years Based on Company/Ziff Start Years **/
	DELETE FROM tblCalcTechnicalDriverAssumption WHERE [YearZiff]>0 AND [Year]>=[YearZiff] AND [IdModel]=@IdModel;
	
	/** Update Ziff Client/Ziff Years Lookup **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff
	SET YearClient=CTDAZLookup.YearClient, YearZiff=CTDAZLookup.YearZiff
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		(
		SELECT CTDAZ.IdModel, CTDAZ.IdZiffAccount, CTDAZ.IdField, CTDAZ.YearClient, CTDAZ.YearZiff
		FROM tblCalcTechnicalDriverAssumptionZiff AS CTDAZ INNER JOIN
			tblProjects AS P ON CTDAZ.IdField = P.IdField AND CTDAZ.IdModel = P.IdModel INNER JOIN
			tblCostStructureAssumptionsByProject AS CSABP ON P.IdProject = CSABP.IdProject
		WHERE CTDAZ.Operation=1 AND CTDAZ.BaseYear=1
		GROUP BY CTDAZ.IdModel, CTDAZ.IdZiffAccount, CTDAZ.IdField, CTDAZ.YearClient, CTDAZ.YearZiff
		) AS CTDAZLookup ON 
		tblCalcTechnicalDriverAssumptionZiff.IdModel = CTDAZLookup.IdModel AND tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = CTDAZLookup.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = CTDAZLookup.IdField
	WHERE tblCalcTechnicalDriverAssumptionZiff.YearClient IS NULL AND tblCalcTechnicalDriverAssumptionZiff.YearZiff IS NULL AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	/** Remove Ziff Years Based on Company/Ziff Start Years (SHOULD BE NO RECORDS - THIS IS JUST A DOUBLE CHECK) **/
	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE [YearClient]>0 AND [Year]<[YearZiff] AND [IdModel]=@IdModel;
	
	/** Ensure All Ziff BO Items as properly flagged as based **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET BaseYear=1 WHERE [Operation]=2 AND [BaseYear]=0 AND [Year]=[YearZiff] AND IdModel=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- PREP FOR TECHNICAL BASE COST RATE CALCS
	--------------------------------------------------------------------------------------
	
	/** Update Company TF (Base Year Only for BCR Calc) **/
	UPDATE tblCalcTechnicalDriverAssumption SET 
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeNormal], 0) = 0 THEN 0 ELSE [SizeNormal]*(CASE WHEN ISNULL([PerformanceNormal],0)=0 THEN 1 ELSE [PerformanceNormal] END) END ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizePessimistic], 0) = 0 THEN 0 ELSE [SizePessimistic]*(CASE WHEN ISNULL([PerformancePessimistic],0)=0 THEN 1 ELSE [PerformancePessimistic] END) END ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeOptimistic], 0) = 0 THEN 0 ELSE [SizeOptimistic]*(CASE WHEN ISNULL([PerformanceOptimistic],0)=0 THEN 1 ELSE [PerformanceOptimistic] END) END ELSE 0 END
	WHERE [BaseYear]=1 AND [IdModel]=@IdModel;
	
	/** Update Ziff TF  (Base Year Only for BCR Calc) **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeNormal], 0) = 0 THEN 0 ELSE [SizeNormal]*(CASE WHEN ISNULL([PerformanceNormal],0)=0 THEN 1 ELSE [PerformanceNormal] END) END ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizePessimistic], 0) = 0 THEN 0 ELSE [SizePessimistic]*(CASE WHEN ISNULL([PerformancePessimistic],0)=0 THEN 1 ELSE [PerformancePessimistic] END) END ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeOptimistic], 0) = 0 THEN 0 ELSE [SizeOptimistic]*(CASE WHEN ISNULL([PerformanceOptimistic],0)=0 THEN 1 ELSE [PerformanceOptimistic] END) END ELSE 0 END
	WHERE [BaseYear]=1 AND [IdModel]=@IdModel;
	
	/** Technical Base Cost Rate **/
	EXEC calcModuleTechnicalBaseCostRate @IdModel;
	
	--------------------------------------------------------------------------------------
	-- POST TECHNICAL BASE COST RATE RESET
	--------------------------------------------------------------------------------------
	
	/** Update All Years Driver Values **/
	EXEC calcModuleTechnicalDriverUpdate @IdModel,1,0;
	EXEC calcModuleTechnicalDriverUpdate @IdModel,2,0;
		
	/** Update Company TF **/
	UPDATE tblCalcTechnicalDriverAssumption SET 
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeNormal], 0) = 0 THEN 0 ELSE [SizeNormal]*(CASE WHEN ISNULL([PerformanceNormal],0)=0 THEN 1 ELSE [PerformanceNormal] END) END ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizePessimistic], 0) = 0 THEN 0 ELSE [SizePessimistic]*(CASE WHEN ISNULL([PerformancePessimistic],0)=0 THEN 1 ELSE [PerformancePessimistic] END) END ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeOptimistic], 0) = 0 THEN 0 ELSE [SizeOptimistic]*(CASE WHEN ISNULL([PerformanceOptimistic],0)=0 THEN 1 ELSE [PerformanceOptimistic] END) END ELSE 0 END
	WHERE [IdModel]=@IdModel;
	
	/** Update Ziff TF **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5  THEN CASE WHEN ISNULL([SizeNormal], 0) = 0 THEN 0 ELSE [SizeNormal]*(CASE WHEN ISNULL([PerformanceNormal],0)=0 THEN 1 ELSE [PerformanceNormal] END) END ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizePessimistic], 0) = 0 THEN 0 ELSE [SizePessimistic]*(CASE WHEN ISNULL([PerformancePessimistic],0)=0 THEN 1 ELSE [PerformancePessimistic] END) END ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeOptimistic], 0) = 0 THEN 0 ELSE [SizeOptimistic]*(CASE WHEN ISNULL([PerformanceOptimistic],0)=0 THEN 1 ELSE [PerformanceOptimistic] END) END ELSE 0 END
	WHERE [IdModel]=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- UPDATE BASE COST RATES ALL YEARS
	--------------------------------------------------------------------------------------
	
	/** Update Company Base Cost Rates **/
	UPDATE tblCalcTechnicalDriverAssumption SET
		[BCRNormal] = CalcBaseCostByFieldSummary.BCRNormal,
		[BCRPessimistic] = CalcBaseCostByFieldSummary.BCRPessimistic,
		[BCROptimistic] = CalcBaseCostByFieldSummary.BCROptimistic
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(BCRNormal) AS BCRNormal, SUM(BCRPessimistic) AS BCRPessimistic, SUM(BCROptimistic) AS BCROptimistic
		FROM tblCalcBaseCostByField AS CBCBF
		GROUP BY IdModel, IdField, IdZiffAccount
		) AS CalcBaseCostByFieldSummary ON 
		tblCalcTechnicalDriverAssumption.IdModel = CalcBaseCostByFieldSummary.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = CalcBaseCostByFieldSummary.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = CalcBaseCostByFieldSummary.IdField
	WHERE tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Update Ziff Base Cost Rates **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		[BCRNormal] = CalcBaseCostByFieldSummaryZiff.BCRNormal,
		[BCRPessimistic] = CalcBaseCostByFieldSummaryZiff.BCRPessimistic,
		[BCROptimistic] = CalcBaseCostByFieldSummaryZiff.BCROptimistic
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(BCRNormal) AS BCRNormal, SUM(BCRPessimistic) AS BCRPessimistic, SUM(BCROptimistic) AS BCROptimistic
		FROM tblCalcBaseCostByFieldZiff AS CBCBFZ
		GROUP BY IdModel, IdField, IdZiffAccount
		) AS CalcBaseCostByFieldSummaryZiff ON 
		tblCalcTechnicalDriverAssumptionZiff.IdModel = CalcBaseCostByFieldSummaryZiff.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = CalcBaseCostByFieldSummaryZiff.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = CalcBaseCostByFieldSummaryZiff.IdField
	WHERE tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- RUN FORECASTS
	--------------------------------------------------------------------------------------
	
	/** Technical Forecast **/
	EXEC calcModuleTechnicalForecast @IdModel,@intMinYear,@intMaxYear;
	
END

GO

-- =============================================
-- Author:		Gareth Slater
-- Create date: 2014-04-20
-- Description:	Module Technical Forecast
-- =============================================
ALTER PROCEDURE [dbo].[calcModuleTechnicalBaseCostRate](
@IdModel int
)
AS
BEGIN
	
	/** Set Variables **/
	DECLARE @BaseYear int = 0;
	SELECT @BaseYear=BaseYear FROM tblModels WHERE IdModel=@IdModel;
	
	/** Cleanup Calc Tables for New Data **/
	DELETE FROM tblCalcTechnicalBaseCostRateSelections WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalBaseCostRate WHERE [IdModel]=@IdModel;
	
	/** Add All Possible Combinations for Report Table **/
	INSERT INTO tblCalcTechnicalBaseCostRateSelections ([IdModel],[IdZiffAccount],[CodeZiff],[ZiffAccount],[DisplayCode],[SortOrder],[IdField],[IdStage],[BaseCostType],[ProjectionCriteria],[NameProjectionCriteria],[BaseCost],[BaseCostRate])
	SELECT vListZiffAccounts.IdModel, vListZiffAccounts.IdZiffAccount, vListZiffAccounts.Code AS CodeZiff, vListZiffAccounts.Name AS ZiffAccount, 
		vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder, tblFields.IdField, tblStage.IdStage, BCLookup.Code AS BaseCostType, 
		98 AS ProjectionCriteria, 'N/A' AS NameProjectionCriteria, NULL AS BaseCost, NULL AS BaseCostRate
	FROM vListZiffAccounts INNER JOIN
		tblModels ON vListZiffAccounts.IdModel = tblModels.IdModel INNER JOIN
		tblFields ON tblModels.IdModel = tblFields.IdModel CROSS JOIN
		tblStage CROSS JOIN
		(SELECT Code FROM tblParameters WHERE Type = N'BaseCost' AND IdLanguage=1) AS BCLookup
	WHERE vListZiffAccounts.Parent IS NOT NULL AND vListZiffAccounts.IdModel=@IdModel;
	
	/** Company Normal BaseCostRate **/
	INSERT INTO tblCalcTechnicalBaseCostRate ([IdModel],[IdZiffAccount],[CodeZiff],[ZiffAccount],[DisplayCode],[SortOrder],[IdField],[IdStage],[BaseCostType],[ProjectionCriteria],[NameProjectionCriteria],[CycleValue],[SizeName],[SizeValue],[PerformanceName],[PerformanceValue],[BaseCost],[BaseCostRate])
	SELECT tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, vListZiffAccounts.Code AS CodeZiff, vListZiffAccounts.Name AS ZiffAccount, vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder,
		tblCalcTechnicalDriverAssumption.IdField, 1 AS IdStage, 1 AS BaseCostType,
		tblCalcTechnicalDriverAssumption.ProjectionCriteria, PCLookup.Name AS NameProjectionCriteria, tblCalcTechnicalDriverAssumption.CycleValue, TDS.Name AS SizeName,
		SUM(tblCalcTechnicalDriverAssumption.SizeNormal) AS SizeValue, TDP.Name AS PerformanceName, 
		SUM(tblCalcTechnicalDriverAssumption.PerformanceNormal) AS PerformanceValue, 
		SUM(tblCalcTechnicalDriverAssumption.TFNormal * tblCalcTechnicalDriverAssumption.BCRNormal) AS BaseCost, 
		AVG(tblCalcTechnicalDriverAssumption.BCRNormal) AS BaseCostRate
	FROM tblCalcTechnicalDriverAssumption LEFT OUTER JOIN
		vListZiffAccounts ON tblCalcTechnicalDriverAssumption.IdZiffAccount = vListZiffAccounts.IdZiffAccount LEFT OUTER JOIN
		(
		SELECT IdParameters, Type, Code, Name, IdLanguage
		FROM tblParameters
		WHERE Type = N'ProjectionCriteria'
		) AS PCLookup ON tblCalcTechnicalDriverAssumption.ProjectionCriteria = PCLookup.Code LEFT OUTER JOIN
		tblTechnicalDrivers AS TDP ON tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance = TDP.IdTechnicalDriver LEFT OUTER JOIN
		tblTechnicalDrivers AS TDS ON tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize = TDS.IdTechnicalDriver
	WHERE tblCalcTechnicalDriverAssumption.Operation=1 AND tblCalcTechnicalDriverAssumption.BaseYear=1 AND PCLookup.IdLanguage=1
	GROUP BY tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, vListZiffAccounts.Code, vListZiffAccounts.Name, 
		vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder, tblCalcTechnicalDriverAssumption.IdField, 
		tblCalcTechnicalDriverAssumption.ProjectionCriteria, PCLookup.Name, tblCalcTechnicalDriverAssumption.CycleValue, TDS.Name, TDP.Name
	HAVING tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Company Optimistic BaseCostRate **/
	INSERT INTO tblCalcTechnicalBaseCostRate ([IdModel],[IdZiffAccount],[CodeZiff],[ZiffAccount],[DisplayCode],[SortOrder],[IdField],[IdStage],[BaseCostType],[ProjectionCriteria],[NameProjectionCriteria],[CycleValue],[SizeName],[SizeValue],[PerformanceName],[PerformanceValue],[BaseCost],[BaseCostRate])
	SELECT tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, vListZiffAccounts.Code AS CodeZiff, vListZiffAccounts.Name AS ZiffAccount, vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder,
		tblCalcTechnicalDriverAssumption.IdField, 2 AS IdStage, 1 AS BaseCostType,
		tblCalcTechnicalDriverAssumption.ProjectionCriteria, PCLookup.Name AS NameProjectionCriteria, tblCalcTechnicalDriverAssumption.CycleValue, TDS.Name AS SizeName,
		SUM(tblCalcTechnicalDriverAssumption.SizeOptimistic) AS SizeValue, TDP.Name AS PerformanceName, 
		SUM(tblCalcTechnicalDriverAssumption.PerformanceOptimistic) AS PerformanceValue, 
		SUM(tblCalcTechnicalDriverAssumption.TFOptimistic * tblCalcTechnicalDriverAssumption.BCROptimistic) AS BaseCost, 
		AVG(tblCalcTechnicalDriverAssumption.BCROptimistic) AS BaseCostRate
	FROM tblCalcTechnicalDriverAssumption LEFT OUTER JOIN
		vListZiffAccounts ON tblCalcTechnicalDriverAssumption.IdZiffAccount = vListZiffAccounts.IdZiffAccount LEFT OUTER JOIN
		(
		SELECT IdParameters, Type, Code, Name, IdLanguage
		FROM tblParameters
		WHERE Type = N'ProjectionCriteria'
		) AS PCLookup ON tblCalcTechnicalDriverAssumption.ProjectionCriteria = PCLookup.Code LEFT OUTER JOIN
		tblTechnicalDrivers AS TDP ON tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance = TDP.IdTechnicalDriver LEFT OUTER JOIN
		tblTechnicalDrivers AS TDS ON tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize = TDS.IdTechnicalDriver
	WHERE tblCalcTechnicalDriverAssumption.Operation=1 AND tblCalcTechnicalDriverAssumption.BaseYear=1 AND PCLookup.IdLanguage=1
	GROUP BY tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, vListZiffAccounts.Code, vListZiffAccounts.Name, 
		vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder, tblCalcTechnicalDriverAssumption.IdField, 
		tblCalcTechnicalDriverAssumption.ProjectionCriteria, PCLookup.Name, tblCalcTechnicalDriverAssumption.CycleValue, TDS.Name, TDP.Name
	HAVING tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Company Pessimistic BaseCostRate **/
	INSERT INTO tblCalcTechnicalBaseCostRate ([IdModel],[IdZiffAccount],[CodeZiff],[ZiffAccount],[DisplayCode],[SortOrder],[IdField],[IdStage],[BaseCostType],[ProjectionCriteria],[NameProjectionCriteria],[CycleValue],[SizeName],[SizeValue],[PerformanceName],[PerformanceValue],[BaseCost],[BaseCostRate])
	SELECT tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, vListZiffAccounts.Code AS CodeZiff, vListZiffAccounts.Name AS ZiffAccount, vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder,
		tblCalcTechnicalDriverAssumption.IdField, 3 AS IdStage, 1 AS BaseCostType,
		tblCalcTechnicalDriverAssumption.ProjectionCriteria, PCLookup.Name AS NameProjectionCriteria, tblCalcTechnicalDriverAssumption.CycleValue, TDS.Name AS SizeName,
		SUM(tblCalcTechnicalDriverAssumption.SizePessimistic) AS SizeValue, TDP.Name AS PerformanceName, 
		SUM(tblCalcTechnicalDriverAssumption.PerformancePessimistic) AS PerformanceValue, 
		SUM(tblCalcTechnicalDriverAssumption.TFPessimistic * tblCalcTechnicalDriverAssumption.BCRPessimistic) AS BaseCost, 
		AVG(tblCalcTechnicalDriverAssumption.BCRPessimistic) AS BaseCostRate
	FROM tblCalcTechnicalDriverAssumption LEFT OUTER JOIN
		vListZiffAccounts ON tblCalcTechnicalDriverAssumption.IdZiffAccount = vListZiffAccounts.IdZiffAccount LEFT OUTER JOIN
		(
		SELECT IdParameters, Type, Code, Name, IdLanguage
		FROM tblParameters
		WHERE Type = N'ProjectionCriteria'
		) AS PCLookup ON tblCalcTechnicalDriverAssumption.ProjectionCriteria = PCLookup.Code LEFT OUTER JOIN
		tblTechnicalDrivers AS TDP ON tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance = TDP.IdTechnicalDriver LEFT OUTER JOIN
		tblTechnicalDrivers AS TDS ON tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize = TDS.IdTechnicalDriver
	WHERE tblCalcTechnicalDriverAssumption.Operation=1 AND tblCalcTechnicalDriverAssumption.BaseYear=1 AND PCLookup.IdLanguage=1
	GROUP BY tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, vListZiffAccounts.Code, vListZiffAccounts.Name, 
		vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder, tblCalcTechnicalDriverAssumption.IdField, 
		tblCalcTechnicalDriverAssumption.ProjectionCriteria, PCLookup.Name, tblCalcTechnicalDriverAssumption.CycleValue, TDS.Name, TDP.Name
	HAVING tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Ziff Normal BaseCostRate **/
	INSERT INTO tblCalcTechnicalBaseCostRate ([IdModel],[IdZiffAccount],[CodeZiff],[ZiffAccount],[DisplayCode],[SortOrder],[IdField],[IdStage],[BaseCostType],[ProjectionCriteria],[NameProjectionCriteria],[CycleValue],[SizeName],[SizeValue],[PerformanceName],[PerformanceValue],[BaseCost],[BaseCostRate])
	SELECT tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, vListZiffAccounts.Code AS CodeZiff, vListZiffAccounts.Name AS ZiffAccount, vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder, 
		tblCalcTechnicalDriverAssumptionZiff.IdField, 1 AS IdStage, 2 AS BaseCostType, 
		tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, PCLookup.Name AS NameProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.CycleValue, TDS.Name AS SizeName, 
		SUM(tblCalcTechnicalDriverAssumptionZiff.SizeNormal) AS SizeValue, TDP.Name AS PerformanceName, 
		SUM(tblCalcTechnicalDriverAssumptionZiff.PerformanceNormal) AS PerformanceValue, SUM(tblCalcTechnicalDriverAssumptionZiff.TFNormal * tblCalcTechnicalDriverAssumptionZiff.BCRNormal) AS BaseCost, 
		AVG(tblCalcTechnicalDriverAssumptionZiff.BCRNormal) AS BaseCostRate
	FROM tblCalcTechnicalDriverAssumptionZiff LEFT OUTER JOIN
		vListZiffAccounts ON tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = vListZiffAccounts.IdZiffAccount LEFT OUTER JOIN
		(
		SELECT IdParameters, Type, Code, Name, IdLanguage
		FROM tblParameters
		WHERE Type = N'ProjectionCriteria'
		) AS PCLookup ON tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria = PCLookup.Code LEFT OUTER JOIN
		tblTechnicalDrivers AS TDP ON tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverPerformance = TDP.IdTechnicalDriver LEFT OUTER JOIN
		tblTechnicalDrivers AS TDS ON tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverSize = TDS.IdTechnicalDriver
	WHERE tblCalcTechnicalDriverAssumptionZiff.Operation=1 AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 AND PCLookup.IdLanguage=1
	GROUP BY tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, vListZiffAccounts.Code, vListZiffAccounts.Name, 
		vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder, tblCalcTechnicalDriverAssumptionZiff.IdField, 
		tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, PCLookup.Name, tblCalcTechnicalDriverAssumptionZiff.CycleValue, TDS.Name, TDP.Name
	HAVING tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	/** Ziff Optimistic BaseCostRate **/
	INSERT INTO tblCalcTechnicalBaseCostRate ([IdModel],[IdZiffAccount],[CodeZiff],[ZiffAccount],[DisplayCode],[SortOrder],[IdField],[IdStage],[BaseCostType],[ProjectionCriteria],[NameProjectionCriteria],[CycleValue],[SizeName],[SizeValue],[PerformanceName],[PerformanceValue],[BaseCost],[BaseCostRate])
	SELECT tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, vListZiffAccounts.Code AS CodeZiff, vListZiffAccounts.Name AS ZiffAccount, vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder, 
		tblCalcTechnicalDriverAssumptionZiff.IdField, 2 AS IdStage, 2 AS BaseCostType, 
		tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, PCLookup.Name AS NameProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.CycleValue, TDS.Name AS SizeName, 
		SUM(tblCalcTechnicalDriverAssumptionZiff.SizeOptimistic) AS SizeValue, TDP.Name AS PerformanceName, 
		SUM(tblCalcTechnicalDriverAssumptionZiff.PerformanceOptimistic) AS PerformanceValue, SUM(tblCalcTechnicalDriverAssumptionZiff.TFOptimistic * tblCalcTechnicalDriverAssumptionZiff.BCROptimistic) AS BaseCost, 
		AVG(tblCalcTechnicalDriverAssumptionZiff.BCROptimistic) AS BaseCostRate
	FROM tblCalcTechnicalDriverAssumptionZiff LEFT OUTER JOIN
		vListZiffAccounts ON tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = vListZiffAccounts.IdZiffAccount LEFT OUTER JOIN
		(
		SELECT IdParameters, Type, Code, Name, IdLanguage
		FROM tblParameters
		WHERE Type = N'ProjectionCriteria'
		) AS PCLookup ON tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria = PCLookup.Code LEFT OUTER JOIN
		tblTechnicalDrivers AS TDP ON tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverPerformance = TDP.IdTechnicalDriver LEFT OUTER JOIN
		tblTechnicalDrivers AS TDS ON tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverSize = TDS.IdTechnicalDriver
	WHERE tblCalcTechnicalDriverAssumptionZiff.Operation=1 AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 AND PCLookup.IdLanguage=1
	GROUP BY tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, vListZiffAccounts.Code, vListZiffAccounts.Name, 
		vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder, tblCalcTechnicalDriverAssumptionZiff.IdField, 
		tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, PCLookup.Name, tblCalcTechnicalDriverAssumptionZiff.CycleValue, TDS.Name, TDP.Name
	HAVING tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	/** Ziff Pessimistic BaseCostRate **/
	INSERT INTO tblCalcTechnicalBaseCostRate ([IdModel],[IdZiffAccount],[CodeZiff],[ZiffAccount],[DisplayCode],[SortOrder],[IdField],[IdStage],[BaseCostType],[ProjectionCriteria],[NameProjectionCriteria],[CycleValue],[SizeName],[SizeValue],[PerformanceName],[PerformanceValue],[BaseCost],[BaseCostRate])
	SELECT tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, vListZiffAccounts.Code AS CodeZiff, vListZiffAccounts.Name AS ZiffAccount, vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder, 
		tblCalcTechnicalDriverAssumptionZiff.IdField, 3 AS IdStage, 2 AS BaseCostType, 
		tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, PCLookup.Name AS NameProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.CycleValue, TDS.Name AS SizeName, 
		SUM(tblCalcTechnicalDriverAssumptionZiff.SizePessimistic) AS SizeValue, TDP.Name AS PerformanceName, 
		SUM(tblCalcTechnicalDriverAssumptionZiff.PerformancePessimistic) AS PerformanceValue, SUM(tblCalcTechnicalDriverAssumptionZiff.TFPessimistic * tblCalcTechnicalDriverAssumptionZiff.BCRPessimistic) AS BaseCost, 
		AVG(tblCalcTechnicalDriverAssumptionZiff.BCRPessimistic) AS BaseCostRate
	FROM tblCalcTechnicalDriverAssumptionZiff LEFT OUTER JOIN
		vListZiffAccounts ON tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = vListZiffAccounts.IdZiffAccount LEFT OUTER JOIN
		(
		SELECT IdParameters, Type, Code, Name, IdLanguage
		FROM tblParameters
		WHERE Type = N'ProjectionCriteria'
		) AS PCLookup ON tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria = PCLookup.Code LEFT OUTER JOIN
		tblTechnicalDrivers AS TDP ON tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverPerformance = TDP.IdTechnicalDriver LEFT OUTER JOIN
		tblTechnicalDrivers AS TDS ON tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverSize = TDS.IdTechnicalDriver
	WHERE tblCalcTechnicalDriverAssumptionZiff.Operation=1 AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 AND PCLookup.IdLanguage=1
	GROUP BY tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, vListZiffAccounts.Code, vListZiffAccounts.Name, 
		vListZiffAccounts.DisplayCode, vListZiffAccounts.SortOrder, tblCalcTechnicalDriverAssumptionZiff.IdField, 
		tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, PCLookup.Name, tblCalcTechnicalDriverAssumptionZiff.CycleValue, TDS.Name, TDP.Name
	HAVING tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	/** Update Missing Values **/
	UPDATE tblCalcTechnicalBaseCostRate SET [BaseCost]=0 WHERE [BaseCost] IS NULL AND [IdModel]=@IdModel;
	UPDATE tblCalcTechnicalBaseCostRate SET [BaseCostRate]=0 WHERE [BaseCostRate] IS NULL AND [IdModel]=@IdModel;
	
	/** Update Fixed and SemiFixed **/
	UPDATE tblCalcTechnicalBaseCostRate SET [BaseCostRate]=[BaseCost] WHERE [ProjectionCriteria]=1 AND [IdModel]=@IdModel;
	UPDATE tblCalcTechnicalBaseCostRate SET [BaseCostRate]=NULL WHERE [ProjectionCriteria]=2 AND [IdModel]=@IdModel;
	
	/** Add any missing codes for the Report **/
	INSERT INTO tblCalcTechnicalBaseCostRate ([IdModel],[IdZiffAccount],[CodeZiff],[ZiffAccount],[DisplayCode],[SortOrder],[IdField],[IdStage],[BaseCostType],[ProjectionCriteria],[NameProjectionCriteria],[BaseCost],[BaseCostRate])
	SELECT tblCalcTechnicalBaseCostRateSelections.IdModel, tblCalcTechnicalBaseCostRateSelections.IdZiffAccount, 
		tblCalcTechnicalBaseCostRateSelections.CodeZiff, tblCalcTechnicalBaseCostRateSelections.ZiffAccount, 
		tblCalcTechnicalBaseCostRateSelections.DisplayCode, tblCalcTechnicalBaseCostRateSelections.SortOrder, 
		tblCalcTechnicalBaseCostRateSelections.IdField, tblCalcTechnicalBaseCostRateSelections.IdStage, 
		tblCalcTechnicalBaseCostRateSelections.BaseCostType, tblCalcTechnicalBaseCostRateSelections.ProjectionCriteria, 
		tblCalcTechnicalBaseCostRateSelections.NameProjectionCriteria, tblCalcTechnicalBaseCostRateSelections.BaseCost, 
		tblCalcTechnicalBaseCostRateSelections.BaseCostRate
	FROM tblCalcTechnicalBaseCostRateSelections LEFT OUTER JOIN
		tblCalcTechnicalBaseCostRate ON tblCalcTechnicalBaseCostRateSelections.IdModel = tblCalcTechnicalBaseCostRate.IdModel AND 
		tblCalcTechnicalBaseCostRateSelections.IdZiffAccount = tblCalcTechnicalBaseCostRate.IdZiffAccount AND 
		tblCalcTechnicalBaseCostRateSelections.IdField = tblCalcTechnicalBaseCostRate.IdField AND 
		tblCalcTechnicalBaseCostRateSelections.IdStage = tblCalcTechnicalBaseCostRate.IdStage AND 
		tblCalcTechnicalBaseCostRateSelections.BaseCostType = tblCalcTechnicalBaseCostRate.BaseCostType
	WHERE tblCalcTechnicalBaseCostRate.IdModel IS NULL AND tblCalcTechnicalBaseCostRateSelections.IdModel=@IdModel;
	
	/** Set RootParent Columns **/
	UPDATE tblCalcTechnicalBaseCostRate
	SET [IdRootZiffAccount]=tblZiffAccountsRoot.IdZiffAccount, [RootCodeZiff]=tblZiffAccountsRoot.Code, [RootZiffAccount]=tblZiffAccountsRoot.Name
	FROM tblCalcTechnicalBaseCostRate LEFT OUTER JOIN tblZiffAccounts ON tblCalcTechnicalBaseCostRate.IdModel = tblZiffAccounts.IdModel AND tblCalcTechnicalBaseCostRate.IdZiffAccount = tblZiffAccounts.IdZiffAccount LEFT OUTER JOIN tblZiffAccounts AS tblZiffAccountsRoot ON tblZiffAccounts.RootParent = tblZiffAccountsRoot.IdZiffAccount
	WHERE tblCalcTechnicalBaseCostRate.[IdModel]=@IdModel;
		
END

GO
USE [DBCPM]
GO
/****** Object:  StoredProcedure [dbo].[parpBaseCostTechnicalModule]    Script Date: 09/10/2015 11:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gareth Slater
-- Create date: 2014-04-22
-- Description:	Technical Base Cost Rates Lookup
-- =============================================
ALTER PROCEDURE [dbo].[parpBaseCostTechnicalModule](
@IdModel int,
@IdField int,
@IdStage int,
@BaseCostType int,
@IdCurrency int,
@IdLanguage int
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	--Original Code (R Manubens 2015/02/23)
	--SELECT * FROM tblCalcTechnicalBaseCostRate
	--WHERE IdModel=@IdModel AND IdField=@IdField AND IdStage=@IdStage AND BaseCostType=@BaseCostType AND ProjectionCriteria<>98
	--ORDER BY SortOrder;
	
	--New Code to include Currency Conversion(R Manubens 2015/02/23)

	/** DROP Temporary Tables **/
	IF OBJECT_ID('tempdb..#tblCalcTechnicalBaseCostRate') IS NOT NULL DROP TABLE #tblCalcTechnicalBaseCostRate
	IF OBJECT_ID('tempdb..#tblParameters') IS NOT NULL DROP TABLE #tblParameters

	/** Get Currency Factor to use **/
	DECLARE @CurrencyFactor FLOAT;
	SELECT @CurrencyFactor=[Value] FROM tblModelsCurrencies WHERE [IdCurrency]=@IdCurrency AND [IdModel]=@IdModel;
	
	CREATE TABLE #tblCalcTechnicalBaseCostRate ([IdModel] INT, [IdZiffAccount] INT, [CodeZiff] VARCHAR(25), [ZiffAccount] VARCHAR(100), [DisplayCode] VARCHAR(255), 
												[SortOrder] VARCHAR(100), [IdField] INT, [IdStage] INT, [BaseCostType] INT, [ProjectionCriteria] INT, 
												[NameProjectionCriteria] VARCHAR(25), CycleValue INT, [SizeName] VARCHAR(100), [SizeValue] INT, [PerformanceName] VARCHAR(100),
												[PerformanceValue] INT, [BaseCost] FLOAT, [BaseCostRate] FLOAT, [IdRootZiffAccount] INT, [RootCodeZiff] VARCHAR(20),
												[RootZiffAccount] VARCHAR(250)) ON [PRIMARY]
	INSERT INTO #tblCalcTechnicalBaseCostRate ([IdModel], [IdZiffAccount], [CodeZiff], [ZiffAccount], [DisplayCode], [SortOrder], [IdField], [IdStage], [BaseCostType], 
											   [ProjectionCriteria], [NameProjectionCriteria], CycleValue, [SizeName], [SizeValue], [PerformanceName],[PerformanceValue], 
											   [BaseCost], [BaseCostRate], [IdRootZiffAccount], [RootCodeZiff],[RootZiffAccount])
	SELECT	IdModel
			, IdZiffAccount
			, CodeZiff
			, ZiffAccount
			, DisplayCode
			, SortOrder
			, IdField
			, IdStage
			, BaseCostType
			, ProjectionCriteria
			, NameProjectionCriteria
			, CycleValue
			, SizeName
			, "SizeValue" = CASE 
								WHEN NameProjectionCriteria != 'SemiFixed' THEN SUM(SizeValue)
								ELSE NULL
						   END
			, PerformanceName
			, SUM(PerformanceValue) AS PerformanceValue
			, SUM(BaseCost) * @CurrencyFactor  AS BaseCost
			, "BaseCostRate" = CASE
									WHEN NameProjectionCriteria != 'Fixed' THEN (BaseCostRate) * @CurrencyFactor
									ELSE NULL
							  END
			, IdRootZiffAccount
			, RootCodeZiff
			, RootZiffAccount
	FROM	tblCalcTechnicalBaseCostRate 
	WHERE	IdModel=@IdModel AND IdField=@IdField AND IdStage=@IdStage AND BaseCostType=@BaseCostType AND ProjectionCriteria<>98
	GROUP BY IdModel
			, IdZiffAccount
			, CodeZiff
			, ZiffAccount
			, DisplayCode
			, SortOrder
			, IdField
			, IdStage
			, BaseCostType
			, ProjectionCriteria
			, NameProjectionCriteria
			, CycleValue
			, SizeName
			, SizeValue
			, PerformanceName
			, PerformanceValue
			, BaseCost
			, BaseCostRate
			, IdRootZiffAccount
			, RootCodeZiff
			, RootZiffAccount
	ORDER BY SortOrder;
	
	CREATE TABLE #tblParameters ([Code] INT, [Name] VARCHAR(50), [IdLanguage] INT)
	INSERT INTO #tblParameters (Code, Name, IdLanguage)
	SELECT Code, Name, IdLanguage
	FROM   tblParameters
	WHERE (Type = 'ProjectionCriteria')
	
	UPDATE #tblCalcTechnicalBaseCostRate
	SET	   NameProjectionCriteria = #tblParameters.Name
	FROM   #tblCalcTechnicalBaseCostRate INNER JOIN #tblParameters on #tblParameters.Code = (SELECT Code FROM #tblParameters WHERE Name = #tblCalcTechnicalBaseCostRate.NameProjectionCriteria AND IdLanguage = 1)
	WHERE  #tblParameters.IdLanguage=@IdLanguage

	SELECT * FROM #tblCalcTechnicalBaseCostRate ORDER BY SortOrder;
		
END

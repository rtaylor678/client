USE [DBCPM]
GO
/****** Object:  StoredProcedure [dbo].[tecpUtilizationCapacity]    Script Date: 07/10/2015 13:24:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-07-10
-- Description:	List ONLY Technical Assumptions that have Internal or External Costs, all other wil lbe hidden
-- =================================================================================================================
CREATE PROCEDURE [dbo].[tecpListTechnicalAssumption](
@IdModel INT,
@IdField INT,	/* NOT REALLY NEEDED BUT WILL KEEP IN CASE AT SOME POINT THE LIST IS SHOWN ON A PER DIELD BASIS, FOR KNOW PASS IN 0 FOR ALL */
@IdTypeOperation INT
)
AS
BEGIN
	
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#BaseCostReferencesIDs') IS NOT NULL DROP TABLE #BaseCostReferencesIDs

	CREATE TABLE #BaseCostReferencesIDs (IdZiffAccount INT)
	IF @IdField <> 0 
	BEGIN
		INSERT INTO #BaseCostReferencesIDs (IdZiffAccount)
		SELECT CBCBFZ.IdZiffAccount
		FROM tblCalcBaseCostByFieldZiff CBCBFZ left outer join tblZiffAccounts ZA on 
			 ZA.IdModel=CBCBFZ.IdModel and ZA.IdZiffAccount=CBCBFZ.IdZiffAccount 
		WHERE CBCBFZ.IdModel = @IdModel
		AND CBCBFZ.IdField = @IdField
		AND CBCBFZ.TotalCost > 0
		GROUP BY CBCBFZ.IdZiffAccount
	END
	ELSE
	BEGIN
		INSERT INTO #BaseCostReferencesIDs (IdZiffAccount)
		SELECT CBCBFZ.IdZiffAccount
		FROM tblCalcBaseCostByFieldZiff CBCBFZ left outer join tblZiffAccounts ZA on 
			 ZA.IdModel=CBCBFZ.IdModel and ZA.IdZiffAccount=CBCBFZ.IdZiffAccount 
		WHERE CBCBFZ.IdModel = @IdModel
		AND CBCBFZ.IdField IN (SELECT [IdField] FROM [DBCPM].[dbo].[tblFields] WHERE IdModel = @IdModel)
		AND CBCBFZ.TotalCost > 0
		GROUP BY CBCBFZ.IdZiffAccount
	END

	IF @IdField <> 0 
	BEGIN
		INSERT INTO #BaseCostReferencesIDs (IdZiffAccount)
		SELECT IdZiffAccount
		FROM [tblCalcBaseCostByField]
		WHERE IdModel = @IdModel 
		AND IdField = @IdField
		AND IdRootZiffAccount IS NOT NULL
		EXCEPT SELECT IdZiffAccount FROM #BaseCostReferencesIDs
		GROUP BY IdZiffAccount
		ORDER BY IdZiffAccount
	END
	ELSE
	BEGIN
		INSERT INTO #BaseCostReferencesIDs (IdZiffAccount)
		SELECT IdZiffAccount
		FROM [tblCalcBaseCostByField]
		WHERE IdModel = @IdModel 
		AND IdField IN (SELECT [IdField] FROM [DBCPM].[dbo].[tblFields] WHERE IdModel = @IdModel)
		AND IdRootZiffAccount IS NOT NULL
		EXCEPT SELECT IdZiffAccount FROM #BaseCostReferencesIDs
		GROUP BY IdZiffAccount
		ORDER BY IdZiffAccount
	END

	SELECT [IdModel]
		  ,[IdTypeOperation]
		  ,[IdTechnicalAssumption]
		  ,[vListTechnicalAssumption].[IdZiffAccount]
		  ,[DisplayName]
		  ,[CodeZiffAccount]
		  ,[NameZiffAccount]
		  ,[ProjectionCriteria]
		  ,[IdTechnicalDriverSize]
		  ,[IdTechnicalDriverPerformance]
		  ,[CycleValue]
		  ,[NameTechnicalSize]
		  ,[NameTechnicalPerformance]
		  ,[NameProjectionCriteria]
		  ,[SortOrder]
		  ,[UserCreation]
		  ,[DateCreation]
		  ,[UserModification]
		  ,[DateModification]
	  FROM [DBCPM].[dbo].[vListTechnicalAssumption] INNER JOIN #BaseCostReferencesIDs ON
			vListTechnicalAssumption.IdZiffAccount = #BaseCostReferencesIDs.IdZiffAccount
	  WHERE IdModel=@IdModel
	  AND  IdTypeOperation = @IdTypeOperation
	  UNION 
		SELECT IdModel,NULL, NULL, IdZiffAccount,Code + ': ' + Name,Code,Name,NULL,NULL,NULL,NULL,NULL,NULL,NULL,SortOrder,NULL,NULL,NULL,NULL
		FROM tblZiffAccounts 
		WHERE IdModel = @IdModel
		AND Parent IS NULL 
	  order by SortOrder
END
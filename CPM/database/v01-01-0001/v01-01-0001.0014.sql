USE [DBCPM]
GO
/****** Object:  StoredProcedure [dbo].[calcModuleTechnical]    Script Date: 08/24/2015 10:28:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Gareth Slater
-- Create date: 2014-04-03
-- Description:	Module Technical Calc
-- ================================================================================================ 
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 2015-08-04	RM		Added a new code to poulate table tblCalcBaselineUnitCostDenominatorSumAmount
-- 2015-08-15	GS		Updated to work with tblCalcCostStructureAssumptionsByField and Handling Semi-Variable Assumptions
-- ================================================================================================	
ALTER PROCEDURE [dbo].[calcModuleTechnical](
@IdModel int
)
AS
BEGIN
	
	DECLARE @intMinYear INT;
	DECLARE @intMaxYear INT;
	SELECT @intMinYear=BaseYear, @intMaxYear=BaseYear+Projection FROM tblModels WHERE [IdModel]=@IdModel;

	DECLARE @intLoop int = 0;
	DECLARE @intMaxLevel int = 0;
	SELECT @intMaxLevel = MAX([RelationLevel])+1 FROM tblZiffAccounts WHERE [IdModel]=@IdModel;
	
	/** Precalc **/
	EXEC calcModuleTechnicalPreCalc @IdModel;
	
	/** Cleanup Calc Tables for New Data **/
	DELETE FROM tblCalcModelMultiBaseline WHERE IdModel=@IdModel;
	DELETE FROM tblCalcUnitCostDenominatorSumAmount WHERE IdModel=@IdModel;
	DELETE FROM tblCalcUnitCostDenominatorAmount WHERE IdModel=@IdModel;
	DELETE FROM tblCalcBaselineUnitCostDenominatorSumAmount WHERE IdModel=@IdModel;
	DELETE FROM tblCalcCostStructureAssumptionsByField WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalDriverData WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalAssumption WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalDriverAssumption WHERE [IdModel]=@IdModel;
	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE [IdModel]=@IdModel;
	
	/** Base table records - which fields have multiple baselines **/
	INSERT INTO tblCalcModelMultiBaseline ([IdModel],[IdField])
	SELECT tblFields.IdModel, tblFields.IdField
	FROM tblFields INNER JOIN
		tblProjects ON tblFields.IdField = tblProjects.IdField
	GROUP BY tblFields.IdModel, tblFields.IdField, tblProjects.Operation
	HAVING SUM(1)>1 AND tblProjects.Operation=1 AND tblFields.IdModel=@IdModel;

	/** Base table records - Total Value of Denominator Drivers by Field **/
	INSERT INTO tblCalcUnitCostDenominatorSumAmount ([IdModel],[IdField],[Year],[SumNormal],[SumPessimistic],[SumOptimistic])
	SELECT tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year, SUM(tblTechnicalDriverData.Normal) AS SumNormal, SUM(tblTechnicalDriverData.Pessimistic) 
		AS SumPessimistic, SUM(tblTechnicalDriverData.Optimistic) AS SumOptimistic
	FROM tblTechnicalDriverData INNER JOIN
		tblTechnicalDrivers ON tblTechnicalDriverData.IdTechnicalDriver = tblTechnicalDrivers.IdTechnicalDriver AND tblTechnicalDriverData.IdModel = tblTechnicalDrivers.IdModel INNER JOIN
		tblProjects ON tblTechnicalDriverData.IdProject = tblProjects.IdProject
	WHERE tblTechnicalDrivers.UnitCostDenominator=1 AND tblProjects.Operation=1
	GROUP BY tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year
	HAVING tblTechnicalDrivers.IdModel=@IdModel;

	/** Base table records - Total Value of Denominator Drivers by Field/Project **/
	INSERT INTO tblCalcUnitCostDenominatorAmount ([IdModel],[IdField],[IdProject],[Year],[SumNormal],[SumPessimistic],[SumOptimistic])
	SELECT tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.IdProject, tblTechnicalDriverData.Year, SUM(tblTechnicalDriverData.Normal) AS SumNormal, 
		SUM(tblTechnicalDriverData.Pessimistic) AS SumPessimistic, SUM(tblTechnicalDriverData.Optimistic) AS SumOptimistic
	FROM tblTechnicalDriverData INNER JOIN
		tblTechnicalDrivers ON tblTechnicalDriverData.IdTechnicalDriver = tblTechnicalDrivers.IdTechnicalDriver AND tblTechnicalDriverData.IdModel = tblTechnicalDrivers.IdModel INNER JOIN
		tblProjects ON tblTechnicalDriverData.IdProject = tblProjects.IdProject AND tblTechnicalDriverData.IdModel = tblProjects.IdModel
	WHERE tblTechnicalDrivers.UnitCostDenominator=1 AND tblProjects.Operation=1
	GROUP BY tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year, tblTechnicalDriverData.IdProject
	HAVING tblTechnicalDrivers.IdModel=@IdModel;

	/** Base table records - Baseline Unit Cost Denominator **/
	INSERT INTO tblCalcBaselineUnitCostDenominatorSumAmount ([IdModel],[IdField],[Year],[SumNormal],[SumPessimistic],[SumOptimistic])
	SELECT tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year, SUM(tblTechnicalDriverData.Normal) AS SumNormal, SUM(tblTechnicalDriverData.Pessimistic) 
		AS SumPessimistic, SUM(tblTechnicalDriverData.Optimistic) AS SumOptimistic
	FROM tblTechnicalDriverData INNER JOIN
		tblTechnicalDrivers ON tblTechnicalDriverData.IdTechnicalDriver = tblTechnicalDrivers.IdTechnicalDriver AND 
		tblTechnicalDriverData.IdModel = tblTechnicalDrivers.IdModel RIGHT OUTER JOIN
		tblProjects ON tblTechnicalDriverData.IdModel = tblProjects.IdModel AND tblTechnicalDriverData.IdField = tblProjects.IdField AND 
		tblTechnicalDriverData.IdProject = tblProjects.IdProject
	WHERE tblTechnicalDrivers.UnitCostDenominator=1 AND tblProjects.Operation=1
	GROUP BY tblTechnicalDrivers.IdModel, tblTechnicalDriverData.IdField, tblTechnicalDriverData.Year
	HAVING tblTechnicalDrivers.IdModel=@IdModel;

	/** Check if @intMaxYear (projection years) are all required (based on Denominator Totals) **/
	DECLARE @intMaxYearDenominator INT;
	SELECT @intMaxYearDenominator=MAX([Year]) FROM tblCalcUnitCostDenominatorSumAmount WHERE [IdModel]=@IdModel AND ([SumNormal]>0 OR [SumPessimistic]>0 OR [SumOptimistic]>0) GROUP BY [IdModel];

	IF (@intMaxYearDenominator<@intMaxYear)
	BEGIN
		SET @intMaxYear = @intMaxYearDenominator;
	END

	/** Base table records - Baseline Cost Structure Assumptions By Field **/
	INSERT INTO tblCalcCostStructureAssumptionsByField ([IdModel],[IdField],[IdZiffAccount],[Client],[Ziff])
	SELECT tblProjects.IdModel, tblProjects.IdField, tblCostStructureAssumptionsByProject.IdZiffAccount,  MAX(tblCostStructureAssumptionsByProject.Client) AS Client, MAX(tblCostStructureAssumptionsByProject.Ziff) AS Ziff
	FROM tblCostStructureAssumptionsByProject INNER JOIN
		tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject
	GROUP BY tblCostStructureAssumptionsByProject.IdZiffAccount, tblCostStructureAssumptionsByProject.Client, tblCostStructureAssumptionsByProject.Ziff, tblProjects.IdField, tblProjects.IdModel
	HAVING tblProjects.IdModel=@IdModel;

	/** Create Base for Technical Driver Data **/
	INSERT INTO tblCalcTechnicalDriverData ([IdModel],[IdTechnicalDriver],[IdField],[IdProject],[IdTypeOperation],[Year],[Normal],[Pessimistic],[Optimistic])
	SELECT tblTechnicalDriverData.IdModel, tblTechnicalDriverData.IdTechnicalDriver, tblTechnicalDriverData.IdField, CASE WHEN tblProjects.Operation = 1 THEN 0 ELSE tblProjects.IdProject END AS IdProject, 
		tblFields.IdTypeOperation, tblTechnicalDriverData.Year, SUM(tblTechnicalDriverData.Normal) AS Normal, SUM(tblTechnicalDriverData.Pessimistic) AS Pessimistic, 
		SUM(tblTechnicalDriverData.Optimistic) AS Optimistic
	FROM tblTechnicalDriverData INNER JOIN
		tblFields ON tblTechnicalDriverData.IdField = tblFields.IdField AND tblTechnicalDriverData.IdModel = tblFields.IdModel INNER JOIN
		tblProjects ON tblTechnicalDriverData.IdModel = tblProjects.IdModel AND tblTechnicalDriverData.IdProject = tblProjects.IdProject
	GROUP BY tblTechnicalDriverData.IdModel, tblTechnicalDriverData.IdTechnicalDriver, tblTechnicalDriverData.IdField, tblFields.IdTypeOperation, tblTechnicalDriverData.Year, 
		CASE WHEN tblProjects.Operation = 1 THEN 0 ELSE tblProjects.IdProject END
	HAVING tblTechnicalDriverData.IdModel=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- SETUP HIERARCHY OF ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Set Hierarchy of Technical Assumptions (Specific TypeOperation) **/
	SET @intLoop = @intMaxLevel;
	WHILE @intLoop > 0 BEGIN
		SET @intLoop = @intLoop - 1
		
		INSERT INTO tblCalcTechnicalAssumption ([IdModel],[IdZiffAccount],[IdTypeOperation],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria])
		SELECT tblTechnicalAssumption.IdModel, tblTechnicalAssumption.IdZiffAccount, tblTechnicalAssumption.IdTypeOperation, 
			tblTechnicalAssumption.IdTechnicalDriverSize, tblTechnicalAssumption.IdTechnicalDriverPerformance, 
			tblTechnicalAssumption.CycleValue, tblTechnicalAssumption.ProjectionCriteria
		FROM tblTechnicalAssumption INNER JOIN
			tblZiffAccounts ON tblTechnicalAssumption.IdZiffAccount = tblZiffAccounts.IdZiffAccount AND 
			tblTechnicalAssumption.IdModel = tblZiffAccounts.IdModel
		WHERE tblTechnicalAssumption.IdTypeOperation<>0 AND tblZiffAccounts.RelationLevel=@intLoop AND tblTechnicalAssumption.IdModel=@IdModel;
	END
	
	SET @intLoop = @intMaxLevel;
	WHILE @intLoop > 0 BEGIN
		SET @intLoop = @intLoop - 1
		
		INSERT INTO tblCalcTechnicalAssumption ([IdModel],[IdZiffAccount],[IdTypeOperation],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria])
		SELECT tblTechnicalAssumption.IdModel, tblZiffAccounts.IdZiffAccount, tblTechnicalAssumption.IdTypeOperation, 
			tblTechnicalAssumption.IdTechnicalDriverSize, tblTechnicalAssumption.IdTechnicalDriverPerformance, 
			tblTechnicalAssumption.CycleValue, tblTechnicalAssumption.ProjectionCriteria
		FROM tblTechnicalAssumption INNER JOIN
			tblCalcZiffAccountParentChild ON tblTechnicalAssumption.IdZiffAccount = tblCalcZiffAccountParentChild.IdParent AND 
			tblTechnicalAssumption.IdModel = tblCalcZiffAccountParentChild.IdModel INNER JOIN
			tblZiffAccounts ON tblCalcZiffAccountParentChild.IdChild = tblZiffAccounts.IdZiffAccount AND 
			tblCalcZiffAccountParentChild.IdModel = tblZiffAccounts.IdModel INNER JOIN
			tblZiffAccounts AS tblZiffAccountsParent ON tblTechnicalAssumption.IdZiffAccount = tblZiffAccountsParent.IdZiffAccount
		WHERE tblTechnicalAssumption.IdTypeOperation<>0 AND tblZiffAccountsParent.RelationLevel=@intLoop AND tblTechnicalAssumption.IdModel=@IdModel AND tblZiffAccounts.IdZiffAccount NOT IN (SELECT CTA.IdZiffAccount FROM tblCalcTechnicalAssumption AS CTA WHERE CTA.IdTypeOperation=tblTechnicalAssumption.IdTypeOperation AND CTA.IdModel=@IdModel);
	END
	
	/** Set Hierarchy of Technical Assumptions (General/All TypeOperation) **/
	SET @intLoop = @intMaxLevel;
	WHILE @intLoop > 0 BEGIN
		SET @intLoop = @intLoop - 1
		
		INSERT INTO tblCalcTechnicalAssumption ([IdModel],[IdZiffAccount],[IdTypeOperation],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria])
		SELECT tblTechnicalAssumption.IdModel, tblTechnicalAssumption.IdZiffAccount, TypeOpList.IdTypeOperation, tblTechnicalAssumption.IdTechnicalDriverSize, 
			tblTechnicalAssumption.IdTechnicalDriverPerformance, 
			tblTechnicalAssumption.CycleValue, tblTechnicalAssumption.ProjectionCriteria
		FROM tblTechnicalAssumption INNER JOIN
			tblZiffAccounts ON tblTechnicalAssumption.IdZiffAccount = tblZiffAccounts.IdZiffAccount AND 
			tblTechnicalAssumption.IdModel = tblZiffAccounts.IdModel CROSS JOIN
			  (SELECT IdTypeOperation FROM tblFields WHERE (IdModel=@IdModel) GROUP BY IdTypeOperation) AS TypeOpList
		WHERE tblTechnicalAssumption.IdTypeOperation=0 AND tblZiffAccounts.RelationLevel=@intLoop AND tblTechnicalAssumption.IdModel=@IdModel AND tblZiffAccounts.IdZiffAccount NOT IN (SELECT IdZiffAccount FROM tblCalcTechnicalAssumption AS CTA WHERE IdTypeOperation=TypeOpList.IdTypeOperation AND IdModel=@IdModel);
	END
	
	SET @intLoop = @intMaxLevel;
	WHILE @intLoop > 0 BEGIN
		SET @intLoop = @intLoop - 1
		
		INSERT INTO tblCalcTechnicalAssumption ([IdModel],[IdZiffAccount],[IdTypeOperation],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria])
		SELECT tblTechnicalAssumption.IdModel, tblZiffAccounts.IdZiffAccount, TypeOpList.IdTypeOperation, tblTechnicalAssumption.IdTechnicalDriverSize, 
			tblTechnicalAssumption.IdTechnicalDriverPerformance, 
			tblTechnicalAssumption.CycleValue, tblTechnicalAssumption.ProjectionCriteria
		FROM tblTechnicalAssumption INNER JOIN
			tblCalcZiffAccountParentChild ON tblTechnicalAssumption.IdZiffAccount = tblCalcZiffAccountParentChild.IdParent AND 
			tblTechnicalAssumption.IdModel = tblCalcZiffAccountParentChild.IdModel INNER JOIN
			tblZiffAccounts ON tblCalcZiffAccountParentChild.IdChild = tblZiffAccounts.IdZiffAccount AND 
			tblCalcZiffAccountParentChild.IdModel = tblZiffAccounts.IdModel INNER JOIN
			tblZiffAccounts AS tblZiffAccountsParent ON tblTechnicalAssumption.IdZiffAccount = tblZiffAccountsParent.IdZiffAccount CROSS JOIN
			  (SELECT IdTypeOperation FROM tblFields WHERE (IdModel=@IdModel) GROUP BY IdTypeOperation) AS TypeOpList
		WHERE tblTechnicalAssumption.IdTypeOperation=0 AND tblZiffAccountsParent.RelationLevel=@intLoop AND tblTechnicalAssumption.IdModel=@IdModel AND tblZiffAccounts.IdZiffAccount NOT IN (SELECT IdZiffAccount FROM tblCalcTechnicalAssumption AS CTA WHERE IdTypeOperation=TypeOpList.IdTypeOperation AND IdModel=@IdModel);
	END
	
	/** Remove all that have been marked as N/A **/
	DELETE FROM tblCalcTechnicalAssumption WHERE [ProjectionCriteria]=98 AND [IdModel]=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- SETUP COMPANY BASE AND FORECAST ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Create Company BaseYear Technical Driver Assumption Lookup **/
	DECLARE @YearBase INT;
	DECLARE YearList CURSOR FOR
	SELECT Client AS Year
	FROM tblCalcCostStructureAssumptionsByField
	WHERE IdModel=@IdModel
	GROUP BY Client
	HAVING Client > 0
	ORDER BY Year;

	OPEN YearList;
	FETCH NEXT FROM YearList INTO @YearBase;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		INSERT INTO tblCalcTechnicalDriverAssumption ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff])
		SELECT 1, tblCalcTechnicalAssumption.IdModel, tblCalcTechnicalAssumption.IdZiffAccount, tblCalcTechnicalAssumption.IdTypeOperation, tblFields.IdField, 
			CASE WHEN tblProjects.Operation = 1 THEN 0 ELSE tblProjects.IdProject END AS IdProject, @YearBase + CASE WHEN tblCalcTechnicalAssumption.ProjectionCriteria = 4 AND 
			tblCalcCostStructureAssumptionsByField.Client<>@intMinYear THEN tblCalcTechnicalAssumption.CycleValue - 1 ELSE 0 END AS [Year], tblProjects.Operation, CAST(1 AS BIT) AS BaseYear, 
			tblCalcTechnicalAssumption.IdTechnicalDriverSize, tblCalcTechnicalAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalAssumption.CycleValue, 
			tblCalcTechnicalAssumption.ProjectionCriteria, tblCalcCostStructureAssumptionsByField.Client, tblCalcCostStructureAssumptionsByField.Ziff
		FROM tblCalcTechnicalAssumption LEFT OUTER JOIN
			tblCalcCostStructureAssumptionsByField LEFT OUTER JOIN
			tblFields ON tblCalcCostStructureAssumptionsByField.IdField = tblFields.IdField ON tblCalcTechnicalAssumption.IdZiffAccount = tblCalcCostStructureAssumptionsByField.IdZiffAccount AND 
			tblFields.IdTypeOperation = tblCalcTechnicalAssumption.IdTypeOperation LEFT OUTER JOIN
			tblProjects ON tblFields.IdField = tblProjects.IdField
		GROUP BY tblCalcTechnicalAssumption.IdModel, tblCalcTechnicalAssumption.IdZiffAccount, tblCalcTechnicalAssumption.IdTypeOperation, tblFields.IdField, 
			CASE WHEN tblProjects.Operation = 1 THEN 0 ELSE tblProjects.IdProject END, tblProjects.Operation, tblCalcTechnicalAssumption.IdTechnicalDriverSize, 
			tblCalcTechnicalAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalAssumption.CycleValue, tblCalcTechnicalAssumption.ProjectionCriteria, 
			tblCalcCostStructureAssumptionsByField.Client, tblCalcCostStructureAssumptionsByField.Ziff
		HAVING tblCalcCostStructureAssumptionsByField.Client=@YearBase AND tblProjects.Operation=1 AND tblCalcTechnicalAssumption.IdModel=@IdModel;

		FETCH NEXT FROM YearList INTO @YearBase;
	END

	CLOSE YearList;
	DEALLOCATE YearList;

	/** Set Semi-Variable Flag **/
	UPDATE tblCalcTechnicalDriverAssumption SET [SemivarForecast]=1 WHERE ProjectionCriteria=5 AND Operation=1 AND IdModel=@IdModel;
	
	/** Update Company BaseYear Driver Values **/
	EXEC calcModuleTechnicalDriverUpdate @IdModel,1,0;
	
	/** Update Company BaseCost Value for BaseYear **/
	UPDATE tblCalcBaseCostByField SET
		[TechApplicable]=1,
		[TFNormal] = 1,
		[TFPessimistic] = 1,
		[TFOptimistic] = 1
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField
	WHERE tblCalcTechnicalDriverAssumption.BaseYear=1 AND tblCalcTechnicalDriverAssumption.Operation=1 AND tblCalcTechnicalDriverAssumption.ProjectionCriteria IN (1,2) AND tblCalcBaseCostByField.IdModel=@IdModel;

	UPDATE tblCalcBaseCostByField SET
		tblCalcBaseCostByField.TechApplicable=1,
		tblCalcBaseCostByField.TFNormal = CBCBF.TFNormal,
		tblCalcBaseCostByField.TFPessimistic = CBCBF.TFPessimistic, 
		tblCalcBaseCostByField.TFOptimistic = CBCBF.TFOptimistic
	FROM tblCalcBaseCostByField INNER JOIN
		(
		SELECT tblCalcBaseCostByField_1.IdModel, tblCalcBaseCostByField_1.IdActivity, tblCalcBaseCostByField_1.IdResources, 
			tblCalcBaseCostByField_1.IdClientAccount, tblCalcBaseCostByField_1.IdField, tblCalcBaseCostByField_1.IdClientCostCenter, 
			tblCalcBaseCostByField_1.IdZiffAccount, SUM(CASE WHEN [SizeNormal] IS NULL 
			THEN 0 ELSE [SizeNormal] * (CASE WHEN ISNULL([PerformanceNormal], 0) = 0 THEN 1 ELSE [PerformanceNormal] END) END) AS TFNormal, 
			SUM(CASE WHEN [SizePessimistic] IS NULL THEN 0 ELSE [SizePessimistic] * (CASE WHEN ISNULL([PerformancePessimistic], 0) 
			= 0 THEN 1 ELSE [PerformancePessimistic] END) END) AS TFPessimistic, SUM(CASE WHEN [SizeOptimistic] IS NULL 
			THEN 0 ELSE [SizeOptimistic] * (CASE WHEN ISNULL([PerformanceOptimistic], 0) = 0 THEN 1 ELSE [PerformanceOptimistic] END) END) 
			AS TFOptimistic
		FROM tblCalcBaseCostByField AS tblCalcBaseCostByField_1 INNER JOIN
			tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField_1.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
			tblCalcBaseCostByField_1.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount AND 
			tblCalcBaseCostByField_1.IdField = tblCalcTechnicalDriverAssumption.IdField
			WHERE tblCalcTechnicalDriverAssumption.BaseYear=1 AND tblCalcTechnicalDriverAssumption.Operation=1 AND 
			tblCalcTechnicalDriverAssumption.ProjectionCriteria IN (3, 4, 5)
		GROUP BY tblCalcBaseCostByField_1.IdModel, tblCalcBaseCostByField_1.IdActivity, tblCalcBaseCostByField_1.IdResources, 
			tblCalcBaseCostByField_1.IdClientAccount, tblCalcBaseCostByField_1.IdField, tblCalcBaseCostByField_1.IdClientCostCenter, 
			tblCalcBaseCostByField_1.IdZiffAccount
		) AS CBCBF ON tblCalcBaseCostByField.IdModel = CBCBF.IdModel AND 
		tblCalcBaseCostByField.IdActivity = CBCBF.IdActivity AND tblCalcBaseCostByField.IdResources = CBCBF.IdResources AND 
		tblCalcBaseCostByField.IdClientAccount = CBCBF.IdClientAccount AND tblCalcBaseCostByField.IdField = CBCBF.IdField AND 
		tblCalcBaseCostByField.IdClientCostCenter = CBCBF.IdClientCostCenter AND tblCalcBaseCostByField.IdZiffAccount = CBCBF.IdZiffAccount
	WHERE tblCalcBaseCostByField.IdModel = @IdModel;
	
	UPDATE tblCalcBaseCostByField SET
		[BCRNormal] = CASE WHEN ISNULL([TFNormal],0)=0 THEN 0 ELSE [Amount]/[TFNormal] END,
		[BCRPessimistic] = CASE WHEN ISNULL([TFPessimistic],0)=0 THEN 0 ELSE [Amount]/[TFPessimistic] END,
		[BCROptimistic] = CASE WHEN ISNULL([TFOptimistic],0)=0 THEN 0 ELSE [Amount]/[TFOptimistic] END
	WHERE TechApplicable=1 AND IdModel=@IdModel;

	UPDATE tblCalcBaseCostByField SET
		[BCRNormal] = 0,
		[BCRPessimistic] = 0,
		[BCROptimistic] = 0
	WHERE TechApplicable=0 AND IdModel=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- SETUP ZIFF BASE AND FORECAST ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Create Ziff BaseYear Technical Driver Assumption Lookup **/
	DECLARE @YearBaseZiff INT;
	DECLARE YearListZiff CURSOR FOR
	SELECT Ziff AS Year
	FROM tblCalcCostStructureAssumptionsByField
	WHERE IdModel=@IdModel
	GROUP BY Ziff
	HAVING Ziff > 0
	ORDER BY Year;

	OPEN YearListZiff;
	FETCH NEXT FROM YearListZiff INTO @YearBaseZiff;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		INSERT INTO tblCalcTechnicalDriverAssumptionZiff ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff])
		SELECT 1, tblCalcTechnicalAssumption.IdModel, tblCalcTechnicalAssumption.IdZiffAccount, tblCalcTechnicalAssumption.IdTypeOperation, tblFields.IdField, 
			CASE WHEN tblProjects.Operation = 1 THEN 0 ELSE tblProjects.IdProject END AS IdProject, @YearBaseZiff + CASE WHEN tblCalcTechnicalAssumption.ProjectionCriteria = 4 AND 
			tblCalcCostStructureAssumptionsByField.Ziff = 0 THEN tblCalcTechnicalAssumption.CycleValue - 1 ELSE 0 END AS [Year], tblProjects.Operation, CAST(1 AS BIT) AS BaseYear, 
			tblCalcTechnicalAssumption.IdTechnicalDriverSize, tblCalcTechnicalAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalAssumption.CycleValue, 
			tblCalcTechnicalAssumption.ProjectionCriteria, tblCalcCostStructureAssumptionsByField.Client, tblCalcCostStructureAssumptionsByField.Ziff
		FROM tblCalcTechnicalAssumption LEFT OUTER JOIN
			tblCalcCostStructureAssumptionsByField LEFT OUTER JOIN
			tblFields ON tblCalcCostStructureAssumptionsByField.IdField = tblFields.IdField ON tblCalcTechnicalAssumption.IdZiffAccount = tblCalcCostStructureAssumptionsByField.IdZiffAccount AND 
			tblFields.IdTypeOperation = tblCalcTechnicalAssumption.IdTypeOperation LEFT OUTER JOIN
			tblProjects ON tblFields.IdField = tblProjects.IdField
		GROUP BY tblCalcTechnicalAssumption.IdModel, tblCalcTechnicalAssumption.IdZiffAccount, tblCalcTechnicalAssumption.IdTypeOperation, tblFields.IdField, 
			CASE WHEN tblProjects.Operation = 1 THEN 0 ELSE tblProjects.IdProject END, tblProjects.Operation, tblCalcTechnicalAssumption.IdTechnicalDriverSize, 
			tblCalcTechnicalAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalAssumption.CycleValue, tblCalcTechnicalAssumption.ProjectionCriteria, 
			tblCalcCostStructureAssumptionsByField.Client, tblCalcCostStructureAssumptionsByField.Ziff
		HAVING tblCalcCostStructureAssumptionsByField.Ziff=@YearBaseZiff AND tblProjects.Operation=1 AND tblCalcTechnicalAssumption.IdModel=@IdModel;

		FETCH NEXT FROM YearListZiff INTO @YearBaseZiff;
	END

	CLOSE YearListZiff;
	DEALLOCATE YearListZiff;

	/** Set Semi-Variable Flag **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET [SemivarForecast]=1 WHERE ProjectionCriteria=5 AND Operation=1 AND IdModel=@IdModel;
	
	/** Update Ziff BaseYear Driver Values **/
	EXEC calcModuleTechnicalDriverUpdate @IdModel,2,1;
	
	/** Update Ziff BaseCost Value for BaseYear **/
	UPDATE tblCalcBaseCostByFieldZiff SET
		[TechApplicable]=1,
		[TFNormal] = 1,
		[TFPessimistic] = 1,
		[TFOptimistic] = 1
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField
	WHERE tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 AND tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria IN (1,2) AND tblCalcBaseCostByFieldZiff.IdModel=@IdModel;

	UPDATE tblCalcBaseCostByFieldZiff SET
		tblCalcBaseCostByFieldZiff.TechApplicable=1,
		tblCalcBaseCostByFieldZiff.TFNormal = CBCBFZ.TFNormal,
		tblCalcBaseCostByFieldZiff.TFPessimistic = CBCBFZ.TFPessimistic,
		tblCalcBaseCostByFieldZiff.TFOptimistic = CBCBFZ.TFOptimistic
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		 (
		 SELECT tblCalcBaseCostByFieldZiff_1.IdModel, tblCalcBaseCostByFieldZiff_1.IdField, tblCalcBaseCostByFieldZiff_1.IdZiffAccount, 
			 SUM(CASE WHEN [SizeNormal] IS NULL THEN 0 ELSE [SizeNormal] * (CASE WHEN ISNULL([PerformanceNormal], 0) 
			 = 0 THEN 1 ELSE [PerformanceNormal] END) END) AS TFNormal, SUM(CASE WHEN [SizePessimistic] IS NULL 
			 THEN 0 ELSE [SizePessimistic] * (CASE WHEN ISNULL([PerformancePessimistic], 0) = 0 THEN 1 ELSE [PerformancePessimistic] END) END) 
			 AS TFPessimistic, SUM(CASE WHEN [SizeOptimistic] IS NULL THEN 0 ELSE [SizeOptimistic] * (CASE WHEN ISNULL([PerformanceOptimistic], 0) 
			 = 0 THEN 1 ELSE [PerformanceOptimistic] END) END) AS TFOptimistic
			 FROM tblCalcBaseCostByFieldZiff AS tblCalcBaseCostByFieldZiff_1 INNER JOIN
			 tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff_1.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
			 tblCalcBaseCostByFieldZiff_1.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount AND 
			 tblCalcBaseCostByFieldZiff_1.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField
		 WHERE tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 AND 
			tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria IN (3, 4, 5)
		 GROUP BY tblCalcBaseCostByFieldZiff_1.IdModel, tblCalcBaseCostByFieldZiff_1.IdField, tblCalcBaseCostByFieldZiff_1.IdZiffAccount
		 ) AS CBCBFZ ON 
		 tblCalcBaseCostByFieldZiff.IdModel = CBCBFZ.IdModel AND tblCalcBaseCostByFieldZiff.IdField = CBCBFZ.IdField AND 
		 tblCalcBaseCostByFieldZiff.IdZiffAccount = CBCBFZ.IdZiffAccount
	WHERE tblCalcBaseCostByFieldZiff.IdModel = @IdModel;

	UPDATE tblCalcBaseCostByFieldZiff SET
		[BCRNormal] = CASE WHEN ISNULL([TFNormal],0)=0 THEN 0 ELSE [TotalCost]/[TFNormal] END,
		[BCRPessimistic] = CASE WHEN ISNULL([TFPessimistic],0)=0 THEN 0 ELSE [TotalCost]/[TFPessimistic] END,
		[BCROptimistic] = CASE WHEN ISNULL([TFOptimistic],0)=0 THEN 0 ELSE [TotalCost]/[TFOptimistic] END
	WHERE TechApplicable=1 AND IdModel=@IdModel;
	
	UPDATE tblCalcBaseCostByFieldZiff SET
		[BCRNormal] = 0,
		[BCRPessimistic] = 0,
		[BCROptimistic] = 0
	WHERE TechApplicable=0 AND IdModel=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- UPDATE BASE COST RATES BASE YEAR
	--------------------------------------------------------------------------------------
	
	/** Update Company Base Cost Rates **/
	UPDATE tblCalcTechnicalDriverAssumption SET
		[BCRNormal] = CalcBaseCostByFieldSummary.BCRNormal,
		[BCRPessimistic] = CalcBaseCostByFieldSummary.BCRPessimistic,
		[BCROptimistic] = CalcBaseCostByFieldSummary.BCROptimistic
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(BCRNormal) AS BCRNormal, SUM(BCRPessimistic) AS BCRPessimistic, SUM(BCROptimistic) AS BCROptimistic
		FROM tblCalcBaseCostByField AS CBCBF
		GROUP BY IdModel, IdField, IdZiffAccount
		) AS CalcBaseCostByFieldSummary ON 
		tblCalcTechnicalDriverAssumption.IdModel = CalcBaseCostByFieldSummary.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = CalcBaseCostByFieldSummary.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = CalcBaseCostByFieldSummary.IdField
	WHERE tblCalcTechnicalDriverAssumption.BaseYear=1 AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Update Ziff Base Cost Rates **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		[BCRNormal] = CalcBaseCostByFieldSummaryZiff.BCRNormal,
		[BCRPessimistic] = CalcBaseCostByFieldSummaryZiff.BCRPessimistic,
		[BCROptimistic] = CalcBaseCostByFieldSummaryZiff.BCROptimistic
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(BCRNormal) AS BCRNormal, SUM(BCRPessimistic) AS BCRPessimistic, SUM(BCROptimistic) AS BCROptimistic
		FROM tblCalcBaseCostByFieldZiff AS CBCBFZ
		GROUP BY IdModel, IdField, IdZiffAccount
		) AS CalcBaseCostByFieldSummaryZiff ON 
		tblCalcTechnicalDriverAssumptionZiff.IdModel = CalcBaseCostByFieldSummaryZiff.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = CalcBaseCostByFieldSummaryZiff.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = CalcBaseCostByFieldSummaryZiff.IdField
	WHERE tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	/** Drop Ziff BaseYear with no costs **/
	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE [BaseYear]=1 AND [BCRNormal] IS NULL AND [BCRPessimistic] IS NULL AND [BCROptimistic] IS NULL AND [IdModel]=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- SETUP ADDITIONAL YEARS ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Create Company Additional Years Technical Driver Assumption Lookup **/
	INSERT INTO tblCalcTechnicalDriverAssumption ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff],[SemivarForecast])
	SELECT 2, tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, tblCalcTechnicalDriverAssumption.IdTypeOperation, 
		tblCalcTechnicalDriverAssumption.IdField, tblCalcTechnicalDriverAssumption.IdProject, tblCalcModelYears.Year, 
		tblCalcTechnicalDriverAssumption.Operation, CAST(0 AS BIT) AS BaseYear, tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize, 
		tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance, tblCalcTechnicalDriverAssumption.CycleValue, 
		tblCalcTechnicalDriverAssumption.ProjectionCriteria, tblCalcTechnicalDriverAssumption.YearClient, tblCalcTechnicalDriverAssumption.YearZiff, tblCalcTechnicalDriverAssumption.SemivarForecast
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		tblCalcModelYears ON tblCalcTechnicalDriverAssumption.IdModel = tblCalcModelYears.IdModel AND 
		tblCalcTechnicalDriverAssumption.Year < tblCalcModelYears.Year
	WHERE tblCalcModelYears.Year<=@intMaxYear AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Create Ziff Additional Years Technical Driver Assumption Lookup **/
	INSERT INTO tblCalcTechnicalDriverAssumptionZiff ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff],[SemivarForecast])
	SELECT 2, tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdTypeOperation, 
		tblCalcTechnicalDriverAssumptionZiff.IdField, tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcModelYears.Year, 
		tblCalcTechnicalDriverAssumptionZiff.Operation, CAST(0 AS BIT) AS BaseYear, tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverSize, 
		tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverPerformance, tblCalcTechnicalDriverAssumptionZiff.CycleValue, 
		tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.YearClient, tblCalcTechnicalDriverAssumptionZiff.YearZiff, tblCalcTechnicalDriverAssumptionZiff.SemivarForecast
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		tblCalcModelYears ON tblCalcTechnicalDriverAssumptionZiff.IdModel = tblCalcModelYears.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.Year < tblCalcModelYears.Year
	WHERE tblCalcModelYears.Year<=@intMaxYear AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	/** Create Company BusinessOpportunity Technical Driver Assumption Lookup **/
	INSERT INTO tblCalcTechnicalDriverAssumption ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff],[SemivarForecast])
	SELECT 3, tblCalcTechnicalDriverAssumption.IdModel, tblCalcTechnicalDriverAssumption.IdZiffAccount, tblCalcTechnicalDriverAssumption.IdTypeOperation, 
		tblCalcTechnicalDriverAssumption.IdField, tblProjects.IdProject, tblCalcTechnicalDriverAssumption.Year, tblProjects.Operation, 
		CASE WHEN tblCalcTechnicalDriverAssumption.Year = tblProjects.Starts THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS BaseYear, 
		tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize, tblCalcTechnicalDriverAssumption.IdTechnicalDriverPerformance,  
		tblCalcTechnicalDriverAssumption.CycleValue, tblCalcTechnicalDriverAssumption.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumption.YearClient, tblCalcTechnicalDriverAssumption.YearZiff, tblCalcTechnicalDriverAssumption.SemivarForecast
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumption.IdModel = tblProjects.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdField = tblProjects.IdField AND tblCalcTechnicalDriverAssumption.Year >= tblProjects.Starts
	WHERE tblProjects.Operation=2 AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Remove Company BusinessOpportunity Technical Driver Assumption = Fixed or Semi-Fixed **/
	DELETE FROM tblCalcTechnicalDriverAssumption WHERE ([ProjectionCriteria]=1 OR [ProjectionCriteria]=2) AND [Operation]=2 AND [IdModel]=@IdModel;

	/** Create Ziff BusinessOpportunity Technical Driver Assumption Lookup **/
	/** Note: Modifying SemivarProjection with Fixed=1 to Variable=3 for Forecast **/
	INSERT INTO tblCalcTechnicalDriverAssumptionZiff ([SourceDebug],[IdModel],[IdZiffAccount],[IdTypeOperation],[IdField],[IdProject],[Year],[Operation],[BaseYear],[IdTechnicalDriverSize],[IdTechnicalDriverPerformance],[CycleValue],[ProjectionCriteria],[YearClient],[YearZiff],[SemivarForecast])
	SELECT 3, tblCalcTechnicalDriverAssumptionZiff.IdModel, tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdTypeOperation, 
		tblCalcTechnicalDriverAssumptionZiff.IdField, tblProjects.IdProject, tblCalcTechnicalDriverAssumptionZiff.Year, tblProjects.Operation, 
		CASE WHEN tblCalcTechnicalDriverAssumptionZiff.Year = tblProjects.Starts THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS BaseYear, 
		tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverSize, tblCalcTechnicalDriverAssumptionZiff.IdTechnicalDriverPerformance, 
		tblCalcTechnicalDriverAssumptionZiff.CycleValue, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria,
		tblCalcTechnicalDriverAssumptionZiff.YearClient, tblCalcTechnicalDriverAssumptionZiff.YearZiff, tblCalcTechnicalDriverAssumptionZiff.SemivarForecast
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		tblProjects ON tblCalcTechnicalDriverAssumptionZiff.IdModel = tblProjects.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = tblProjects.IdField AND tblCalcTechnicalDriverAssumptionZiff.Year >= tblProjects.Starts
	WHERE tblProjects.Operation=2 AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	/** Remove Ziff BusinessOpportunity Technical Driver Assumption = Fixed or Semi-Fixed **/
	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE ([ProjectionCriteria]=1 OR [ProjectionCriteria]=2) AND [Operation]=2 AND [IdModel]=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- CLEAN UP ASSUMPTIONS
	--------------------------------------------------------------------------------------
	
	/** Remove Base Year Assumptions not in initial Year for Semi-Fixed **/
	UPDATE tblCalcTechnicalDriverAssumption SET [BaseYear]=0 WHERE [ProjectionCriteria]=2 AND [BaseYear]=1 AND [Year]<>@intMinYear AND [IdModel]=@IdModel;
	
	/** Rebase and Remove Excess Years from all Cyclical Assumptions **/
	EXEC calcModuleTechnicalCyclicalRebase @IdModel,@intMinYear,@intMaxYear;
	
	--------------------------------------------------------------------------------------
	-- ADD CLIENT AND ZIFF COST ASSUMPTION START YEARS TO TABLE
	--------------------------------------------------------------------------------------
	
	/** Update Company Client/Ziff Years Lookup **/
	UPDATE tblCalcTechnicalDriverAssumption
	SET YearClient=CTDALookup.YearClient, YearZiff=CTDALookup.YearZiff
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		(
		SELECT CTDA.IdModel, CTDA.IdZiffAccount, CTDA.IdField, CTDA.YearClient, CTDA.YearZiff
		FROM tblCalcTechnicalDriverAssumption AS CTDA INNER JOIN
			tblCalcCostStructureAssumptionsByField AS CSABF ON CTDA.IdModel = CSABF.IdModel AND CTDA.IdField = CSABF.IdField AND CTDA.IdZiffAccount = CSABF.IdZiffAccount
		WHERE CTDA.Operation=1 AND CTDA.BaseYear=1
		GROUP BY CTDA.IdModel, CTDA.IdZiffAccount, CTDA.IdField, CTDA.YearClient, CTDA.YearZiff
		) AS CTDALookup ON 
		tblCalcTechnicalDriverAssumption.IdModel = CTDALookup.IdModel AND tblCalcTechnicalDriverAssumption.IdZiffAccount = CTDALookup.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = CTDALookup.IdField
	WHERE tblCalcTechnicalDriverAssumption.YearClient IS NULL AND tblCalcTechnicalDriverAssumption.YearZiff IS NULL AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Remove Company Years Based on Company/Ziff Start Years **/
	DELETE FROM tblCalcTechnicalDriverAssumption WHERE [YearZiff]>0 AND [Year]>=[YearZiff] AND [IdModel]=@IdModel;
	
	/** Update Ziff Client/Ziff Years Lookup **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff
	SET YearClient=CTDAZLookup.YearClient, YearZiff=CTDAZLookup.YearZiff
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		(
		SELECT CTDAZ.IdModel, CTDAZ.IdZiffAccount, CTDAZ.IdField, CTDAZ.YearClient, CTDAZ.YearZiff
		FROM tblCalcTechnicalDriverAssumptionZiff AS CTDAZ INNER JOIN
			tblCalcCostStructureAssumptionsByField AS CSABF ON CTDAZ.IdModel = CSABF.IdModel AND CTDAZ.IdField = CSABF.IdField AND CTDAZ.IdZiffAccount = CSABF.IdZiffAccount
		WHERE CTDAZ.Operation=1 AND CTDAZ.BaseYear=1
		GROUP BY CTDAZ.IdModel, CTDAZ.IdZiffAccount, CTDAZ.IdField, CTDAZ.YearClient, CTDAZ.YearZiff
		) AS CTDAZLookup ON 
		tblCalcTechnicalDriverAssumptionZiff.IdModel = CTDAZLookup.IdModel AND tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = CTDAZLookup.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = CTDAZLookup.IdField
	WHERE tblCalcTechnicalDriverAssumptionZiff.YearClient IS NULL AND tblCalcTechnicalDriverAssumptionZiff.YearZiff IS NULL AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	/** Remove Ziff Years Based on Company/Ziff Start Years (SHOULD BE NO RECORDS - THIS IS JUST A DOUBLE CHECK) **/
	DELETE FROM tblCalcTechnicalDriverAssumptionZiff WHERE [YearClient]>0 AND [Year]<[YearZiff] AND [IdModel]=@IdModel;
	
	/** Ensure All Ziff BO Items as properly flagged as base year **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET BaseYear=1 WHERE [Operation]=2 AND [BaseYear]=0 AND [Year]=[YearZiff] AND IdModel=@IdModel;	

	--------------------------------------------------------------------------------------
	-- PREP FOR TECHNICAL BASE COST RATE CALCS
	--------------------------------------------------------------------------------------
	
	/** Update Company TF (Base Year Only for BCR Calc) **/
	UPDATE tblCalcTechnicalDriverAssumption SET 
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeNormal], 0) = 0 THEN 0 ELSE [SizeNormal]*(CASE WHEN ISNULL([PerformanceNormal],0)=0 THEN 1 ELSE [PerformanceNormal] END) END ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizePessimistic], 0) = 0 THEN 0 ELSE [SizePessimistic]*(CASE WHEN ISNULL([PerformancePessimistic],0)=0 THEN 1 ELSE [PerformancePessimistic] END) END ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeOptimistic], 0) = 0 THEN 0 ELSE [SizeOptimistic]*(CASE WHEN ISNULL([PerformanceOptimistic],0)=0 THEN 1 ELSE [PerformanceOptimistic] END) END ELSE 0 END
	WHERE [BaseYear]=1 AND [IdModel]=@IdModel;
	
	/** Update Ziff TF  (Base Year Only for BCR Calc) **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeNormal], 0) = 0 THEN 0 ELSE [SizeNormal]*(CASE WHEN ISNULL([PerformanceNormal],0)=0 THEN 1 ELSE [PerformanceNormal] END) END ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizePessimistic], 0) = 0 THEN 0 ELSE [SizePessimistic]*(CASE WHEN ISNULL([PerformancePessimistic],0)=0 THEN 1 ELSE [PerformancePessimistic] END) END ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeOptimistic], 0) = 0 THEN 0 ELSE [SizeOptimistic]*(CASE WHEN ISNULL([PerformanceOptimistic],0)=0 THEN 1 ELSE [PerformanceOptimistic] END) END ELSE 0 END
	WHERE [BaseYear]=1 AND [IdModel]=@IdModel;

	/** Remove Base Year Items without Project Drivers **/
	DELETE FROM tblCalcTechnicalDriverAssumption
	WHERE SizeNormal IS NULL AND SizePessimistic IS NULL AND SizeOptimistic IS NULL AND PerformanceNormal IS NULL AND PerformancePessimistic IS NULL AND PerformanceOptimistic IS NULL AND BCRNormal IS NULL AND BCRPessimistic IS NULL AND BCROptimistic IS NULL AND ProjectionCriteria IN (2,3,4,5) AND BaseYear=1 AND Operation=1 AND IdModel=@IdModel;

	/** Technical Base Cost Rate **/
	EXEC calcModuleTechnicalBaseCostRate @IdModel;

	--------------------------------------------------------------------------------------
	-- POST TECHNICAL BASE COST RATE RESET
	--------------------------------------------------------------------------------------
	
	/** Update All Years Driver Values **/
	EXEC calcModuleTechnicalDriverUpdate @IdModel,1,0;
	EXEC calcModuleTechnicalDriverUpdate @IdModel,2,0;
		
	/** Update Company TF **/
	UPDATE tblCalcTechnicalDriverAssumption SET 
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeNormal], 0) = 0 THEN 0 ELSE [SizeNormal]*(CASE WHEN ISNULL([PerformanceNormal],0)=0 THEN 1 ELSE [PerformanceNormal] END) END ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizePessimistic], 0) = 0 THEN 0 ELSE [SizePessimistic]*(CASE WHEN ISNULL([PerformancePessimistic],0)=0 THEN 1 ELSE [PerformancePessimistic] END) END ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeOptimistic], 0) = 0 THEN 0 ELSE [SizeOptimistic]*(CASE WHEN ISNULL([PerformanceOptimistic],0)=0 THEN 1 ELSE [PerformanceOptimistic] END) END ELSE 0 END
	WHERE [IdModel]=@IdModel;
	
	/** Update Ziff TF **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		[TFNormal] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5  THEN CASE WHEN ISNULL([SizeNormal], 0) = 0 THEN 0 ELSE [SizeNormal]*(CASE WHEN ISNULL([PerformanceNormal],0)=0 THEN 1 ELSE [PerformanceNormal] END) END ELSE 0 END,
		[TFPessimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizePessimistic], 0) = 0 THEN 0 ELSE [SizePessimistic]*(CASE WHEN ISNULL([PerformancePessimistic],0)=0 THEN 1 ELSE [PerformancePessimistic] END) END ELSE 0 END,
		[TFOptimistic] = CASE WHEN [ProjectionCriteria]=1 OR [ProjectionCriteria]=2 THEN 1 WHEN [ProjectionCriteria]=3 OR [ProjectionCriteria]=4 OR [ProjectionCriteria]=5 THEN CASE WHEN ISNULL([SizeOptimistic], 0) = 0 THEN 0 ELSE [SizeOptimistic]*(CASE WHEN ISNULL([PerformanceOptimistic],0)=0 THEN 1 ELSE [PerformanceOptimistic] END) END ELSE 0 END
	WHERE [IdModel]=@IdModel;

	--/** Remove Base Year Items without Project Drivers **/
	DELETE FROM tblCalcTechnicalDriverAssumption
	WHERE SizeNormal IS NULL AND SizePessimistic IS NULL AND SizeOptimistic IS NULL AND PerformanceNormal IS NULL AND PerformancePessimistic IS NULL AND PerformanceOptimistic IS NULL AND BCRNormal IS NULL AND BCRPessimistic IS NULL AND BCROptimistic IS NULL AND ProjectionCriteria IN (2,3,4,5) AND BaseYear=1 AND Operation=1 AND IdModel=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- UPDATE BASE COST RATES ALL YEARS
	--------------------------------------------------------------------------------------
	
	/** Update Company Base Cost Rates **/
	UPDATE tblCalcTechnicalDriverAssumption SET
		[BCRNormal] = CalcBaseCostByFieldSummary.BCRNormal,
		[BCRPessimistic] = CalcBaseCostByFieldSummary.BCRPessimistic,
		[BCROptimistic] = CalcBaseCostByFieldSummary.BCROptimistic
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(BCRNormal) AS BCRNormal, SUM(BCRPessimistic) AS BCRPessimistic, SUM(BCROptimistic) AS BCROptimistic
		FROM tblCalcBaseCostByField AS CBCBF
		GROUP BY IdModel, IdField, IdZiffAccount
		) AS CalcBaseCostByFieldSummary ON 
		tblCalcTechnicalDriverAssumption.IdModel = CalcBaseCostByFieldSummary.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = CalcBaseCostByFieldSummary.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = CalcBaseCostByFieldSummary.IdField
	WHERE tblCalcTechnicalDriverAssumption.IdModel=@IdModel;
	
	/** Update Ziff Base Cost Rates **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		[BCRNormal] = CalcBaseCostByFieldSummaryZiff.BCRNormal,
		[BCRPessimistic] = CalcBaseCostByFieldSummaryZiff.BCRPessimistic,
		[BCROptimistic] = CalcBaseCostByFieldSummaryZiff.BCROptimistic
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		(
		SELECT IdModel, IdField, IdZiffAccount, SUM(BCRNormal) AS BCRNormal, SUM(BCRPessimistic) AS BCRPessimistic, SUM(BCROptimistic) AS BCROptimistic
		FROM tblCalcBaseCostByFieldZiff AS CBCBFZ
		GROUP BY IdModel, IdField, IdZiffAccount
		) AS CalcBaseCostByFieldSummaryZiff ON 
		tblCalcTechnicalDriverAssumptionZiff.IdModel = CalcBaseCostByFieldSummaryZiff.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = CalcBaseCostByFieldSummaryZiff.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = CalcBaseCostByFieldSummaryZiff.IdField
	WHERE tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;

	--------------------------------------------------------------------------------------
	-- SEMI-VARIABLE Baseline Reset as Fixed Rates
	--------------------------------------------------------------------------------------

	/** Update Company Base Cost Rates Semi-Variable **/
	UPDATE tblCalcTechnicalDriverAssumption SET
		tblCalcTechnicalDriverAssumption.TFNormal=1,
		tblCalcTechnicalDriverAssumption.TFPessimistic=1,
		tblCalcTechnicalDriverAssumption.TFOptimistic=1,
		tblCalcTechnicalDriverAssumption.BCRNormal=BCRSV.SumAmount,
		tblCalcTechnicalDriverAssumption.BCRPessimistic=BCRSV.SumAmount,
		tblCalcTechnicalDriverAssumption.BCROptimistic=BCRSV.SumAmount
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		(
		SELECT tblCalcTechnicalDriverAssumption_1.IdModel, tblCalcTechnicalDriverAssumption_1.IdZiffAccount, tblCalcTechnicalDriverAssumption_1.IdField, tblCalcTechnicalDriverAssumption_1.IdProject, 
			tblCalcTechnicalDriverAssumption_1.Year, SUM(tblCalcBaseCostByField.Amount) AS SumAmount
		FROM tblCalcTechnicalDriverAssumption AS tblCalcTechnicalDriverAssumption_1 INNER JOIN
			tblCalcBaseCostByField ON tblCalcTechnicalDriverAssumption_1.IdModel = tblCalcBaseCostByField.IdModel AND 
			tblCalcTechnicalDriverAssumption_1.IdField = tblCalcBaseCostByField.IdField AND tblCalcTechnicalDriverAssumption_1.IdZiffAccount = tblCalcBaseCostByField.IdZiffAccount
		WHERE tblCalcTechnicalDriverAssumption_1.Operation=1 AND tblCalcTechnicalDriverAssumption_1.ProjectionCriteria=5
		GROUP BY tblCalcTechnicalDriverAssumption_1.IdModel, tblCalcTechnicalDriverAssumption_1.IdZiffAccount, tblCalcTechnicalDriverAssumption_1.IdField, tblCalcTechnicalDriverAssumption_1.IdProject, 
			tblCalcTechnicalDriverAssumption_1.Year
		) AS BCRSV ON tblCalcTechnicalDriverAssumption.IdModel = BCRSV.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = BCRSV.IdZiffAccount AND tblCalcTechnicalDriverAssumption.IdField = BCRSV.IdField AND 
		tblCalcTechnicalDriverAssumption.IdProject = BCRSV.IdProject AND tblCalcTechnicalDriverAssumption.Year = BCRSV.Year
	WHERE tblCalcTechnicalDriverAssumption.IdModel=@IdModel;

	UPDATE tblCalcTechnicalDriverAssumption SET
		tblCalcTechnicalDriverAssumption.TFNormal=1,
		tblCalcTechnicalDriverAssumption.TFPessimistic=1,
		tblCalcTechnicalDriverAssumption.TFOptimistic=1
	FROM tblCalcTechnicalDriverAssumption
	WHERE tblCalcTechnicalDriverAssumption.Operation=1 AND tblCalcTechnicalDriverAssumption.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumption.IdModel=@IdModel;

	/** Update Ziff Base Cost Rates Semi-Variable **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		tblCalcTechnicalDriverAssumptionZiff.TFNormal=1,
		tblCalcTechnicalDriverAssumptionZiff.TFPessimistic=1,
		tblCalcTechnicalDriverAssumptionZiff.TFOptimistic=1,
		tblCalcTechnicalDriverAssumptionZiff.BCRNormal=BCRSVZ.SumAmount,
		tblCalcTechnicalDriverAssumptionZiff.BCRPessimistic=BCRSVZ.SumAmount,
		tblCalcTechnicalDriverAssumptionZiff.BCROptimistic=BCRSVZ.SumAmount
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		(
		SELECT tblCalcTechnicalDriverAssumptionZiff_1.IdModel, tblCalcTechnicalDriverAssumptionZiff_1.IdZiffAccount, tblCalcTechnicalDriverAssumptionZiff_1.IdField, tblCalcTechnicalDriverAssumptionZiff_1.IdProject, 
			tblCalcTechnicalDriverAssumptionZiff_1.Year, SUM(tblCalcBaseCostByFieldZiff.TotalCost) AS SumAmount
		FROM tblCalcTechnicalDriverAssumptionZiff AS tblCalcTechnicalDriverAssumptionZiff_1 INNER JOIN
			tblCalcBaseCostByFieldZiff ON tblCalcTechnicalDriverAssumptionZiff_1.IdModel = tblCalcBaseCostByFieldZiff.IdModel AND 
			tblCalcTechnicalDriverAssumptionZiff_1.IdField = tblCalcBaseCostByFieldZiff.IdField AND tblCalcTechnicalDriverAssumptionZiff_1.IdZiffAccount = tblCalcBaseCostByFieldZiff.IdZiffAccount
		WHERE tblCalcTechnicalDriverAssumptionZiff_1.Operation=1 AND tblCalcTechnicalDriverAssumptionZiff_1.ProjectionCriteria=5
		GROUP BY tblCalcTechnicalDriverAssumptionZiff_1.IdModel, tblCalcTechnicalDriverAssumptionZiff_1.IdZiffAccount, tblCalcTechnicalDriverAssumptionZiff_1.IdField, tblCalcTechnicalDriverAssumptionZiff_1.IdProject, 
			tblCalcTechnicalDriverAssumptionZiff_1.Year
		) AS BCRSVZ ON tblCalcTechnicalDriverAssumptionZiff.IdModel = BCRSVZ.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = BCRSVZ.IdZiffAccount AND tblCalcTechnicalDriverAssumptionZiff.IdField = BCRSVZ.IdField AND 
		tblCalcTechnicalDriverAssumptionZiff.IdProject = BCRSVZ.IdProject AND tblCalcTechnicalDriverAssumptionZiff.Year = BCRSVZ.Year
	WHERE tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;

	UPDATE tblCalcTechnicalDriverAssumptionZiff SET
		tblCalcTechnicalDriverAssumptionZiff.TFNormal=1,
		tblCalcTechnicalDriverAssumptionZiff.TFPessimistic=1,
		tblCalcTechnicalDriverAssumptionZiff.TFOptimistic=1
	FROM tblCalcTechnicalDriverAssumptionZiff
	WHERE tblCalcTechnicalDriverAssumptionZiff.Operation=1 AND tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@IdModel;
	
	--------------------------------------------------------------------------------------
	-- RUN FORECASTS
	--------------------------------------------------------------------------------------
	
	/** Technical Forecast **/
	EXEC calcModuleTechnicalForecast @IdModel,@intMinYear,@intMaxYear;
	
END
GO

/****** Object:  StoredProcedure [dbo].[calcModuleTechnicalForecast]    Script Date: 08/24/2015 10:27:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
---- ================================================================================================
---- Author:		Gareth Slater
---- Create date: 2014-04-20
---- Description:	Module Technical Forecast
---- ================================================================================================ 
---- Modifications:	
---- Date			Author	Description
---- ---------	------	----------------------------------------------------------------------
---- 2015-08-04	RM		Added local variables, made temporary tables in order to speed up execution. 
----						Added split of baselines when necessary
---- ================================================================================================ 
ALTER PROCEDURE [dbo].[calcModuleTechnicalForecast](
@IdModel int,
@MinYear int,
@MaxYear int
) WITH RECOMPILE 
AS
BEGIN
	--SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/** Declare local variables to reduce processing time **/
	DECLARE @LocIdModel INT = @IdModel
	DECLARE @LocMinYear INT = @MinYear
	DECLARE @LocMaxYear INT = @MaxYear
	
	/** Cleanup Calc Tables for New Data **/
	DELETE FROM tblCalcTechnicalSemifixedSelections WHERE [IdModel]=@LocIdModel;
	DELETE FROM tblCalcTechnicalSemifixedAmount WHERE [IdModel]=@LocIdModel;
	DELETE FROM tblCalcTechnicalSemifixedSumAmount WHERE [IdModel]=@LocIdModel;
	DELETE FROM tblCalcTechnicalSemifixedSplit WHERE [IdModel]=@LocIdModel;


	/** Cleanup Calc Tables for New Data **/
	IF OBJECT_ID('tempdb..#tblCalcProjectionTechnical') IS NOT NULL DROP TABLE #tblCalcProjectionTechnical
	IF OBJECT_ID('tempdb..#VolumePerField') IS NOT NULL DROP TABLE #VolumePerField
	IF OBJECT_ID('tempdb..#VolumeJoiner') IS NOT NULL DROP TABLE #VolumeJoiner
	
	/** Temporary table to store Projection data **/
	CREATE TABLE [dbo].[#tblCalcProjectionTechnical](
		[SourceDebug] [int] NULL,
		[IdModel] [int] NOT NULL,
		[IdActivity] [int] NULL,
		[CodeAct] [nvarchar](50) NULL,
		[Activity] [nvarchar](255) NULL,
		[IdResources] [int] NULL,
		[CodeRes] [nvarchar](50) NULL,
		[Resource] [nvarchar](255) NULL,
		[IdClientAccount] [int] NULL,
		[Account] [nvarchar](50) NULL,
		[NameAccount] [nvarchar](255) NULL,
		[IdField] [int] NOT NULL,
		[Field] [nvarchar](255) NULL,
		[IdClientCostCenter] [int] NULL,
		[ClientCostCenter] [nvarchar](255) NULL,
		[Amount] [float] NULL,
		[TypeAllocation] [int] NULL,
		[IdZiffAccount] [int] NULL,
		[CodeZiff] [nvarchar](50) NULL,
		[ZiffAccount] [nvarchar](255) NULL,
		[IdRootZiffAccount] [int] NULL,
		[RootCodeZiff] [nvarchar](50) NULL,
		[RootZiffAccount] [nvarchar](255) NULL,
		[IdProject] [int] NULL,
		[Project] [nvarchar](255) NULL,
		[ProjectionCriteria] [int] NULL,
		[Year] [int] NULL,
		[BaseYear] [bit] NULL,
		[TFNormal] [float] NULL,
		[TFPessimistic] [float] NULL,
		[TFOptimistic] [float] NULL,
		[BCRNormal] [float] NULL,
		[BCRPessimistic] [float] NULL,
		[BCROptimistic] [float] NULL,
		[BaseCostType] [int] NULL,
		[Operation] [int] NULL,
		[SortOrder] [int] NULL
	) ON [PRIMARY]
	
	/** Set Allowed Combinations for Semi-Fixed Assumptions (Shared) **/	
	INSERT INTO tblCalcTechnicalSemifixedSelections ([IdModel],[IdField],[IdZiffAccount],[IdTechnicalDriverSize],[TypeAllocation],[IdActivity],[IdResources],[IdClientAccount],[IdClientCostCenter])
	SELECT tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.IdZiffAccount, tblCalcTechnicalAssumption.IdTechnicalDriverSize, 
		tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.IdClientAccount, 
		tblCalcBaseCostByField.IdClientCostCenter
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalAssumption.IdModel AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalAssumption.IdZiffAccount INNER JOIN
		tblFields ON tblCalcBaseCostByField.IdField = tblFields.IdField AND tblCalcBaseCostByField.IdModel = tblFields.IdModel AND 
		tblCalcTechnicalAssumption.IdTypeOperation = tblFields.IdTypeOperation
	WHERE tblCalcTechnicalAssumption.ProjectionCriteria=2
	GROUP BY tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.TypeAllocation, 
		tblCalcTechnicalAssumption.IdTechnicalDriverSize, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.IdClientAccount, 
		tblCalcBaseCostByField.IdClientCostCenter
	HAVING tblCalcBaseCostByField.TypeAllocation=2 AND tblCalcBaseCostByField.IdModel=@LocIdModel;
	
	/** Set Allowed Combinations for Semi-Fixed Assumptions (Hierarchy) **/
	INSERT INTO tblCalcTechnicalSemifixedSelections ([IdModel],[IdField],[IdZiffAccount],[IdTechnicalDriverSize],[TypeAllocation],[IdActivity],[IdResources],[IdClientAccount],[IdClientCostCenter])
	SELECT tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.IdZiffAccount, tblCalcTechnicalAssumption.IdTechnicalDriverSize, 
		tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.IdClientAccount, 
		tblCalcBaseCostByField.IdClientCostCenter
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalAssumption.IdModel AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalAssumption.IdZiffAccount INNER JOIN
		tblFields ON tblCalcBaseCostByField.IdField = tblFields.IdField AND tblCalcBaseCostByField.IdModel = tblFields.IdModel AND 
		tblCalcTechnicalAssumption.IdTypeOperation = tblFields.IdTypeOperation
	WHERE tblCalcTechnicalAssumption.ProjectionCriteria=2 AND tblCalcBaseCostByField.BCRNormal<>0 AND tblCalcBaseCostByField.BCRPessimistic<>0 AND tblCalcBaseCostByField.BCROptimistic<>0
	GROUP BY tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.TypeAllocation, 
		tblCalcTechnicalAssumption.IdTechnicalDriverSize, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.IdClientAccount, 
		tblCalcBaseCostByField.IdClientCostCenter
	HAVING tblCalcBaseCostByField.TypeAllocation=3 AND tblCalcBaseCostByField.IdModel=@LocIdModel;

	/** Get Tech Driver Totals by Field for Semi-Fixed **/
	INSERT INTO tblCalcTechnicalSemifixedAmount ([IdModel],[IdField],[IdZiffAccount],[IdTechnicalDriverSize],[TypeAllocation],[IdActivity],[IdResources],[IdClientAccount],[IdClientCostCenter],[Year],[Normal],[Pessimistic],[Optimistic])
	SELECT tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalSemifixedSelections.IdField, tblCalcTechnicalSemifixedSelections.IdZiffAccount, 
		tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize, tblCalcTechnicalSemifixedSelections.TypeAllocation, tblCalcTechnicalSemifixedSelections.IdActivity, 
		tblCalcTechnicalSemifixedSelections.IdResources, tblCalcTechnicalSemifixedSelections.IdClientAccount, tblCalcTechnicalSemifixedSelections.IdClientCostCenter, 
		tblCalcTechnicalDriverData.Year, SUM(tblCalcTechnicalDriverData.Normal) AS Normal, SUM(tblCalcTechnicalDriverData.Pessimistic) AS Pessimistic, SUM(tblCalcTechnicalDriverData.Optimistic) 
		AS Optimistic
	FROM tblCalcTechnicalSemifixedSelections INNER JOIN
		tblCalcTechnicalDriverData ON tblCalcTechnicalSemifixedSelections.IdModel = tblCalcTechnicalDriverData.IdModel AND 
		tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize = tblCalcTechnicalDriverData.IdTechnicalDriver AND 
		tblCalcTechnicalSemifixedSelections.IdField = tblCalcTechnicalDriverData.IdField
	GROUP BY tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalSemifixedSelections.IdField, tblCalcTechnicalSemifixedSelections.IdZiffAccount, 
		tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize, tblCalcTechnicalSemifixedSelections.TypeAllocation, tblCalcTechnicalSemifixedSelections.IdActivity, 
		tblCalcTechnicalSemifixedSelections.IdResources, tblCalcTechnicalSemifixedSelections.IdClientAccount, tblCalcTechnicalSemifixedSelections.IdClientCostCenter, 
		tblCalcTechnicalDriverData.Year
	HAVING tblCalcTechnicalDriverData.Year<>@LocMinYear AND tblCalcTechnicalSemifixedSelections.IdModel=@LocIdModel;

	/** Get Tech Driver Totals by Set of Applicable Fields for Semi-Fixed **/
	INSERT INTO tblCalcTechnicalSemifixedSumAmount ([IdModel],[IdZiffAccount],[IdTechnicalDriverSize],[TypeAllocation],[IdActivity],[IdResources],[IdClientAccount],[IdClientCostCenter],[Year],[Normal],[Pessimistic],[Optimistic])
	SELECT tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalSemifixedSelections.IdZiffAccount, tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize, 
		tblCalcTechnicalSemifixedSelections.TypeAllocation, tblCalcTechnicalSemifixedSelections.IdActivity, tblCalcTechnicalSemifixedSelections.IdResources, 
		tblCalcTechnicalSemifixedSelections.IdClientAccount, tblCalcTechnicalSemifixedSelections.IdClientCostCenter, tblCalcTechnicalDriverData.Year, SUM(tblCalcTechnicalDriverData.Normal) 
		AS Normal, SUM(tblCalcTechnicalDriverData.Pessimistic) AS Pessimistic, SUM(tblCalcTechnicalDriverData.Optimistic) AS Optimistic
	FROM tblCalcTechnicalSemifixedSelections INNER JOIN
		tblCalcTechnicalDriverData ON tblCalcTechnicalSemifixedSelections.IdModel = tblCalcTechnicalDriverData.IdModel AND 
		tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize = tblCalcTechnicalDriverData.IdTechnicalDriver AND 
		tblCalcTechnicalSemifixedSelections.IdField = tblCalcTechnicalDriverData.IdField
	GROUP BY tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalSemifixedSelections.IdZiffAccount, tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize, 
		tblCalcTechnicalSemifixedSelections.TypeAllocation, tblCalcTechnicalSemifixedSelections.IdActivity, tblCalcTechnicalSemifixedSelections.IdResources, 
		tblCalcTechnicalSemifixedSelections.IdClientAccount, tblCalcTechnicalSemifixedSelections.IdClientCostCenter, tblCalcTechnicalDriverData.Year
	HAVING tblCalcTechnicalDriverData.Year<>@LocMinYear AND tblCalcTechnicalSemifixedSelections.IdModel=@LocIdModel;

	/** Set Tech Driver Splits for Semi-Fixed **/
	INSERT INTO tblCalcTechnicalSemifixedSplit ([IdModel],[IdField],[IdZiffAccount],[IdTechnicalDriverSize],[TypeAllocation],[IdActivity],[IdResources],[IdClientAccount],[IdClientCostCenter],[Year],[SplitNormal],[SplitPessimistic],[SplitOptimistic])
	SELECT tblCalcTechnicalSemifixedAmount.IdModel, tblCalcTechnicalSemifixedAmount.IdField, tblCalcTechnicalSemifixedAmount.IdZiffAccount, tblCalcTechnicalSemifixedAmount.IdTechnicalDriverSize, 
		tblCalcTechnicalSemifixedAmount.TypeAllocation, tblCalcTechnicalSemifixedAmount.IdActivity, tblCalcTechnicalSemifixedAmount.IdResources, tblCalcTechnicalSemifixedAmount.IdClientAccount, 
		tblCalcTechnicalSemifixedAmount.IdClientCostCenter, tblCalcTechnicalSemifixedAmount.Year, 
		CASE WHEN ISNULL(tblCalcTechnicalSemifixedSumAmount.Normal, 0) = 0 THEN 0 ELSE tblCalcTechnicalSemifixedAmount.Normal / tblCalcTechnicalSemifixedSumAmount.Normal END AS SplitNormal, 
		CASE WHEN ISNULL(tblCalcTechnicalSemifixedSumAmount.Pessimistic, 0) = 0 THEN 0 ELSE tblCalcTechnicalSemifixedAmount.Pessimistic / tblCalcTechnicalSemifixedSumAmount.Pessimistic END AS SplitPessimistic, 
		CASE WHEN ISNULL(tblCalcTechnicalSemifixedSumAmount.Optimistic, 0) = 0 THEN 0 ELSE tblCalcTechnicalSemifixedAmount.Optimistic / tblCalcTechnicalSemifixedSumAmount.Optimistic END AS SplitOptimistic
	FROM tblCalcTechnicalSemifixedAmount INNER JOIN
		tblCalcTechnicalSemifixedSumAmount ON tblCalcTechnicalSemifixedAmount.IdModel = tblCalcTechnicalSemifixedSumAmount.IdModel AND 
		tblCalcTechnicalSemifixedAmount.IdZiffAccount = tblCalcTechnicalSemifixedSumAmount.IdZiffAccount AND 
		tblCalcTechnicalSemifixedAmount.IdTechnicalDriverSize = tblCalcTechnicalSemifixedSumAmount.IdTechnicalDriverSize AND 
		tblCalcTechnicalSemifixedAmount.TypeAllocation = tblCalcTechnicalSemifixedSumAmount.TypeAllocation AND 
		tblCalcTechnicalSemifixedAmount.IdActivity = tblCalcTechnicalSemifixedSumAmount.IdActivity AND 
		tblCalcTechnicalSemifixedAmount.IdResources = tblCalcTechnicalSemifixedSumAmount.IdResources AND 
		tblCalcTechnicalSemifixedAmount.IdClientAccount = tblCalcTechnicalSemifixedSumAmount.IdClientAccount AND 
		tblCalcTechnicalSemifixedAmount.IdClientCostCenter = tblCalcTechnicalSemifixedSumAmount.IdClientCostCenter AND 
		tblCalcTechnicalSemifixedAmount.Year = tblCalcTechnicalSemifixedSumAmount.Year
	WHERE tblCalcTechnicalSemifixedAmount.Year<>@LocMinYear AND tblCalcTechnicalSemifixedAmount.IdModel=@LocIdModel;
	
	/** Update Ziff Semi-Fixed Forecast to Fixed **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET [ProjectionCriteria]=1 WHERE [ProjectionCriteria]=2 AND [IdModel]=@LocIdModel;
	
	/** (1) Insert Company Base Year Into #tblCalcProjectionTechnical **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 1, tblCalcBaseCostByField.IdModel,tblCalcBaseCostByField.IdActivity,tblCalcBaseCostByField.CodeAct,tblCalcBaseCostByField.Activity,tblCalcBaseCostByField.IdResources,tblCalcBaseCostByField.CodeRes,tblCalcBaseCostByField.Resource,tblCalcBaseCostByField.IdClientAccount,tblCalcBaseCostByField.Account,tblCalcBaseCostByField.NameAccount,tblCalcBaseCostByField.IdField,tblCalcBaseCostByField.Field,tblCalcBaseCostByField.IdClientCostCenter,tblCalcBaseCostByField.ClientCostCenter,tblCalcBaseCostByField.Amount,tblCalcBaseCostByField.TypeAllocation,tblCalcBaseCostByField.IdZiffAccount,tblCalcBaseCostByField.CodeZiff,tblCalcBaseCostByField.ZiffAccount,tblCalcBaseCostByField.IdRootZiffAccount,[RootCodeZiff],[RootZiffAccount],tblCalcTechnicalDriverAssumption.IdProject,tblCalcTechnicalDriverAssumption.ProjectionCriteria,tblCalcTechnicalDriverAssumption.Year,tblCalcTechnicalDriverAssumption.BaseYear,
		tblCalcTechnicalDriverAssumption.TFNormal,tblCalcTechnicalDriverAssumption.TFPessimistic,tblCalcTechnicalDriverAssumption.TFOptimistic,
		CASE WHEN tblCalcTechnicalDriverAssumption.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumption.Operation=1 THEN tblCalcBaseCostByField.BCRNormal * tblCalcBaseCostByField.TFNormal ELSE tblCalcBaseCostByField.BCRNormal END,
		CASE WHEN tblCalcTechnicalDriverAssumption.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumption.Operation=1 THEN tblCalcBaseCostByField.BCRPessimistic * tblCalcBaseCostByField.TFPessimistic ELSE tblCalcBaseCostByField.BCRPessimistic END,
		CASE WHEN tblCalcTechnicalDriverAssumption.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumption.Operation=1 THEN tblCalcBaseCostByField.BCROptimistic * tblCalcBaseCostByField.TFOptimistic ELSE tblCalcBaseCostByField.BCROptimistic END,
		1 AS BaseCostType,tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount
	WHERE tblCalcTechnicalDriverAssumption.BaseYear=1 AND tblCalcBaseCostByField.IdModel=@LocIdModel;

	/** (2) Insert Ziff Base Year Into #tblCalcProjectionTechnical (If Applicable) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 2, tblCalcBaseCostByFieldZiff.IdModel,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,tblCalcBaseCostByFieldZiff.IdField,tblCalcBaseCostByFieldZiff.Field,NULL,NULL,tblCalcBaseCostByFieldZiff.TotalCost,NULL,tblCalcBaseCostByFieldZiff.IdZiffAccount,tblCalcBaseCostByFieldZiff.CodeZiff,tblCalcBaseCostByFieldZiff.ZiffAccount,tblCalcBaseCostByFieldZiff.IdRootZiffAccount,[RootCodeZiff],[RootZiffAccount],tblCalcTechnicalDriverAssumptionZiff.IdProject,tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria,tblCalcTechnicalDriverAssumptionZiff.Year,tblCalcTechnicalDriverAssumptionZiff.BaseYear,
		tblCalcTechnicalDriverAssumptionZiff.TFNormal,tblCalcTechnicalDriverAssumptionZiff.TFPessimistic,tblCalcTechnicalDriverAssumptionZiff.TFOptimistic,
		CASE WHEN tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 THEN tblCalcBaseCostByFieldZiff.BCRNormal * tblCalcBaseCostByFieldZiff.TFNormal ELSE tblCalcBaseCostByFieldZiff.BCRNormal END,
		CASE WHEN tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 THEN tblCalcBaseCostByFieldZiff.BCRPessimistic * tblCalcBaseCostByFieldZiff.TFPessimistic ELSE tblCalcBaseCostByFieldZiff.BCRPessimistic END,
		CASE WHEN tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 THEN tblCalcBaseCostByFieldZiff.BCROptimistic * tblCalcBaseCostByFieldZiff.TFOptimistic ELSE tblCalcBaseCostByFieldZiff.BCROptimistic END,
		2 AS BaseCostType,tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount
	WHERE tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 
	AND tblCalcBaseCostByFieldZiff.IdModel=@LocIdModel;
	
	/** (3) Update Company Projection - Semi-Fixed (Direct Portion) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 3, tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.CodeAct, tblCalcBaseCostByField.Activity, tblCalcBaseCostByField.IdResources, 
		tblCalcBaseCostByField.CodeRes, tblCalcBaseCostByField.Resource, tblCalcBaseCostByField.IdClientAccount, tblCalcBaseCostByField.Account, tblCalcBaseCostByField.NameAccount, 
		tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.Field, tblCalcBaseCostByField.IdClientCostCenter, tblCalcBaseCostByField.ClientCostCenter, tblCalcBaseCostByField.Amount, 
		tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.CodeZiff, tblCalcBaseCostByField.ZiffAccount, 
		tblCalcBaseCostByField.IdRootZiffAccount, tblCalcBaseCostByField.RootCodeZiff, tblCalcBaseCostByField.RootZiffAccount, 0 AS IdProject, 2 AS ProjectionCriteria, tblCalcModelYears.Year, 
		CAST(0 AS BIT) AS BaseYear, tblCalcBaseCostByField.TFNormal, tblCalcBaseCostByField.TFPessimistic, tblCalcBaseCostByField.TFOptimistic, tblCalcBaseCostByField.BCRNormal, 
		tblCalcBaseCostByField.BCRPessimistic, tblCalcBaseCostByField.BCROptimistic, 1 AS BaseCostType, 1 AS Operation
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcModelYears ON tblCalcBaseCostByField.IdModel = tblCalcModelYears.IdModel INNER JOIN
		tblCalcTechnicalAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalAssumption.IdModel AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalAssumption.IdZiffAccount
	WHERE tblCalcBaseCostByField.TypeAllocation=1 AND tblCalcModelYears.Year<=@LocMaxYear AND tblCalcTechnicalAssumption.ProjectionCriteria=2 AND tblCalcBaseCostByField.IdModel=@LocIdModel;
	
	/** (4) Update Company Projection - Semi-Fixed (Shared) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 4, CBCSUM.IdModel, CBCSUM.IdActivity, CBCSUM.CodeAct, CBCSUM.Activity, CBCSUM.IdResources, CBCSUM.CodeRes, CBCSUM.Resource, CBCSUM.IdClientAccount, CBCSUM.Account, CBCSUM.NameAccount, 
		tblCalcTechnicalSemifixedSplit.IdField, NULL AS Field, CBCSUM.IdClientCostCenter, CBCSUM.ClientCostCenter, CBCSUM.Amount, CBCSUM.TypeAllocation, CBCSUM.IdZiffAccount, CBCSUM.CodeZiff, 
		CBCSUM.ZiffAccount, CBCSUM.IdRootZiffAccount, CBCSUM.RootCodeZiff, CBCSUM.RootZiffAccount, 0 AS IdProject, 2 AS ProjectionCriteria, tblCalcTechnicalSemifixedSplit.Year, 
		CAST(0 AS BIT) AS BaseYear, 1 AS TFNormal, 1 AS TFPessimistic, 1 AS TFOptimistic, CBCSUM.Amount * tblCalcTechnicalSemifixedSplit.SplitNormal AS BCRNormal, 
		CBCSUM.Amount * tblCalcTechnicalSemifixedSplit.SplitPessimistic AS BCRPessimistic, CBCSUM.Amount * tblCalcTechnicalSemifixedSplit.SplitOptimistic AS BCROptimistic,
		1 AS BaseCostType, 1 AS Operation
	FROM
	(
		SELECT IdModel, IdActivity, CodeAct, Activity, IdResources, CodeRes, Resource, IdClientAccount, Account, NameAccount, IdClientCostCenter, ClientCostCenter, SUM(Amount) AS Amount, TypeAllocation, 
			IdZiffAccount, CodeZiff, ZiffAccount, IdRootZiffAccount, RootCodeZiff, RootZiffAccount
		FROM tblCalcBaseCostByField
		GROUP BY IdModel, IdActivity, CodeAct, Activity, IdResources, CodeRes, Resource, IdClientAccount, Account, NameAccount, IdClientCostCenter, ClientCostCenter, TypeAllocation, IdZiffAccount, CodeZiff, 
			ZiffAccount, IdRootZiffAccount, RootCodeZiff, RootZiffAccount
		HAVING TypeAllocation<>1 AND IdModel=@LocIdModel
	) AS CBCSUM INNER JOIN
		tblCalcTechnicalSemifixedSplit ON CBCSUM.IdModel = tblCalcTechnicalSemifixedSplit.IdModel AND CBCSUM.IdActivity = tblCalcTechnicalSemifixedSplit.IdActivity AND 
		CBCSUM.IdResources = tblCalcTechnicalSemifixedSplit.IdResources AND CBCSUM.IdClientAccount = tblCalcTechnicalSemifixedSplit.IdClientAccount AND 
		CBCSUM.IdClientCostCenter = tblCalcTechnicalSemifixedSplit.IdClientCostCenter AND CBCSUM.IdZiffAccount = tblCalcTechnicalSemifixedSplit.IdZiffAccount AND 
		CBCSUM.TypeAllocation = tblCalcTechnicalSemifixedSplit.TypeAllocation;
	
	/** 1 - Fixed, 2 - SemiFixed, 3 - Variable, 4 - Cyclical, 5 - SemiVariable **/
	/** (6) Update Company Projection - Fixed **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT DISTINCT 6, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, 
		#tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, #tblCalcProjectionTechnical.NameAccount, 
		#tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, tblCalcTechnicalDriverAssumption.Year, CAST(0 AS BIT) AS BaseYear, 
		#tblCalcProjectionTechnical.TFNormal,#tblCalcProjectionTechnical.TFPessimistic,#tblCalcProjectionTechnical.TFOptimistic,#tblCalcProjectionTechnical.BCRNormal, #tblCalcProjectionTechnical.BCRPessimistic, 
		#tblCalcProjectionTechnical.BCROptimistic, 1 AS BaseCostType, tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		#tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumption.IdModel = #tblCalcProjectionTechnical.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = #tblCalcProjectionTechnical.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = #tblCalcProjectionTechnical.IdField
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=1) 
	AND #tblCalcProjectionTechnical.BaseYear=1 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND #tblCalcProjectionTechnical.BaseCostType=1 
	AND tblCalcTechnicalDriverAssumption.IdModel=@LocIdModel;
		
	/** (7) Update Ziff Projection - Fixed **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT DISTINCT 7, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, 
		#tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, #tblCalcProjectionTechnical.NameAccount, 
		#tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.Year, CAST(0 AS BIT) AS BaseYear, 
		#tblCalcProjectionTechnical.TFNormal, #tblCalcProjectionTechnical.TFPessimistic, #tblCalcProjectionTechnical.TFOptimistic, 
		#tblCalcProjectionTechnical.BCRNormal, #tblCalcProjectionTechnical.BCRPessimistic, #tblCalcProjectionTechnical.BCROptimistic, 2 AS BaseCostType, tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		#tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumptionZiff.IdModel = #tblCalcProjectionTechnical.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = #tblCalcProjectionTechnical.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = #tblCalcProjectionTechnical.IdField
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=1) 
	AND #tblCalcProjectionTechnical.BaseYear=1 
	AND #tblCalcProjectionTechnical.BaseCostType=2 
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0 
	AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@LocIdModel;
		
	/** (8) Update Company Projection - Variable and Cyclical **/
	/** Baselines **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 8, tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.CodeAct, tblCalcBaseCostByField.Activity, 
		tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.CodeRes, tblCalcBaseCostByField.Resource, 
		tblCalcBaseCostByField.IdClientAccount, tblCalcBaseCostByField.Account, tblCalcBaseCostByField.NameAccount, tblCalcBaseCostByField.IdField, 
		tblCalcBaseCostByField.Field, tblCalcBaseCostByField.IdClientCostCenter, tblCalcBaseCostByField.ClientCostCenter, 
		tblCalcBaseCostByField.Amount, tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.CodeZiff, 
		tblCalcBaseCostByField.ZiffAccount, tblCalcBaseCostByField.IdRootZiffAccount, tblCalcBaseCostByField.RootCodeZiff, 
		tblCalcBaseCostByField.RootZiffAccount, tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumption.Year, tblCalcTechnicalDriverAssumption.BaseYear, 
		tblCalcTechnicalDriverAssumption.TFNormal,tblCalcTechnicalDriverAssumption.TFPessimistic,tblCalcTechnicalDriverAssumption.TFOptimistic, 
		tblCalcBaseCostByField.BCRNormal, tblCalcBaseCostByField.BCRPessimistic, tblCalcBaseCostByField.BCROptimistic, 1 AS BaseCostType, 
		tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=3 OR tblCalcTechnicalDriverAssumption.ProjectionCriteria=4) 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND tblCalcBaseCostByField.IdModel=@LocIdModel
	AND tblCalcTechnicalDriverAssumption.Operation = 1;
	
	/** (9) Business Opportunities **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 9, tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.CodeAct, tblCalcBaseCostByField.Activity, 
		tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.CodeRes, tblCalcBaseCostByField.Resource, 
		tblCalcBaseCostByField.IdClientAccount, tblCalcBaseCostByField.Account, tblCalcBaseCostByField.NameAccount, tblCalcBaseCostByField.IdField, 
		tblCalcBaseCostByField.Field, tblCalcBaseCostByField.IdClientCostCenter, tblCalcBaseCostByField.ClientCostCenter, 
		tblCalcBaseCostByField.Amount, tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.CodeZiff, 
		tblCalcBaseCostByField.ZiffAccount, tblCalcBaseCostByField.IdRootZiffAccount, tblCalcBaseCostByField.RootCodeZiff, 
		tblCalcBaseCostByField.RootZiffAccount, tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumption.Year, tblCalcTechnicalDriverAssumption.BaseYear, 
		tblCalcTechnicalDriverAssumption.TFNormal, tblCalcTechnicalDriverAssumption.TFPessimistic, tblCalcTechnicalDriverAssumption.TFOptimistic, 
		tblCalcBaseCostByField.BCRNormal, tblCalcBaseCostByField.BCRPessimistic, tblCalcBaseCostByField.BCROptimistic, 1 AS BaseCostType, 
		tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=3 OR tblCalcTechnicalDriverAssumption.ProjectionCriteria=4) 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND tblCalcBaseCostByField.IdModel=@LocIdModel
	AND tblCalcTechnicalDriverAssumption.Operation <> 1;

	/** (10) Update Ziff Projection - Variable and Cyclical **/
	/** Baselines **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 10, tblCalcBaseCostByFieldZiff.IdModel, NULL, NULL, NULL, NULL, NULL, NULL, 
		NULL, NULL, NULL, tblCalcBaseCostByFieldZiff.IdField, tblCalcBaseCostByFieldZiff.Field, NULL, NULL, 
		tblCalcBaseCostByFieldZiff.TotalCost, NULL, tblCalcBaseCostByFieldZiff.IdZiffAccount, tblCalcBaseCostByFieldZiff.CodeZiff, 
		tblCalcBaseCostByFieldZiff.ZiffAccount, tblCalcBaseCostByFieldZiff.IdRootZiffAccount, tblCalcBaseCostByFieldZiff.RootCodeZiff, 
		tblCalcBaseCostByFieldZiff.RootZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumptionZiff.Year, tblCalcTechnicalDriverAssumptionZiff.BaseYear, tblCalcBaseCostByFieldZiff.TFNormal,tblCalcBaseCostByFieldZiff.TFPessimistic,tblCalcBaseCostByFieldZiff.TFOptimistic, 
		tblCalcBaseCostByFieldZiff.BCRNormal, tblCalcBaseCostByFieldZiff.BCRPessimistic, tblCalcBaseCostByFieldZiff.BCROptimistic, 2 AS BaseCostType, 
		tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=3 OR tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=4) 
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0 
	AND tblCalcBaseCostByFieldZiff.IdModel=@LocIdModel
	AND tblCalcTechnicalDriverAssumptionZiff.Operation=1;

	/** (11) Business Opportunities **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 11, tblCalcBaseCostByFieldZiff.IdModel, NULL, NULL, NULL, NULL, NULL, NULL, 
		NULL, NULL, NULL, tblCalcBaseCostByFieldZiff.IdField, tblCalcBaseCostByFieldZiff.Field, NULL, NULL, 
		tblCalcBaseCostByFieldZiff.TotalCost, NULL, tblCalcBaseCostByFieldZiff.IdZiffAccount, tblCalcBaseCostByFieldZiff.CodeZiff, 
		tblCalcBaseCostByFieldZiff.ZiffAccount, tblCalcBaseCostByFieldZiff.IdRootZiffAccount, tblCalcBaseCostByFieldZiff.RootCodeZiff, 
		tblCalcBaseCostByFieldZiff.RootZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumptionZiff.Year, tblCalcTechnicalDriverAssumptionZiff.BaseYear, tblCalcTechnicalDriverAssumptionZiff.TFNormal, 
		tblCalcTechnicalDriverAssumptionZiff.TFPessimistic, tblCalcTechnicalDriverAssumptionZiff.TFOptimistic, tblCalcBaseCostByFieldZiff.BCRNormal, 
		tblCalcBaseCostByFieldZiff.BCRPessimistic, tblCalcBaseCostByFieldZiff.BCROptimistic, 2 AS BaseCostType, 
		tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=3 OR tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=4)
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0 AND tblCalcBaseCostByFieldZiff.IdModel=@LocIdModel
	AND tblCalcTechnicalDriverAssumptionZiff.Operation<>1;

	/** (12) Update Company Projection - SemiVariable (BaseLine) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT DISTINCT 12, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, 
		#tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, #tblCalcProjectionTechnical.NameAccount, 
		#tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, tblCalcTechnicalDriverAssumption.Year, CAST(0 AS BIT) AS BaseYear, 
		#tblCalcProjectionTechnical.TFNormal, #tblCalcProjectionTechnical.TFPessimistic, #tblCalcProjectionTechnical.TFOptimistic, 
		#tblCalcProjectionTechnical.BCRNormal, #tblCalcProjectionTechnical.BCRPessimistic, #tblCalcProjectionTechnical.BCROptimistic, 1 AS BaseCostType, tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		#tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumption.IdModel = #tblCalcProjectionTechnical.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = #tblCalcProjectionTechnical.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = #tblCalcProjectionTechnical.IdField AND
		tblCalcTechnicalDriverAssumption.IdProject = #tblCalcProjectionTechnical.IdProject
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=5) 
	AND #tblCalcProjectionTechnical.BaseYear=1 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND #tblCalcProjectionTechnical.BaseCostType=1 
	AND #tblCalcProjectionTechnical.Operation=1
	AND tblCalcTechnicalDriverAssumption.IdModel=@LocIdModel;

	/** (13) Update Ziff Projection - SemiVariable (BaseLine) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT DISTINCT 13, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, 
		#tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, #tblCalcProjectionTechnical.NameAccount, 
		#tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.Year, CAST(0 AS BIT) AS BaseYear, 
		0,0,0,0,0,0, 2 AS BaseCostType, tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		#tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumptionZiff.IdModel = #tblCalcProjectionTechnical.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = #tblCalcProjectionTechnical.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = #tblCalcProjectionTechnical.IdField		
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5) 
	AND #tblCalcProjectionTechnical.BaseYear=1 
	AND #tblCalcProjectionTechnical.BaseCostType=2 
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0
	AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 
	AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@LocIdModel;

	/** (14) Update Company Projection - SemiVariable (Business Opportunity) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 14, tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.CodeAct, tblCalcBaseCostByField.Activity, 
		tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.CodeRes, tblCalcBaseCostByField.Resource, 
		tblCalcBaseCostByField.IdClientAccount, tblCalcBaseCostByField.Account, tblCalcBaseCostByField.NameAccount, tblCalcBaseCostByField.IdField, 
		tblCalcBaseCostByField.Field, tblCalcBaseCostByField.IdClientCostCenter, tblCalcBaseCostByField.ClientCostCenter, 
		tblCalcBaseCostByField.Amount, tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.CodeZiff, 
		tblCalcBaseCostByField.ZiffAccount, tblCalcBaseCostByField.IdRootZiffAccount, tblCalcBaseCostByField.RootCodeZiff, 
		tblCalcBaseCostByField.RootZiffAccount, tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumption.Year, tblCalcTechnicalDriverAssumption.BaseYear, 
		tblCalcTechnicalDriverAssumption.TFNormal, tblCalcTechnicalDriverAssumption.TFPessimistic, tblCalcTechnicalDriverAssumption.TFOptimistic, 
		tblCalcBaseCostByField.BCRNormal, tblCalcBaseCostByField.BCRPessimistic,tblCalcBaseCostByField.BCROptimistic, 
		1 AS BaseCostType, 
		tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=5) 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND tblCalcTechnicalDriverAssumption.Operation=2
	AND tblCalcBaseCostByField.IdModel=@LocIdModel;

	/** (15) Update Ziff Projection - SemiVariable (Business Opportunity)  **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 15, tblCalcBaseCostByFieldZiff.IdModel, NULL, NULL, NULL, NULL, NULL, NULL, 
		NULL, NULL, NULL, tblCalcBaseCostByFieldZiff.IdField, tblCalcBaseCostByFieldZiff.Field, NULL, NULL, 
		tblCalcBaseCostByFieldZiff.TotalCost, NULL, tblCalcBaseCostByFieldZiff.IdZiffAccount, tblCalcBaseCostByFieldZiff.CodeZiff, 
		tblCalcBaseCostByFieldZiff.ZiffAccount, tblCalcBaseCostByFieldZiff.IdRootZiffAccount, tblCalcBaseCostByFieldZiff.RootCodeZiff, 
		tblCalcBaseCostByFieldZiff.RootZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumptionZiff.Year, tblCalcTechnicalDriverAssumptionZiff.BaseYear, 
		tblCalcTechnicalDriverAssumptionZiff.TFNormal, tblCalcTechnicalDriverAssumptionZiff.TFPessimistic, tblCalcTechnicalDriverAssumptionZiff.TFOptimistic, 
		tblCalcBaseCostByFieldZiff.BCRNormal, tblCalcBaseCostByFieldZiff.BCRPessimistic, tblCalcBaseCostByFieldZiff.BCROptimistic, 
		2 AS BaseCostType, 
		tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5) 
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0 
	AND tblCalcTechnicalDriverAssumptionZiff.Operation=2
	AND tblCalcBaseCostByFieldZiff.IdModel=@LocIdModel;

	/** Volume preparation tables **/
	/** Get Base Year **/
	DECLARE @BaseYear INT
	SELECT @BaseYear = [BaseYear] FROM [DBCPM].[dbo].[tblModels] WHERE IdModel=@LocIdModel

	/** Volume per driver **/
	CREATE TABLE #VolumePerField ([IdModel] INT, [IdTechnicalDriver] INT, [IdField] INT, [DriverName] NVARCHAR(255), [Year] INT, [NormalAmount] FLOAT, [PessimisticAmount] FLOAT, [OptimisticAmount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumePerField ([IdModel], [IdTechnicalDriver], [IdField], [DriverName], [Year], [NormalAmount], [PessimisticAmount], [OptimisticAmount])
	SELECT tblTechnicalDrivers.IdModel, tblTechnicalDrivers.IdTechnicalDriver, tblTechnicalDriverData.IdField, tblTechnicalDrivers.Name AS DriverName, tblTechnicalDriverData.Year, SUM(tblTechnicalDriverData.Normal), SUM(tblTechnicalDriverData.Pessimistic), SUM(tblTechnicalDriverData.Optimistic)
	FROM tblTechnicalDrivers INNER JOIN tblTechnicalDriverData ON tblTechnicalDriverData.IdModel=tblTechnicalDrivers.IdModel AND tblTechnicalDriverData.IdTechnicalDriver=tblTechnicalDrivers.IdTechnicalDriver
	WHERE tblTechnicalDrivers.IdModel=@LocIdModel
	GROUP BY  tblTechnicalDrivers.IdModel, tblTechnicalDrivers.IdTechnicalDriver, tblTechnicalDriverData.IdField, tblTechnicalDrivers.Name, tblTechnicalDriverData.Year
	ORDER BY 2,3,5

	/** Volume joiner **/
	CREATE TABLE #VolumeJoiner ([IdModel] INT, [IdTechnicalDriver] INT, [IdField] INT, [Year] INT, [IdZiffAccount] INT) ON [PRIMARY]
	INSERT INTO #VolumeJoiner ([IdModel], [IdTechnicalDriver], [IdField], [Year], [IdZiffAccount])
	SELECT #tblCalcProjectionTechnical.IdModel, tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize, #tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Year, #tblCalcProjectionTechnical.IdZiffAccount
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
         #tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumption.IdModel = #tblCalcProjectionTechnical.IdModel AND 
         tblCalcTechnicalDriverAssumption.IdField = #tblCalcProjectionTechnical.IdField AND tblCalcTechnicalDriverAssumption.Year = #tblCalcProjectionTechnical.Year AND 
         tblCalcTechnicalDriverAssumption.IdZiffAccount = #tblCalcProjectionTechnical.IdZiffAccount
	WHERE (#tblCalcProjectionTechnical.ProjectionCriteria = 5) AND (#tblCalcProjectionTechnical.Operation = 2)
	GROUP BY #tblCalcProjectionTechnical.IdModel, tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize, #tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Year, #tblCalcProjectionTechnical.IdZiffAccount
	HAVING (#tblCalcProjectionTechnical.IdModel = @LocIdModel)
    ORDER BY 2,3,4

	/** Update the SemiVariable Factor of Business Oportunites depending on Volume versus Driver Capacity**/
	--UPDATE CPT
	--SET CPT.TFNormal =	CASE WHEN #VolumePerField.NormalAmount > VPF.BaseNormalAmount THEN CPT.TFNormal ELSE 0 END, 
	--	CPT.TFPessimistic = CASE WHEN #VolumePerField.PessimisticAmount > VPF.BasePessimisticAmount THEN CPT.TFPessimistic ELSE 0 END, 
	--	CPT.TFOptimistic = CASE WHEN #VolumePerField.OptimisticAmount > VPF.BaseOptimisticAmount THEN CPT.TFOptimistic ELSE 0 END 
	--from #tblCalcProjectionTechnical CPT INNER JOIN 
	--	#VolumeJoiner ON #VolumeJoiner.IdModel = CPT.IdModel AND
	--	#VolumeJoiner.IdField = CPT.IdField AND
	--	#VolumeJoiner.Year = CPT.Year AND
	--	#VolumeJoiner.IdZiffAccount = CPT.IdZiffAccount INNER JOIN
	--	#VolumePerField ON #VolumePerField.IdModel = #VolumeJoiner.IdModel AND
	--	#VolumePerField.IdField = #VolumeJoiner.IdField AND
	--	#VolumePerField.Year = #VolumeJoiner.Year AND
	--	#VolumePerField.IdTechnicalDriver = #VolumeJoiner.IdTechnicalDriver INNER JOIN
	--	(
	--	SELECT IdField, IdTechnicalDriver, NormalAmount AS BaseNormalAmount, PessimisticAmount AS BasePessimisticAmount, OptimisticAmount AS BaseOptimisticAmount 
	--	FROM #VolumePerField 
	--	WHERE IdModel = @LocIdModel
	--	AND Year = @BaseYear
	--	) AS VPF ON
	--	 VPF.IdTechnicalDriver=#VolumeJoiner.IdTechnicalDriver AND
	--	 VPF.IdField=#VolumeJoiner.IdField 
	--WHERE CPT.ProjectionCriteria=5 
	--and CPT.Operation=2

	UPDATE CPT
	SET CPT.TFNormal =	CASE WHEN #VolumePerField.NormalAmount > VPF.BaseNormalAmount/(A.UtilizationValue/100)  THEN CPT.TFNormal ELSE 0 END, 
		CPT.TFPessimistic = CASE WHEN #VolumePerField.PessimisticAmount > VPF.BasePessimisticAmount/(1/A.UtilizationValue) THEN CPT.TFPessimistic ELSE 0 END, 
		CPT.TFOptimistic = CASE WHEN #VolumePerField.OptimisticAmount > VPF.BaseOptimisticAmount/(1/A.UtilizationValue) THEN CPT.TFOptimistic ELSE 0 END 
	FROM #tblCalcProjectionTechnical CPT INNER JOIN
		 #VolumeJoiner ON #VolumeJoiner.IdModel = CPT.IdModel AND #VolumeJoiner.IdField = CPT.IdField AND #VolumeJoiner.Year = CPT.Year AND 
		 #VolumeJoiner.IdZiffAccount = CPT.IdZiffAccount INNER JOIN
		 #VolumePerField ON #VolumePerField.IdModel = #VolumeJoiner.IdModel AND #VolumePerField.IdField = #VolumeJoiner.IdField AND 
		 #VolumePerField.Year = #VolumeJoiner.Year AND #VolumePerField.IdTechnicalDriver = #VolumeJoiner.IdTechnicalDriver  INNER JOIN
		(
		SELECT     IdField, IdTechnicalDriver, NormalAmount AS BaseNormalAmount, PessimisticAmount AS BasePessimisticAmount, OptimisticAmount AS BaseOptimisticAmount
		FROM         #VolumePerField
		WHERE     (IdModel = @LocIdModel) AND (Year = @BaseYear)
		) AS VPF ON
		 VPF.IdTechnicalDriver=#VolumeJoiner.IdTechnicalDriver AND
		 VPF.IdField=#VolumeJoiner.IdField  INNER JOIN
		 tblUtilizationCapacity A ON A.IdModel = #VolumeJoiner.IdModel AND A.IdField = #VolumeJoiner.IdField AND 
		 A.IdZiffAccount = #VolumeJoiner.IdZiffAccount INNER JOIN
		 tblFields ON A.IdField = tblFields.IdField
	WHERE     (CPT.ProjectionCriteria=5) AND (CPT.Operation = 2)

	/** Apply denominator if applicable **/
	/** Create temporary table to house changes **/
	IF OBJECT_ID('tempdb..#HousingDataTable') IS NOT NULL DROP TABLE #HousingDataTable
	IF OBJECT_ID('tempdb..#HousingDataTableZiff') IS NOT NULL DROP TABLE #HousingDataTableZiff
	SELECT 
		#tblCalcProjectionTechnical.SourceDebug, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, #tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, 
		#tblCalcProjectionTechnical.NameAccount, #tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		TDUCD.IdProject, #tblCalcProjectionTechnical.ProjectionCriteria, #tblCalcProjectionTechnical.Year, #tblCalcProjectionTechnical.BaseYear, 
		#tblCalcProjectionTechnical.TFNormal * TDUCD.TFNormal AS TFNormal,
		#tblCalcProjectionTechnical.TFPessimistic * TDUCD.TFPessimistic AS TFPessimistic,
		#tblCalcProjectionTechnical.TFOptimistic * TDUCD.TFOptimistic AS TFOptimistic,
		#tblCalcProjectionTechnical.BCRNormal, #tblCalcProjectionTechnical.BCRPessimistic, 
		#tblCalcProjectionTechnical.BCROptimistic, #tblCalcProjectionTechnical.BaseCostType, #tblCalcProjectionTechnical.Operation,#tblCalcProjectionTechnical.SortOrder
	INTO #HousingDataTable	
	FROM #tblCalcProjectionTechnical INNER JOIN
		(
		SELECT tblCalcUnitCostDenominatorAmount.IdModel, tblCalcUnitCostDenominatorAmount.IdField, tblCalcUnitCostDenominatorAmount.IdProject, 
		tblCalcUnitCostDenominatorAmount.Year, 
		CASE WHEN ISNULL(tblCalcBaselineUnitCostDenominatorSumAmount.SumNormal, 0) = 0 THEN 0 ELSE tblCalcUnitCostDenominatorAmount.SumNormal / tblCalcBaselineUnitCostDenominatorSumAmount.SumNormal END AS TFNormal, 
		CASE WHEN ISNULL(tblCalcBaselineUnitCostDenominatorSumAmount.SumPessimistic, 0) = 0 THEN 0 ELSE tblCalcUnitCostDenominatorAmount.SumPessimistic / tblCalcBaselineUnitCostDenominatorSumAmount.SumPessimistic END AS TFPessimistic, 
		CASE WHEN ISNULL(tblCalcBaselineUnitCostDenominatorSumAmount.SumOptimistic, 0) = 0 THEN 0 ELSE tblCalcUnitCostDenominatorAmount.SumOptimistic / tblCalcBaselineUnitCostDenominatorSumAmount.SumOptimistic END AS TFOptimistic
		FROM tblCalcUnitCostDenominatorAmount INNER JOIN
		tblCalcBaselineUnitCostDenominatorSumAmount ON 
		tblCalcUnitCostDenominatorAmount.IdModel = tblCalcBaselineUnitCostDenominatorSumAmount.IdModel AND 
		tblCalcUnitCostDenominatorAmount.IdField = tblCalcBaselineUnitCostDenominatorSumAmount.IdField AND 
		tblCalcUnitCostDenominatorAmount.Year = tblCalcBaselineUnitCostDenominatorSumAmount.Year RIGHT OUTER JOIN 
		tblProjects  ON tblCalcUnitCostDenominatorAmount.IdModel = tblProjects.IdModel AND
		tblCalcUnitCostDenominatorAmount.IdField = tblProjects.IdField AND 
		tblCalcUnitCostDenominatorAmount.IdProject = tblProjects.IdProject
		WHERE tblProjects.Operation=1
		AND tblCalcUnitCostDenominatorAmount.IdModel=@LocIdModel) AS TDUCD ON 
		#tblCalcProjectionTechnical.IdModel = TDUCD.IdModel AND #tblCalcProjectionTechnical.IdField = TDUCD.IdField AND #tblCalcProjectionTechnical.Year = TDUCD.Year
	WHERE #tblCalcProjectionTechnical.IdModel=@LocIdModel AND #tblCalcProjectionTechnical.BaseCostType = 1 AND #tblCalcProjectionTechnical.Operation = 1;

	SELECT 
		#tblCalcProjectionTechnical.SourceDebug, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, #tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, 
		#tblCalcProjectionTechnical.NameAccount, #tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		TDUCD.IdProject, #tblCalcProjectionTechnical.ProjectionCriteria, #tblCalcProjectionTechnical.Year, #tblCalcProjectionTechnical.BaseYear, 
		#tblCalcProjectionTechnical.TFNormal * TDUCD.TFNormal AS TFNormal,
		#tblCalcProjectionTechnical.TFPessimistic * TDUCD.TFPessimistic AS TFPessimistic,
		#tblCalcProjectionTechnical.TFOptimistic * TDUCD.TFOptimistic AS TFOptimistic,
		#tblCalcProjectionTechnical.BCRNormal, #tblCalcProjectionTechnical.BCRPessimistic, 
		#tblCalcProjectionTechnical.BCROptimistic, #tblCalcProjectionTechnical.BaseCostType, #tblCalcProjectionTechnical.Operation,#tblCalcProjectionTechnical.SortOrder
	INTO #HousingDataTableZiff	
	FROM #tblCalcProjectionTechnical INNER JOIN
		(
		SELECT tblCalcUnitCostDenominatorAmount.IdModel, tblCalcUnitCostDenominatorAmount.IdField, tblCalcUnitCostDenominatorAmount.IdProject, 
		tblCalcUnitCostDenominatorAmount.Year, 
		CASE WHEN ISNULL(tblCalcBaselineUnitCostDenominatorSumAmount.SumNormal, 0) = 0 THEN 0 ELSE tblCalcUnitCostDenominatorAmount.SumNormal / tblCalcBaselineUnitCostDenominatorSumAmount.SumNormal END AS TFNormal, 
		CASE WHEN ISNULL(tblCalcBaselineUnitCostDenominatorSumAmount.SumPessimistic, 0) = 0 THEN 0 ELSE tblCalcUnitCostDenominatorAmount.SumPessimistic / tblCalcBaselineUnitCostDenominatorSumAmount.SumPessimistic END AS TFPessimistic, 
		CASE WHEN ISNULL(tblCalcBaselineUnitCostDenominatorSumAmount.SumOptimistic, 0) = 0 THEN 0 ELSE tblCalcUnitCostDenominatorAmount.SumOptimistic / tblCalcBaselineUnitCostDenominatorSumAmount.SumOptimistic END AS TFOptimistic
		FROM tblCalcUnitCostDenominatorAmount INNER JOIN
		tblCalcBaselineUnitCostDenominatorSumAmount ON 
		tblCalcUnitCostDenominatorAmount.IdModel = tblCalcBaselineUnitCostDenominatorSumAmount.IdModel AND 
		tblCalcUnitCostDenominatorAmount.IdField = tblCalcBaselineUnitCostDenominatorSumAmount.IdField AND 
		tblCalcUnitCostDenominatorAmount.Year = tblCalcBaselineUnitCostDenominatorSumAmount.Year RIGHT OUTER JOIN 
		tblProjects  ON tblCalcUnitCostDenominatorAmount.IdModel = tblProjects.IdModel AND
		tblCalcUnitCostDenominatorAmount.IdField = tblProjects.IdField AND 
		tblCalcUnitCostDenominatorAmount.IdProject = tblProjects.IdProject
		WHERE tblProjects.Operation=1
		AND tblCalcUnitCostDenominatorAmount.IdModel=@LocIdModel) AS TDUCD ON 
		#tblCalcProjectionTechnical.IdModel = TDUCD.IdModel AND #tblCalcProjectionTechnical.IdField = TDUCD.IdField AND #tblCalcProjectionTechnical.Year = TDUCD.Year
	WHERE #tblCalcProjectionTechnical.IdModel=@LocIdModel AND #tblCalcProjectionTechnical.BaseCostType = 2 AND #tblCalcProjectionTechnical.Operation = 1;

	/** Delete all data prior to Base Year for multiple baselines **/
	DELETE  CPT
	FROM #tblCalcProjectionTechnical CPT INNER JOIN tblProjects ON tblProjects.IdModel = CPT.IdModel 
	WHERE CPT.IdModel = @LocIdModel
	AND tblProjects.Operation=1
	AND CPT.BaseYear=0
	AND CPT.Year = tblProjects.Starts

	/** Delete data from #tblCalcProjectionTechnical **/
	DELETE FROM #tblCalcProjectionTechnical WHERE IdModel=@LocIdModel AND IdProject=0

	/** Insert data from #HousingDataTable into #tblCalcProjectionTechnical  for that field**/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[SortOrder])
	SELECT SourceDebug, IdModel, IdActivity, CodeAct, Activity,IdResources, CodeRes, Resource, IdClientAccount, Account, NameAccount, IdField, Field, IdClientCostCenter, 
		ClientCostCenter, Amount, TypeAllocation, IdZiffAccount, CodeZiff, ZiffAccount, IdRootZiffAccount, RootCodeZiff, RootZiffAccount, IdProject, ProjectionCriteria, Year, BaseYear, 
		TFNormal, TFPessimistic, TFOptimistic,BCRNormal, BCRPessimistic, BCROptimistic, BaseCostType, Operation,SortOrder
	FROM #HousingDataTable	

	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[SortOrder])
	SELECT SourceDebug, IdModel, IdActivity, CodeAct, Activity,IdResources, CodeRes, Resource, IdClientAccount, Account, NameAccount, IdField, Field, IdClientCostCenter, 
		ClientCostCenter, Amount, TypeAllocation, IdZiffAccount, CodeZiff, ZiffAccount, IdRootZiffAccount, RootCodeZiff, RootZiffAccount, IdProject, ProjectionCriteria, Year, BaseYear, 
		TFNormal, TFPessimistic, TFOptimistic,BCRNormal, BCRPessimistic, BCROptimistic, BaseCostType, Operation,SortOrder
	FROM #HousingDataTableZiff	

	DELETE FROM #tblCalcProjectionTechnical WHERE IdModel=@LocIdModel AND BCRNormal =0 AND BCROptimistic=0 AND BCRPessimistic=0 
	DELETE FROM #tblCalcProjectionTechnical WHERE IdModel=@LocIdModel AND TFNormal=0 AND TFOptimistic=0 AND TFNormal=0
	
	/** Prep for Reporting **/
	UPDATE #tblCalcProjectionTechnical SET [Activity]='Ziff/Third-party Costs',[SortOrder]=2 WHERE [Activity] IS NULL AND [BaseCostType]=2 AND [IdModel]=@LocIdModel;
	UPDATE #tblCalcProjectionTechnical SET [Resource]='Ziff/Third-party Costs',[SortOrder]=2 WHERE [Resource] IS NULL AND [BaseCostType]=2 AND [IdModel]=@LocIdModel;
	UPDATE #tblCalcProjectionTechnical SET [Project]=tblProjects.Name, [Operation]=tblProjects.Operation FROM #tblCalcProjectionTechnical INNER JOIN tblProjects ON #tblCalcProjectionTechnical.IdProject = tblProjects.IdProject AND #tblCalcProjectionTechnical.IdModel = tblProjects.IdModel WHERE #tblCalcProjectionTechnical.IdModel=@LocIdModel;
	UPDATE #tblCalcProjectionTechnical SET [Project]='Unknown Project' WHERE [Project] IS NULL AND [IdProject] IS NOT NULL AND [IdModel]=@LocIdModel;
	UPDATE #tblCalcProjectionTechnical SET [Field]=tblFields.Name FROM #tblCalcProjectionTechnical INNER JOIN tblFields ON #tblCalcProjectionTechnical.IdField = tblFields.IdField AND #tblCalcProjectionTechnical.IdModel = tblFields.IdModel WHERE #tblCalcProjectionTechnical.IdModel=@LocIdModel;

	----/** Move data from temp table tblCalcProjectionTecnical to permanent table #tblCalcProjectionTechnical **/
	DELETE FROM tblCalcProjectionTechnical where IdModel=@LocIdModel
	INSERT INTO tblCalcProjectionTechnical 
	SELECT DISTINCT * FROM #tblCalcProjectionTechnical

	/** Clean up temporary tables **/
	DROP TABLE #tblCalcProjectionTechnical
	DROP TABLE #VolumePerField
	DROP TABLE #VolumeJoiner

END


GO

/****** Object:  StoredProcedure [dbo].[tecpUpdateUtilizationCapacity]    Script Date: 2015-08-20 9:35:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------
 Author			: Rodrigo Manubens
 Create date	: 2015-03-19
 Description	: Update Utilization Capacity Data
--------------------------------------------*/
ALTER PROC [dbo].[tecpUpdateUtilizationCapacity](
@IdUtilization int,
@IdModel int,
@UtilizationValue float,
@UserModification int,
@DateModification datetime)
AS 
BEGIN
  UPDATE tblUtilizationCapacity 
  SET UtilizationValue=@UtilizationValue, UserModification=@UserModification, DateModification=@DateModification
  WHERE [IdUtilization] = @IdUtilization AND [IdModel] = @IdModel
END

GO

/****** Object:  StoredProcedure [dbo].[allpListCostStructureAssumptionsByProject]    Script Date: 2015-08-20 9:36:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-06-11
-- Description:	List Cost Structure Assumptions By Project as the view will no longer work as we are adding a row to display the parent
-- ========================================================================================================================================
-- 2015-07-27	Added the Case name to the query as it will be displayed in the Cost Structure Assumptions page of the tool
-- ========================================================================================================================================

ALTER PROCEDURE [dbo].[allpListCostStructureAssumptionsByProject](
 @IdModel int,
 @IdField int
)
AS
BEGIN
	
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#CostStructureAssumptions') IS NOT NULL DROP TABLE #CostStructureAssumptions

	CREATE TABLE #CostStructureAssumptions (IdModel INT, IdCostStructureAssumptions INT, IdProject INT, ProjectName NVARCHAR(255), IdZiffAccount INT, Code NVARCHAR(50), Name NVARCHAR(255), ParentCode NVARCHAR(50), ParentName NVARCHAR(255), Client INT, Ziff INT , IdField INT, SortOrder NVARCHAR(100), SeqOrder INT)
	INSERT INTO #CostStructureAssumptions (IdModel, IdCostStructureAssumptions, IdProject, ProjectName, IdZiffAccount, Code, Name, ParentCode, ParentName, Client, Ziff, IdField, SortOrder,SeqOrder)
	SELECT	tblZiffAccountsParent.IdModel, tblCostStructureAssumptionsByProject.IdCostStructureAssumptions, tblCostStructureAssumptionsByProject.IdProject, tblProjects.Name,
			tblCostStructureAssumptionsByProject.IdZiffAccount, '&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;' + tblZiffAccounts.Code as Code, tblZiffAccounts.Name, tblZiffAccounts.Code AS ParentCode, 
			tblZiffAccountsParent.Name AS ParentName, tblCostStructureAssumptionsByProject.Client, tblCostStructureAssumptionsByProject.Ziff, 
			tblProjects.IdField, tblZiffAccounts.SortOrder, NULL
	FROM    tblCostStructureAssumptionsByProject LEFT OUTER JOIN
			tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject LEFT OUTER JOIN
			tblZiffAccounts ON tblCostStructureAssumptionsByProject.IdZiffAccount = tblZiffAccounts.IdZiffAccount LEFT OUTER JOIN
			tblZiffAccounts AS tblZiffAccountsParent ON tblZiffAccounts.Parent = tblZiffAccountsParent.IdZiffAccount
	WHERE  	tblProjects.IdModel = @IdModel
	AND		tblProjects.IdField = @IdField 
	UNION
	SELECT NULL, NULL, NULL, NULL, NULL, dbo.tblZiffAccounts.Code, Name, NULL, NULL, 9999, 9999, NULL, dbo.tblZiffAccounts.SortOrder,NULL
	FROM tblZiffAccounts
	WHERE  	IdModel = @IdModel AND Parent IS NULL
	ORDER BY SortOrder

	/** Add Sequence(ROW_NUMBER) to SeqOrder **/
	UPDATE x 
	SET x.SeqOrder = x.New_SeqOrder
	FROM (
			SELECT SeqOrder, ROW_NUMBER() OVER(ORDER BY SortOrder ASC) AS New_SeqOrder
			FROM #CostStructureAssumptions
		 ) x
	
	/** Build Output Table **/
--	SELECT IdModel, IdCostStructureAssumptions, IdProject, ProjectName, IdZiffAccount, CASE WHEN CHARINDEX('C',Code,1) = 32 THEN SUBSTRING(Code,1,33) + '0' + SUBSTRING(Code,34,LEN(Code)) ELSE Code END AS Code, CASE WHEN CHARINDEX('-',Name,1) = 4 THEN SUBSTRING(Name,1,2) + '0' + SUBSTRING(Name,CHARINDEX('-',Name,1)-1,LEN(Name)) ELSE Name END AS Name, ParentCode, ParentName, Client, Ziff, IdField, CASE WHEN CHARINDEX('-',SortOrder,6) = 10 THEN SUBSTRING(SortOrder,1,8) + '0' + SUBSTRING(SortOrder,9,LEN(SortOrder)) + N' ' + ISNULL(ProjectName,'') ELSE SortOrder END AS /*Sort, SortOrder + N' ' + ISNULL(ProjectName,'') AS*/ SortOrder, SeqOrder
	SELECT IdModel, IdCostStructureAssumptions, IdProject, ProjectName, IdZiffAccount, Code, Name, ParentCode, ParentName, Client, Ziff, IdField, SortOrder + N' ' + ISNULL(ProjectName,'') AS SortOrder, SeqOrder
	FROM #CostStructureAssumptions 
	ORDER BY SeqOrder
	
END
GO
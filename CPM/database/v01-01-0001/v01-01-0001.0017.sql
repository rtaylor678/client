USE [DBCPM]
GO

DELETE FROM [dbo].[tblProfilesRoles]
GO

DBCC CHECKIDENT (tblProfilesRoles, reseed, 0)
GO

DELETE FROM [dbo].[tblRoles]
GO

ALTER TABLE [dbo].[tblRoles] DROP COLUMN IdLanguage
GO

CREATE TABLE [dbo].[tblRolesLanguages](
	[IdRole] [int] NOT NULL,
	[IdLanguage] [int] NOT NULL,
	[RoleText] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_tblRolesLanguages] PRIMARY KEY CLUSTERED 
(
	[IdRole] ASC,
	[IdLanguage] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblRolesLanguages]  WITH CHECK ADD  CONSTRAINT [FK_tblRolesLanguages_tblLanguages] FOREIGN KEY([IdLanguage])
REFERENCES [dbo].[tblLanguages] ([IdLanguage])
GO

ALTER TABLE [dbo].[tblRolesLanguages] CHECK CONSTRAINT [FK_tblRolesLanguages_tblLanguages]
GO

ALTER TABLE [dbo].[tblRolesLanguages]  WITH CHECK ADD  CONSTRAINT [FK_tblRolesLanguages_tblRoles] FOREIGN KEY([IdRole])
REFERENCES [dbo].[tblRoles] ([IdRole])
GO

ALTER TABLE [dbo].[tblRolesLanguages] CHECK CONSTRAINT [FK_tblRolesLanguages_tblRoles]
GO

SET ANSI_PADDING OFF
GO

SET IDENTITY_INSERT [dbo].[tblRoles] ON 
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (1, N'Model Administrator', 0, NULL, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (2, N'Model Creation', 0, NULL, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (3, N'Parameters Module', 1, NULL, 1, 1, CAST(N'2012-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (4, N'Aggregation Levels', 1, 1, 1, 1, CAST(N'2012-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (5, N'Types of Operations', 1, 2, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (6, N'List of Fields', 1, 3, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (7, N'Field Projects', 1, 4, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (8, N'Currency', 1, 5, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (9, N'Activities', 1, 6, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (10, N'Resources', 1, 7, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (11, N'Cost Objects', 1, 8, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (12, N'Chart of Accounts', 1, 9, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (13, N'Allocation Module', 2, NULL, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (14, N'Base Cost Data', 2, 1, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (15, N'Direct', 2, 2, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (16, N'Shared', 2, 3, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (17, N'Hierarchy', 2, 4, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (18, N'List of Cost Benchmark Drivers', 2, 6, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (19, N'Cost Benchmark Drivers Criteria', 2, 7, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (20, N'Cost Benchmark Drivers Data', 2, 8, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (21, N'Internal Benchmarks Files Repository', 2, 11, 1, 1, CAST(N'2015-09-14 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (22, N'Mapping (Company Structure vs. Ziff Cost Categories)', 2, 9, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (23, N'Company Base Cost References', 2, 10, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (24, N'List of Peer Criteria', 2, 12, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (25, N'Peer Criteria Data', 2, 13, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (26, N'External Base Cost References', 2, 14, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (27, N'Cost Structure Assumptions', 2, 16, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (28, N'Technical Module', 3, NULL, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (29, N'List of Technical Drivers', 3, 1, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (30, N'Technical Drivers Data', 3, 2, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (31, N'Technical Assumptions', 3, 3, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (32, N'Base Rates By Cost Category', 3, 5, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (33, N'Technical Files Repository', 3, 7, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (34, N'Economic Module', 4, NULL, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (35, N'List of Economic Drivers', 4, 1, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (36, N'Economic Drivers Data', 4, 2, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (37, N'Economic Assumptions', 4, 3, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (38, N'Base Cost Rates Forecast', 4, 4, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (39, N'Economic Files Repository', 4, 5, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (40, N'Reports Module', 5, NULL, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (41, N'Projection by Cost Category', 5, 1, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (42, N'Projection by Field Project', 5, 2, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (43, N'Base Rates By Cost Category', 5, 3, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (44, N'Cost Projection Categories', 1, 10, 1, 1, CAST(N'2013-10-10 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (45, N'Utilization Capacity', 3, 4, 1, 1, CAST(N'2015-05-20 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (46, N'Base Rates By Drivers', 3, 6, 1, 1, CAST(N'2015-05-20 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (47, N'Base Rates by Drivers', 5, 4, 1, 1, CAST(N'2015-05-20 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (48, N'Internal Base Cost References', 5, 5, 1, 1, CAST(N'2015-05-20 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (49, N'External Base Cost References', 5, 6, 1, 1, CAST(N'2015-05-20 00:00:00.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (50, N'External Benchmarkes Files Repository', 2, 15, 1, 1, CAST(N'2015-09-14 00:00:00.000' AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[tblRoles] OFF
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (1, 1, N'Model Administrator')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (1, 2, N'Modulo Administrador')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (2, 1, N'Model Creation')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (2, 2, N'Modulo Creación')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (3, 1, N'Parameters Module')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (3, 2, N'Módulo de Parámetros')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (4, 1, N'Aggregation Levels')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (4, 2, N'Niveles de Agregación')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (5, 1, N'Types of Operations')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (5, 2, N'Tipo de Operaciones')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (6, 1, N'List of Fields')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (6, 2, N'Lista de Campos')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (7, 1, N'Field Projects')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (7, 2, N'Proyectos de Campo')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (8, 1, N'Currency')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (8, 2, N'Moneda')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (9, 1, N'Activities')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (9, 2, N'Actividades')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (10, 1, N'Resources')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (10, 2, N'Recursos')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (11, 1, N'Cost Objects')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (11, 2, N'Objetos de Costo')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (12, 1, N'Chart of Accounts')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (12, 2, N'Estructura de Cuentas')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (13, 1, N'Allocation Module')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (13, 2, N'Referentes de Costos')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (14, 1, N'Base Cost Data')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (14, 2, N'Entrada de Costos')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (15, 1, N'Direct')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (15, 2, N'Directo')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (16, 1, N'Shared')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (16, 2, N'Compartido')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (17, 1, N'Hierarchy')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (17, 2, N'Jerarquía')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (18, 1, N'List of Cost Benchmark Drivers')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (18, 2, N'Lista de Drivers de Distribución')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (19, 1, N'Cost Benchmark Drivers Criteria')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (19, 2, N'Selección de Drivers de Distribución')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (20, 1, N'Cost Benchmark Drivers Data')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (20, 2, N'Datos de Drivers de Distribución')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (21, 1, N'Internal Benchmarks Files Repository')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (21, 2, N'Repositorio de Archivos Referentes Internos')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (22, 1, N'Mapping (Company Structure vs. Ziff Cost Categories)')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (22, 2, N'Mapeo (Estructura Empresa vs Estructura Proyección)')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (23, 1, N'Company Base Cost References')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (23, 2, N'Referentes Internos de Costos Base')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (24, 1, N'List of Peer Criteria')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (24, 2, N'Lista de Criterios')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (25, 1, N'Peer Criteria Data')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (25, 2, N'Datos de Criterios')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (26, 1, N'External Base Cost References')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (26, 2, N'Referentes Externos de Costos Base')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (27, 1, N'Cost Structure Assumptions')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (27, 2, N'Supuestos de Estructura de Costos ')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (28, 1, N'Technical Module')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (28, 2, N'Módulo Técnico')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (29, 1, N'List of Technical Drivers')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (29, 2, N'Lista de Drivers Técnicos')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (30, 1, N'Technical Drivers Data')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (30, 2, N'Datos de Drivers Técnicos')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (31, 1, N'Technical Assumptions')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (31, 2, N'Supuestos Técnicos')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (32, 1, N'Base Rates By Cost Category')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (32, 2, N'tarifas de Costos Base por Categoria')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (33, 1, N'Technical Files Repository')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (33, 2, N'Repositorio de Archivos Técnico')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (34, 1, N'Economic Module')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (34, 2, N'Módulo Económico')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (35, 1, N'List of Economic Drivers')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (35, 2, N'Lista de Drivers Económicos')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (36, 1, N'Economic Drivers Data')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (36, 2, N'Datos de Drivers Económicos')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (37, 1, N'Economic Assumptions')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (37, 2, N'Supuestos Económicos')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (38, 1, N'Base Cost Rates Forecast')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (38, 2, N'Pronóstico de Tarifa de Costos Base')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (39, 1, N'Economic Files Repository')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (39, 2, N'Repositorio de Archivos Económico')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (40, 1, N'Reports Module')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (40, 2, N'Módulo de Reportes')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (41, 1, N'Projection by Cost Category')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (41, 2, N'Proyección por Categoria de Costo')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (42, 1, N'Projection by Field Project')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (42, 2, N'Proyección Detallada por Proyecto')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (43, 1, N'Base Rates By Cost Category')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (43, 2, N'Tarifas de Costos Base por Categoria')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (44, 1, N'Cost Projection Categories')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (44, 2, N'Estructura de Proyección de Costo')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (45, 1, N'Utilization Capacity')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (45, 2, N'Capacidad Utilizada')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (46, 1, N'Base Rates By Drivers')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (46, 2, N'Tarifas de Costos Base por Drivers')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (47, 1, N'Base Rates by Drivers')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (47, 2, N'Tarifas de Costos Base por Drivers')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (48, 1, N'Internal Base Cost References')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (48, 2, N'Referentes Internos de Costos Base')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (49, 1, N'External Base Cost References')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (49, 2, N'Referentes Externos de Costos Base')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (50, 1, N'External Benchmarkes Files Repository')
GO
INSERT [dbo].[tblRolesLanguages] ([IdRole], [IdLanguage], [RoleText]) VALUES (50, 2, N'Repositorio de Archivos Referentes Externos')
GO

-- Alter FK Relationships
ALTER TABLE [dbo].[tblProfilesRoles] DROP CONSTRAINT [FK_tblProfilesRoles_tblModels]
GO

ALTER TABLE [dbo].[tblProfilesRoles] DROP CONSTRAINT [FK_tblProfilesRoles_tblRoles]
GO

ALTER TABLE [dbo].[tblProfilesRoles] DROP CONSTRAINT [FK_tblProfilesRoles_tblProfiles]
GO

ALTER TABLE [dbo].[tblProfilesRoles]  WITH CHECK ADD  CONSTRAINT [FK_tblProfilesRoles_tblProfiles] FOREIGN KEY([IdProfile])
REFERENCES [dbo].[tblProfiles] ([IdProfile])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tblProfilesRoles] CHECK CONSTRAINT [FK_tblProfilesRoles_tblProfiles]
GO

ALTER TABLE [dbo].[tblProfilesRoles]  WITH CHECK ADD  CONSTRAINT [FK_tblProfilesRoles_tblRoles] FOREIGN KEY([IdRole])
REFERENCES [dbo].[tblRoles] ([IdRole])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tblProfilesRoles] CHECK CONSTRAINT [FK_tblProfilesRoles_tblRoles]
GO

-- Add Defaults for Built-in Accounts
INSERT INTO tblProfilesRoles([IdProfile],[IdRole],[IdModel],[DenyAccess],[GrantAccess],[UserCreation],[DateCreation])
SELECT tblProfiles.IdProfile, tblRoles.IdRole, 0 AS IdModel, 0 AS DenyAccess, 1 AS GrantAccess, 0 AS UserCreation, GETDATE() AS DateCreation
FROM tblProfiles CROSS JOIN tblRoles
WHERE tblProfiles.Type=1 AND tblProfiles.BuiltIn=1;
GO

INSERT INTO tblProfilesRoles([IdProfile],[IdRole],[IdModel],[DenyAccess],[GrantAccess],[UserCreation],[DateCreation])
SELECT tblProfiles.IdProfile, tblRoles.IdRole, 0 AS IdModel, 0 AS DenyAccess, 1 AS GrantAccess, 0 AS UserCreation, GETDATE() AS DateCreation
FROM tblProfiles CROSS JOIN tblRoles
WHERE tblProfiles.Type=2 AND tblProfiles.BuiltIn=1 AND tblRoles.IdRole>1;
GO

/****** Object:  StoredProcedure [dbo].[usrpListProfileRoles]    Script Date: 2015-09-14 12:08:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usrpListProfileRoles] 
    @IdModel INT, 
    @IdProfile INT,
    @IdLanguage INT 
AS 
BEGIN
	
    SET NOCOUNT ON;
    
	SELECT 0 AS IdProfileRoles
		, 0 IdModel
		, 0 IdProfile
		, tblRoles.IdRole
		, CASE WHEN SubSortOrder IS NULL THEN N'' ELSE N'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' END + CASE WHEN SortOrder IS NULL THEN N'' ELSE CAST(SortOrder AS nvarchar) END + CASE WHEN SubSortOrder IS NULL THEN N'' ELSE N'.' + CAST(SubSortOrder AS nvarchar) END + N' - ' + tblRolesLanguages.RoleText AS RoleName
		, 0 AS DenyAccess
		, 0 AS GrantAccess
		, SortOrder
		, SubSortOrder
	FROM tblRoles INNER JOIN
		tblRolesLanguages ON tblRoles.IdRole = tblRolesLanguages.IdRole
	WHERE tblRoles.IdRole NOT IN
	(
	SELECT tblRoles.IdRole
	FROM tblProfilesRoles 
		INNER JOIN tblProfiles ON tblProfilesRoles.IdProfile = tblProfiles.IdProfile 
		INNER JOIN tblRoles ON tblProfilesRoles.IdRole = tblRoles.IdRole
	WHERE tblProfilesRoles.IdModel = @IdModel AND tblProfilesRoles.IdProfile = @IdProfile
	) AND tblRolesLanguages.IdLanguage = @IdLanguage
	UNION ALL
	SELECT tblProfilesRoles.IdProfileRoles
		, tblProfilesRoles.IdModel	
		, tblProfiles.IdProfile
		, tblRoles.IdRole
		, CASE WHEN SubSortOrder IS NULL THEN N'' ELSE N'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' END + CASE WHEN SortOrder IS NULL THEN N'' ELSE CAST(SortOrder AS nvarchar) END + CASE WHEN SubSortOrder IS NULL THEN N'' ELSE N'.' + CAST(SubSortOrder AS nvarchar) END + N' - ' + tblRoles.Name AS RoleName
		, ISNULL(tblProfilesRoles.DenyAccess,0) AS DenyAccess
		, ISNULL(tblProfilesRoles.GrantAccess,0) AS GrantAccess
		, SortOrder
		, SubSortOrder
	FROM tblProfilesRoles INNER JOIN
		tblProfiles ON tblProfilesRoles.IdProfile = tblProfiles.IdProfile INNER JOIN
		tblRoles ON tblProfilesRoles.IdRole = tblRoles.IdRole INNER JOIN
		tblRolesLanguages ON tblRoles.IdRole = tblRolesLanguages.IdRole
	WHERE tblProfilesRoles.IdModel = @IdModel AND tblProfilesRoles.IdProfile = @IdProfile AND tblRolesLanguages.IdLanguage = @IdLanguage	
	ORDER BY SortOrder, SubSortOrder
	
END
GO

/****** Object:  StoredProcedure [dbo].[usrpInsertModelDefaultPermission]    Script Date: 2015-09-14 2:34:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gareth Slater
-- Create date: 2014-04-03
-- Description:	Create default permissions for new model for Admin
-- =============================================
ALTER PROC [dbo].[usrpInsertModelDefaultPermission](
@IdModel int,
@UserCreation int
)
AS
BEGIN
	
	-- Cleanup Old Records First
	DELETE FROM tblProfilesRoles WHERE [IdModel]<>0 AND [IdModel] NOT IN (SELECT [IdModel] FROM tblModels);
	
	-- Insert Current User's Role with Standard User Rights if not built-in
	INSERT INTO tblProfilesRoles([IdProfile],[IdRole],[IdModel],[DenyAccess],[GrantAccess],[UserCreation],[DateCreation])
	SELECT tblProfiles.IdProfile, tblRoles.IdRole, @IdModel AS IdModel, 0 AS DenyAccess, 1 AS GrantAccess, 0 AS UserCreation, GETDATE() AS DateCreation
	FROM tblProfiles INNER JOIN
		tblUsers ON tblProfiles.IdProfile = tblUsers.IdProfile CROSS JOIN
		tblRoles
	WHERE tblProfiles.BuiltIn=0 AND tblUsers.IdUser=@UserCreation;
	
END
GO
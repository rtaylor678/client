USE [DBCPM]
GO

BEGIN TRANSACTION

GO
ALTER TABLE dbo.tblProjects ALTER COLUMN [Code] varchar(150) NOT NULL
ALTER TABLE dbo.tblProjects ALTER COLUMN [Name] varchar(255) NOT NULL

COMMIT

GO
/****** Object:  StoredProcedure [dbo].[parpInsertProjects]    Script Date: 2015-08-01 12:36:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------
Autor   		:Rodrigo Lopez Robles
Fecha Creacion	:09/10/2013
Descripcion     : Adiciona un registra en la tabla tblProjects
--------------------------------------------*/
ALTER PROC [dbo].[parpInsertProjects](
@IdProject numeric output,
@IdModel int,
@Code varchar(150),
@Name varchar(255),
@IdField int,
@Operation int,
@Starts int,
@TypeAllocation Int,
@UserCreation int,
@DateCreation datetime,
@UserModification int,
@DateModification datetime)
AS
BEGIN
	
	INSERT INTO tblProjects ([IdModel],[Code],[Name],[IdField],[Operation],[Starts],[TypeAllocation],[UserCreation],[DateCreation],[UserModification],[DateModification])
	VALUES(@IdModel,@Code,@Name,@IdField,@Operation,@Starts,@TypeAllocation,@UserCreation,@DateCreation,@UserModification,@DateModification);
	
	SET @IdProject = SCOPE_IDENTITY();
	
END

GO
/****** Object:  StoredProcedure [dbo].[parpUpdateProjects]    Script Date: 2015-08-01 12:35:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-------------------------------------------
Autor   		:Rodrigo Lopez Robles
Fecha Creacion	:09/10/2013
Descripcion     : Modifica un registro en la tabla tblProjects
--------------------------------------------*/
ALTER PROC [dbo].[parpUpdateProjects](
@IdProject int,
@IdModel int,
@Code varchar(150),
@Name varchar(255),
@IdField int,
@Operation int,
@Starts int,
@TypeAllocation Int,
@UserCreation int,
@DateCreation datetime,
@UserModification int,
@DateModification datetime)
AS 
BEGIN
  UPDATE tblProjects SET IdModel=@IdModel,Code=@Code,Name=@Name,IdField=@IdField,Operation=@Operation,Starts=@Starts,TypeAllocation=@TypeAllocation,UserCreation=@UserCreation,DateCreation=@DateCreation,UserModification=@UserModification,DateModification=@DateModification
      WHERE IdProject =@IdProject
END
USE [DBCPM]
GO
/****** Object:  StoredProcedure [dbo].[allpListCostStructureAssumptionsByProject]    Script Date: 07/02/2015 08:01:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-06-11
-- Description:	List Cost Structure Assumptions By Project as the view will no longer work as we are adding a row to display the parent
-- ========================================================================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 7/02/2015	RM		Added Name from tblZiffAccounts to header row
-- ========================================================================================================================================

ALTER PROCEDURE [dbo].[allpListCostStructureAssumptionsByProject](
 @IdModel int,
 @IdField int
)
AS
BEGIN
	
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#CostStructureAssumptions') IS NOT NULL DROP TABLE #CostStructureAssumptions

	CREATE TABLE #CostStructureAssumptions (IdModel INT, IdCostStructureAssumptions INT, IdProject INT, IdZiffAccount INT, Code NVARCHAR(50), Name NVARCHAR(255), ParentCode NVARCHAR(50), ParentName NVARCHAR(255), Client INT, Ziff INT , IdField INT, SortOrder NVARCHAR(100))
	INSERT INTO #CostStructureAssumptions (IdModel, IdCostStructureAssumptions, IdProject, IdZiffAccount, Code, Name, ParentCode, ParentName, Client, Ziff, IdField, SortOrder)
	SELECT	tblZiffAccountsParent.IdModel, tblCostStructureAssumptionsByProject.IdCostStructureAssumptions, tblCostStructureAssumptionsByProject.IdProject, 
			tblCostStructureAssumptionsByProject.IdZiffAccount, '&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;' + tblZiffAccounts.Code as Code, tblZiffAccounts.Name, tblZiffAccounts.Code AS ParentCode, 
			tblZiffAccountsParent.Name AS ParentName, tblCostStructureAssumptionsByProject.Client, tblCostStructureAssumptionsByProject.Ziff, 
			tblProjects.IdField, tblZiffAccounts.SortOrder
	FROM    tblCostStructureAssumptionsByProject LEFT OUTER JOIN
			tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject LEFT OUTER JOIN
			tblZiffAccounts ON tblCostStructureAssumptionsByProject.IdZiffAccount = tblZiffAccounts.IdZiffAccount LEFT OUTER JOIN
			tblZiffAccounts AS tblZiffAccountsParent ON tblZiffAccounts.Parent = tblZiffAccountsParent.IdZiffAccount
	WHERE  	tblProjects.IdModel = @IdModel
	AND		tblProjects.IdField = @IdField 

	INSERT INTO #CostStructureAssumptions (IdModel, IdCostStructureAssumptions, IdProject, IdZiffAccount, Code, Name, ParentCode, ParentName, Client, Ziff, IdField, SortOrder)
	SELECT NULL, NULL, NULL, NULL, dbo.tblZiffAccounts.Code, Name, NULL, NULL, 9999, 9999, NULL, dbo.tblZiffAccounts.SortOrder
	FROM tblZiffAccounts
	WHERE  	IdModel = @IdModel AND Parent IS NULL

	/** Build Output Table **/
	SELECT IdModel, IdCostStructureAssumptions, IdProject, IdZiffAccount, Code, Name, ParentCode, ParentName, Client, Ziff, IdField, SortOrder 
	FROM #CostStructureAssumptions 
	ORDER BY SortOrder
END

GO
/****** Object:  StoredProcedure [dbo].[allpListExternalBaseCostReferences]    Script Date: 07/02/2015 08:07:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-06-17
-- Description:	List of External Base Cost References
-- ================================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 7/02/2015	RM		Added Name from tblZiffAccounts to header row
-- ================================================================================================
ALTER PROCEDURE [dbo].[allpListExternalBaseCostReferences]
	-- Add the parameters for the stored procedure here
	@IdModel INT,
	@IdField INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF OBJECT_ID('tempdb..#ExternalCostReferences') IS NOT NULL DROP TABLE #ExternalCostReferences

	CREATE TABLE #ExternalCostReferences (IdModel INT,IdZiffBaseCost INT,IdField INT,IdZiffAccount INT,IdPeerCriteria INT,UnitCost FLOAT,Quantity FLOAT,TotalCost FLOAT,
										  Field VARCHAR(150),CodeZiff VARCHAR(50),ZiffAccount VARCHAR(150),IdRootZiffAccount INT,RootCodeZiff VARCHAR(50),RootZiffAccount VARCHAR(150),
										  PeerCriteria VARCHAR(50),Unit VARCHAR(150),IdUnitPeerCriteria INT,TechApplicable BIT,TFNormal FLOAT,TFPessimistic FLOAT,TFOptimistic FLOAT,
										  BCRNormal FLOAT,BCRPessimistic FLOAT,BCROptimistic FLOAT, SortOrder VARCHAR(50))	
	INSERT INTO #ExternalCostReferences (IdModel,IdZiffBaseCost,IdField,IdZiffAccount,IdPeerCriteria,UnitCost,Quantity,TotalCost,Field,CodeZiff,ZiffAccount,IdRootZiffAccount,
										 RootCodeZiff,RootZiffAccount,PeerCriteria,Unit,IdUnitPeerCriteria,TechApplicable,TFNormal,TFPessimistic,TFOptimistic,BCRNormal,
										 BCRPessimistic,BCROptimistic, SortOrder)	
	SELECT CBCBFZ.IdModel,CBCBFZ.IdZiffBaseCost,CBCBFZ.IdField,CBCBFZ.IdZiffAccount,CBCBFZ.IdPeerCriteria,CBCBFZ.UnitCost,CBCBFZ.Quantity,CBCBFZ.TotalCost,CBCBFZ.Field,
		   '&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;'+CBCBFZ.CodeZiff as CodeZiff,CBCBFZ.ZiffAccount,CBCBFZ.IdRootZiffAccount,CBCBFZ.RootCodeZiff,CBCBFZ.RootZiffAccount,
		   CBCBFZ.PeerCriteria,CBCBFZ.Unit,CBCBFZ.IdUnitPeerCriteria,CBCBFZ.TechApplicable,CBCBFZ.TFNormal,CBCBFZ.TFPessimistic,CBCBFZ.TFOptimistic,CBCBFZ.BCRNormal,
		   CBCBFZ.BCRPessimistic,CBCBFZ.BCROptimistic,ZA.SortOrder 
	FROM tblCalcBaseCostByFieldZiff CBCBFZ left outer join tblZiffAccounts ZA on 
		 ZA.IdModel=CBCBFZ.IdModel and ZA.IdZiffAccount=CBCBFZ.IdZiffAccount 
	WHERE CBCBFZ.IdModel = @IdModel AND CBCBFZ.IdField = @IdField 
	UNION 
	SELECT IdModel,NULL, NULL, IdZiffAccount,NULL,-99,NULL,-99,NULL,Code,Name,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,SortOrder 
	FROM tblZiffAccounts 
	WHERE IdModel = @IdModel 
	AND Parent IS NULL 

	SELECT * FROM #ExternalCostReferences 	ORDER BY SortOrder 
END

GO
/****** Object:  StoredProcedure [dbo].[tecpUtilizationCapacity]    Script Date: 07/02/2015 08:10:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-03-19
-- Description:	Utilization Capacity Report per Field
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 7/02/2015	RM		Added Name from tblZiffAccounts to header row
-- ===========================================================================================
ALTER PROCEDURE [dbo].[tecpUtilizationCapacity](
@IdModel int,
@IdStage int,
@BaseCostType int
)
AS
BEGIN
	
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#FieldList') IS NOT NULL DROP TABLE #FieldList
	IF OBJECT_ID('tempdb..#UtilizationCapacity') IS NOT NULL DROP TABLE #UtilizationCapacity
	IF OBJECT_ID('tempdb..#tblCalcTechnicalBaseCostRate') IS NOT NULL DROP TABLE #tblCalcTechnicalBaseCostRate

	/** Build Field List **/
	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL, Name NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #FieldList SELECT A.[IdField], B.Name FROM tblCalcTechnicalBaseCostRate A INNER JOIN tblFields B ON A.IdField=B.IdField WHERE A.[IdModel]=@IdModel GROUP BY A.[IdField], B.Name;

	/** Build Field Column **/
	DECLARE @FieldColumnList NVARCHAR(MAX)='';
	DECLARE @FieldName NVARCHAR(255);
	DECLARE FieldList CURSOR FOR
	SELECT Name FROM #FieldList;

	OPEN FieldList;
	FETCH NEXT FROM FieldList INTO @FieldName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @FieldColumnList=''
		BEGIN
			SET @FieldColumnList =  @FieldColumnList + '[' + @FieldName  + ']';
		END
		ELSE
		BEGIN
			SET @FieldColumnList =  @FieldColumnList + ',[' + @FieldName  + ']';
		END
		FETCH NEXT FROM FieldList INTO @FieldName;
	END

	CLOSE FieldList;
	DEALLOCATE FieldList;

	CREATE TABLE #UtilizationCapacity (CodeZiff VARCHAR(150), FieldName NVARCHAR(255), SizeValue FLOAT, CodeZiffExport VARCHAR(150), SortOrder NVARCHAR(100))
	INSERT INTO #UtilizationCapacity (CodeZiff,FieldName,SizeValue,CodeZiffExport,SortOrder)
	SELECT  dbo.svfLevelStringSpacer(dbo.tblZiffAccounts.RelationLevel) + dbo.tblZiffAccounts.Code + N': ' + dbo.tblZiffAccounts.Name AS DisplayName
		  , tblFields.Name
		  , A.UtilizationValue
		  , A.[CodeZiff] + ': ' + A.ZiffAccount AS CodeZiffExport
		  , tblZiffAccounts.SortOrder
	  FROM tblUtilizationCapacity A INNER JOIN tblFields
			ON A.IdField = tblFields.IdField LEFT OUTER JOIN tblZiffAccounts 
			ON tblZiffAccounts.Code=A.CodeZiff
	WHERE  	A.IdModel = @IdModel
	GROUP BY dbo.svfLevelStringSpacer(dbo.tblZiffAccounts.RelationLevel) + dbo.tblZiffAccounts.Code + N': ' + dbo.tblZiffAccounts.Name,tblFields.Name,A.UtilizationValue, A.[CodeZiff] + ': ' + A.ZiffAccount,tblZiffAccounts.SortOrder
	ORDER BY tblZiffAccounts.SortOrder

	INSERT INTO #UtilizationCapacity (CodeZiff,FieldName,SizeValue,CodeZiffExport,SortOrder)
	SELECT  dbo.svfLevelStringSpacer(ZA.RelationLevel) + ZA.Code + N': ' + ZA.Name/*ZA.[Code]*/ AS CodeZiff
		  , #FieldList.Name
		  , '-1'
		  , dbo.svfLevelStringSpacer(ZA.RelationLevel) + ZA.Code + N': ' + ZA.Name/*ZA.[Code]*/ AS CodeZiffExport
		  , ZA.SortOrder
	  FROM tblZiffAccounts ZA , #FieldList
	WHERE  	ZA.IdModel = @IdModel AND Parent IS NULL

	--/** Build Output Table **/
	DECLARE @SQLUtilizationCapacity NVARCHAR(MAX);
	SET @SQLUtilizationCapacity = 'SELECT * FROM #UtilizationCapacity PIVOT (Sum(SizeValue) FOR FieldName IN (' + @FieldColumnList + ')) AS PivotTable ORDER BY SortOrder';
	EXEC(@SQLUtilizationCapacity);

END

/****** Object:  Table [dbo].[tblLanguages]    Script Date: 06/09/2016 09:34:12 ******/
SET IDENTITY_INSERT [dbo].[tblLanguages] ON
INSERT [dbo].[tblLanguages] ([IdLanguage], [Name], [Flag], [Default], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (1, N'English', N'FlagUs', 0, 1, CAST(0x01380B00 AS Date), NULL, NULL)
INSERT [dbo].[tblLanguages] ([IdLanguage], [Name], [Flag], [Default], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (2, N'Español', N'FlagEs', 1, 2, CAST(0xA7380B00 AS Date), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblLanguages] OFF
GO
DECLARE @IdCount INT = 0
SELECT @IdCount = MAX([IdLanguage]) from [tblLanguages]
DBCC CHECKIDENT ('[tblLanguages]', RESEED, @IdCount); 
GO

/****** Object:  Table [dbo].[tblForms]    Script Date: 06/09/2016 09:34:12 ******/
SET IDENTITY_INSERT [dbo].[tblForms] ON
INSERT [dbo].[tblForms] ([IdForm], [Name], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (1, N'Login', 1, CAST(0xAE370B00 AS Date), NULL, NULL)
INSERT [dbo].[tblForms] ([IdForm], [Name], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (2, N'MasterPage', 1, CAST(0xAE370B00 AS Date), NULL, NULL)
INSERT [dbo].[tblForms] ([IdForm], [Name], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (3, N'CaseDetails', 1, CAST(0xA8380B00 AS Date), NULL, NULL)
INSERT [dbo].[tblForms] ([IdForm], [Name], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (4, N'UserManagement', 1, CAST(0xA9380B00 AS Date), NULL, NULL)
INSERT [dbo].[tblForms] ([IdForm], [Name], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (5, N'RolesPermissions', 1, CAST(0xA9380B00 AS Date), NULL, NULL)
INSERT [dbo].[tblForms] ([IdForm], [Name], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (7, N'ParameterModule', 1, CAST(0xAA380B00 AS Date), NULL, NULL)
INSERT [dbo].[tblForms] ([IdForm], [Name], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (8, N'AllocationModule', 1, CAST(0xAA380B00 AS Date), NULL, NULL)
INSERT [dbo].[tblForms] ([IdForm], [Name], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (9, N'ClientReferences', 1, CAST(0xAA380B00 AS Date), NULL, NULL)
INSERT [dbo].[tblForms] ([IdForm], [Name], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (10, N'TechnicalModule', 1, CAST(0xAB380B00 AS Date), NULL, NULL)
INSERT [dbo].[tblForms] ([IdForm], [Name], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (11, N'EconomicModule', 1, CAST(0xAB380B00 AS Date), NULL, NULL)
INSERT [dbo].[tblForms] ([IdForm], [Name], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (12, N'ReportsModule', 1, CAST(0xAB380B00 AS Date), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblForms] OFF
GO
DECLARE @IdCount INT = 0
SELECT @IdCount = MAX([IdForm]) from [tblForms]
DBCC CHECKIDENT ('[tblForms]', RESEED, @IdCount); 
GO

/****** Object:  Table [dbo].[tblCostType]    Script Date: 06/09/2016 09:34:12 ******/
SET IDENTITY_INSERT [dbo].[tblCostType] ON
INSERT [dbo].[tblCostType] ([IdCostType], [Code], [Name]) VALUES (1, N'AbsoluteCost', N'Absolute Cost')
INSERT [dbo].[tblCostType] ([IdCostType], [Code], [Name]) VALUES (2, N'UnitCost', N'Unit Cost')
SET IDENTITY_INSERT [dbo].[tblCostType] OFF
GO
DECLARE @IdCount INT = 0
SELECT @IdCount = MAX([IdCostType]) from [tblCostType]
DBCC CHECKIDENT ('[tblCostType]', RESEED, @IdCount); 
GO


/****** Object:  Table [dbo].[tblProfiles]    Script Date: 06/09/2016 09:35:44 ******/
SET IDENTITY_INSERT [dbo].[tblProfiles] ON
INSERT [dbo].[tblProfiles] ([IdProfile], [Name], [UserCreation], [DateCreation], [UserModification], [DateModification], [Type], [BuiltIn]) VALUES (1, N'Administrator', 1, CAST(0x0000A2A600000000 AS DateTime), 1, CAST(0x0000A34E0100FDD0 AS DateTime), 1, 1)
INSERT [dbo].[tblProfiles] ([IdProfile], [Name], [UserCreation], [DateCreation], [UserModification], [DateModification], [Type], [BuiltIn]) VALUES (2, N'Operator', 1, CAST(0x0000A2A600000000 AS DateTime), 18, CAST(0x0000A59100C59694 AS DateTime), 2, 1)
INSERT [dbo].[tblProfiles] ([IdProfile], [Name], [UserCreation], [DateCreation], [UserModification], [DateModification], [Type], [BuiltIn]) VALUES (4, N'VRO', 2, CAST(0x0000A51000FB9EF3 AS DateTime), 18, CAST(0x0000A5CC0115E271 AS DateTime), 1, 0)
INSERT [dbo].[tblProfiles] ([IdProfile], [Name], [UserCreation], [DateCreation], [UserModification], [DateModification], [Type], [BuiltIn]) VALUES (5, N'VRS', 2, CAST(0x0000A51000FC5FDF AS DateTime), 1, CAST(0x0000A513010F2DE1 AS DateTime), 2, 0)
INSERT [dbo].[tblProfiles] ([IdProfile], [Name], [UserCreation], [DateCreation], [UserModification], [DateModification], [Type], [BuiltIn]) VALUES (7, N'VRC', 2, CAST(0x0000A51000FD35F0 AS DateTime), 18, CAST(0x0000A5CC005F70AB AS DateTime), 2, 0)
INSERT [dbo].[tblProfiles] ([IdProfile], [Name], [UserCreation], [DateCreation], [UserModification], [DateModification], [Type], [BuiltIn]) VALUES (8, N'Corporativo', 2, CAST(0x0000A51000FE0D3C AS DateTime), 2, CAST(0x0000A5100107429C AS DateTime), 2, 0)
INSERT [dbo].[tblProfiles] ([IdProfile], [Name], [UserCreation], [DateCreation], [UserModification], [DateModification], [Type], [BuiltIn]) VALUES (9, N'VAS', 2, CAST(0x0000A51000FE4917 AS DateTime), 2, CAST(0x0000A52B00E1CD5E AS DateTime), 2, 0)
SET IDENTITY_INSERT [dbo].[tblProfiles] OFF
GO
DECLARE @IdCount INT = 0
SELECT @IdCount = MAX([IdProfile]) from tblProfiles
DBCC CHECKIDENT ('tblProfiles', RESEED, @IdCount); 
GO

/****** Object:  Table [dbo].[tblRoles]    Script Date: 06/09/2016 09:35:44 ******/
SET IDENTITY_INSERT [dbo].[tblRoles] ON
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (1, N'Model Administrator', 0, NULL, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (2, N'Model Creation', 0, NULL, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (3, N'Parameters Module', 1, NULL, 1, 1, CAST(0x0000A0E600000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (4, N'Aggregation Levels', 1, 1, 1, 1, CAST(0x0000A0E600000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (5, N'Types of Operations', 1, 2, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (6, N'List of Fields', 1, 3, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (7, N'List of Cases', 1, 4, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (8, N'Currency', 1, 5, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (9, N'Activities', 2, 1, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (10, N'Resources', 2, 2, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (11, N'Cost Objects', 2, 3, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (12, N'Chart of Accounts', 2, 4, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (13, N'Cost Benchmarking Module', 2, NULL, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (14, N'Base Cost Data', 2, 5, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (15, N'Direct', 2, 6, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (16, N'Shared', 2, 7, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (17, N'Hierarchy', 2, 8, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (18, N'List of Cost Allocation Drivers', 2, 9, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (19, N'Cost Allocation Drivers Criteria', 2, 10, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (20, N'Cost Allocation Drivers Data', 2, 11, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (21, N'Internal Benchmarks Files Repository', 2, 11, 0, 1, CAST(0x0000A51300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (22, N'Mapping (Company Cost Structure vs Cost Projection Categories)', 2, 12, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (23, N'Internal Base Cost References', 2, 13, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (24, N'List of Peer Criteria', 2, 15, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (25, N'Peer Criteria Data', 2, 16, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (26, N'External Base Cost References', 2, 17, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (27, N'Cost Structure Assumptions', 3, 7, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (28, N'Cost Projection Module', 3, NULL, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (29, N'List of Technical Drivers', 3, 1, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (30, N'Technical Drivers Data', 3, 2, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (31, N'Technical Assumptions', 3, 3, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (32, N'Base Rates By Cost Category', 3, 5, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (33, N'Technical Files Repository', 3, 7, 0, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (34, N'Economic Module', 4, NULL, 0, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (35, N'List of Economic Drivers', 3, 10, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (36, N'Economic Drivers Data', 3, 11, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (37, N'Economic Assumptions', 3, 12, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (38, N'Base Cost Rates Forecast', 3, 13, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (39, N'Economic Files Repository', 4, 5, 0, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (40, N'Reports Module', 4, NULL, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (41, N'Projection By Development Case', 4, 3, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (42, N'Projection by Field Project', 5, 2, 0, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (43, N'Base Rates By Cost Category', 5, 3, 0, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (44, N'Cost Projection Categories', 1, 6, 1, 1, CAST(0x0000A25300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (45, N'Utilization Capacity', 3, 4, 1, 1, CAST(0x0000A49E00000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (46, N'Base Rates By Drivers', 3, 6, 1, 1, CAST(0x0000A49E00000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (47, N'Base Rates by Drivers', 5, 4, 0, 1, CAST(0x0000A49E00000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (48, N'Internal Base Cost References', 5, 5, 0, 1, CAST(0x0000A49E00000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (49, N'External Base Cost References', 5, 6, 0, 1, CAST(0x0000A49E00000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (50, N'External Benchmarkes Files Repository', 2, 15, 0, 1, CAST(0x0000A51300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (51, N'List of Peer Group', 2, 14, 1, 1, CAST(0x0000A62B00E676E2 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (52, N'Price Scenarios', 3, 8, 1, 1, CAST(0x0000A62B00EC5647 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (53, N'Prices Data', 3, 9, 1, 1, CAST(0x0000A62B00EC5649 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (54, N'Competitiveness Cost Benchmarking', 2, 18, 1, 1, CAST(0x0000A62B00FC8889 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (55, N'Operational Margins Projection', 3, 14, 1, 1, CAST(0x0000A62B010414D9 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (56, N'Projection by Cost Category', 3, 15, 1, 1, CAST(0x0000A62B010414DC AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (57, N'Projection by Development Case', 3, 16, 1, 1, CAST(0x0000A62B010414DD AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (58, N'Projection by Cost Type', 3, 17, 1, 1, CAST(0x0000A62B010414DE AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (59, N'Multiple Fields Projection', 3, 18, 1, 1, CAST(0x0000A62B010414DF AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (60, N'Operational Margins Projection', 4, 1, 1, 1, CAST(0x0000A62B010414E0 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (61, N'Projection by Cost Category', 4, 2, 1, 1, CAST(0x0000A62B010414E2 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (62, N'Projection by Cost Type', 4, 4, 1, 1, CAST(0x0000A62B010414E3 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (63, N'Multiple Fields Projection', 4, 5, 1, 1, CAST(0x0000A62B010414E4 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([IdRole], [Name], [SortOrder], [SubSortOrder], [Enabled], [UserCreation], [DateCreation], [UserModification], [DateModification]) VALUES (64, N'Competitiveness Cost Benchmarking', 4, 6, 1, 1, CAST(0x0000A62B0104B658 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblRoles] OFF
GO
DECLARE @IdCount INT = 0
SELECT @IdCount = MAX([IdRole]) from tblRoles
DBCC CHECKIDENT ('tblRoles', RESEED, @IdCount); 
GO

/****** Object:  Table [dbo].[tblStage]    Script Date: 06/09/2016 09:35:45 ******/
SET IDENTITY_INSERT [dbo].[tblStage] ON
INSERT [dbo].[tblStage] ([IdStage], [Name]) VALUES (1, N'Base')
INSERT [dbo].[tblStage] ([IdStage], [Name]) VALUES (2, N'Optimistic')
INSERT [dbo].[tblStage] ([IdStage], [Name]) VALUES (3, N'Pessimistic')
SET IDENTITY_INSERT [dbo].[tblStage] OFF
GO
DECLARE @IdCount INT = 0
SELECT @IdCount = MAX([IdStage]) from tblStage
DBCC CHECKIDENT ('tblStage', RESEED, @IdCount); 
GO

/****** Object:  Table [dbo].[tblSiteResources]    Script Date: 06/09/2016 09:35:45 ******/
SET IDENTITY_INSERT [dbo].[tblSiteResources] ON
INSERT [dbo].[tblSiteResources] ([IdResource], [IdLanguage], [ResourceName], [ResourceValue], [ResourceComment]) VALUES (1, 1, N'Username', N'Username', NULL)
INSERT [dbo].[tblSiteResources] ([IdResource], [IdLanguage], [ResourceName], [ResourceValue], [ResourceComment]) VALUES (2, 2, N'Username', N'Usuario', NULL)
INSERT [dbo].[tblSiteResources] ([IdResource], [IdLanguage], [ResourceName], [ResourceValue], [ResourceComment]) VALUES (3, 1, N'Password', N'Password', NULL)
INSERT [dbo].[tblSiteResources] ([IdResource], [IdLanguage], [ResourceName], [ResourceValue], [ResourceComment]) VALUES (4, 2, N'Password', N'Clave', NULL)
INSERT [dbo].[tblSiteResources] ([IdResource], [IdLanguage], [ResourceName], [ResourceValue], [ResourceComment]) VALUES (5, 1, N'Language', N'Language', NULL)
INSERT [dbo].[tblSiteResources] ([IdResource], [IdLanguage], [ResourceName], [ResourceValue], [ResourceComment]) VALUES (6, 2, N'language', N'Idioma', NULL)
SET IDENTITY_INSERT [dbo].[tblSiteResources] OFF
GO
DECLARE @IdCount INT = 0
SELECT @IdCount = MAX([IdResource]) from [tblSiteResources]
DBCC CHECKIDENT ('[tblSiteResources]', RESEED, @IdCount); 
GO

/****** Object:  Table [dbo].[tblScenarios]    Script Date: 06/09/2016 09:35:45 ******/
SET IDENTITY_INSERT [dbo].[tblScenarios] ON
INSERT [dbo].[tblScenarios] ([IdScenarioType], [Code], [Name]) VALUES (1, N'Base', N'Base')
INSERT [dbo].[tblScenarios] ([IdScenarioType], [Code], [Name]) VALUES (2, N'Optimistic', N'Optimistic')
INSERT [dbo].[tblScenarios] ([IdScenarioType], [Code], [Name]) VALUES (3, N'Pessimistic', N'Pessimistic')
SET IDENTITY_INSERT [dbo].[tblScenarios] OFF
GO
DECLARE @IdCount INT = 0
SELECT @IdCount = MAX([IdScenarioType]) from [tblScenarios]
DBCC CHECKIDENT ('[tblScenarios]', RESEED, @IdCount); 
GO

/****** Object:  Table [dbo].[tblUsers]    Script Date: 07/06/2016 08:31:51 ******/
SET IDENTITY_INSERT [dbo].[tblUsers] ON
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (1, N'Administrator', N'noreply@domain.com', N'admin', N'Ue2nT+Shc9kznGJBzk58oA==', 1, CAST(0x0000A63900A93245 AS DateTime), 1, CAST(0x0000A2A600000000 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (2, N'Soporte Funcional', N'soporte@solomon.com', N'Soporte', N'bUm6tNiwIgVPoETVBOvuBg==', 1, CAST(0x0000A635008E7DD1 AS DateTime), 1, CAST(0x0000A32C0185245B AS DateTime), 9, CAST(0x0000A5F40095D66E AS DateTime), 0)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (4, N'VRO', N'', N'VRO', N'LCjRhOUJUG6b8BvxdH2ErA==', 1, CAST(0x0000A5090094CF64 AS DateTime), 2, CAST(0x0000A34500FA78FF AS DateTime), 2, CAST(0x0000A5090094CF64 AS DateTime), 0)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (5, N'VRS', N'', N'VRS', N'r3h4uEZg9n+vMGvRB/MdUA==', 2, CAST(0x0000A377011AE19C AS DateTime), 2, CAST(0x0000A36F00F6D054 AS DateTime), NULL, NULL, 0)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (6, N'VRC', N'', N'VRC', N'j/d6/LVyQO8b/WMZSTy9+Q==', 2, CAST(0x0000A48800D620E9 AS DateTime), 2, CAST(0x0000A48700FA9A58 AS DateTime), NULL, NULL, 0)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (7, N'Corporativo', N'', N'Corporativo', N'A40jXIaMedcmFwyw28fNWA==', 1, CAST(0x0000A53A00CA6E48 AS DateTime), 2, CAST(0x0000A509009549FF AS DateTime), 1, CAST(0x0000A509009560CB AS DateTime), 0)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (8, N'Alba Larrota', N'Alba.Larrotta@ecopetrol.com.co', N'Alba.Larrotta', N'5ZnyNiQTg2z0H9HwZAx7Jw==', 2, CAST(0x0000A5100108C415 AS DateTime), 2, CAST(0x0000A4B9008F2415 AS DateTime), 2, CAST(0x0000A5100108C415 AS DateTime), 0)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (9, N'Maria Lucia Aguas', N'maria.aguas@ecopetrol.com.co', N'Maria.Aguas', N'5ZnyNiQTg2z0H9HwZAx7Jw==', 1, CAST(0x0000A61A00D3EFD7 AS DateTime), 2, CAST(0x0000A4B9008F7799 AS DateTime), 2, CAST(0x0000A514009091AE AS DateTime), 0)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (10, N'Gustavo Costa', N'Gustavo.Costa@ecopetrol.com.co', N'Gustavo.Costa', N'pVCK+u1L/idRuhIDfmtc2A==', 7, CAST(0x0000A62500E625F3 AS DateTime), 2, CAST(0x0000A4B9008FD8F0 AS DateTime), 18, CAST(0x0000A584006E7508 AS DateTime), 0)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (11, N'Diana Nunez', N'Diana.Nunez@ecopetrol.com.co', N'Diana.Nunez', N'TScIWuU5WwF6VCbj3pvlFA==', 7, CAST(0x0000A54B00E772F6 AS DateTime), 2, CAST(0x0000A4B900901621 AS DateTime), 2, CAST(0x0000A5140090693C AS DateTime), 0)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (12, N'Liliana Cortes', N'Liliana.Cortes@ecopetrol.com.co', N'Liliana.Cortes', N'NCH+WwNgTtekk4Ef0MCo/Q==', 4, CAST(0x0000A62500815702 AS DateTime), 2, CAST(0x0000A4B900906F77 AS DateTime), 18, CAST(0x0000A5CC01163F2F AS DateTime), 0)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (13, N'Myriam Diaz', N'myriam.diaz@ecopetrol.com.co', N'Myriam.Diaz', N'pVeAwQ2frhgfe3LjOl8UAQ==', 4, CAST(0x0000A52B00A16D79 AS DateTime), 2, CAST(0x0000A4B90090C2AB AS DateTime), 2, CAST(0x0000A51400908C0F AS DateTime), 0)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (14, N'Carolina Martinez', N'Carolina.Martinez@ecopetrol.com.co', N'Carolina.Martinez', N'um8/v5BpjcoIN33wmntClw==', 5, CAST(0x0000A6320079CF07 AS DateTime), 2, CAST(0x0000A4B90091143E AS DateTime), 2, CAST(0x0000A51400904F53 AS DateTime), 0)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (15, N'Diana Huaca', N'diana.huaca@ecopetrol.com.co', N'Diana.Huaca', N'V8kEXYEbD/d4ml4LWMmq4Q==', 5, CAST(0x0000A5370090BDAE AS DateTime), 2, CAST(0x0000A4B900929B23 AS DateTime), 2, CAST(0x0000A51400905C2E AS DateTime), 0)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (16, N'Lenix Trujillo', N'lenix.trujillo@ecopetrol.com.co', N'Lenix.Trujillo', N'W3x57RTP0KmzdGfsIGSvDw==', 5, CAST(0x0000A53000D8C75B AS DateTime), 2, CAST(0x0000A4B90092C18D AS DateTime), 2, CAST(0x0000A51400909FD6 AS DateTime), 0)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (17, N'Leydy Jerez', N'leydy.jerez@ecopetrol.com.co', N'Leydy.Jerez', N'2EKM8BDSU0TmgNRlU2gpVg==', 9, CAST(0x0000A52B00E6059E AS DateTime), 2, CAST(0x0000A4B9009EBC84 AS DateTime), 18, CAST(0x0000A52B00E6059E AS DateTime), 0)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (18, N'Ginet Sepulveda', N'Ginet.Sepulveda@ecopetrol.com.co', N'Ginet.Sepulveda', N'5ZnyNiQTg2z0H9HwZAx7Jw==', 1, CAST(0x0000A5E100F1AAF6 AS DateTime), 2, CAST(0x0000A4DE00F3DC12 AS DateTime), 2, CAST(0x0000A5140090B2BB AS DateTime), 0)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (19, N'Jesus Andres Arias', N'jesus.arias@solomononline.com', N'jesus.arias', N'P+h0fSwXNPagW1kp1odawA==', 1, CAST(0x0000A65000EC787F AS DateTime), 1, CAST(0x0000A64800A5FA4B AS DateTime), NULL, NULL, 0)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (22, N'Alfonso Murillo', N'alfonso.murillo@ecopetrol.com.co', N'Alfonso.Murillo', N'5ZnyNiQTg2z0H9HwZAx7Jw==', 2, CAST(0x0000A5ED00E2B4B7 AS DateTime), 2, CAST(0x0000A5130087E994 AS DateTime), NULL, NULL, 0)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (23, N'Marlon Garcia', N'Marlon.Garia@ecopetrol.com.co', N'Marlon.Garcia', N'HGxnHD70goeYr8Lbu8P94w==', 9, CAST(0x0000A52B00E1F6C4 AS DateTime), 2, CAST(0x0000A52B00E03365 AS DateTime), NULL, NULL, 0)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (24, N'Luis Carlos Mingan', N'Luis.Migan@ecopetrol.com.co', N'Luis.Migan', N'NiKR+Xo8xbXFPOI0J259lg==', 2, CAST(0x0000A5A100CC724F AS DateTime), 18, CAST(0x0000A58E00D13A72 AS DateTime), 18, CAST(0x0000A58E00D2DAE7 AS DateTime), 0)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (25, N'Nestor Gomez', N'Nestor.Gomez@ecopetrol.com.co', N'Nestor.Gomez', N'rUYpHg7UJhyKe03WszttEQ==', 7, CAST(0x0000A5CA0133B7EA AS DateTime), 18, CAST(0x0000A5BE0078FBDC AS DateTime), NULL, NULL, 0)
INSERT [dbo].[tblUsers] ([IdUser], [Name], [Email], [UserName], [Password], [IdProfile], [LastLogin], [UserCreation], [DateCreation], [UserModification], [DateModification], [BuiltIn]) VALUES (26, N'Portafolio VRC', N'gustavo.costa@ecopetrol.com.co', N'POrtafolio.VRC', N'v3OFyImHk5iiKVXc19f+hg==', 7, CAST(0x0000A6280095E44E AS DateTime), 18, CAST(0x0000A5BE0079B03C AS DateTime), NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[tblUsers] OFF
GO
DECLARE @IdCount INT = 0
SELECT @IdCount = MAX([IdUser]) from [tblUsers]
DBCC CHECKIDENT ('[tblUsers]', RESEED, @IdCount); 
GO


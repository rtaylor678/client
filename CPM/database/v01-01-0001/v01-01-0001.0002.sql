USE [DBCPM]
GO
/****** Object:  StoredProcedure [dbo].[reppOpexProjection]    Script Date: 06/10/2015 14:04:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gareth Slater
-- Create date: 2014-05-05
-- Description:	Module Report Opex Projection
-- =============================================
ALTER PROCEDURE [dbo].[reppOpexProjection](


@IdModel INT,
@IdAggregationLevel INT,
@IdField INT,
@IdStructure INT,
@From INT,
@To INT,
@IdTechnicalScenario INT,
@IdEconomicScenario INT,
@IdCurrency INT,
@IdTerm INT,
@IdTechnicalDriver INT,
@IdCostType INT,
@IdTypeOperation INT
)
AS
BEGIN
	DECLARE @StartYear INT = @From;
	DECLARE @EndYear INT = @To;
	
	/** Get Base Year **/
	DECLARE @BaseYear INT
	SELECT @BaseYear = [BaseYear] FROM [DBCPM].[dbo].[tblModels] WHERE IdModel=@IdModel

	/** Build Year Columns **/
	DECLARE @YearColumnList NVARCHAR(MAX)='';	
	WHILE (@From <= @To)
	BEGIN
		SET @YearColumnList =  @YearColumnList + '[' + CAST(@From AS VARCHAR(4)) + ']';
		SET @From = @From + 1;
		IF (@From <= @To)
		BEGIN
			SET @YearColumnList = @YearColumnList + ',';
		END
	END
	
	/** Build Field List **/
	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF (@IdAggregationLevel=0 AND @IdField=0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0 AND @IdField>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdField>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	DECLARE @FieldCount INT;
	SELECT @FieldCount=COUNT([IdField]) FROM #FieldList
	
	/** Get Chart Columns to Temp Table **/
	CREATE TABLE #AccountList (ColumnName NVARCHAR(255) PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF @IdStructure=1
	BEGIN
		INSERT INTO #AccountList SELECT [RootZiffAccount] FROM tblCalcProjectionTechnical WHERE IdField IN (SELECT [IdField] FROM #FieldList) AND [IdModel]=@IdModel GROUP BY [RootZiffAccount] ORDER BY [RootZiffAccount];
	END
	ELSE IF @IdStructure=2
	BEGIN
		INSERT INTO #AccountList SELECT [Activity] FROM tblCalcProjectionTechnical WHERE IdField IN (SELECT [IdField] FROM #FieldList) AND [IdModel]=@IdModel GROUP BY [Activity], [SortOrder] ORDER BY [SortOrder], [Activity];
	END
	
	/** Generate Chart Columns from Temp Table **/
	DECLARE @AccountColumnList NVARCHAR(MAX)='';
	DECLARE @AccountName NVARCHAR(255);
	DECLARE AccountList CURSOR FOR
	SELECT ColumnName FROM #AccountList;

	OPEN AccountList;
	FETCH NEXT FROM AccountList INTO @AccountName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @AccountColumnList=''
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + '[' + @AccountName + ']';
		END
		ELSE
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + ',[' + @AccountName + ']';
		END
		FETCH NEXT FROM AccountList INTO @AccountName;
	END

	CLOSE AccountList;
	DEALLOCATE AccountList;
	
	/** Get Currency Factor to use **/
	DECLARE @CurrencyFactor FLOAT;
	SELECT @CurrencyFactor=[Value] FROM tblModelsCurrencies WHERE [IdCurrency]=@IdCurrency AND [IdModel]=@IdModel;
	
	/** Build Technical Driver Factors (used for KPI Report) **/
	CREATE TABLE #TechDriver ([Year] INT, [Normal] FLOAT, [Optimistic] FLOAT, [Pessimistic] FLOAT) ON [PRIMARY]
	IF @IdTechnicalDriver=0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, 1 AS [Normal], 1 AS [Optimistic], 1 AS [Pessimistic]
		FROM tblCalcModelYears
		WHERE [IdModel]=@IdModel AND [Year] BETWEEN @StartYear AND @EndYear;
	END
	ELSE IF @IdTechnicalDriver>0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, SUM([Normal]) AS [Normal], SUM([Pessimistic]) AS [Pessimistic], SUM([Optimistic]) AS [Optimistic]
		FROM tblTechnicalDriverData
		WHERE [IdTechnicalDriver]=@IdTechnicalDriver AND [IdModel]=@IdModel AND IdField IN (SELECT [IdField] FROM #FieldList)
		GROUP BY Year;
	END
	
	/** Build Volume Tables **/
	/** Report (Baseline)**/
	CREATE TABLE #VolumeReportBLRows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLRows ([ParentName], [Name], [YearProjection], [Amount])	
		SELECT	DISTINCT N'All Cost Accounts' + 
			CASE WHEN @IdTechnicalScenario=1 THEN 'Base' 
				WHEN @IdTechnicalScenario=2 THEN 'Optimistic'
	 			WHEN @IdTechnicalScenario=3 THEN 'Pessimistic'
			END AS ParentName, dbo.tblProjects.Name AS Name, 
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN ISNULL(dbo.tblTechnicalDriverData.Normal,0)
				WHEN @IdTechnicalScenario=2 THEN ISNULL(dbo.tblTechnicalDriverData.Optimistic,0)
				WHEN @IdTechnicalScenario=3 THEN ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0)
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON
			dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=1
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	ORDER BY dbo.tblTechnicalDriverData.Year
	
	CREATE TABLE #VolumeReportBL ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBL([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBLRows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBLList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBL WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #BLList to Fill Properly **/
	INSERT INTO #VolumeReportBL ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBLList.ParentName,#VolumeReportBLList.Name, #VolumeReportBLList.YearProjection,0 AS Amount
	FROM	#VolumeReportBLList LEFT OUTER JOIN
			#VolumeReportBL ON #VolumeReportBLList.ParentName=#VolumeReportBL.ParentName AND #VolumeReportBLList.Name=#VolumeReportBL.Name AND #VolumeReportBLList.YearProjection=#VolumeReportBL.YearProjection
	WHERE	#VolumeReportBL.YearProjection IS NULL;

	/** Report (Business Opportunities)**/
	CREATE TABLE #VolumeReportBORows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBORows ([ParentName], [Name], [YearProjection], [Amount])	
		SELECT	DISTINCT N'All Cost Accounts' + 
			CASE WHEN @IdTechnicalScenario=1 THEN 'Base' 
				WHEN @IdTechnicalScenario=2 THEN 'Optimistic'
	 			WHEN @IdTechnicalScenario=3 THEN 'Pessimistic'
			END AS ParentName, dbo.tblProjects.Name AS Name, 
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN ISNULL(dbo.tblTechnicalDriverData.Normal,0)
				WHEN @IdTechnicalScenario=2 THEN ISNULL(dbo.tblTechnicalDriverData.Optimistic,0)
				WHEN @IdTechnicalScenario=3 THEN ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0)
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON
			dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=2
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	ORDER BY dbo.tblTechnicalDriverData.Year

	CREATE TABLE #VolumeReportBO ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBO([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBORows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBOList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBOList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBO WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #BOList to Fill Properly **/
	INSERT INTO #VolumeReportBO ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBOList.ParentName,#VolumeReportBOList.Name, #VolumeReportBOList.YearProjection,0 AS Amount
	FROM	#VolumeReportBOList LEFT OUTER JOIN
			#VolumeReportBO ON #VolumeReportBOList.ParentName=#VolumeReportBO.ParentName AND #VolumeReportBOList.Name=#VolumeReportBO.Name AND #VolumeReportBOList.YearProjection=#VolumeReportBO.YearProjection
	WHERE	#VolumeReportBO.YearProjection IS NULL;

	/** Report (All Fields Grouped)**/
	CREATE TABLE #VolumeReportGroupedBL ([YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBL ([YearProjection], [Amount])
	SELECT YearProjection,SUM(amount) AS Amount
	FROM #VolumeReportBL
	GROUP BY YearProjection
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGroupedBO ([YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBO ([YearProjection], [Amount])
	SELECT YearProjection,SUM(amount) AS Amount
	FROM #VolumeReportBO
	GROUP BY YearProjection
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGrouped ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255),[YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGrouped ([ParentName], [Name], [YearProjection], [Amount])
	SELECT N'All Cost Accounts' AS ParentName, 
		   CASE WHEN @IdTechnicalScenario=1 THEN 'Base' 
				WHEN @IdTechnicalScenario=2 THEN 'Optimistic'
				WHEN @IdTechnicalScenario=3 THEN 'Pessimistic'
		   END AS Name,
		   #VolumeReportGroupedBL.YearProjection,
		   #VolumeReportGroupedBL.Amount + #VolumeReportGroupedBO.Amount AS AMOUNT
	FROM #VolumeReportGroupedBL INNER JOIN	#VolumeReportGroupedBO ON #VolumeReportGroupedBO.YearProjection = #VolumeReportGroupedBL.YearProjection		   

	DECLARE @TotalCapacityDriver INT = 0;

	/** Get grouped driver value for Base Year **/
	SELECT @TotalCapacityDriver = Amount FROM #VolumeReportGrouped WHERE YearProjection=@BaseYear

	/** Build Utilization Capacity Values **/
	CREATE TABLE #UtilizationCapacityValues ([CodeZiff] NVARCHAR(50), [IdField] INT, [UtilizationValue] FLOAT) ON [PRIMARY]
	INSERT INTO #UtilizationCapacityValues([CodeZiff], [IdField], [UtilizationValue])
	SELECT  A.[CodeZiff], tblFields.IdField, @TotalCapacityDriver/A.UtilizationValue*100
	  FROM tblUtilizationCapacity A INNER JOIN tblFields
			ON A.IdField = tblFields.IdField      
	WHERE  	A.IdModel = @IdModel
	AND		A.IdField = @IdField

	/** Build Source Data Table - Total **/
	CREATE TABLE #Opex (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #Opex ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS ParentName, 
		CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
		tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
		) AS CostProjection
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList)
	GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END, tblCalcProjectionTechnical.Year;
	
	/** Build Source Data Table - Chart **/
	CREATE TABLE #Chart (ParentName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #Chart ([ParentName],[YearProjection],[CostProjection])
	SELECT CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS ParentName, 
		tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
		) AS CostProjection
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, tblCalcProjectionTechnical.Year;
	
	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #ChartList (ParentName NVARCHAR(255), YearProjection INT) ON [PRIMARY]
	INSERT INTO #ChartList ([ParentName],[YearProjection])
	SELECT CNList.ParentName, CYList.Year FROM
		(SELECT [ParentName] FROM #Chart GROUP BY [ParentName])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList
	
	/** Add Any Missing Years to Make the Chart Fill Properly (eg. Fields with only Cyclical Drivers) **/
	INSERT INTO #Chart ([ParentName],[YearProjection],[CostProjection])
	SELECT #ChartList.ParentName, #ChartList.YearProjection, NULL AS CostProjection FROM
		#ChartList LEFT OUTER JOIN
		#Chart ON #ChartList.ParentName=#Chart.ParentName AND #ChartList.YearProjection=#Chart.YearProjection
	WHERE #Chart.YearProjection IS NULL;	
	
	IF @IdTechnicalDriver=0
	BEGIN
		/** Build Source Data Table - Baseline and Business Opportunities (No Techical KPI Required) **/
		CREATE TABLE #BL (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
		CREATE TABLE #BO (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
		IF @FieldCount = 1
		BEGIN
			/** Baseline **/
			INSERT INTO #BL ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=1 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, tblCalcProjectionTechnical.Year;
			
			/** Business Opportunities **/
			INSERT INTO #BO ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=2 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, tblCalcProjectionTechnical.Year;
		END
		ELSE IF @FieldCount <> 1
		BEGIN
			/** Baseline **/
			INSERT INTO #BL ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, Project AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=1 
			GROUP BY tblCalcProjectionTechnical.Project, tblCalcProjectionTechnical.Year;
			
			/** Business Opportunities **/
			INSERT INTO #BO ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, Project Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=2 
			GROUP BY tblCalcProjectionTechnical.Project, tblCalcProjectionTechnical.Year;
		END
		
		/* NEED to do calculations for SEMI-VARIABLE */
		
	END

	/** Add Unit Cost factor, IF NECESSARY **/
	IF @IdCostType = 2
	BEGIN		 
		IF @IdField <>0
		BEGIN		
			UPDATE	#BL
			SET		#BL.CostProjection = #BL.CostProjection/#VolumeReportBL.Amount
			FROM	#BL INNER JOIN #VolumeReportBL ON
					#BL.YearProjection = #VolumeReportBL.YearProjection 

			UPDATE	#BO
			SET		#BO.CostProjection = #BO.CostProjection/#VolumeReportBO.Amount
			FROM	#BO INNER JOIN #VolumeReportBO ON
					#BO.YearProjection = #VolumeReportBO.YearProjection 
		END
		ELSE
		BEGIN
			UPDATE	#BL
			SET		#BL.CostProjection = #BL.CostProjection/#VolumeReportBL.Amount
			FROM	#BL INNER JOIN #VolumeReportBL ON
					#BL.YearProjection = #VolumeReportBL.YearProjection AND #BL.Name=#VolumeReportBL.Name

			UPDATE	#BO
			SET		#BO.CostProjection = #BO.CostProjection/#VolumeReportBO.Amount
			FROM	#BO INNER JOIN #VolumeReportBO ON
					#BO.YearProjection = #VolumeReportBO.YearProjection AND #BO.Name=#VolumeReportBO.Name
		END
		UPDATE	#Opex
		SET		#Opex.CostProjection = #Opex.CostProjection/#VolumeReportGrouped.Amount
		FROM	#Opex INNER JOIN #VolumeReportGrouped ON
				#Opex.YearProjection = #VolumeReportGrouped.YearProjection
	END
		
	/** Build Output Table **/
	DECLARE @SQLOpex NVARCHAR(MAX);
	SET @SQLOpex = 'SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #Opex PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
	EXEC(@SQLOpex);
	DECLARE @SQLOpexChart NVARCHAR(MAX);
	SET @SQLOpexChart = 'SELECT [YearProjection] AS [Year],' + @AccountColumnList + ' FROM #Chart PIVOT (SUM(CostProjection) FOR ParentName IN (' + @AccountColumnList + ')) AS PivotTable ORDER BY [YearProjection]';
	EXEC(@SQLOpexChart);
	IF @IdTechnicalDriver=0
	BEGIN
		DECLARE @SQLOpexBL NVARCHAR(MAX);
		SET @SQLOpexBL = 'SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #BL PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
		EXEC(@SQLOpexBL);
		DECLARE @SQLOpexBO NVARCHAR(MAX);
		SET @SQLOpexBO = 'SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #BO PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
		EXEC(@SQLOpexBO);
	END

	DECLARE @SQLVolumeTotal NVARCHAR(MAX);
	SET @SQLVolumeTotal = 'SELECT [ParentName], [Name],' + @YearColumnList + ' FROM #VolumeReportGrouped PIVOT (SUM(Amount) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
	EXEC(@SQLVolumeTotal);

	DECLARE @SQLVolumeBL NVARCHAR(MAX);
	SET @SQLVolumeBL = 'SELECT [ParentName], [Name],' + @YearColumnList + ' FROM #VolumeReportBL PIVOT (SUM(Amount) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
	EXEC(@SQLVolumeBL);
	
	DECLARE @SQLVolumeBO NVARCHAR(MAX);
	SET @SQLVolumeBO = 'SELECT [ParentName], [Name],' + @YearColumnList + ' FROM #VolumeReportBO PIVOT (SUM(Amount) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName],[Name]';
	EXEC(@SQLVolumeBO);
	
	/** Output Stats Table for UI **/
	IF @IdTechnicalDriver=0
	BEGIN
		SELECT @FieldCount AS FieldCount;
	END
	
END

USE [DBCPM]
GO

/****** Object:  Table [dbo].[tblZiffInternalAnalysis]    Script Date: 06/11/2015 08:58:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblZiffInternalAnalysis](
	[IdZiffInternalAnalysis] [int] IDENTITY(1,1) NOT NULL,
	[IdModel] [int] NULL,
	[IdZiffAccount] [int] NULL,
	[AttachFile] [varbinary](max) NULL,
	[FileName] [varchar](200) NULL,
	[UserCreation] [int] NULL,
	[DateCreation] [datetime] NULL,
	[UserModification] [int] NULL,
	[DateModification] [datetime] NULL,
 CONSTRAINT [PK_tblZiffInternalAnalysis] PRIMARY KEY CLUSTERED 
(
	[IdZiffInternalAnalysis] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tblZiffInternalAnalysis]  WITH CHECK ADD  CONSTRAINT [FK_tblZiffInternalAnalysis_tblModels] FOREIGN KEY([IdModel])
REFERENCES [dbo].[tblModels] ([IdModel])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tblZiffInternalAnalysis] CHECK CONSTRAINT [FK_tblZiffInternalAnalysis_tblModels]
GO

USE [DBCPM]
GO
/****** Object:  StoredProcedure [dbo].[allpInsertZiffInternalAnalysis]    Script Date: 06/11/2015 08:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*---------------------------------------------------------------
Autor   		: Rodrigo Manubens
Fecha Creacion	: 06/11/2015
Descripcion     : Add files to tblZiffInternalAnalysis
---------------------------------------------------------------*/
CREATE PROC [dbo].[allpInsertZiffInternalAnalysis](
@IdZiffInternalAnalysis numeric output,
@IdModel int,
@IdZiffAccount int,
@AttachFile varbinary(Max),
@FileName varchar(200),
@UserCreation int,
@DateCreation datetime,
@UserModification int,
@DateModification datetime)
AS
BEGIN
	
	INSERT INTO tblZiffInternalAnalysis ([IdModel],[IdZiffAccount],[AttachFile],[FileName],[UserCreation],[DateCreation],[UserModification],[DateModification])
	VALUES(@IdModel,@IdZiffAccount,@AttachFile,@FileName,@UserCreation,@DateCreation,@UserModification,@DateModification);
	
	SET @IdZiffInternalAnalysis = SCOPE_IDENTITY();
	
END

USE [DBCPM]
GO

USE [DBCPM]
GO

/****** Object:  StoredProcedure [dbo].[allpUpdateZiffInternalAnalysis]    Script Date: 06/11/2015 09:09:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------
Autor   		: Rodrigo Manubens
Fecha Creacion	: 06/11/2015
Descripcion     : Update file in table tblZiffInternalAnalysis
-----------------------------------------------------------------*/
CREATE PROC [dbo].[allpUpdateZiffInternalAnalysis](
@IdZiffInternalAnalysis numeric output,
@IdModel int,
@IdZiffAccount int,
@AttachFile varbinary(Max),
@FileName varchar(200),
@UserCreation int,
@DateCreation datetime,
@UserModification int,
@DateModification datetime)
AS 
BEGIN
  UPDATE tblZiffInternalAnalysis SET IdModel=@IdModel, IdZiffAccount=@IdZiffAccount,AttachFile=@AttachFile,[FileName]=@FileName, UserCreation=@UserCreation,DateCreation=@DateCreation,UserModification=@UserModification,DateModification=@DateModification
      WHERE IdZiffInternalAnalysis =@IdZiffInternalAnalysis
END

GO

USE [DBCPM]
GO

/****** Object:  StoredProcedure [dbo].[allpDeleteZiffInternalAnalysis]    Script Date: 06/11/2015 09:11:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------
Autor   		: Rodrigo Manubens
Fecha Creacion	: 06/11/2015
Descripcion     : Delete file in table tblZiffInternalAnalysis
-----------------------------------------------------------------*/
CREATE PROC [dbo].[allpDeleteZiffInternalAnalysis](
@IdZiffInternalAnalysis int
)
AS 
BEGIN 
   DELETE FROM tblZiffInternalAnalysis   WHERE IdZiffInternalAnalysis =@IdZiffInternalAnalysis
END

GO

USE [DBCPM]
GO

/****** Object:  View [dbo].[vListZiffInternalAnalysis]    Script Date: 06/11/2015 09:21:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vListZiffInternalAnalysis]
AS
SELECT     dbo.tblZiffInternalAnalysis.IdZiffInternalAnalysis, dbo.tblZiffInternalAnalysis.IdModel, dbo.tblZiffInternalAnalysis.IdZiffAccount, 
                      dbo.tblZiffInternalAnalysis.AttachFile, dbo.tblZiffInternalAnalysis.FileName, dbo.tblZiffInternalAnalysis.UserCreation, dbo.tblZiffInternalAnalysis.DateCreation, 
                      dbo.tblZiffInternalAnalysis.UserModification, dbo.tblZiffInternalAnalysis.DateModification, dbo.tblZiffAccounts.Code AS CodeZiffAccount, 
                      dbo.tblZiffAccounts.Name AS NameZiffAccount
FROM         dbo.tblZiffInternalAnalysis LEFT OUTER JOIN
                      dbo.tblZiffAccounts ON dbo.tblZiffInternalAnalysis.IdZiffAccount = dbo.tblZiffAccounts.IdZiffAccount

GO

/****** Object:  Table [dbo].[tblZiffExternalAnalysis]    Script Date: 06/11/2015 08:58:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblZiffExternalAnalysis](
	[IdZiffExternalAnalysis] [int] IDENTITY(1,1) NOT NULL,
	[IdModel] [int] NULL,
	[IdZiffAccount] [int] NULL,
	[AttachFile] [varbinary](max) NULL,
	[FileName] [varchar](200) NULL,
	[UserCreation] [int] NULL,
	[DateCreation] [datetime] NULL,
	[UserModification] [int] NULL,
	[DateModification] [datetime] NULL,
 CONSTRAINT [PK_tblZiffExternalAnalysis] PRIMARY KEY CLUSTERED 
(
	[IdZiffExternalAnalysis] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tblZiffExternalAnalysis]  WITH CHECK ADD  CONSTRAINT [FK_tblZiffExternalAnalysis_tblModels] FOREIGN KEY([IdModel])
REFERENCES [dbo].[tblModels] ([IdModel])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tblZiffExternalAnalysis] CHECK CONSTRAINT [FK_tblZiffExternalAnalysis_tblModels]
GO

USE [DBCPM]
GO
/****** Object:  StoredProcedure [dbo].[allpInsertZiffExternalAnalysis]    Script Date: 06/11/2015 08:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*---------------------------------------------------------------
Autor   		: Rodrigo Manubens
Fecha Creacion	: 06/11/2015
Descripcion     : Add files to tblZiffExternalAnalysis
---------------------------------------------------------------*/
CREATE PROC [dbo].[allpInsertZiffExternalAnalysis](
@IdZiffExternalAnalysis numeric output,
@IdModel int,
@IdZiffAccount int,
@AttachFile varbinary(Max),
@FileName varchar(200),
@UserCreation int,
@DateCreation datetime,
@UserModification int,
@DateModification datetime)
AS
BEGIN
	
	INSERT INTO tblZiffExternalAnalysis ([IdModel],[IdZiffAccount],[AttachFile],[FileName],[UserCreation],[DateCreation],[UserModification],[DateModification])
	VALUES(@IdModel,@IdZiffAccount,@AttachFile,@FileName,@UserCreation,@DateCreation,@UserModification,@DateModification);
	
	SET @IdZiffExternalAnalysis = SCOPE_IDENTITY();
	
END

USE [DBCPM]
GO

/****** Object:  StoredProcedure [dbo].[allpUpdateZiffExternalAnalysis]    Script Date: 06/11/2015 09:09:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------
Autor   		: Rodrigo Manubens
Fecha Creacion	: 06/11/2015
Descripcion     : Update file in table tblZiffExternalAnalysis
-----------------------------------------------------------------*/
CREATE PROC [dbo].[allpUpdateZiffExternalAnalysis](
@IdZiffExternalAnalysis numeric output,
@IdModel int,
@IdZiffAccount int,
@AttachFile varbinary(Max),
@FileName varchar(200),
@UserCreation int,
@DateCreation datetime,
@UserModification int,
@DateModification datetime)
AS 
BEGIN
  UPDATE tblZiffExternalAnalysis SET IdModel=@IdModel, IdZiffAccount=@IdZiffAccount,AttachFile=@AttachFile,[FileName]=@FileName, UserCreation=@UserCreation,DateCreation=@DateCreation,UserModification=@UserModification,DateModification=@DateModification
      WHERE IdZiffExternalAnalysis =@IdZiffExternalAnalysis
END

GO

USE [DBCPM]
GO

/****** Object:  StoredProcedure [dbo].[allpDeleteZiffExternalAnalysis]    Script Date: 06/11/2015 09:11:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*-----------------------------------------------------------------
Autor   		: Rodrigo Manubens
Fecha Creacion	: 06/11/2015
Descripcion     : Delete file in table tblZiffExternalAnalysis
-----------------------------------------------------------------*/
CREATE PROC [dbo].[allpDeleteZiffExternalAnalysis](
@IdZiffExternalAnalysis int
)
AS 
BEGIN 
   DELETE FROM tblZiffExternalAnalysis   WHERE IdZiffExternalAnalysis =@IdZiffExternalAnalysis
END

GO

USE [DBCPM]
GO

/****** Object:  View [dbo].[vListZiffExternalAnalysis]    Script Date: 06/11/2015 09:21:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vListZiffExternalAnalysis]
AS
SELECT     dbo.tblZiffExternalAnalysis.IdZiffExternalAnalysis, dbo.tblZiffExternalAnalysis.IdModel, dbo.tblZiffExternalAnalysis.IdZiffAccount, 
                      dbo.tblZiffExternalAnalysis.AttachFile, dbo.tblZiffExternalAnalysis.FileName, dbo.tblZiffExternalAnalysis.UserCreation, dbo.tblZiffExternalAnalysis.DateCreation, 
                      dbo.tblZiffExternalAnalysis.UserModification, dbo.tblZiffExternalAnalysis.DateModification, dbo.tblZiffAccounts.Code AS CodeZiffAccount, 
                      dbo.tblZiffAccounts.Name AS NameZiffAccount
FROM         dbo.tblZiffExternalAnalysis LEFT OUTER JOIN
                      dbo.tblZiffAccounts ON dbo.tblZiffExternalAnalysis.IdZiffAccount = dbo.tblZiffAccounts.IdZiffAccount

GO

USE [DBCPM]
GO
/****** Object:  StoredProcedure [dbo].[allpListCostStructureAssumptionsByProject]    Script Date: 06/11/2015 10:26:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-06-11
-- Description:	List Cost Structure Assumptions By Project as the view will no longer work as we are adding a row to display the parent
-- ========================================================================================================================================

CREATE PROCEDURE [dbo].[allpListCostStructureAssumptionsByProject](
 @IdModel int,
 @IdField int
)
AS
BEGIN
	
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#CostStructureAssumptions') IS NOT NULL DROP TABLE #CostStructureAssumptions

	CREATE TABLE #CostStructureAssumptions (IdModel INT, IdCostStructureAssumptions INT, IdProject INT, IdZiffAccount INT, Code NVARCHAR(50), Name NVARCHAR(255), ParentCode NVARCHAR(50), ParentName NVARCHAR(255), Client INT, Ziff INT , IdField INT, SortOrder NVARCHAR(100))
	INSERT INTO #CostStructureAssumptions (IdModel, IdCostStructureAssumptions, IdProject, IdZiffAccount, Code, Name, ParentCode, ParentName, Client, Ziff, IdField, SortOrder)
	SELECT	tblZiffAccountsParent.IdModel, tblCostStructureAssumptionsByProject.IdCostStructureAssumptions, tblCostStructureAssumptionsByProject.IdProject, 
			tblCostStructureAssumptionsByProject.IdZiffAccount, tblZiffAccounts.Code, tblZiffAccounts.Name, tblZiffAccounts.Code AS ParentCode, 
			tblZiffAccountsParent.Name AS ParentName, tblCostStructureAssumptionsByProject.Client, tblCostStructureAssumptionsByProject.Ziff, 
			tblProjects.IdField, tblZiffAccounts.SortOrder
	FROM    tblCostStructureAssumptionsByProject LEFT OUTER JOIN
			tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject LEFT OUTER JOIN
			tblZiffAccounts ON tblCostStructureAssumptionsByProject.IdZiffAccount = tblZiffAccounts.IdZiffAccount LEFT OUTER JOIN
			tblZiffAccounts AS tblZiffAccountsParent ON tblZiffAccounts.Parent = tblZiffAccountsParent.IdZiffAccount
	WHERE  	tblProjects.IdModel = @IdModel
	AND		tblProjects.IdField = @IdField 

	INSERT INTO #CostStructureAssumptions (IdModel, IdCostStructureAssumptions, IdProject, IdZiffAccount, Code, Name, ParentCode, ParentName, Client, Ziff, IdField, SortOrder)
	SELECT NULL, NULL, NULL, NULL, dbo.tblZiffAccounts.Code, NULL, NULL, NULL, NULL, NULL, NULL, dbo.tblZiffAccounts.SortOrder
	FROM tblZiffAccounts
	WHERE  	IdModel = @IdModel AND Parent IS NULL

	/** Build Output Table **/
	SELECT IdModel, IdCostStructureAssumptions, IdProject, IdZiffAccount, Code, Name, ParentCode, ParentName, Client, Ziff, IdField, SortOrder 
	FROM #CostStructureAssumptions 
	ORDER BY SortOrder
END
USE [DBCPM]
GO
/****** Object:  StoredProcedure [dbo].[reppOpexProjection]    Script Date: 06/11/2015 15:59:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================================
-- Author:		Gareth Slater
-- Create date: 2014-05-05
-- Description:	Module Report Opex Projection
-- ===========================================================================================
-- Modifications:	
-- Date			Author	Description
-- ---------	------	----------------------------------------------------------------------
-- 6/10/2015	RM		Added @Factor parameter
-- ===========================================================================================
ALTER PROCEDURE [dbo].[reppOpexProjection](


@IdModel INT,
@IdAggregationLevel INT,
@IdField INT,
@IdStructure INT,
@From INT,
@To INT,
@IdTechnicalScenario INT,
@IdEconomicScenario INT,
@IdCurrency INT,
@IdTerm INT,
@IdTechnicalDriver INT,
@IdCostType INT,
@IdTypeOperation INT,
@Factor INT
)
AS
BEGIN
	DECLARE @StartYear INT = @From;
	DECLARE @EndYear INT = @To;
	
	/** Get Base Year **/
	DECLARE @BaseYear INT
	SELECT @BaseYear = [BaseYear] FROM [DBCPM].[dbo].[tblModels] WHERE IdModel=@IdModel

	/** Build Year Columns **/
	DECLARE @YearColumnList NVARCHAR(MAX)='';	
	WHILE (@From <= @To)
	BEGIN
		SET @YearColumnList =  @YearColumnList + '[' + CAST(@From AS VARCHAR(4)) + ']';
		SET @From = @From + 1;
		IF (@From <= @To)
		BEGIN
			SET @YearColumnList = @YearColumnList + ',';
		END
	END
	
	/** Build Field List **/
	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF (@IdAggregationLevel=0 AND @IdField=0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0 AND @IdField>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdAggregationLevel>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdAggregationLevel]=@IdAggregationLevel AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	ELSE IF (@IdField>0)
	BEGIN
		INSERT INTO #FieldList SELECT [IdField] FROM tblCalcAggregationLevelsField WHERE [IdField]=@IdField AND [IdModel]=@IdModel GROUP BY [IdField];
	END
	DECLARE @FieldCount INT;
	SELECT @FieldCount=COUNT([IdField]) FROM #FieldList
	
	/** Get Chart Columns to Temp Table **/
	CREATE TABLE #AccountList (ColumnName NVARCHAR(255) PRIMARY KEY NOT NULL) ON [PRIMARY]
	IF @IdStructure=1
	BEGIN
		INSERT INTO #AccountList SELECT [RootZiffAccount] FROM tblCalcProjectionTechnical WHERE IdField IN (SELECT [IdField] FROM #FieldList) AND [IdModel]=@IdModel GROUP BY [RootZiffAccount] ORDER BY [RootZiffAccount];
	END
	ELSE IF @IdStructure=2
	BEGIN
		INSERT INTO #AccountList SELECT [Activity] FROM tblCalcProjectionTechnical WHERE IdField IN (SELECT [IdField] FROM #FieldList) AND [IdModel]=@IdModel GROUP BY [Activity], [SortOrder] ORDER BY [SortOrder], [Activity];
	END
	
	/** Generate Chart Columns from Temp Table **/
	DECLARE @AccountColumnList NVARCHAR(MAX)='';
	DECLARE @AccountName NVARCHAR(255);
	DECLARE AccountList CURSOR FOR
	SELECT ColumnName FROM #AccountList;

	OPEN AccountList;
	FETCH NEXT FROM AccountList INTO @AccountName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @AccountColumnList=''
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + '[' + @AccountName + ']';
		END
		ELSE
		BEGIN
			SET @AccountColumnList =  @AccountColumnList + ',[' + @AccountName + ']';
		END
		FETCH NEXT FROM AccountList INTO @AccountName;
	END

	CLOSE AccountList;
	DEALLOCATE AccountList;
	
	/** Get Currency Factor to use **/
	DECLARE @CurrencyFactor FLOAT;
	SELECT @CurrencyFactor=[Value] FROM tblModelsCurrencies WHERE [IdCurrency]=@IdCurrency AND [IdModel]=@IdModel;
	
	/** Build Technical Driver Factors (used for KPI Report) **/
	CREATE TABLE #TechDriver ([Year] INT, [Normal] FLOAT, [Optimistic] FLOAT, [Pessimistic] FLOAT) ON [PRIMARY]
	IF @IdTechnicalDriver=0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, 1 AS [Normal], 1 AS [Optimistic], 1 AS [Pessimistic]
		FROM tblCalcModelYears
		WHERE [IdModel]=@IdModel AND [Year] BETWEEN @StartYear AND @EndYear;
	END
	ELSE IF @IdTechnicalDriver>0
	BEGIN
		INSERT INTO #TechDriver ([Year], [Normal], [Optimistic], [Pessimistic])
		SELECT Year, SUM([Normal]) AS [Normal], SUM([Pessimistic]) AS [Pessimistic], SUM([Optimistic]) AS [Optimistic]
		FROM tblTechnicalDriverData
		WHERE [IdTechnicalDriver]=@IdTechnicalDriver AND [IdModel]=@IdModel AND IdField IN (SELECT [IdField] FROM #FieldList)
		GROUP BY Year;
	END
	
	/** Build Volume Tables **/
	/** Report (Baseline)**/
	CREATE TABLE #VolumeReportBLRows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLRows ([ParentName], [Name], [YearProjection], [Amount])	
		SELECT	DISTINCT N'All Cost Accounts' + 
			CASE WHEN @IdTechnicalScenario=1 THEN 'Base' 
				WHEN @IdTechnicalScenario=2 THEN 'Optimistic'
	 			WHEN @IdTechnicalScenario=3 THEN 'Pessimistic'
			END AS ParentName, dbo.tblProjects.Name AS Name, 
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN ISNULL(dbo.tblTechnicalDriverData.Normal,0)
				WHEN @IdTechnicalScenario=2 THEN ISNULL(dbo.tblTechnicalDriverData.Optimistic,0)
				WHEN @IdTechnicalScenario=3 THEN ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0)
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON
			dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=1
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	ORDER BY dbo.tblTechnicalDriverData.Year
	
	CREATE TABLE #VolumeReportBL ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBL([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBLRows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBLList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBLList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBL WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #BLList to Fill Properly **/
	INSERT INTO #VolumeReportBL ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBLList.ParentName,#VolumeReportBLList.Name, #VolumeReportBLList.YearProjection,0 AS Amount
	FROM	#VolumeReportBLList LEFT OUTER JOIN
			#VolumeReportBL ON #VolumeReportBLList.ParentName=#VolumeReportBL.ParentName AND #VolumeReportBLList.Name=#VolumeReportBL.Name AND #VolumeReportBLList.YearProjection=#VolumeReportBL.YearProjection
	WHERE	#VolumeReportBL.YearProjection IS NULL;

	/** Report (Business Opportunities)**/
	CREATE TABLE #VolumeReportBORows ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBORows ([ParentName], [Name], [YearProjection], [Amount])	
		SELECT	DISTINCT N'All Cost Accounts' + 
			CASE WHEN @IdTechnicalScenario=1 THEN 'Base' 
				WHEN @IdTechnicalScenario=2 THEN 'Optimistic'
	 			WHEN @IdTechnicalScenario=3 THEN 'Pessimistic'
			END AS ParentName, dbo.tblProjects.Name AS Name, 
			dbo.tblTechnicalDriverData.Year AS YearProjection, 
			CASE WHEN @IdTechnicalScenario=1 THEN ISNULL(dbo.tblTechnicalDriverData.Normal,0)
				WHEN @IdTechnicalScenario=2 THEN ISNULL(dbo.tblTechnicalDriverData.Optimistic,0)
				WHEN @IdTechnicalScenario=3 THEN ISNULL(dbo.tblTechnicalDriverData.Pessimistic,0)
			ELSE 0 END AS Amount
	FROM	dbo.tblTechnicalDriverData INNER JOIN 
			dbo.tblTechnicalDrivers ON
			dbo.tblTechnicalDrivers.IdTechnicalDriver = dbo.tblTechnicalDriverData.IdTechnicalDriver AND
			dbo.tblTechnicalDrivers.IdModel = dbo.tblTechnicalDriverData.IdModel INNER JOIN
			dbo.tblProjects ON dbo.tblProjects.IdModel = dbo.tblTechnicalDriverData.IdModel AND
			dbo.tblProjects.IdField = dbo.tblTechnicalDriverData.IdField AND
			dbo.tblProjects.IdProject = dbo.tblTechnicalDriverData.IdProject
	WHERE	dbo.tblTechnicalDriverData.IdModel = @IdModel
	AND		dbo.tblTechnicalDriverData.IdField IN (SELECT [IdField] FROM #FieldList)  
	AND		dbo.tblTechnicalDrivers.UnitCostDenominator = 1 
	AND		dbo.tblProjects.[Operation]=2
	AND		dbo.tblTechnicalDriverData.Year BETWEEN @StartYear AND @EndYear
	ORDER BY dbo.tblTechnicalDriverData.Year

	CREATE TABLE #VolumeReportBO ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255), [YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBO([ParentName], [Name], [YearProjection], [Amount])
	SELECT	ParentName, Name, YearProjection, SUM(Amount)
	FROM	#VolumeReportBORows
	GROUP BY ParentName, Name, YearProjection
	ORDER BY ParentName, Name, YearProjection

	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #VolumeReportBOList (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportBOList ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CNList.ParentName, CNList.Name, CYList.Year, 0 FROM
		(SELECT [ParentName],[Name] FROM #VolumeReportBO WHERE [ParentName] IS NOT NULL GROUP BY [ParentName],[Name])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList 

	/** Add Any Missing Years to #BOList to Fill Properly **/
	INSERT INTO #VolumeReportBO ([ParentName],[Name],[YearProjection],[Amount])
	SELECT	#VolumeReportBOList.ParentName,#VolumeReportBOList.Name, #VolumeReportBOList.YearProjection,0 AS Amount
	FROM	#VolumeReportBOList LEFT OUTER JOIN
			#VolumeReportBO ON #VolumeReportBOList.ParentName=#VolumeReportBO.ParentName AND #VolumeReportBOList.Name=#VolumeReportBO.Name AND #VolumeReportBOList.YearProjection=#VolumeReportBO.YearProjection
	WHERE	#VolumeReportBO.YearProjection IS NULL;

	/** Report (All Fields Grouped)**/
	CREATE TABLE #VolumeReportGroupedBL ([YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBL ([YearProjection], [Amount])
	SELECT YearProjection,SUM(amount) AS Amount
	FROM #VolumeReportBL
	GROUP BY YearProjection
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGroupedBO ([YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGroupedBO ([YearProjection], [Amount])
	SELECT YearProjection,SUM(amount) AS Amount
	FROM #VolumeReportBO
	GROUP BY YearProjection
	ORDER BY YearProjection

	CREATE TABLE #VolumeReportGrouped ([ParentName] NVARCHAR(255), [Name] NVARCHAR(255),[YearProjection] INT, [Amount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumeReportGrouped ([ParentName], [Name], [YearProjection], [Amount])
	SELECT N'All Cost Accounts' AS ParentName, 
		   CASE WHEN @IdTechnicalScenario=1 THEN 'Base' 
				WHEN @IdTechnicalScenario=2 THEN 'Optimistic'
				WHEN @IdTechnicalScenario=3 THEN 'Pessimistic'
		   END AS Name,
		   #VolumeReportGroupedBL.YearProjection,
		   #VolumeReportGroupedBL.Amount + #VolumeReportGroupedBO.Amount AS AMOUNT
	FROM #VolumeReportGroupedBL INNER JOIN	#VolumeReportGroupedBO ON #VolumeReportGroupedBO.YearProjection = #VolumeReportGroupedBL.YearProjection		   

	DECLARE @TotalCapacityDriver INT = 0;

	/** Get grouped driver value for Base Year **/
	SELECT @TotalCapacityDriver = Amount FROM #VolumeReportGrouped WHERE YearProjection=@BaseYear

	/** Build Utilization Capacity Values **/
	CREATE TABLE #UtilizationCapacityValues ([CodeZiff] NVARCHAR(50), [IdField] INT, [UtilizationValue] FLOAT) ON [PRIMARY]
	INSERT INTO #UtilizationCapacityValues([CodeZiff], [IdField], [UtilizationValue])
	SELECT  A.[CodeZiff], tblFields.IdField, @TotalCapacityDriver/A.UtilizationValue*100
	  FROM tblUtilizationCapacity A INNER JOIN tblFields
			ON A.IdField = tblFields.IdField      
	WHERE  	A.IdModel = @IdModel
	AND		A.IdField = @IdField

	/** Build Source Data Table - Total **/
	CREATE TABLE #Opex (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #Opex ([ParentName],[Name],[YearProjection],[CostProjection])
	SELECT CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS ParentName, 
		CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END AS Name, 
		tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
		) AS CostProjection
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList)
	GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.ZiffAccount ELSE [Resource] END, tblCalcProjectionTechnical.Year;
	
	/** Build Source Data Table - Chart **/
	CREATE TABLE #Chart (ParentName NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
	INSERT INTO #Chart ([ParentName],[YearProjection],[CostProjection])
	SELECT CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS ParentName, 
		tblCalcProjectionTechnical.Year AS YearProjection, 
		SUM (
			CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
			WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
			WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
			ELSE 0 END 
			* 
			CASE WHEN @IdTerm=2 THEN 1
			WHEN @IdTerm=1 THEN
				CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
				WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
				WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
				ELSE 1 END 
			END
			* @CurrencyFactor / 
			CASE WHEN @IdTechnicalScenario=1 THEN #TechDriver.[Normal] 
			WHEN @IdTechnicalScenario=2 THEN #TechDriver.[Optimistic] 
			WHEN @IdTechnicalScenario=3 THEN #TechDriver.[Pessimistic] 
			ELSE 1 END 
			/ @Factor 
		) AS CostProjection
	FROM tblCalcProjectionTechnical LEFT OUTER JOIN
		#TechDriver ON tblCalcProjectionTechnical.Year = #TechDriver.Year LEFT OUTER JOIN
		tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
		tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
		tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
	WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND tblCalcProjectionTechnical.Year BETWEEN @StartYear AND @EndYear
	GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, tblCalcProjectionTechnical.Year;
	
	/** Get Full List of Years and ParentNames **/
	CREATE TABLE #ChartList (ParentName NVARCHAR(255), YearProjection INT) ON [PRIMARY]
	INSERT INTO #ChartList ([ParentName],[YearProjection])
	SELECT CNList.ParentName, CYList.Year FROM
		(SELECT [ParentName] FROM #Chart GROUP BY [ParentName])AS CNList CROSS JOIN
		(SELECT [Year] FROM tblCalcModelYears WHERE IdModel=@IdModel) AS CYList
	
	/** Add Any Missing Years to Make the Chart Fill Properly (eg. Fields with only Cyclical Drivers) **/
	INSERT INTO #Chart ([ParentName],[YearProjection],[CostProjection])
	SELECT #ChartList.ParentName, #ChartList.YearProjection, NULL AS CostProjection FROM
		#ChartList LEFT OUTER JOIN
		#Chart ON #ChartList.ParentName=#Chart.ParentName AND #ChartList.YearProjection=#Chart.YearProjection
	WHERE #Chart.YearProjection IS NULL;	
	
	IF @IdTechnicalDriver=0
	BEGIN
		/** Build Source Data Table - Baseline and Business Opportunities (No Techical KPI Required) **/
		CREATE TABLE #BL (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
		CREATE TABLE #BO (ParentName NVARCHAR(255), Name NVARCHAR(255), YearProjection INT, CostProjection FLOAT) ON [PRIMARY]
		IF @FieldCount = 1
		BEGIN
			/** Baseline **/
			INSERT INTO #BL ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=1 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, tblCalcProjectionTechnical.Year;
			
			/** Business Opportunities **/
			INSERT INTO #BO ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=2 
			GROUP BY CASE WHEN @IdStructure=1 THEN tblCalcProjectionTechnical.RootZiffAccount ELSE [Activity] END, tblCalcProjectionTechnical.Year;
		END
		ELSE IF @FieldCount <> 1
		BEGIN
			/** Baseline **/
			INSERT INTO #BL ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, Project AS Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=1 
			GROUP BY tblCalcProjectionTechnical.Project, tblCalcProjectionTechnical.Year;
			
			/** Business Opportunities **/
			INSERT INTO #BO ([ParentName],[Name],[YearProjection],[CostProjection])
			SELECT N'All Cost Accounts' AS ParentName, Project Name, 
				tblCalcProjectionTechnical.Year AS YearProjection, 
				SUM (
					CASE WHEN @IdTechnicalScenario=1 THEN tblCalcProjectionTechnical.TFNormal * tblCalcProjectionTechnical.BCRNormal 
					WHEN @IdTechnicalScenario=2 THEN tblCalcProjectionTechnical.TFOptimistic * tblCalcProjectionTechnical.BCROptimistic 
					WHEN @IdTechnicalScenario=3 THEN tblCalcProjectionTechnical.TFPessimistic * tblCalcProjectionTechnical.BCRPessimistic 
					ELSE 0 END 
					* 
					CASE WHEN @IdTerm=2 THEN 1
					WHEN @IdTerm=1 THEN
						CASE WHEN @IdEconomicScenario=1 THEN ISNULL(tblCalcProjectionEconomic.EFNormal,1)
						WHEN @IdEconomicScenario=2 THEN ISNULL(tblCalcProjectionEconomic.EFOptimistic,1)
						WHEN @IdEconomicScenario=3 THEN ISNULL(tblCalcProjectionEconomic.EFPessimistic,1) 
						ELSE 1 END 
					END
					* @CurrencyFactor
					/ @Factor 
				) AS CostProjection
			FROM tblCalcProjectionTechnical LEFT OUTER JOIN
				tblCalcProjectionEconomic ON tblCalcProjectionTechnical.Year = tblCalcProjectionEconomic.Year AND 
				tblCalcProjectionTechnical.IdZiffAccount = tblCalcProjectionEconomic.IdZiffAccount AND 
				tblCalcProjectionTechnical.IdModel = tblCalcProjectionEconomic.IdModel
			WHERE tblCalcProjectionTechnical.IdModel=@IdModel AND tblCalcProjectionTechnical.IdField IN (SELECT [IdField] FROM #FieldList) AND [Operation]=2 
			GROUP BY tblCalcProjectionTechnical.Project, tblCalcProjectionTechnical.Year;
		END
		
		/* NEED to do calculations for SEMI-VARIABLE */
		
	END

	/** Add Unit Cost factor, IF NECESSARY **/
	IF @IdCostType = 2
	BEGIN		 
		IF @IdField <>0
		BEGIN		
			UPDATE	#BL
			SET		#BL.CostProjection = #BL.CostProjection/#VolumeReportBL.Amount
			FROM	#BL INNER JOIN #VolumeReportBL ON
					#BL.YearProjection = #VolumeReportBL.YearProjection 

			UPDATE	#BO
			SET		#BO.CostProjection = #BO.CostProjection/#VolumeReportBO.Amount
			FROM	#BO INNER JOIN #VolumeReportBO ON
					#BO.YearProjection = #VolumeReportBO.YearProjection 
		END
		ELSE
		BEGIN
			UPDATE	#BL
			SET		#BL.CostProjection = #BL.CostProjection/#VolumeReportBL.Amount
			FROM	#BL INNER JOIN #VolumeReportBL ON
					#BL.YearProjection = #VolumeReportBL.YearProjection AND #BL.Name=#VolumeReportBL.Name

			UPDATE	#BO
			SET		#BO.CostProjection = #BO.CostProjection/#VolumeReportBO.Amount
			FROM	#BO INNER JOIN #VolumeReportBO ON
					#BO.YearProjection = #VolumeReportBO.YearProjection AND #BO.Name=#VolumeReportBO.Name
		END
		UPDATE	#Opex
		SET		#Opex.CostProjection = #Opex.CostProjection/#VolumeReportGrouped.Amount
		FROM	#Opex INNER JOIN #VolumeReportGrouped ON
				#Opex.YearProjection = #VolumeReportGrouped.YearProjection
	END
		
	/** Build Output Table **/
	DECLARE @SQLOpex NVARCHAR(MAX);
	SET @SQLOpex = 'SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #Opex PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
	EXEC(@SQLOpex);
	DECLARE @SQLOpexChart NVARCHAR(MAX);
	SET @SQLOpexChart = 'SELECT [YearProjection] AS [Year],' + @AccountColumnList + ' FROM #Chart PIVOT (SUM(CostProjection) FOR ParentName IN (' + @AccountColumnList + ')) AS PivotTable ORDER BY [YearProjection]';
	EXEC(@SQLOpexChart);
	IF @IdTechnicalDriver=0
	BEGIN
		DECLARE @SQLOpexBL NVARCHAR(MAX);
		SET @SQLOpexBL = 'SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #BL PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
		EXEC(@SQLOpexBL);
		DECLARE @SQLOpexBO NVARCHAR(MAX);
		SET @SQLOpexBO = 'SELECT [ParentName],[Name],' + @YearColumnList + ' FROM #BO PIVOT (SUM(CostProjection) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
		EXEC(@SQLOpexBO);
	END

	DECLARE @SQLVolumeTotal NVARCHAR(MAX);
	SET @SQLVolumeTotal = 'SELECT [ParentName], [Name],' + @YearColumnList + ' FROM #VolumeReportGrouped PIVOT (SUM(Amount) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
	EXEC(@SQLVolumeTotal);

	DECLARE @SQLVolumeBL NVARCHAR(MAX);
	SET @SQLVolumeBL = 'SELECT [ParentName], [Name],' + @YearColumnList + ' FROM #VolumeReportBL PIVOT (SUM(Amount) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName], [Name]';
	EXEC(@SQLVolumeBL);
	
	DECLARE @SQLVolumeBO NVARCHAR(MAX);
	SET @SQLVolumeBO = 'SELECT [ParentName], [Name],' + @YearColumnList + ' FROM #VolumeReportBO PIVOT (SUM(Amount) FOR YearProjection IN (' + @YearColumnList + ')) AS PivotTable ORDER BY [ParentName],[Name]';
	EXEC(@SQLVolumeBO);
	
	/** Output Stats Table for UI **/
	IF @IdTechnicalDriver=0
	BEGIN
		SELECT @FieldCount AS FieldCount;
	END
	
END


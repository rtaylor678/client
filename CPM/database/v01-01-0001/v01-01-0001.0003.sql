USE [DBCPM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-06-17
-- Description:	List of External Base Cost References
-- ================================================================================================
CREATE PROCEDURE allpListExternalBaseCostReferences
	-- Add the parameters for the stored procedure here
	@IdModel INT,
	@IdField INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF OBJECT_ID('tempdb..#ExternalCostReferences') IS NOT NULL DROP TABLE #ExternalCostReferences

	CREATE TABLE #ExternalCostReferences (IdModel INT,IdZiffBaseCost INT,IdField INT,IdZiffAccount INT,IdPeerCriteria INT,UnitCost FLOAT,Quantity FLOAT,TotalCost FLOAT,
										  Field VARCHAR(150),CodeZiff VARCHAR(50),ZiffAccount VARCHAR(150),IdRootZiffAccount INT,RootCodeZiff VARCHAR(50),RootZiffAccount VARCHAR(150),
										  PeerCriteria VARCHAR(50),Unit VARCHAR(150),IdUnitPeerCriteria INT,TechApplicable BIT,TFNormal FLOAT,TFPessimistic FLOAT,TFOptimistic FLOAT,
										  BCRNormal FLOAT,BCRPessimistic FLOAT,BCROptimistic FLOAT, SortOrder VARCHAR(50))	
	INSERT INTO #ExternalCostReferences (IdModel,IdZiffBaseCost,IdField,IdZiffAccount,IdPeerCriteria,UnitCost,Quantity,TotalCost,Field,CodeZiff,ZiffAccount,IdRootZiffAccount,
										 RootCodeZiff,RootZiffAccount,PeerCriteria,Unit,IdUnitPeerCriteria,TechApplicable,TFNormal,TFPessimistic,TFOptimistic,BCRNormal,
										 BCRPessimistic,BCROptimistic, SortOrder)	
	SELECT CBCBFZ.IdModel,CBCBFZ.IdZiffBaseCost,CBCBFZ.IdField,CBCBFZ.IdZiffAccount,CBCBFZ.IdPeerCriteria,CBCBFZ.UnitCost,CBCBFZ.Quantity,CBCBFZ.TotalCost,CBCBFZ.Field,
		   '&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;'+CBCBFZ.CodeZiff as CodeZiff,CBCBFZ.ZiffAccount,CBCBFZ.IdRootZiffAccount,CBCBFZ.RootCodeZiff,CBCBFZ.RootZiffAccount,
		   CBCBFZ.PeerCriteria,CBCBFZ.Unit,CBCBFZ.IdUnitPeerCriteria,CBCBFZ.TechApplicable,CBCBFZ.TFNormal,CBCBFZ.TFPessimistic,CBCBFZ.TFOptimistic,CBCBFZ.BCRNormal,
		   CBCBFZ.BCRPessimistic,CBCBFZ.BCROptimistic,ZA.SortOrder 
	FROM tblCalcBaseCostByFieldZiff CBCBFZ left outer join tblZiffAccounts ZA on 
		 ZA.IdModel=CBCBFZ.IdModel and ZA.IdZiffAccount=CBCBFZ.IdZiffAccount 
	WHERE CBCBFZ.IdModel = @IdModel AND CBCBFZ.IdField = @IdField 
	UNION 
	SELECT IdModel,NULL, NULL, IdZiffAccount,NULL,-99,NULL,-99,NULL,Code,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,SortOrder 
	FROM tblZiffAccounts 
	WHERE IdModel = @IdModel 
	AND Parent IS NULL 

	SELECT * FROM #ExternalCostReferences 	ORDER BY SortOrder 
END
GO
-- Update tblLabelsLanguages table
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Login' WHERE  [IdLabel] = 1 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Username' WHERE  [IdLabel] = 2 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Password' WHERE  [IdLabel] = 3 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Language' WHERE  [IdLabel] = 4 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Remember me' WHERE  [IdLabel] = 5 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Forgot Password' WHERE  [IdLabel] = 6 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Login' WHERE  [IdLabel] = 7 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'The username or password is incorrect!' WHERE  [IdLabel] = 8 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'The username is incorrect!' WHERE  [IdLabel] = 9 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Mail sent successfully!' WHERE  [IdLabel] = 10 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'This field is required' WHERE  [IdLabel] = 11 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'The username is incorrect!' WHERE  [IdLabel] = 12 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Modify user data' WHERE  [IdLabel] = 13 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'User Management' WHERE  [IdLabel] = 14 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Roles and Permissions' WHERE  [IdLabel] = 15 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Log out' WHERE  [IdLabel] = 16 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Last Login' WHERE  [IdLabel] = 17 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'This field is required' WHERE  [IdLabel] = 19 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'User' WHERE  [IdLabel] = 20 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Username' WHERE  [IdLabel] = 21 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Name' WHERE  [IdLabel] = 22 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Password' WHERE  [IdLabel] = 23 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Email' WHERE  [IdLabel] = 24 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Save' WHERE  [IdLabel] = 25 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Cancel' WHERE  [IdLabel] = 26 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Application Home' WHERE  [IdLabel] = 27 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Modules' WHERE  [IdLabel] = 28 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Inicio de sesi�n' WHERE  [IdLabel] = 1 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Usuario' WHERE  [IdLabel] = 2 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Contrase�a' WHERE  [IdLabel] = 3 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Idioma' WHERE  [IdLabel] = 4 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Recu�rdame' WHERE  [IdLabel] = 5 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Olvid� mi clave' WHERE  [IdLabel] = 6 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Inicio' WHERE  [IdLabel] = 7 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'El usuario o clave son incorrectos!' WHERE  [IdLabel] = 8 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'El usuario es incorrecto!' WHERE  [IdLabel] = 9 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Correo enviado con �xito!' WHERE  [IdLabel] = 10 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Este campo es obligatorio' WHERE  [IdLabel] = 11 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'El usuario es incorrecto!' WHERE  [IdLabel] = 12 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Modificar la informaci�n del usuario' WHERE  [IdLabel] = 13 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Gesti�n de usuarios' WHERE  [IdLabel] = 14 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Funciones y permisos' WHERE  [IdLabel] = 15 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Finalizar la sesi�n' WHERE  [IdLabel] = 16 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Ultimo ingreso' WHERE  [IdLabel] = 17 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Este campo es obligatorio' WHERE  [IdLabel] = 19 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Usuario' WHERE  [IdLabel] = 20 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Usuario' WHERE  [IdLabel] = 21 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Nombre' WHERE  [IdLabel] = 22 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Clave' WHERE  [IdLabel] = 23 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Email' WHERE  [IdLabel] = 24 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Guardar' WHERE  [IdLabel] = 25 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Cancelar' WHERE  [IdLabel] = 26 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Inicio de Aplicaci�n' WHERE  [IdLabel] = 27 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'M�dulos' WHERE  [IdLabel] = 28 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Model Details' WHERE  [IdLabel] = 29 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Base Model' WHERE  [IdLabel] = 30 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Name' WHERE  [IdLabel] = 31 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Created By' WHERE  [IdLabel] = 32 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Created' WHERE  [IdLabel] = 33 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Base Year' WHERE  [IdLabel] = 34 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Projection' WHERE  [IdLabel] = 35 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Status' WHERE  [IdLabel] = 36 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Level' WHERE  [IdLabel] = 37 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Note' WHERE  [IdLabel] = 38 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Save' WHERE  [IdLabel] = 39 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Close' WHERE  [IdLabel] = 40 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Form Status' WHERE  [IdLabel] = 41 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Detalles del Modelo' WHERE  [IdLabel] = 29 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Modelo Base' WHERE  [IdLabel] = 30 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Nombre' WHERE  [IdLabel] = 31 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Creado Por' WHERE  [IdLabel] = 32 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Creado' WHERE  [IdLabel] = 33 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'A�o Base' WHERE  [IdLabel] = 34 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Proyecci�n' WHERE  [IdLabel] = 35 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Estado' WHERE  [IdLabel] = 36 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Nivel' WHERE  [IdLabel] = 37 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Nota' WHERE  [IdLabel] = 38 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Guardar' WHERE  [IdLabel] = 39 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Cerrar' WHERE  [IdLabel] = 40 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Estado del Formulario' WHERE  [IdLabel] = 41 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Add New Model' WHERE  [IdLabel] = 42 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Crear Nuevo Modelo' WHERE  [IdLabel] = 42 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Select' WHERE  [IdLabel] = 43 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Seleccionar' WHERE  [IdLabel] = 43 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Date' WHERE  [IdLabel] = 44 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Fecha' WHERE  [IdLabel] = 44 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Edit' WHERE  [IdLabel] = 46 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Editar' WHERE  [IdLabel] = 46 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Delete' WHERE  [IdLabel] = 47 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Borrar' WHERE  [IdLabel] = 47 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Functions' WHERE  [IdLabel] = 48 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Funciones' WHERE  [IdLabel] = 48 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Add New User' WHERE  [IdLabel] = 49 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Crear Nuevo Usuario' WHERE  [IdLabel] = 49 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Model Selection' WHERE  [IdLabel] = 51 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Selecci�n de Modelo' WHERE  [IdLabel] = 51 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Users' WHERE  [IdLabel] = 52 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Usuarios' WHERE  [IdLabel] = 52 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'User Name' WHERE  [IdLabel] = 53 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Nombre de Usuario' WHERE  [IdLabel] = 53 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Form is valid' WHERE  [IdLabel] = 54 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Formulario es v�lido' WHERE  [IdLabel] = 54 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Email' WHERE  [IdLabel] = 55 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Email' WHERE  [IdLabel] = 55 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Last Login' WHERE  [IdLabel] = 56 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Ultimo Ingreso' WHERE  [IdLabel] = 56 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Import' WHERE  [IdLabel] = 57 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Importar' WHERE  [IdLabel] = 57 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'User' WHERE  [IdLabel] = 59 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Usuario' WHERE  [IdLabel] = 59 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Password' WHERE  [IdLabel] = 60 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Contrase�a' WHERE  [IdLabel] = 60 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Repeat Password' WHERE  [IdLabel] = 61 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Repetir Contrase�a' WHERE  [IdLabel] = 61 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Select Role' WHERE  [IdLabel] = 62 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Seleccione Rol' WHERE  [IdLabel] = 62 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Form is invalid' WHERE  [IdLabel] = 63 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Formulario no es v�lido' WHERE  [IdLabel] = 63 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Yes' WHERE  [IdLabel] = 64 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'S�' WHERE  [IdLabel] = 64 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Open' WHERE  [IdLabel] = 65 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Abrir' WHERE  [IdLabel] = 65 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'No' WHERE  [IdLabel] = 66 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'No' WHERE  [IdLabel] = 66 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Delete All' WHERE  [IdLabel] = 67 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Borrar Todos' WHERE  [IdLabel] = 67 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Add New Role' WHERE  [IdLabel] = 68 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Crear Nuevo Rol' WHERE  [IdLabel] = 68 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Model Selection' WHERE  [IdLabel] = 69 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Selecci�n de Modelo' WHERE  [IdLabel] = 69 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Roles' WHERE  [IdLabel] = 70 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Rol' WHERE  [IdLabel] = 70 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Code' WHERE  [IdLabel] = 71 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'C�digo' WHERE  [IdLabel] = 71 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Economic Scenarios:' WHERE  [IdLabel] = 72 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Escenarios Econ�micos:' WHERE  [IdLabel] = 72 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Edit' WHERE  [IdLabel] = 73 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Editar' WHERE  [IdLabel] = 73 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Confirm' WHERE  [IdLabel] = 74 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Confirmar' WHERE  [IdLabel] = 74 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Role Type' WHERE  [IdLabel] = 75 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Tipo de Rol' WHERE  [IdLabel] = 75 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Selected Model' WHERE  [IdLabel] = 76 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Modelo Seleccionado' WHERE  [IdLabel] = 76 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Selected Role Permissions' WHERE  [IdLabel] = 77 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Permisos de Rol Seleccionado' WHERE  [IdLabel] = 77 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Grant All' WHERE  [IdLabel] = 78 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Concede Todos' WHERE  [IdLabel] = 78 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Deny All' WHERE  [IdLabel] = 79 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Negar Todos' WHERE  [IdLabel] = 79 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Clear All' WHERE  [IdLabel] = 80 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Borrar Todos' WHERE  [IdLabel] = 80 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Grant Access' WHERE  [IdLabel] = 81 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Conceder Acceso' WHERE  [IdLabel] = 81 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Deny Access' WHERE  [IdLabel] = 82 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Negar Acceso' WHERE  [IdLabel] = 82 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Save' WHERE  [IdLabel] = 83 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Guardar' WHERE  [IdLabel] = 83 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Close' WHERE  [IdLabel] = 84 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Cerrar' WHERE  [IdLabel] = 84 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Role' WHERE  [IdLabel] = 85 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Rol' WHERE  [IdLabel] = 85 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Parameters' WHERE  [IdLabel] = 86 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Par�metros' WHERE  [IdLabel] = 86 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Cost Benchmark' WHERE  [IdLabel] = 87 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Referentes de Costos' WHERE  [IdLabel] = 87 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Technical' WHERE  [IdLabel] = 88 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'T�cnico' WHERE  [IdLabel] = 88 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Economic' WHERE  [IdLabel] = 89 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Econ�mico' WHERE  [IdLabel] = 89 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Reports' WHERE  [IdLabel] = 90 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Reportes' WHERE  [IdLabel] = 90 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Model Selection' WHERE  [IdLabel] = 91 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Selecci�n de Modelo' WHERE  [IdLabel] = 91 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Selected Model' WHERE  [IdLabel] = 93 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Modelo Seleccionado' WHERE  [IdLabel] = 93 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Aggregation Levels' WHERE  [IdLabel] = 95 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Niveles de Agregaci�n' WHERE  [IdLabel] = 95 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Types of Operations' WHERE  [IdLabel] = 96 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Tipos de Operaciones' WHERE  [IdLabel] = 96 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'List of Fields' WHERE  [IdLabel] = 97 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Lista de Campos' WHERE  [IdLabel] = 97 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'List of Cases' WHERE  [IdLabel] = 98 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Lista de Casos' WHERE  [IdLabel] = 98 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Currency' WHERE  [IdLabel] = 99 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Moneda' WHERE  [IdLabel] = 99 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Activities' WHERE  [IdLabel] = 100 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Actividades' WHERE  [IdLabel] = 100 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Resources' WHERE  [IdLabel] = 101 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Recursos' WHERE  [IdLabel] = 101 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Cost Objects' WHERE  [IdLabel] = 102 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Objetos de Costo' WHERE  [IdLabel] = 102 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Chart of Accounts' WHERE  [IdLabel] = 103 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Estructura de Cuentas' WHERE  [IdLabel] = 103 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Cost Projection Categories' WHERE  [IdLabel] = 104 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Estructura de Proyecci�n de Costo' WHERE  [IdLabel] = 104 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Parameters Module' WHERE  [IdLabel] = 105 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'M�dulo de Par�metros' WHERE  [IdLabel] = 105 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'1. General Parameters' WHERE  [IdLabel] = 106 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'1. Par�metros Generales' WHERE  [IdLabel] = 106 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Currency' WHERE  [IdLabel] = 107 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Moneda' WHERE  [IdLabel] = 107 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'2. Company Cost Structure' WHERE  [IdLabel] = 108 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'2. Estructura de Costos de la Empresa' WHERE  [IdLabel] = 108 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'3. Cost Projection Categories' WHERE  [IdLabel] = 109 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'3. Estructura de Proyecci�n de Costo' WHERE  [IdLabel] = 109 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Cost Benchmark Module' WHERE  [IdLabel] = 110 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'M�dulo de Referentes de Costo' WHERE  [IdLabel] = 110 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'1. References' WHERE  [IdLabel] = 111 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'1. Referentes' WHERE  [IdLabel] = 111 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Internal Cost References' WHERE  [IdLabel] = 112 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Referentes Internos de Costos' WHERE  [IdLabel] = 112 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'External Cost References' WHERE  [IdLabel] = 113 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Referentes Externos de Costos' WHERE  [IdLabel] = 113 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'2. Assumptions' WHERE  [IdLabel] = 114 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'2. Supuestos' WHERE  [IdLabel] = 114 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Cost Structure Assumptions' WHERE  [IdLabel] = 115 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Supuestos de Estructura de Costos ' WHERE  [IdLabel] = 115 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Base Cost Data' WHERE  [IdLabel] = 116 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Entrada de Costos' WHERE  [IdLabel] = 116 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Direct' WHERE  [IdLabel] = 117 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Directo' WHERE  [IdLabel] = 117 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Shared' WHERE  [IdLabel] = 118 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Compartido' WHERE  [IdLabel] = 118 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Hierarchy' WHERE  [IdLabel] = 119 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Jerarqu�a' WHERE  [IdLabel] = 119 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'List of Cost Allocation Drivers' WHERE  [IdLabel] = 120 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Lista de Drivers de Distribuci�n' WHERE  [IdLabel] = 120 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Cost Allocation Drivers Criteria' WHERE  [IdLabel] = 121 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Selecci�n de Drivers de Distribuci�n' WHERE  [IdLabel] = 121 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Cost Allocation Drivers Data' WHERE  [IdLabel] = 122 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Datos de Drivers de Distribuci�n' WHERE  [IdLabel] = 122 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Internal Base Cost References' WHERE  [IdLabel] = 123 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Referentes Internos de Costos Base' WHERE  [IdLabel] = 123 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Mapping (Company Cost Structure vs. Ziff/Third-party Cost Categories)' WHERE  [IdLabel] = 124 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Mapeo (Estructura Empresa  vs Proyecci�n)' WHERE  [IdLabel] = 124 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Base Cost by Field (Ziff/Third-party Cost Categories)' WHERE  [IdLabel] = 125 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Costo Base por Campo (Ziff/Terceros Categor�as de Costos)' WHERE  [IdLabel] = 125 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'1. Company Cost Input' WHERE  [IdLabel] = 126 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'1. Entrada de Costos de la Empresa' WHERE  [IdLabel] = 126 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'2. Cost Benchmark Process' WHERE  [IdLabel] = 127 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'2. Proceso de Distribuci�n de Costos' WHERE  [IdLabel] = 127 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'3. Cost Allocation Drivers' WHERE  [IdLabel] = 128 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'3. Drivers de Distribuci�n de Costos' WHERE  [IdLabel] = 128 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'4. Base Cost' WHERE  [IdLabel] = 130 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'4. Costo Base' WHERE  [IdLabel] = 130 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Mapping (Company Structure vs. Ziff/Third-party)' WHERE  [IdLabel] = 131 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Mapeo (Estructura Empresa vs Estructura Proyecci�n)' WHERE  [IdLabel] = 131 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Base Cost By Field (Ziff/Third-party Categories)' WHERE  [IdLabel] = 132 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Costo Base por Campo (Ziff/Terceros Categor�as)' WHERE  [IdLabel] = 132 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'List of Peer Criteria' WHERE  [IdLabel] = 133 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Lista de Criterios' WHERE  [IdLabel] = 133 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Peer Criteria Data' WHERE  [IdLabel] = 134 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Datos de Criterios' WHERE  [IdLabel] = 134 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'External Base Cost References' WHERE  [IdLabel] = 135 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Referentes Externos de Costos Base' WHERE  [IdLabel] = 135 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'List Technical Drivers' WHERE  [IdLabel] = 136 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Lista de Drivers T�cnicos' WHERE  [IdLabel] = 136 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Technical Drivers Data' WHERE  [IdLabel] = 137 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Datos de Drivers T�cnicos' WHERE  [IdLabel] = 137 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Technical Assumptions' WHERE  [IdLabel] = 138 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Supuestos T�cnicos' WHERE  [IdLabel] = 138 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Base Rates by Cost Category' WHERE  [IdLabel] = 139 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Tarifas de Costos Base por Categoria' WHERE  [IdLabel] = 139 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Technical Files Repository' WHERE  [IdLabel] = 140 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Repositorio de Archivos T�cnicos' WHERE  [IdLabel] = 140 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Technical Module' WHERE  [IdLabel] = 141 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'M�dulo T�cnico' WHERE  [IdLabel] = 141 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'List of Economic Drivers' WHERE  [IdLabel] = 142 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Lista de Drivers Econ�micos' WHERE  [IdLabel] = 142 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Economic Drivers Data' WHERE  [IdLabel] = 143 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Datos de Drivers Econ�micos' WHERE  [IdLabel] = 143 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Economic Assumptions' WHERE  [IdLabel] = 144 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Supuestos Econ�micos' WHERE  [IdLabel] = 144 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Base Cost Rates Forecast' WHERE  [IdLabel] = 145 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Pron�stico de Tarifa de Costos Base' WHERE  [IdLabel] = 145 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Economic Files Repository' WHERE  [IdLabel] = 146 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Repositorio de Archivos Econ�micos' WHERE  [IdLabel] = 146 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Economic Module' WHERE  [IdLabel] = 147 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'M�dulo Econ�mico' WHERE  [IdLabel] = 147 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Projection by Cost Category' WHERE  [IdLabel] = 148 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Proyecci�n por Categoria de Costo' WHERE  [IdLabel] = 148 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Technical Drivers Forecast' WHERE  [IdLabel] = 149 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Pron�stica de Drivers T�cnicos' WHERE  [IdLabel] = 149 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'KPI Estimates' WHERE  [IdLabel] = 150 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Estimaci�nes KPI' WHERE  [IdLabel] = 150 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Reports Module' WHERE  [IdLabel] = 151 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'M�dulo de Reportes' WHERE  [IdLabel] = 151 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Key Performance Indicators Estimates' WHERE  [IdLabel] = 152 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Estimaci�nes de Indicadores Clave de Rendimiento' WHERE  [IdLabel] = 152 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Key Performance Cost Indicators Estimates' WHERE  [IdLabel] = 153 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Estimaci�nes Costo de Indicadores Clave de Rendimiento' WHERE  [IdLabel] = 153 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Aggregation Level' WHERE  [IdLabel] = 154 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Nivel de Agregaci�n' WHERE  [IdLabel] = 154 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Field:' WHERE  [IdLabel] = 155 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Campo:' WHERE  [IdLabel] = 155 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Cost Structure:' WHERE  [IdLabel] = 156 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Estructura de Costo:' WHERE  [IdLabel] = 156 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'From:' WHERE  [IdLabel] = 157 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'De:' WHERE  [IdLabel] = 157 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'To:' WHERE  [IdLabel] = 158 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'a:' WHERE  [IdLabel] = 158 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Technical Driver:' WHERE  [IdLabel] = 159 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Drivers T�cnicos:' WHERE  [IdLabel] = 159 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Technical Scenario:' WHERE  [IdLabel] = 160 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Escenario T�cnico:' WHERE  [IdLabel] = 160 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Economic Scenario:' WHERE  [IdLabel] = 161 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Escenario Econ�mico:' WHERE  [IdLabel] = 161 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Currency:' WHERE  [IdLabel] = 162 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Moneda:' WHERE  [IdLabel] = 162 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Term:' WHERE  [IdLabel] = 163 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Termino:' WHERE  [IdLabel] = 163 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Run Report' WHERE  [IdLabel] = 164 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Ejecutar Reporte' WHERE  [IdLabel] = 164 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'KPI Target' WHERE  [IdLabel] = 165 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Objetivo KPI' WHERE  [IdLabel] = 165 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Years' WHERE  [IdLabel] = 166 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'A�os' WHERE  [IdLabel] = 166 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Reset' WHERE  [IdLabel] = 167 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Reinicializar' WHERE  [IdLabel] = 167 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Export Report Data To Excel' WHERE  [IdLabel] = 168 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Exportar Informe a Excel' WHERE  [IdLabel] = 168 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Driver:' WHERE  [IdLabel] = 169 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Driver:' WHERE  [IdLabel] = 169 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Project:' WHERE  [IdLabel] = 170 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Proyecto:' WHERE  [IdLabel] = 170 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Technical Driver #' WHERE  [IdLabel] = 172 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Driver T�cnico #' WHERE  [IdLabel] = 172 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Save Changes' WHERE  [IdLabel] = 173 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Guardar Cambios' WHERE  [IdLabel] = 173 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Year' WHERE  [IdLabel] = 174 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'A�o' WHERE  [IdLabel] = 174 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Base' WHERE  [IdLabel] = 175 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Base' WHERE  [IdLabel] = 175 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Pessimistic' WHERE  [IdLabel] = 176 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Pesimista' WHERE  [IdLabel] = 176 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Optimistic' WHERE  [IdLabel] = 177 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Optimista' WHERE  [IdLabel] = 177 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Technical Drivers Forecast Indicator' WHERE  [IdLabel] = 178 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Indicador Pron�sticos de Drivers T�cnicos' WHERE  [IdLabel] = 178 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Total Projection' WHERE  [IdLabel] = 179 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Proyecci�n Total' WHERE  [IdLabel] = 179 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Cost Projection by Baseline and Incremental Line' WHERE  [IdLabel] = 180 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Proyeccion de Costos de Linea Base y Linea Incremental' WHERE  [IdLabel] = 180 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Baseline' WHERE  [IdLabel] = 181 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'L�nea Base' WHERE  [IdLabel] = 181 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Incremental Line' WHERE  [IdLabel] = 182 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'L�nea Incremental' WHERE  [IdLabel] = 182 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Economic Drivers' WHERE  [IdLabel] = 183 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Drivers Econ�micos' WHERE  [IdLabel] = 183 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Economic Driver Group' WHERE  [IdLabel] = 184 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Grupo de Drivers Econ�micos' WHERE  [IdLabel] = 184 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Manage Economic Driver Groups' WHERE  [IdLabel] = 185 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Administrar Grupos de Drivers Econ�micos' WHERE  [IdLabel] = 185 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Add New Economic Driver' WHERE  [IdLabel] = 186 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Crear Nuevo Driver Econ�mico' WHERE  [IdLabel] = 186 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Driver Group' WHERE  [IdLabel] = 187 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Grupo de Driver' WHERE  [IdLabel] = 187 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Economic Driver' WHERE  [IdLabel] = 188 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Driver Econ�mico' WHERE  [IdLabel] = 188 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Import Economic Drivers' WHERE  [IdLabel] = 189 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Importar Drivers Econ�micos' WHERE  [IdLabel] = 189 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Add New Economic Driver Group' WHERE  [IdLabel] = 190 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Crear Nuevo Grupo de Driver Econ�mico' WHERE  [IdLabel] = 190 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'List of Economic Drivers' WHERE  [IdLabel] = 191 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Lista de Drivers Econ�micos' WHERE  [IdLabel] = 191 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Pessimistic' WHERE  [IdLabel] = 192 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Pesimista' WHERE  [IdLabel] = 192 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Optimistic' WHERE  [IdLabel] = 193 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Optimista' WHERE  [IdLabel] = 193 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Import Economic Driver Data' WHERE  [IdLabel] = 194 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Importar Informaci�n de Drivers Econ�micos' WHERE  [IdLabel] = 194 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Import Technical Driver Data' WHERE  [IdLabel] = 195 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Importar Informaci�n de Drivers T�cnicos' WHERE  [IdLabel] = 195 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Utilization Capacity' WHERE  [IdLabel] = 196 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Capacidad Utilizada' WHERE  [IdLabel] = 196 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Base Rates By Drivers' WHERE  [IdLabel] = 197 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Tarifas de Costos Base por Drivers' WHERE  [IdLabel] = 197 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Projection by Case' WHERE  [IdLabel] = 198 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Proyecci�n Detallada por Caso' WHERE  [IdLabel] = 198 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Base Rates by Cost Category' WHERE  [IdLabel] = 199 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Tarifas de Costos Base por Categor�a de Costo' WHERE  [IdLabel] = 199 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Internal Base Cost References' WHERE  [IdLabel] = 200 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Referentes Internos de Costos Base' WHERE  [IdLabel] = 200 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'External Base Cost References' WHERE  [IdLabel] = 201 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Referentes Externos de Costos Base' WHERE  [IdLabel] = 201 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Detailed Projection' WHERE  [IdLabel] = 202 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Proyecci�n Detallada' WHERE  [IdLabel] = 202 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'1. Cost Projection Reports' WHERE  [IdLabel] = 203 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'1. Reportes de Proyecci�n de Costos' WHERE  [IdLabel] = 203 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'2. Technical Module Reports' WHERE  [IdLabel] = 204 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'2. Reportes del Modulo T�cnico' WHERE  [IdLabel] = 204 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'3. Cost Benchmark Module Reports' WHERE  [IdLabel] = 205 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'3. Reportes del Modulo de Referentes de Costo' WHERE  [IdLabel] = 205 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Type of Operation' WHERE  [IdLabel] = 207 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Tipo de Operaci�n' WHERE  [IdLabel] = 207 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Cost Type' WHERE  [IdLabel] = 208 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Tipo de Costo' WHERE  [IdLabel] = 208 AND  [IdLanguage] = 2;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Help' WHERE  [IdLabel] = 209 AND  [IdLanguage] = 1;
UPDATE [dbo].[tblLabelsLanguages] SET  [LabelText] =  N'Ayuda' WHERE  [IdLabel] = 209 AND  [IdLanguage] = 2;
GO
--Update tblLabels table
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Title Form' WHERE [IdLabel] = 1;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'UserName' WHERE [IdLabel] = 2;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Password' WHERE [IdLabel] = 3;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Language' WHERE [IdLabel] = 4;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Remember Me' WHERE [IdLabel] = 5;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Forgot Password?' WHERE [IdLabel] = 6;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Login Button' WHERE [IdLabel] = 7;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Login Message Error' WHERE [IdLabel] = 8;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Forgot Password? Message Error' WHERE [IdLabel] = 9;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Forgot Password? Message Mail' WHERE [IdLabel] = 10;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Valid' WHERE [IdLabel] = 11;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Forgot Password? User Error' WHERE [IdLabel] = 12;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Admin' WHERE [IdLabel] = 13;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'User' WHERE [IdLabel] = 14;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Setting' WHERE [IdLabel] = 15;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Logon' WHERE [IdLabel] = 16;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'LastLogin' WHERE [IdLabel] = 17;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Title Form' WHERE [IdLabel] = 18;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Valid' WHERE [IdLabel] = 19;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Windows Title User' WHERE [IdLabel] = 20;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Form UserName' WHERE [IdLabel] = 21;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Form Name' WHERE [IdLabel] = 22;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Form Password' WHERE [IdLabel] = 23;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Form Email' WHERE [IdLabel] = 24;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Form Acept Button' WHERE [IdLabel] = 25;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Form Cancel Button' WHERE [IdLabel] = 26;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Application Home' WHERE [IdLabel] = 27;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Modules' WHERE [IdLabel] = 28;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Case Details' WHERE [IdLabel] = 29;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Base Case' WHERE [IdLabel] = 30;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Name' WHERE [IdLabel] = 31;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Created By' WHERE [IdLabel] = 32;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Created' WHERE [IdLabel] = 33;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Base Year' WHERE [IdLabel] = 34;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Projection' WHERE [IdLabel] = 35;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Status' WHERE [IdLabel] = 36;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Level' WHERE [IdLabel] = 37;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Note' WHERE [IdLabel] = 38;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Save' WHERE [IdLabel] = 39;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Close' WHERE [IdLabel] = 40;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Form Status' WHERE [IdLabel] = 41;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Add New Case' WHERE [IdLabel] = 42;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Select' WHERE [IdLabel] = 43;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Date' WHERE [IdLabel] = 44;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Edit' WHERE [IdLabel] = 46;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Delete' WHERE [IdLabel] = 47;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Functions' WHERE [IdLabel] = 48;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Add New User' WHERE [IdLabel] = 49;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Case Selection' WHERE [IdLabel] = 51;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Users' WHERE [IdLabel] = 52;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'User Name' WHERE [IdLabel] = 53;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Form is valid' WHERE [IdLabel] = 54;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Email' WHERE [IdLabel] = 55;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Last Login' WHERE [IdLabel] = 56;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Import' WHERE [IdLabel] = 57;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'User' WHERE [IdLabel] = 59;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Password' WHERE [IdLabel] = 60;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Repeat Password' WHERE [IdLabel] = 61;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Select Role' WHERE [IdLabel] = 62;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Form is invalid' WHERE [IdLabel] = 63;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Yes' WHERE [IdLabel] = 64;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Open' WHERE [IdLabel] = 65;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'No' WHERE [IdLabel] = 66;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Delete All' WHERE [IdLabel] = 67;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Add New Role' WHERE [IdLabel] = 68;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Case Selection' WHERE [IdLabel] = 69;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Roles' WHERE [IdLabel] = 70;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Code' WHERE [IdLabel] = 71;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Economic Scenarios:' WHERE [IdLabel] = 72;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Edit' WHERE [IdLabel] = 73;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Confirm' WHERE [IdLabel] = 74;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Role Type' WHERE [IdLabel] = 75;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Selected Case' WHERE [IdLabel] = 76;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Selected Role Permissions' WHERE [IdLabel] = 77;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Grant All' WHERE [IdLabel] = 78;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Deny All' WHERE [IdLabel] = 79;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Clear All' WHERE [IdLabel] = 80;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Grant Access' WHERE [IdLabel] = 81;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Deny Access' WHERE [IdLabel] = 82;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Save' WHERE [IdLabel] = 83;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Close' WHERE [IdLabel] = 84;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Role' WHERE [IdLabel] = 85;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Parameters' WHERE [IdLabel] = 86;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Allocation' WHERE [IdLabel] = 87;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Technical' WHERE [IdLabel] = 88;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Economic' WHERE [IdLabel] = 89;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Reports' WHERE [IdLabel] = 90;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Case Selection' WHERE [IdLabel] = 91;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Selected Case' WHERE [IdLabel] = 93;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Aggregation Levels' WHERE [IdLabel] = 95;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Types of Operations' WHERE [IdLabel] = 96;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'List of Fields' WHERE [IdLabel] = 97;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Field Projects' WHERE [IdLabel] = 98;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Currency' WHERE [IdLabel] = 99;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Activities' WHERE [IdLabel] = 100;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Resources' WHERE [IdLabel] = 101;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Cost Objects' WHERE [IdLabel] = 102;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Chart of Accounts' WHERE [IdLabel] = 103;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Ziff/Third-party Cost Categories' WHERE [IdLabel] = 104;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Parameters Module' WHERE [IdLabel] = 105;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'1. General Parameters' WHERE [IdLabel] = 106;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Currency' WHERE [IdLabel] = 107;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'2. Company Cost Structure' WHERE [IdLabel] = 108;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'3. Cost Projection Categories' WHERE [IdLabel] = 109;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Allocation Module' WHERE [IdLabel] = 110;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'1. References' WHERE [IdLabel] = 111;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Internal Cost References' WHERE [IdLabel] = 112;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'External Cost References' WHERE [IdLabel] = 113;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'2. Assumptions' WHERE [IdLabel] = 114;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Cost Structure Assumptions' WHERE [IdLabel] = 115;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Base Cost Data' WHERE [IdLabel] = 116;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Direct' WHERE [IdLabel] = 117;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Shared' WHERE [IdLabel] = 118;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Hierarchy' WHERE [IdLabel] = 119;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'List of Allocation Drivers' WHERE [IdLabel] = 120;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Allocation Drivers Criteria' WHERE [IdLabel] = 121;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Allocation Drivers Data' WHERE [IdLabel] = 122;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Base Cost by Field (Company Structure)' WHERE [IdLabel] = 123;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Mapping (Company Cost Structure vs. Ziff/Third-party Cost Categories)' WHERE [IdLabel] = 124;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Base Cost by Field (Ziff/Third-party Cost Categories)' WHERE [IdLabel] = 125;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'1. Company Cost Input' WHERE [IdLabel] = 126;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'2. Cost Allocation Process' WHERE [IdLabel] = 127;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'3. Allocation Drivers' WHERE [IdLabel] = 128;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'4. Base Cost' WHERE [IdLabel] = 130;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Mapping (Company Structure vs. Ziff/Third-party)' WHERE [IdLabel] = 131;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Base Cost By Field (Ziff/Third-party Categories)' WHERE [IdLabel] = 132;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'List of Peer Criteria' WHERE [IdLabel] = 133;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Peer Criteria Data' WHERE [IdLabel] = 134;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Ziff/Third-party Base Cost' WHERE [IdLabel] = 135;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'List Technical Drivers' WHERE [IdLabel] = 136;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Technical Drivers Data' WHERE [IdLabel] = 137;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Technical Assumptions' WHERE [IdLabel] = 138;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Base Cost Rates' WHERE [IdLabel] = 139;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Technical Files Repository' WHERE [IdLabel] = 140;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Technical Module' WHERE [IdLabel] = 141;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'List of Economic Drivers' WHERE [IdLabel] = 142;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Economic Drivers Data' WHERE [IdLabel] = 143;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Economic Assumptions' WHERE [IdLabel] = 144;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Base Cost Rates Forecast' WHERE [IdLabel] = 145;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Economic File Repository' WHERE [IdLabel] = 146;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Economic Module' WHERE [IdLabel] = 147;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Projection by Cost Category' WHERE [IdLabel] = 148;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Technical Drivers Forecast' WHERE [IdLabel] = 149;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'KPI Estimates' WHERE [IdLabel] = 150;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Reports Module' WHERE [IdLabel] = 151;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Key Performance Indicators Estimates' WHERE [IdLabel] = 152;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Key Performance Cost Indicators Estimates' WHERE [IdLabel] = 153;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Aggregation Level:' WHERE [IdLabel] = 154;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Field:' WHERE [IdLabel] = 155;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Cost Structure:' WHERE [IdLabel] = 156;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'From:' WHERE [IdLabel] = 157;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'To:' WHERE [IdLabel] = 158;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Technical Driver:' WHERE [IdLabel] = 159;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Technical Scenario:' WHERE [IdLabel] = 160;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Economic Scenario:' WHERE [IdLabel] = 161;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Currency:' WHERE [IdLabel] = 162;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Term:' WHERE [IdLabel] = 163;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Run Report' WHERE [IdLabel] = 164;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'KPI Target' WHERE [IdLabel] = 165;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Years' WHERE [IdLabel] = 166;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Reset' WHERE [IdLabel] = 167;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Export Report Data To Excel' WHERE [IdLabel] = 168;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Driver:' WHERE [IdLabel] = 169;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Project:' WHERE [IdLabel] = 170;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Technical Driver #' WHERE [IdLabel] = 172;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Save Changes' WHERE [IdLabel] = 173;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Year' WHERE [IdLabel] = 174;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Base' WHERE [IdLabel] = 175;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Pessimistic' WHERE [IdLabel] = 176;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Optimistic' WHERE [IdLabel] = 177;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Technical Drivers Forecast Indicator' WHERE [IdLabel] = 178;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Total Projection' WHERE [IdLabel] = 179;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Projection By Project' WHERE [IdLabel] = 180;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Baseline' WHERE [IdLabel] = 181;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Business Opportunities' WHERE [IdLabel] = 182;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Economic Drivers' WHERE [IdLabel] = 183;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Economic Driver Group' WHERE [IdLabel] = 184;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Manage Economic Driver Groups' WHERE [IdLabel] = 185;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Add New Economic Driver' WHERE [IdLabel] = 186;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Driver Group' WHERE [IdLabel] = 187;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Economic Driver' WHERE [IdLabel] = 188;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Import Economic Drivers' WHERE [IdLabel] = 189;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Add New Economic Driver Group' WHERE [IdLabel] = 190;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'List of Economic Drivers' WHERE [IdLabel] = 191;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Pessimistic' WHERE [IdLabel] = 192;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Optimistic' WHERE [IdLabel] = 193;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Import Economic Driver Data' WHERE [IdLabel] = 194;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Import Technical Driver Data' WHERE [IdLabel] = 195;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Utilization Capacity' WHERE [IdLabel] = 196;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Base Rates By Drivers' WHERE [IdLabel] = 197;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Projection by Field Project' WHERE [IdLabel] = 198;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Base Rates By Cost Category' WHERE [IdLabel] = 199;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Internal Base Cost References' WHERE [IdLabel] = 200;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'External Base Cost References' WHERE [IdLabel] = 201;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Detailed Projection' WHERE [IdLabel] = 202;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'1. Cost Projection Reports' WHERE [IdLabel] = 203;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'2. Projection Base Rates Reports' WHERE [IdLabel] = 204;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'3. Historical Base Cost Reports' WHERE [IdLabel] = 205;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Type of Operation' WHERE [IdLabel] = 207;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Cost Type' WHERE [IdLabel] = 208;
UPDATE [dbo].[tblLabels] SET  [Name] =  N'Help' WHERE [IdLabel] = 209;
GO
USE [DBCPM]
GO

/****** Object:  StoredProcedure [dbo].[tecpUtilizationCapacity]    Script Date: 06/17/2015 15:07:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ============================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-03-19
-- Description:	Utilization Capacity Report per Field
-- ============================================================
ALTER PROCEDURE [dbo].[tecpUtilizationCapacity](
@IdModel int,
@IdStage int,
@BaseCostType int
)
AS
BEGIN
	
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#FieldList') IS NOT NULL DROP TABLE #FieldList
	IF OBJECT_ID('tempdb..#UtilizationCapacity') IS NOT NULL DROP TABLE #UtilizationCapacity
	IF OBJECT_ID('tempdb..#tblCalcTechnicalBaseCostRate') IS NOT NULL DROP TABLE #tblCalcTechnicalBaseCostRate

	/** Build Field List **/
	CREATE TABLE #FieldList (IdField INT PRIMARY KEY NOT NULL, Name NVARCHAR(255)) ON [PRIMARY]
	INSERT INTO #FieldList SELECT A.[IdField], B.Name FROM tblCalcTechnicalBaseCostRate A INNER JOIN tblFields B ON A.IdField=B.IdField WHERE A.[IdModel]=@IdModel GROUP BY A.[IdField], B.Name;

	/** Build Field Column **/
	DECLARE @FieldColumnList NVARCHAR(MAX)='';
	DECLARE @FieldName NVARCHAR(255);
	DECLARE FieldList CURSOR FOR
	SELECT Name FROM #FieldList;

	OPEN FieldList;
	FETCH NEXT FROM FieldList INTO @FieldName;

	WHILE @@FETCH_STATUS = 0
	BEGIN		
		IF @FieldColumnList=''
		BEGIN
			SET @FieldColumnList =  @FieldColumnList + '[' + @FieldName  + ']';
		END
		ELSE
		BEGIN
			SET @FieldColumnList =  @FieldColumnList + ',[' + @FieldName  + ']';
		END
		FETCH NEXT FROM FieldList INTO @FieldName;
	END

	CLOSE FieldList;
	DEALLOCATE FieldList;

	CREATE TABLE #UtilizationCapacity (CodeZiff VARCHAR(150), FieldName NVARCHAR(255), SizeValue FLOAT, SortOrder NVARCHAR(100))
	INSERT INTO #UtilizationCapacity (CodeZiff,FieldName,SizeValue,SortOrder)
	SELECT  dbo.svfLevelStringSpacer(dbo.tblZiffAccounts.RelationLevel) + dbo.tblZiffAccounts.Code + N': ' + dbo.tblZiffAccounts.Name AS DisplayName
		  , tblFields.Name
		  , A.UtilizationValue
		  , tblZiffAccounts.SortOrder
	  FROM tblUtilizationCapacity A INNER JOIN tblFields
			ON A.IdField = tblFields.IdField LEFT OUTER JOIN tblZiffAccounts 
			ON tblZiffAccounts.Code=A.CodeZiff
	WHERE  	A.IdModel = @IdModel
	ORDER BY tblZiffAccounts.SortOrder--A.[CodeZiff] + ': ' + A.ZiffAccount

	INSERT INTO #UtilizationCapacity (CodeZiff,FieldName,SizeValue,SortOrder)
	SELECT  ZA.[Code]AS CodeZiff
		  , #FieldList.Name
		  , '-1'
		  , ZA.SortOrder
	  FROM tblZiffAccounts ZA , #FieldList
	WHERE  	ZA.IdModel = @IdModel AND Parent IS NULL

	--/** Build Output Table **/
	DECLARE @SQLUtilizationCapacity NVARCHAR(MAX);
	SET @SQLUtilizationCapacity = 'SELECT * FROM #UtilizationCapacity PIVOT (Sum(SizeValue) FOR FieldName IN (' + @FieldColumnList + ')) AS PivotTable';
	EXEC(@SQLUtilizationCapacity);

END
GO
USE [DBCPM]
GO

/****** Object:  StoredProcedure [dbo].[allpListCostStructureAssumptionsByProject]    Script Date: 06/17/2015 15:09:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================================================
-- Author:		Rodrigo Manubens
-- Create date: 2015-06-11
-- Description:	List Cost Structure Assumptions By Project as the view will no longer work as we are adding a row to display the parent
-- ========================================================================================================================================

ALTER PROCEDURE [dbo].[allpListCostStructureAssumptionsByProject](
 @IdModel int,
 @IdField int
)
AS
BEGIN
	
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#CostStructureAssumptions') IS NOT NULL DROP TABLE #CostStructureAssumptions

	CREATE TABLE #CostStructureAssumptions (IdModel INT, IdCostStructureAssumptions INT, IdProject INT, IdZiffAccount INT, Code NVARCHAR(50), Name NVARCHAR(255), ParentCode NVARCHAR(50), ParentName NVARCHAR(255), Client INT, Ziff INT , IdField INT, SortOrder NVARCHAR(100))
	INSERT INTO #CostStructureAssumptions (IdModel, IdCostStructureAssumptions, IdProject, IdZiffAccount, Code, Name, ParentCode, ParentName, Client, Ziff, IdField, SortOrder)
	SELECT	tblZiffAccountsParent.IdModel, tblCostStructureAssumptionsByProject.IdCostStructureAssumptions, tblCostStructureAssumptionsByProject.IdProject, 
			tblCostStructureAssumptionsByProject.IdZiffAccount, '&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;' + tblZiffAccounts.Code as Code, tblZiffAccounts.Name, tblZiffAccounts.Code AS ParentCode, 
			tblZiffAccountsParent.Name AS ParentName, tblCostStructureAssumptionsByProject.Client, tblCostStructureAssumptionsByProject.Ziff, 
			tblProjects.IdField, tblZiffAccounts.SortOrder
	FROM    tblCostStructureAssumptionsByProject LEFT OUTER JOIN
			tblProjects ON tblCostStructureAssumptionsByProject.IdProject = tblProjects.IdProject LEFT OUTER JOIN
			tblZiffAccounts ON tblCostStructureAssumptionsByProject.IdZiffAccount = tblZiffAccounts.IdZiffAccount LEFT OUTER JOIN
			tblZiffAccounts AS tblZiffAccountsParent ON tblZiffAccounts.Parent = tblZiffAccountsParent.IdZiffAccount
	WHERE  	tblProjects.IdModel = @IdModel
	AND		tblProjects.IdField = @IdField 

	INSERT INTO #CostStructureAssumptions (IdModel, IdCostStructureAssumptions, IdProject, IdZiffAccount, Code, Name, ParentCode, ParentName, Client, Ziff, IdField, SortOrder)
	SELECT NULL, NULL, NULL, NULL, dbo.tblZiffAccounts.Code, NULL, NULL, NULL, 9999, 9999, NULL, dbo.tblZiffAccounts.SortOrder
	FROM tblZiffAccounts
	WHERE  	IdModel = @IdModel AND Parent IS NULL

	/** Build Output Table **/
	SELECT IdModel, IdCostStructureAssumptions, IdProject, IdZiffAccount, Code, Name, ParentCode, ParentName, Client, Ziff, IdField, SortOrder 
	FROM #CostStructureAssumptions 
	ORDER BY SortOrder
END
GO



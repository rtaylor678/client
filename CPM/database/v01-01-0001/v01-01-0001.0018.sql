USE [DBCPM]
GO
/****** Object:  StoredProcedure [dbo].[calcModuleTechnicalForecast]    Script Date: 09/15/2015 11:30:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---- ================================================================================================
---- Author:		Gareth Slater
---- Create date: 2014-04-20
---- Description:	Module Technical Forecast
---- ================================================================================================ 
---- Modifications:	
---- Date			Author	Description
---- ---------	------	----------------------------------------------------------------------
---- 2015-08-04	RM		Added local variables, made temporary tables in order to speed up execution. 
----						Added split of baselines when necessary
---- ================================================================================================ 
ALTER PROCEDURE [dbo].[calcModuleTechnicalForecast](
@IdModel int,
@MinYear int,
@MaxYear int
) WITH RECOMPILE 
AS
BEGIN
	--SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/** Declare local variables to reduce processing time **/
	DECLARE @LocIdModel INT = @IdModel
	DECLARE @LocMinYear INT = @MinYear
	DECLARE @LocMaxYear INT = @MaxYear
	
	/** Cleanup Calc Tables for New Data **/
	DELETE FROM tblCalcTechnicalSemifixedSelections WHERE [IdModel]=@LocIdModel;
	DELETE FROM tblCalcTechnicalSemifixedAmount WHERE [IdModel]=@LocIdModel;
	DELETE FROM tblCalcTechnicalSemifixedSumAmount WHERE [IdModel]=@LocIdModel;
	DELETE FROM tblCalcTechnicalSemifixedSplit WHERE [IdModel]=@LocIdModel;


	/** Cleanup Calc Tables for New Data **/
	IF OBJECT_ID('tempdb..#tblCalcProjectionTechnical') IS NOT NULL DROP TABLE #tblCalcProjectionTechnical
	IF OBJECT_ID('tempdb..#VolumePerField') IS NOT NULL DROP TABLE #VolumePerField
	IF OBJECT_ID('tempdb..#VolumeJoiner') IS NOT NULL DROP TABLE #VolumeJoiner
	
	/** Temporary table to store Projection data **/
	CREATE TABLE [dbo].[#tblCalcProjectionTechnical](
		[SourceDebug] [int] NULL,
		[IdModel] [int] NOT NULL,
		[IdActivity] [int] NULL,
		[CodeAct] [nvarchar](50) NULL,
		[Activity] [nvarchar](255) NULL,
		[IdResources] [int] NULL,
		[CodeRes] [nvarchar](50) NULL,
		[Resource] [nvarchar](255) NULL,
		[IdClientAccount] [int] NULL,
		[Account] [nvarchar](50) NULL,
		[NameAccount] [nvarchar](255) NULL,
		[IdField] [int] NOT NULL,
		[Field] [nvarchar](255) NULL,
		[IdClientCostCenter] [int] NULL,
		[ClientCostCenter] [nvarchar](255) NULL,
		[Amount] [float] NULL,
		[TypeAllocation] [int] NULL,
		[IdZiffAccount] [int] NULL,
		[CodeZiff] [nvarchar](50) NULL,
		[ZiffAccount] [nvarchar](255) NULL,
		[IdRootZiffAccount] [int] NULL,
		[RootCodeZiff] [nvarchar](50) NULL,
		[RootZiffAccount] [nvarchar](255) NULL,
		[IdProject] [int] NULL,
		[Project] [nvarchar](255) NULL,
		[ProjectionCriteria] [int] NULL,
		[Year] [int] NULL,
		[BaseYear] [bit] NULL,
		[TFNormal] [float] NULL,
		[TFPessimistic] [float] NULL,
		[TFOptimistic] [float] NULL,
		[BCRNormal] [float] NULL,
		[BCRPessimistic] [float] NULL,
		[BCROptimistic] [float] NULL,
		[BaseCostType] [int] NULL,
		[Operation] [int] NULL,
		[SortOrder] [int] NULL
	) ON [PRIMARY]
	
	/** Set Allowed Combinations for Semi-Fixed Assumptions (Shared) **/	
	INSERT INTO tblCalcTechnicalSemifixedSelections ([IdModel],[IdField],[IdZiffAccount],[IdTechnicalDriverSize],[TypeAllocation],[IdActivity],[IdResources],[IdClientAccount],[IdClientCostCenter])
	SELECT tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.IdZiffAccount, tblCalcTechnicalAssumption.IdTechnicalDriverSize, 
		tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.IdClientAccount, 
		tblCalcBaseCostByField.IdClientCostCenter
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalAssumption.IdModel AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalAssumption.IdZiffAccount INNER JOIN
		tblFields ON tblCalcBaseCostByField.IdField = tblFields.IdField AND tblCalcBaseCostByField.IdModel = tblFields.IdModel AND 
		tblCalcTechnicalAssumption.IdTypeOperation = tblFields.IdTypeOperation
	WHERE tblCalcTechnicalAssumption.ProjectionCriteria=2
	GROUP BY tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.TypeAllocation, 
		tblCalcTechnicalAssumption.IdTechnicalDriverSize, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.IdClientAccount, 
		tblCalcBaseCostByField.IdClientCostCenter
	HAVING tblCalcBaseCostByField.TypeAllocation=2 AND tblCalcBaseCostByField.IdModel=@LocIdModel;
	
	/** Set Allowed Combinations for Semi-Fixed Assumptions (Hierarchy) **/
	INSERT INTO tblCalcTechnicalSemifixedSelections ([IdModel],[IdField],[IdZiffAccount],[IdTechnicalDriverSize],[TypeAllocation],[IdActivity],[IdResources],[IdClientAccount],[IdClientCostCenter])
	SELECT tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.IdZiffAccount, tblCalcTechnicalAssumption.IdTechnicalDriverSize, 
		tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.IdClientAccount, 
		tblCalcBaseCostByField.IdClientCostCenter
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalAssumption.IdModel AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalAssumption.IdZiffAccount INNER JOIN
		tblFields ON tblCalcBaseCostByField.IdField = tblFields.IdField AND tblCalcBaseCostByField.IdModel = tblFields.IdModel AND 
		tblCalcTechnicalAssumption.IdTypeOperation = tblFields.IdTypeOperation
	WHERE tblCalcTechnicalAssumption.ProjectionCriteria=2 AND tblCalcBaseCostByField.BCRNormal<>0 AND tblCalcBaseCostByField.BCRPessimistic<>0 AND tblCalcBaseCostByField.BCROptimistic<>0
	GROUP BY tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.TypeAllocation, 
		tblCalcTechnicalAssumption.IdTechnicalDriverSize, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.IdClientAccount, 
		tblCalcBaseCostByField.IdClientCostCenter
	HAVING tblCalcBaseCostByField.TypeAllocation=3 AND tblCalcBaseCostByField.IdModel=@LocIdModel;

	/** Get Tech Driver Totals by Field for Semi-Fixed **/
	INSERT INTO tblCalcTechnicalSemifixedAmount ([IdModel],[IdField],[IdZiffAccount],[IdTechnicalDriverSize],[TypeAllocation],[IdActivity],[IdResources],[IdClientAccount],[IdClientCostCenter],[Year],[Normal],[Pessimistic],[Optimistic])
	SELECT tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalSemifixedSelections.IdField, tblCalcTechnicalSemifixedSelections.IdZiffAccount, 
		tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize, tblCalcTechnicalSemifixedSelections.TypeAllocation, tblCalcTechnicalSemifixedSelections.IdActivity, 
		tblCalcTechnicalSemifixedSelections.IdResources, tblCalcTechnicalSemifixedSelections.IdClientAccount, tblCalcTechnicalSemifixedSelections.IdClientCostCenter, 
		tblCalcTechnicalDriverData.Year, SUM(tblCalcTechnicalDriverData.Normal) AS Normal, SUM(tblCalcTechnicalDriverData.Pessimistic) AS Pessimistic, SUM(tblCalcTechnicalDriverData.Optimistic) 
		AS Optimistic
	FROM tblCalcTechnicalSemifixedSelections INNER JOIN
		tblCalcTechnicalDriverData ON tblCalcTechnicalSemifixedSelections.IdModel = tblCalcTechnicalDriverData.IdModel AND 
		tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize = tblCalcTechnicalDriverData.IdTechnicalDriver AND 
		tblCalcTechnicalSemifixedSelections.IdField = tblCalcTechnicalDriverData.IdField
	GROUP BY tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalSemifixedSelections.IdField, tblCalcTechnicalSemifixedSelections.IdZiffAccount, 
		tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize, tblCalcTechnicalSemifixedSelections.TypeAllocation, tblCalcTechnicalSemifixedSelections.IdActivity, 
		tblCalcTechnicalSemifixedSelections.IdResources, tblCalcTechnicalSemifixedSelections.IdClientAccount, tblCalcTechnicalSemifixedSelections.IdClientCostCenter, 
		tblCalcTechnicalDriverData.Year
	HAVING tblCalcTechnicalDriverData.Year<>@LocMinYear AND tblCalcTechnicalSemifixedSelections.IdModel=@LocIdModel;

	/** Get Tech Driver Totals by Set of Applicable Fields for Semi-Fixed **/
	INSERT INTO tblCalcTechnicalSemifixedSumAmount ([IdModel],[IdZiffAccount],[IdTechnicalDriverSize],[TypeAllocation],[IdActivity],[IdResources],[IdClientAccount],[IdClientCostCenter],[Year],[Normal],[Pessimistic],[Optimistic])
	SELECT tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalSemifixedSelections.IdZiffAccount, tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize, 
		tblCalcTechnicalSemifixedSelections.TypeAllocation, tblCalcTechnicalSemifixedSelections.IdActivity, tblCalcTechnicalSemifixedSelections.IdResources, 
		tblCalcTechnicalSemifixedSelections.IdClientAccount, tblCalcTechnicalSemifixedSelections.IdClientCostCenter, tblCalcTechnicalDriverData.Year, SUM(tblCalcTechnicalDriverData.Normal) 
		AS Normal, SUM(tblCalcTechnicalDriverData.Pessimistic) AS Pessimistic, SUM(tblCalcTechnicalDriverData.Optimistic) AS Optimistic
	FROM tblCalcTechnicalSemifixedSelections INNER JOIN
		tblCalcTechnicalDriverData ON tblCalcTechnicalSemifixedSelections.IdModel = tblCalcTechnicalDriverData.IdModel AND 
		tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize = tblCalcTechnicalDriverData.IdTechnicalDriver AND 
		tblCalcTechnicalSemifixedSelections.IdField = tblCalcTechnicalDriverData.IdField
	GROUP BY tblCalcTechnicalSemifixedSelections.IdModel, tblCalcTechnicalSemifixedSelections.IdZiffAccount, tblCalcTechnicalSemifixedSelections.IdTechnicalDriverSize, 
		tblCalcTechnicalSemifixedSelections.TypeAllocation, tblCalcTechnicalSemifixedSelections.IdActivity, tblCalcTechnicalSemifixedSelections.IdResources, 
		tblCalcTechnicalSemifixedSelections.IdClientAccount, tblCalcTechnicalSemifixedSelections.IdClientCostCenter, tblCalcTechnicalDriverData.Year
	HAVING tblCalcTechnicalDriverData.Year<>@LocMinYear AND tblCalcTechnicalSemifixedSelections.IdModel=@LocIdModel;

	/** Set Tech Driver Splits for Semi-Fixed **/
	INSERT INTO tblCalcTechnicalSemifixedSplit ([IdModel],[IdField],[IdZiffAccount],[IdTechnicalDriverSize],[TypeAllocation],[IdActivity],[IdResources],[IdClientAccount],[IdClientCostCenter],[Year],[SplitNormal],[SplitPessimistic],[SplitOptimistic])
	SELECT tblCalcTechnicalSemifixedAmount.IdModel, tblCalcTechnicalSemifixedAmount.IdField, tblCalcTechnicalSemifixedAmount.IdZiffAccount, tblCalcTechnicalSemifixedAmount.IdTechnicalDriverSize, 
		tblCalcTechnicalSemifixedAmount.TypeAllocation, tblCalcTechnicalSemifixedAmount.IdActivity, tblCalcTechnicalSemifixedAmount.IdResources, tblCalcTechnicalSemifixedAmount.IdClientAccount, 
		tblCalcTechnicalSemifixedAmount.IdClientCostCenter, tblCalcTechnicalSemifixedAmount.Year, 
		CASE WHEN ISNULL(tblCalcTechnicalSemifixedSumAmount.Normal, 0) = 0 THEN 0 ELSE tblCalcTechnicalSemifixedAmount.Normal / tblCalcTechnicalSemifixedSumAmount.Normal END AS SplitNormal, 
		CASE WHEN ISNULL(tblCalcTechnicalSemifixedSumAmount.Pessimistic, 0) = 0 THEN 0 ELSE tblCalcTechnicalSemifixedAmount.Pessimistic / tblCalcTechnicalSemifixedSumAmount.Pessimistic END AS SplitPessimistic, 
		CASE WHEN ISNULL(tblCalcTechnicalSemifixedSumAmount.Optimistic, 0) = 0 THEN 0 ELSE tblCalcTechnicalSemifixedAmount.Optimistic / tblCalcTechnicalSemifixedSumAmount.Optimistic END AS SplitOptimistic
	FROM tblCalcTechnicalSemifixedAmount INNER JOIN
		tblCalcTechnicalSemifixedSumAmount ON tblCalcTechnicalSemifixedAmount.IdModel = tblCalcTechnicalSemifixedSumAmount.IdModel AND 
		tblCalcTechnicalSemifixedAmount.IdZiffAccount = tblCalcTechnicalSemifixedSumAmount.IdZiffAccount AND 
		tblCalcTechnicalSemifixedAmount.IdTechnicalDriverSize = tblCalcTechnicalSemifixedSumAmount.IdTechnicalDriverSize AND 
		tblCalcTechnicalSemifixedAmount.TypeAllocation = tblCalcTechnicalSemifixedSumAmount.TypeAllocation AND 
		tblCalcTechnicalSemifixedAmount.IdActivity = tblCalcTechnicalSemifixedSumAmount.IdActivity AND 
		tblCalcTechnicalSemifixedAmount.IdResources = tblCalcTechnicalSemifixedSumAmount.IdResources AND 
		tblCalcTechnicalSemifixedAmount.IdClientAccount = tblCalcTechnicalSemifixedSumAmount.IdClientAccount AND 
		tblCalcTechnicalSemifixedAmount.IdClientCostCenter = tblCalcTechnicalSemifixedSumAmount.IdClientCostCenter AND 
		tblCalcTechnicalSemifixedAmount.Year = tblCalcTechnicalSemifixedSumAmount.Year
	WHERE tblCalcTechnicalSemifixedAmount.Year<>@LocMinYear AND tblCalcTechnicalSemifixedAmount.IdModel=@LocIdModel;
	
	/** Update Ziff Semi-Fixed Forecast to Fixed **/
	UPDATE tblCalcTechnicalDriverAssumptionZiff SET [ProjectionCriteria]=1 WHERE [ProjectionCriteria]=2 AND [IdModel]=@LocIdModel;
	
	/** (1) Insert Company Base Year Into #tblCalcProjectionTechnical **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 1, tblCalcBaseCostByField.IdModel,tblCalcBaseCostByField.IdActivity,tblCalcBaseCostByField.CodeAct,tblCalcBaseCostByField.Activity,tblCalcBaseCostByField.IdResources,tblCalcBaseCostByField.CodeRes,tblCalcBaseCostByField.Resource,tblCalcBaseCostByField.IdClientAccount,tblCalcBaseCostByField.Account,tblCalcBaseCostByField.NameAccount,tblCalcBaseCostByField.IdField,tblCalcBaseCostByField.Field,tblCalcBaseCostByField.IdClientCostCenter,tblCalcBaseCostByField.ClientCostCenter,tblCalcBaseCostByField.Amount,tblCalcBaseCostByField.TypeAllocation,tblCalcBaseCostByField.IdZiffAccount,tblCalcBaseCostByField.CodeZiff,tblCalcBaseCostByField.ZiffAccount,tblCalcBaseCostByField.IdRootZiffAccount,[RootCodeZiff],[RootZiffAccount],tblCalcTechnicalDriverAssumption.IdProject,tblCalcTechnicalDriverAssumption.ProjectionCriteria,tblCalcTechnicalDriverAssumption.Year,tblCalcTechnicalDriverAssumption.BaseYear,
		tblCalcTechnicalDriverAssumption.TFNormal,tblCalcTechnicalDriverAssumption.TFPessimistic,tblCalcTechnicalDriverAssumption.TFOptimistic,
		CASE WHEN tblCalcTechnicalDriverAssumption.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumption.Operation=1 THEN tblCalcBaseCostByField.BCRNormal * tblCalcBaseCostByField.TFNormal ELSE tblCalcBaseCostByField.BCRNormal END,
		CASE WHEN tblCalcTechnicalDriverAssumption.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumption.Operation=1 THEN tblCalcBaseCostByField.BCRPessimistic * tblCalcBaseCostByField.TFPessimistic ELSE tblCalcBaseCostByField.BCRPessimistic END,
		CASE WHEN tblCalcTechnicalDriverAssumption.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumption.Operation=1 THEN tblCalcBaseCostByField.BCROptimistic * tblCalcBaseCostByField.TFOptimistic ELSE tblCalcBaseCostByField.BCROptimistic END,
		1 AS BaseCostType,tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount
	WHERE tblCalcTechnicalDriverAssumption.BaseYear=1 AND tblCalcBaseCostByField.IdModel=@LocIdModel;

	/** (2) Insert Ziff Base Year Into #tblCalcProjectionTechnical (If Applicable) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 2, tblCalcBaseCostByFieldZiff.IdModel,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,tblCalcBaseCostByFieldZiff.IdField,tblCalcBaseCostByFieldZiff.Field,NULL,NULL,tblCalcBaseCostByFieldZiff.TotalCost,NULL,tblCalcBaseCostByFieldZiff.IdZiffAccount,tblCalcBaseCostByFieldZiff.CodeZiff,tblCalcBaseCostByFieldZiff.ZiffAccount,tblCalcBaseCostByFieldZiff.IdRootZiffAccount,[RootCodeZiff],[RootZiffAccount],tblCalcTechnicalDriverAssumptionZiff.IdProject,tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria,tblCalcTechnicalDriverAssumptionZiff.Year,tblCalcTechnicalDriverAssumptionZiff.BaseYear,
		tblCalcTechnicalDriverAssumptionZiff.TFNormal,tblCalcTechnicalDriverAssumptionZiff.TFPessimistic,tblCalcTechnicalDriverAssumptionZiff.TFOptimistic,
		CASE WHEN tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 THEN tblCalcBaseCostByFieldZiff.BCRNormal * tblCalcBaseCostByFieldZiff.TFNormal ELSE tblCalcBaseCostByFieldZiff.BCRNormal END,
		CASE WHEN tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 THEN tblCalcBaseCostByFieldZiff.BCRPessimistic * tblCalcBaseCostByFieldZiff.TFPessimistic ELSE tblCalcBaseCostByFieldZiff.BCRPessimistic END,
		CASE WHEN tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5 AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 THEN tblCalcBaseCostByFieldZiff.BCROptimistic * tblCalcBaseCostByFieldZiff.TFOptimistic ELSE tblCalcBaseCostByFieldZiff.BCROptimistic END,
		2 AS BaseCostType,tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount
	WHERE tblCalcTechnicalDriverAssumptionZiff.BaseYear=1 
	AND tblCalcBaseCostByFieldZiff.IdModel=@LocIdModel;
	
	/** (3) Update Company Projection - Semi-Fixed (Direct Portion) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 3, tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.CodeAct, tblCalcBaseCostByField.Activity, tblCalcBaseCostByField.IdResources, 
		tblCalcBaseCostByField.CodeRes, tblCalcBaseCostByField.Resource, tblCalcBaseCostByField.IdClientAccount, tblCalcBaseCostByField.Account, tblCalcBaseCostByField.NameAccount, 
		tblCalcBaseCostByField.IdField, tblCalcBaseCostByField.Field, tblCalcBaseCostByField.IdClientCostCenter, tblCalcBaseCostByField.ClientCostCenter, tblCalcBaseCostByField.Amount, 
		tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.CodeZiff, tblCalcBaseCostByField.ZiffAccount, 
		tblCalcBaseCostByField.IdRootZiffAccount, tblCalcBaseCostByField.RootCodeZiff, tblCalcBaseCostByField.RootZiffAccount, 0 AS IdProject, 2 AS ProjectionCriteria, tblCalcModelYears.Year, 
		CAST(0 AS BIT) AS BaseYear, tblCalcBaseCostByField.TFNormal, tblCalcBaseCostByField.TFPessimistic, tblCalcBaseCostByField.TFOptimistic, tblCalcBaseCostByField.BCRNormal, 
		tblCalcBaseCostByField.BCRPessimistic, tblCalcBaseCostByField.BCROptimistic, 1 AS BaseCostType, 1 AS Operation
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcModelYears ON tblCalcBaseCostByField.IdModel = tblCalcModelYears.IdModel INNER JOIN
		tblCalcTechnicalAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalAssumption.IdModel AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalAssumption.IdZiffAccount
	WHERE tblCalcBaseCostByField.TypeAllocation=1 AND tblCalcModelYears.Year<=@LocMaxYear AND tblCalcTechnicalAssumption.ProjectionCriteria=2 AND tblCalcBaseCostByField.IdModel=@LocIdModel;
	
	/** (4) Update Company Projection - Semi-Fixed (Shared) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 4, CBCSUM.IdModel, CBCSUM.IdActivity, CBCSUM.CodeAct, CBCSUM.Activity, CBCSUM.IdResources, CBCSUM.CodeRes, CBCSUM.Resource, CBCSUM.IdClientAccount, CBCSUM.Account, CBCSUM.NameAccount, 
		tblCalcTechnicalSemifixedSplit.IdField, NULL AS Field, CBCSUM.IdClientCostCenter, CBCSUM.ClientCostCenter, CBCSUM.Amount, CBCSUM.TypeAllocation, CBCSUM.IdZiffAccount, CBCSUM.CodeZiff, 
		CBCSUM.ZiffAccount, CBCSUM.IdRootZiffAccount, CBCSUM.RootCodeZiff, CBCSUM.RootZiffAccount, 0 AS IdProject, 2 AS ProjectionCriteria, tblCalcTechnicalSemifixedSplit.Year, 
		CAST(0 AS BIT) AS BaseYear, 1 AS TFNormal, 1 AS TFPessimistic, 1 AS TFOptimistic, CBCSUM.Amount * tblCalcTechnicalSemifixedSplit.SplitNormal AS BCRNormal, 
		CBCSUM.Amount * tblCalcTechnicalSemifixedSplit.SplitPessimistic AS BCRPessimistic, CBCSUM.Amount * tblCalcTechnicalSemifixedSplit.SplitOptimistic AS BCROptimistic,
		1 AS BaseCostType, 1 AS Operation
	FROM
	(
		SELECT IdModel, IdActivity, CodeAct, Activity, IdResources, CodeRes, Resource, IdClientAccount, Account, NameAccount, IdClientCostCenter, ClientCostCenter, SUM(Amount) AS Amount, TypeAllocation, 
			IdZiffAccount, CodeZiff, ZiffAccount, IdRootZiffAccount, RootCodeZiff, RootZiffAccount
		FROM tblCalcBaseCostByField
		GROUP BY IdModel, IdActivity, CodeAct, Activity, IdResources, CodeRes, Resource, IdClientAccount, Account, NameAccount, IdClientCostCenter, ClientCostCenter, TypeAllocation, IdZiffAccount, CodeZiff, 
			ZiffAccount, IdRootZiffAccount, RootCodeZiff, RootZiffAccount
		HAVING TypeAllocation<>1 AND IdModel=@LocIdModel
	) AS CBCSUM INNER JOIN
		tblCalcTechnicalSemifixedSplit ON CBCSUM.IdModel = tblCalcTechnicalSemifixedSplit.IdModel AND CBCSUM.IdActivity = tblCalcTechnicalSemifixedSplit.IdActivity AND 
		CBCSUM.IdResources = tblCalcTechnicalSemifixedSplit.IdResources AND CBCSUM.IdClientAccount = tblCalcTechnicalSemifixedSplit.IdClientAccount AND 
		CBCSUM.IdClientCostCenter = tblCalcTechnicalSemifixedSplit.IdClientCostCenter AND CBCSUM.IdZiffAccount = tblCalcTechnicalSemifixedSplit.IdZiffAccount AND 
		CBCSUM.TypeAllocation = tblCalcTechnicalSemifixedSplit.TypeAllocation;
	
	/** 1 - Fixed, 2 - SemiFixed, 3 - Variable, 4 - Cyclical, 5 - SemiVariable **/
	/** (6) Update Company Projection - Fixed **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT DISTINCT 6, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, 
		#tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, #tblCalcProjectionTechnical.NameAccount, 
		#tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, tblCalcTechnicalDriverAssumption.Year, CAST(0 AS BIT) AS BaseYear, 
		#tblCalcProjectionTechnical.TFNormal,#tblCalcProjectionTechnical.TFPessimistic,#tblCalcProjectionTechnical.TFOptimistic,#tblCalcProjectionTechnical.BCRNormal, #tblCalcProjectionTechnical.BCRPessimistic, 
		#tblCalcProjectionTechnical.BCROptimistic, 1 AS BaseCostType, tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		#tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumption.IdModel = #tblCalcProjectionTechnical.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = #tblCalcProjectionTechnical.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = #tblCalcProjectionTechnical.IdField
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=1) 
	AND #tblCalcProjectionTechnical.BaseYear=1 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND #tblCalcProjectionTechnical.BaseCostType=1 
	AND tblCalcTechnicalDriverAssumption.IdModel=@LocIdModel;
		
	/** (7) Update Ziff Projection - Fixed **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT DISTINCT 7, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, 
		#tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, #tblCalcProjectionTechnical.NameAccount, 
		#tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.Year, CAST(0 AS BIT) AS BaseYear, 
		#tblCalcProjectionTechnical.TFNormal, #tblCalcProjectionTechnical.TFPessimistic, #tblCalcProjectionTechnical.TFOptimistic, 
		#tblCalcProjectionTechnical.BCRNormal, #tblCalcProjectionTechnical.BCRPessimistic, #tblCalcProjectionTechnical.BCROptimistic, 2 AS BaseCostType, tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		#tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumptionZiff.IdModel = #tblCalcProjectionTechnical.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = #tblCalcProjectionTechnical.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = #tblCalcProjectionTechnical.IdField
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=1) 
	AND #tblCalcProjectionTechnical.BaseYear=1 
	AND #tblCalcProjectionTechnical.BaseCostType=2 
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0 
	AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@LocIdModel;
		
	/** (8) Update Company Projection - Variable and Cyclical **/
	/** Baselines **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 8, tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.CodeAct, tblCalcBaseCostByField.Activity, 
		tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.CodeRes, tblCalcBaseCostByField.Resource, 
		tblCalcBaseCostByField.IdClientAccount, tblCalcBaseCostByField.Account, tblCalcBaseCostByField.NameAccount, tblCalcBaseCostByField.IdField, 
		tblCalcBaseCostByField.Field, tblCalcBaseCostByField.IdClientCostCenter, tblCalcBaseCostByField.ClientCostCenter, 
		tblCalcBaseCostByField.Amount, tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.CodeZiff, 
		tblCalcBaseCostByField.ZiffAccount, tblCalcBaseCostByField.IdRootZiffAccount, tblCalcBaseCostByField.RootCodeZiff, 
		tblCalcBaseCostByField.RootZiffAccount, tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumption.Year, tblCalcTechnicalDriverAssumption.BaseYear, 
		tblCalcTechnicalDriverAssumption.TFNormal,tblCalcTechnicalDriverAssumption.TFPessimistic,tblCalcTechnicalDriverAssumption.TFOptimistic, 
		tblCalcBaseCostByField.BCRNormal, tblCalcBaseCostByField.BCRPessimistic, tblCalcBaseCostByField.BCROptimistic, 1 AS BaseCostType, 
		tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=3 OR tblCalcTechnicalDriverAssumption.ProjectionCriteria=4) 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND tblCalcBaseCostByField.IdModel=@LocIdModel
	AND tblCalcTechnicalDriverAssumption.Operation = 1;
	
	/** (9) Business Opportunities **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 9, tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.CodeAct, tblCalcBaseCostByField.Activity, 
		tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.CodeRes, tblCalcBaseCostByField.Resource, 
		tblCalcBaseCostByField.IdClientAccount, tblCalcBaseCostByField.Account, tblCalcBaseCostByField.NameAccount, tblCalcBaseCostByField.IdField, 
		tblCalcBaseCostByField.Field, tblCalcBaseCostByField.IdClientCostCenter, tblCalcBaseCostByField.ClientCostCenter, 
		tblCalcBaseCostByField.Amount, tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.CodeZiff, 
		tblCalcBaseCostByField.ZiffAccount, tblCalcBaseCostByField.IdRootZiffAccount, tblCalcBaseCostByField.RootCodeZiff, 
		tblCalcBaseCostByField.RootZiffAccount, tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumption.Year, tblCalcTechnicalDriverAssumption.BaseYear, 
		tblCalcTechnicalDriverAssumption.TFNormal, tblCalcTechnicalDriverAssumption.TFPessimistic, tblCalcTechnicalDriverAssumption.TFOptimistic, 
		tblCalcBaseCostByField.BCRNormal, tblCalcBaseCostByField.BCRPessimistic, tblCalcBaseCostByField.BCROptimistic, 1 AS BaseCostType, 
		tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=3 OR tblCalcTechnicalDriverAssumption.ProjectionCriteria=4) 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND tblCalcBaseCostByField.IdModel=@LocIdModel
	AND tblCalcTechnicalDriverAssumption.Operation <> 1;

	/** (10) Update Ziff Projection - Variable and Cyclical **/
	/** Baselines **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 10, tblCalcBaseCostByFieldZiff.IdModel, NULL, NULL, NULL, NULL, NULL, NULL, 
		NULL, NULL, NULL, tblCalcBaseCostByFieldZiff.IdField, tblCalcBaseCostByFieldZiff.Field, NULL, NULL, 
		tblCalcBaseCostByFieldZiff.TotalCost, NULL, tblCalcBaseCostByFieldZiff.IdZiffAccount, tblCalcBaseCostByFieldZiff.CodeZiff, 
		tblCalcBaseCostByFieldZiff.ZiffAccount, tblCalcBaseCostByFieldZiff.IdRootZiffAccount, tblCalcBaseCostByFieldZiff.RootCodeZiff, 
		tblCalcBaseCostByFieldZiff.RootZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumptionZiff.Year, tblCalcTechnicalDriverAssumptionZiff.BaseYear, tblCalcBaseCostByFieldZiff.TFNormal,tblCalcBaseCostByFieldZiff.TFPessimistic,tblCalcBaseCostByFieldZiff.TFOptimistic, 
		tblCalcBaseCostByFieldZiff.BCRNormal, tblCalcBaseCostByFieldZiff.BCRPessimistic, tblCalcBaseCostByFieldZiff.BCROptimistic, 2 AS BaseCostType, 
		tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=3 OR tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=4) 
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0 
	AND tblCalcBaseCostByFieldZiff.IdModel=@LocIdModel
	AND tblCalcTechnicalDriverAssumptionZiff.Operation=1;

	/** (11) Business Opportunities **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 11, tblCalcBaseCostByFieldZiff.IdModel, NULL, NULL, NULL, NULL, NULL, NULL, 
		NULL, NULL, NULL, tblCalcBaseCostByFieldZiff.IdField, tblCalcBaseCostByFieldZiff.Field, NULL, NULL, 
		tblCalcBaseCostByFieldZiff.TotalCost, NULL, tblCalcBaseCostByFieldZiff.IdZiffAccount, tblCalcBaseCostByFieldZiff.CodeZiff, 
		tblCalcBaseCostByFieldZiff.ZiffAccount, tblCalcBaseCostByFieldZiff.IdRootZiffAccount, tblCalcBaseCostByFieldZiff.RootCodeZiff, 
		tblCalcBaseCostByFieldZiff.RootZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumptionZiff.Year, tblCalcTechnicalDriverAssumptionZiff.BaseYear, tblCalcTechnicalDriverAssumptionZiff.TFNormal, 
		tblCalcTechnicalDriverAssumptionZiff.TFPessimistic, tblCalcTechnicalDriverAssumptionZiff.TFOptimistic, tblCalcBaseCostByFieldZiff.BCRNormal, 
		tblCalcBaseCostByFieldZiff.BCRPessimistic, tblCalcBaseCostByFieldZiff.BCROptimistic, 2 AS BaseCostType, 
		tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=3 OR tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=4)
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0 AND tblCalcBaseCostByFieldZiff.IdModel=@LocIdModel
	AND tblCalcTechnicalDriverAssumptionZiff.Operation<>1;

	/** (12) Update Company Projection - SemiVariable (BaseLine) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT DISTINCT 12, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, 
		#tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, #tblCalcProjectionTechnical.NameAccount, 
		#tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, tblCalcTechnicalDriverAssumption.Year, CAST(0 AS BIT) AS BaseYear, 
		#tblCalcProjectionTechnical.TFNormal, #tblCalcProjectionTechnical.TFPessimistic, #tblCalcProjectionTechnical.TFOptimistic, 
		#tblCalcProjectionTechnical.BCRNormal, #tblCalcProjectionTechnical.BCRPessimistic, #tblCalcProjectionTechnical.BCROptimistic, 1 AS BaseCostType, tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
		#tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumption.IdModel = #tblCalcProjectionTechnical.IdModel AND 
		tblCalcTechnicalDriverAssumption.IdZiffAccount = #tblCalcProjectionTechnical.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumption.IdField = #tblCalcProjectionTechnical.IdField AND
		tblCalcTechnicalDriverAssumption.IdProject = #tblCalcProjectionTechnical.IdProject
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=5) 
	AND #tblCalcProjectionTechnical.BaseYear=1 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND #tblCalcProjectionTechnical.BaseCostType=1 
	AND #tblCalcProjectionTechnical.Operation=1
	AND tblCalcTechnicalDriverAssumption.IdModel=@LocIdModel;

	/** (13) Update Ziff Projection - SemiVariable (BaseLine) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT DISTINCT 13, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, 
		#tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, #tblCalcProjectionTechnical.NameAccount, 
		#tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, tblCalcTechnicalDriverAssumptionZiff.Year, CAST(0 AS BIT) AS BaseYear, 
		0,0,0,0,0,0, 2 AS BaseCostType, tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcTechnicalDriverAssumptionZiff INNER JOIN
		#tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumptionZiff.IdModel = #tblCalcProjectionTechnical.IdModel AND 
		tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount = #tblCalcProjectionTechnical.IdZiffAccount AND 
		tblCalcTechnicalDriverAssumptionZiff.IdField = #tblCalcProjectionTechnical.IdField		
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5) 
	AND #tblCalcProjectionTechnical.BaseYear=1 
	AND #tblCalcProjectionTechnical.BaseCostType=2 
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0
	AND tblCalcTechnicalDriverAssumptionZiff.Operation=1 
	AND tblCalcTechnicalDriverAssumptionZiff.IdModel=@LocIdModel;

	/** (14) Update Company Projection - SemiVariable (Business Opportunity) **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 14, tblCalcBaseCostByField.IdModel, tblCalcBaseCostByField.IdActivity, tblCalcBaseCostByField.CodeAct, tblCalcBaseCostByField.Activity, 
		tblCalcBaseCostByField.IdResources, tblCalcBaseCostByField.CodeRes, tblCalcBaseCostByField.Resource, 
		tblCalcBaseCostByField.IdClientAccount, tblCalcBaseCostByField.Account, tblCalcBaseCostByField.NameAccount, tblCalcBaseCostByField.IdField, 
		tblCalcBaseCostByField.Field, tblCalcBaseCostByField.IdClientCostCenter, tblCalcBaseCostByField.ClientCostCenter, 
		tblCalcBaseCostByField.Amount, tblCalcBaseCostByField.TypeAllocation, tblCalcBaseCostByField.IdZiffAccount, tblCalcBaseCostByField.CodeZiff, 
		tblCalcBaseCostByField.ZiffAccount, tblCalcBaseCostByField.IdRootZiffAccount, tblCalcBaseCostByField.RootCodeZiff, 
		tblCalcBaseCostByField.RootZiffAccount, tblCalcTechnicalDriverAssumption.IdProject, tblCalcTechnicalDriverAssumption.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumption.Year, tblCalcTechnicalDriverAssumption.BaseYear, 
		tblCalcTechnicalDriverAssumption.TFNormal, tblCalcTechnicalDriverAssumption.TFPessimistic, tblCalcTechnicalDriverAssumption.TFOptimistic, 
		tblCalcBaseCostByField.BCRNormal, tblCalcBaseCostByField.BCRPessimistic,tblCalcBaseCostByField.BCROptimistic, 
		1 AS BaseCostType, 
		tblCalcTechnicalDriverAssumption.Operation
	FROM tblCalcBaseCostByField INNER JOIN
		tblCalcTechnicalDriverAssumption ON tblCalcBaseCostByField.IdModel = tblCalcTechnicalDriverAssumption.IdModel AND 
		tblCalcBaseCostByField.IdField = tblCalcTechnicalDriverAssumption.IdField AND 
		tblCalcBaseCostByField.IdZiffAccount = tblCalcTechnicalDriverAssumption.IdZiffAccount
	WHERE (tblCalcTechnicalDriverAssumption.ProjectionCriteria=5) 
	AND tblCalcTechnicalDriverAssumption.BaseYear=0 
	AND tblCalcTechnicalDriverAssumption.Operation=2
	AND tblCalcBaseCostByField.IdModel=@LocIdModel;

	/** (15) Update Ziff Projection - SemiVariable (Business Opportunity)  **/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation])
	SELECT 15, tblCalcBaseCostByFieldZiff.IdModel, NULL, NULL, NULL, NULL, NULL, NULL, 
		NULL, NULL, NULL, tblCalcBaseCostByFieldZiff.IdField, tblCalcBaseCostByFieldZiff.Field, NULL, NULL, 
		tblCalcBaseCostByFieldZiff.TotalCost, NULL, tblCalcBaseCostByFieldZiff.IdZiffAccount, tblCalcBaseCostByFieldZiff.CodeZiff, 
		tblCalcBaseCostByFieldZiff.ZiffAccount, tblCalcBaseCostByFieldZiff.IdRootZiffAccount, tblCalcBaseCostByFieldZiff.RootCodeZiff, 
		tblCalcBaseCostByFieldZiff.RootZiffAccount, tblCalcTechnicalDriverAssumptionZiff.IdProject, tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria, 
		tblCalcTechnicalDriverAssumptionZiff.Year, tblCalcTechnicalDriverAssumptionZiff.BaseYear, 
		tblCalcTechnicalDriverAssumptionZiff.TFNormal, tblCalcTechnicalDriverAssumptionZiff.TFPessimistic, tblCalcTechnicalDriverAssumptionZiff.TFOptimistic, 
		tblCalcBaseCostByFieldZiff.BCRNormal, tblCalcBaseCostByFieldZiff.BCRPessimistic, tblCalcBaseCostByFieldZiff.BCROptimistic, 
		2 AS BaseCostType, 
		tblCalcTechnicalDriverAssumptionZiff.Operation
	FROM tblCalcBaseCostByFieldZiff INNER JOIN
		tblCalcTechnicalDriverAssumptionZiff ON tblCalcBaseCostByFieldZiff.IdModel = tblCalcTechnicalDriverAssumptionZiff.IdModel AND 
		tblCalcBaseCostByFieldZiff.IdField = tblCalcTechnicalDriverAssumptionZiff.IdField AND 
		tblCalcBaseCostByFieldZiff.IdZiffAccount = tblCalcTechnicalDriverAssumptionZiff.IdZiffAccount
	WHERE (tblCalcTechnicalDriverAssumptionZiff.ProjectionCriteria=5) 
	AND tblCalcTechnicalDriverAssumptionZiff.BaseYear=0 
	AND tblCalcTechnicalDriverAssumptionZiff.Operation=2
	AND tblCalcBaseCostByFieldZiff.IdModel=@LocIdModel;

	/** Volume preparation tables **/
	/** Get Base Year **/
	DECLARE @BaseYear INT
	SELECT @BaseYear = [BaseYear] FROM [DBCPM].[dbo].[tblModels] WHERE IdModel=@LocIdModel

	/** Volume per driver **/
	CREATE TABLE #VolumePerField ([IdModel] INT, [IdTechnicalDriver] INT, [IdField] INT, [DriverName] NVARCHAR(255), [Year] INT, [NormalAmount] FLOAT, [PessimisticAmount] FLOAT, [OptimisticAmount] FLOAT) ON [PRIMARY]
	INSERT INTO #VolumePerField ([IdModel], [IdTechnicalDriver], [IdField], [DriverName], [Year], [NormalAmount], [PessimisticAmount], [OptimisticAmount])
	SELECT tblTechnicalDrivers.IdModel, tblTechnicalDrivers.IdTechnicalDriver, tblTechnicalDriverData.IdField, tblTechnicalDrivers.Name AS DriverName, tblTechnicalDriverData.Year, SUM(tblTechnicalDriverData.Normal), SUM(tblTechnicalDriverData.Pessimistic), SUM(tblTechnicalDriverData.Optimistic)
	FROM tblTechnicalDrivers INNER JOIN tblTechnicalDriverData ON tblTechnicalDriverData.IdModel=tblTechnicalDrivers.IdModel AND tblTechnicalDriverData.IdTechnicalDriver=tblTechnicalDrivers.IdTechnicalDriver
	WHERE tblTechnicalDrivers.IdModel=@LocIdModel
	GROUP BY  tblTechnicalDrivers.IdModel, tblTechnicalDrivers.IdTechnicalDriver, tblTechnicalDriverData.IdField, tblTechnicalDrivers.Name, tblTechnicalDriverData.Year
	ORDER BY 2,3,5

	/** Volume joiner **/
	CREATE TABLE #VolumeJoiner ([IdModel] INT, [IdTechnicalDriver] INT, [IdField] INT, [Year] INT, [IdZiffAccount] INT) ON [PRIMARY]
	INSERT INTO #VolumeJoiner ([IdModel], [IdTechnicalDriver], [IdField], [Year], [IdZiffAccount])
	SELECT #tblCalcProjectionTechnical.IdModel, tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize, #tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Year, #tblCalcProjectionTechnical.IdZiffAccount
	FROM tblCalcTechnicalDriverAssumption INNER JOIN
         #tblCalcProjectionTechnical ON tblCalcTechnicalDriverAssumption.IdModel = #tblCalcProjectionTechnical.IdModel AND 
         tblCalcTechnicalDriverAssumption.IdField = #tblCalcProjectionTechnical.IdField AND tblCalcTechnicalDriverAssumption.Year = #tblCalcProjectionTechnical.Year AND 
         tblCalcTechnicalDriverAssumption.IdZiffAccount = #tblCalcProjectionTechnical.IdZiffAccount
	WHERE (#tblCalcProjectionTechnical.ProjectionCriteria = 5) AND (#tblCalcProjectionTechnical.Operation = 2)
	GROUP BY #tblCalcProjectionTechnical.IdModel, tblCalcTechnicalDriverAssumption.IdTechnicalDriverSize, #tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Year, #tblCalcProjectionTechnical.IdZiffAccount
	HAVING (#tblCalcProjectionTechnical.IdModel = @LocIdModel)
    ORDER BY 2,3,4

	/** Update the SemiVariable Factor of Business Oportunites depending on Volume versus Driver Capacity**/
	UPDATE CPT
	SET CPT.TFNormal =	CASE WHEN #VolumePerField.NormalAmount > VPF.BaseNormalAmount/(A.UtilizationValue/100)  THEN CPT.TFNormal ELSE 0 END, 
		CPT.TFPessimistic = CASE WHEN #VolumePerField.PessimisticAmount > VPF.BasePessimisticAmount/(A.UtilizationValue/100) THEN CPT.TFPessimistic ELSE 0 END, 
		CPT.TFOptimistic = CASE WHEN #VolumePerField.OptimisticAmount > VPF.BaseOptimisticAmount/(A.UtilizationValue/100) THEN CPT.TFOptimistic ELSE 0 END 
	FROM #tblCalcProjectionTechnical CPT INNER JOIN
		 #VolumeJoiner ON #VolumeJoiner.IdModel = CPT.IdModel AND #VolumeJoiner.IdField = CPT.IdField AND #VolumeJoiner.Year = CPT.Year AND 
		 #VolumeJoiner.IdZiffAccount = CPT.IdZiffAccount INNER JOIN
		 #VolumePerField ON #VolumePerField.IdModel = #VolumeJoiner.IdModel AND #VolumePerField.IdField = #VolumeJoiner.IdField AND 
		 #VolumePerField.Year = #VolumeJoiner.Year AND #VolumePerField.IdTechnicalDriver = #VolumeJoiner.IdTechnicalDriver  INNER JOIN
		(
		SELECT     IdField, IdTechnicalDriver, NormalAmount AS BaseNormalAmount, PessimisticAmount AS BasePessimisticAmount, OptimisticAmount AS BaseOptimisticAmount
		FROM         #VolumePerField
		WHERE     (IdModel = @LocIdModel) AND (Year = @BaseYear)
		) AS VPF ON
		 VPF.IdTechnicalDriver=#VolumeJoiner.IdTechnicalDriver AND
		 VPF.IdField=#VolumeJoiner.IdField  INNER JOIN
		 tblUtilizationCapacity A ON A.IdModel = #VolumeJoiner.IdModel AND A.IdField = #VolumeJoiner.IdField AND 
		 A.IdZiffAccount = #VolumeJoiner.IdZiffAccount INNER JOIN
		 tblFields ON A.IdField = tblFields.IdField
	WHERE     (CPT.ProjectionCriteria=5) AND (CPT.Operation = 2)

	/** Apply denominator if applicable **/
	/** Create temporary table to house changes **/
	IF OBJECT_ID('tempdb..#HousingDataTable') IS NOT NULL DROP TABLE #HousingDataTable
	IF OBJECT_ID('tempdb..#HousingDataTableZiff') IS NOT NULL DROP TABLE #HousingDataTableZiff
	SELECT 
		#tblCalcProjectionTechnical.SourceDebug, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, #tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, 
		#tblCalcProjectionTechnical.NameAccount, #tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		TDUCD.IdProject, #tblCalcProjectionTechnical.ProjectionCriteria, #tblCalcProjectionTechnical.Year, #tblCalcProjectionTechnical.BaseYear, 
		#tblCalcProjectionTechnical.TFNormal * TDUCD.TFNormal AS TFNormal,
		#tblCalcProjectionTechnical.TFPessimistic * TDUCD.TFPessimistic AS TFPessimistic,
		#tblCalcProjectionTechnical.TFOptimistic * TDUCD.TFOptimistic AS TFOptimistic,
		#tblCalcProjectionTechnical.BCRNormal, #tblCalcProjectionTechnical.BCRPessimistic, 
		#tblCalcProjectionTechnical.BCROptimistic, #tblCalcProjectionTechnical.BaseCostType, #tblCalcProjectionTechnical.Operation,#tblCalcProjectionTechnical.SortOrder
	INTO #HousingDataTable	
	FROM #tblCalcProjectionTechnical INNER JOIN
		(
		SELECT tblCalcUnitCostDenominatorAmount.IdModel, tblCalcUnitCostDenominatorAmount.IdField, tblCalcUnitCostDenominatorAmount.IdProject, 
		tblCalcUnitCostDenominatorAmount.Year, 
		CASE WHEN ISNULL(tblCalcBaselineUnitCostDenominatorSumAmount.SumNormal, 0) = 0 THEN 0 ELSE tblCalcUnitCostDenominatorAmount.SumNormal / tblCalcBaselineUnitCostDenominatorSumAmount.SumNormal END AS TFNormal, 
		CASE WHEN ISNULL(tblCalcBaselineUnitCostDenominatorSumAmount.SumPessimistic, 0) = 0 THEN 0 ELSE tblCalcUnitCostDenominatorAmount.SumPessimistic / tblCalcBaselineUnitCostDenominatorSumAmount.SumPessimistic END AS TFPessimistic, 
		CASE WHEN ISNULL(tblCalcBaselineUnitCostDenominatorSumAmount.SumOptimistic, 0) = 0 THEN 0 ELSE tblCalcUnitCostDenominatorAmount.SumOptimistic / tblCalcBaselineUnitCostDenominatorSumAmount.SumOptimistic END AS TFOptimistic
		FROM tblCalcUnitCostDenominatorAmount INNER JOIN
		tblCalcBaselineUnitCostDenominatorSumAmount ON 
		tblCalcUnitCostDenominatorAmount.IdModel = tblCalcBaselineUnitCostDenominatorSumAmount.IdModel AND 
		tblCalcUnitCostDenominatorAmount.IdField = tblCalcBaselineUnitCostDenominatorSumAmount.IdField AND 
		tblCalcUnitCostDenominatorAmount.Year = tblCalcBaselineUnitCostDenominatorSumAmount.Year RIGHT OUTER JOIN 
		tblProjects  ON tblCalcUnitCostDenominatorAmount.IdModel = tblProjects.IdModel AND
		tblCalcUnitCostDenominatorAmount.IdField = tblProjects.IdField AND 
		tblCalcUnitCostDenominatorAmount.IdProject = tblProjects.IdProject
		WHERE tblProjects.Operation=1
		AND tblCalcUnitCostDenominatorAmount.IdModel=@LocIdModel) AS TDUCD ON 
		#tblCalcProjectionTechnical.IdModel = TDUCD.IdModel AND #tblCalcProjectionTechnical.IdField = TDUCD.IdField AND #tblCalcProjectionTechnical.Year = TDUCD.Year
	WHERE #tblCalcProjectionTechnical.IdModel=@LocIdModel AND #tblCalcProjectionTechnical.BaseCostType = 1 AND #tblCalcProjectionTechnical.Operation = 1;

	SELECT 
		#tblCalcProjectionTechnical.SourceDebug, #tblCalcProjectionTechnical.IdModel, #tblCalcProjectionTechnical.IdActivity, #tblCalcProjectionTechnical.CodeAct, #tblCalcProjectionTechnical.Activity, 
		#tblCalcProjectionTechnical.IdResources, #tblCalcProjectionTechnical.CodeRes, #tblCalcProjectionTechnical.Resource, #tblCalcProjectionTechnical.IdClientAccount, #tblCalcProjectionTechnical.Account, 
		#tblCalcProjectionTechnical.NameAccount, #tblCalcProjectionTechnical.IdField, #tblCalcProjectionTechnical.Field, #tblCalcProjectionTechnical.IdClientCostCenter, 
		#tblCalcProjectionTechnical.ClientCostCenter, #tblCalcProjectionTechnical.Amount, #tblCalcProjectionTechnical.TypeAllocation, 
		#tblCalcProjectionTechnical.IdZiffAccount, #tblCalcProjectionTechnical.CodeZiff, #tblCalcProjectionTechnical.ZiffAccount, 
		#tblCalcProjectionTechnical.IdRootZiffAccount, #tblCalcProjectionTechnical.RootCodeZiff, #tblCalcProjectionTechnical.RootZiffAccount, 
		TDUCD.IdProject, #tblCalcProjectionTechnical.ProjectionCriteria, #tblCalcProjectionTechnical.Year, #tblCalcProjectionTechnical.BaseYear, 
		#tblCalcProjectionTechnical.TFNormal * TDUCD.TFNormal AS TFNormal,
		#tblCalcProjectionTechnical.TFPessimistic * TDUCD.TFPessimistic AS TFPessimistic,
		#tblCalcProjectionTechnical.TFOptimistic * TDUCD.TFOptimistic AS TFOptimistic,
		#tblCalcProjectionTechnical.BCRNormal, #tblCalcProjectionTechnical.BCRPessimistic, 
		#tblCalcProjectionTechnical.BCROptimistic, #tblCalcProjectionTechnical.BaseCostType, #tblCalcProjectionTechnical.Operation,#tblCalcProjectionTechnical.SortOrder
	INTO #HousingDataTableZiff	
	FROM #tblCalcProjectionTechnical INNER JOIN
		(
		SELECT tblCalcUnitCostDenominatorAmount.IdModel, tblCalcUnitCostDenominatorAmount.IdField, tblCalcUnitCostDenominatorAmount.IdProject, 
		tblCalcUnitCostDenominatorAmount.Year, 
		CASE WHEN ISNULL(tblCalcBaselineUnitCostDenominatorSumAmount.SumNormal, 0) = 0 THEN 0 ELSE tblCalcUnitCostDenominatorAmount.SumNormal / tblCalcBaselineUnitCostDenominatorSumAmount.SumNormal END AS TFNormal, 
		CASE WHEN ISNULL(tblCalcBaselineUnitCostDenominatorSumAmount.SumPessimistic, 0) = 0 THEN 0 ELSE tblCalcUnitCostDenominatorAmount.SumPessimistic / tblCalcBaselineUnitCostDenominatorSumAmount.SumPessimistic END AS TFPessimistic, 
		CASE WHEN ISNULL(tblCalcBaselineUnitCostDenominatorSumAmount.SumOptimistic, 0) = 0 THEN 0 ELSE tblCalcUnitCostDenominatorAmount.SumOptimistic / tblCalcBaselineUnitCostDenominatorSumAmount.SumOptimistic END AS TFOptimistic
		FROM tblCalcUnitCostDenominatorAmount INNER JOIN
		tblCalcBaselineUnitCostDenominatorSumAmount ON 
		tblCalcUnitCostDenominatorAmount.IdModel = tblCalcBaselineUnitCostDenominatorSumAmount.IdModel AND 
		tblCalcUnitCostDenominatorAmount.IdField = tblCalcBaselineUnitCostDenominatorSumAmount.IdField AND 
		tblCalcUnitCostDenominatorAmount.Year = tblCalcBaselineUnitCostDenominatorSumAmount.Year RIGHT OUTER JOIN 
		tblProjects  ON tblCalcUnitCostDenominatorAmount.IdModel = tblProjects.IdModel AND
		tblCalcUnitCostDenominatorAmount.IdField = tblProjects.IdField AND 
		tblCalcUnitCostDenominatorAmount.IdProject = tblProjects.IdProject
		WHERE tblProjects.Operation=1
		AND tblCalcUnitCostDenominatorAmount.IdModel=@LocIdModel) AS TDUCD ON 
		#tblCalcProjectionTechnical.IdModel = TDUCD.IdModel AND #tblCalcProjectionTechnical.IdField = TDUCD.IdField AND #tblCalcProjectionTechnical.Year = TDUCD.Year
	WHERE #tblCalcProjectionTechnical.IdModel=@LocIdModel AND #tblCalcProjectionTechnical.BaseCostType = 2 AND #tblCalcProjectionTechnical.Operation = 1;

	/** Delete all data prior to Base Year for multiple baselines **/
	DELETE  CPT
	FROM #tblCalcProjectionTechnical CPT INNER JOIN tblProjects ON tblProjects.IdModel = CPT.IdModel 
	WHERE CPT.IdModel = @LocIdModel
	AND tblProjects.Operation=1
	AND CPT.BaseYear=0
	AND CPT.Year = tblProjects.Starts

	/** Delete data from #tblCalcProjectionTechnical **/
	DELETE FROM #tblCalcProjectionTechnical WHERE IdModel=@LocIdModel AND IdProject=0

	/** Insert data from #HousingDataTable into #tblCalcProjectionTechnical  for that field**/
	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[SortOrder])
	SELECT SourceDebug, IdModel, IdActivity, CodeAct, Activity,IdResources, CodeRes, Resource, IdClientAccount, Account, NameAccount, IdField, Field, IdClientCostCenter, 
		ClientCostCenter, Amount, TypeAllocation, IdZiffAccount, CodeZiff, ZiffAccount, IdRootZiffAccount, RootCodeZiff, RootZiffAccount, IdProject, ProjectionCriteria, Year, BaseYear, 
		TFNormal, TFPessimistic, TFOptimistic,BCRNormal, BCRPessimistic, BCROptimistic, BaseCostType, Operation,SortOrder
	FROM #HousingDataTable	

	INSERT INTO #tblCalcProjectionTechnical ([SourceDebug],[IdModel],[IdActivity],[CodeAct],[Activity],[IdResources],[CodeRes],[Resource],[IdClientAccount],[Account],[NameAccount],[IdField],[Field],[IdClientCostCenter],[ClientCostCenter],[Amount],[TypeAllocation],[IdZiffAccount],[CodeZiff],[ZiffAccount],[IdRootZiffAccount],[RootCodeZiff],[RootZiffAccount],[IdProject],[ProjectionCriteria],[Year],[BaseYear],[TFNormal],[TFPessimistic],[TFOptimistic],[BCRNormal],[BCRPessimistic],[BCROptimistic],[BaseCostType],[Operation],[SortOrder])
	SELECT SourceDebug, IdModel, IdActivity, CodeAct, Activity,IdResources, CodeRes, Resource, IdClientAccount, Account, NameAccount, IdField, Field, IdClientCostCenter, 
		ClientCostCenter, Amount, TypeAllocation, IdZiffAccount, CodeZiff, ZiffAccount, IdRootZiffAccount, RootCodeZiff, RootZiffAccount, IdProject, ProjectionCriteria, Year, BaseYear, 
		TFNormal, TFPessimistic, TFOptimistic,BCRNormal, BCRPessimistic, BCROptimistic, BaseCostType, Operation,SortOrder
	FROM #HousingDataTableZiff	

	DELETE FROM #tblCalcProjectionTechnical WHERE IdModel=@LocIdModel AND BCRNormal =0 AND BCROptimistic=0 AND BCRPessimistic=0 
	DELETE FROM #tblCalcProjectionTechnical WHERE IdModel=@LocIdModel AND TFNormal=0 AND TFOptimistic=0 AND TFNormal=0
	
	/** Prep for Reporting **/
	UPDATE #tblCalcProjectionTechnical SET [Activity]='Ziff/Third-party Costs',[SortOrder]=2 WHERE [Activity] IS NULL AND [BaseCostType]=2 AND [IdModel]=@LocIdModel;
	UPDATE #tblCalcProjectionTechnical SET [Resource]='Ziff/Third-party Costs',[SortOrder]=2 WHERE [Resource] IS NULL AND [BaseCostType]=2 AND [IdModel]=@LocIdModel;
	UPDATE #tblCalcProjectionTechnical SET [Project]=tblProjects.Name, [Operation]=tblProjects.Operation FROM #tblCalcProjectionTechnical INNER JOIN tblProjects ON #tblCalcProjectionTechnical.IdProject = tblProjects.IdProject AND #tblCalcProjectionTechnical.IdModel = tblProjects.IdModel WHERE #tblCalcProjectionTechnical.IdModel=@LocIdModel;
	UPDATE #tblCalcProjectionTechnical SET [Project]='Unknown Project' WHERE [Project] IS NULL AND [IdProject] IS NOT NULL AND [IdModel]=@LocIdModel;
	UPDATE #tblCalcProjectionTechnical SET [Field]=tblFields.Name FROM #tblCalcProjectionTechnical INNER JOIN tblFields ON #tblCalcProjectionTechnical.IdField = tblFields.IdField AND #tblCalcProjectionTechnical.IdModel = tblFields.IdModel WHERE #tblCalcProjectionTechnical.IdModel=@LocIdModel;

	----/** Move data from temp table tblCalcProjectionTecnical to permanent table #tblCalcProjectionTechnical **/
	DELETE FROM tblCalcProjectionTechnical where IdModel=@LocIdModel
	INSERT INTO tblCalcProjectionTechnical 
	SELECT DISTINCT * FROM #tblCalcProjectionTechnical

	/** Clean up temporary tables **/
	DROP TABLE #tblCalcProjectionTechnical
	DROP TABLE #VolumePerField
	DROP TABLE #VolumeJoiner

END



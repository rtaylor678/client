﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_master/default.master" AutoEventWireup="true" CodeBehind="settings.aspx.cs" Inherits="CPM._settings" %>

<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="cntCenter" runat="server">

    <script type="text/javascript">
        var clearaccess = function (grid) {
            grid.store.each(function (record, index) {
                record.set('DenyAccess', false);
                record.set('GrantAccess', false);
                grid.view.refresh(false);
            });
        }

        var grantaccess = function (grid) {
            grid.store.each(function (record, index) {
                record.set('DenyAccess', false);
                record.set('GrantAccess', true);
                grid.view.refresh(false);
            });
        }

        var denyaccess = function (grid) {
            grid.store.each(function (record, index) {
                record.set('DenyAccess', true);
                record.set('GrantAccess', false);
                grid.view.refresh(false);
            });
        }

        var prepareCommand = function (grid, command, record, row) {
            if (command.command == 'Delete' && record.data.BuiltIn == false) {
                command.disabled = false;
            }
        };
    </script>
    <ext:Panel ID="pnlTitlePanel" Layout="FitLayout" AutoScroll="true" runat="server" BodyStyle="background-color: transparent;">
        <TopBar>
            <ext:Toolbar Height="40" ID="tbMenu" runat="server" Flat="true" Cls="menubackground">
                <Items>
                    <ext:ToolbarSeparator runat="server" ID="sepAddSetting" Border="false" Width="2" />
                    <ext:Button ID="btnAdd" runat="server" IconCls="menu-addnew" Scale="Large">
                        <DirectEvents>
                            <Click OnEvent="btnAdd_Click">
                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:ToolbarFill />
                    <ext:Button ID="btnModels" runat="server" ToolTip="Case Selection" Text="Case Selection" IconCls="menu-modelselection" Scale="Large" style="font-weight:700;">
                        <DirectEvents>
                            <Click OnEvent="btnModel_Click">
                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </TopBar>
        <Items>
            <ext:GridPanel ID="grdSettings" runat="server" Border="false" Icon="TableKey">
                <Store>
                    <ext:Store ID="storeProfiles" runat="server">
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="IdUser">
                                <Fields>
                                    <ext:ModelField Name="IdProfile" Type="Int" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="BuiltIn" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:Column ID="colID" runat="server" Hidden="true" Text="ID" DataIndex="IdProfile" />
                        <ext:Column ID="Name" runat="server" Text="Name" DataIndex="Name" Flex="1" />
                        <ext:ImageCommandColumn Align="Center" Width="150" Text="Functions" ID="ImageCommandColumn1" runat="server">
                            <Commands>
                                <ext:ImageCommand CommandName="Edit" IconCls="icon-edit_16" Text="&nbsp;Edit" />
                                <ext:ImageCommand CommandName="Delete" IconCls="icon-delete_16" Text="&nbsp;Delete" Disabled="true" />
                            </Commands>
                            <PrepareCommand Fn="prepareCommand" />
                            <DirectEvents>
                                <Command OnEvent="grdSetting_Command">
                                    <ExtraParams>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw">
                                        </ext:Parameter>
                                        <ext:Parameter Name="IdProfile" Value="record.data.IdProfile" Mode="Raw">
                                        </ext:Parameter>
                                        <ext:Parameter Name="Name" Value="record.data.Name" Mode="Raw">
                                        </ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:ImageCommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" />
                </SelectionModel>
                <Features>
                    <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                        <Filters>
                            <ext:StringFilter DataIndex="Name" />
                            <ext:NumericFilter DataIndex="IdProfile" />
                        </Filters>
                    </ext:GridFilters>
                </Features>
            </ext:GridPanel>
        </Items>
    </ext:Panel>
    <ext:Window ID="winSettingEdit" runat="server" Hidden="true" Icon="TableKey" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
        <Items>
            <ext:FormPanel ID="pnlSetting" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                <Items>
                    <ext:Panel ID="Panel2" runat="server" Border="false" Header="false" ColumnWidth=".5" Layout="Form" LabelAlign="Top" Cls="PopupFormColumnPanel">
                        <Defaults>
                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                            <ext:Parameter Name="MsgTarget" Value="side" />
                        </Defaults>
                        <Items>
                            <ext:TextField ID="txtName" runat="server" AnchorHorizontal="92%" Cls="PopupFormField"/>
                        </Items>
                    </ext:Panel>
                    <ext:Panel ID="Panel3" runat="server" Border="false" Layout="Form" ColumnWidth=".5" LabelAlign="Top" Cls="PopupFormColumnPanel">
                        <Defaults>
                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                            <ext:Parameter Name="MsgTarget" Value="side" />
                        </Defaults>
                        <Items>
                            <ext:ComboBox ID="ddlType" runat="server" DisplayField="Name" Editable="false" TriggerAction="All" FieldLabel="Role Type" ValueField="Code" Cls="PopupFormField">
                                <Store>
                                    <ext:Store ID="storeType" runat="server" OnReadData="StoreType_ReadData">
                                        <Model>
                                            <ext:Model ID="modelType" runat="server" IDProperty="Code">
                                                <Fields>
                                                    <ext:ModelField Name="Code" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                                <Reader>
                                                    <ext:JsonReader />
                                                </Reader>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                        </Items>
                    </ext:Panel>
                    <ext:Panel ID="Panel4" runat="server" Border="false" Layout="Form" ColumnWidth="1" LabelAlign="Top" Cls="PopupFormColumnPanel">
                        <Defaults>
                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                            <ext:Parameter Name="MsgTarget" Value="side" />
                        </Defaults>
                        <Items>
                            <ext:ComboBox ID="ddlModels" runat="server" DisplayField="Name" Editable="false" TriggerAction="All" FieldLabel="Selected Case" ValueField="ID" Cls="PopupFormField">
                                <Store>
                                    <ext:Store ID="storeModels" runat="server" OnReadData="StoreModels_ReadData">
                                        <Model>
                                            <ext:Model ID="mdlModels" runat="server" IDProperty="ID">
                                                <Fields>
                                                    <ext:ModelField Name="ID" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                                <Reader>
                                                    <ext:JsonReader />
                                                </Reader>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Select OnEvent="ddlModels_Select" />
                                </DirectEvents>
                            </ext:ComboBox>
                        </Items>
                    </ext:Panel>
                    <ext:FieldSet ID="FieldSet1" runat="server" Title="Roles" ColumnWidth="1" Layout="AnchorLayout" DefaultAnchor="100%" Cls="PopupFormColumnPanelGrid">
                        <Items>
                            <ext:GridPanel ID="grdRoles" runat="server" Title="Selected Role Permissions" Icon="KeyAdd" Height="368">
                                <TopBar>
                                    <ext:Toolbar ID="Toolbar1" runat="server">
                                        <Items>
                                            <ext:Button ID="btnGrantAll" runat="server" Text="Grant All" Icon="Accept">
                                                <Listeners>
                                                    <Click Handler="grantaccess(#{grdRoles});" />
                                                </Listeners>
                                            </ext:Button>
                                            <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server" />
                                            <ext:Button ID="btnDenyAll" runat="server" Text="Deny All" Icon="Delete">
                                                <Listeners>
                                                    <Click Handler="denyaccess(#{grdRoles});" />
                                                </Listeners>
                                            </ext:Button>
                                            <ext:ToolbarFill />
                                            <ext:Button ID="btnClearAll" runat="server" Text="Clear All" Icon="Decline">
                                                <Listeners>
                                                    <Click Handler="clearaccess(#{grdRoles});" />
                                                </Listeners>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                                <Store>
                                    <ext:Store ID="storeRoles" runat="server">
                                        <Model>
                                            <ext:Model ID="modelRoles" runat="server" IDProperty="IdUser">
                                                <Fields>
                                                    <ext:ModelField Name="IdProfileRoles" Type="Int" />
                                                    <ext:ModelField Name="IdRole" Type="Int" />
                                                    <ext:ModelField Name="RoleName" Type="String" />
                                                    <ext:ModelField Name="DenyAccess" Type="Boolean" />
                                                    <ext:ModelField Name="GrantAccess" Type="Boolean" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel ID="cmRoles" runat="server">
                                    <Columns>
                                        <ext:Column Hidden="true" ID="IdProfileRoles" runat="server" Text="ID" DataIndex="IdProfileRoles">
                                        </ext:Column>
                                        <ext:Column Hidden="true" ID="IdRole" runat="server" Text="ID" DataIndex="IdRole">
                                        </ext:Column>
                                        <ext:Column ID="RoleName" runat="server" Text="Name" DataIndex="RoleName" Flex="1">
                                        </ext:Column>
                                        <ext:ComponentColumn ID="chkGrantAccess" runat="server" Editor="true" Align="Center" DataIndex="GrantAccess" Width="100" Text="Grant Access">
                                            <Component>
                                                <ext:Checkbox ID="chkGrantAccessItem" Checked="true" runat="server" />
                                            </Component>
                                        </ext:ComponentColumn>
                                        <ext:ComponentColumn ID="chkDenyAccess" runat="server" Editor="true" Align="Center" DataIndex="DenyAccess" Width="80" Text="Deny Access">
                                            <Component>
                                                <ext:Checkbox ID="chkDenyAccessItem" Checked="true" runat="server" />
                                            </Component>
                                        </ext:ComponentColumn>
                                    </Columns>
                                </ColumnModel>
                                <Features>
                                    <ext:GridFilters runat="server" ID="GridFilters2" Local="true">
                                        <Filters>
                                            <ext:StringFilter DataIndex="RoleName" />
                                            <ext:NumericFilter DataIndex="IdProfileRoles" />
                                        </Filters>
                                    </ext:GridFilters>
                                </Features>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" />
                                </SelectionModel>
                                <Plugins>
                                    <ext:CellEditing ID="CellEditing1" runat="server">
                                    </ext:CellEditing>
                                </Plugins>
                                <View>
                                    <ext:GridView MarkDirty="false" />
                                </View>
                            </ext:GridPanel>
                        </Items>
                    </ext:FieldSet>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="FormStatusBar" runat="server" />
                </BottomBar>
                <Buttons>
                    <ext:Button ID="btnSave" runat="server" Text="Save" Icon="DatabaseSave" Disabled="true" FormBind="true">
                        <DirectEvents>
                            <Click OnEvent="btnSave_Click">
                                <ExtraParams>
                                    <ext:Parameter Name="rowsValues" Value="#{grdRoles}.getRowsValues(false)" Mode="Raw" Encode="true" />
                                </ExtraParams>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="btnCancel" runat="server" Icon="Decline" Text="Close">
                        <DirectEvents>
                            <Click OnEvent="btnCancel_Click" />
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
                <Listeners>
                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                            });
                                            #{btnSave}.setDisabled(!valid);" />
                </Listeners>
            </ext:FormPanel>
        </Items>
    </ext:Window>

</asp:Content>
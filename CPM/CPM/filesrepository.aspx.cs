﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using System.Xml.Xsl;
using CD;
using DataClass;

namespace CPM
{
    public partial class _filesrepository : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdFileRepository { get { return (Int32)Session["IdFileRepository"]; } set { Session["IdFileRepository"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"]; } }//  set { Session["idLanguage"] = value; } } }
        public DataTable datViewData { get; set; }
        public static Object fileObj;
        protected static byte[] Filebyte;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                IdFileRepository = 0;
                this.LoadZiffInternalAnalysis();
                this.LoadLabel();
            }
        }
    
        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.IdFileRepository = 0;
                //this.ddlFileCategory.Value = null;
                this.txtFileName.Text = "";
                Filebyte = new Byte[0];
                this.winFilesRepositoryEdit.Show();                
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = ex.Message, IconCls = "icon-exclamation", Clear2 = false });
            }
        }
        
        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                Save();
                this.LoadZiffInternalAnalysis();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.StatusBar1.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            this.winFilesRepositoryEdit.Hide();
        }

        protected void btnModel_Click(object sender, DirectEventArgs e)
        {
            Response.Redirect("/caseselection.aspx");
        }

        protected void fileImport_Change(object sender, DirectEventArgs e)
        {
            if (this.fileImport.HasFile)
            {
                fileObj = fileImport;
                txtFileName.Text = fileImport.FileName;
            }
        }

        protected void grdFilesRepository_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            IdFileRepository = Convert.ToInt32(e.ExtraParams["Id"].ToString());
            switch (Command)
            {
                case "Edit":
                    fileObj = null;
                    this.EditFileRepositoryLoad();
                    this.winFilesRepositoryEdit.Show();

                    break;
                case "Delete":
                    X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + IdFileRepository + "?", new Ext.Net.MessageBoxButtonsConfig
                    {
                        Yes = new MessageBoxButtonConfig
                        {
                            Handler = "UsersX.ClickedDeleteYES()",
                            Text = modMain.strCommonYes
                        },
                        No = new MessageBoxButtonConfig
                        {
                            Text = modMain.strCommonNo
                        }
                    }).Show();
                    break;
                default:
                    Download();
                    break;
            }
        }

        protected void StoreFileCategory_ReadData(object sender, StoreReadDataEventArgs e)
        {
            //DataClass.clsFilesRepository objFilecategory = new DataClass.clsFilesRepository();
            //DataTable dtFileCategory = objFilecategory.LoadComboBox("IdLanguage = " + idLanguage, " ORDER BY Code");
            ////DataRow drAccount = dtFileCategory.NewRow();
            ////drAccount["IdZiffAccount"] = 0;
            ////drAccount["Name"] = "- Any -";
            ////dtFileCategory.Rows.Add(drAccount);
            ////dtFileCategory.AcceptChanges();
            //this.storeFileCategory.DataSource = dtFileCategory;
            //this.storeFileCategory.DataBind();
        }

        #endregion

        #region Methods

        protected void Save()
        {
            DataClass.clsFilesRepository objFileRepository = new DataClass.clsFilesRepository();
            objFileRepository.IdFileRepository = Convert.ToInt32(IdFileRepository);
            objFileRepository.loadObject();

            this.fileImport = (FileUploadField)fileObj;
            if (IdFileRepository == 0 || fileObj != null)
            {
                if (this.fileImport.HasFile)
                {
                    this.fileImport = (FileUploadField)fileObj;
                    BufferedStream reader = new BufferedStream(fileImport.PostedFile.InputStream);
                    Filebyte = new byte[fileImport.PostedFile.InputStream.Length];
                    reader.Read(Filebyte, 0, Filebyte.Length);
                }
                else if (Filebyte.Length == 0 || txtFileName.Text.Trim() == "")
                {
                    this.StatusBar1.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Upload file failed"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                Filebyte = objFileRepository.AttachFile;
            }
            objFileRepository.Comments = txtComments.Text;
            objFileRepository.IdModel = IdModel;
            objFileRepository.AttachFile = Filebyte;
            objFileRepository.FileName = txtFileName.Text;
            if (objFileRepository.IdFileRepository == 0)
            {
                objFileRepository.DateCreation = DateTime.Now;
                objFileRepository.UserCreation = (Int32)IdUserCreate;
                IdFileRepository = objFileRepository.Insert();
            }
            else
            {
                objFileRepository.DateModification = DateTime.Now;
                objFileRepository.UserModification = (Int32)IdUserCreate;
                objFileRepository.Update();
            }
            this.StatusBar1.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Record saved successfully!"), IconCls = "icon-accept", Clear2 = false });
            this.winFilesRepositoryEdit.Hide();
        }

        protected void EditFileRepositoryLoad()
        {
            DataClass.clsFilesRepository objFileRepository = new DataClass.clsFilesRepository();
            DataTable dt = objFileRepository.LoadList("IdModel = " + IdModel, "");
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["Comments"] == System.DBNull.Value)
                {
                    this.txtComments.Value = "";
                }
                else
                {
                    this.txtComments.Value = (String)dt.Rows[0]["Comments"];
                }
                this.txtFileName.Text = (String)dt.Rows[0]["FileName"];
                this.fileImport.Text = (String)dt.Rows[0]["FileName"];
                Filebyte = (Byte[])dt.Rows[0]["AttachFile"];

            }
        }

        public void DeleteInternal()
        {
            DataClass.clsFilesRepository objFileRepository = new DataClass.clsFilesRepository();

            objFileRepository.IdFileRepository = (Int32)IdFileRepository;
            objFileRepository.Delete();
        }

        [DirectMethod]
        public void ClickedDeleteYES()
        {
            this.DeleteInternal();
            this.LoadZiffInternalAnalysis();
        }

        protected void LoadZiffInternalAnalysis()
        {
            DataClass.clsFilesRepository objFilesRepository = new DataClass.clsFilesRepository();
            DataTable dt = objFilesRepository.LoadList("IdModel = " + IdModel,"");
            storeFilesRepository.DataSource = dt;
            storeFilesRepository.DataBind();
        }

        protected void Download()
        {
            DataClass.clsFilesRepository objFileRepository = new clsFilesRepository();
            objFileRepository.IdFileRepository = IdFileRepository;
            objFileRepository.loadObject();
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, objFileRepository.FileName, objFileRepository.AttachFile);
        }

        protected void LoadLabel()
        {
            this.pnlParameters1.Title = (String)GetGlobalResourceObject("CPM_Resources", "Files Repository");
            this.btnNew.Text = (String)GetGlobalResourceObject("CPM_Resources", "Add New File");
            this.btnNew.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "New Analysis");
            this.cFileName.Text = (String)GetGlobalResourceObject("CPM_Resources", "Filename");
            this.cDateCreation.Text = (String)GetGlobalResourceObject("CPM_Resources", "DateCreation");
            this.cComments.Text = (String)GetGlobalResourceObject("CPM_Resources", "Comments");
            this.cUserName.Text = (String)GetGlobalResourceObject("CPM_Resources", "User");
            this.ImageCommandColumn2.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn2.Commands[0].Text = "&nbsp;" + modMain.strCommonEdit;
            this.ImageCommandColumn2.Commands[1].Text = "&nbsp;" + modMain.strCommonDelete;
            this.ImageCommandColumn2.Commands[2].Text = "&nbsp;" + (String)GetGlobalResourceObject("CPM_Resources", "Download");
            this.winFilesRepositoryEdit.Title = (String)GetGlobalResourceObject("CPM_Resources", "Files Repository");
            //this.ddlFileCategory.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "ZiffAccount");
            this.txtFileName.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "NewFile");
            this.fileImport.ButtonText = (String)GetGlobalResourceObject("CPM_Resources", "AttachNewFile");
            this.txtComments.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Comments");
            this.btnSave.Text = modMain.strCommonSave;
            this.btnSave.ToolTip = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.btnCancel.ToolTip = modMain.strCommonClose;
        }

        #endregion

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using Ext.Net.Utilities;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Threading;
using CD;
using DataClass;

namespace CPM
{
    public partial class _defaultmaster : System.Web.UI.MasterPage
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUser { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public String NameUser { get { return (String)Session["NameUser"]; } set { Session["NameUser"] = value; } }
        public String Language { get { return (String)Session["Language"]; } set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  set { Session["idLanguage"] = value; } } 
        public DateTime LastLogin { get { return (DateTime)Session["LastLogin"]; }}// set { Session["LastLogin"] = value; } }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if ((Session["LastLogin"] == null || Session["IdUser"] == null) && Session["IdModel"] == null)
                {
                    X.Redirect("/account/login.aspx");
                }
                if (!X.IsAjaxRequest)
                {
                    this.LoadLanguage();
                }
                this.CreateMenu();
                this.LoadApplication();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                X.Redirect("/account/login.aspx");
            }
        }

        protected void btnHome_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/caseselection.aspx");
        }

        protected void btnHelp_Click(object sender, DirectEventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (this.idLanguage == 1) // English
            {
                objApplication.StreamFile(context, "cpm_en.pdf", "help");
            }
            else if (this.idLanguage == 2) // Spanish
            {
                objApplication.StreamFile(context, "cpm_es.pdf", "help");
            }
        }

        protected void btnAdmin_Click(object sender, DirectEventArgs e)
        {
            this.winUserEdit.Show();
            this.UserLoad();
        }

        protected void btnUser_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/users.aspx");
        }

        protected void btnSetting_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/settings.aspx");
        }

        protected void btnLogon_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/account/login.aspx");
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            if (ValidForm())
            {
                this.SaveUser();
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            winUserEdit.Hide();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        #endregion

        #region Methods

        protected void UserLoad()
        {
            DataClass.clsUsers objUsers = new DataClass.clsUsers();
            DataTable dt = objUsers.LoadList("IdUser = " + IdUser , "");
            if (dt.Rows.Count > 0)
            {
                this.txtUserName.Text =(String)dt.Rows[0]["UserName"];
                this.txtName.Text = (String)dt.Rows[0]["Name"];
                this.txtPassword.Text = CD.DAC.Decrypt((String)dt.Rows[0]["Password"]);
                this.txtEmail.Text = (String)dt.Rows[0]["Email"];
            }
        }

        protected void CreateMenu()
        {
            try
            {
                //btnHome.Text = modMain.strMasterHome;
                ////btnAdmin.ID = "btnAdmin";
                //btnHome.Icon = Icon.UserEdit;
                ////btnAdmin.DirectClick += btnAdmin_Click;
                //btnHome.ToolTip = modMain.strMasterHome;

                //Ext.Net.Button btnAdmin = new Ext.Net.Button();
                btnAdmin.Text = NameUser;
                //btnAdmin.ID = "btnAdmin";
                btnAdmin.Icon = Icon.UserEdit;
                //btnAdmin.DirectClick += btnAdmin_Click;
                btnAdmin.ToolTip = modMain.strMasterAdmin;
                //this.tbMenu.Add(btnAdmin);

                //Ext.Net.Button btnUser = new Ext.Net.Button();
                btnUser.Text = modMain.strMasterUser;
                //btnUser.ID = "btnUser";
                //btnUser.DirectClick += btnUser_Click;
                btnUser.Icon = Icon.Group;
                btnUser.ToolTip = modMain.strMasterUser;

                //this.tbMenu.Add(btnUser);

                //Ext.Net.Button btnAdmin = new Ext.Net.Button();
                btnMainUser.Text = NameUser;
                //btnAdmin.ID = "btnAdmin";
                btnMainUser.Icon = Icon.User;
                //btnAdmin.DirectClick += btnAdmin_Click;
                btnMainUser.ToolTip = modMain.strMasterAdmin;
                //this.tbMenu.Add(btnAdmin);

                //Ext.Net.Button btnSetting = new Ext.Net.Button();
                btnSetting.Text = modMain.strMasterSetting;
                //btnSetting.ID = "btnSetting";
                //btnSetting.DirectClick += btnSetting_Click;
                btnSetting.Icon = Icon.TableKey;
                btnSetting.ToolTip = modMain.strMasterSetting;

                //this.tbMenu.Add(btnSetting);

                //Ext.Net.Button btnLogon = new Ext.Net.Button();
                btnLogon.Text = modMain.strMasterLogon;
                //btnLogon.ID = "btnLogon";
                //btnLogon.DirectClick += btnLogon_Click;
                btnLogon.Icon = Icon.KeyDelete;
                btnLogon.ToolTip = modMain.strMasterLogon;

                if (!this.IsAdministrator())
                {
                    btnUser.Hidden = true;
                    btnSetting.Hidden = true;
                }
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
            }
        }

        protected void LoadLanguage()
        {
            DataClass.clsLabelsLanguages objLabelsLanguages = new DataClass.clsLabelsLanguages();
            DataTable dt = objLabelsLanguages.LoadList("LanguageName = '" + Language + "' AND FormName = 'MasterPage'", "ORDER BY IdLabel");
            foreach (DataRow row in dt.Rows)
            {
                idLanguage = (Int32)row["IdLanguage"];
                switch ((String)row["LabelName"])
                {
                    case "Title Form":
                        modMain.strMasterTitleForm = (String)row["LabelText"];
                        break;
                    case "Application Home":
                        modMain.strMasterHome = (String)row["LabelText"];
                        break;
                    case "Admin":
                        modMain.strMasterAdmin = (String)row["LabelText"];
                        break;
                    case "User":
                        modMain.strMasterUser = (String)row["LabelText"];
                        break;
                    case "Setting":
                        modMain.strMasterSetting = (String)row["LabelText"];
                        break;
                    case "Logon":
                        modMain.strMasterLogon = (String)row["LabelText"];
                        break;
                    case "LastLogin":
                        modMain.strMasterLastLogin = (String)row["LabelText"];
                        break;
                    case "Windows Title User":
                        modMain.strMasterWindowsTitleUser = (String)row["LabelText"];
                        break;
                    case "Form UserName":
                        modMain.strMasterFormUserName = (String)row["LabelText"];
                        break;
                    case "Form Name":
                        modMain.strMasterFormName = (String)row["LabelText"];
                        break;
                    case "Form Password":
                        modMain.strMasterFormPassword = (String)row["LabelText"];
                        break;
                    case "Form Email":
                        modMain.strMasterFormEmail = (String)row["LabelText"];
                        break;
                    case "Form Accept Button":
                        modMain.strMasterFormAcceptButton = (String)row["LabelText"];
                        break;
                    case "Form Cancel Button":
                        modMain.strMasterFormCancelButton = (String)row["LabelText"];
                        break;
                    case "Valid":
                        modMain.strMasterValid = (String)row["LabelText"];
                        break;
                }
            }
            this.LoadLabel();
        }

        protected void LoadLabel()
        {
            this.btnHelp.Text = modMain.strCommonHelp;
            //this.pnlUser.Title = modMain.strMasterWindowsTitleUser;
            this.lblLastLogin.Text = modMain.strMasterLastLogin + " " + LastLogin.ToString("yyyy-MM-dd H:mm:ss tt");
            this.winUserEdit.Title = modMain.strMasterWindowsTitleUser;
            
            this.txtUserName.EmptyText = modMain.strMasterFormUserName;
            this.txtUserName.FieldLabel = modMain.strMasterFormUserName;
            this.txtUserName.BlankText = modMain.strMasterValid;
            this.txtUserName.Reset();

            this.txtName.EmptyText = modMain.strMasterFormName;
            this.txtName.FieldLabel = modMain.strMasterFormName;
            this.txtName.BlankText = modMain.strMasterValid;
            this.txtName.Reset();

            this.txtPassword.EmptyText = modMain.strMasterFormPassword;
            this.txtPassword.FieldLabel = modMain.strMasterFormPassword;
            this.txtPassword.BlankText = modMain.strMasterValid;
            this.txtPassword.Reset();

            this.txtEmail.EmptyText = modMain.strMasterFormEmail;
            this.txtEmail.FieldLabel = modMain.strMasterFormEmail;
            this.txtEmail.BlankText = modMain.strMasterValid;
            this.txtEmail.Reset();

            this.btnSave.Text = modMain.strMasterFormAcceptButton;
            this.btnSave.Icon = Icon.DatabaseSave;
            this.btnCancel.Text = modMain.strMasterFormCancelButton;
            this.btnCancel.Icon = Icon.Decline;
        }

        protected void SaveUser()
        {
            DataClass.clsUsers objUsers = new DataClass.clsUsers();
            CD.DAC DAC = new CD.DAC();

            objUsers.IdUser = IdUser;
            objUsers.loadObject();

            if (IdUser == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (objUsers.UserName != txtUserName.Text)
                {
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }

            objUsers.UserName = this.txtUserName.Text;
            objUsers.Name = this.txtName.Text;
            objUsers.Password = CD.DAC.Encrypt(this.txtPassword.Text);
            objUsers.Email = this.txtEmail.Text;
            objUsers.DateModification = DateTime.Now;
            objUsers.UserModification = IdUser;
            objUsers.Update();
            NameUser = this.txtName.Text;
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winUserEdit.Hide();
        }

        #endregion

        #region Functions

        protected void LoadApplication()
        {
            this.lblVersion.Text = objApplication.AppVersion;
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsUsers objUsers = new DataClass.clsUsers();
            DataTable dt = objUsers.LoadList("UserName = '" + txtUserName.Text + "'", "");
            Value = dt.Rows.Count;

            return Value;
        }

        protected Boolean IsAdministrator()
        {
            Boolean Value = false;
            DataClass.clsProfiles objProfiles = new DataClass.clsProfiles();

            DataTable dt = objProfiles.LoadList("Type = 1 AND IdProfile = " + this.IdProfile(), "ORDER BY Name");

            if (dt.Rows.Count > 0)
            {
                Value = true;
            }
            return Value;
        }

        protected Int32 IdProfile()
        {
            Int32 Value = 0;
            DataClass.clsUsers objUsers = new DataClass.clsUsers();

            DataTable dt = objUsers.LoadList("IdUser = " + IdUser, "ORDER BY Name");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdProfile"];
            }
            return Value;
        }

        protected Boolean ValidForm()
        {
            Boolean Valid = true;
            if (this.txtUserName.Text == "")
            {
                Valid = false;
            }
            else if (this.txtName.Text == "")
            {
                Valid = false;
            }
            else if (this.txtPassword.Text == "")
            {
                Valid = false;
            }
            else if (this.txtEmail.Text == "")
            {
                Valid = false;
            }
            return Valid;
        }

        #endregion

    }
}
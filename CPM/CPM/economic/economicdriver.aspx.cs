﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _economicdriver : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdEconomicDriver { get { return (Int32)Session["IdEconomicDriver"]; } set { Session["IdEconomicDriver"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }
        private DataReportObject ExportEconomicDrivers { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                IdEconomicDriver = 0;
                this.LoadEconomicDrivers();
                LoadLabel();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winEconomicDriverEdit.Show();
        }

        protected void btnDeleteAll_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources","Are you sure you want to do delete all records?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDeleteAllYES()",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnEconomicDriverGroup_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/economic/economicdrivergroup.aspx");
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.Save();
                this.LoadEconomicDrivers();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            winEconomicDriverEdit.Hide();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void grdEconomicDriver_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            IdEconomicDriver = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            if (Command == "Edit")
            {
                this.winEconomicDriverEdit.Show();
                this.EditEconomicDriverLoad();
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + _Name + "?", new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedDeleteYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
        }

        protected void StoreFilterDriverGroup_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsDriverGroups clsDriverGroups = new DataClass.clsDriverGroups();
            DataTable dtDriverGroup = clsDriverGroups.LoadComboBox("IdModel = " + IdModel, "ORDER BY Name");

            DataRow drLevel = dtDriverGroup.NewRow();
            drLevel["ID"] = 0;
            drLevel["Name"] = "- All -";
            dtDriverGroup.Rows.Add(drLevel);
            dtDriverGroup.AcceptChanges();

            dtDriverGroup.DefaultView.Sort = "Name";
            DataTable dtSort = dtDriverGroup.DefaultView.ToTable();

            this.storeFilterDriverGroup.DataSource = dtSort;
            this.storeFilterDriverGroup.DataBind();
            this.ddlDriverGroupCond.Value = 0;
        }

        protected void StoreDriverGroup_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsDriverGroups objDriverGroups = new DataClass.clsDriverGroups();
            this.storeDriverGroup.DataSource = objDriverGroups.LoadComboBox("IdModel = " + IdModel, "ORDER BY Name");
            this.storeDriverGroup.DataBind();
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.EconomicDriver);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Driver Desc"].ToString(), row["Driver Group Name"].ToString());
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadEconomicDrivers();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void ddlDriverGroupCond_Select(object sender, DirectEventArgs e)
        {
            this.LoadEconomicDrivers();
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        protected void Clear()
        {
            this.IdEconomicDriver = 0;
            this.txtName.Text = "";
            this.txtName.Reset();
            this.ddlDriverGroup.Reset();
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void Save()
        {
            DataClass.clsEconomicDriver objEconomicDriver = new DataClass.clsEconomicDriver();
            objEconomicDriver.IdEconomicDriver = (Int32)IdEconomicDriver;
            objEconomicDriver.loadObject();            

            if (IdEconomicDriver == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (objEconomicDriver.Name.ToString().ToUpper() != txtName.Text.ToString().ToUpper())
                {
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }

            objEconomicDriver.Name = txtName.Text;
            objEconomicDriver.IdModel = IdModel;
            objEconomicDriver.IdDriverGroup = Convert.ToInt32(ddlDriverGroup.SelectedItem.Value);
            if (IdEconomicDriver == 0)
            {
                objEconomicDriver.DateCreation = DateTime.Now;
                objEconomicDriver.UserCreation = (Int32)IdUserCreate;
                IdEconomicDriver = objEconomicDriver.Insert();
            }
            else
            {
                objEconomicDriver.DateModification = DateTime.Now;
                objEconomicDriver.UserModification = (Int32)IdUserCreate;
                objEconomicDriver.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winEconomicDriverEdit.Hide();
            this.ModelUpdateTimestamp();
        }

        protected void SaveImport(String _Name, String _GroupName)
        {
            Int32 _IdDriverGroup = GetIdDriverGroup(_GroupName);

            if (!String.IsNullOrEmpty(_Name) && _IdDriverGroup != 0)
            {
                DataClass.clsEconomicDriver objEconomicDriver = new DataClass.clsEconomicDriver();
                objEconomicDriver.IdEconomicDriver = (Int32)IdEconomicDriver;
                objEconomicDriver.loadObject();
                objEconomicDriver.Name = _Name;
                objEconomicDriver.IdModel = IdModel;
                objEconomicDriver.DateCreation = DateTime.Now;
                objEconomicDriver.UserCreation = (Int32)IdUserCreate;
                objEconomicDriver.IdDriverGroup = _IdDriverGroup;
                IdEconomicDriver = objEconomicDriver.Insert();
                objEconomicDriver = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected Int32 GetIdDriverGroup(String _Name)
        {
            Int32 Value = 0;
            DataClass.clsDriverGroups clsDriverGroups = new DataClass.clsDriverGroups();
            DataTable dt = clsDriverGroups.LoadList("Name = '" + _Name + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdDriverGroup"];
            }
            return Value;
        }

        protected void EditEconomicDriverLoad()
        {
            DataClass.clsEconomicDriver objEconomicDriver = new DataClass.clsEconomicDriver();
            DataTable dt = objEconomicDriver.LoadList("IdEconomicDriver = " + IdEconomicDriver, "");
            if (dt.Rows.Count > 0)
            {
                
                this.txtName.Text = (String)dt.Rows[0]["Name"];
                this.ddlDriverGroup.SetValue((Int32)dt.Rows[0]["IdDriverGroup"]);
            }
        }

        public void Delete()
        {
            DataClass.clsEconomicDriver objEconomicDriver = new DataClass.clsEconomicDriver();
            objEconomicDriver.IdEconomicDriver = (Int32)IdEconomicDriver;
            objEconomicDriver.Delete();
            this.ModelUpdateTimestamp();
        }

        public void DeleteAll()
        {
            DataClass.clsEconomicDriver objEconomicDriver = new DataClass.clsEconomicDriver();
            objEconomicDriver.IdModel = (Int32)IdModel;
            objEconomicDriver.DeleteAll();
            this.ModelUpdateTimestamp();
        }

        [DirectMethod]
        public void ClickedDeleteYES()
        {
            this.Delete();
            this.LoadEconomicDrivers();
        }

        [DirectMethod]
        public void ClickedDeleteAllYES()
        {
            this.DeleteAll();
            this.LoadEconomicDrivers();
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsEconomicDriver objEconomicDriver = new DataClass.clsEconomicDriver();
            DataTable dt = objEconomicDriver.LoadList("IdModel = " + IdModel + " AND Name = '" + txtName.Text + "'", "");
            Value = dt.Rows.Count;

            return Value;
        }

        protected void LoadEconomicDrivers()
        {
            String cond = "";
            DataClass.clsEconomicDriver objEconomicDriver = new DataClass.clsEconomicDriver();

            if (ddlDriverGroupCond.SelectedItem.Value != null && ddlDriverGroupCond.SelectedItem.Text != null )
            {
                if (ddlDriverGroupCond.SelectedItem.Value != "0")
                {
                    cond = cond + "IdDriverGroup = " + Convert.ToInt32(ddlDriverGroupCond.SelectedItem.Value);
                }
            }
            if (cond != "")
            {
                cond = " And " + cond;
            }
            DataTable dt = objEconomicDriver.LoadList("IdModel = " + IdModel + cond, " ORDER BY NameDriverGroup, Name");
            storeEconomicDriver.DataSource = dt;
            storeEconomicDriver.DataBind();
            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Name", "Driver Desc"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("NameDriverGroup", "Driver Group Name"));
            String strSelectedParameters = "";
            String[] columnsToCopy = { "Name", "NameDriverGroup" };
            System.Data.DataView view = new System.Data.DataView(dt);
            DataTable dtEconomicDrivers = view.ToTable(true, columnsToCopy);

            ExportEconomicDrivers = new DataReportObject(dtEconomicDrivers, "DATASHEET", null, strSelectedParameters, null, "NameDriverGroup, Name", datColumnTD, null);
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportEconomicDrivers;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=4.1 ExportListOfEconomicDrivers.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportListOfEconomicDrivers.xls", "template");
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = modMain.strEconomicModuleEconomicDrivers;
            this.lbDriverGroup.Text = modMain.strEconomicModuleEconomicDriverGroup;
            this.btnEconomicDriverGroups.Text = modMain.strEconomicModuleManageEconomicDriverGroups;
            this.btnAdd.Text = (String)GetGlobalResourceObject("CPM_Resources", "Add");
            this.btnAdd.ToolTip = modMain.strEconomicModuleAddNewEconomicDriver;
            this.btnDelete.Text = modMain.strCommonDeleteAll;
            this.btnDelete.ToolTip = modMain.strCommonDeleteAll;
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.Name.Text = modMain.strCommonName;
            this.NameDriverGroup.Text = modMain.strEconomicModuleEconomicDriverGroup;
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = modMain.strCommonEdit;
            this.ImageCommandColumn1.Commands[1].Text = modMain.strCommonDelete;
            this.winEconomicDriverEdit.Title = modMain.strEconomicModuleEconomicDriver;
            this.txtName.FieldLabel = modMain.strCommonName;
            this.ddlDriverGroup.FieldLabel = modMain.strEconomicModuleDriverGroup;
            this.btnSave.Text = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.winImport.Title = modMain.strEconomicModuleImportEconomicDrivers;
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        #endregion

    }
}
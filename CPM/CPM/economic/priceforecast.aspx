﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="priceforecast.aspx.cs" Inherits="CPM._priceforecast" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" 
                runat="server" 
                Title="Base Cost Rates Forecast" 
                IconCls="icon-pricesforecast_16" 
                AutoScroll="true" 
                Border="false"
                Resizable="false" 
                Layout="BorderLayout">
                <Items>
                    <%-- Economic Scenarios drop down list --%>
                    <ext:Panel ID="Panel1" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0" Region="North">
                        <Items>
                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lbStage" runat="server" Text="Economic Scenarios:"></ext:Label>
                            <ext:Combobox Style="margin: 10px 10px 10px 10px" ID="ddlStage" runat="server" Width="200" DisplayField="Name" ValueField="ID">
                                <ListConfig LoadingText="Searching..." MinWidth="300">
                                </ListConfig>
                                <Store>
                                    <ext:Store ID="storeStage" runat="server" OnReadData="StoreStage_ReadData">
                                        <Model>
                                            <ext:Model ID="Model6" runat="server" IDProperty="Name">
                                                <Fields>
                                                    <ext:ModelField Name="Name" />
                                                    <ext:ModelField Name="ID" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                                <Reader>
                                                    <ext:JsonReader  />
                                                </Reader>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Select OnEvent="ddlStage_Select">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>
                            </ext:Combobox>
                        </Items>
                    </ext:Panel>
                    <%-- Data Grid --%>
                    <ext:GridPanel ID="grdPriceForecast" 
                        runat="server" 
                        Border="false"
                        Layout="AutoLayout"
                        Resizable="false"
                        StripeRows="true"   
                        Style="width: 100%" 
                        EnableLocking="true"
                        Header="true"
                        Region="Center">
                        <Store>
                            <ext:Store ID="storePriceForecast" runat="server" GroupField="RootZiffAccount">
                                <Model>
                                    <ext:Model ID="modelPriceForecast" runat="server">
                                        <Fields>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel RenderColumnsOnly="False" RenderXType="True" IDMode="Explicit" Namespace="App" IsDynamic="False">
                            <Columns>
                                <ext:Column ID="DisplayCode" runat="server" Text="Code"/>
                                <ext:Column ID="ZiffAccount" runat="server" Text="Name"/>
                                <ext:Column ID="Years" runat="server" Text="Years"/>
                                <ext:Column ID="Filler" runat="server" Text=" " Flex="1"/>
                            </Columns>
                        </ColumnModel>
                        <Features>
                            <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                                <Filters>
                                    <ext:StringFilter DataIndex="ZiffAccount" />
                                </Filters>
                            </ext:GridFilters>
                            <ext:GroupingSummary ID="RootZiffAccount" runat="server" GroupHeaderTplString="{name}" HideGroupedHeader="true" EnableGroupingMenu="false" />
                        </Features>
                        <Listeners>
                            <Reconfigure Handler="this.setHeight(this.container.getHeight()+20);" Delay="100" />
                        </Listeners>
                    </ext:GridPanel>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="FormStatusBar" runat="server" />
                </BottomBar>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
    
</body>
</html>
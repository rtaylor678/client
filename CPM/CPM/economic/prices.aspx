﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="prices.aspx.cs" Inherits="CPM._prices" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var formatZero = function (value) {
            if (value == 0) {
                return "- N/A -";
            }
            else if (value == 9999) {
                return "";
            }
            return value;
        }

        var beforeCellEditHandler = function (e) {
            if (e.originalValue == "9999") {
                App.CellEditing1.cancelEdit(); 
                return false;
            }
        }

        var cellEditingHandler = function (e) {
            if (e.value < 1 || e.value > 100) {
                Ext.Msg.alert('Error!!!', 'Value must be between 1 and 100. (El valor debe estar entre 1 y 100.)');
                e.record.data[e.field] = e.originalValue;
                e.cancel = true;
            }
        }

        function applyPageSize(grid) {
            var headerHeight = 22;
            var rowHeight = 21;

            var gridTopOffset = 100;
            var gridBottomOffset = 50;
            var rowHeight = 20;
            var myHeight = window.innerHeight;

            grid.store.pageSize = Math.round(((myHeight - gridTopOffset - gridBottomOffset) / rowHeight)-3); // - 3);
            grid.store.load();
        }
    </script>
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" 
                runat="server" 
                Title="Prices" 
                IconCls="icon-currency_16" 
                AutoScroll="true" 
                Border="false"
                Resizable="false" 
                Layout="BorderLayout">
                <Items>
                    <%-- Field, Currency, Product and Scenario drop down lists --%>
                    <ext:Panel ID="pnlButtons01" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0" Region="North">
                        <Items>
                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lblFilterField" runat="server" Text="Field:"></ext:Label>
                            <ext:ComboBox Style="margin: 10px 10px 10px 10px" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" QueryMode="Remote" ID="ddlField" runat="server" DisplayField="Name" ValueField="IdField" Width="200">
                                <ListConfig LoadingText="Searching..." MinWidth="300">
                                    <ItemTpl ID="ItemTpl3" runat="server">
                                        <Html>
                                            <div class="search-item">
							                    <h3>{Name}</h3>
							                    <span>Code: {Code}</span>
						                    </div>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Store>
                                    <ext:Store ID="storeFields" runat="server" OnReadData="storeFields_ReadData">
                                        <Model>
                                            <ext:Model ID="Model3" runat="server" IDProperty="IdField">
                                                <Fields>
                                                    <ext:ModelField Name="IdField" />
                                                    <ext:ModelField Name="Code" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                                <Reader>
                                                    <ext:JsonReader />
                                                </Reader>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Select OnEvent="ddlField_Select">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>
                            </ext:ComboBox>
                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lblCurrency" runat="server" Text="Currency:"></ext:Label>
                            <ext:ComboBox Style="margin: 10px 10px 10px 10px" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" QueryMode="Remote" ID="ddlCurrency" runat="server" DisplayField="Name" ValueField="IdCurrency" Width="150">
                                <ListConfig LoadingText="Searching..." MinWidth="250">
                                    <ItemTpl ID="ItemTpl4" runat="server">
                                        <Html>
                                            <div class="search-item">
							                    <h3>{Name}</h3>
							                    <span>Code: {Code}</span>
						                    </div>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Store>
                                    <ext:Store ID="storeCurrency" runat="server" OnReadData="StoreCurr_ReadData">
                                        <Model>
                                            <ext:Model ID="Model1" runat="server" IDProperty="IdCurrency">
                                                <Fields>
                                                    <ext:ModelField Name="IdCurrency" />
                                                    <ext:ModelField Name="Name" />
                                                    <ext:ModelField Name="Code" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                                <Reader>
                                                    <ext:JsonReader />
                                                </Reader>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Select OnEvent="ddlCurrency_Select">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>
                            </ext:ComboBox>
                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lblProduct" runat="server" Text="Product:"></ext:Label>
                            <ext:ComboBox Style="margin: 10px 10px 10px 10px" ID="ddlProduct" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" ValueField="IdTechnicalDriver">
                                <ListConfig LoadingText="Searching..." MinWidth="250">
                                    <ItemTpl ID="ItemTpl5" runat="server">
                                        <Html>
                                            <div class="search-item">
							                    <h3>{Name}</h3>
							                    <span>Code: {IdTechnicalDriver}</span>
						                    </div>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Store>
                                    <ext:Store ID="storeProduct" runat="server" OnReadData="StoreProduct_ReadData">
                                        <Model>
                                            <ext:Model ID="Model6" runat="server" IDProperty="IdTechnicalDriver">
                                                <Fields>
                                                    <ext:ModelField Name="IdTechnicalDriver" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                                <Reader>
                                                    <ext:JsonReader />
                                                </Reader>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Select OnEvent="ddlProduct_Select">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>
                            </ext:ComboBox>
                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lblScenario" runat="server" Text="Scenario:"></ext:Label>
                            <ext:ComboBox Style="margin: 10px 10px 10px 10px" ID="ddlScenario" runat="server" DisplayField="ScenarioName" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" ValueField="IdPriceScenario" Width="150">
                                <ListConfig LoadingText="Searching..." MinWidth="250">
                                    <ItemTpl ID="ItemTpl2" runat="server">
                                        <Html>
                                            <div class="search-item">
							                    <h3>{ScenarioName}</h3>
							                    <span>Code: {IdPriceScenario}</span>
						                    </div>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Store>
                                    <ext:Store ID="storeScenario" runat="server" OnReadData="StoreScenario_ReadData">
                                        <Model>
                                            <ext:Model ID="Model5" runat="server" IDProperty="IdPriceScenario">
                                                <Fields>
                                                    <ext:ModelField Name="IdPriceScenario" />
                                                    <ext:ModelField Name="ScenarioName" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                                <Reader>
                                                    <ext:JsonReader />
                                                </Reader>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Select OnEvent="ddlScenario_Select">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>
                            </ext:ComboBox>
                        </Items>
                    </ext:Panel>                            
                    <%-- Data Grid --%>
                    <ext:GridPanel ID="sgrGrid" runat="server" Border="false" Style="width: 100%" Region="Center" EnableLocking="true">
                        <TopBar>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btnSave" Icon="DatabaseSave" ToolTip="Save Changes" runat="server" Text="Save Changes">
                                        <DirectEvents>
                                            <Click OnEvent="btnSave_Click">
                                                <ExtraParams>
                                                    <ext:Parameter Name="rowsValues" Value="#{sgrGrid}.getRowsValues(false)" Mode="Raw" Encode="true" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarSeparator />
                                    <ext:Button ID="btnImport" Icon="BookAdd" runat="server" Text="Import">
                                        <DirectEvents>
                                            <Click OnEvent="btnImport_Click">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarFill />
                                    <ext:Button ID="btnSaveExcel" runat="server" Text="Export Data To Excel" Icon="PageExcel">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveExcel_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnDownloadTemplate" runat="server" Text="Download Import Template" Icon="PageSave">
                                        <DirectEvents>
                                            <Click OnEvent="btnDownloadTemplate_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="storePrices" runat="server">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="IdModelPrice" Type="Int" />
                                            <ext:ModelField Name="IdModel" Type="Int" />
                                            <ext:ModelField Name="IdCurrency" Type="Int" />
                                            <ext:ModelField Name="CurrencyCode" Type="String" />
                                            <ext:ModelField Name="IdField" Type="Int" />
                                            <ext:ModelField Name="FieldCode" Type="String" />
                                            <ext:ModelField Name="PriceYear" Type="Int" />
                                            <ext:ModelField Name="IdTechnicalDriver" Type="Int" />
                                            <ext:ModelField Name="Product" Type="String" />
                                            <ext:ModelField Name="IdPriceScenario" Type="Int" />
                                            <ext:ModelField Name="PriceValue" Type="Float" />
                                            <ext:ModelField Name="PriceOffset" Type="Int" />
                                            <ext:ModelField Name="NetPrice" Type="Float" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                                <Sorters>
                                    <ext:DataSorter Property="PriceYear" Direction="ASC" />
                                </Sorters>
                          </ext:Store>
                        </Store>
                        <ColumnModel RenderColumnsOnly="False" RenderXType="True" IDMode="Explicit" Namespace="App" IsDynamic="False">
                            <Columns>
                                <ext:Column Hidden="true" ID="colIdModelPrice" runat="server" Text="" DataIndex="IdModelPrice" Width ="25" Sortable="false" />
                                <ext:Column Hidden="true" ID="colIdModel" runat="server" Text="" DataIndex="IdModel" Style="width: 25" Sortable="false" />
                                <ext:Column Hidden="true" ID="colIdCurrency" runat="server" Text="" DataIndex="IdCurrency" Style="width: 25" Sortable="false" />
                                <ext:Column Hidden="true" ID="colIdField" runat="server" Text="" DataIndex="IdField" Style="width: 25" Sortable="false" />
                                <ext:Column Hidden="true" ID="colIdTechnicalDriver" runat="server" Text="" DataIndex="IdTechnicalDriver" Style="width: 25" Sortable="false" />
                                <ext:Column Hidden="true" ID="colIdPriceScenario" runat="server" Text="" DataIndex="IdPriceScenario" Style="width: 25" Sortable="false" />
                                <ext:Column ID="colPriceYear" runat="server" Text="colPriceYear" DataIndex="PriceYear" Flex="1" Style="width: 25%" Sortable="true" />
                                <ext:Column ID="colPriceValue" runat="server" Text="colPriceValue" DataIndex="PriceValue" Flex="1" Style="width: 25%" Sortable="false" />
                                <ext:Column ID="colPriceOffset" runat="server" Text="colPriceOffset" DataIndex="PriceOffset" Flex="1" Style="width: 25%" Sortable="false" />
                                <ext:Column ID="colNetPrice" runat="server" Text="colNetPrice" DataIndex="NetPrice" Flex="1" Style="width: 25%" Sortable="false" />
                            </Columns>
                        </ColumnModel>
                        <View>
                            <ext:GridView ID="GridView1" runat="server">
                                <GetRowClass Handler="return 'x-grid-row-expanded';" />
                            </ext:GridView>
                        </View>
                        <Plugins>
                            <ext:CellEditing ID="CellEditing1" runat="server">
                                <Listeners>
                                    <BeforeEdit Handler="return beforeCellEditHandler(e)"></BeforeEdit>
                                    <ValidateEdit Handler="cellEditingHandler(e);"></ValidateEdit>
                                </Listeners>
                            </ext:CellEditing>
                        </Plugins>
                        <Features>
                            <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                                <Filters>
                                    <ext:StringFilter DataIndex="PriceYear" />
                                </Filters>
                            </ext:GridFilters>
                        </Features>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" />
                        </SelectionModel>
                        <BottomBar>
                            <ext:StatusBar ID="FormStatusBar" runat="server" />
<%-- 
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="true" EmptyMsg="No records to display">
                            </ext:PagingToolbar>
--%>
                        </BottomBar>
                        <Listeners>
                            <Resize Handler="applyPageSize(this)" Buffer="250" />            
                        </Listeners>
                    </ext:GridPanel>
                    <%-- Import Window --%>
                    <ext:Window ID="winImport" runat="server" Title="Import Prices" Hidden="true" Icon="ArrowLeft" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="frmImportFile" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                                <Items>
                                    <ext:Panel ID="pnlImportFile1" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:FileUploadField ID="fileImport" FieldLabel="File" runat="server" Icon="Attach" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnImportFile" runat="server" Text="Import" Icon="TableRefresh" Disabled="true" FormBind="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnImportFile_Click" >
                                                <EventMask ShowMask="true" Target="CustomTarget" CustomTarget="vpMain" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnCancelImport" runat="server" Icon="Decline" Text="Close">
                                        <DirectEvents>
                                            <Click OnEvent="btnCancelImport_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                                <BottomBar>
                                    <ext:StatusBar ID="FormImportStatusBar" runat="server" />
                                </BottomBar>
                                <Listeners>
                                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                            });
                                                            #{btnImportFile}.setDisabled(!valid);" />
                                </Listeners>
                            </ext:FormPanel>
                        </Items>
                    </ext:Window>
                </Items>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>
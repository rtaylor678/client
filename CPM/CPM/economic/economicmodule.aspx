﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="economicmodule.aspx.cs" Inherits="CPM._economicmodule" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>


<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_iconxl.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Border="false" Layout="FormLayout" OverflowY="Scroll">
        <Items>
            <ext:Panel ID="pnlTableLayout" runat="server" Region="Center" Title="Economic Module" Border="false" BodyPadding="10" IconCls="icon-economic_16" Layout="AutoLayout">
                <Items>
                    <ext:FieldSet ID="FieldSet3" runat="server" Flex="1" Layout="AnchorLayout" Cls="buttongroup-border" ColumnWidth="1">
                        <Defaults>
                            <ext:Parameter Name="HideEmptyLabel" Value="false" Mode="Raw" />
                        </Defaults>
                        <Items>
                            <ext:Button ID="btnPriceScenarios" runat="server" Disabled="true" Cls="icon-currency_48" Text="Price Scenarios" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnPriceScenarios_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnPrices" runat="server" Disabled="true" Cls="icon-currency_48" Text="Prices" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnPrices_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnEconomicDrivers" runat="server" Disabled="true" Cls="icon-economicdrivers_48" Text="List of Economic Drivers" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnEconomicDrivers_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnEconomicDriverData" runat="server" Disabled="true" Cls="icon-economicdriversdata_48" Text="Economic Drivers Data" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnEconomicDriverData_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnPricesEconomic" runat="server" Disabled="true" Cls="icon-economicassumptions_48" Text="Economic Assumptions" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnPricesEconomic_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnPriceForecast" runat="server" Disabled="true" Cls="icon-pricesforecast_48" Text="Base Cost Rates Forecast" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnPriceForecast_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnZiffEconomicAnalysis" runat="server" Disabled="true" Cls="icon-ziffeconomicanalysis_48" Text="Economic Files Repository" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnZiffEconomicAnalysis_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:FieldSet>
                </Items>
            </ext:Panel>
        </Items>
    </ext:Viewport>
</body>
</html>
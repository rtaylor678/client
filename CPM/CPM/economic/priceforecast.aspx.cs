﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;
using System.ComponentModel;
using System.Web.Script.Serialization;

namespace CPM
{
    public partial class _priceforecast : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdPriceForecast { get { return (Int32)Session["IdPriceForecast"]; } set { Session["IdPriceForecast"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                IdPriceForecast = 0;
                this.ModelCheckRecalcModuleEconomic();
                LoadLabel();
            }
        }

        #endregion

        #region Methods

        protected void ddlStage_Select(object sender, DirectEventArgs e)
        {
            this.LoadPriceForecast();
        }

        protected void StoreStage_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsStage objStage = new DataClass.clsStage();
            DataTable dt = objStage.LoadComboBox("", "ORDER BY Name");
            foreach (DataRow row in dt.Rows)
            {
                if (idLanguage == 2)
                {
                    row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
                }
            }
            this.storeStage.DataSource = dt;
            this.storeStage.DataBind();
        }

        protected void LoadPriceForecast()
        {
            this.ModelCheckRecalcModuleEconomic();
            if (ddlStage.SelectedItem.Value == null || ddlStage.SelectedItem.Text == null)
            {
                storePriceForecast.RemoveAll();
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = "Select Stage please", IconCls = "icon-exclamation", Clear2 = false });
                return;
            }

            storePriceForecast.RemoveFields();
            this.grdPriceForecast.ColumnModel.Columns.Clear();            

            DataClass.clsPriceForecast objPriceForecast = new DataClass.clsPriceForecast();
            DataTable dt = objPriceForecast.LoadReport(IdModel, Convert.ToInt32(ddlStage.SelectedItem.Value));
            DataTable dtRows = objPriceForecast.LoadRows(IdModel);
            DataTable dtValues = new DataTable("ValueTable");

            Column colCode = new Column();
            colCode.Text = (String)GetGlobalResourceObject("CPM_Resources", "Code");
            colCode.DataIndex = "DisplayCode";
            colCode.MinWidth = 200;
            colCode.Locked = true;
            Column colName = new Column();
            colName.Text = (String)GetGlobalResourceObject("CPM_Resources", "Name");
            colName.DataIndex = "ZiffAccount";
            colName.MinWidth = 250;
            colName.Locked = true;
            this.grdPriceForecast.ColumnModel.Columns.Add(colCode);
            this.grdPriceForecast.ColumnModel.Columns.Add(colName);
            storePriceForecast.AddField(new ModelField("DisplayCode", ModelFieldType.Auto));
            storePriceForecast.AddField(new ModelField("ZiffAccount", ModelFieldType.Auto));
            storePriceForecast.AddField(new ModelField("RootZiffAccount", ModelFieldType.Auto));
            dtValues.Columns.Add("DisplayCode", typeof(String));
            dtValues.Columns.Add("ZiffAccount", typeof(String));
            dtValues.Columns.Add("RootZiffAccount", typeof(String));

            DataClass.clsModels objModels = new DataClass.clsModels();
            objModels.IdModel = this.IdModel;
            objModels.loadObject();
            Int32 intMinYear = objModels.BaseYear;
            Int32 intMaxYear = objModels.BaseYear + objModels.Projection;
            objModels = null;

            while (intMinYear <= intMaxYear)
            {
                NumberColumn colYear = new NumberColumn();
                colYear.Text = intMinYear.ToString();
                colYear.DataIndex = intMinYear.ToString();
                colYear.Width = 70;
                colYear.Format = "0.0 %";
                colYear.Align = Alignment.Center;
                this.grdPriceForecast.ColumnModel.Columns.Add(colYear);
                storePriceForecast.AddField(new ModelField(intMinYear.ToString(), ModelFieldType.Float));
                dtValues.Columns.Add(intMinYear.ToString(), typeof(Double));
                intMinYear = intMinYear + 1;
            }

            foreach (DataRow drRows in dtRows.Rows)
            {
                DataRow drReport = dtValues.NewRow();

                drReport["DisplayCode"] = drRows["DisplayCode"].ToString();
                drReport["ZiffAccount"] = drRows["ZiffAccount"].ToString();
                drReport["RootZiffAccount"] = drRows["RootZiffAccount"].ToString();

                DataRow[] FilterRows;
                FilterRows = dt.Select("DisplayCode = '" + drRows["DisplayCode"].ToString() + "' AND ZiffAccount = '" + drRows["ZiffAccount"].ToString() + "'");

                foreach (DataRow drValues in FilterRows)
                {
                    drReport[drValues["Year"].ToString()] = Convert.ToDouble(drValues["Forecast"]) * 100;
                }
                dtValues.Rows.Add(drReport);
                dtValues.AcceptChanges();
            }

            storePriceForecast.DataSource = dtValues;
            this.grdPriceForecast.Reconfigure();
            storePriceForecast.DataBind();
        }

        public void ModelCheckRecalcModuleEconomic()
        {
            clsModels objModelRecalc = new clsModels();
            objModelRecalc.IdModel = IdModel;
            objModelRecalc.CheckRecalcModuleParameter(objApplication.MaxRelationLevels);
            objModelRecalc.CheckRecalcModuleAllocation();
            objModelRecalc.CheckRecalcModuleEconomic();
            objModelRecalc = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = modMain.strMasterBaseCostRatesForecast;
            this.lbStage.Text = modMain.strEconomicModuleEconomicScenarios;
            this.DisplayCode.Text = modMain.strCommonCode;
            this.ZiffAccount.Text = modMain.strCommonName;
            this.Years.Text = (String)GetGlobalResourceObject("CPM_Resources", "Years");
        }
        #endregion

    } 
}
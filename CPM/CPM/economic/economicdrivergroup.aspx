﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="economicdrivergroup.aspx.cs" Inherits="CPM._economicdrivergroup" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody"  AutoScroll="true" runat="server" Title="Economic Driver Group" IconCls="icon-economicdrivers_16" Border="false">
                <Items>
                <ext:GridPanel ID="grdDriverGroup" runat="server" Border="false" Header="false">
                    <TopBar>
                        <ext:Toolbar ID="Toolbar1" runat="server">
                            <Items>
                                <ext:Button  ID="btnAdd" Icon="Add" ToolTip="Add New Economic Driver Group" runat="server" Text="Add New Economic Driver Group">
                                    <DirectEvents>
                                        <Click OnEvent="btnAdd_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>
                                <ext:ToolbarSeparator />
                                    <ext:Button Style="margin: 0 0 0 10px" ID="btnEconomicDriver" TextAlign="Center" IconCls="icon-economicdrivers_16" Text="List of Economic Drivers" runat="server">
                                        <DirectEvents>
                                            <Click OnEvent="btnEconomicDriver_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                            </Items>
                        </ext:Toolbar>
                    </TopBar>
                    <Store>
                        <ext:Store ID="storeDriverGroup" runat="server">
                            <Model>
                                <ext:Model ID="Model1" runat="server" IDProperty="IdDriverGroup">
                                    <Fields>
                                        <ext:ModelField Name="IdDriverGroup" Type="Int" />
                                        <ext:ModelField Name="Name" Type="String" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            <ext:Column ID="colID" Hidden="true" runat="server" Text="ID" DataIndex="IdDriverGroup"></ext:Column>
                            <ext:Column ID="Name" runat="server" Text="Name" DataIndex="Name" Flex="1">
                            </ext:Column>
                            <ext:ImageCommandColumn Align="Center" Width="150" Text="Functions" ID="ImageCommandColumn1" runat="server">
                                <Commands>
                                    <ext:ImageCommand CommandName="Edit" IconCls="icon-edit_16" Text="&nbsp;Edit" />
                                </Commands> 
                                    <Commands>
                                    <ext:ImageCommand CommandName="Delete" IconCls="icon-delete_16" Text="&nbsp;Delete" />
                                </Commands> 
                                <DirectEvents>                                       
                                    <Command OnEvent="grdDriverGroup_Command">
                                        <ExtraParams>  
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>  
                                                <ext:Parameter Name="Id" Value="record.data.IdDriverGroup" Mode="Raw"></ext:Parameter> 
                                                <ext:Parameter Name="Name" Value="record.data.Name" Mode="Raw"></ext:Parameter> 
                                            </ExtraParams> 
                                    </Command>
                                </DirectEvents>
                            </ext:ImageCommandColumn>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" />
                    </SelectionModel>
                    <Features>
                        <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="Name" />
                            </Filters>
                        </ext:GridFilters>
                    </Features>
        
                </ext:GridPanel>
                </Items>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    <ext:Window ID="winDriverGroupEdit" runat="server" Title="Economic Driver Group" Hidden="true" IconCls="icon-economicdrivers_16" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
        <Items>
            <ext:FormPanel ID="pnlEditDriverGroup" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
            <Items>
                <ext:Panel ID="Panel3" runat="server" Border="false" Layout="Form" ColumnWidth="1" LabelAlign="Top" Cls="PopupFormColumnPanel">
                    <Defaults>
                        <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                        <ext:Parameter Name="MsgTarget" Value="side" />
                    </Defaults>
                    <Items>
                        <ext:TextField ID="txtName" runat="server" FieldLabel="Name" AnchorHorizontal="92%" Cls="PopupFormField" />
                    </Items>
                </ext:Panel>
            </Items>
            <Buttons>
                <ext:Button ID="btnSave" runat="server" Text="Save" icon="DatabaseSave" Disabled="true" FormBind="true">
                    <DirectEvents>
                        <Click OnEvent="btnSave_Click" />
                    </DirectEvents>
                </ext:Button>
                <ext:Button ID="btnCancel" runat="server" icon="Decline" Text="Close">
                    <DirectEvents>
                        <Click OnEvent="btnCancel_Click" />
                    </DirectEvents>
                </ext:Button>
            </Buttons>
            <BottomBar>
                <ext:StatusBar ID="FormStatusBar" runat="server" />
            </BottomBar>
            <Listeners>
                <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                            });
                                            #{btnSave}.setDisabled(!valid);" />
            </Listeners>
        </ext:FormPanel>
        </Items>
    </ext:Window>
</body>
</html>
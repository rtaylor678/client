﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _economicdrivergroup : System.Web.UI.Page
    {

        #region Definitions

        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdDriverGroup { get { return (Int32)Session["IdTechnicalDriver"]; } set { Session["IdTechnicalDriver"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                IdDriverGroup = 0;
                this.LoadDriverGroups();
                LoadLabel();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winDriverGroupEdit.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.Save();
                this.LoadDriverGroups();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            winDriverGroupEdit.Hide();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnEconomicDriver_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/economic/economicdriver.aspx");
        }

        #endregion

        #region Methods

        protected void Clear()
        {
            this.IdDriverGroup = 0;
            this.txtName.Text = "";
            this.txtName.Reset();
        }

        protected void grdDriverGroup_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            IdDriverGroup = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            if (Command == "Edit")
            {
                this.winDriverGroupEdit.Show();
                this.EditDriverGroupLoad();
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources","Are you sure you want to do delete the record:") + " " + _Name + "?", new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedDeleteYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }      
        }

        protected void Save()
        {
            DataClass.clsDriverGroups clsDriverGroups = new DataClass.clsDriverGroups();
            clsDriverGroups.IdDriverGroup = (Int32)IdDriverGroup;
            clsDriverGroups.loadObject();

            if (IdDriverGroup == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (clsDriverGroups.Name.ToString().ToUpper() != txtName.Text.ToString().ToUpper())
                {
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }

            clsDriverGroups.Name = txtName.Text;
            clsDriverGroups.IdModel = IdModel;
            if (IdDriverGroup == 0)
            {
                clsDriverGroups.DateCreation = DateTime.Now;
                clsDriverGroups.UserCreation = (Int32)IdUserCreate;
                IdDriverGroup = clsDriverGroups.Insert();
            }
            else
            {
                clsDriverGroups.DateModification = DateTime.Now;
                clsDriverGroups.UserModification = (Int32)IdUserCreate;
                clsDriverGroups.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winDriverGroupEdit.Hide();
        }
                
        protected void EditDriverGroupLoad()
        {
            DataClass.clsDriverGroups clsDriverGroups= new DataClass.clsDriverGroups();
            DataTable dt = clsDriverGroups.LoadList("IdDriverGroup = " + IdDriverGroup, "");
            if (dt.Rows.Count > 0)
            {
                
                this.txtName.Text = (String)dt.Rows[0]["Name"];
            }
        }

        public void Delete()
        {
            DataClass.clsDriverGroups clsDriverGroups = new DataClass.clsDriverGroups();

            clsDriverGroups.IdDriverGroup = (Int32)IdDriverGroup;
            clsDriverGroups.Delete();
        }

        [DirectMethod]
        public void ClickedDeleteYES()
        {
            this.Delete();
            this.LoadDriverGroups();
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsDriverGroups objDriverGroups = new DataClass.clsDriverGroups();
            DataTable dt = objDriverGroups.LoadList("IdModel = " + IdModel + " AND Name = '" + txtName.Text + "'", "");
            Value = dt.Rows.Count;

            return Value;
        }

        protected void LoadDriverGroups()
        {
            String cond = "";
            DataClass.clsDriverGroups clsDriverGroups = new DataClass.clsDriverGroups();
            DataTable dt = clsDriverGroups.LoadList("IdModel = " + IdModel + cond, " ORDER BY Name");
            storeDriverGroup.DataSource = dt;
            storeDriverGroup.DataBind();
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = modMain.strEconomicModuleEconomicDriverGroup;
            this.btnAdd.Text = modMain.strEconomicModuleAddNewEconomicDriverGroup;
            this.btnAdd.ToolTip = modMain.strEconomicModuleAddNewEconomicDriverGroup;
            this.btnEconomicDriver.Text = modMain.strEconomicModuleListofEconomicDrivers;
            this.btnEconomicDriver.ToolTip = modMain.strEconomicModuleListofEconomicDrivers;
            this.Name.Text = modMain.strCommonName;
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = modMain.strCommonEdit;
            this.ImageCommandColumn1.Commands[1].Text = modMain.strCommonDelete;
            this.winDriverGroupEdit.Title = modMain.strEconomicModuleEconomicDriverGroup;
            this.txtName.FieldLabel = modMain.strCommonName;
            this.btnSave.Text = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;

        }

        #endregion

    }
}
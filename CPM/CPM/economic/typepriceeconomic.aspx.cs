﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _typepriceeconomic : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdTypePriceEconomic { get { return (Int32)Session["IdTypePriceEconomic"]; } set { Session["IdTypePriceEconomic"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        public DataTable datViewData { get; set; }
        
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                IdTypePriceEconomic = 0;
                this.ModelPrecalcModuleEconomic();
                this.LoadTypePriceEconomic();
                LoadLabel();
            }
        }

        protected void Economic_BeforeEdit(object sender, DirectEventArgs e)
        {
            int Criteria = Convert.ToInt32(e.ExtraParams["Criteria"]);

            switch (Criteria)
            {
                case 0:
                    //this.ddlEconomicDriver.Disable();
                    //this.txtElasticityBase.Disable();
                    //this.txtElasticityOptimistic.Disable();
                    //this.txtElasticityPessimistic.Disable();
                    //this.ddlEconomicDriver.Value = 2;
                    //this.ddlEconomicDriver.Value = null;
                    //this.ddlProjectionCriteria.Value = null;
                    this.ddlProjectionCriteria.Clear();
                    this.ddlProjectionCriteria.Disable();
                    break;
                case 2:
                    //this.ddlEconomicDriver.Disable();
                    //this.txtElasticityBase.Disable();
                    //this.txtElasticityOptimistic.Disable();
                    //this.txtElasticityPessimistic.Disable();
                    //this.ddlEconomicDriver.Value = null;
                    break;

                case 3:
                    //this.ddlEconomicDriver.Enable();
                    //this.txtElasticityBase.Enable();
                    //this.txtElasticityOptimistic.Enable();
                    //this.txtElasticityPessimistic.Enable();
                    //this.ddlEconomicDriver.Value = null;

                    break;
                default:
                    this.ddlProjectionCriteria.Enable();
                    break;
            }
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                String strData = e.ExtraParams["rowsValues"];
                datViewData = JSON.Deserialize<DataTable>(strData);
                this.Save();
                this.LoadTypePriceEconomic();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnSaveWeight_Click(object sender, DirectEventArgs e)
        {
            try
            {
                String strData = e.ExtraParams["rowsValues"];
                DataTable datViewData = new DataTable();
                datViewData = JSON.Deserialize<DataTable>(strData);

                object sumObject;
                sumObject = datViewData.Compute("Sum(Weight)", "");


                if (Convert.ToDouble(sumObject) != 100)
                {
                    this.StatusBar1.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Sum of weights has to equal 100%"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                this.SaveHeight(datViewData);
                //this.LoadDrivers();
                this.winWeightEdit.Hide();
                this.LoadTypePriceEconomic();

            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.StatusBar1.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            winWeightEdit.Hide();
            this.StatusBar1.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void grdPrice_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            IdTypePriceEconomic = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            if (Command == "Weight")
            {
                this.winWeightEdit.Title = (String)GetGlobalResourceObject("CPM_Resources", "Weight") + " (" + _Name + ")";
                this.LoadDrivers();
                this.winWeightEdit.Show();
            }
        }

        protected void ddlProjectionCriteria_Select(object sender, DirectEventArgs e)
        {
            int valor = Convert.ToInt32(e.ExtraParams["CriteriaValue"]);
            Int32 indexrowsel = RowSelectionModel1.SelectedIndex;
            switch (valor.ToString())
            {
                case "2":
                    //this.ddlEconomicDriver.Disable();
                    //this.txtElasticityBase.Disable();
                    //this.txtElasticityOptimistic.Disable();
                    //this.txtElasticityPessimistic.Disable();
                    break;

                case "3":
                    //this.ddlEconomicDriver.Enable();
                    //this.txtElasticityBase.Enable();
                    //this.txtElasticityOptimistic.Enable();
                    //this.txtElasticityPessimistic.Enable();
                    break;
            }
        }

        #endregion

        #region Methods

        protected void Save()
        {            
            foreach (DataRow row in datViewData.Rows)
            {
                DataClass.clsTypePriceEconomic objTypePriceEconomic = new DataClass.clsTypePriceEconomic();
                objTypePriceEconomic.IdTypePriceEconomic = Convert.ToInt32(row["IdTypePriceEconomic"]);
                objTypePriceEconomic.loadObject();
                objTypePriceEconomic.IdModel = IdModel;
                objTypePriceEconomic.IdZiffAccount = Convert.ToInt32(row["IdZiffAccount"]);
                objTypePriceEconomic.ProjectionCriteria = Convert.ToInt32(row["ProjectionCriteria"]);
                if (objTypePriceEconomic.IdTypePriceEconomic == 0)
                {
                    objTypePriceEconomic.DateCreation = DateTime.Now;
                    objTypePriceEconomic.UserCreation = (Int32)IdUserCreate;
                    IdTypePriceEconomic = objTypePriceEconomic.Insert();
                }
                else
                {
                    objTypePriceEconomic.DateModification = DateTime.Now;
                    objTypePriceEconomic.UserModification = (Int32)IdUserCreate;
                    objTypePriceEconomic.Update();
                }
            }
            ModelUpdateTimestamp();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Record saved successfully!"), IconCls = "icon-accept", Clear2 = false });
        }

        protected void SaveHeight(DataTable dt)
        {

            foreach (DataRow row in dt.Rows)
            {
                Int32 intIdTypePriceWeight = 0;
                Int32 intIdTypePriceEconomic = 0;
                Int32 intIdEconomicDriver = 0;
                Double dblWeight = 0;

                DataClass.clsTypePriceEconomic objTypePriceEconomic = new DataClass.clsTypePriceEconomic();
                intIdTypePriceWeight = Convert.ToInt32(row["IdTypePriceWeight"]);
                intIdTypePriceEconomic = Convert.ToInt32(row["IdTypePriceEconomic"]);
                intIdEconomicDriver = Convert.ToInt32(row["IdEconomicDriver"]);
                dblWeight = Convert.ToDouble(row["Weight"]);
                if (intIdTypePriceWeight == 0 && dblWeight != 0)
                {
                    objTypePriceEconomic.InsertWeight(intIdTypePriceWeight, IdTypePriceEconomic, intIdEconomicDriver, dblWeight);
                }
                else
                {
                    if (dblWeight == 0)
                    {
                        objTypePriceEconomic.DeleteWeight(intIdTypePriceWeight);
                    }
                    else
                    {
                        objTypePriceEconomic.UpdateWeight(intIdTypePriceWeight, IdTypePriceEconomic, intIdEconomicDriver, dblWeight);
                    }
                }
            }
            this.StatusBar1.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Record saved successfully!"), IconCls = "icon-accept", Clear2 = false });
        }

        protected void ProjectionCriteria()
        {
            DataClass.clsParameters objParameters = new DataClass.clsParameters();
            this.storeProjectionCriteria.DataSource = objParameters.LoadList("Type='EconomicProjectionCriteria'", "ORDER BY Name");
            this.storeProjectionCriteria.DataBind();
        }

        protected void EconomicDriver()
        {
            //DataClass.clsEconomicDriver objEconomicDriver = new DataClass.clsEconomicDriver();
            //this.storeEconomicDriver.DataSource = objEconomicDriver.LoadComboBox("IdModel=" + IdModel, "");
            //this.storeEconomicDriver.DataBind();
        }

        protected void LoadDrivers()
        {
            DataClass.clsTypePriceEconomic objTypePriceEconomic = new DataClass.clsTypePriceEconomic();
            objTypePriceEconomic.IdTypePriceEconomic = IdTypePriceEconomic;
            DataTable dt = objTypePriceEconomic.LoadDriverWeight(IdModel);

            storeWeight.DataSource = dt;
            storeWeight.DataBind();
        }

        protected void LoadTypePriceEconomic()
        {
            DataClass.clsTypePriceEconomic objTypePriceEconomic = new DataClass.clsTypePriceEconomic();
            objTypePriceEconomic.IdModel = IdModel;
            DataTable dt = objTypePriceEconomic.LoadList();
            ProjectionCriteria();
            EconomicDriver();
            storeTypePriceEconomic.DataSource = dt;
            storeTypePriceEconomic.DataBind();
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        public void ModelPrecalcModuleEconomic()
        {
            clsModels objModelPrecalc = new clsModels();
            objModelPrecalc.IdModel = IdModel;
            objModelPrecalc.CheckRecalcModuleParameter(objApplication.MaxRelationLevels);
            objModelPrecalc.CheckRecalcModuleAllocation();
            objModelPrecalc.PrecalcModuleEconomic();
            objModelPrecalc = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = modMain.strMasterEconomicAssumptions;
            this.btnSave.Text = modMain.strCommonSaveChanges;
            this.CodeZiffAccount.Text = modMain.strCommonCode;
            this.NameZiffAccount.Text = (String)GetGlobalResourceObject("CPM_Resources", "Description");
            this.Column1.Text = (String)GetGlobalResourceObject("CPM_Resources", "Projection Criteria");
            this.ImageCommandColumn1.Text = (String)GetGlobalResourceObject("CPM_Resources", "Weight");
            this.ImageCommandColumn1.Commands[0].Text = "&nbsp;" + (String)GetGlobalResourceObject("CPM_Resources", "Drivers");
            this.winWeightEdit.Title = (String)GetGlobalResourceObject("CPM_Resources", "Weight");
            this.colName.Text = (String)GetGlobalResourceObject("CPM_Resources", "Driver");
            this.colValue.Text = (String)GetGlobalResourceObject("CPM_Resources", "Weight") + "&nbsp;%";
            this.btnSaveWeight.Text = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
        }
        
        #endregion

    } 
}
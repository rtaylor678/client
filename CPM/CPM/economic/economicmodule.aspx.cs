﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _economicmodule : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }

        private static modMain objMain = new modMain();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                this.LoadLanguage();
                this.LoadModules();
            }
        }

        protected void btnPriceScenarios_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/economic/pricescenarios.aspx");
        }

        protected void btnPrices_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/economic/prices.aspx");
        }

        protected void btnEconomicDrivers_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/economic/economicdriver.aspx");
        }

        protected void btnEconomicDriverData_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/economic/economicdriverdata.aspx");
        }

        protected void btnZiffEconomicAnalysis_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/economic/ziffeconomicanalysis.aspx");
        }

        protected void btnPricesEconomic_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/economic/typepriceeconomic.aspx");
        }

        protected void btnPricesComposition_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/PriceComposition.aspx");
        }

        protected void btnPriceForecast_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/economic/priceforecast.aspx");
        }

        protected void LoadModules()
        {
            // Economic Module
            if (objApplication.AllowedRoles.Contains(34))
            {
                // Price Scenarios
                if (objApplication.AllowedRoles.Contains(8))
                {
                    this.btnPriceScenarios.Disabled = false;
                }
                // Prices
                if (objApplication.AllowedRoles.Contains(8))
                {
                    this.btnPrices.Disabled = false;
                }
                // List of Economic Drivers
                if (objApplication.AllowedRoles.Contains(35))
                {
                    this.btnEconomicDrivers.Disabled = false;
                }
                // Economic Drivers Data
                if (objApplication.AllowedRoles.Contains(36))
                {
                    this.btnEconomicDriverData.Disabled = false;
                }
                // Economic Assumptions
                if (objApplication.AllowedRoles.Contains(37))
                {
                    this.btnPricesEconomic.Disabled = false;
                }
                // Base Cost Rates Forecast
                if (objApplication.AllowedRoles.Contains(38))
                {
                    this.btnPriceForecast.Disabled = false;
                }
                // Ziff Economic Analysis
                if (objApplication.AllowedRoles.Contains(39))
                {
                    this.btnZiffEconomicAnalysis.Disabled = false;
                }
            }            
        }

        #endregion

        #region Methods

        protected void LoadLanguage()
        {
            DataClass.clsLabelsLanguages objLabelsLanguages = new DataClass.clsLabelsLanguages();
            DataTable dt = objLabelsLanguages.LoadList("LanguageName = '" + Language + "' AND FormName = 'EconomicModule'", "ORDER BY IdLabel");
            foreach (DataRow row in dt.Rows)
            {
                //idLanguage = (Int32)row["IdLanguage"];
                switch ((String)row["LabelName"])
                {
                    case "Economic Module":
                        modMain.strEconomicModuleFormTitle = (String)row["LabelText"];
                        break;
                    case "Economic Drivers":
                        modMain.strEconomicModuleEconomicDrivers = (String)row["LabelText"];
                        break;
                    case "Economic Driver Group":
                        modMain.strEconomicModuleEconomicDriverGroup = (String)row["LabelText"];
                        break;
                    case "Manage Economic Driver Groups":
                        modMain.strEconomicModuleManageEconomicDriverGroups = (String)row["LabelText"];
                        break;
                    case "Add New Economic Driver":
                        modMain.strEconomicModuleAddNewEconomicDriver = (String)row["LabelText"];
                        break;
                    case "Driver Group":
                        modMain.strEconomicModuleDriverGroup = (String)row["LabelText"];
                        break;
                    case "Economic Driver":
                        modMain.strEconomicModuleEconomicDriver = (String)row["LabelText"];
                        break;
                    case "Import Economic Drivers":
                        modMain.strEconomicModuleImportEconomicDrivers = (String)row["LabelText"];
                        break;
                    case "Add New Economic Driver Group":
                        modMain.strEconomicModuleAddNewEconomicDriverGroup = (String)row["LabelText"];
                        break;
                    case "List of Economic Drivers":
                        modMain.strEconomicModuleListofEconomicDrivers= (String)row["LabelText"];
                        break;
                    case "Pessimistic":
                        modMain.strEconomicModulePessimistic= (String)row["LabelText"];
                        break;
                    case "Optimistic":
                        modMain.strEconomicModuleOptimistic= (String)row["LabelText"];
                        break;
                    case "Import Economic Driver Data":
                        modMain.strEconomicModuleImportEconomicDriverData = (String)row["LabelText"];
                        break;
                    case "Economic Scenarios:":
                        modMain.strEconomicModuleEconomicScenarios = (String)row["LabelText"];
                        break;
                }
            }
            this.LoadLabel();
        }

        protected void LoadLabel()
        {
            this.pnlTableLayout.Title = modMain.strEconomicModuleFormTitle;
            this.btnEconomicDrivers.Text = objMain.FormatButtonText(modMain.strMasterListofEconomicDrivers);
            this.btnEconomicDriverData.Text = objMain.FormatButtonText(modMain.strMasterEconomicDriversData);
            this.btnPricesEconomic.Text = objMain.FormatButtonText(modMain.strMasterEconomicAssumptions);
            this.btnPriceForecast.Text = objMain.FormatButtonText(modMain.strMasterBaseCostRatesForecast);
            this.btnZiffEconomicAnalysis.Text = objMain.FormatButtonText(modMain.strMasterZiffEconomicAnalysis);
            this.btnPrices.Text = (String)GetGlobalResourceObject("CPM_Resources", "Prices");
            this.btnPriceScenarios.Text = (String)GetGlobalResourceObject("CPM_Resources", "PriceScenarios");
        }

        #endregion
    }
}
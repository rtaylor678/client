﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using System.Xml.Xsl;
using CD;
using DataClass;
using System.ComponentModel;
using System.Web.Script.Serialization;

namespace CPM
{
    public partial class _economicdriverdata : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdEconomicDriverData { get { return (Int32)Session["IdEconomicDriverData"]; } set { Session["IdEconomicDriverData"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        public DataTable datViewData { get; set; }
        private DataReportObject ExportEconomicDriversData { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;
      
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                IdEconomicDriverData = 0;
                LoadLabel();
            }
        }

        protected void ddlFilterEconomicDriver_Select(object sender, DirectEventArgs e)
        {
            try
            {
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = " ", IconCls = "", Clear2 = false });
                LoadEconomicDriverData();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                String strData = e.ExtraParams["rowsValues"];

                datViewData = new DataTable();

                datViewData.Columns.Add("IdEconomicDriverData", typeof(int));
                datViewData.Columns.Add("IdEconomicDriver", typeof(int));
                datViewData.Columns.Add("Year", typeof(int));
                datViewData.Columns.Add("Normal", typeof(float));
                datViewData.Columns.Add("NormalElasticity", typeof(float));
                datViewData.Columns.Add("NormalWeight", typeof(float));
                datViewData.Columns.Add("Optimistic", typeof(float));
                datViewData.Columns.Add("OptimisticElasticity", typeof(float));
                datViewData.Columns.Add("OptimisticWeight", typeof(float));
                datViewData.Columns.Add("Pessimistic", typeof(float));
                datViewData.Columns.Add("PessimisticElasticity", typeof(float));
                datViewData.Columns.Add("PessimisticWeight", typeof(float));

                List<Object> data = JSON.Deserialize<List<Object>>(strData);

                foreach (Object value in data)
                {
                    Object _ObjectRow = new Object();
                    _ObjectRow = value.ToString();

                    JavaScriptSerializer ser = new JavaScriptSerializer();

                    Dictionary<string, object> _Row = ser.Deserialize<Dictionary<string, object>>(_ObjectRow.ToString());
                    DataRow _NewRow = datViewData.NewRow();

                    _NewRow["IdEconomicDriverData"] = _Row["IdEconomicDriverData"];
                    _NewRow["IdEconomicDriver"] = _Row["IdEconomicDriver"];
                    _NewRow["Year"] = _Row["Year"];
                    _NewRow["Normal"] = _Row["Normal"];
                    _NewRow["NormalElasticity"] = _Row["NormalElasticity"];
                    _NewRow["NormalWeight"] = _Row["NormalWeight"];
                    _NewRow["Optimistic"] = _Row["Optimistic"];
                    _NewRow["OptimisticElasticity"] = _Row["OptimisticElasticity"];
                    _NewRow["OptimisticWeight"] = _Row["OptimisticWeight"];
                    _NewRow["Pessimistic"] = _Row["Pessimistic"];
                    _NewRow["PessimisticElasticity"] = _Row["PessimisticElasticity"];
                    _NewRow["PessimisticWeight"] = _Row["PessimisticWeight"];

                    datViewData.Rows.Add(_NewRow);
                }

                this.Save();
                this.LoadEconomicDriverData();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.fileImport.Text = "";
            this.winImport.Show();
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void StoreFilterEconomicDriver_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsEconomicDriver objEconomicDriver = new DataClass.clsEconomicDriver();
            this.storeFilterEconomicDriver.DataSource = objEconomicDriver.LoadList("IdModel = " + IdModel, "ORDER BY Name");
            this.storeFilterEconomicDriver.DataBind();
        }

        protected void EDDWeight_BeforeEdit(object sender, DirectEventArgs e)
        {
            DataClass.clsEconomicDriverData Operation = new clsEconomicDriverData();
            int yearprocess = Convert.ToInt32(e.ExtraParams["Year"]);
            int driver = Convert.ToInt32(e.ExtraParams["Driver"]);
            double restper = Operation.PercentageResidual(driver, IdModel, yearprocess, 1);
            EDDNormalWeight.SetMaxValue(restper);
            double restper2 = Operation.PercentageResidual(driver, IdModel, yearprocess, 2);
            EDDOptimisticWeight.SetMaxValue(restper2);
            double restper3 = Operation.PercentageResidual(driver, IdModel, yearprocess, 3);
            EDDPessimisticWeight.SetMaxValue(restper3);
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            clsModels objModels = new clsModels();
            objModels.IdModel = IdModel;
            objModels.loadObject();
            int FirstYear = objModels.BaseYear;
            int LastYear = objModels.BaseYear + objModels.Projection;
            if (LastYear > FirstYear + objApplication.EconomicProjectionYears)
            {
                // Limit to set number of years allowed - any years in addition to the max will use the last year as their value
                LastYear = FirstYear + objApplication.EconomicProjectionYears;
            }
            objModels = null;

            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.EconomicDriverData);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(FirstYear, LastYear, row["Driver Desc"].ToString(), Convert.ToInt32(row["Year"]), Convert.ToDouble(row["Normal"]), Convert.ToDouble(row["Optimistic"]), Convert.ToDouble(row["Pessimistic"]));
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadEconomicDriverData();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        protected void Save()
        {
            foreach (DataRow row in datViewData.Rows)
            {
                DataClass.clsEconomicDriverData objEconomicDriverData = new DataClass.clsEconomicDriverData();
                objEconomicDriverData.IdEconomicDriverData = Convert.ToInt32(row["IdEconomicDriverData"]);
                objEconomicDriverData.IdEconomicDriver = Convert.ToInt32(row["IdEconomicDriver"]);
                objEconomicDriverData.loadObject();
                objEconomicDriverData.IdModel = IdModel;
                objEconomicDriverData.Year = Convert.ToInt32(row["Year"]);
                objEconomicDriverData.Normal = Convert.ToDouble(row["Normal"]);
                objEconomicDriverData.NormalElasticity = Convert.ToDouble(row["NormalElasticity"]);
                objEconomicDriverData.NormalWeight = Convert.ToDouble(row["NormalWeight"]);
                objEconomicDriverData.Optimistic = Convert.ToDouble(row["Optimistic"]);
                objEconomicDriverData.OptimisticElasticity = Convert.ToDouble(row["OptimisticElasticity"]);
                objEconomicDriverData.OptimisticWeight = Convert.ToDouble(row["OptimisticWeight"]);
                objEconomicDriverData.Pessimistic = Convert.ToDouble(row["Pessimistic"]);
                objEconomicDriverData.PessimisticElasticity = Convert.ToDouble(row["PessimisticElasticity"]);
                objEconomicDriverData.PessimisticWeight = Convert.ToDouble(row["PessimisticWeight"]);

                if (objEconomicDriverData.IdEconomicDriverData == 0)
                {
                    objEconomicDriverData.DateCreation = DateTime.Now;
                    objEconomicDriverData.UserCreation = (Int32)IdUserCreate;
                    IdEconomicDriverData = objEconomicDriverData.Insert();
                }
                else
                {
                    objEconomicDriverData.DateModification = DateTime.Now;
                    objEconomicDriverData.UserModification = (Int32)IdUserCreate;
                    objEconomicDriverData.Update();
                }
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Record saved successfully!"), IconCls = "icon-accept", Clear2 = false });
            this.ModelUpdateTimestamp();
        }

        protected void SaveImport(Int32 _First, Int32 _Last, String _Driver, Int32 _Year, Double _Normal, Double _Optimistic, Double _Pessimistic)
        {
            Int32 _IdDriver = GetIdDriver(_Driver);

            if (_IdDriver != 0 && _Year != 0 && _Year >= _First && _Year <= _Last)
            {
                DataClass.clsEconomicDriverData objEconomicDriverData = new DataClass.clsEconomicDriverData();
                DataTable dtAux = objEconomicDriverData.LoadList("IdModel = " + IdModel + " And IdEconomicDriver = " + _IdDriver + " And Year = " + _Year, "");
                if (dtAux.Rows.Count > 0)
                {
                    IdEconomicDriverData = Convert.ToInt32(dtAux.Rows[0]["IdEconomicDriverData"]);
                }
                else
                {
                    IdEconomicDriverData = 0;
                }
                objEconomicDriverData.IdEconomicDriverData = (Int32)IdEconomicDriverData;
                objEconomicDriverData.loadObject();
                objEconomicDriverData.IdEconomicDriver = _IdDriver;
                objEconomicDriverData.Year = _Year;
                objEconomicDriverData.IdModel = IdModel;
                objEconomicDriverData.Normal = _Normal;
                //objEconomicDriverData.NormalElasticity = _NormalElasticity;
                //objEconomicDriverData.NormalWeight = _NormalWeight;
                objEconomicDriverData.Optimistic = _Optimistic;
                //objEconomicDriverData.OptimisticElasticity = _OptimisticElasticity;
                //objEconomicDriverData.OptimisticWeight = _OptimisticWeight;
                objEconomicDriverData.Pessimistic = _Pessimistic;
                //objEconomicDriverData.PessimisticElasticity = _PessimisticElasticity;
                //objEconomicDriverData.PessimisticWeight = _PessimisticWeight;
                if (objEconomicDriverData.IdEconomicDriverData == 0)
                {
                    objEconomicDriverData.DateCreation = DateTime.Now;
                    objEconomicDriverData.UserCreation = (Int32)IdUserCreate;
                    IdEconomicDriverData = objEconomicDriverData.Insert();
                }
                else
                {
                    objEconomicDriverData.DateModification = DateTime.Now;
                    objEconomicDriverData.UserModification = (Int32)IdUserCreate;
                    objEconomicDriverData.Update();
                }
                dtAux.Dispose();
                dtAux = null;
                objEconomicDriverData = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected Int32 GetIdDriver(String _Driver)
        {
            Int32 Value = 0;
            DataClass.clsEconomicDriver objEconomicDriver = new DataClass.clsEconomicDriver();
            DataTable dt = objEconomicDriver.LoadList("Name = '" + _Driver + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdEconomicDriver"];
            }
            return Value;
        }

        protected void LoadEconomicDriverData()
        {
            DataClass.clsEconomicDriverData objEconomicDriverData = new DataClass.clsEconomicDriverData();
            if (ddlFilterEconomicDriver.SelectedItem.Value != null && ddlFilterEconomicDriver.SelectedItem.Text != null)
            {
                    objEconomicDriverData.IdEconomicDriver = Convert.ToInt32(ddlFilterEconomicDriver.SelectedItem.Value);
                    objEconomicDriverData.IdModel = Convert.ToInt32(IdModel);
                    DataTable dt = objEconomicDriverData.LoadList();
                    storeEconomicDriverData.DataSource = dt;
                    storeEconomicDriverData.DataBind();                    
            }
            else
            {
                storeEconomicDriverData.RemoveAll();
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","Select Economic Driver please"), IconCls = "icon-exclamation", Clear2 = false });
                return;
            }
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportEconomicDriversData.xls", "template");
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = modMain.strMasterEconomicDriversData;
            this.lbEconomicDriver.Text = modMain.strEconomicModuleEconomicDriver + ":";
            this.btnSave.Text = modMain.strCommonSaveChanges;
            this.btnSave.ToolTip = modMain.strCommonSaveChanges;
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strEconomicModuleImportEconomicDriverData;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.Year.Text = modMain.strTechnicalModuleYear;
            this.Column1.Text = (String)GetGlobalResourceObject("CPM_Resources", "Scenarios");
            this.numColumn2.Text = (String)GetGlobalResourceObject("CPM_Resources", "Normal");
            this.NumColumn6.Text = modMain.strEconomicModuleOptimistic;
            this.NumColumn10.Text = modMain.strEconomicModulePessimistic;
            this.winImport.Title = modMain.strEconomicModuleImportEconomicDriverData;
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataClass.clsEconomicDriverData objTechnicalDriverData = new DataClass.clsEconomicDriverData();
            DataTable dt = objTechnicalDriverData.LoadListExport("IdModel = " + IdModel, " ORDER BY DriverDesc, Year");

            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("DriverDesc", "Driver Desc"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Year", "Year"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Normal", "Normal"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Optimistic", "Optimistic"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Pessimistic", "Pessimistic"));
            String strSelectedParameters = "";

            ExportEconomicDriversData = new DataReportObject(dt, "DATASHEET", null, strSelectedParameters, null, "DriverDesc, Year", datColumnTD, null);

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportEconomicDriversData;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=4.2 ExportEconomicDriversData.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        #endregion

    }
}
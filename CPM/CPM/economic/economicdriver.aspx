﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="economicdriver.aspx.cs" Inherits="CPM._economicdriver" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" AutoScroll="true" runat="server" Title="Economic Drivers" IconCls="icon-economicdrivers_16" Border="false">
                <Items>
                    <ext:Panel ID="Panel1" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                        <Items>
                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lbDriverGroup" runat="server" Text="Economic Driver Group:"></ext:Label>
                            <ext:Combobox Style="margin: 10px 010px 10px 10px" ID="ddlDriverGroupCond"  MinChars="0" runat="server" DisplayField="Name" ValueField="ID" Width="200">
                                <ListConfig LoadingText="Searching..." MinWidth="300">
                                </ListConfig>
                                <Store>
                                    <ext:Store ID="storeFilterDriverGroup" runat="server" OnReadData="StoreFilterDriverGroup_ReadData">
                                        <Model>
                                            <ext:Model ID="Model5" runat="server" IDProperty="ID">
                                                <Fields>
                                                    <ext:ModelField Name="Name" />
                                                    <ext:ModelField Name="ID" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                                <Reader>
                                                    <ext:JsonReader  />
                                                </Reader>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Select OnEvent="ddlDriverGroupCond_Select" />
                                </DirectEvents>
                            </ext:Combobox>
                            <ext:Button Style="margin: 10px 10px 10px 10px" ID="btnEconomicDriverGroups" TextAlign="Center" IconCls="icon-economicdrivers_16" Text="Manage Economic Driver Groups" runat="server">
                                <DirectEvents>
                                    <Click OnEvent="btnEconomicDriverGroup_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Panel>
                    <ext:GridPanel ID="grdEconomicDrivers" runat="server" Border="false" Header="false">
                        <TopBar>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                    <ext:Button  ID="btnAdd" Icon="Add" ToolTip="Add New Economic Driver" runat="server" Text="Add New Economic Driver">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarSeparator />
                                    <ext:Button ID="btnDelete" Icon="Delete" ToolTip="Delete All" runat="server" Text="Delete All">
                                        <DirectEvents>
                                            <Click OnEvent="btnDeleteAll_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarSeparator />
                                    <ext:Button ID="btnImport" Icon="BookAdd" runat="server" Text="Import">
                                        <DirectEvents>
                                            <Click OnEvent="btnImport_Click">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarFill />
                                    <ext:Button ID="btnSaveExcel" runat="server" Text="Export Data To Excel" Icon="PageExcel">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveExcel_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnDownloadTemplate" runat="server" Text="Download Import Template" Icon="PageSave">
                                        <DirectEvents>
                                            <Click OnEvent="btnDownloadTemplate_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="storeEconomicDriver" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="IdEconomicDriver">
                                        <Fields>
                                            <ext:ModelField Name="IdEconomicDriver" Type="Int" />
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="NameDriverGroup" Type="String" />
                                            <ext:ModelField Name="IdDriverGroup" Type="Int" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel ID="ColumnModel1" runat="server">
                            <Columns>
                                <ext:Column ID="colID" Hidden="true" runat="server" Text="ID" DataIndex="IdEconomicDriver" />
                                <ext:Column ID="Name" runat="server" Text="Name" DataIndex="Name" Flex="1" />
                                <ext:Column ID="NameDriverGroup" runat="server" Text="Economic Driver Group" DataIndex="NameDriverGroup" Flex="1" />
                                <ext:ImageCommandColumn Align="Center" Width="150" Text="Functions" ID="ImageCommandColumn1" runat="server">
                                    <Commands>
                                        <ext:ImageCommand CommandName="Edit" IconCls="icon-edit_16" Text="&nbsp;Edit" />
                                    </Commands> 
                                        <Commands>
                                        <ext:ImageCommand CommandName="Delete" IconCls="icon-delete_16" Text="&nbsp;Delete" />
                                    </Commands> 
                                    <DirectEvents>                                       
                                        <Command OnEvent="grdEconomicDriver_Command">
                                            <ExtraParams>  
                                                    <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>  
                                                    <ext:Parameter Name="Id" Value="record.data.IdEconomicDriver" Mode="Raw"></ext:Parameter> 
                                                    <ext:Parameter Name="Name" Value="record.data.Name" Mode="Raw"></ext:Parameter> 
                                            </ExtraParams> 
                                        </Command>
                                    </DirectEvents>
                                </ext:ImageCommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" />
                        </SelectionModel>
                        <Features>
                            <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                                <Filters>
                                    <ext:StringFilter DataIndex="Name" />
                                    <ext:StringFilter DataIndex="NameDriverGroup" />
                                </Filters>
                            </ext:GridFilters>
                        </Features>
                    </ext:GridPanel>
                    <ext:Window ID="winEconomicDriverEdit" runat="server" Title="Economic Driver" Hidden="true" IconCls="icon-economicdrivers_16" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="pnlEditEconomicDriver" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                            <Items>
                                <ext:Panel ID="Panel3" runat="server" Border="false" Layout="Form" ColumnWidth="1" Cls="PopupFormColumnPanel">
                                    <Defaults>
                                        <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                        <ext:Parameter Name="MsgTarget" Value="side" />
                                    </Defaults>
                                    <Items>
                                        <ext:TextField ID="txtName" runat="server" FieldLabel="Name" AnchorHorizontal="92%" Cls="PopupFormField" />
                                    </Items>
                                </ext:Panel>
                                <ext:Panel ID="Panel2" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                    <Defaults>
                                        <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                        <ext:Parameter Name="MsgTarget" Value="side" />
                                    </Defaults>
                                    <Items>
                                        <ext:ComboBox ID="ddlDriverGroup" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" MinChars="0" FieldLabel="Driver Group" ValueField="ID" Cls="PopupFormField">
                                            <Store>
                                                <ext:Store ID="storeDriverGroup" runat="server" OnReadData="StoreDriverGroup_ReadData">
                                                    <Model>
                                                        <ext:Model ID="Model2" runat="server" IDProperty="ID">
                                                            <Fields>
                                                                <ext:ModelField Name="Name" />
                                                                <ext:ModelField Name="ID" />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                    <Proxy>
                                                        <ext:PageProxy>
                                                            <Reader>
                                                                <ext:JsonReader  />
                                                            </Reader>
                                                        </ext:PageProxy>
                                                    </Proxy>
                                                </ext:Store>
                                            </Store>
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Panel>
                            </Items>
                            <Buttons>
                                <ext:Button ID="btnSave" runat="server" Text="Save"  icon="DatabaseSave" Disabled="true" FormBind="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnSave_Click" />
                                    </DirectEvents>
                                </ext:Button>
                                <ext:Button ID="btnCancel" runat="server" icon="Decline" Text="Close">
                                    <DirectEvents>
                                        <Click OnEvent="btnCancel_Click" />
                                    </DirectEvents>
                                </ext:Button>
                            </Buttons>
                            <BottomBar>
                                <ext:StatusBar ID="FormStatusBar" runat="server" />
                            </BottomBar>
                            <Listeners>
                                <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                            });
                                                            #{btnSave}.setDisabled(!valid);" />
                            </Listeners>
                        </ext:FormPanel>
                        </Items>
                    </ext:Window>
                    <ext:Window ID="winImport"  runat="server" Title="Import Economic Drivers" Hidden="true" Icon="ArrowLeft" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="frmImportFile" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                                <Items>
                                    <ext:Panel ID="pnlImportFile1" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:FileUploadField ID="fileImport" FieldLabel="File" runat="server" Icon="Attach" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnImportFile" runat="server" Text="Import" Icon="TableRefresh" Disabled="true" FormBind="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnImportFile_Click">
                                                <EventMask ShowMask="true" Target="CustomTarget" CustomTarget="vpMain" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnCancelImport" runat="server" Icon="Decline" Text="Close">
                                        <DirectEvents>
                                            <Click OnEvent="btnCancelImport_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                                <BottomBar>
                                    <ext:StatusBar ID="FormImportStatusBar" runat="server" />
                                </BottomBar>
                                <Listeners>
                                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                            });
                                                            #{btnImportFile}.setDisabled(!valid);" />
                                </Listeners>
                            </ext:FormPanel>
                        </Items>
                    </ext:Window>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="StatusBar1" runat="server" />
                </BottomBar>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="typepriceeconomic.aspx.cs" Inherits="CPM._typepriceeconomic" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var criteriarenderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
            var r = App.storeProjectionCriteria.getById(value);
            if (Ext.isEmpty(r)) {
                return "";
            }
            return r.data.Name;
        }

        var economicdriverrenderer = function (value) {
            var r = App.storeEconomicDriver.getById(value);
            if (Ext.isEmpty(r)) {
                return "";
            }
            return r.data.Name;
        }
        var prepareCommand = function (grid, command, record, row) {
            if (command.command == 'Weight' && record.data.Parent == 0) {
                command.disabled = true;
                command.hidden = true;
            }
        };
    </script>
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" Layout="FitLayout" AutoScroll="true" runat="server" Title="Economic Assumptions" IconCls="icon-economicassumptions_16" Border="false">
                <Items>
                    <ext:GridPanel ID="grdTypePriceEconomic" runat="server" Border="false" Header="false">
                        <TopBar>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                    <ext:Button  ID="btnSave" Icon="DataBaseSave" ToolTip="Save Changes" runat="server" Text="Save Changes">
                                        <DirectEvents>
                                            <Click OnEvent="btnSave_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="rowsValues" Value="#{grdTypePriceEconomic}.getRowsValues(false)" Mode="Raw" Encode="true" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="storeTypePriceEconomic" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="IdTypePriceEconomic">
                                        <Fields>
                                            <ext:ModelField Name="Parent" Type="Int" />
                                            <ext:ModelField Name="IdTypePriceEconomic" Type="Int" />
                                            <ext:ModelField Name="IdZiffAccount" Type="Int" />
                                            <ext:ModelField Name="CodeZiffAccount" Type="String" />
                                            <ext:ModelField Name="NameZiffAccount" Type="String" />
                                            <ext:ModelField Name="ProjectionCriteria" Type="Int" />
                                            <ext:ModelField Name="NameProjectionCriteria" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Plugins>
                            <ext:CellEditing ID="CellEditing1" runat="server" ClicksToEdit="1" />
                        </Plugins>
                        <ColumnModel ID="ColumnModel1" runat="server">
                            <Columns>
                                <ext:Column ID="colID" Hidden="true" runat="server" Text="ID" DataIndex="IdTypePriceEconomic" />
                                <ext:Column ID="IdZiff" Hidden="true" runat="server" Text="IDZiff" DataIndex="IdZiffAccount" />
                                <ext:Column ID="CodeZiffAccount" runat="server" Text="Code" DataIndex="CodeZiffAccount" Flex="1" />
                                <ext:Column ID="NameZiffAccount" runat="server" Text="Description" DataIndex="NameZiffAccount" Flex="1" />
                                <ext:Column ID="Column1" runat="server" DataIndex="ProjectionCriteria" Text="Projection Criteria" Flex="1">
                                    <Renderer Fn="criteriarenderer" />
                                    <Editor>
                                        <ext:ComboBox ID="ddlProjectionCriteria" runat="server" DisplayField="Name" ValueField="Code" QueryMode="Local">
                                            <Store>
                                                <ext:Store ID="storeProjectionCriteria" runat="server">
                                                    <Model>
                                                        <ext:Model ID="Model5" runat="server" IDProperty="Code">
                                                            <Fields>
                                                                <ext:ModelField Name="Name" />
                                                                <ext:ModelField Name="Code" />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                </ext:Store>
                                            </Store>
                                            <DirectEvents>
                                                <Select OnEvent="ddlProjectionCriteria_Select">
                                                    <%-- <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" /> --%>
                                                    <ExtraParams>
                                                        <ext:Parameter Name="CriteriaValue" Value="#{ddlProjectionCriteria}.getValue()" Mode="Raw" Encode="true" />
                                                    </ExtraParams>
                                                </Select>
                                            </DirectEvents>
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:ImageCommandColumn Align="Center" Width="80" Text="Weight" ID="ImageCommandColumn1" runat="server">
                                    <Commands>
                                        <ext:ImageCommand CommandName="Weight" Icon="PageWhiteEdit" Text="&nbsp;Drivers" />
                                    </Commands>
                                    <PrepareCommand Fn="prepareCommand" />
                                    <DirectEvents>
                                        <Command OnEvent="grdPrice_Command">
                                            <ExtraParams>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="Id" Value="record.data.IdTypePriceEconomic" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="Name" Value="record.data.NameZiffAccount" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="Parent" Value="record.data.Parent" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:ImageCommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" />
                        </SelectionModel>
                        <Features>
                            <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                                <Filters>
                                    <ext:StringFilter DataIndex="CodeZiffAccount" />
                                    <ext:StringFilter DataIndex="NameZiffAccount" />
                                    <ext:StringFilter DataIndex="ProjectionCriteria" />
                                </Filters>
                            </ext:GridFilters>
                        </Features>
                        <DirectEvents>
                            <BeforeEdit OnEvent="Economic_BeforeEdit">
                                <EventMask ShowMask="true" Target="This">
                                </EventMask>
                                <ExtraParams>
                                    <ext:Parameter Name="Criteria" Value="e.record.data.Parent" Mode="Raw">
                                    </ext:Parameter>
                                </ExtraParams>
                            </BeforeEdit>
                        </DirectEvents>
                    </ext:GridPanel>
                    <ext:Window ID="winWeightEdit" runat="server" Title="Weight" Hidden="true" Icon="WeatherSun" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="pnlWeightEdit" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                                <Items>
                                    <ext:Panel ID="Panel3" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" LabelAlign="Top" Cls="PopupFormColumnPanelGrid">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:GridPanel ID="grdWeight" runat="server" Height="145">
                                                <Store>
                                                    <ext:Store ID="storeWeight" runat="server">
                                                        <Model>
                                                            <ext:Model ID="modelWeight" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdTypePriceWeight" Type="Int" />
                                                                    <ext:ModelField Name="IdTypePriceEconomic" Type="Int" />
                                                                    <ext:ModelField Name="IdEconomicDriver" Type="Int" />
                                                                    <ext:ModelField Name="Name" Type="String" />
                                                                    <ext:ModelField Name="Weight" Type="Float" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel ID="cmWeight" runat="server">
                                                    <Columns>
                                                        <ext:Column ID="colName" runat="server" Text="Driver" DataIndex="Name" Flex="1">
                                                        </ext:Column>
                                                        <ext:NumberColumn ID="colValue" Align="Center" Format="0.00 %" runat="server" Text="Weight %" DataIndex="Weight">
                                                            <Editor>
                                                                <ext:NumberField DecimalPrecision="2" MinValue="0" ID="numValue" runat="server" />
                                                            </Editor>
                                                        </ext:NumberColumn>
                                                    </Columns>
                                                </ColumnModel>
                                                <Features>
                                                    <ext:GridFilters runat="server" ID="GridFilters2" Local="true">
                                                        <Filters>
                                                            <ext:StringFilter DataIndex="Code" />
                                                            <ext:StringFilter DataIndex="Name" />
                                                            <ext:NumericFilter DataIndex="Value" />
                                                        </Filters>
                                                    </ext:GridFilters>
                                                </Features>
                                                <SelectionModel>
                                                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" />
                                                </SelectionModel>
                                                <Plugins>
                                                    <ext:CellEditing ID="CellEditing2" runat="server" ClicksToEdit="1" />
                                                </Plugins>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnSaveWeight" runat="server" Text="Save" Icon="DatabaseSave" Disabled="true" FormBind="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveWeight_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="rowsValues" Value="#{grdWeight}.getRowsValues(false)" Mode="Raw" Encode="true" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnCancel" runat="server" Icon="Decline" Text="Close">
                                        <DirectEvents>
                                            <Click OnEvent="btnCancel_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                                <BottomBar>
                                    <ext:StatusBar ID="StatusBar1" runat="server" />
                                </BottomBar>
                                <Listeners>
                                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                            });
                                            #{btnSave}.setDisabled(!valid);" />
                                </Listeners>
                            </ext:FormPanel>
                        </Items>
                    </ext:Window>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="FormStatusBar" runat="server" />
                </BottomBar>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>
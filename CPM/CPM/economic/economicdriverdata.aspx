﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="economicdriverdata.aspx.cs" Inherits="CPM._economicdriverdata" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" AutoScroll="true" runat="server" Title="Economic Drivers Data" IconCls="icon-economicdriversdata_16" Border="false">
                <Items>
                    <ext:Panel ID="Panel1" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                        <Items>
                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lbEconomicDriver" runat="server" Text="Economic Driver:"></ext:Label>
                            <ext:ComboBox Style="margin: 10px 10px 10px 10px" ID="ddlFilterEconomicDriver" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" Width="200" MinChars="0" ValueField="IdEconomicDriver">
                                <ListConfig LoadingText="Searching..." MinWidth="300">
                                    <ItemTpl ID="ItemTpl2" runat="server">
                                        <Html>
                                            <div class="search-item">
							                    <h3>{Name}</h3>
							                    <span>Group: </span>{NameDriverGroup}</span>
						                    </div>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Store>
                                    <ext:Store ID="storeFilterEconomicDriver" runat="server" OnReadData="StoreFilterEconomicDriver_ReadData">
                                        <Model>
                                            <ext:Model ID="Model6" runat="server" IDProperty="IdEconomicDriver">
                                                <Fields>
                                                    <ext:ModelField Name="IdEconomicDriver" />
                                                    <ext:ModelField Name="Name" />
                                                    <ext:ModelField Name="NameDriverGroup" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                                <Reader>
                                                    <ext:JsonReader />
                                                </Reader>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Select OnEvent="ddlFilterEconomicDriver_Select" />
                                </DirectEvents>
                            </ext:ComboBox>
                        </Items>
                    </ext:Panel>
                    <ext:GridPanel ID="grdEconomicDriverData" runat="server" Border="false" Header="false">
                        <TopBar>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btnSave" Icon="DataBaseSave" ToolTip="Save Changes" runat="server" Text="Save Changes">
                                        <DirectEvents>
                                            <Click OnEvent="btnSave_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="rowsValues" Value="#{grdEconomicDriverData}.getRowsValues(false)"
                                                        Mode="Raw" Encode="true" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarSeparator />
                                    <ext:Button ID="btnImport" Icon="BookAdd" ToolTip="Import Economic Driver Data" runat="server" Text="Import">
                                        <DirectEvents>
                                            <Click OnEvent="btnImport_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="rowsValues" Value="#{grdEconomicDriverData}.getRowsValues(false)" Mode="Raw" Encode="true" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarFill />
                                    <ext:Button ID="btnSaveExcel" runat="server" Text="Export Data To Excel" Icon="PageExcel">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveExcel_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnDownloadTemplate" runat="server" Text="Download Import Template" Icon="PageSave">
                                        <DirectEvents>
                                            <Click OnEvent="btnDownloadTemplate_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="storeEconomicDriverData" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="IdEconomicDriverData">
                                        <Fields>
                                            <ext:ModelField Name="IdEconomicDriver" Type="Int" />
                                            <ext:ModelField Name="Year" Type="Int" />
                                            <ext:ModelField Name="Normal" Type="Float" />
                                            <ext:ModelField Name="NormalElasticity" Type="Float" />
                                            <ext:ModelField Name="NormalWeight" Type="Float" />
                                            <ext:ModelField Name="Optimistic" Type="Float" />
                                            <ext:ModelField Name="OptimisticElasticity" Type="Float" />
                                            <ext:ModelField Name="OptimisticWeight" Type="Float" />
                                            <ext:ModelField Name="Pessimistic" Type="Float" />
                                            <ext:ModelField Name="PessimisticElasticity" Type="Float" />
                                            <ext:ModelField Name="PessimisticWeight" Type="Float" />
                                            <ext:ModelField Name="IdEconomicDriverData" Type="Int" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Plugins>
                            <ext:CellEditing ID="CellEditing1" runat="server" ClicksToEdit="1" />
                        </Plugins>
                        <ColumnModel ID="ColumnModel1" runat="server">
                            <Columns>
                                <ext:Column ID="colID" Hidden="true" runat="server" Text="ID" DataIndex="IdEconomicDriverData" />
                                <ext:Column ID="Year" Width="200" runat="server" Text="Year" DataIndex="Year" />
                                <ext:Column ID="Column1" runat="server" Text="Scenarios" Flex="2">
                                    <Columns>
                                        <ext:NumberColumn Format="0.00" ID="numColumn2" runat="server" DataIndex="Normal" Align="Right" Text="Normal" Width="200">
                                            <Editor>
                                                <ext:NumberField ID="NumberField1" runat="server" />
                                            </Editor>
                                        </ext:NumberColumn>
                                        <ext:NumberColumn Format="0.00" ID="NumColumn3" runat="server" DataIndex="NormalElasticity" Text="Elasticity" Hidden="true">
                                            <Editor>
                                                <ext:NumberField ID="NumberField2" runat="server" MinValue="0" MaxValue="2" />
                                            </Editor>
                                        </ext:NumberColumn>
                                        <ext:NumberColumn Format="0.00" ID="NumColumn4" runat="server" DataIndex="NormalWeight" Text="Weight" Hidden="true">
                                            <Editor>
                                                <ext:NumberField ID="EDDNormalWeight" runat="server" MinValue="0" MaxValue="100" />
                                            </Editor>
                                        </ext:NumberColumn>
                                        <ext:NumberColumn Format="0.00" ID="NumColumn6" runat="server" DataIndex="Optimistic" Align="Right" Text="Optimistic" Width="200">
                                            <Editor>
                                                <ext:NumberField ID="NumberField4" runat="server" />
                                            </Editor>
                                        </ext:NumberColumn>
                                        <ext:NumberColumn Format="0.00" ID="NumColumn7" runat="server" DataIndex="OptimisticElasticity" Text="Elasticity" Hidden="true">
                                            <Editor>
                                                <ext:NumberField ID="NumberField5" runat="server" MinValue="0" MaxValue="2" />
                                            </Editor>
                                        </ext:NumberColumn>
                                        <ext:NumberColumn Format="0.00" ID="NumColumn8" runat="server" DataIndex="OptimisticWeight" Text="Weight" Hidden="true">
                                            <Editor>
                                                <ext:NumberField ID="EDDOptimisticWeight" runat="server" MinValue="0" MaxValue="100" />
                                            </Editor>
                                        </ext:NumberColumn>
                                        <ext:NumberColumn Format="0.00" ID="NumColumn10" runat="server" DataIndex="Pessimistic" Align="Right" Text="Pessimistic" Width="200">
                                            <Editor>
                                                <ext:NumberField ID="NumberField7" runat="server" />
                                            </Editor>
                                        </ext:NumberColumn>
                                        <ext:NumberColumn Format="0.00" ID="NumColumn11" runat="server" DataIndex="PessimisticElasticity" Text="Elasticity" Hidden="true">
                                            <Editor>
                                                <ext:NumberField ID="NumberField8" runat="server" MinValue="0" MaxValue="2" />
                                            </Editor>
                                        </ext:NumberColumn>
                                        <ext:NumberColumn Format="0.00  " ID="NumColumn12" runat="server" DataIndex="PessimisticWeight" Text="Weight" Hidden="true">
                                            <Editor>
                                                <ext:NumberField ID="EDDPessimisticWeight" runat="server" MinValue="0" MaxValue="100" />
                                            </Editor>
                                        </ext:NumberColumn>
                                    </Columns>
                                </ext:Column>
                            </Columns>
                        </ColumnModel>
                        <Features>
                            <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                                <Filters>
                                    <ext:NumericFilter DataIndex="Year" />
                                    <ext:NumericFilter DataIndex="Normal" />
                                    <ext:NumericFilter DataIndex="Optimistic" />
                                    <ext:NumericFilter DataIndex="Pessimistic" />
                                </Filters>
                            </ext:GridFilters>
                        </Features>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" />
                        </SelectionModel>
                        <DirectEvents>
                            <BeforeEdit OnEvent="EDDWeight_BeforeEdit">
                                <EventMask ShowMask="true" Target="This" />
                                <ExtraParams>
                                    <ext:Parameter Name="Year" Value="e.record.data.Year" Mode="Raw" />
                                    <ext:Parameter Name="Driver" Value="#{ddlFilterEconomicDriver}.getValue()" Mode="Raw" Encode="true" />
                                </ExtraParams>
                            </BeforeEdit>
                        </DirectEvents>
                    </ext:GridPanel>
                    <ext:Window ID="winImport" runat="server" Title="Import Economic Driver Data" Hidden="true" Icon="ArrowLeft" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="frmImportFile" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                                <Items>
                                    <ext:Panel ID="pnlImportFile1" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:FileUploadField ID="fileImport" FieldLabel="File" runat="server" Icon="Attach" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnImportFile" runat="server" Text="Import" Icon="TableRefresh" Disabled="true" FormBind="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnImportFile_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="rowsValues" Value="#{grdEconomicDriverData}.getRowsValues(false)" Mode="Raw" Encode="true" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnCancelImport" runat="server" Icon="Decline" Text="Close">
                                        <DirectEvents>
                                            <Click OnEvent="btnCancelImport_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                                <BottomBar>
                                    <ext:StatusBar ID="FormImportStatusBar" runat="server" />
                                </BottomBar>
                                <Listeners>
                                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                            });
                                                            #{btnImportFile}.setDisabled(!valid);" />
                                </Listeners>
                            </ext:FormPanel>
                        </Items>
                    </ext:Window>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="FormStatusBar" runat="server" />
                </BottomBar>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>

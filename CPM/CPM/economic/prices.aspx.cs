﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Xml;
using CD;
using DataClass;
using System.Data.SqlClient;

namespace CPM
{
    public partial class _prices : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private DataReportObject ExportPrices { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"]; } }//  set { Session["idLanguage"] = value; } } }
        public Int32 IdProject { get { return (Int32)Session["IdProject"]; } set { Session["IdProject"] = value; } }
        public Int32 IdModelPrice { get { return (Int32)Session["IdModelPrice"]; } set { Session["IdModelPrice"] = value; } }
        public DataTable datViewData { get; set; }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private String strImportErrorMessageSpecial = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                IdModelPrice = 0;
                this.ddlCurrency.SelectedItem.Index = 0;
                this.ddlProduct.SelectedItem.Index = 0;
                this.ddlScenario.SelectedItem.Index = 0;
                //this.ModelPrecalcModuleAllocation();
                this.LoadLabel();
            }
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                String rowsValues = e.ExtraParams["rowsValues"];
                datViewData = JSON.Deserialize<DataTable>(rowsValues);
                this.Save();
                this.LoadPrices();
                string message = (String)GetGlobalResourceObject("CPM_Resources", "Records were successfully updated");

                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = message, IconCls = "icon-accept", Clear2 = false });
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void Structure_BeforeEdit(object sender, DirectEventArgs e)
        {
            //Int32 intIdProject = 0;
            //Int32 intClientYear = 0;
            //Int32 intIdField = 0;

            //if (Convert.ToInt32(e.ExtraParams["ClientYear"]) != 9999)
            //{
            //    if (ddlField.Value != null && ddlField.Value.ToString() != "")
            //    {
            //        intIdField = Convert.ToInt32(ddlField.Value);
            //    }

            //    intIdProject = getIdBLProject(intIdField);
            //    try
            //    {
            //        intClientYear = Convert.ToInt32(e.ExtraParams["ClientYear"]);
            //    }
            //    catch
            //    {
            //        intClientYear = 0;
            //    }

            //}
            //else
            //{
            //    //DataTable dt = new DataTable();
            //    //storeCboClient.DataSource = dt;
            //    //storeCboClient.DataBind();
            //    //storeCboZiff.DataSource = dt;
            //    //storeCboZiff.DataBind();
            //}
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.Prices);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    strImportErrorMessageSpecial = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Year"].ToString(), row["Field Code"].ToString(), row["Currency Code"].ToString(), row["Product"].ToString(), row["Scenario"].ToString(), Convert.ToDouble(row["Price"]), Convert.ToInt32(row["Offset"]));
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                //this.LoadProjects();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void storeFields_ReadData(object sender, StoreReadDataEventArgs e)
        {
            storeCurrency.RemoveAll();
            storeProduct.RemoveAll();
            storeScenario.RemoveAll();
            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dtField = new DataTable();
            dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblModelsPrices WHERE IdModel = " + IdModel + " GROUP BY IdField)", "ORDER BY Code, Name");

            DataRow drField = dtField.NewRow();
            drField["IdField"] = -1;
            drField["Name"] = " ";
            dtField.Rows.Add(drField);
            DataRow drField2 = dtField.NewRow();
            drField2["IdField"] = 0;
            drField2["Name"] = "- All -";
            dtField.Rows.Add(drField2);
            dtField.AcceptChanges();
            dtField.DefaultView.Sort = "IdField";
            DataTable dtFieldSort = dtField.DefaultView.ToTable();

            this.storeFields.DataSource = dtFieldSort;
            this.storeFields.DataBind();

            this.ddlField.Value = -1;
        }

        protected void StoreCurr_ReadData(object sender, StoreReadDataEventArgs e)
        {
            storeProduct.RemoveAll();
            storeScenario.RemoveAll();
            DataClass.clsCurrencies objCurrencies = new DataClass.clsCurrencies();
            DataTable dtCurrency = new DataTable();
            if (ddlField.Value.ToString() == "")
                dtCurrency = objCurrencies.LoadList("IdModel=" + IdModel + " AND IdCurrency IN (SELECT IdCurrency FROM tblModelsPrices WHERE IdModel = " + IdModel + " GROUP BY IdCurrency)", "ORDER BY Code, Name");
            else
                dtCurrency = objCurrencies.LoadList("IdModel=" + IdModel + " AND IdCurrency IN (SELECT IdCurrency FROM tblModelsPrices WHERE IdModel = " + IdModel + " AND IdField = " + ddlField.Value + "GROUP BY IdCurrency)", "ORDER BY Code, Name");

            DataRow dr = dtCurrency.NewRow();
            dr["IdCurrency"] = 0;
            dr["Name"] = " ";
            dtCurrency.Rows.Add(dr);
            dtCurrency.AcceptChanges();
            dtCurrency.DefaultView.Sort = "IdCurrency";
            DataTable dtCurrencySort = dtCurrency.DefaultView.ToTable();

            this.storeCurrency.DataSource = dtCurrencySort;
            this.storeCurrency.DataBind();
        }

        protected void StoreProduct_ReadData(object sender, StoreReadDataEventArgs e)
        {
            storeScenario.RemoveAll();
            DataClass.clsTechnicalDrivers objTechnicalDrivers = new DataClass.clsTechnicalDrivers();
            DataTable dtProduct = new DataTable();
            if (ddlField.Value.ToString() == "" || ddlCurrency.Value.ToString() == "")
                dtProduct = objTechnicalDrivers.LoadList(" IdModel = " + IdModel + " AND IdTechnicalDriver IN (SELECT IdTechnicalDriver FROM tblModelsPrices WHERE IdModel=" + IdModel + " AND IdField IN (SELECT IdField FROM tblModelsPrices WHERE IdModel= " + IdModel + " GROUP BY IdField) GROUP BY IdTechnicalDriver)", " ORDER BY Name ");
            else
                dtProduct = objTechnicalDrivers.LoadList(" IdModel = " + IdModel + " AND IdTechnicalDriver IN (SELECT IdTechnicalDriver FROM tblModelsPrices WHERE IdModel=" + IdModel + " AND IdField=" + ddlField.Value + " AND IdCurrency = " + ddlCurrency.Value + " GROUP BY IdTechnicalDriver)", " ORDER BY Name ");
            DataRow dr = dtProduct.NewRow();
            dr["IdTechnicalDriver"] = 0;
            dr["Name"] = " ";
            dtProduct.Rows.Add(dr);
            dtProduct.AcceptChanges();
            dtProduct.DefaultView.Sort = "IdTechnicalDriver";
            DataTable dtProductSort = dtProduct.DefaultView.ToTable();

            this.storeProduct.DataSource = dtProductSort;
            this.storeProduct.DataBind();
            this.ddlProduct.Value = 0;
        }

        protected void StoreScenario_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataTable dtScenario = new DataTable();
            if (ddlField.Value.ToString() == "" || ddlCurrency.Value.ToString() == "" || ddlProduct.Value.ToString() == "")
                dtScenario = CD.DAC.ConsultSQL("SELECT IdPriceScenario, ScenarioName FROM tblPriceScenarios WHERE IdModel = " + IdModel + " AND IdPriceScenario IN (SELECT IdPriceScenario FROM tblModelsPrices WHERE IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblModelsPrices WHERE IdModel= " + IdModel + " GROUP BY IdField)) GROUP BY IdPriceScenario,ScenarioName").Tables[0];
            else
                dtScenario = CD.DAC.ConsultSQL("SELECT IdPriceScenario, ScenarioName FROM tblPriceScenarios WHERE IdModel = " + IdModel + " AND IdPriceScenario IN (SELECT IdPriceScenario FROM tblModelsPrices WHERE IdModel = " + IdModel + " AND IdField = " + ddlField.Value + " AND IdCurrency = " + ddlCurrency.Value + " AND IdTechnicalDriver = " + ddlProduct.Value + ") GROUP BY IdPriceScenario,ScenarioName").Tables[0];

            DataRow dr = dtScenario.NewRow();
            dr["IdPriceScenario"] = 0;
            dr["ScenarioName"] = " ";
            dtScenario.Rows.Add(dr);
            dtScenario.AcceptChanges();
            dtScenario.DefaultView.Sort = "IdPriceScenario";
            DataTable dtScenarioSort = dtScenario.DefaultView.ToTable();

            this.storeScenario.DataSource = dtScenarioSort;
            this.storeScenario.DataBind();
            this.ddlScenario.Value = 0;
        }

        protected void ddlField_Select(object sender, DirectEventArgs e)
        {
            this.ddlCurrency.Reset();
            this.storeCurrency.Reload();
            this.ddlProduct.Reset();
            this.storeProduct.Reload();
            this.ddlScenario.Reset();
            this.storeScenario.Reload();
            this.storePrices.Reload();
        }

        protected void ddlCurrency_Select(object sender, DirectEventArgs e)
        {
            this.ddlProduct.Reset();
            this.storeProduct.Reload();
            this.ddlScenario.Reset();
            this.storeScenario.Reload();
            this.storePrices.Reload();
        }

        protected void ddlProduct_Select(object sender, DirectEventArgs e)
        {
            this.ddlScenario.Reset();
            this.storeScenario.Reload();
            this.storePrices.Reload();
        }

        protected void ddlScenario_Select(object sender, DirectEventArgs e)
        {
            if (ddlField.SelectedItem.Value != null && ddlField.SelectedItem.Text != null)
            {
                if (ddlCurrency.SelectedItem.Value != null && ddlCurrency.SelectedItem.Text != null)
                {
                    if (ddlProduct.SelectedItem.Value != null && ddlProduct.SelectedItem.Text != null)
                    {
                        if (ddlScenario.SelectedItem.Value != null && ddlScenario.SelectedItem.Text != null)
                        {
                            DataClass.clsPrices objPrices = new DataClass.clsPrices();
                            DataTable dtPrices = new DataTable();
                            dtPrices = objPrices.LoadList(" IdModel=" + IdModel + " AND IdField=" + ddlField.Value + " AND IdCurrency=" + ddlCurrency.Value + " AND IdTechnicalDriver=" + ddlProduct.Value + " AND IdPriceScenario=" + ddlScenario.Value, " ORDER BY PriceYear, IdField");

                            //Add the editor to the two column
                            sgrGrid.ColumnModel.Columns[6].Editor.Add(new Ext.Net.NumberField());
                            sgrGrid.ColumnModel.Columns[7].Editor.Add(new Ext.Net.NumberField());

                            storePrices.DataSource = dtPrices;
                            if (X.IsAjaxRequest)
                            {
                                sgrGrid.Reconfigure();
                            }
                            storePrices.DataBind();

                            ArrayList datColumnTD = new ArrayList();
                            datColumnTD.Add(new DataClass.DataTableColumnDisplay("PriceYear", "Year"));
                            datColumnTD.Add(new DataClass.DataTableColumnDisplay("FieldId", "Field Code"));
                            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Code", "Currency Code"));
                            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Description", "Product"));
                            datColumnTD.Add(new DataClass.DataTableColumnDisplay("ScenarioName", "Scenario"));
                            datColumnTD.Add(new DataClass.DataTableColumnDisplay("PriceValue", "Price"));
                            datColumnTD.Add(new DataClass.DataTableColumnDisplay("PriceOffset", "Offset"));
                            String strSelectedParameters = "";
                            String[] columnsToCopy = { "PriceYear", "FieldId", "Code", "Description", "ScenarioName", "PriceValue", "PriceOffset" };
                            System.Data.DataView view = new System.Data.DataView(dtPrices);
                            DataTable dtCloned = view.ToTable(true, columnsToCopy);

                            DataTable dtPricesExport = dtCloned.Clone();
                            dtPricesExport.Columns["FieldId"].DataType = typeof(String);
                            foreach (DataRow row in dtCloned.Rows)
                            {
                                dtPricesExport.ImportRow(row);
                            }
                            foreach (DataRow dr in dtPricesExport.Rows)
                            {
                                if (dr["FieldId"].ToString() == "0")
                                {
                                    dr["FieldId"] = "All";
                                }
                                else
                                {
                                    DataClass.clsFields objFields = new DataClass.clsFields();
                                    DataTable dt = objFields.LoadList("IdModel = " + IdModel + " AND IdField = " + Convert.ToInt32(dr["FieldId"].ToString()), "");

                                    if (dt.Rows.Count > 0)
                                    {
                                        dr["FieldId"] = dt.Rows[0]["Name"];
                                    }

                                }
                            }
                            ExportPrices = new DataReportObject(dtPricesExport, "DATASHEET", null, strSelectedParameters, null, "Code", datColumnTD, null);
                        }
                        else { return; }
                    }
                    else { return; }
                }
                else { return; }
            }
            else
            {
                //this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select Field please"), IconCls = "icon-exclamation", Clear2 = false });
                return;
            }
        }
        #endregion

        #region Methods

        protected void LoadPrices()
        {
            DataClass.clsPrices objPrices = new DataClass.clsPrices();
            DataTable dtPrices = new DataTable();
            dtPrices = objPrices.LoadList(" IdModel=" + IdModel + " AND IdField=" + ddlField.Value + " AND IdCurrency=" + ddlCurrency.Value + " AND IdTechnicalDriver=" + ddlProduct.Value + " AND IdPriceScenario=" + ddlScenario.Value, " ORDER BY PriceYear, IdField");

            //Add the editor to the two column
            sgrGrid.ColumnModel.Columns[6].Editor.Add(new Ext.Net.NumberField());
            sgrGrid.ColumnModel.Columns[7].Editor.Add(new Ext.Net.NumberField());

            storePrices.DataSource = dtPrices;
            if (X.IsAjaxRequest)
            {
                sgrGrid.Reconfigure();
            }
            storePrices.DataBind();

            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("PriceYear", "Year"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("FieldId", "Field Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Code", "Currency Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Description", "Product"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("ScenarioName", "Scenario"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("PriceValue", "Price"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("PriceOffset", "Offset"));
            String strSelectedParameters = "";
            String[] columnsToCopy = { "PriceYear", "FieldId", "Code", "Description", "ScenarioName", "PriceValue", "PriceOffset" };
            System.Data.DataView view = new System.Data.DataView(dtPrices);
            DataTable dtCloned = view.ToTable(true, columnsToCopy);

            DataTable dtPricesExport = dtCloned.Clone();
            dtPricesExport.Columns["FieldId"].DataType = typeof(String);
            foreach (DataRow row in dtCloned.Rows)
            {
                dtPricesExport.ImportRow(row);
            }
            foreach (DataRow dr in dtPricesExport.Rows)
            {
                if (dr["FieldId"].ToString() == "0")
                {
                    dr["FieldId"] = "All";
                }
                else
                {
                    DataClass.clsFields objFields = new DataClass.clsFields();
                    DataTable dt = objFields.LoadList("IdModel = " + IdModel + " AND IdField = " + Convert.ToInt32(dr["FieldId"].ToString()), "");

                    if (dt.Rows.Count > 0)
                    {
                        dr["FieldId"] = dt.Rows[0]["Name"];
                    }

                }
            }
            ExportPrices = new DataReportObject(dtPricesExport, "DATASHEET", null, strSelectedParameters, null, "Code", datColumnTD, null);
        }

        protected int getIdBLProject(Int32 pIdField)
        {
            DataClass.clsProjects objProjects = new DataClass.clsProjects();
            DataTable dt;

            dt = objProjects.LoadList("IdModel = " + IdModel + " AND IdField = " + pIdField + " AND Operation = 1", "ORDER BY IdProject");
            if (dt.Rows.Count > 0)
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
            else
            {
                return 0;
            }
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void Save()
        {
            foreach (DataRow row in datViewData.Rows)
            {
                DataClass.clsPrices objPrices = new DataClass.clsPrices();
                objPrices.IdModelPrice = GetIdModelPrice(Convert.ToInt32(row[2]), Convert.ToInt32(row[4]), Convert.ToInt32(row[7]), Convert.ToInt32(row[9]), Convert.ToInt32(row[6]));
                objPrices.loadObject();
                objPrices.IdModel = IdModel;
                objPrices.IdCurrency = Convert.ToInt32(row[2]);
                objPrices.IdField = Convert.ToInt32(row[4]);
                objPrices.PriceYear = Convert.ToInt32(row[6]);
                objPrices.IdTechnicalDriver = Convert.ToInt32(row[7]);
                objPrices.IdPriceScenario = Convert.ToInt32(row[9]);
                objPrices.PriceValue = Convert.ToInt32(row[10]);
                objPrices.PriceOffset = Convert.ToInt32(row[11]);
                objPrices.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.ModelUpdateTimestamp();
        }

        protected void SaveImport(String _Year, String _FieldCode, String _CurrencyCode, String _Product, String _Scenario, Double _Price, Int32 _Offset)
        {
            Int32 _IdField = GetIdField(_FieldCode);
            Int32 _IdCurrency = GetIdCurrency(_CurrencyCode);
            Int32 _IdTechnicalDriver = GetIdTechnicalDriver(_Product);
            Int32 _IdPriceScenario = GetIdScenarioName(_Scenario);

            //All Id's must have a value as well as _Year, _Price and _Offset
            if (!String.IsNullOrEmpty(_Year) && _IdField != -1 && _IdCurrency != 0 && _IdTechnicalDriver != 0 && _IdPriceScenario != 0 && _Price != 0 /*&& _Offset != 0*/)
            {
                DataClass.clsPrices objPrices = new DataClass.clsPrices();
                objPrices.IdModelPrice = (Int32)IdModelPrice;
                objPrices.loadObject();
                objPrices.IdModel = IdModel;
                objPrices.IdCurrency = _IdCurrency;
                objPrices.IdField = _IdField;
                objPrices.PriceYear = Convert.ToInt32(_Year);
                objPrices.IdTechnicalDriver = _IdTechnicalDriver;
                objPrices.IdPriceScenario = _IdPriceScenario;
                objPrices.PriceValue = _Price;
                objPrices.PriceOffset = _Offset;
                IdModelPrice = objPrices.Insert();
                objPrices = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected Int32 GetIdField(String _Field)
        {
            Int32 Value = -1;
            if (_Field == "All")
            {
                Value = 0;
            }
            else
            {
                DataClass.clsFields objFields = new DataClass.clsFields();
                DataTable dt = objFields.LoadList("Name = '" + _Field + "' AND IdModel = " + IdModel, "");

                if (dt.Rows.Count > 0)
                {
                    Value = (Int32)dt.Rows[0]["IdField"];
                }
            }
            return Value;
        }

        protected Int32 GetIdCurrency(String _Code)
        {
            Int32 Value = 0;
            DataClass.clsCurrencies objCurrencies = new DataClass.clsCurrencies();

            DataTable dt = objCurrencies.LoadList("Code = '" + _Code + "'", "ORDER BY IdCurrency");
            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdCurrency"];
            }
            return Value;
        }

        protected Int32 GetIdPeerCriteria(String _Criteria)
        {
            Int32 Value = 0;
            DataClass.clsPeerCriteria objPeerCriteria = new DataClass.clsPeerCriteria();
            DataTable dt = objPeerCriteria.LoadList("Description = '" + _Criteria + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdPeerCriteria"];
            }
            return Value;
        }

        protected Int32 GetIdScenarioName(String _Scenario)
        {
            Int32 Value = 0;
            DataClass.clsPriceScenarios objPriceScenarios = new DataClass.clsPriceScenarios();
            DataTable dt = objPriceScenarios.LoadList("UPPER(ScenarioName) = '" + _Scenario.ToUpper() + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdPriceScenario"];
            }
            return Value;
        }

        protected Int32 GetIdTechnicalDriver(String _Criteria)
        {
            Int32 Value = 0;
            DataClass.clsTechnicalDrivers objTechnicalDrivers = new DataClass.clsTechnicalDrivers();
            DataTable dt = objTechnicalDrivers.LoadList("Name = '" + _Criteria + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdTechnicalDriver"];
            }
            return Value;
        }

        protected Int32 GetIdModelPrice(Int32 _IdCurrency, Int32 _IdField, Int32 _IdTechnicalDriver, Int32 _IdPriceScenario, Int32 _PriceYear)
        {
            Int32 Value = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT IdModelPrice FROM vListPrices WHERE IdModel=" + IdModel + " AND IdCurrency=" + _IdCurrency + " AND IdField=" + _IdField + " AND IdTechnicalDriver=" + _IdTechnicalDriver + " AND IdPriceScenario=" + _IdPriceScenario + " AND PriceYear=" + _PriceYear).Tables[0];

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdModelPrice"];
            }
            return Value;
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExportTD = new DataReportObject();
            StoredExportTD = ExportPrices;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExportTD);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=4.1 ExportPrices.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        public String MessageConfirmDelete(String itemName)
        {
            String messageConfirmDeleteReturn = "";
            if (!String.IsNullOrEmpty(itemName))
            {
                messageConfirmDeleteReturn = (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + itemName + "?";
            }
            else
            {
                messageConfirmDeleteReturn = (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete all records?");
            }
            //messageConfirmDeleteReturn += "<br><br><b>This will also delete any related data from the following:</b><br>"
            //        + "<br> - Mapping (Ziff Cost Structure VS Client Cost Structure)"
            //        + "<br> - Peer criteria Data"
            //        + "<br> - Ziff Base Cost"
            //        + "<br> - Technical Assumptions"
            //        + "<br> - Economic Assumptions";
            return messageConfirmDeleteReturn;
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportPrices.xls", "template");
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        public void ModelPrecalcModuleAllocation()
        {
            clsModels objModelPrecalc = new clsModels();
            objModelPrecalc.IdModel = IdModel;
            objModelPrecalc.CheckRecalcModuleParameter(objApplication.MaxRelationLevels);
            objModelPrecalc.PrecalcModuleAllocation();
            objModelPrecalc = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = (String)GetGlobalResourceObject("CPM_Resources", "Prices");
            this.lblFilterField.Text = modMain.strTechnicalModuleField;
            this.lblCurrency.Text = (String)GetGlobalResourceObject("CPM_Resources", "Currency");
            this.lblProduct.Text = (String)GetGlobalResourceObject("CPM_Resources", "Product");
            this.lblScenario.Text = (String)GetGlobalResourceObject("CPM_Resources", "Scenario");
            this.btnSave.Text = modMain.strCommonSaveChanges;
            this.btnSave.ToolTip = modMain.strCommonSaveChanges;
            this.colPriceYear.Text = (String)GetGlobalResourceObject("CPM_Resources", "Year");
            this.winImport.Title = (String)GetGlobalResourceObject("CPM_Resources", "ImportPrices");
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.colPriceYear.Text = (String)GetGlobalResourceObject("CPM_Resources", "colPriceYear");
            this.colPriceValue.Text = (String)GetGlobalResourceObject("CPM_Resources", "colPriceValue");
            this.colPriceOffset.Text = (String)GetGlobalResourceObject("CPM_Resources", "colPriceOffset");
            this.colNetPrice.Text = (String)GetGlobalResourceObject("CPM_Resources", "colNetPrice");
        }

        #endregion

    }
}
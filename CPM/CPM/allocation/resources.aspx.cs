﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _resources : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdResources { get { return (Int32)Session["IdResources"]; } set { Session["IdResources"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private DataReportObject ExportResources { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;
        
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                IdResources = 0;
                this.LoadResource();
                this.LoadLabel();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winResourcesEdit.Show();
        }

        protected void btnDeleteAll_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete all records?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDeleteAllYES()",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                //Int32 intValue = Convert.ToInt32(ddlParents.SelectedItem.Value);
                this.Save();
                this.LoadResource();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Parent is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            this.winResourcesEdit.Hide();
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            this.winImport.Hide();
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.Resources);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Resource Code"].ToString(), row["Resource Desc"].ToString(), row["Activity Code"].ToString());
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadResource();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        protected void LoadResource()
        {
            DataClass.clsResources objResources = new DataClass.clsResources();

            DataTable dt = objResources.LoadList("IdModel = " + IdModel, "ORDER BY Name");

            storeResources.DataSource = dt;
            storeResources.DataBind();

            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Name", "Resource Desc"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Code", "Resource Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("ActivityCode", "Activity Code"));
            String strSelectedParameters = "";
            String[] columnsToCopy = { "Code", "Name", "ActivityCode" };
            System.Data.DataView view = new System.Data.DataView(dt);
            DataTable dtResources = view.ToTable(true, columnsToCopy);
            ExportResources = new DataReportObject(dtResources, "DATASHEET", null, strSelectedParameters, null, "Name", datColumnTD, null);
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportResources;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=1.5 ExportResources.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        protected void Clear()
        {
            this.IdResources = 0;
            this.txtName.Text = "";
            this.txtName.Reset();
            this.txtCode.Text = "";
            this.txtCode.Reset();
            this.ddlActivity.Reset();
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void grdResources_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            IdResources = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            if (Command == "Edit")
            {
                this.winResourcesEdit.Show();
                this.EditResourceLoad();
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + _Name + "?", new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedDeleteYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
        }

        private Int32? GetIdActivity(String _Activity)
        {
            Int32? Value = null;// 0;
            DataClass.clsActivities objActivities = new DataClass.clsActivities();
            DataTable dt = objActivities.LoadList("Code = '" + _Activity + "' AND IdModel = " + IdModel, "");
            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdActivity"];
            }

            return Value;
        }

        protected void Save()
        {
            DataClass.clsResources objAggregationLevels = new DataClass.clsResources();
            objAggregationLevels.IdResources = (Int32)IdResources;
            objAggregationLevels.loadObject();

            if (IdResources == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (objAggregationLevels.Code != txtCode.Text)
                {
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }
            objAggregationLevels.Code = txtCode.Text;
            objAggregationLevels.Name = txtName.Text;
            objAggregationLevels.IdModel = IdModel;
            if (Convert.ToInt32(this.ddlActivity.Value) == 0)
            {
                objAggregationLevels.IdActivity = null;
            }
            else
            {
                objAggregationLevels.IdActivity = this.ddlActivity.Value;
            }
            objAggregationLevels.Parent = null;
            if (IdResources == 0)
            {
                objAggregationLevels.DateCreation = DateTime.Now;
                objAggregationLevels.UserCreation = (Int32)IdUserCreate;
                IdResources = objAggregationLevels.Insert();
            }
            else
            {
                objAggregationLevels.DateModification = DateTime.Now;
                objAggregationLevels.UserModification = (Int32)IdUserCreate;
                objAggregationLevels.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winResourcesEdit.Hide();
            this.ModelUpdateTimestamp();
        }

        protected void SaveImport(String _Code, String _Name, String _Activity)
        {
            //Int32 _IdActivity = GetIdActivity(_Activity);
            Int32? _IdActivity = null;//-1;
            if (String.IsNullOrEmpty(_Activity))
            {
                _IdActivity = null;// 0;
            }
            else
            {
                _IdActivity = GetIdActivity(_Activity);
            }

            //if (!String.IsNullOrEmpty(_Code) && !String.IsNullOrEmpty(_Name) && _IdActivity != 0)
            if (!String.IsNullOrEmpty(_Code) && !String.IsNullOrEmpty(_Name) && _IdActivity != -1)
            {
                DataClass.clsResources objResources = new DataClass.clsResources();
                objResources.IdResources = (Int32)IdResources;
                objResources.loadObject();
                objResources.Code = _Code;
                objResources.Name = _Name;
                objResources.IdModel = IdModel;
                objResources.IdActivity = _IdActivity;
                objResources.DateCreation = DateTime.Now;
                objResources.UserCreation = (Int32)IdUserCreate;
                IdResources = objResources.Insert();
                objResources = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected void EditResourceLoad()
        {
            DataClass.clsResources objResources = new DataClass.clsResources();
            DataTable dt = objResources.LoadList("IdResources = " + IdResources, "");
            if (dt.Rows.Count > 0)
            {
                this.txtCode.Text = (String)dt.Rows[0]["Code"];
                this.txtName.Text = (String)dt.Rows[0]["Name"];
                this.ddlActivity.Value = (!DBNull.Value.Equals( dt.Rows[0]["IdActivity"])) ? Convert.ToInt32(dt.Rows[0]["IdActivity"]) :0;
            }
        }

        protected void StoreAct_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsActivities objActivities = new DataClass.clsActivities();
//            this.stroreActivity.DataSource = objActivities.LoadList("IdModel = " + IdModel, "ORDER BY Name");
            this.stroreActivity.DataSource = objActivities.LoadListWithGeneral("IdModel = " + IdModel, "ORDER BY Name");
            this.stroreActivity.DataBind();
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsResources objResources = new DataClass.clsResources();
            DataTable dt = objResources.LoadList("IdModel = " + IdModel + " AND Code = '" + txtCode.Text + "'", "");
            Value = dt.Rows.Count;

            return Value;
        }

        public void Delete()
        {
            DataClass.clsResources objAggregationLevels = new DataClass.clsResources();
            objAggregationLevels.IdResources = (Int32)IdResources;
            objAggregationLevels.Delete();
            this.ModelUpdateTimestamp();
        }

        public void DeleteAll()
        {
            DataClass.clsResources objAggregationLevels = new DataClass.clsResources();
            objAggregationLevels.IdModel = (Int32)IdModel;
            objAggregationLevels.DeleteAll();
            this.ModelUpdateTimestamp();
        }

        [DirectMethod]
        public void ClickedDeleteYES()
        {
            this.Delete();
            this.LoadResource();
        }

        [DirectMethod]
        public void ClickedDeleteAllYES()
        {
            this.DeleteAll();
            this.LoadResource();
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportResources.xls", "template");
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true,false, true, false, false);
            objModelUpdate = null;
        }

        protected void LoadLabel()
        {
            this.grdResources.Title = modMain.strMasterResources;
            this.btnNew.Text = (String)GetGlobalResourceObject("CPM_Resources", "Add New Resource");
            this.btnNew.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Add New Resource");
            this.btnDelete.Text = modMain.strCommonDeleteAll;
            this.btnDelete.ToolTip = modMain.strCommonDeleteAll;
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.colCode.Text = modMain.strCommonCode;
            this.colName.Text = modMain.strCommonName;
            this.colActivityName.Text = (String)GetGlobalResourceObject("CPM_Resources", "Activity");
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = "&nbsp;" + modMain.strCommonEdit;
            this.ImageCommandColumn1.Commands[1].Text = "&nbsp;" + modMain.strCommonDelete;
            this.winResourcesEdit.Title = modMain.strMasterResources;
            this.txtCode.FieldLabel = modMain.strCommonCode;
            this.txtName.FieldLabel = modMain.strCommonName;
            this.ddlActivity.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Activity");
            this.btnSave.Text = modMain.strCommonSave;
            this.btnSave.ToolTip = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.btnCancel.ToolTip = modMain.strCommonClose;
            this.winImport.Title = modMain.strCommonImport + " " + modMain.strMasterResources;
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnImportFile.ToolTip = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.btnCancelImport.ToolTip = modMain.strCommonClose;
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        #endregion

    }
}
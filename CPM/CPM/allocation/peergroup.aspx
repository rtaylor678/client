﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="peergroup.aspx.cs" Inherits="CPM._peergroup" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function applyPageSize(grid) {
            var headerHeight = 22;
            var rowHeight = 21;

            var gridTopOffset = 100;
            var gridBottomOffset = 50;
            var rowHeight = 20;
            var myHeight = window.innerHeight;

            grid.store.pageSize = Math.round(((myHeight - gridTopOffset - gridBottomOffset) / rowHeight)); // - 3);
            grid.store.load();
        }
    </script>
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" Layout="FitLayout" AutoScroll="true" runat="server">
                <Items>
                    <ext:GridPanel ID="grdPeerGroup" AutoScroll="true" runat="server" Border="false" Title="List of Peer Group" IconCls="icon-listpeergroup_16">
                        <TopBar>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btnAdd" Icon="Add" ToolTip="Add New Peer Criteria" runat="server" Text="Add New Peer Criteria">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarSeparator />
                                    <ext:Button ID="btnImport" Icon="BookAdd" runat="server" Text="Import">
                                        <DirectEvents>
                                            <Click OnEvent="btnImport_Click">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarFill />
                                    <ext:Button ID="btnSaveExcel" runat="server" Text="Export Data To Excel" Icon="PageExcel">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveExcel_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnDownloadTemplate" runat="server" Text="Download Import Template" Icon="PageSave">
                                        <DirectEvents>
                                            <Click OnEvent="btnDownloadTemplate_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="storePeerGroup" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="IdActivity">
                                        <Fields>
                                            <ext:ModelField Name="IdPeerGroup" Type="Int" />
                                            <ext:ModelField Name="PeerGroupCode" Type="String" />
                                            <ext:ModelField Name="PeerGroupDescription" Type="String" />
                                            <ext:ModelField Name="FullBenchmark" Type="Boolean" />
                                            <ext:ModelField Name="Fields" Type="String" />
                                            <ext:ModelField Name="Comments" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel ID="ColumnModel1" runat="server">
                            <Columns>
                                <ext:Column Hidden="true" ID="colIdPeerGroup" runat="server" Text="ID" DataIndex="IdPeerGroup"></ext:Column>
                                <ext:Column ID="colPeerGroupCode" runat="server" Text="Peer Group Code" DataIndex="PeerGroupCode" Flex="1"></ext:Column>
                                <ext:Column ID="colPeerGroupDescription" runat="server" Text="Peer Group Description" DataIndex="PeerGroupDescription" Flex="1"></ext:Column>
                                <ext:Column ID="colFields" runat="server" Text="Fileds" DataIndex="Fields" Flex="1"></ext:Column>
                                <ext:ComponentColumn ID="colFullBenchmark" runat="server" Editor="true" Align="Center" DataIndex="FullBenchmark" Flex="1" Text="Full Benchmark">
                                    <Component>
                                        <ext:Checkbox ID="Checkbox1" Checked="true" runat="server" ReadOnly="True" />
                                    </Component>
                                </ext:ComponentColumn>
                                <ext:Column ID="colComments" runat="server" Text="Comments" DataIndex="Comments" Flex="1"></ext:Column>
                                <ext:ImageCommandColumn Align="Center" Width="150" Text="Functions" ID="ImageCommandColumn1" runat="server">
                                    <Commands>
                                        <ext:ImageCommand CommandName="Edit" IconCls="icon-edit_16" Text="&nbsp;Edit" />
                                    </Commands>
                                    <Commands>
                                        <ext:ImageCommand CommandName="Delete" IconCls="icon-delete_16" Text="&nbsp;Delete" />
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="grdPeerGroup_Command">
                                            <ExtraParams>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="Id" Value="record.data.IdPeerGroup" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="Name" Value="record.data.PeerGroupCode" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:ImageCommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" />
                        </SelectionModel>
                        <Features>
                            <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                                <Filters>
                                    <ext:StringFilter DataIndex="PeerGroupCode" />
                                </Filters>
                            </ext:GridFilters>
                        </Features>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="true">
                            </ext:PagingToolbar>
                        </BottomBar>
                        <Listeners>
                            <Resize Handler="applyPageSize(this)" Buffer="250" />            
                        </Listeners>
                    </ext:GridPanel>
                    <ext:Window ID="winPeerGroupEdit" runat="server" Title="Peer Group" Hidden="true" IconCls="icon-listpeercriteria_16" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="pnlEditPeerGroup" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                                <Items>
                                    <ext:Panel ID="Panel1" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:TextField ID="txtPeerGroupCode" runat="server" FieldLabel="Peer Group Code" AnchorHorizontal="92%" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel2" runat="server" Border="false" Layout="Form" ColumnWidth="1" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:TextField ID="txtPeerGroupDescription" runat="server" FieldLabel="Peer Group Description" AnchorHorizontal="92%" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel  ID="Panel3" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel" ColSpan=2>
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:Checkbox ID="chkFullBenchmark" runat="server" FieldLabel="Full Benchmark" Cls="PopupFormField" ReadOnly="False">
                                            </ext:Checkbox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel4" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:TextField ID="txtComments" runat="server" FieldLabel="Comments" AnchorHorizontal="92%" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                    <ext:FieldSet ID="FieldSet1" runat="server" Title="Select Fields" ColumnWidth="1" Layout="AnchorLayout" DefaultAnchor="100%" Cls="PopupFormColumnPanelGrid">
                                        <Items>
                                            <ext:GridPanel ID="grdField" runat="server" Height="168">
                                                <Store>
                                                    <ext:Store ID="storeField" OnReadData="StoreField_ReadData" runat="server">
                                                        <Model>
                                                            <ext:Model ID="modelField" runat="server">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdField" Type="Int" />
                                                                    <ext:ModelField Name="IdPeerGroupField" Type="Int" />
                                                                    <ext:ModelField Name="Code" Type="String" />
                                                                    <ext:ModelField Name="Name" Type="String" />
                                                                    <ext:ModelField Name="Selected" Type="Boolean" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel ID="cmRoles" runat="server">
                                                    <Columns>
                                                        <ext:Column Hidden="true" ID="colIdField" runat="server" Text="ID Field" DataIndex="IdField">
                                                        </ext:Column>
                                                        <ext:Column Hidden="true" ID="colIdPeerGroupField" runat="server" Text="ID Peer Group" DataIndex="IdPeerGroupField">
                                                        </ext:Column>
                                                        <ext:Column ID="colCode" runat="server" Text="Code" DataIndex="Code">
                                                        </ext:Column>
                                                        <ext:Column ID="colName" runat="server" Text="Name" DataIndex="Name" Flex="1">
                                                        </ext:Column>
                                                        <ext:ComponentColumn ID="chkSelected" runat="server" Editor="true" Align="Center" DataIndex="Selected" Flex="1" Text="Select">
                                                            <Component>
                                                                <ext:Checkbox ID="Checkbox2" Checked="true" runat="server" />
                                                            </Component>
                                                        </ext:ComponentColumn>
                                                    </Columns>
                                                </ColumnModel>
                                                <Features>
                                                    <ext:GridFilters runat="server" ID="GridFilters2" Local="true">
                                                        <Filters>
                                                            <ext:StringFilter DataIndex="Code" />
                                                            <ext:StringFilter DataIndex="Name" />
                                                            <ext:NumericFilter DataIndex="IdField" />
                                                            <ext:BooleanFilter DataIndex="Selected" />
                                                        </Filters>
                                                    </ext:GridFilters>
                                                </Features>
                                                <SelectionModel>
                                                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" />
                                                </SelectionModel>
                                                <Plugins>
                                                    <ext:CellEditing ID="CellEditing1" runat="server">
                                                    </ext:CellEditing>
                                                </Plugins>
                                                <View>
                                                    <ext:GridView MarkDirty="false" />
                                                </View>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:FieldSet>
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnSave" runat="server" Text="Save" Icon="DatabaseSave" Disabled="true" FormBind="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnSave_Click">
                                                <ExtraParams>
                                                    <ext:Parameter Name="rowsValues" Value="#{grdField}.getRowsValues(false)" Mode="Raw" Encode="true" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnCancel" runat="server" Icon="Decline" Text="Close">
                                        <DirectEvents>
                                            <Click OnEvent="btnCancel_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                                <BottomBar>
                                    <ext:StatusBar ID="FormStatusBar" runat="server" />
                                </BottomBar>
                                <Listeners>
                                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                            });
                                            #{btnSave}.setDisabled(!valid);" />
                                </Listeners>
                            </ext:FormPanel>
                        </Items>
                    </ext:Window>
                    <ext:Window ID="winImport" runat="server" Title="Import Peer Group" Hidden="true" Icon="ArrowLeft" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="frmImportFile" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                                <Items>
                                    <ext:Panel ID="pnlImportFile1" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:FileUploadField ID="fileImport" FieldLabel="File" runat="server" Icon="Attach" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnImportFile" runat="server" Text="Import" Icon="TableRefresh" Disabled="true" FormBind="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnImportFile_Click" >
                                                <EventMask ShowMask="true" Target="CustomTarget" CustomTarget="vpMain" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnCancelImport" runat="server" Icon="Decline" Text="Close">
                                        <DirectEvents>
                                            <Click OnEvent="btnCancelImport_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                                <BottomBar>
                                    <ext:StatusBar ID="FormImportStatusBar" runat="server" />
                                </BottomBar>
                                <Listeners>
                                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                            });
                                                            #{btnImportFile}.setDisabled(!valid);" />
                                </Listeners>
                            </ext:FormPanel>
                        </Items>
                    </ext:Window>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="statusDialogs" runat="server" />
                </BottomBar>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>
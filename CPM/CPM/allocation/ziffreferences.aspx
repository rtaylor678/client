﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ziffreferences.aspx.cs" Inherits="CPM._ziffreferences" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_iconxl.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Border="false" Layout="FormLayout" OverflowY="Scroll">
        <Items>
            <ext:Panel ID="pnlZiffReferences" runat="server" Title="Ziff/Third-party Cost References" IconCls="icon-ziffreferences_16" BodyPadding="10" Border="false" Layout="Column">
                <Items>
                    <ext:FieldSet ID="FieldSet1" runat="server" Flex="1" Cls="buttongroup-border" Layout="AnchorLayout" ColumnWidth="1">
                        <Defaults>
                            <ext:Parameter Name="HideEmptyLabel" Value="false" Mode="Raw" />
                        </Defaults>
                        <Items>
                            <ext:Button ID="btnListOfPeerGroup" runat="server" Disabled="true" Cls="icon-listpeercriteria_48" Text="List of Peer Group" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnListOfPeerGroup_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnZiffListOfCriteria" runat="server" Disabled="true" Cls="icon-listpeercriteria_48" Text="List of Peer Criteria" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnZiffListOfCriteria_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnZiffPeerCriteriaData" runat="server" Disabled="true" Cls="icon-peercriteriadata_48" Text="Peer Criteria Data" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnZiffPeerCriteriaData_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnZiffBaseCost" runat="server" Disabled="true" Cls="icon-ziffbasecost_48" Text="External Base Cost References" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnZiffBaseCost_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnExternalBenchmarkRepository" runat="server" Disabled="true" Cls="icon-zifftechnicalanalysis_48" Text="External Benchmarks Files Repository" IconAlign="Top" Scale="Large" Visible="false">
                                <DirectEvents>
                                    <Click OnEvent="btnExternalBenchmarkRepository_Click" />
                                </DirectEvents>
                            </ext:Button>

                        </Items>
                    </ext:FieldSet>
                </Items>
            </ext:Panel>
        </Items>
    </ext:Viewport>
</body>
</html>
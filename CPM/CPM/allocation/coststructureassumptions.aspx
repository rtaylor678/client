﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="coststructureassumptions.aspx.cs" Inherits="CPM._coststructureassumptions" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var formatZero = function (value, metaData, record, rowIndex, colIndex, store, view) {
            var str = record.data.Code;
            var pos = str.indexOf("_");
            if (record.data.Client == 9999) {
                metaData.tdAttr = 'style="background-color:LightGray;font-weight:bold;"';
                return "";
            }
            if (value == 0) {
                return "- N/A -";
            }
            else if (value == 9999) {
                return "";
            }
            return value;
        }

        var formatCol = function (value, metaData, record, rowIndex, colIndex, store, view) {
            var str = record.data.Code;
            var pos = str.indexOf("_");
            if (record.data.Client == 9999) {
                metaData.tdAttr = 'style="background-color:LightGray;font-weight:bold;"';
                return value;
            }
            return value;
        }

        var beforeCellEditHandler = function (e) {
            if (e.originalValue == "9999") {
                App.CellEditing1.cancelEdit(); 
                return false;
            }
        }

        var cellEditingHandler = function (e) {
            var cancel = false;
            if (!Ext.isEmpty(e.value)) {
                for (var item in e.record.data) {
                    switch (item) {
                        case "IdModel":
                        case "IdField":
                        case "IdZiffAccount":
                        case "Code":
                        case "Client":
                        case "Name":
                            break;
                        default:
                            if (e.colIdx > 2) {
                                if (e.value <= e.record.data["Client"] || e.value == e.record.data[item]) {
                                    Ext.Msg.alert('Error!!!', 'Years must be different. (Años deben ser diferente.)');
                                    cancel = true;
                                }
                            }
                    }
                    var x = e.record.data[item];
                }
            }

            e.cancel = cancel;
        }

        function applyPageSize(grid) {
            var headerHeight = 22;
            var rowHeight = 21;

            var gridTopOffset = 100;
            var gridBottomOffset = 50;
            var rowHeight = 20;
            var myHeight = window.innerHeight;

            grid.store.pageSize = Math.round(((myHeight - gridTopOffset - gridBottomOffset) / rowHeight)-3); // - 3);
            grid.store.load();
        }
    </script>
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" AutoScroll="true" runat="server" Title="Cost Structure Assumptions" IconCls="icon-coststructure_16" Border="false" Layout="BorderLayout">
                <Items>
                    <ext:Panel ID="Panel4" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0" region="North">
                        <Items>
                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lblFilterField" runat="server" Text="Field:"></ext:Label>
                            <ext:ComboBox Style="margin: 10px 10px 10px 10px" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" QueryMode="Remote" ID="ddlField" runat="server" DisplayField="Name" ValueField="IdField" Width="200">
                                <ListConfig LoadingText="Searching..." MinWidth="300">
                                    <ItemTpl ID="ItemTpl3" runat="server">
                                        <Html>
                                            <div class="search-item">
							                    <h3>{Name}</h3>
							                    <span>Code: {Code}</span>
						                    </div>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Store>
                                    <ext:Store ID="storeFields" runat="server" OnReadData="storeFields_ReadData">
                                        <Model>
                                            <ext:Model ID="Model3" runat="server" IDProperty="IdField">
                                                <Fields>
                                                    <ext:ModelField Name="IdField" />
                                                    <ext:ModelField Name="Code" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                                <Reader>
                                                    <ext:JsonReader />
                                                </Reader>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Select OnEvent="ddlField_Select">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>
                            </ext:ComboBox>
                        </Items>
                    </ext:Panel>
                    <ext:GridPanel ID="grdCostStructureAssumptionsProj" runat="server" Border="false" Style="width: 100%" Region="Center">
                        <TopBar>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btnSave" Icon="DatabaseSave" ToolTip="Save Changes" runat="server" Text="Save Changes">
                                        <DirectEvents>
                                            <Click OnEvent="btnSave_Click">
                                                <ExtraParams>
                                                    <ext:Parameter Name="rowsValues" Value="#{grdCostStructureAssumptionsProj}.getRowsValues(false)" Mode="Raw" Encode="true" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarSeparator />
                                    <ext:Button ID="btnImport" Icon="BookAdd" TextAlign="Center" ToolTip="Import Cost Structure Assumptions" runat="server" Text="Import">
                                        <DirectEvents>
                                            <Click OnEvent="btnImport_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="rowsValues" Value="#{grdCostStructureAssumptionsProj}.getRowsValues(false)"
                                                        Mode="Raw" Encode="true" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarFill />
                                    <ext:Button ID="btnSaveExcel" runat="server" Text="Export Data To Excel" Icon="PageExcel">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveExcel_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnDownloadTemplate" runat="server" Text="Download Import Template" Icon="PageSave">
                                        <DirectEvents>
                                            <Click OnEvent="btnDownloadTemplate_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="storeCostStructureAssumptionsProj" runat="server">
                                <Model>
                                    <ext:Model ID="Model4" runat="server">
                                        <Fields>
<%-- 
                                            <ext:ModelField Name="SeqOrder" Type="Int" />
                                            <ext:ModelField Name="IdCostStructureAssumptions" />
                                            <ext:ModelField Name="IdZiffAccount" />
                                            <ext:ModelField Name="Code" />
                                            <ext:ModelField Name="Name" />
                                            <ext:ModelField Name="ParentCode" />
                                            <ext:ModelField Name="ParentName" />
                                            <ext:ModelField Name="ProjectName" />
                                            <ext:ModelField Name="Client" Type="String" />
                                            <ext:ModelField Name="Ziff" Type="String" />
                                            <ext:ModelField Name="SortOrder" Type="String" />
--%>
                                            <ext:ModelField Name="IdZiffAccount" Type="Int" />
                                            <ext:ModelField Name="Code" Type="String" />
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="Client" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
<%--
                                <Sorters>
                                    <ext:DataSorter Property="SeqOrder" Direction="ASC" />
                                </Sorters>
--%>
                          </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
<%--
                                <ext:Column Hidden="true" ID="Column20" runat="server" Text="" DataIndex="SeqOrder" Width ="25" Sortable="false" />
                                <ext:Column Hidden="false" ID="Column9" runat="server" Text="Code" DataIndex="Code" Flex="1" Style="width: 25%" Sortable="false" />
                                <ext:Column Hidden="false" ID="Column11" runat="server" Text="Name" DataIndex="Name" Flex="1" Style="width: 25%" Sortable="false" />
                                <ext:Column ID="Column15" runat="server" Text="Case" DataIndex="ProjectName" Flex="1" Style="width: 25%" Sortable="false" />
                                <ext:Column ID="Column2" runat="server" Text="Company" DataIndex="Client" Flex="1" Align="Right" Style="width: 25%" Sortable="false" >
                                    <Renderer Fn="formatZero"></Renderer>
                                    <Editor>
                                        <ext:ComboBox ID="cboClient" runat="server" StyleSpec="text-align:right" Editable="false" QueryMode="Default" DisplayField="Text" ValueField="Value">
                                            <Store>
                                                <ext:Store ID="storeCboClient" runat="server">
                                                    <Model>
                                                        <ext:Model ID="modelStoreCboClient" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="Value" />
                                                                <ext:ModelField Name="Text" />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                </ext:Store>
                                            </Store>
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column4" runat="server" Text="Ziff" DataIndex="Ziff" Flex="1" Align="Right" Style="width: 25%" Sortable="false" >
                                <Renderer Fn="formatZero"></Renderer>
                                    <Editor>
                                        <ext:ComboBox ID="cboZiff" runat="server" StyleSpec="text-align:right" Editable="false" QueryMode="Default" DisplayField="Text" ValueField="Value">
                                            <Store>
                                                <ext:Store ID="storeCboZiff" runat="server">
                                                    <Model>
                                                        <ext:Model ID="model2" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="Value" />
                                                                <ext:ModelField Name="Text" />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                </ext:Store>
                                            </Store>
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
--%>
                                <ext:Column Hidden="true" ID="colIdZiffAccount" runat="server" Text="" DataIndex="IdZiffAccount" />
                                <ext:Column ID="colCode" runat="server" Text="Code" DataIndex="Code" Flex="1" Style="width: 25%" Sortable="false" >
                                    <Renderer Fn="formatCol"></Renderer>
                                </ext:Column>
                                <ext:Column ID="colName" runat="server" Text="Name" DataIndex="Name" Flex="1" Style="width: 25%" Sortable="false" >
                                    <Renderer Fn="formatCol"></Renderer>
                                </ext:Column>
                                <ext:Column ID="colClient" runat="server" Text="Client" DataIndex="Client" Flex="1" Align="Right" Style="width: 25%" Sortable="false" >
                                    <Renderer Fn="formatZero"></Renderer>
                                    <Editor>
                                        <ext:ComboBox ID="cboClient" runat="server" StyleSpec="text-align:right" Editable="false" QueryMode="Default" DisplayField="Text" ValueField="Value">
                                            <Store>
                                                <ext:Store ID="storeCboClient" runat="server">
                                                    <Model>
                                                        <ext:Model ID="modelStoreCboClient" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="Value" />
                                                                <ext:ModelField Name="Text" />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                </ext:Store>
                                            </Store>
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                            </Columns>
                        </ColumnModel>
                        <View>
                            <ext:GridView ID="GridView1" runat="server">
                                <GetRowClass Handler="return 'x-grid-row-expanded';" />
                            </ext:GridView>
                        </View>
                        <Plugins>
                            <ext:CellEditing ID="CellEditing1" runat="server">
                                <Listeners>
                                    <BeforeEdit Handler="return beforeCellEditHandler(e)"></BeforeEdit>
                                    <ValidateEdit Handler="cellEditingHandler(e);"></ValidateEdit>
                                </Listeners>
                            </ext:CellEditing>
                        </Plugins>
<%-- 
                        <Features>
                            <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                                <Filters>
                                    <ext:StringFilter DataIndex="Name" />
                                    <ext:StringFilter DataIndex="Code" />
                                    <ext:StringFilter DataIndex="ProjectName" />
                                    <ext:StringFilter DataIndex="Client" />
                                    <ext:StringFilter DataIndex="Ziff" />
                                </Filters>
                            </ext:GridFilters>
                        </Features>
--%>
                        <DirectEvents>
                            <BeforeEdit OnEvent="Structure_BeforeEdit">
                                <EventMask ShowMask="true" Target="This">
                                </EventMask>
                            </BeforeEdit>
                        </DirectEvents>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" />
                        </SelectionModel>
<%--                       <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="true" EmptyMsg="No records to display">
                            </ext:PagingToolbar>
                        </BottomBar>
                        <Listeners>
                            <Resize Handler="applyPageSize(this)" Buffer="250" />            
                        </Listeners>--%>
                    </ext:GridPanel>
                    <ext:Window ID="winImport" runat="server" Title="Import Cost Structure Assumptions" Hidden="true" Icon="ArrowLeft" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="frmImportFile" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                                <Items>
                                    <ext:Panel ID="pnlImportFile1" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:FileUploadField ID="fileImport" FieldLabel="File" runat="server" Icon="Attach" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnImportFile" runat="server" Text="Import" Icon="TableRefresh" Disabled="true" FormBind="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnImportFile_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="rowsValues" Value="#{grdCostStructureAssumptionsProj}.getRowsValues(false)" Mode="Raw" Encode="true" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnCancelImport" runat="server" Icon="Decline" Text="Close">
                                        <DirectEvents>
                                            <Click OnEvent="btnCancelImport_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                                <BottomBar>
                                    <ext:StatusBar ID="FormImportStatusBar" runat="server" />
                                </BottomBar>
                                <Listeners>
                                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                                            });
                                                                            #{btnImportFile}.setDisabled(!valid);" />
                                </Listeners>
                            </ext:FormPanel>
                        </Items>
                    </ext:Window>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="FormStatusBar" BusyText="I'm loading"  runat="server" />
                </BottomBar>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>
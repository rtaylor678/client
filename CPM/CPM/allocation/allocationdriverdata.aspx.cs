﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _allocationdriverdata : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdAllocationDriverData { get { return (Int32)Session["IdAllocationDriverData"]; } set { Session["IdAllocationDriverData"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private DataReportObject ExportAllocationDriverData { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                this.LoadLabel();
                IdAllocationDriverData = 0;
                this.LoadAllocationListDriver();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winAllocationDriverDataEdit.Show();
        }

        protected void btnDeleteAll_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete all records?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDeleteAllYES()",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.Save();
                this.LoadAllocationListDriver();
            }
            catch (Exception ex)
            {
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving") + " " + ex.Message, IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            winAllocationDriverDataEdit.Hide();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.AllocationDriverData);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Driver Desc"].ToString(), row["Cost Object Code"].ToString());
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadAllocationListDriver();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        protected void Clear()
        {
            this.IdAllocationDriverData = 0;
            this.ddlCostCenter.Reset();
            this.ddlDriver.Reset();
            this.storeCostCenter.Reload();
            this.ddlDriver.ReadOnly = true;
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void grdAllocationDriverData_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            IdAllocationDriverData = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            if (Command == "Edit")
            {
                this.winAllocationDriverDataEdit.Show();
                this.EditAllocationListDriverLoad();
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + _Name + "?", new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedDeleteYES('" + _Name + "')",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }      
        }

        protected void Save()
        {
            DataClass.clsAllocationDriversData objAllocationDriversData = new DataClass.clsAllocationDriversData();

            IdAllocationDriverData = IdAllocationDriverData;

            objAllocationDriversData.IdAllocationDriverData = IdAllocationDriverData;
            objAllocationDriversData.loadObject();

            objAllocationDriversData.IdClientCostCenter = Convert.ToInt32(ddlCostCenter.Value);
            objAllocationDriversData.IdAllocationListDriver = Convert.ToInt32(ddlDriver.Value);

            if (IdAllocationDriverData == 0)
            {
                objAllocationDriversData.DateCreation = DateTime.Now;
                objAllocationDriversData.UserCreation = (Int32)IdUserCreate;
                IdAllocationDriverData = objAllocationDriversData.Insert();
            }
            else
            {
                objAllocationDriversData.DateModification = DateTime.Now;
                objAllocationDriversData.UserModification = (Int32)IdUserCreate;
                objAllocationDriversData.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winAllocationDriverDataEdit.Hide();
            this.ModelUpdateTimestamp();
        }

        protected Int32 LoadIdUnit(String _Unit)
        {
            Int32 IdUnit = 0;
            DataClass.clsUnit objUnit = new DataClass.clsUnit();
            DataTable dt = objUnit.LoadList("Name LIKE '%" + _Unit + "%'", "ORDER BY Name");    

            if (dt.Rows.Count>0)
            {
                IdUnit = (Int32)dt.Rows[0]["IdUnit"];
            }
            return IdUnit;
        }

        protected void SaveImport(String _Driver, String _CostCenter)
        {
            Int32 _IdDriver = GetIdDriver(_Driver);
            Int32 _IdCostCenter = GetIdCostCenter(_CostCenter);

            if (_IdDriver != 0 && _IdCostCenter != 0)
            {
                DataClass.clsAllocationDriversData objAllocationDriversData = new DataClass.clsAllocationDriversData();
                objAllocationDriversData.IdAllocationDriverData = (Int32)IdAllocationDriverData;
                objAllocationDriversData.loadObject();
                objAllocationDriversData.IdAllocationListDriver = _IdDriver;
                objAllocationDriversData.IdClientCostCenter = _IdCostCenter;
                objAllocationDriversData.DateCreation = DateTime.Now;
                objAllocationDriversData.UserCreation = (Int32)IdUserCreate;
                IdAllocationDriverData = objAllocationDriversData.Insert();
                objAllocationDriversData = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected String GetUnit(Int32 _IdUnit)
        {
            String Value = "";
            DataClass.clsUnit objUnit = new DataClass.clsUnit();
            DataTable dt = objUnit.LoadComboBox("IdUnit = " + _IdUnit , "");

            if (dt.Rows.Count > 0)
            {
                Value = (String)dt.Rows[0]["Name"];
            }
            return Value;
        }

        protected Int32 GetIdCostCenter(String _CostCenter)
        {
            Int32 Value = 0;
            DataClass.clsClientCostCenter objClientCostCenter = new DataClass.clsClientCostCenter();
            DataTable dt = objClientCostCenter.LoadList("Code = '" + _CostCenter + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdClientCostCenter"];
            }
            return Value;
        }

        protected Int32 GetIdDriver(String _Driver)
        {
            Int32 Value = 0;
            DataClass.clsAllocationListDriver objAllocationListDriver = new DataClass.clsAllocationListDriver();
            DataTable dt = objAllocationListDriver.LoadComboBox("Name = '" + _Driver + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["ID"];
            }
            return Value;
        }

        protected void EditAllocationListDriverLoad()
        {
            DataClass.clsAllocationDriversData objAllocationDriversData = new DataClass.clsAllocationDriversData();
            DataTable dt = objAllocationDriversData.LoadList("IdModel = " + IdModel + " AND IdAllocationDriverData = " + IdAllocationDriverData, "");
            if (dt.Rows.Count > 0)
            {
                this.storeCostCenter.Reload();
                IdAllocationDriverData = Convert.ToInt32(dt.Rows[0]["IdAllocationDriverData"]);
                this.ddlCostCenter.Value = Convert.ToInt32(dt.Rows[0]["IdClientCostCenter"]);
                this.ddlDriver.Value = Convert.ToInt32(dt.Rows[0]["IdAllocationListDriver"]);
                this.ddlDriver.ReadOnly = false;
            }
        }

        protected void StoreDriver_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsAllocationListDriver objAllocationListDriver = new DataClass.clsAllocationListDriver();
            this.storeDriver.DataSource = objAllocationListDriver.LoadList("IdModel = " + IdModel, "ORDER BY Name");
            this.storeDriver.DataBind();
        }

        protected void StoreCostCenter_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsAllocationDriversData objAllocationDriversData = new DataClass.clsAllocationDriversData();
            objAllocationDriversData.IdAllocationDriverData = IdAllocationDriverData;
            DataTable dt = objAllocationDriversData.LoadCostCenter(IdModel);
            storeCostCenter.DataSource = dt;
            storeCostCenter.DataBind();
        }

        protected void ddlCostCenter_Select(object sender, DirectEventArgs e)
        {
            if (ddlCostCenter.Value == null || ddlCostCenter.Value.ToString() == "")
            {
                this.ddlDriver.ReadOnly = true;
            }
            else
            {
                this.ddlDriver.ReadOnly = false;
            }
        }

        public void Delete()
        {
            DataClass.clsAllocationDriversData objAllocationDriversData = new DataClass.clsAllocationDriversData();
            objAllocationDriversData.IdAllocationDriverData = (Int32)IdAllocationDriverData;
            objAllocationDriversData.Delete();
            this.ModelUpdateTimestamp();
        }

        public void DeleteAll()
        {
            DataClass.clsAllocationDriversData objAllocationDriversData = new DataClass.clsAllocationDriversData();
            objAllocationDriversData.DeleteAll(IdModel);
            this.ModelUpdateTimestamp();
        }

        [DirectMethod]
        public void ClickedDeleteYES(String _Driver)
        {
            this.Delete();
            this.LoadAllocationListDriver();
        }

        [DirectMethod]
        public void ClickedDeleteAllYES()
        {
            this.DeleteAll();
            this.LoadAllocationListDriver();
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsAllocationDriversData objAllocationDriversData = new DataClass.clsAllocationDriversData();
            DataTable dt = objAllocationDriversData.LoadList("Name = '" + ddlDriver.Text + "'", "");
            Value = dt.Rows.Count;

            return Value;
        }

        protected void LoadAllocationListDriver()
        {
            DataClass.clsAllocationDriversData objAllocationDriversData = new DataClass.clsAllocationDriversData();

            DataTable dt = objAllocationDriversData.LoadList("IdModel = " + IdModel + " AND IdLanguage = " + idLanguage, "ORDER BY Driver");

            storeAllocationDriverData.DataSource = dt;
            storeAllocationDriverData.DataBind();

            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Driver", "Driver Desc"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("CostObjectCode", "Cost Object Code"));
            String strSelectedParameters = "";
            String[] columnsToCopy = { "Driver", "CostObjectCode" };
            System.Data.DataView view = new System.Data.DataView(dt);
            DataTable dtAllocationDriverData = view.ToTable(true, columnsToCopy);
            ExportAllocationDriverData = new DataReportObject(dtAllocationDriverData, "DATASHEET", null, strSelectedParameters, null, "Driver", datColumnTD, null);
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportAllocationDriverData;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=2.3.2 ExportAllocationDriversCriteria.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportAllocationDriversCriteria.xls", "template");
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        protected void LoadLabel()
        {
            this.grdDriverData.Title = modMain.strMasterAllocationDriversCriteria;
            this.btnAdd.Text = (String)GetGlobalResourceObject("CPM_Resources", "Add New Criteria");
            this.btnAdd.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Add New Criteria");
            this.btnDelete.Text = modMain.strCommonDeleteAll;
            this.btnDelete.ToolTip = modMain.strCommonDeleteAll;
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.colClientCostCenter.Text = (String)GetGlobalResourceObject("CPM_Resources", "Company Cost Object");
            this.colTypeAllocation.Text = (String)GetGlobalResourceObject("CPM_Resources", "Type of Allocation");
            this.Driver.Text = (String)GetGlobalResourceObject("CPM_Resources", "Driver");
            this.colUnit.Text = (String)GetGlobalResourceObject("CPM_Resources", "Unit");
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = "&nbsp;" + modMain.strCommonEdit;
            this.ImageCommandColumn1.Commands[1].Text = "&nbsp;" + modMain.strCommonDelete;
            this.winAllocationDriverDataEdit.Title = modMain.strMasterAllocationDriversCriteria;
            this.ddlCostCenter.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Cost Object");
            this.ddlDriver.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Driver");
            this.btnSave.Text = modMain.strCommonSave;
            this.btnSave.ToolTip = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.btnCancel.ToolTip = modMain.strCommonClose;
            this.winImport.Title = modMain.strCommonImport + " " + modMain.strMasterAllocationDriversCriteria;
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnImportFile.ToolTip = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.btnCancelImport.ToolTip = modMain.strCommonClose;
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        #endregion

    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _basecostbyfieldziff : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }
        private DataReportObject ExportCostByField { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }
        public DataTable dtVolume;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                this.LoadLabel();
                this.ModelCheckRecalcModuleAllocation();
                DataTable dtValues = new DataTable();
                this.FillGrid(this.storeZiffBaseCostField, this.grdBaseCostByFieldZiff, dtValues, null);
            }
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void ddlLevels_Select(object sender, DirectEventArgs e)
        {
            this.ddlField.Reset();
            this.storeField.Reload();
        }

        #endregion

        #region Methods

        protected void LoadVolumeResults(Double _dblCurrencyFactor, String _strCurrencyCode)
        {
            DataClass.clsBaseCostByField objBaseCostByField = new DataClass.clsBaseCostByField();

            DataTable dt = objBaseCostByField.LoadVolumeReport(IdModel, 1);
            DataTable dtRows = objBaseCostByField.LoadVolumeRows(IdModel,1);
            DataTable dtValues = new DataTable();

            dtValues.Columns.Add("Name");

            DataTable dtField;

            DataClass.clsFields objFields = new DataClass.clsFields();

            if ((ddlLevels.Value == null) || (ddlLevels.Value.ToString() == "0") || (ddlLevels.Value.ToString() == ""))
            {
                if (this.ddlField.Value == null)
                {
                    dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
                }
                else
                {
                    if (this.ddlField.Value.ToString() == "0")
                    {
                        dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
                    }
                    else
                    {
                        dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField = " + this.ddlField.Value, "ORDER BY Name");
                    }
                }
            }
            else
            {
                if (this.ddlField.Value == null)
                {
                    dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
                }
                else
                {
                    if (this.ddlField.Value.ToString() == "0")
                    {
                        dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
                    }
                    else
                    {
                        dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField = " + this.ddlField.Value, "ORDER BY Name");
                    }
                }
            }

            foreach (DataRow drField in dtField.Rows)
            {
                dtValues.Columns.Add(drField["Name"].ToString().Replace(" ", "_"), typeof(Double));
            }
            dtValues.Columns.Add((String)GetGlobalResourceObject("CPM_Resources", "Grand_Total"), typeof(Double));

            foreach (DataRow drRows in dtRows.Rows)
            {
                Int32 intIdAllocationListDriver = (Int32)drRows["IdAllocationListDriver"];

                DataRow drAct = dtValues.NewRow();
                drAct["Name"] = drRows["Name"].ToString();

                foreach (DataColumn dcValues in dtValues.Columns)
                {
                    if (dcValues.ColumnName == (String)GetGlobalResourceObject("CPM_Resources", "Grand_Total"))
                    {
                        DataRow[] FilterRows;
                        FilterRows = dt.Select("IdAllocationListDriver = '" + intIdAllocationListDriver + "'");

                        Double dblValue = 0;

                        foreach (DataRow row in FilterRows)
                        {
                            dblValue = dblValue + Convert.ToDouble(row["Amount"]);
                        }

                        drAct[dcValues.ColumnName] = dblValue * _dblCurrencyFactor;
                    }
                    else if (dcValues.ColumnName != "Name")
                    {
                        DataRow[] FilterRows;
                        FilterRows = dt.Select("IdField = " + GetIdField(dcValues.ColumnName.Replace("_", " ")) + " AND IdAllocationListDriver = '" + intIdAllocationListDriver + "'");

                        Double dblValue = 0;

                        foreach (DataRow row in FilterRows)
                        {
                            dblValue = dblValue + Convert.ToDouble(row["Amount"]);
                        }

                        drAct[dcValues.ColumnName] = dblValue * _dblCurrencyFactor;
                    }
                }
                dtValues.Rows.Add(drAct);
                dtValues.AcceptChanges();
            }

            dtVolume = dtValues;

            clsModels objModels = new clsModels();
            this.FillGridVolume(this.storeVolume, this.grdVolume, dtValues, _strCurrencyCode);

            //    // ToDo: can reset language of columns here
            //    ArrayList datColumn = new ArrayList();
            //    datColumn.Add(new DataClass.DataTableColumnDisplay("Cost", "Resource"));

            //    String strSelectedParameters = lblField.Text + " '" + (String.IsNullOrEmpty(ddlField.SelectedItem.Text) == true ? "- All -" : ddlField.SelectedItem.Text) + "'";
            //    ExportCostByField = new DataReportObject(dtValues, "CostByField", "Base Cost by Field (Company Structure)", strSelectedParameters, null, "Activity,Cost", datColumn, null);
        }

        protected void LoadBaseCostByFieldZiff(Double _dblCurrencyFactor, String _strCurrencyCode)
        {
            DataClass.clsBaseCostByFieldZiff objBaseCostByFieldZiff = new DataClass.clsBaseCostByFieldZiff();

            DataTable dt = objBaseCostByFieldZiff.LoadReport(IdModel);
            DataTable dtRows = objBaseCostByFieldZiff.LoadRows(IdModel);
            DataTable dtValues = new DataTable();

            dtValues.Columns.Add("Parent");
            dtValues.Columns.Add("Cost");

            DataTable dtField;

            DataClass.clsFields objFields = new DataClass.clsFields();
            if ((ddlLevels.Value == null) || (ddlLevels.Value.ToString() == "0") || (ddlLevels.Value.ToString() == ""))
            {
                if (this.ddlField.Value == null)
                {
                    dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
                }
                else
                {
                    if (this.ddlField.Value.ToString() == "0")
                    {
                        dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
                    }
                    else
                    {
                        dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField = " + this.ddlField.Value, "ORDER BY Name");
                    }
                }
            }
            else
            {
                if (this.ddlField.Value == null)
                {
                    dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
                }
                else
                {
                    if (this.ddlField.Value.ToString() == "0")
                    {
                        dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
                    }
                    else
                    {
                        dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField = " + this.ddlField.Value, "ORDER BY Name");
                    }
                }
            }

            foreach (DataRow drField in dtField.Rows)
            {
                dtValues.Columns.Add(drField["Name"].ToString().Replace(" ", "_"), typeof(Double));
            }
            dtValues.Columns.Add((String)GetGlobalResourceObject("CPM_Resources", "Grand_Total"), typeof(Double));

            foreach (DataRow drRows in dtRows.Rows)
            {
                Int32 intIdRootZiffAccount = (Int32)drRows["IdRootZiffAccount"];
                Int32 intIdZiffAccount = (Int32)drRows["IdZiffAccount"];

                DataRow drAct = dtValues.NewRow();
                drAct["Parent"] = drRows["RootZiffAccount"].ToString();
                drAct["Cost"] = drRows["ZiffAccount"].ToString();

                foreach (DataColumn dcValues in dtValues.Columns)
                {
                    if (dcValues.ColumnName == (String)GetGlobalResourceObject("CPM_Resources", "Grand_Total"))
                    {
                        DataRow[] FilterRows;
                        FilterRows = dt.Select("IdRootZiffAccount = '" + intIdRootZiffAccount + "' AND IdZiffAccount = '" + intIdZiffAccount + "'");

                        Double dblValue = 0;

                        foreach (DataRow row in FilterRows)
                        {
                            dblValue = dblValue + Convert.ToDouble(row["Amount"]);
                        }

                        if ((String)ddlCostType.Value == "2")
                        {
                            drAct[dcValues.ColumnName] = (dblValue * _dblCurrencyFactor) / ((Double)dtVolume.Rows[0][dcValues.ColumnName] * _dblCurrencyFactor);
                        }
                        else
                        {
                            drAct[dcValues.ColumnName] = dblValue * _dblCurrencyFactor;
                        }
                    }
                    else if (dcValues.ColumnName != "Parent" && dcValues.ColumnName != "Cost")
                    {
                        DataRow[] FilterRows;
                        FilterRows = dt.Select("IdField = " + GetIdField(dcValues.ColumnName.Replace("_", " ")) + " AND IdRootZiffAccount = '" + intIdRootZiffAccount + "' AND IdZiffAccount = '" + intIdZiffAccount + "'");

                        Double dblValue = 0;

                        foreach (DataRow row in FilterRows)
                        {
                            dblValue = dblValue + Convert.ToDouble(row["Amount"]);
                        }

                        if ((String)ddlCostType.Value == "2")
                        {
                            drAct[dcValues.ColumnName] = (dblValue * _dblCurrencyFactor) / ((Double)dtVolume.Rows[0][dcValues.ColumnName] * _dblCurrencyFactor);
                        }
                        else
                        {
                            drAct[dcValues.ColumnName] = dblValue * _dblCurrencyFactor;
                        }
                    }
                }
                dtValues.Rows.Add(drAct);
                dtValues.AcceptChanges();
            }

            clsModels objModels = new clsModels();
            this.FillGrid(this.storeZiffBaseCostField, this.grdBaseCostByFieldZiff, dtValues, _strCurrencyCode);

            // ToDo: can reset language of columns here
            ArrayList datColumn = new ArrayList();
            datColumn.Add(new DataClass.DataTableColumnDisplay("Parent", "Parent_ZiffAccount"));
            datColumn.Add(new DataClass.DataTableColumnDisplay("Cost", "ZiffAccount"));

            String strSelectedParameters = lblField.Text + " '" + (String.IsNullOrEmpty(ddlField.SelectedItem.Text) == true ? "- All -" : ddlField.SelectedItem.Text) + "'";
            ExportCostByField = new DataReportObject(dtValues, "CostByField", "Base Cost by Field (Ziff Cost Categories)", strSelectedParameters, null, "Parent,Cost", datColumn, null);
        }

        private void FillGridVolume(Ext.Net.Store s, GridPanel g, DataTable dt, String CurrencyCode)
        {
            s.RemoveFields();

            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "Name")
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Auto));
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Float));
                }
            }

            g.Features.Clear();

            GridFilters f = new GridFilters();
            f.Local = true;
            g.Features.Add(f);

            foreach (DataColumn c in dt.Columns)
            {
                if (!c.ColumnName.Contains("Id"))
                {
                    if (c.ColumnName == "Name")
                    {
                        StringFilter sFilter = new StringFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                    else
                    {
                        NumericFilter sFilter = new NumericFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                }
            }

            Column col = new Column();
            col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Fields") + " (" + CurrencyCode + ")";
            SummaryColumn colSum = new SummaryColumn();
            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "Name")
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        if (c.ColumnName != "Activity")
                        {
                            colSum = new SummaryColumn();
                            colSum.DataIndex = c.ColumnName;
                            colSum.Text = "Volume Driver Name";// (String)GetGlobalResourceObject("CPM_Resources", "Cost");
                            colSum.SummaryType = Ext.Net.SummaryType.Count;
                            colSum.TdCls = "task";
                            colSum.Sortable = true;
                            colSum.Groupable = true;
                            colSum.Width = 300;
                            colSum.SummaryRenderer.Handler = "return '(' + value + ' Total)';";
                            colSum.MenuDisabled = false;
                            colSum.Align = Alignment.Left;
                            g.ColumnModel.Columns.Add(colSum);
                        }
                    }
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        colSum = new SummaryColumn();
                        colSum.DataIndex = c.ColumnName;
                        colSum.Text = c.ColumnName.Replace("_", " ");
                        colSum.SummaryType = Ext.Net.SummaryType.Sum;
                        Renderer sumRen = new Renderer();
                        sumRen.Fn = "Ext.util.Format.numberRenderer('0,000.00')";
                        colSum.SummaryRenderer = sumRen;
                        colSum.Renderer = sumRen;
                        colSum.Align = Alignment.Right;
                        colSum.MinWidth = 120;
                        col.Columns.Add(colSum);
                    }
                }
            }

            g.ColumnModel.Columns.Add(col);

            s.DataSource = dt;
            //s.GroupField = "Name";
            if (X.IsAjaxRequest)
            {
                g.Reconfigure();
            }
            s.DataBind();
        }

        private void FillGrid(Ext.Net.Store s, GridPanel g, DataTable dt, String CurrencyCode)
        {
            s.RemoveFields();

            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "Parent" || c.ColumnName == "Cost")
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Auto));
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Float));
                }
            }

            g.Features.Clear();

            GroupingSummary gsum = new GroupingSummary();
            gsum.GroupHeaderTplString = "{name}";
            gsum.HideGroupedHeader = true;
            gsum.ShowSummaryRow = true;

            g.Features.Add(gsum);

            GridFilters f = new GridFilters();
            f.Local = true;
            g.Features.Add(f);

            Summary smry = new Summary();
            smry.Dock = new SummaryDock();
            smry.ID = "Summary" + g.ID.ToString();

            g.Features.Add(smry);

            foreach (DataColumn c in dt.Columns)
            {
                if (!c.ColumnName.Contains("Id"))
                {
                    if (c.ColumnName == "Parent" || c.ColumnName == "Cost")
                    {
                        StringFilter sFilter = new StringFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                    else
                    {
                        NumericFilter sFilter = new NumericFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                }
            }

            Column col = new Column();
            col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Fields") + " (" + CurrencyCode + ")";
            SummaryColumn colSum = new SummaryColumn();
            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "Parent" || c.ColumnName == "Cost")
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        if (c.ColumnName != "Parent")
                        {
                            colSum = new SummaryColumn();
                            colSum.DataIndex = c.ColumnName;
                            colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", "Cost");
                            colSum.SummaryType = Ext.Net.SummaryType.Count;
                            colSum.TdCls = "task";
                            colSum.Sortable = true;
                            colSum.Groupable = true;
                            colSum.Width = 300;
                            colSum.SummaryRenderer.Handler = "return '(' + value + ' Total)';";
                            colSum.MenuDisabled = false;
                            colSum.Align = Alignment.Left;
                            g.ColumnModel.Columns.Add(colSum);
                        }
                    }
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        colSum = new SummaryColumn();
                        colSum.DataIndex = c.ColumnName;
                        colSum.Text = c.ColumnName.Replace("_", " ");
                        colSum.SummaryType = Ext.Net.SummaryType.Sum;
                        Renderer sumRen = new Renderer();
                        sumRen.Fn = "Ext.util.Format.numberRenderer('0,000.00')";
                        colSum.SummaryRenderer = sumRen;
                        colSum.Renderer = sumRen;
                        colSum.Align = Alignment.Right;
                        colSum.MinWidth = 120;
                        col.Columns.Add(colSum);
                    }
                }
            }

            g.ColumnModel.Columns.Add(col);

            s.DataSource = dt;
            s.GroupField = "Parent";
            if (X.IsAjaxRequest)
            {
                g.Reconfigure();
            }
            s.DataBind();
        }

        protected void StoreField_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dtField = new DataTable();
            if ((ddlLevels.Value == null) || (ddlLevels.Value.ToString() == "0") || (ddlLevels.Value.ToString() == ""))
            {
                dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
            }
            else
            {
                dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
            }
            DataRow drField = dtField.NewRow();
            drField["IdField"] = 0;
            drField["Name"] = "- All -";
            dtField.Rows.Add(drField);
            dtField.AcceptChanges();
            dtField.DefaultView.Sort = "Name";
            DataTable dtSort = dtField.DefaultView.ToTable();

            this.storeField.DataSource = dtSort;
            this.storeField.DataBind();

            this.ddlField.Value = 0;

            this.storeField.DataBind();
        }

        protected void StoreLevel_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dt = objAggregationLevels.LoadList("IdModel = " + IdModel, "ORDER BY Name");

            DataRow drLevel = dt.NewRow();
            drLevel["IdAggregationLevel"] = 0;
            drLevel["Name"] = "- All -";
            dt.Rows.Add(drLevel);
            dt.AcceptChanges();
            dt.DefaultView.Sort = "Name";
            DataTable dtSort = dt.DefaultView.ToTable();

            this.storeLevels.DataSource = dtSort;
            this.storeLevels.DataBind();

            this.ddlLevels.Value = 0;

            this.storeLevels.DataBind();
        }

        protected void StoreCurr_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsCurrencies objCurrencies = new DataClass.clsCurrencies();
            this.storeBase.DataSource = objCurrencies.LoadList("IdModel IN (" + IdModel + ") AND Output=1", "ORDER BY BaseCurrency DESC,Name");
            this.storeBase.DataBind();
            DataTable dt = objCurrencies.LoadList("BaseCurrency = 1 AND IdModel = " + IdModel, "");
            if (dt.Rows.Count > 0)
            {
                ddlCurrency.Value = dt.Rows[0]["IdCurrency"];
            }
            else
            {
                ddlCurrency.SelectedItem.Index = 0;
            }
        }

        protected void StoreCostType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsCostType objCostTypes = new DataClass.clsCostType();
            this.storeCostType.DataSource = objCostTypes.LoadList("", "ORDER BY Name");
            this.storeCostType.DataBind();
            DataTable dt = objCostTypes.LoadList("", "");
            if (dt.Rows.Count > 0)
            {
                ddlCostType.Value = dt.Rows[0]["IdCostType"];
            }
            else
            {
                ddlCostType.SelectedItem.Index = 0;
            }
        }

        protected void ddlField_Select(object sender, DirectEventArgs e)
        {
            Int32 intCurrency = Convert.ToInt32(ddlCurrency.Value);
            Int32 intCostType = Convert.ToInt32(ddlCostType.Value);
            Double dblCurrencyFactor = 1;

            clsCurrencies objCurrencies = new clsCurrencies();
            objCurrencies.IdCurrency = intCurrency;
            objCurrencies.loadObject();

            DataTable dtCurrencyFactor = objCurrencies.GetCurrencyFactor(IdModel, intCurrency);
            if (dtCurrencyFactor.Rows.Count > 0)
            {
                dblCurrencyFactor = (Double)dtCurrencyFactor.Rows[0]["Value"];
            }

            this.LoadVolumeResults(dblCurrencyFactor, objCurrencies.Code);
            this.LoadBaseCostByFieldZiff(dblCurrencyFactor, objCurrencies.Code);
        }

        private Int32 GetIdField(String _Name)
        {
            Int32 intID = 0;

            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dtField = objFields.LoadList("Name = '" + _Name + "' AND IdModel = " + IdModel, "ORDER BY Name");

            if (dtField.Rows.Count > 0)
            {
                intID = (Int32)dtField.Rows[0]["IdField"];
            }

            return intID;
        }

        private Int32 GetIdLevel(String _Name)
        {
            Int32 intID = 0;

            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dtLevel = objAggregationLevels.LoadList("IdModel = " + IdModel + " AND Name = '" + _Name + "'", "ORDER BY Name");

            if (dtLevel.Rows.Count > 0)
            {
                intID = (Int32)dtLevel.Rows[0]["IdAggregationLevel"];
            }

            return intID;
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExportCostByField = new DataReportObject();
            StoredExportCostByField = ExportCostByField;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExportCostByField);
            objWriter.BuildExportFile(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2007);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=CostByFieldZiff.xlsx");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        public void ModelCheckRecalcModuleAllocation()
        {
            clsModels objModelRecalc = new clsModels();
            objModelRecalc.IdModel = IdModel;
            objModelRecalc.CheckRecalcModuleParameter(objApplication.MaxRelationLevels);
            objModelRecalc.CheckRecalcModuleAllocation();
            objModelRecalc = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = modMain.strMasterBaseCostbyFieldZiff;
            this.lblLevels.Text = (String)GetGlobalResourceObject("CPM_Resources", "Aggregation Level");
            this.lblField.Text = modMain.strTechnicalModuleField;
            this.lblCurrency.Text = (String)GetGlobalResourceObject("CPM_Resources", "Currency");
            this.lblCostType.Text = (String)GetGlobalResourceObject("CPM_Resources", "Type");
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.pnlVolumeResults.Title = (String)GetGlobalResourceObject("CPM_Resources", "Volume Results");
        }

        #endregion

    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Xml;
using CD;
using DataClass;
using System.Data.SqlClient;

namespace CPM
{
    public partial class _coststructureassumptions : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        public DataTable datViewData { get; set; }
        private DataReportObject ExportTechnicalAssumption { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events
        
        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                this.ModelPrecalcModuleAllocation();
                this.LoadLabel();
            }
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            String rowsValues = e.ExtraParams["rowsValues"];
            datViewData = JSON.Deserialize<DataTable>(rowsValues);

            DataClass.clsCostStructureAssumptions objCostStructureAssumptions = new clsCostStructureAssumptions();

            foreach (DataRow drRow in datViewData.Rows)
            {
                for (int w = 6; w < this.datViewData.Columns.Count; w++)
                {
                    String ColumnName = datViewData.Columns[w].ColumnName.ToString();
                    if (drRow[3].ToString().IndexOf("-") == 4)
                    {
                        //GetIdPeerGroup
                        Int32 intIdPeerGroup = objCostStructureAssumptions.GetIdPeerGroup(IdModel, ColumnName);
                        //Get Projects
                        DataTable dtPrj = objCostStructureAssumptions.GetProjects(IdModel, Convert.ToInt32(ddlField.Value));
                        foreach (DataRow drPrj in dtPrj.Rows)
                        {
                            Int32 intIdProject = Convert.ToInt32(drPrj["IdProject"]);
                            Int32 intIdZiffAccount = Convert.ToInt32(drRow["IdZiffAccount"].ToString());
                            //GetIdCostStructureAssumptions
                            Int32 intIdCostStructureAssumptions = objCostStructureAssumptions.GetIdCostStructureAssumptions(intIdProject, intIdZiffAccount, intIdPeerGroup, Convert.ToInt32(ddlField.Value));
                            Int32 intClient = 0;
                            Int32 intZiff = 0;
                            try
                            {
                                try
                                {
                                    intClient = Convert.ToInt32(drRow["Client"].ToString());
                                }
                                catch
                                {
                                    intClient = 0;
                                }
                            }
                            catch
                            {
                                intClient = 0;
                            }
                            try
                            {
                                intZiff = Convert.ToInt32(drRow[w].ToString());
                                //objCostStructureAssumptions.UpdateRecord(intIdCostStructureAssumptions, intIdProject, intIdZiffAccount, intClient, intZiff, intIdPeerGroup);
                            }
                            catch
                            {
                                intZiff = 0;
                            }
                            objCostStructureAssumptions.UpdateRecord(intIdCostStructureAssumptions, intIdProject, intIdZiffAccount, intClient, intZiff, intIdPeerGroup, Convert.ToInt32(ddlField.Value));
                        }
                    }
                }
            }

            ModelUpdateTimestamp();
            this.LoadCostStructureAssumptions();
            string message = (String)GetGlobalResourceObject("CPM_Resources", "Records were successfully updated");

            //this.statusDialogs.SetStatus(new StatusBarStatusConfig { Text = message, IconCls = "icon-accept", Clear2 = false });
        }

        protected void Structure_BeforeEdit(object sender, DirectEventArgs e)
        {
            Int32 intIdProject = 0;
            Int32 intClientYear = 0;
            Int32 intIdField = 0;
            
            if (Convert.ToInt32(e.ExtraParams["ClientYear"]) != 9999)
            {
                if (ddlField.Value != null && ddlField.Value.ToString() != "")
                {
                    intIdField = Convert.ToInt32(ddlField.Value);
                }

                intIdProject = getIdBLProject(intIdField);
                try
                {
                    intClientYear = Convert.ToInt32(e.ExtraParams["ClientYear"]);
                }
                catch
                {
                    intClientYear = 0;
                }

                if (intIdProject > 0)
                {
                    UpdateZiffCombo(intIdProject, intClientYear);
                }
            }
            else
            {
                //DataTable dt = new DataTable();
                //storeCboClient.DataSource = dt;
                //storeCboClient.DataBind();
                //storeCboZiff.DataSource = dt;
                //storeCboZiff.DataBind();
            }
        }

        protected void storeFields_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsFields objFields = new DataClass.clsFields();
            this.storeFields.DataSource = objFields.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblProjects WHERE IdModel = " + IdModel + " AND Operation = 1)", "ORDER BY Code, Name");
            this.storeFields.DataBind();
        }

        protected void ddlField_Select(object sender, DirectEventArgs e)
        {
            RunCalculations();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.ModelPrecalcModuleAllocation();
                String strReturnMessage = null;
                if (this.fileImport.HasFile)
                {
                    DataFileReader objReader = null;
                    DataFileType objType = new DataFileType();
                    objType.CheckFileType(fileImport.PostedFile.FileName);

                    if (!objType.IsValidType)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }

                    if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                    {
                        objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.ExternalBaseCostReferences);
                        objReader.GetDataTable(ref strReturnMessage);
                    }
                    else
                    {
                        strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                    }

                    if (String.IsNullOrEmpty(strReturnMessage))
                    {
                        intFailedImportRowCount = 0;
                        strImportErrorMessage = "";
                        intCurrentImportRow = 0;
                        foreach (DataRow row in objReader.ImportTable.Rows)
                        {
                            intCurrentImportRow++;
                            this.SaveImport(row["Peer Group Code"].ToString(), row["Cost Category Code"].ToString(), Convert.ToInt32(row["Year"].ToString()));
                        }
                        if (intFailedImportRowCount != 0)
                        {
                            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                        }
                        else
                        {
                            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                        }
                        this.winImport.Hide();
                        //this.LoadCostStructureAssumptions();
                        ModelUpdateTimestamp();
                        //Trim Error if too long
                        if (strImportErrorMessage.Length > 1024)
                        {
                            strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                        }
                        X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                    }
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
                }
            }
            catch
            {
            }
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        protected void LoadCostStructureAssumptions()
        {
            DataClass.clsCostStructureAssumptions objCostStructureAssumptions = new DataClass.clsCostStructureAssumptions();
            DataTable dt = objCostStructureAssumptions.LoadList(IdModel, Convert.ToInt32(this.ddlField.Value));

            DataColumnCollection columns = dt.Columns;
            if (columns.Contains("PeerGroupDescription"))
            {
                dt.Columns["PeerGroupDescription"].ColumnName = "N/A";
            }
            
            this.FillGrid(storeCostStructureAssumptionsProj, grdCostStructureAssumptionsProj, dt);
        }

        protected int getIdBLProject(Int32 pIdField)
        {
            DataClass.clsProjects objProjects = new DataClass.clsProjects();
            DataTable dt;

            dt = objProjects.LoadList("IdModel = " + IdModel + " AND IdField = " + pIdField + " AND Operation = 1", "ORDER BY IdProject");
            if (dt.Rows.Count > 0)
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
            else
            {
                return 0;
            }
        }

        protected void RunCalculations()
        {
            Int32 intIdField = 0;
            Int32 intIdProject = 0;

            if (ddlField.Value != null && ddlField.Value.ToString() != "")
            {
                intIdField = Convert.ToInt32(ddlField.Value);
            }
            intIdProject = getIdBLProject(intIdField);
            if (intIdProject > 0)
            {
                UpdateClientCombo(intIdProject);
            }
            this.LoadCostStructureAssumptions();
        }

        protected void UpdateClientCombo(Int32 pIdProject)
        {
            DataTable dtTmp = CD.DAC.ConsultSQL("SELECT tblProjects.Starts, tblModels.BaseYear, tblModels.BaseYear+tblModels.Projection AS LastYear FROM tblModels INNER JOIN tblProjects ON tblModels.IdModel = tblProjects.IdModel WHERE tblProjects.IdProject=" + pIdProject.ToString() + " AND tblModels.IdModel=" + IdModel.ToString()).Tables[0];
            Int32 intBaseYear = Convert.ToInt32(dtTmp.Rows[0]["BaseYear"]);
            Int32 intStartYear = Convert.ToInt32(dtTmp.Rows[0]["Starts"]);
            Int32 intEndYear = Convert.ToInt32(dtTmp.Rows[0]["LastYear"]);

            DataTable dt = new DataTable();
            dt.Columns.Add("Value", typeof(Int32));
            dt.Columns.Add("Text", typeof(String));

            DataRow drNA = dt.NewRow();
            drNA[0] = 0;
            drNA[1] = "- N/A -";
            dt.Rows.Add(drNA);

            if (intStartYear == intBaseYear)
            {
                for (int i = intStartYear; i <= intEndYear; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i;
                    dr[1] = Convert.ToString(i);
                    dt.Rows.Add(dr);
                }
            }
            storeCboClient.DataSource = dt;
            storeCboClient.DataBind();
        }

        protected void UpdateZiffCombo(Int32 pIdProject, Int32 pClientYear)
        {
            DataTable dtTmp = CD.DAC.ConsultSQL("SELECT tblProjects.Starts, tblModels.BaseYear, tblModels.BaseYear+tblModels.Projection AS LastYear FROM tblModels INNER JOIN tblProjects ON tblModels.IdModel = tblProjects.IdModel WHERE tblProjects.IdProject=" + pIdProject.ToString() + " AND tblModels.IdModel=" + IdModel.ToString()).Tables[0];
            Int32 intBaseYear = Convert.ToInt32(dtTmp.Rows[0]["BaseYear"]);
            Int32 intStartYear = Convert.ToInt32(dtTmp.Rows[0]["Starts"]);
            Int32 intEndYear = Convert.ToInt32(dtTmp.Rows[0]["LastYear"]);

            DataTable dt = new DataTable();
            dt.Columns.Add("Value", typeof(Int32));
            dt.Columns.Add("Text", typeof(String));

            DataRow drNA = dt.NewRow();
            drNA[0] = 0;
            drNA[1] = "- N/A -"; 
            dt.Rows.Add(drNA);

            if (pClientYear > 0)
            {
                intStartYear = pClientYear + 1;
            }

            for (int i = intStartYear; i <= intEndYear; i++)
            {
                DataRow dr = dt.NewRow();
                dr[0] = i;
                dr[1] = Convert.ToString(i);
                dt.Rows.Add(dr);
            }
            //storeCboZiff.DataSource = dt;
            //storeCboZiff.DataBind();
        }

        private void FillGrid(Ext.Net.Store s, GridPanel g, DataTable dt)
        {
            s.RemoveFields();
            foreach (DataColumn c in dt.Columns)
            {
                //Want to add all columns in the Model
                s.AddField(new ModelField(c.ColumnName, ModelFieldType.Auto));

            }

            Column col = new Column();
            SummaryColumn colSum = new SummaryColumn();
            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "Code" || c.ColumnName == "Name" || c.ColumnName == "ProjectName" || c.ColumnName == "Client" || c.ColumnName == "N/A")
                {
                    //Don't want to do anything to these columns
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        //Only add peer groups if any exist
                        col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Peer Group");
                        colSum = new SummaryColumn();
                        colSum.DataIndex = c.ColumnName;
                        colSum.Text = c.ColumnName;
                        colSum.SummaryType = Ext.Net.SummaryType.Sum;
                        Renderer sumRen = new Renderer();
                        sumRen.Fn = "formatZero";
                        colSum.SummaryRenderer = sumRen;
                        colSum.Renderer = sumRen;
                        colSum.Align = Alignment.Right;
                        colSum.MinWidth = 175;
                        colSum.MenuDisabled = true;
                        col.Columns.Add(colSum);
                    }
                }
            }
            if (col.Text != "")
            {
                g.ColumnModel.Columns.Add(col);
                //Add the editor to each column, if peer group exists
                for (int x = 0; x < g.ColumnModel.Columns[4].Columns.Count; x++)
                {
                    //g.ColumnModel.Columns[4].Columns[x].Editor.Add(new Ext.Net.ComboBox());
                    g.ColumnModel.Columns[4].Columns[x].Editor.Add(new Ext.Net.TextField());
                }
            }
            s.DataSource = dt;
            if (X.IsAjaxRequest)
            {
                g.Reconfigure();
            }
            s.DataBind();
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        public void ModelPrecalcModuleAllocation()
        {
            clsModels objModelPrecalc = new clsModels();
            objModelPrecalc.IdModel = IdModel;
            objModelPrecalc.CheckRecalcModuleParameter(objApplication.MaxRelationLevels);
            objModelPrecalc.PrecalcModuleAllocation();
            objModelPrecalc = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = modMain.strMasterCostStructureAssumptions;
            this.lblFilterField.Text = modMain.strTechnicalModuleField;
            this.btnSave.Text = modMain.strCommonSaveChanges;
            this.btnSave.ToolTip = modMain.strCommonSaveChanges;
            //this.Column1.Text = (String)GetGlobalResourceObject("CPM_Resources", "ParentCode");
            this.colCode.Text = modMain.strCommonCode;
            this.colName.Text = modMain.strCommonName;
            this.colClient.Text = (String)GetGlobalResourceObject("CPM_Resources", "Company");
            //this.colCase.Text = (String)GetGlobalResourceObject("CPM_Resources", "Case");
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        protected void SaveImport(String _PeerGroupCode, String _CostCategory, Int32 _Year)
        {
            try
            {
                DataClass.clsCostStructureAssumptions objCostStructureAssumptions = new clsCostStructureAssumptions();
                Int32 _IdZiffAccount = GetIdZiffAccount(_CostCategory);
                Int32 _IdPeerGroup = objCostStructureAssumptions.GetIdPeerGroup(IdModel, _PeerGroupCode);


                if (_IdZiffAccount != 0 && _IdPeerGroup != 0)
                {
                    DataTable dtCostStructureAssumptions = objCostStructureAssumptions.GetIdCostStructureAssumptions(_IdZiffAccount, _IdPeerGroup);
                    foreach (DataRow drCostStructureAssumptions in dtCostStructureAssumptions.Rows)
                    {
                        objCostStructureAssumptions.UpdateRecord(Convert.ToInt32(drCostStructureAssumptions["IdCostStructureAssumptions"].ToString()), 0, _IdZiffAccount, 0, _Year, _IdPeerGroup, Convert.ToInt32(ddlField.Value));
                    }
                }
                else
                {
                    intFailedImportRowCount++;
                    strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
                }
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
            }
            catch
            {
            }
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
            this.fileImport.Reset();
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataClass.clsCostStructureAssumptions objCostStructureAssumptions = new DataClass.clsCostStructureAssumptions();
            DataTable dt = objCostStructureAssumptions.LoadExport(IdModel);

            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Peer Group Code", "Peer Group Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Cost Category Code", "Cost Category Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Year", "Year"));
            String strSelectedParameters = "";

            ExportTechnicalAssumption = new DataReportObject(dt, "DATASHEET", null, strSelectedParameters, null, null, datColumnTD, null);

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportTechnicalAssumption;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=3.7 ExportCostStructureAssumptions.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportExternalBaseCostReferences.xls", "template");
        }

        protected Int32 GetIdZiffAccount(String _CostCategory)
        {
            Int32 Value = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT IdZiffAccount FROM tblZiffAccounts WHERE IdModel = " + IdModel + " AND UPPER(Code) = '" + _CostCategory.ToUpper() + "'").Tables[0];
            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdZiffAccount"];
            }
            return Value;
        }

        #endregion

    }
}
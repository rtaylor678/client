﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _clientcostdata : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdClientCostData { get { return (Int32)Session["IdClientCostData"]; } set { Session["IdClientCostData"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private DataReportObject ExportClientCostData { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                clsModels objModels = new clsModels();
                this.colCost.Text = (String)GetGlobalResourceObject("CPM_Resources", "Cost") + " " + objModels.GetDefaultCurrencySymbol(IdModel);
                this.LoadLabel();
                IdClientCostData = 0;
                this.storeAccount.Reload();
                this.storeCostCenter.Reload();
                this.LoadClientCostData();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winClientCostDataEdit.Show();
        }

        protected void btnDeleteAll_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete all records?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDeleteAllYES()",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.Save();
                this.LoadClientCostData();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            winClientCostDataEdit.Hide();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.CostData);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Cost Object Code"].ToString(), row["Account Code"].ToString(), Convert.ToDouble(row["Amount"]));
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadClientCostData();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        protected void Clear()
        {
            this.IdClientCostData = 0;
            this.txtAmount.Value = 0;
            this.ddlCostCenter.Reset();
            this.ddlAccount.Reset();
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void grdClientCostData_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            IdClientCostData = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            if (Command == "Edit")
            {
                this.winClientCostDataEdit.Show();
                this.EditActivityLoad();
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + _Name + "?", new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedDeleteYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }      
        }

        protected void Save()
        {
            DataClass.clsClientCostData objClientCostData = new DataClass.clsClientCostData();
            objClientCostData.IdClientCostData = (Int32)IdClientCostData;
            objClientCostData.loadObject();

            if (IdClientCostData == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (objClientCostData.IdClientCostCenter != Convert.ToInt32(ddlCostCenter.Value))
                {
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }
            objClientCostData.IdModel = IdModel;
            objClientCostData.Amount = Convert.ToDouble(txtAmount.Value);
            objClientCostData.IdClientCostCenter = Convert.ToInt32(this.ddlCostCenter.Value);
            objClientCostData.IdClientAccount = Convert.ToInt32(this.ddlAccount.Value);
            if (IdClientCostData == 0)
            {
                objClientCostData.DateCreation = DateTime.Now;
                objClientCostData.UserCreation = (Int32)IdUserCreate;
                IdClientCostData = objClientCostData.Insert();
            }
            else
            {
                objClientCostData.DateModification = DateTime.Now;
                objClientCostData.UserModification = (Int32)IdUserCreate;
                objClientCostData.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winClientCostDataEdit.Hide();
            this.ModelUpdateTimestamp();
        }

        protected Int32 LoadIdCostCenter(String _Value)
        {
            Int32 IdClientCostCenter = 0;
            DataClass.clsClientCostCenter objClientCostCenter = new DataClass.clsClientCostCenter();
            DataTable dt = objClientCostCenter.LoadList("IdModel = " + IdModel + " AND Name LIKE '%" + ddlCostCenter.Text + "%'", "ORDER BY Name");    

            if (dt.Rows.Count>0)
            {
                IdClientCostCenter = (Int32)dt.Rows[0]["IdClientCostCenter"];
            }
            return IdClientCostCenter;
        }

        protected Int32 LoadIdAccount(String _Value)
        {
            Int32 IdAccount = 0;
            DataClass.clsClientAccounts objClientAccounts = new DataClass.clsClientAccounts();
            DataTable dt = objClientAccounts.LoadList("IdModel = " + IdModel + " AND Name LIKE '%" + ddlAccount.Text + "%'", "ORDER BY Name");

            if (dt.Rows.Count > 0)
            {
                IdAccount = (Int32)dt.Rows[0]["IdClientAccount"];
            }
            return IdAccount;
        }

        protected void SaveImport(String _CostCenter, String _Account, Double _Amount)
        {
            Int32 _IdCostCenter = GetIdCostCenter(_CostCenter);
            Int32 _IdAccount = GetIdAccount(_Account);

            if (_IdCostCenter != 0 && _IdAccount != 0 && _Amount != 0)
            {
                DataClass.clsClientCostData objClientCostData = new DataClass.clsClientCostData();
                objClientCostData.IdClientCostData = (Int32)IdClientCostData;
                objClientCostData.loadObject();
                objClientCostData.IdModel = IdModel;
                objClientCostData.IdClientCostCenter = _IdCostCenter;
                objClientCostData.IdClientAccount = _IdAccount;
                objClientCostData.Amount = _Amount;
                objClientCostData.DateCreation = DateTime.Now;
                objClientCostData.UserCreation = (Int32)IdUserCreate;
                IdClientCostData = objClientCostData.Insert();
                objClientCostData = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected Int32 GetIdCostCenter(String _CostCenter)
        {
            Int32 Value = 0;
            DataClass.clsClientCostCenter objClientCostCenter = new DataClass.clsClientCostCenter();
            DataTable dt = objClientCostCenter.LoadList("Code = '" + _CostCenter + "' AND IdModel = " + IdModel, "");
            
            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdClientCostCenter"];
            }
            return Value;
        }

        protected Int32 GetIdAccount(String _Account)
        {
            Int32 Value = 0;
            DataClass.clsClientAccounts objClientAccounts = new DataClass.clsClientAccounts();
            DataTable dt = objClientAccounts.LoadList("Account = '" + _Account + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdClientAccount"];
            }
            return Value;
        }

        protected void EditActivityLoad()
        {
            DataClass.clsClientCostData objClientCostData = new DataClass.clsClientCostData();
            DataTable dt = objClientCostData.LoadList("IdClientCostData = " + IdClientCostData, "");
            if (dt.Rows.Count > 0)
            {
                this.txtAmount.Value = Convert.ToString(dt.Rows[0]["Amount"]);

                this.ddlCostCenter.Value = dt.Rows[0]["IdClientCostCenter"];
                this.ddlAccount.Value = dt.Rows[0]["IdClientAccount"];
            }
        }

        protected void StoreCostCenter_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsClientCostCenter objClientCostCenter = new DataClass.clsClientCostCenter();
            this.storeCostCenter.DataSource = objClientCostCenter.LoadList("IdModel = " + IdModel, "ORDER BY Name");
            this.storeCostCenter.DataBind();
        }

        protected void StoreAccount_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsClientAccounts objClientAccounts = new DataClass.clsClientAccounts();
            this.storeAccount.DataSource = objClientAccounts.LoadAccountCeCo("IdModel = " + IdModel, "ORDER BY Name");
            this.storeAccount.DataBind();
        }

        public void Delete()
        {
            DataClass.clsClientCostData objClientCostData = new DataClass.clsClientCostData();
            objClientCostData.IdClientCostData = (Int32)IdClientCostData;
            objClientCostData.Delete();
            this.ModelUpdateTimestamp();
        }

        public void DeleteAll()
        {
            DataClass.clsClientCostData objClientCostData = new DataClass.clsClientCostData();
            objClientCostData.IdModel = (Int32)IdModel;
            objClientCostData.DeleteAll();
            this.ModelUpdateTimestamp();
        }

        [DirectMethod]
        public void ClickedDeleteYES()
        {
            this.Delete();
            this.LoadClientCostData();
        }

        [DirectMethod]
        public void ClickedDeleteAllYES()
        {
            this.DeleteAll();
            this.LoadClientCostData();
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsClientCostData objClientCostData = new DataClass.clsClientCostData();
            DataTable dt = objClientCostData.LoadList("IdModel = " + IdModel + " AND IdClientCostCenter = " + ddlCostCenter.Value + " AND IdClientAccount = " + ddlAccount.Value, "");
            Value = dt.Rows.Count;

            return Value;
        }

        protected void LoadClientCostData()
        {
            DataClass.clsClientCostData objClientCostData = new DataClass.clsClientCostData();

            DataTable dt = objClientCostData.LoadList("IdModel = " + IdModel, "ORDER BY ClientCostCenter");

            storeClientCostData.DataSource = dt;
            storeClientCostData.DataBind();

            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("CostObjectCode", "Cost Object Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("AccountCode", "Account Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Amount", "Amount"));
            String strSelectedParameters = "";
            String[] columnsToCopy = { "CostObjectCode", "AccountCode", "Amount" };
            System.Data.DataView view = new System.Data.DataView(dt);
            DataTable dtClientCostData = view.ToTable(true, columnsToCopy);
            ExportClientCostData = new DataReportObject(dtClientCostData, "DATASHEET", null, strSelectedParameters, null, "CostObjectCode", datColumnTD, null);
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportClientCostData;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=2.1 ExportBaseCostData.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportBaseCostData.xls", "template");
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        protected void LoadLabel()
        {
            this.grdClientCostData.Title = modMain.strMasterBaseCostData;
            this.btnAdd.Text = (String)GetGlobalResourceObject("CPM_Resources", "Add New Cost Data");
            this.btnAdd.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Add New Cost Data");
            this.btnDelete.Text = modMain.strCommonDeleteAll;
            this.btnDelete.ToolTip = modMain.strCommonDeleteAll;
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.colClientCostCenter.Text = (String)GetGlobalResourceObject("CPM_Resources", "Company Cost Object");
            this.colClientAccount.Text = (String)GetGlobalResourceObject("CPM_Resources", "Company Account");
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = "&nbsp;" + modMain.strCommonEdit;
            this.ImageCommandColumn1.Commands[1].Text = "&nbsp;" + modMain.strCommonDelete;
            this.winClientCostDataEdit.Title = modMain.strMasterBaseCostData;
            this.ddlCostCenter.FieldLabel = modMain.strCommonName;
            this.ddlAccount.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Account");
            this.txtAmount.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Amount");
            this.btnSave.Text = modMain.strCommonSave;
            this.btnSave.ToolTip = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.btnCancel.ToolTip = modMain.strCommonClose;
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnImportFile.ToolTip = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.btnCancelImport.ToolTip = modMain.strCommonClose;
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        #endregion

    }
}
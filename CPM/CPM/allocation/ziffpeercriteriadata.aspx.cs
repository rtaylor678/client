﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CD;
using DataClass;
using Ext.Net;
using System.Data;
using System.Web.Script.Serialization;

namespace CPM
{
    public partial class _ziffpeercriteriadata : System.Web.UI.Page
    {

        #region Declarations

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        private DataReportObject ExportPeerCriteriaData { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            this.LoadLabel();
        }

        protected void btnSaveQtys_Click(object sender, DirectEventArgs e)
        {
            String rowsValues = e.ExtraParams["rowsValues"];
            SaveQtys(rowsValues);
            LoadQty();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.ZiffPeerCriteriaData);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Criteria Desc"].ToString(), row["Peer Group Code"].ToString(), Convert.ToDouble(row["Value"]));
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadQty();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void StorePeerGroup_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsPeerGroup objPeerGroup = new DataClass.clsPeerGroup();
            this.storePeerGroup.DataSource = objPeerGroup.LoadList("IdModel = " + IdModel, "ORDER BY PeerGroupCode");
            this.storePeerGroup.DataBind();
        }

        protected void ddlPeerGroup_Select(object sender, DirectEventArgs e)
        {
            LoadQty();
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void LoadQty()
        {
            DataClass.clsPeerCriteriaCost objPeerCriteriaCost = new DataClass.clsPeerCriteriaCost();
            objPeerCriteriaCost.IdModel = IdModel;
            string idPeerGroup = ddlPeerGroup.Value.ToString();
            if (!String.IsNullOrEmpty(idPeerGroup))
            {
                DataTable dt = objPeerCriteriaCost.LoadList(" IdModel = " + IdModel + " AND IdPeerGroup = " + Convert.ToInt32(idPeerGroup), " ORDER BY PeerCriteria ");
                this.storePeerCriteriaData.DataSource = dt;
                this.storePeerCriteriaData.DataBind();

                //Generate Export Details
                ArrayList datColumnTD = new ArrayList();
                datColumnTD.Add(new DataClass.DataTableColumnDisplay("PeerCriteria", "Criteria Desc"));
                datColumnTD.Add(new DataClass.DataTableColumnDisplay("PeerGroupCode", "Peer Group Code"));
                datColumnTD.Add(new DataClass.DataTableColumnDisplay("Quantity", "Value"));
                String strSelectedParameters = "";
                String[] columnsToCopy = { "PeerCriteria", "PeerGroupCode", "Quantity" };
                System.Data.DataView view = new System.Data.DataView(dt);
                DataTable dtPeerCriteriaData = view.ToTable(true, columnsToCopy);
                ExportPeerCriteriaData = new DataReportObject(dtPeerCriteriaData, "DATASHEET", null, strSelectedParameters, null, "PeerCriteria", datColumnTD, null);
            }
            else
            {
                this.storePeerCriteriaData.RemoveAll();
            }
        } 

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportPeerCriteriaData;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=2.5.2 ExportPeerCriteriaData.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        protected void SaveQtys(String _Values)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("IdPeerCriteria", typeof(int));
            dt.Columns.Add("IdPeerGroup", typeof(int));
            dt.Columns.Add("Description", typeof(string));
            dt.Columns.Add("Quantity", typeof(float));

            List<Object> data = JSON.Deserialize<List<Object>>(_Values);

            foreach (Object value in data)
            {
                Object _ObjectRow = new Object();
                _ObjectRow = value.ToString();

                JavaScriptSerializer ser = new JavaScriptSerializer();

                Dictionary<string, object> _Row = ser.Deserialize<Dictionary<string, object>>(_ObjectRow.ToString());
                DataRow _NewRow = dt.NewRow();

                _NewRow["IdPeerCriteria"] = _Row["IdPeerCriteria"];
                _NewRow["IdPeerGroup"] = _Row["IdPeerGroup"].ToString();
                _NewRow["Description"] = _Row["PeerCriteria"].ToString();
                _NewRow["Quantity"] = _Row["Quantity"].ToString();

                dt.Rows.Add(_NewRow);
            }

            foreach (DataRow row in dt.Rows)
            {
                DataClass.clsPeerCriteriaCost objPeerCriteriaCost = new DataClass.clsPeerCriteriaCost();
                objPeerCriteriaCost.IdPeerCriteria = Convert.ToInt32(row["IdPeerCriteria"]);
                objPeerCriteriaCost.IdPeerGroup = Convert.ToInt32(row["IdPeerGroup"]);
                objPeerCriteriaCost.IdModel = IdModel;
                objPeerCriteriaCost.Quantity = Convert.ToDouble(row["Quantity"]);
                if (objPeerCriteriaCost.Quantity == 0)
                {
                    objPeerCriteriaCost.Delete();
                }
                else
                {
                    objPeerCriteriaCost.Update();
                }
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Record saved successfully!"), IconCls = "icon-accept", Clear2 = false });
        }

        protected void SaveImport(String _PeerCriteria, String _PeerGroupCode, Double _Value)
        {
            Int32 _IdPeerCriteria = GetIdPeerCriteria(_PeerCriteria);
            //Int32 _IdField = GetIdField(_Field);
            Int32 _IdPeerGroup = GetIdPeerGroup(_PeerGroupCode);
            if (_IdPeerCriteria != 0 && _IdPeerGroup != 0 && _Value != 0)
            {
                DataClass.clsPeerCriteriaCost objPeerCriteriaCost = new DataClass.clsPeerCriteriaCost();
                objPeerCriteriaCost.IdPeerCriteria = _IdPeerCriteria;
                objPeerCriteriaCost.IdPeerGroup = _IdPeerGroup;
                objPeerCriteriaCost.loadObject();
                objPeerCriteriaCost.Quantity = _Value;
                objPeerCriteriaCost.Insert();
                objPeerCriteriaCost = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected Int32 GetIdPeerCriteria(String _Criteria)
        {
            Int32 Value = 0;
            DataClass.clsPeerCriteria objPeerCriteria = new DataClass.clsPeerCriteria();
            DataTable dt = objPeerCriteria.LoadList("Description = '" + _Criteria + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdPeerCriteria"];
            }
            return Value;
        }

        protected Int32 GetIdField(String _Field)
        {
            Int32 Value = 0;
            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dt = objFields.LoadList("Code = '" + _Field + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdField"];
            }
            return Value;
        }

        protected Int32 GetIdPeerGroup(String _PeerGroupCode)
        {
            Int32 Value = 0;
            DataClass.clsPeerGroup objPeerGroup = new DataClass.clsPeerGroup();
            DataTable dt = objPeerGroup.LoadList("PeerGroupCode = '" + _PeerGroupCode + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdPeerGroup"];
            }
            return Value;
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportPeerCriteriaData.xls", "template");
        }
        
        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = modMain.strMasterPeerCriteriaData;
            this.lblFilterPeerGroup.Text = (String)GetGlobalResourceObject("CPM_Resources", "Peer Group");//modMain.strTechnicalModuleField;
            this.btnSaveQtys.Text = modMain.strCommonSaveChanges;
            this.btnSaveQtys.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Save Peer Criteria Data");
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.colPeerCriteria.Text = (String)GetGlobalResourceObject("CPM_Resources", "Criteria");
            this.colTypeUnit.Text = (String)GetGlobalResourceObject("CPM_Resources", "Unit Type");
            this.colUnit.Text = (String)GetGlobalResourceObject("CPM_Resources", "Unit");
            this.colQuantity.Text = (String)GetGlobalResourceObject("CPM_Resources", "Value");
            this.winImport.Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Peer Criteria Data");
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnImportFile.ToolTip = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.btnCancelImport.ToolTip = modMain.strCommonClose;
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        #endregion

    }
}
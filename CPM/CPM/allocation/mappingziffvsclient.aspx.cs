﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _mappingziffvsclient : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private DataReportObject ExportMappingZiffVSClient { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                this.LoadLabel();
                this.LoadMap();
            }
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnDeleteAll_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do unset all records?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDeleteAllYES()",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnCancelMap_Click(object sender, DirectEventArgs e)
        {
            this.winSetMap.Hide();
        }

        protected void btnSaveMap_Click(object sender, DirectEventArgs e)
        {
            this.SaveMap();
            this.LoadMap();
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.Mapping);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Cost Object Code"].ToString(), row["Account Code"].ToString(), row["External Account Code"].ToString());
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadMap();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        [DirectMethod]
        public void ClickedDeleteAllYES()
        {
            this.DeleteAll();
            this.LoadMap();
        }

        public void DeleteAll()
        {
            DataClass.clsZiffVSClientMap objZiffVSClientMap = new DataClass.clsZiffVSClientMap();
            objZiffVSClientMap.DeleteAll( IdModel);
            this.ModelUpdateTimestamp();
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        public Boolean GetProject()
        {
            Boolean Value = false;
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("Level = 1 AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = true;
            }

            return Value;
        }

        protected Int32 GetIdProfile()
        {
            Int32 Value = 0;
            DataClass.clsUsers objUsers = new DataClass.clsUsers();

            DataTable dt = objUsers.LoadList("IdUser = " + IdUserCreate, "");
            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdProfile"];
            }
            return Value;
        }

        protected void LoadMap()
        {
            DataClass.clsZiffVSClientMap objZiffVSClientMap = new DataClass.clsZiffVSClientMap();
            DataTable dt = objZiffVSClientMap.LoadMap(IdModel);
            this.storeMappZiffVsClient.DataSource = dt;
            this.storeMappZiffVsClient.DataBind();

            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Code", "Cost Object Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Account", "Account Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("ZiffCode", "External Account Code"));
            String strSelectedParameters = "";
            String[] columnsToCopy = { "Code", "Account", "ZiffCode" };
            System.Data.DataView view = new System.Data.DataView(dt);
            DataTable dtMappingZiffVSClient = view.ToTable(true, columnsToCopy);
            ExportMappingZiffVSClient = new DataReportObject(dtMappingZiffVSClient, "DATASHEET", null, strSelectedParameters, null, "Code", datColumnTD, null);
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportMappingZiffVSClient;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=2.4 ExportMapping.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        protected void grdMappingZiffVSClient_Command(object sender, DirectEventArgs e)
        {
            try
            {
                
                String Command = e.ExtraParams["command"].ToString();
                Int32 idCliAcnt = Convert.ToInt32(e.ExtraParams["IdClientAccount"].ToString());
                Int32 idCliCostCenter = Convert.ToInt32(e.ExtraParams["IdClientCostCenter"].ToString());
                String CliAcnt = e.ExtraParams["ClientAccount"].ToString();
                String IdZiffAccount = e.ExtraParams["IdZiffAccount"].ToString();
                String CliAcntDesc = e.ExtraParams["CliGenAccountDesc"].ToString();
                
                if (Command == "Set")
                {
                    clsZiffVSClientMap objZiffVSClientMap = new clsZiffVSClientMap();
                    this.txtIDCliCostCenter.Text = idCliCostCenter.ToString();
                    this.txtIDCliAccount.Text = idCliAcnt.ToString();
                    this.txtCliAccount.Text = CliAcnt.ToString();
                    this.txtCliAccountDes.Text = CliAcntDesc.ToString();
                    if (IdZiffAccount == "null")
                    {
                        this.ddlZiffAccount.Value = null;
                    }
                    else
                    {
                        this.ddlZiffAccount.Value = Convert.ToInt32(IdZiffAccount);
                    }
                    this.winSetMap.Show();
                }
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
            }
        }

        protected void ZiffAccounts_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsZiffAccount objZiffAccount = new DataClass.clsZiffAccount();
            this.storeZiffAccounts.DataSource = objZiffAccount.LoadList("IdModel = " + IdModel + " AND Parent IS NOT NULL", "ORDER BY Code, Name");
            this.storeZiffAccounts.DataBind();
        }        

        protected void SaveMap()
        {
            clsZiffVSClientMap objZiffVSClientMap = new clsZiffVSClientMap();

            Int32 IdCliAcnt = Convert.ToInt32(txtIDCliAccount.Value.ToString());
            Int32 IdCliCoCe = Convert.ToInt32(txtIDCliCostCenter.Value.ToString());
            Int32 IdZiffAcnt = Convert.ToInt32(ddlZiffAccount.Value.ToString());

            objZiffVSClientMap.SaveMap(IdCliAcnt, IdCliCoCe, IdZiffAcnt);
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winSetMap.Hide();
        }

        protected void SaveImport(String _CostCenter, String _ClientAccount, String _ZiffAccount)
        {
            Int32 _IdCostCenter = GetIdCostCenter(_CostCenter);
            Int32 _IdClientAccount = GetIdAccount(_ClientAccount);
            Int32 _IdZiffAccount = GetIdZiffAccount(_ZiffAccount);

            if (_IdCostCenter != 0 && _IdClientAccount != 0 && _IdZiffAccount != 0)
            {
                clsZiffVSClientMap objZiffVSClientMap = new clsZiffVSClientMap();
                objZiffVSClientMap.SaveMap(_IdClientAccount, _IdCostCenter, _IdZiffAccount);
                objZiffVSClientMap = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected Int32 GetIdCostCenter(String _CostCenter)
        {
            Int32 Value = 0;
            DataClass.clsClientCostCenter objClientCostCenter = new DataClass.clsClientCostCenter();
            DataTable dt = objClientCostCenter.LoadList("Code = '" + _CostCenter + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdClientCostCenter"];
            }
            return Value;
        }

        protected Int32 GetIdAccount(String _Account)
        {
            Int32 Value = 0;
            DataClass.clsClientAccounts objClientAccounts = new DataClass.clsClientAccounts();
            DataTable dt = objClientAccounts.LoadList("Account = '" + _Account + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdClientAccount"];
            }
            return Value;
        }

        protected Int32 GetIdZiffAccount(String _Account)
        {
            Int32 Value = 0;
            DataClass.clsZiffAccount objZiffAccount = new DataClass.clsZiffAccount();
            DataTable dt = objZiffAccount.LoadList("Code = '" + _Account + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdZiffAccount"];
            }
            return Value;
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportMapping.xls", "template");
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        protected void LoadLabel()
        {
            this.GridPanel2.Title = modMain.strMasterMapping;
            this.btnDelete.Text = (String)GetGlobalResourceObject("CPM_Resources", "Unset All");
            this.btnDelete.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Unset All");
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.Column8.Text = (String)GetGlobalResourceObject("CPM_Resources", "Company Cost Object & Account");
            this.Column9.Text = (String)GetGlobalResourceObject("CPM_Resources", "Company Cost Object & Account Description");
            this.colBaseCost.Text = modMain.strMasterBaseCostData;
            this.Column10.Text= (String)GetGlobalResourceObject("CPM_Resources", "Ziff Account");
            this.Column11.Text = (String)GetGlobalResourceObject("CPM_Resources", "Ziff Account Description");
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = (String)GetGlobalResourceObject("CPM_Resources", "Set");
            this.winSetMap.Title = modMain.strMasterZiff_Third_partyBaseCost;
            this.txtCliAccount.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Company Account");
            this.txtCliAccountDes.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Company Gen.Account Desc.");
            this.btnSave.Text = modMain.strCommonSave;
            this.btnSave.ToolTip = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.btnCancel.ToolTip = modMain.strCommonClose;
            this.winImport.Title = modMain.strCommonImport + " " + modMain.strMasterMapping;
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnImportFile.ToolTip = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.btnCancelImport.ToolTip = modMain.strCommonClose;
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.ddlZiffAccount.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Ziff Account");
        }
        
        #endregion

    }
}
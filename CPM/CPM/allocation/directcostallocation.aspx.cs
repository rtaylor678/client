﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _directcostallocation : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdDirectCostAllocation { get { return (Int32)Session["IdDirectCostAllocation"]; } set { Session["IdDirectCostAllocation"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private DataReportObject ExportDirectCostAllocation { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                clsModels objModels = new clsModels();
                this.colCost.Text = (String)GetGlobalResourceObject("CPM_Resources", "Cost") + " " + objModels.GetDefaultCurrencySymbol(IdModel);
                IdDirectCostAllocation = 0;
                this.LoadDirectCostAllocation();
                this.LoadLabel();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winDirectCostAllocationEdit.Show();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.Save();
                this.LoadDirectCostAllocation();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            winDirectCostAllocationEdit.Hide();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.DirectCostAllocation);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Cost Object Code"].ToString(), row["Field Code"].ToString());
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadDirectCostAllocation();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        protected void Clear()
        {
            this.IdDirectCostAllocation = 0;
            this.ddlCostCenter.Reset();
            this.ddlField.Reset();
            this.storeCostCenter.Reload();
            this.storeField.Reload();
            this.ddlField.ReadOnly = true;
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void grdDirectCostAllocation_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            IdDirectCostAllocation = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            if (Command == "Edit")
            {
                this.winDirectCostAllocationEdit.Show();
                this.EditDirectLoad();
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + _Name + "?", new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedDeleteYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }      
        }

        protected void Save()
        {
            DataClass.clsDirectCostAllocation objDirectCostAllocation = new DataClass.clsDirectCostAllocation();
            objDirectCostAllocation.IdDirectCostAllocation = (Int32)IdDirectCostAllocation;
            objDirectCostAllocation.loadObject();

            if (IdDirectCostAllocation == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                //if (objClientCostData.cli != txtCode.Text)
                //{
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                //}
            }
            objDirectCostAllocation.IdModel = IdModel;
            objDirectCostAllocation.Cost = Convert.ToDouble(0);
            objDirectCostAllocation.IdClientCostCenter = Convert.ToInt32(this.ddlCostCenter.Value);
            objDirectCostAllocation.IdField = Convert.ToInt32(this.ddlField.Value);
            if (IdDirectCostAllocation == 0)
            {
                objDirectCostAllocation.DateCreation = DateTime.Now;
                objDirectCostAllocation.UserCreation = (Int32)IdUserCreate;
                IdDirectCostAllocation = objDirectCostAllocation.Insert();
            }
            else
            {
                objDirectCostAllocation.DateModification = DateTime.Now;
                objDirectCostAllocation.UserModification = (Int32)IdUserCreate;
                objDirectCostAllocation.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winDirectCostAllocationEdit.Hide();
            this.ModelUpdateTimestamp();
        }

        protected Int32 LoadIdCostCenter(String _Value)
        {
            Int32 IdClientCostCenter = 0;
            DataClass.clsClientCostCenter objClientCostCenter = new DataClass.clsClientCostCenter();
            DataTable dt = objClientCostCenter.LoadList("IdModel = " + IdModel + " AND Name LIKE '%" + _Value + "%'", "ORDER BY Name");    

            if (dt.Rows.Count>0)
            {
                IdClientCostCenter = (Int32)dt.Rows[0]["IdClientCostCenter"];
            }
            return IdClientCostCenter;
        }

        protected Int32 LoadIdField(String _Value)
        {
            Int32 IdAccount = 0;
            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dt = objFields.LoadList("IdModel = " + IdModel + " AND Name LIKE '%" + _Value + "%'", "ORDER BY Name");

            if (dt.Rows.Count > 0)
            {
                IdAccount = (Int32)dt.Rows[0]["IdField"];
            }
            return IdAccount;
        }

        protected void SaveImport(String _CostCenter, String _Field)
        {
            Int32 _IdCostCenter = GetIdCostCenter(_CostCenter);
            Int32 _IdField = GetIdField(_Field);

            if (_IdCostCenter != 0 && _IdField != 0)
            {
                DataClass.clsDirectCostAllocation objDirectCostAllocation = new DataClass.clsDirectCostAllocation();
                objDirectCostAllocation.IdDirectCostAllocation = (Int32)IdDirectCostAllocation;
                objDirectCostAllocation.loadObject();
                objDirectCostAllocation.IdModel = IdModel;
                objDirectCostAllocation.IdClientCostCenter = _IdCostCenter;
                objDirectCostAllocation.IdField = _IdField;
                objDirectCostAllocation.DateCreation = DateTime.Now;
                objDirectCostAllocation.UserCreation = (Int32)IdUserCreate;
                IdDirectCostAllocation = objDirectCostAllocation.Insert();
                objDirectCostAllocation = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected Int32 GetIdCostCenter(String _CostCenter)
        {
            Int32 Value = 0;
            DataClass.clsClientCostCenter objClientCostCenter = new DataClass.clsClientCostCenter();
            DataTable dt = objClientCostCenter.LoadList("Code = '" + _CostCenter + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdClientCostCenter"];
            }
            return Value;
        }

        protected Int32 GetIdField(String _Field)
        {
            Int32 Value = 0;
            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dt = objFields.LoadList("Code = '" + _Field + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdField"];
            }
            return Value;
        }

        protected void EditDirectLoad()
        {
            DataClass.clsDirectCostAllocation objDirectCostAllocation = new DataClass.clsDirectCostAllocation();
            DataTable dt = objDirectCostAllocation.LoadList("IdDirectCostAllocation = " + IdDirectCostAllocation, "");
            if (dt.Rows.Count > 0)
            {
                this.ddlCostCenter.Value = (Int32)(dt.Rows[0]["IdClientCostCenter"]); ;
                this.ddlField.Value = (Int32)(dt.Rows[0]["IdField"]);
                this.storeCostCenter.Reload();
                this.storeField.Reload();
                this.ddlField.ReadOnly = false;
            }
        }

        protected void Activity_Select(object sender, DirectEventArgs e)
        {
            this.ddlCostCenter.Text = null;
            this.ddlField.Text = null;

            this.storeCostCenter.Reload();
            this.storeField.Reload();
        }

        protected void StoreCostCenter_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsClientCostCenter objClientCostCenter = new DataClass.clsClientCostCenter();
            if (IdDirectCostAllocation == 0)
            {
                this.storeCostCenter.DataSource = objClientCostCenter.LoadList("IdModel = " + IdModel + " AND TypeAllocation = 1 AND IdClientCostCenter NOT IN (SELECT IdClientCostCenter FROM tblDirectCostAllocation WHERE IdModel = " + IdModel + ")", "ORDER BY Name");
            }
            else
            {
                this.storeCostCenter.DataSource = objClientCostCenter.LoadList("IdModel = " + IdModel + " AND TypeAllocation = 1 AND IdClientCostCenter = " + ddlCostCenter.Value, "ORDER BY Name");
            }
            this.storeCostCenter.DataBind();
        }

        protected void StoreField_ReadData(object sender, StoreReadDataEventArgs e)
        {
            if (ddlCostCenter.Value != null && ddlCostCenter.Value.ToString() != "")
            {
                DataClass.clsDirectCostAllocation objDirectCostAllocation = new DataClass.clsDirectCostAllocation();

                objDirectCostAllocation.IdDirectCostAllocation = (Int32)IdDirectCostAllocation;
                objDirectCostAllocation.IdModel = (Int32)IdModel;
                DataTable dt = objDirectCostAllocation.LoadField(Convert.ToInt32(ddlCostCenter.Value));

                this.storeField.DataSource = dt;
                this.storeField.DataBind();
            }
            else
            {
                this.storeField.RemoveAll();
            }
        }

        protected void ddlCostCenter_Select(object sender, DirectEventArgs e)
        {
            if (ddlCostCenter.Value != null && ddlCostCenter.Value.ToString() != "")
            {
                this.ddlField.ReadOnly = false;
            }
            else
            {
                this.ddlField.ReadOnly = true;
            }
            this.storeField.Reload();
        }

        public void Delete()
        {
            DataClass.clsDirectCostAllocation objDirectCostAllocation = new DataClass.clsDirectCostAllocation();
            objDirectCostAllocation.IdDirectCostAllocation = (Int32)IdDirectCostAllocation;
            objDirectCostAllocation.Delete();
            this.ModelUpdateTimestamp();
        }

        [DirectMethod]
        public void ClickedDeleteYES()
        {
            this.Delete();
            this.LoadDirectCostAllocation();
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            //DataClass.clsDirectCostAllocation objDirectCostAllocation = new DataClass.clsDirectCostAllocation();
            //DataTable dt = objDirectCostAllocation.LoadList("Code = '" + txtCode.Text + "'", "");
            //Value = dt.Rows.Count;

            return Value;
        }

        protected void LoadDirectCostAllocation()
        {
            DataClass.clsDirectCostAllocation objDirectCostAllocation = new DataClass.clsDirectCostAllocation();

            DataTable dt = objDirectCostAllocation.LoadList("IdModel = " + IdModel, "ORDER BY ClientCostCenter");

            storeDirectCostAllocation.DataSource = dt;
            storeDirectCostAllocation.DataBind();

            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("CostObjectCode", "Cost Object Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("FieldCode", "Field Code"));
            String strSelectedParameters = "";
            String[] columnsToCopy = { "CostObjectCode", "FieldCode" };
            System.Data.DataView view = new System.Data.DataView(dt);
            DataTable dtDirectCostAllocation = view.ToTable(true, columnsToCopy);
            ExportDirectCostAllocation = new DataReportObject(dtDirectCostAllocation, "DATASHEET", null, strSelectedParameters, null, "CostObjectCode", datColumnTD, null);
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportDirectCostAllocation;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=2.2.1 ExportDirectCostAllocation.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportDirectCostAllocation.xls", "template");
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        protected void LoadLabel()
        {
            this.grdDirectCostAllocation.Title = (String)GetGlobalResourceObject("CPM_Resources", "Direct Cost Allocation");
            this.btnAdd.Text = (String)GetGlobalResourceObject("CPM_Resources", "Add New Direct Allocation");
            this.btnAdd.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Add New Direct Allocation");
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.colClientCostCenter.Text = (String)GetGlobalResourceObject("CPM_Resources", "Company Cost Object");
            this.colActivity.Text = (String)GetGlobalResourceObject("CPM_Resources", "Activity");
            this.colField.Text = modMain.strTechnicalModuleField.Remove(modMain.strTechnicalModuleField.Length-1,1);
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = "&nbsp;" + modMain.strCommonEdit;
            this.ImageCommandColumn1.Commands[1].Text = "&nbsp;" + modMain.strCommonDelete;
            this.winDirectCostAllocationEdit.Title = (String)GetGlobalResourceObject("CPM_Resources", "Direct Cost Allocation");
            this.ddlCostCenter.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Cost Object");
            this.ddlField.FieldLabel = modMain.strTechnicalModuleField;
            this.btnSave.Text = modMain.strCommonSave;
            this.btnSave.ToolTip = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.btnCancel.ToolTip = modMain.strCommonClose;
            this.winImport.Title = modMain.strCommonImport + " " + (String)GetGlobalResourceObject("CPM_Resources", "Direct Cost Allocation");
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnImportFile.ToolTip = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.btnCancelImport.ToolTip = modMain.strCommonClose;
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        #endregion

    }
}
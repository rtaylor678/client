﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Xml;
using CD;
using DataClass;
using System.ComponentModel;
using System.Web.Script.Serialization;

namespace CPM
{

    public class QueryPar
    {
        public String query = "";
    }

    public partial class _ziffbasecost : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdZiffBaseCost { get { return (Int32)Session["IdZiffBaseCost"]; } set { Session["IdZiffBaseCost"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        public static Int32 IdPeerGroup;
        private DataReportObject ExportZiffBaseCost { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }
        public DataTable datViewData { get; set; }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                this.LoadLabel();
                clsModels objModels = new clsModels();
                //this.colCostUnit.Text = (String)GetGlobalResourceObject("CPM_Resources", "Unit Cost") + " " + objModels.GetDefaultCurrencySymbol(IdModel);
                //this.colCost.Text = (String)GetGlobalResourceObject("CPM_Resources", "Total Cost") + " " + objModels.GetDefaultCurrencySymbol(IdModel);
                IdZiffBaseCost = 0;
                ddlCriteria.GetStore().Reload();
                DataTable dtParams = new DataTable();
                DataTable dtBase = new DataTable();
                this.FillGridPeerCriteria(storeParameters, grdParameters, dtParams);
                this.FillGrid(storeBaseCost, grdBaseCost, dtBase);
            }
        }

        protected void btnSavePeer_Click(object sender, DirectEventArgs e)
        {
            this.SaveCosts();

            this.storeParameters.DataSource = this.PeerCriteriaData(IdPeerGroup);
            this.storeBaseCost.DataSource = this.ZiffBaseCosts(IdPeerGroup);

            this.RunCalculation();
        }

        protected void btnCancelPeer_Click(object sender, DirectEventArgs e)
        {
            this.winSetBaseCost.Hide();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.ZiffBaseCost);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Peer Group Code"].ToString(), row["Cost Category Code"].ToString(), Convert.ToDouble(row["Unit Cost"]));
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    DataClass.clsZiffBaseCost objZiffBaseCost = new DataClass.clsZiffBaseCost();
                    objZiffBaseCost.IdModel = IdModel;
                    objZiffBaseCost.UpdateQuantity();
                    objZiffBaseCost = null;
                    this.ModelUpdateTimestamp();
                    this.ModelCheckRecalcModuleAllocation();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadData();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void grdZiffBaseCosts_Command(object sender, DirectEventArgs e)
        {
            try
            {
                String Command = e.ExtraParams["command"].ToString();
                IdZiffBaseCost = Convert.ToInt32(e.ExtraParams["IdZiffBaseCost"].ToString());
                Int32 idPeer = Convert.ToInt32(e.ExtraParams["IdPeerCriteria"].ToString());
                Int32 idField = Convert.ToInt32(e.ExtraParams["IdField"].ToString());
                Int32 idZiffAccount = Convert.ToInt32(e.ExtraParams["IdZiffAccount"].ToString());

                if (Command == "Set")
                {
                    this.txtIDPeer.Text = idPeer.ToString();
                    this.txtIDField.Text = idField.ToString();
                    this.txtIDZiffAcnt.Text = idZiffAccount.ToString();
                    if (idPeer > 0)
                    {
                        ddlCriteria.Value = idPeer;
                    }
                    else
                    {
                        ddlCriteria.Value = -1;
                    }
                    clsZiffBaseCost objZiffBaseCost = new clsZiffBaseCost();
                    txtCost.Value = objZiffBaseCost.GetCostPeerCriteria(IdZiffBaseCost,idPeer,idZiffAccount);// idField, idPeer, idZiffAccount);
                    this.winSetBaseCost.Show();
                }

                if (Command == "Unset")
                {
                    if (idPeer > 0 && idField > 0 && idZiffAccount > 0)
                    {
                        X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to unset the selected value?"), new Ext.Net.MessageBoxButtonsConfig
                        {
                            Yes = new MessageBoxButtonConfig
                            {
                                Handler = "UsersX.ClickedUnsetYES(" + IdZiffBaseCost.ToString() + ", " + idField.ToString() + ")",
                                Text = modMain.strCommonYes
                            },
                            No = new MessageBoxButtonConfig
                            {
                                Text = modMain.strCommonNo
                            }
                        }).Show();
                    }
                }
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
            }
        }

        protected void UnitPeerCriteria_ReadData(object sender, StoreReadDataEventArgs e)
        {
            /*
             * DataClass.clsPeerCriteria objPeerCriteria = new DataClass.clsPeerCriteria();
            this.storeUnitPeerCriteria.DataSource = objPeerCriteria.LoadUnitList("Description like ('%" + ddlUnitPeerCriteria.Text + "%')", "ORDER BY Description");
            this.storeUnitPeerCriteria.DataBind();
             * */
        }

        protected void StorePeerGroup_ReadData(object sender, StoreReadDataEventArgs e)
        {
            ////Added a Distinct to the query as it was returning duplicate data - R Manubens 2014-06-26
            //QueryPar objPar = (QueryPar)JSON.Deserialize("{" + e.Parameters[0].ToString() + "}", typeof(QueryPar));
            //this.storePeerGroup.DataSource = CD.DAC.ConsultSQL("SELECT DISTINCT * FROM vListFields WHERE Name LIKE '%" + objPar.query + "%' AND IdModel = " + IdModel + " ORDER BY Name").Tables[0];
            //this.storePeerGroup.DataBind();
            DataClass.clsPeerGroup objPeerGroup = new DataClass.clsPeerGroup();
            DataTable dtPeerGroup = new DataTable();
            dtPeerGroup = objPeerGroup.LoadList("IdModel = " + IdModel, "ORDER BY PeerGroupCode");

            DataRow drPeerGroup = dtPeerGroup.NewRow();
            drPeerGroup["IdPeerGroup"] = 0;
            drPeerGroup["PeerGroupDescription"] = "- All -";
            dtPeerGroup.Rows.Add(drPeerGroup);
            dtPeerGroup.AcceptChanges();
            dtPeerGroup.DefaultView.Sort = "IdPeerGroup";
            DataTable dtPeerGroupSort = dtPeerGroup.DefaultView.ToTable();

            this.storePeerGroup.DataSource = dtPeerGroupSort;
            this.storePeerGroup.DataBind();
        }

        protected void StorePeerCriteria_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsPeerCriteria objPeerCriteria = new DataClass.clsPeerCriteria();
            this.storePeerCriteria.DataSource = objPeerCriteria.LoadList("IdModel = " + IdModel + " AND Description like ('%" + ddlCriteria.Text + "%')", "ORDER BY Description");
            this.storePeerCriteria.DataBind();
        }

        protected void ddlPeerGroup_Select(object sender, DirectEventArgs e)
        {
            this.ModelCheckRecalcModuleAllocation();
            if (ddlPeerGroup.Text != "")
            {
                String cad = ddlPeerGroup.Value.ToString();
                //this.RunCalculation();
                this.LoadData();
            }
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnSaveGrid_Click(object sender, DirectEventArgs e)
        {
            try
            {
                String strData = e.ExtraParams["rowsValues"];
                datViewData = JSON.Deserialize<DataTable>(strData);
                this.SaveGrid();
                this.LoadData();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        #endregion

        #region Methods

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void SaveCosts()
        {
            DataClass.clsZiffBaseCost objZiffBaseCost = new DataClass.clsZiffBaseCost();
            objZiffBaseCost.IdZiffBaseCost = IdZiffBaseCost;
            objZiffBaseCost.loadObject();
            objZiffBaseCost.IdModel = IdModel;
            objZiffBaseCost.IdZiffAccount = Convert.ToInt32(txtIDZiffAcnt.Value.ToString());
            objZiffBaseCost.IdPeerCriteria = Convert.ToInt32(ddlCriteria.Value.ToString());
            objZiffBaseCost.UnitCost = Convert.ToDouble(txtCost.Value.ToString());
            objZiffBaseCost.TotalCost = objZiffBaseCost.Quantity * objZiffBaseCost.UnitCost;
            objZiffBaseCost.IdPeerGroup = IdPeerGroup;
            objZiffBaseCost.Update();
            objZiffBaseCost.UpdateQuantity();

            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Record saved successfully!"), IconCls = "icon-accept", Clear2 = false });
            this.winSetBaseCost.Hide();
            this.ModelUpdateTimestamp();
            this.ModelCheckRecalcModuleAllocation();
        }

        protected void SaveImport(String _PeerGroupCode, String _ZiffAccount, Double _UnitCost)
        {
            Int32 _IdPeerGroup = GetIdPeerGroup(_PeerGroupCode);
            Int32 _IdZiffAccount = GetIdZiffAccount(_ZiffAccount);
            Int32 _IdPeerCriteria = 0;

            DataClass.clsZiffBaseCost objZiffBaseCost = new DataClass.clsZiffBaseCost();
            objZiffBaseCost.IdZiffBaseCost = IdZiffBaseCost;
            objZiffBaseCost.loadObject();
            objZiffBaseCost.IdPeerGroup = _IdPeerGroup;
            objZiffBaseCost.IdZiffAccount = _IdZiffAccount;
            objZiffBaseCost.IdPeerCriteria = _IdPeerCriteria;
            objZiffBaseCost.UnitCost = _UnitCost;
            objZiffBaseCost.IdZiffBaseCost = objZiffBaseCost.SearchZiffBaseCost();

            if (_IdPeerGroup != 0 && _UnitCost != 0)
            {
                if (objZiffBaseCost.IdZiffBaseCost == 0)
                {
                    IdZiffBaseCost = objZiffBaseCost.Insert();
                    objZiffBaseCost.UpdateQuantity();
                }
                else
                {
                    objZiffBaseCost.Update();
                }
                objZiffBaseCost = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected void SaveCostCategory(String _PeerGroupCode, String _ZiffAccount, Double _UnitCost)
        {
            Int32 _IdPeerGroup = GetIdPeerGroup(_PeerGroupCode);
            Int32 _IdZiffAccount = GetIdZiffAccount(_ZiffAccount);
            Int32 _IdPeerCriteria = 0;

            DataClass.clsZiffBaseCost objZiffBaseCost = new DataClass.clsZiffBaseCost();
            objZiffBaseCost.IdZiffBaseCost = IdZiffBaseCost;
            objZiffBaseCost.loadObject();
            objZiffBaseCost.IdPeerGroup = _IdPeerGroup;
            objZiffBaseCost.IdZiffAccount = _IdZiffAccount;
            objZiffBaseCost.IdPeerCriteria = _IdPeerCriteria;
            objZiffBaseCost.UnitCost = _UnitCost;
            objZiffBaseCost.IdZiffBaseCost = objZiffBaseCost.SearchZiffBaseCost();

            if (_IdPeerGroup != 0 && _UnitCost != 0)
            {
                if (objZiffBaseCost.IdZiffBaseCost == 0)
                {
                    IdZiffBaseCost = objZiffBaseCost.Insert();
                    objZiffBaseCost.UpdateQuantity();
                }
                objZiffBaseCost = null;
            }
        }

        protected void SaveGrid()
        {
            foreach (DataRow row in datViewData.Rows)
            {
                for (int w = 2; w < this.datViewData.Columns.Count; w++)
                {
                    String ColumnName = datViewData.Columns[w].ColumnName.ToString();
                    if (row[0].ToString().IndexOf("-") == 4)
                    {
                        DataClass.clsZiffBaseCost objZiffBaseCost = new DataClass.clsZiffBaseCost();
                        objZiffBaseCost.IdModel = IdModel;
                        objZiffBaseCost.UnitCost = Convert.ToDouble(row[w]);
                        SaveCostCategory(ColumnName, row[0].ToString().Substring(6, row[0].ToString().Length - 6), Convert.ToDouble(row[w]));
                        objZiffBaseCost.UpdateQuantity(row[0].ToString().Substring(6, row[0].ToString().Length - 6), ColumnName, Convert.ToDouble(row[w]));
                    }
                }
            }
            this.ModelUpdateTimestamp();
            this.ModelCheckRecalcModuleAllocation();
        }

        protected Int32 GetIdField(String _Field)
        {
            Int32 Value = 0;
            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dt = objFields.LoadList("Code = '" + _Field + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdField"];
            }
            return Value;
        }

        protected Int32 GetIdZiffAccount(String _Account)
        {
            Int32 Value = 0;
            DataClass.clsZiffAccount objZiffAccount = new DataClass.clsZiffAccount();
            DataTable dt = objZiffAccount.LoadList("Code = '" + _Account + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdZiffAccount"];
            }
            return Value;
        }

        protected Int32 GetIdPeerCriteria(String _Criteria)
        {
            Int32 Value = 0;
            DataClass.clsPeerCriteria objPeerCriteria = new DataClass.clsPeerCriteria();
            DataTable dt = objPeerCriteria.LoadList("Description = '" + _Criteria + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdPeerCriteria"];
            }
            return Value;
        }

        protected void RunCalculation()
        {
            if (ddlPeerGroup.Text == "")
            {
                return;
            }
            //Int32 _IdPeerGroup = GetIdPeerGroup(_PeerGroupCode);

            IdPeerGroup = Convert.ToInt32(ddlPeerGroup.Value);

            if (IdPeerGroup == 0)
            {
                this.storeParameters.Reload();
                this.storeBaseCost.Reload();
            }
            else
            {
                DataTable dtParams = PeerCriteriaData(Convert.ToInt32(IdPeerGroup));
                this.storeParameters.DataSource = dtParams;
                DataTable dtBase = ZiffBaseCosts(Convert.ToInt32(IdPeerGroup));
                this.storeBaseCost.DataSource = dtBase;

                if (dtParams.Rows.Count > 0)
                {
                    //PeerGroupCode is in the dtParams datatable so we need to add it to data from dtBase datatable into a temporary dtCombined datatable
                    String strPeerGroupCode = dtParams.Rows[0]["PeerGroupCode"].ToString();

                    DataTable dtCombined = new DataTable();
                    dtCombined = dtBase.Copy();
                    dtCombined.Columns.Add("PeerGroupCode", typeof(System.String));
                    foreach (DataRow dr in dtCombined.Rows)
                    {
                        dr["PeerGroupCode"] = strPeerGroupCode;
                        //dr["PeerCriteria"] = dtParams.Rows[0]["PeerCriteria"].ToString();
                    }

                    //Generate Export Details
                    ArrayList datColumnTD = new ArrayList();
                    datColumnTD.Add(new DataClass.DataTableColumnDisplay("PeerGroupCode", "Peer Group Code"));
                    datColumnTD.Add(new DataClass.DataTableColumnDisplay("ExternalAccountCode", "Cost category Code"));
                    //datColumnTD.Add(new DataClass.DataTableColumnDisplay("PeerCriteria", "Criteria Desc"));
                    datColumnTD.Add(new DataClass.DataTableColumnDisplay("UnitCost", "Unit Cost"));
                    String strSelectedParameters = "";
                    //String[] columnsToCopy = { "FieldCode", "ExternalAccountCode", "PeerCriteria", "UnitCost" };
                    //String[] columnsToCopy = { "PeerGroupCode", "ExternalAccountCode", "PeerCriteria", "UnitCost" };
                    String[] columnsToCopy = { "PeerGroupCode", "ExternalAccountCode", "UnitCost" };
                    System.Data.DataView view = new System.Data.DataView(dtCombined);
                    DataTable dtZiffBaseCost = view.ToTable(true, columnsToCopy);
                    //Clean up export data as to not include header row which have NULL External Account Code and -99 cost
                    for (int i = dtZiffBaseCost.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dtZiffBaseCost.Rows[i];
                        if (DBNull.Value.Equals(dr["ExternalAccountCode"]))
                            dr.Delete();
                    }
                    //ExportZiffBaseCost = new DataReportObject(dtZiffBaseCost, "DATASHEET", null, strSelectedParameters, null, "FieldCode", datColumnTD, null);
                    ExportZiffBaseCost = new DataReportObject(dtZiffBaseCost, "DATASHEET", null, strSelectedParameters, null, "PeerGroupCode", datColumnTD, null);
                }
            }
            this.storeParameters.DataBind();
            this.storeBaseCost.DataBind();

        }

        public DataTable PeerCriteriaData(int _IDPeerGroup)
        {
            DataClass.clsPeerCriteriaCost objPeerCriteriaCost = new DataClass.clsPeerCriteriaCost();
            objPeerCriteriaCost.IdModel = this.IdModel;
            objPeerCriteriaCost.IdPeerGroup = _IDPeerGroup;
            //if (_IDPeerGroup == 0)
            //    return objPeerCriteriaCost.LoadList(" IdModel = " + IdModel + " AND IdPeerGroup IN (SELECT IdPeerGroup FROM tblPeerGroup WHERE IdModel = " + IdModel + ")", " ORDER BY PeerCriteria ");
            //else
            //    return objPeerCriteriaCost.LoadList(" IdModel = " + IdModel + " AND IdPeerGroup = " + _IDPeerGroup, " ORDER BY PeerCriteria ");
            return objPeerCriteriaCost.LoadListPeerCriteria();
        }

        public DataTable ZiffBaseCosts(int _IDPeerGroup)
        {
            DataClass.clsZiffBaseCost objZiffBaseCost = new DataClass.clsZiffBaseCost();
            objZiffBaseCost.IdModel = this.IdModel;
            objZiffBaseCost.IdPeerGroup = _IDPeerGroup;
            return objZiffBaseCost.LoadListExternalBaseCostReferences();//LoadList(" IdModel = " + IdModel + " AND IdField = " + Convert.ToInt32(_IDField), " ORDER BY ZiffAccount ");
        }

        protected void LoadData()
        {
            try
            {
                IdPeerGroup = Convert.ToInt32(ddlPeerGroup.Value);
                DataTable dtParams = PeerCriteriaData(Convert.ToInt32(IdPeerGroup));
                this.FillGridPeerCriteria(storeParameters, grdParameters, dtParams);

                DataTable dtBase = ZiffBaseCosts(Convert.ToInt32(IdPeerGroup));
                this.FillGrid(storeBaseCost, grdBaseCost, dtBase);

                DataClass.clsZiffBaseCost objZiffBaseCost = new DataClass.clsZiffBaseCost();
                objZiffBaseCost.IdModel = IdModel;
                objZiffBaseCost.IdPeerGroup = IdPeerGroup;
                DataTable dtExport = objZiffBaseCost.LoadExport();

                //Generate Export Details
                ArrayList datColumnTD = new ArrayList();
                datColumnTD.Add(new DataClass.DataTableColumnDisplay("PeerGroupCode", "Peer Group Code"));
                datColumnTD.Add(new DataClass.DataTableColumnDisplay("Cost Category Code", "Cost Category Code"));
                datColumnTD.Add(new DataClass.DataTableColumnDisplay("UnitCost", "Unit Cost"));
                String strSelectedParameters = "";
                String[] columnsToCopy = { "PeerGroupCode", "Cost Category Code", "UnitCost" };
                ExportZiffBaseCost = new DataReportObject(dtExport, "DATASHEET", null, strSelectedParameters, null, "PeerGroupCode", datColumnTD, null);
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                if (strMessage.Contains("Timeout"))
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = strMessage.ToString(), IconCls = "icon-exclamation", Clear2 = false });
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select all filters please"), IconCls = "icon-exclamation", Clear2 = false });
                }
            }

        }

        private void FillGrid(Ext.Net.Store s, GridPanel g, DataTable dt)
        {
            try
            {
                s.RemoveFields();

                foreach (DataColumn c in dt.Columns)
                {
                    if (c.ColumnName == "Code" || c.ColumnName == "Description")
                    {
                        if (!c.ColumnName.Contains("Id"))
                            s.AddField(new ModelField(c.ColumnName, ModelFieldType.Auto));
                    }
                    else
                    {
                        if (!c.ColumnName.Contains("Id"))
                            s.AddField(new ModelField(c.ColumnName, ModelFieldType.Float));
                    }
                }

                g.Features.Clear();

                GroupingSummary gsum = new GroupingSummary();
                gsum.GroupHeaderTplString = "{name}";
                gsum.HideGroupedHeader = true;
                gsum.ShowSummaryRow = false;    //Hide the grouping Summary Totals by setting to false

                g.Features.Add(gsum);

                GridFilters f = new GridFilters();
                f.Local = true;
                g.Features.Add(f);

                Summary smry = new Summary();
                smry.Dock = new SummaryDock();
                smry.ID = "Summary" + g.ID.ToString();
                smry.ShowSummaryRow = false;    //Hide the Grand Summary Totals by setting to false

                g.Features.Add(smry);

                foreach (DataColumn c in dt.Columns)
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        if (c.ColumnName == "Code" || c.ColumnName == "Description")
                        {
                            StringFilter sFilter = new StringFilter();
                            sFilter.AutoDataBind = true;
                            sFilter.DataIndex = c.ColumnName;
                            f.Filters.Add(sFilter);
                        }
                        else
                        {
                            NumericFilter sFilter = new NumericFilter();
                            sFilter.AutoDataBind = true;
                            sFilter.DataIndex = c.ColumnName;
                            f.Filters.Add(sFilter);
                        }
                    }
                }

                Column col = new Column();
                col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Peer Group");

                SummaryColumn colSum = new SummaryColumn();
                foreach (DataColumn c in dt.Columns)
                {
                    if (c.ColumnName == "Code" || c.ColumnName == "Description")
                    {
                        if (!c.ColumnName.Contains("Id"))
                        {
                            colSum = new SummaryColumn();
                            colSum.DataIndex = c.ColumnName;
                            colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", c.ColumnName);
                            colSum.Locked = true;
                            colSum.SummaryType = Ext.Net.SummaryType.Count;
                            colSum.TdCls = "task";
                            colSum.Sortable = false;
                            colSum.Groupable = false;
                            colSum.Width = 350;
                            colSum.SummaryRenderer.Handler = "return '(' + value + ' Total)';";
                            colSum.Renderer.Fn = "formatCol";
                            colSum.MenuDisabled = true;
                            colSum.Align = Alignment.Left;
                            g.ColumnModel.Columns.Add(colSum);
                        }
                    }
                    else
                    {
                        if (!c.ColumnName.Contains("Id"))
                        {
                            colSum = new SummaryColumn();
                            colSum.DataIndex = c.ColumnName;
                            colSum.Text = c.ColumnName;
                            colSum.SummaryType = Ext.Net.SummaryType.Sum;
                            Renderer sumRen = new Renderer();
                            sumRen.Fn = "formatZero";
                            colSum.SummaryRenderer = sumRen;
                            colSum.Renderer = sumRen;
                            colSum.Align = Alignment.Right;
                            colSum.MinWidth = 175;
                            colSum.MenuDisabled = true;
                            col.Columns.Add(colSum);
                        }
                    }
                }

                g.ColumnModel.Columns.Add(col);
                //Add the editor to each column
                for (int x = 0; x < g.ColumnModel.Columns[2].Columns.Count; x++)
                {
                    g.ColumnModel.Columns[2].Columns[x].Editor.Add(new Ext.Net.NumberField());
                }
                s.DataSource = dt;
                if (X.IsAjaxRequest)
                {
                    g.Reconfigure();
                }
                s.DataBind();
            }
            catch { }
        }

        private void FillGridPeerCriteria(Ext.Net.Store s, GridPanel g, DataTable dt)
        {
            s.RemoveFields();

            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "Peer Criteria" || c.ColumnName == "Unit")
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Auto));
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Float));
                }
            }

            g.Features.Clear();

            GridFilters f = new GridFilters();
            f.Local = true;
            g.Features.Add(f);

            foreach (DataColumn c in dt.Columns)
            {
                if (!c.ColumnName.Contains("Id"))
                {
                    if (c.ColumnName == "Peer Criteria" || c.ColumnName == "Unit")
                    {
                        StringFilter sFilter = new StringFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                    else
                    {
                        NumericFilter sFilter = new NumericFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                }
            }

            Column col = new Column();
            col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Peer Group");
            SummaryColumn colSum = new SummaryColumn();
            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "Peer Criteria" || c.ColumnName == "Unit")
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        colSum = new SummaryColumn();
                        colSum.DataIndex = c.ColumnName;
                        colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", c.ColumnName);
                        colSum.Locked = true;
                        colSum.SummaryType = Ext.Net.SummaryType.Count;
                        colSum.TdCls = "task";
                        colSum.Sortable = true;
                        colSum.Groupable = true;
                        colSum.Width = 350;
                        colSum.SummaryRenderer.Handler = "return '(' + value + ' Total)';";
                        colSum.MenuDisabled = false;
                        colSum.Align = Alignment.Left;
                        g.ColumnModel.Columns.Add(colSum);
                    }
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        colSum = new SummaryColumn();
                        colSum.DataIndex = c.ColumnName;
                        colSum.Text = c.ColumnName;
                        colSum.SummaryType = Ext.Net.SummaryType.Sum;
                        Renderer sumRen = new Renderer();
                        sumRen.Fn = "Ext.util.Format.numberRenderer('0,000')";
                        colSum.SummaryRenderer = sumRen;
                        colSum.Renderer = sumRen;
                        colSum.Align = Alignment.Right;
                        colSum.MinWidth = 175;
                        col.Columns.Add(colSum);
                    }
                }
            }

            g.ColumnModel.Columns.Add(col);
            s.DataSource = dt;
            if (X.IsAjaxRequest)
            {
                g.Reconfigure();
            }
            s.DataBind();
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportZiffBaseCost;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=2.5.3 ExportExternalBaseCost.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        [DirectMethod]
        public void ClickedUnsetYES(Int32 _IdZiffBaseCost, Int32 _IdField)
        {
            DataClass.clsZiffBaseCost objZiffBaseCost = new clsZiffBaseCost();
            objZiffBaseCost.IdZiffBaseCost = _IdZiffBaseCost;
            objZiffBaseCost.Delete();
            this.ModelUpdateTimestamp();
            this.ModelCheckRecalcModuleAllocation();
            this.storeBaseCost.DataSource = this.ZiffBaseCosts(_IdField);
            this.storeBaseCost.DataBind();
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportExternalBaseCost.xls", "template");
        }

        protected Int32 GetIdPeerGroup(String _PeerGroupCode)
        {
            Int32 Value = 0;
            DataClass.clsPeerGroup objPeerGroup = new DataClass.clsPeerGroup();
            DataTable dt = objPeerGroup.LoadList("PeerGroupCode = '" + _PeerGroupCode + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdPeerGroup"];
            }
            return Value;
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        public void ModelCheckRecalcModuleAllocation()
        {
            clsModels objModelRecalc = new clsModels();
            objModelRecalc.IdModel = IdModel;
            objModelRecalc.CheckRecalcModuleParameter(objApplication.MaxRelationLevels);
            objModelRecalc.CheckRecalcModuleAllocation();
            objModelRecalc = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = modMain.strMasterZiff_Third_partyBaseCost;
            this.lblFilterPeerGroup.Text = (String)GetGlobalResourceObject("CPM_Resources", "Peer Group");
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            //this.colPeerGroupCode.Text = (String)GetGlobalResourceObject("CPM_Resources", "Peer Group");
            //this.colPeerCriteria.Text = (String)GetGlobalResourceObject("CPM_Resources", "Criteria");
            //this.colTypeUnit.Text = (String)GetGlobalResourceObject("CPM_Resources", "Unit Type");
            //this.colUnit.Text = (String)GetGlobalResourceObject("CPM_Resources", "Unit");
            //this.colQuantity.Text = (String)GetGlobalResourceObject("CPM_Resources", "Value");
            this.grdBaseCost.Title = (String)GetGlobalResourceObject("CPM_Resources", "BaseCost");
            //this.Column8.Text = modMain.strCommonCode;
            //this.Column9.Text = (String)GetGlobalResourceObject("CPM_Resources", "Description");
            //this.Column10.Text = (String)GetGlobalResourceObject("CPM_Resources", "Criteria");
            //this.Column111.Text = (String)GetGlobalResourceObject("CPM_Resources", "Unit");
            //this.colCostUnit.Text = (String)GetGlobalResourceObject("CPM_Resources", "Unit Cost");
            //this.colCost.Text = (String)GetGlobalResourceObject("CPM_Resources", "Total Cost");
            //this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            //((Ext.Net.GridCommand)(ImageCommandColumn1.Commands[0])).Text = "&nbsp;" + (String)GetGlobalResourceObject("CPM_Resources", "Set");
            //((Ext.Net.GridCommand)(ImageCommandColumn1.Commands[1])).Text = "&nbsp;" + (String)GetGlobalResourceObject("CPM_Resources", "Unset");
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.winImport.Title = (String)GetGlobalResourceObject("CPM_Resources", "Import External Base Cost References");
            this.winSetBaseCost.Title = (String)GetGlobalResourceObject("CPM_Resources","Ziff Base Cost");
            this.ddlCriteria.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Criteria");
            this.txtCost.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Cost");
            this.btnSave.Text = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.btnSaveGrid.Text = modMain.strCommonSaveChanges;
        }

        #endregion

    }
}
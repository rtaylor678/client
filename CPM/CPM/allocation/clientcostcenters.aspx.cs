﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _clientcostcenters : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdClientCostCenter { get { return (Int32)Session["IdClientCostCenter"]; } set { Session["IdClientCostCenter"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }
        private DataReportObject ExportClientCostCenters { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                IdClientCostCenter = 0;
                this.LoadCostCenter();
                this.LoadLabel();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winCostCenterEdit.Show();
        }

        protected void btnDeleteAll_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete all records?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDeleteAllYES()",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.Save();
                this.LoadCostCenter();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            winCostCenterEdit.Hide();
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.CostCenters);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Cost Object Code"].ToString(), row["Cost Object Desc"].ToString(), row["Activity Code"].ToString(), row["Allocation"].ToString(), row["Notes"].ToString());
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadCostCenter();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        protected void Clear()
        {
            this.IdClientCostCenter = 0;
            this.txtName.Text = "";
            this.txtName.Reset();
            this.txtCode.Text = "";
            this.txtCode.Reset();
            this.ddlActivity.Reset();
            this.ddlAllocation.Reset();
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void grdCostCenter_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            IdClientCostCenter = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            if (Command == "Edit")
            {
                this.winCostCenterEdit.Show();
                this.EditClientCostCenterLoad();
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + _Name + "?", new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedDeleteYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }      
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsClientCostCenter objClientCostCenter = new DataClass.clsClientCostCenter();
            DataTable dt = objClientCostCenter.LoadList("IdModel = " + IdModel + " AND Code = '" + txtCode.Text + "'", "");
            Value = dt.Rows.Count;

            return Value;
        }

        protected void Save()
        {
            DataClass.clsClientCostCenter objClientCostCenter = new DataClass.clsClientCostCenter();
            objClientCostCenter.IdClientCostCenter = (Int32)IdClientCostCenter;
            objClientCostCenter.loadObject();

            if (IdClientCostCenter == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (objClientCostCenter.Code.ToString().ToUpper() != txtCode.Text.ToString().ToUpper())
                {
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }

            
            objClientCostCenter.Code = txtCode.Text;
            objClientCostCenter.Name = txtName.Text;
            objClientCostCenter.IdModel = IdModel;
            objClientCostCenter.TypeAllocation = Convert.ToInt32(ddlAllocation.Value);
            objClientCostCenter.IdActivity = Convert.ToInt32(ddlActivity.Value);
            objClientCostCenter.Notes = this.txtNotes.Text;
            if (IdClientCostCenter == 0)
            {
                objClientCostCenter.DateCreation = DateTime.Now;
                objClientCostCenter.UserCreation = (Int32)IdUserCreate;
                IdClientCostCenter = objClientCostCenter.Insert();
            }
            else
            {
                objClientCostCenter.DateModification = DateTime.Now;
                objClientCostCenter.UserModification = (Int32)IdUserCreate;
                objClientCostCenter.OldTypeAllocation = Convert.ToInt32(hidOldAllocation.Value);
                objClientCostCenter.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winCostCenterEdit.Hide();
            this.ModelUpdateTimestamp();
        }

        protected Int32 LoadIdActivity()
        {
            Int32 IdActivity = 0;
            DataClass.clsActivities objActivities = new DataClass.clsActivities();
            DataTable dt = objActivities.LoadList("IdModel = " + IdModel + " AND Name LIKE '%" + ddlActivity.Text + "%'", "ORDER BY Name");

            if (dt.Rows.Count > 0)
            {
                IdActivity = (Int32)dt.Rows[0]["IdActivity"];
            }
            return IdActivity;
        }

        protected Int32 LoadIdAllocation()
        {
            Int32 IdAllocation = 0;
            DataClass.clsParameters objParameters = new DataClass.clsParameters();
            DataTable dt = objParameters.LoadList("IdLanguage = " + idLanguage + " AND Type = 'TypeAllocation' AND Name LIKE '%" + ddlAllocation.Text + "%'", "ORDER BY Name");

            if (dt.Rows.Count > 0)
            {
                IdAllocation = (Int32)dt.Rows[0]["Code"];
            }
            return IdAllocation;
        }

        protected void SaveImport(String _Code, String _Name, String _Activity, String _TypeAllocation, String _Notes)
        {
            Int32 _IdActivity = GetIdActivity(_Activity);
            Int32 _IdTypeAllocation = GetIdAllocation(_TypeAllocation);

            if (!String.IsNullOrEmpty(_Code) && !String.IsNullOrEmpty(_Name) && _IdActivity != 0 && _IdTypeAllocation != 0)
            {
                DataClass.clsClientCostCenter objClientCostCenter = new DataClass.clsClientCostCenter();
                objClientCostCenter.IdClientCostCenter = (Int32)IdClientCostCenter;
                objClientCostCenter.loadObject();
                objClientCostCenter.Code = _Code;
                objClientCostCenter.Name = _Name;
                objClientCostCenter.IdModel = IdModel;
                objClientCostCenter.TypeAllocation = _IdTypeAllocation;
                objClientCostCenter.IdActivity = _IdActivity;
                objClientCostCenter.Notes = _Notes;
                objClientCostCenter.DateCreation = DateTime.Now;
                objClientCostCenter.UserCreation = (Int32)IdUserCreate;
                IdClientCostCenter = objClientCostCenter.Insert();
                objClientCostCenter = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        private Int32 GetIdActivity(String _Activity)
        {
            Int32 Value = 0;
            DataClass.clsActivities objActivities = new DataClass.clsActivities();
            DataTable dt = objActivities.LoadList("Code = '" + _Activity + "' AND IdModel = " + IdModel, "");
            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdActivity"];
            }

            return Value;
        }

        private Int32 GetIdAllocation(String _Allocation)
        {
            Int32 Value = 0;
            DataClass.clsParameters objParameters = new DataClass.clsParameters();
            DataTable dt = objParameters.LoadList("Type = 'TypeAllocation' AND Name = '" + _Allocation + "'", "");
            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["Code"];
            }

            return Value;
        }

        protected void EditClientCostCenterLoad()
        {
            DataClass.clsClientCostCenter objClientCostCenter = new DataClass.clsClientCostCenter();
            DataTable dt = objClientCostCenter.LoadList("IdClientCostCenter = " + IdClientCostCenter, "");
            if (dt.Rows.Count > 0)
            {
                this.txtCode.Text = (String)dt.Rows[0]["Code"];
                this.txtName.Text = (String)dt.Rows[0]["Name"];;

                this.ddlActivity.Value = Convert.ToInt32(dt.Rows[0]["IdActivity"]);
                this.ddlAllocation.Value = Convert.ToInt32(dt.Rows[0]["TypeAllocation"]);
                this.hidOldAllocation.Value = Convert.ToInt32(dt.Rows[0]["TypeAllocation"]);

                this.txtNotes.Text = (String)dt.Rows[0]["Notes"].ToString();
            }
        }

        protected void StoreAct_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsActivities objActivities = new DataClass.clsActivities();
            this.stroreActivity.DataSource = objActivities.LoadList("IdModel = " + IdModel, "ORDER BY Name");
            this.stroreActivity.DataBind();
        }

        protected void StoreAlloc_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsParameters objParameters = new DataClass.clsParameters();
            this.storeAllocation.DataSource = objParameters.LoadList("IdLanguage = " + idLanguage + " AND Type = 'TypeAllocation'", "ORDER BY Name");
            this.storeAllocation.DataBind();
        }

        public void Delete()
        {
            DataClass.clsClientCostCenter objClientCostCenter = new DataClass.clsClientCostCenter();
            objClientCostCenter.IdClientCostCenter = (Int32)IdClientCostCenter;
            objClientCostCenter.Delete();
            this.ModelUpdateTimestamp();
        }

        public void DeleteAll()
        {
            DataClass.clsClientCostCenter objClientCostCenter = new DataClass.clsClientCostCenter();
            objClientCostCenter.IdModel = (Int32)IdModel;
            objClientCostCenter.DeleteAll();
            this.ModelUpdateTimestamp();
        }

        [DirectMethod]
        public void ClickedDeleteYES()
        {
            this.Delete();
            this.LoadCostCenter();
        }

        [DirectMethod]
        public void ClickedDeleteAllYES()
        {
            this.DeleteAll();
            this.LoadCostCenter();
        }

        protected void LoadCostCenter()
        {
            DataClass.clsClientCostCenter objClientCostCenter = new DataClass.clsClientCostCenter();

            DataTable dt = objClientCostCenter.LoadList("IdModel = " + IdModel + " AND IdLanguage = " + idLanguage, "ORDER BY Name");

            storeClientCostCenters.DataSource = dt;
            storeClientCostCenters.DataBind();

            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Code", "Cost Object Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Name", "Cost Object Desc"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("ActivityCode", "Activity Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Allocation", "Allocation"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Notes", "Notes"));
            String strSelectedParameters = "";
            String[] columnsToCopy = { "Code", "Name", "ActivityCode", "Allocation", "Notes" };
            System.Data.DataView view = new System.Data.DataView(dt);
            DataTable dtClientCostCenters = view.ToTable(true, columnsToCopy);
            ExportClientCostCenters = new DataReportObject(dtClientCostCenters, "DATASHEET", null, strSelectedParameters, null, "Name", datColumnTD, null);
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportClientCostCenters;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=1.6 ExportCostObjects.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportCostObjects.xls", "template");
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, true, false, false);
            objModelUpdate = null;
        }

        protected void LoadLabel()
        {
            this.grdClientCostCenters.Title = modMain.strMasterCostObjects;
            this.btnAdd.Text = (String)GetGlobalResourceObject("CPM_Resources", "Add New Cost Object");
            this.btnAdd.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Add New Cost Object");
            this.btnDelete.Text = modMain.strCommonDeleteAll;
            this.btnDelete.ToolTip = modMain.strCommonDeleteAll;
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.Code.Text = modMain.strCommonCode;
            this.Name.Text = modMain.strCommonName;
            this.Activity.Text = (String)GetGlobalResourceObject("CPM_Resources", "Activity");
            this.Allocation.Text = (String)GetGlobalResourceObject("CPM_Resources", "Allocation");
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = "&nbsp;" + modMain.strCommonEdit;
            this.ImageCommandColumn1.Commands[1].Text = "&nbsp;" + modMain.strCommonDelete;
            this.winCostCenterEdit.Title = modMain.strMasterCostObjects;
            this.txtCode.FieldLabel = modMain.strCommonCode;
            this.txtName.FieldLabel = modMain.strCommonName;
            this.ddlAllocation.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Allocation");
            this.ddlActivity.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Activity");
            this.txtNotes.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Notes");
            this.btnSave.Text = modMain.strCommonSave;
            this.btnSave.ToolTip = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.btnCancel.ToolTip = modMain.strCommonClose;
            this.winImport.Title = modMain.strCommonImport + " " + modMain.strMasterCostObjects;
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnImportFile.ToolTip = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.btnCancelImport.ToolTip = modMain.strCommonClose;
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        #endregion
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _peergroup : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdPeerGroup { get { return (Int32)Session["IdPeerGroup"]; } set { Session["IdPeerGroup"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private DataReportObject ExportPeerGroup { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                IdPeerGroup = 0;
                this.LoadPeerGroup();
                this.LoadLabel();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            IdPeerGroup = 0;
            StoreField_ReadData(null, null);
            this.winPeerGroupEdit.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Boolean blnHasFieldSelected = false;
            try
            {
                 String rowsValues = e.ExtraParams["rowsValues"];

                //Make sure at least 1 field is selected
                DataTable dtCheck = JSON.Deserialize<DataTable>(rowsValues);
                foreach (DataRow objRow in dtCheck.Rows)
                {
                    if ((Boolean)objRow["Selected"] == true)
                    {
                        blnHasFieldSelected = true;
                        break;
                    }
                }
                dtCheck = null;

                if (!blnHasFieldSelected)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "You must select at least one field from the list."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
                else
                {
                    DataClass.clsPeerGroup objPeerGroup = new clsPeerGroup();
                    objPeerGroup.IdPeerGroup = IdPeerGroup;
                    objPeerGroup.loadObject();
                    if (IdPeerGroup == 0)
                    {
                        if (this.Valid() > 0)
                        {
                            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                            return;
                        }
                    }

                    this.Save();
                    this.SaveFields(rowsValues);
                    this.storeField.Reload();

                    this.LoadPeerGroup();
                }
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            winPeerGroupEdit.Hide();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.PeerGroup);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Peer Group Code"].ToString(), row["Peer Group Description"].ToString(), row["Fields"].ToString(), row["Full Benchmark"].ToString().ToUpper() == "YES" ? true : false, row["Comments"].ToString());
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadPeerGroup();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void grdPeerGroup_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            IdPeerGroup = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            if (Command == "Edit")
            {
                this.winPeerGroupEdit.Show();
                this.EditPeerGroupLoad();
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + _Name + "?", new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedDeleteYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
        }

        protected void StoreField_ReadData(object sender, StoreReadDataEventArgs e)
        {
                DataClass.clsPeerGroupField objPeerGroupField = new DataClass.clsPeerGroupField();

                objPeerGroupField.IdModel = (Int32)IdModel;
                DataTable dt = objPeerGroupField.LoadField(IdPeerGroup);

                this.storeField.DataSource = dt;
                this.storeField.DataBind();
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void Clear()
        {
            this.IdPeerGroup = 0;
            this.txtPeerGroupCode.Text = "";
            this.txtPeerGroupDescription.Text = "";
            this.chkFullBenchmark.Checked = false;
            this.txtComments.Text = "";
        }

        protected void Save()
        {
            DataClass.clsPeerGroup objPeerGroup = new DataClass.clsPeerGroup();
            objPeerGroup.IdPeerGroup = (Int32)IdPeerGroup;
            objPeerGroup.loadObject();

            if (IdPeerGroup == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (objPeerGroup.PeerGroupDescription.ToString().ToUpper() != txtPeerGroupDescription.Text.ToString().ToUpper())
                {
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }

            objPeerGroup.IdModel = IdModel;
            objPeerGroup.PeerGroupCode = txtPeerGroupCode.Text;
            objPeerGroup.PeerGroupDescription = txtPeerGroupDescription.Text;
            objPeerGroup.FullBenchmark = Convert.ToBoolean(chkFullBenchmark.Checked);
            objPeerGroup.Comments = txtComments.Text;
            if (IdPeerGroup == 0)
            {
                IdPeerGroup = objPeerGroup.Insert();
            }
            else
            {
                objPeerGroup.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winPeerGroupEdit.Hide();
            this.ModelUpdateTimestamp();
        }

        protected void SaveFields(String strValue)
        {
            DataClass.clsCostStructureAssumptions objCostStructureAssumptions = new clsCostStructureAssumptions();

            DataTable dt = JSON.Deserialize<DataTable>(strValue);
            foreach (DataRow row in dt.Rows)
            {
                DataClass.clsPeerGroupField objPeerGroupField = new DataClass.clsPeerGroupField();

                objPeerGroupField.IdPeerGroupField = Convert.ToInt32(row["IdPeerGroupField"]);
                objPeerGroupField.loadObject();

                objPeerGroupField.IdPeerGroup = IdPeerGroup;
                objPeerGroupField.IdField = Convert.ToInt32(row["IdField"]);

                if (Convert.ToInt32(row["IdPeerGroupField"]) == 0)
                {
                    if ((Boolean)row["Selected"] == true)
                    {
                        objPeerGroupField.Insert();
                        objCostStructureAssumptions.UpdateFields(Convert.ToInt32(row["IdField"]), IdPeerGroup);
                    }
                }
                else
                {
                    if ((Boolean)row["Selected"] == false)
                    {
                        objPeerGroupField.Delete();
                    }
                    else
                    {
                        objPeerGroupField.Update();
                    }
                }
            }
            this.ModelUpdateTimestamp();
        }

        protected void SaveImport(String _PeerGroupCode, String _PeerGroupDescription, String _Fields, Boolean _FullBenchmark, String _Comments)
        {
            if (!String.IsNullOrEmpty(_PeerGroupCode))
            {
                DataClass.clsCostStructureAssumptions objCostStructureAssumptions = new clsCostStructureAssumptions();
                DataClass.clsPeerGroup objPeerGroup = new DataClass.clsPeerGroup();
                DataTable dt = objPeerGroup.LoadList("Idmodel = " + IdModel + " AND UPPER(PeerGroupCode) = '" + _PeerGroupCode.ToUpper() + "'", "ORDER BY IdPeerGroup");
                if (dt.Rows.Count > 0)
                {
                    IdPeerGroup = Convert.ToInt32(dt.Rows[0]["IdPeerGroup"]);
                }
                else
                {
                    objPeerGroup.IdPeerGroup = (Int32)IdPeerGroup;
                    objPeerGroup.loadObject();
                    objPeerGroup.IdModel = IdModel;
                    objPeerGroup.PeerGroupCode = _PeerGroupCode;
                    objPeerGroup.PeerGroupDescription = _PeerGroupDescription;
                    objPeerGroup.FullBenchmark = _FullBenchmark;
                    objPeerGroup.Comments = _Comments;
                    IdPeerGroup = objPeerGroup.Insert();
                }
                dt.Dispose();
                dt = null;
                int IdPeerGroupField = 0;
                DataClass.clsPeerGroupField objPeerGroupField = new DataClass.clsPeerGroupField();
                objPeerGroupField.IdModel = IdModel;
                objPeerGroupField.IdPeerGroup = IdPeerGroup;
                var fields = _Fields.Split(new[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries);
                foreach (String field in fields)
                {
                    if (objPeerGroupField.GetFieldId(field) != 0)
                    {
                        objPeerGroupField.IdField = objPeerGroupField.GetFieldId(field);
                        IdPeerGroupField = objPeerGroupField.Insert();
                        objCostStructureAssumptions.UpdateFields(objPeerGroupField.IdField, IdPeerGroup);
                    }
                }
                objPeerGroup = null;
                objPeerGroupField = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected void EditPeerGroupLoad()
        {
            DataClass.clsPeerGroup objPeerGroup = new DataClass.clsPeerGroup();
            DataTable dt = objPeerGroup.LoadList("IdModel = " + IdModel + " AND IdPeerGroup = " + IdPeerGroup, "");
            if (dt.Rows.Count > 0)
            {
                this.txtPeerGroupCode.Text = (String)dt.Rows[0]["PeerGroupCode"];
                this.txtPeerGroupDescription.Text = (String)dt.Rows[0]["PeerGroupDescription"];
                this.chkFullBenchmark.Checked = Convert.ToBoolean(dt.Rows[0]["FullBenchmark"]);
                this.txtComments.Text = (String)(dt.Rows[0]["Comments"]);
                this.storeField.Reload();
            }
        }

        public void Delete()
        {
            DataClass.clsPeerGroup objPeerGroup = new DataClass.clsPeerGroup();
            objPeerGroup.IdPeerGroup = (Int32)IdPeerGroup;
            objPeerGroup.Delete();
            objPeerGroup = null;
        }

        [DirectMethod]
        public void ClickedDeleteYES()
        {
            this.Delete();
            this.LoadPeerGroup();
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportListOfPeerGroups.xls", "template");
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsPeerGroup objPeerGroup = new DataClass.clsPeerGroup();
            DataTable dt = objPeerGroup.LoadList("IdModel = " + IdModel + " AND IdPeerGroup = " + IdPeerGroup, "");
            Value = dt.Rows.Count;
            return Value;
        }

       protected void LoadPeerGroup()
        {
            DataClass.clsPeerGroup objPeerGroup = new DataClass.clsPeerGroup();
            DataTable dt = objPeerGroup.LoadList("IdModel = " + IdModel, "ORDER BY PeerGroupCode");
            storePeerGroup.DataSource = dt;
            storePeerGroup.DataBind();

            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("PeerGroupCode", "Peer Group Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("PeerGroupDescription", "Peer Group Description"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Fields","Fields"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("FullBenchmark", "Full Benchmark"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Comments", "Comments"));
            String strSelectedParameters = "";
            String[] columnsToCopy = { "PeerGroupCode", "PeerGroupDescription", "Fields", "FullBenchmark", "Comments" };
            System.Data.DataView view = new System.Data.DataView(dt);
            DataTable dtPeerGroup = view.ToTable(true, columnsToCopy);
            ExportPeerGroup = new DataReportObject(dtPeerGroup, "DATASHEET", null, strSelectedParameters, null, "PeerGroupCode", datColumnTD, null);
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportPeerGroup;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=2.10.1 ExportListOfPeerGroup.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        protected void LoadLabel()
        {
            this.grdPeerGroup.Title = (String)GetGlobalResourceObject("CPM_Resources", "ListOfPeerGroup");
            this.btnAdd.Text = (String)GetGlobalResourceObject("CPM_Resources", "Add New Peer Group");
            this.btnAdd.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Add New Peer Group");
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.colPeerGroupCode.Text = (String)GetGlobalResourceObject("CPM_Resources", "PeerGroupCode");
            this.colPeerGroupDescription.Text = (String)GetGlobalResourceObject("CPM_Resources", "PeerGroupDescription");
            this.colFields.Text = (String)GetGlobalResourceObject("CPM_Resources", "Fields");
            this.colFullBenchmark.Text = (String)GetGlobalResourceObject("CPM_Resources", "FullBenchmark");
            this.colComments.Text = (String)GetGlobalResourceObject("CPM_Resources", "Comments");
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = "&nbsp;" + modMain.strCommonEdit;
            this.ImageCommandColumn1.Commands[1].Text = "&nbsp;" + modMain.strCommonDelete;
            this.winPeerGroupEdit.Title = (String)GetGlobalResourceObject("CPM_Resources", "Peer Group");
            this.txtPeerGroupCode.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "PeerGroupCode");
            this.txtPeerGroupDescription.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "PeerGroupDescription");
            this.chkFullBenchmark.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "FullBenchmark");
            this.txtComments.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Comments");
            this.FieldSet1.Title = (String)GetGlobalResourceObject("CPM_Resources", "Select Fields");
            this.colCode.Text = modMain.strCommonCode;
            this.colName.Text = modMain.strCommonName;
            this.chkSelected.Text = (String)GetGlobalResourceObject("CPM_Resources", "Select");
            this.btnSave.Text = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.winImport.Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Peer Group");
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Import");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        #endregion

    }
}
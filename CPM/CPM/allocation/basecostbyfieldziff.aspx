﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="basecostbyfieldziff.aspx.cs" Inherits="CPM._basecostbyfieldziff" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        Ext.num = function (v, defaultValue) {
            v = Number(Ext.isEmpty(v) || Ext.isArray(v) || typeof v == 'boolean' || (typeof v == 'string' && v.trim().length == 0) ? NaN : v);
            return isNaN(v) ? defaultValue : v;
        }

        Ext.util.Format.number = function (v, format) {
            if (!format) {
                return v;
            }
            v = Ext.num(v, NaN);
            if (isNaN(v)) {
                return '';
            }
            var comma = ',',
                dec = '.',
                i18n = false,
                neg = v < 0;

            v = Math.abs(v);
            if (format.substr(format.length - 2) == '/i') {
                format = format.substr(0, format.length - 2);
                i18n = true;
                comma = '.';
                dec = ',';
            }

            var hasComma = format.indexOf(comma) != -1,
                psplit = (i18n ? format.replace(/[^\d\,]/g, '') : format.replace(/[^\d\.]/g, '')).split(dec);

            if (1 < psplit.length) {
                v = v.toFixed(psplit[1].length);
            } else if (2 < psplit.length) {
                throw ('NumberFormatException: invalid format, formats should have no more than 1 period: ' + format);
            } else {
                v = v.toFixed(0);
            }

            var fnum = v.toString();

            psplit = fnum.split('.');

            if (hasComma) {
                var cnum = psplit[0],
                    parr = [],
                    j = cnum.length,
                    m = Math.floor(j / 3),
                    n = cnum.length % 3 || 3,
                    i;

                for (i = 0; i < j; i += n) {
                    if (i != 0) {
                        n = 3;
                    }

                    parr[parr.length] = cnum.substr(i, n);
                    m -= 1;
                }
                fnum = parr.join(comma);
                if (psplit[1]) {
                    fnum += dec + psplit[1];
                }
            } else {
                if (psplit[1]) {
                    fnum = psplit[0] + dec + psplit[1];
                }
            }

            return (neg ? '-' : '') + format.replace(/[\d,?\.?]+/, fnum);
        }
    </script>
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" Layout="FitLayout" Title="Base Cost by Field (Cost Projection Categories)" IconCls="icon-clientcostfieldziff_16" AutoScroll="true" runat="server" Border="false">
                <Items>
                    <ext:Panel ID="Panel3" runat="server" AutoScroll="true">
                        <Items>
                            <ext:Panel ID="pnlButtons01" runat="server" Layout="Column" Border="false" Style="margin: 15px 0 0 0">
                                <Items>
                                    <ext:Panel ID="Panel100" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 15px 0">
                                        <Items>
                                            <ext:Label Style="margin: 0 0 0 10px" ID="lblLevels" runat="server" Text="Aggregation Level:"></ext:Label>
                                            <ext:ComboBox Style="margin: 0 0 0 10px" ID="ddlLevels" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" ValueField="IdAggregationLevel">
                                                <ListConfig LoadingText="Searching..." MinWidth="250">
                                                    <ItemTpl ID="ItemTpl2" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
							                                    <span>Parent: {ParentName}</span>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeLevels" runat="server" OnReadData="StoreLevel_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model2" runat="server" IDProperty="IdAggregationLevel">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdAggregationLevel" />
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="ParentName" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                                <DirectEvents>
                                                    <Select OnEvent="ddlLevels_Select" />
                                                </DirectEvents>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel101" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 15px 0">
                                        <Items>
                                            <ext:Label Style="margin: 0 0 0 10px" ID="lblField" runat="server" Text="Field:"></ext:Label>
                                            <ext:ComboBox Style="margin: 0 0 0 10px" ID="ddlField" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" ValueField="IdField" Width="200">
                                                <ListConfig LoadingText="Searching..." MinWidth="300">
                                                    <ItemTpl ID="ItemTpl1" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
							                                    <span>Code: {Code}</span>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeField" runat="server" OnReadData="StoreField_ReadData">
                                                        <Model>
                                                            <ext:Model ID="mdlLabels" runat="server" IDProperty="IdField">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdField" />
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="Code" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel202" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 15px 0">
                                        <Items>
                                            <ext:Label Style="margin: 0 0 0 10px" ID="lblCurrency" runat="server" Text="Currency:"></ext:Label>
                                            <ext:ComboBox Style="margin: 0 0 0 10px" ID="ddlCurrency" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" ValueField="IdCurrency" Width="150">
                                                <ListConfig LoadingText="Searching..." MinWidth="250">
                                                    <ItemTpl ID="ItemTpl4" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
							                                    <span>Code: {Code}</span>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeBase" runat="server" OnReadData="StoreCurr_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model4" runat="server" IDProperty="IdCurrency">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdCurrency" />
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="Code" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel203" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 15px 0">
                                        <Items>
                                            <ext:Label Style="margin: 0 0 0 10px" ID="lblCostType" runat="server" Text="Type:"></ext:Label>
                                            <ext:ComboBox Style="margin: 0 0 0 10px" ID="ddlCostType" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" ValueField="IdCostType" Width="150">
                                                <ListConfig LoadingText="Searching..." MinWidth="250">
                                                    <ItemTpl ID="ItemTpl3" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
							                                    <span>Code: {Code}</span>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeCostType" runat="server" OnReadData="StoreCostType_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model1" runat="server" IDProperty="IdCostType">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdCostType" />
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="Code" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="pnlReportToolbar" runat="server" Border="false" Hidden="false">
                                <TopBar>
                                    <ext:Toolbar ID="tbrReport" Border="false" Height="30" runat="server">
                                        <Items>
                                            <ext:Button ID="btnRun" runat="server" Text="Execute Base Cost by Field" Icon="PlayGreen">
                                                <DirectEvents>
                                                    <Click Timeout="3600000" ShowWarningOnFailure="false" OnEvent="ddlField_Select">
                                                        <EventMask ShowMask="true">
                                                        </EventMask>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                             <ext:ToolbarFill />
                                            <ext:Button Style="margin: 0 0 0 10px" ID="btnSaveExcel" runat="server" Text="Export Data To Excel" Icon="PageExcel">
                                                <DirectEvents>
                                                    <Click OnEvent="btnSaveExcel_Click" IsUpload="true" />
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                            </ext:Panel>
                            <ext:Panel runat="server" Border="false">
                                <Items>
                                    <ext:GridPanel ID="grdBaseCostByFieldZiff" runat="server" Border="false" Header="false">
                                        <Store>
                                            <ext:Store ID="storeZiffBaseCostField" runat="server">
                                                <Model>
                                                    <ext:Model ID="modelField" runat="server">
                                                        <Fields>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel RenderColumnsOnly="False" RenderXType="True" IDMode="Explicit" Namespace="App" IsDynamic="False">
                                            <Columns>
                                            </Columns>
                                        </ColumnModel>
                                        <Listeners>
                                            <Reconfigure Handler="this.setHeight(this.container.getHeight()+20);" Delay="100" />
                                        </Listeners>
                                   </ext:GridPanel>
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="pnlVolumeResults" runat="server" AutoScroll="true" Title="Volume Results">
                                <Items>
                                    <ext:GridPanel ID="grdVolume" runat="server" Border="false" Header="false">
                                        <Store>
                                            <ext:Store ID="storeVolume" runat="server">
                                                <Model>
                                                    <ext:Model ID="modelVolume" runat="server">
                                                        <Fields>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel RenderColumnsOnly="False" RenderXType="True" IDMode="Explicit" Namespace="App" IsDynamic="False">
                                            <Columns>
                                            </Columns>
                                        </ColumnModel>
                                        <Listeners>
                                            <Reconfigure Handler="this.setHeight(this.container.getHeight()+20);" Delay="100" />
                                        </Listeners>
                                    </ext:GridPanel>
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="pnlForceFit" runat="server" Border="false" Title=" " Height="1" Cls="reportheader">
                            </ext:Panel>
                        </Items>
                    </ext:Panel>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="FormStatusBar" runat="server" />
                </BottomBar>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>
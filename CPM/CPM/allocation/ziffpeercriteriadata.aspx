﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ziffpeercriteriadata.aspx.cs" Inherits="CPM._ziffpeercriteriadata" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function applyPageSize(grid) {
            var headerHeight = 22;
            var rowHeight = 21;

            var gridTopOffset = 100;
            var gridBottomOffset = 50;
            var rowHeight = 20;
            var myHeight = window.innerHeight;

            grid.store.pageSize = Math.round(((myHeight - gridTopOffset - gridBottomOffset) / rowHeight)); // - 3);
            grid.store.load();
        }
    </script>
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:resourcemanager id="rscManager" runat="server" directmethodnamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" AutoScroll="true" runat="server" Title="Peer Criteria Data" IconCls="icon-peercriteriadata_16" Border="false">
                <Items>
                    <ext:Panel ID="Panel1" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                        <Items>
                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lblFilterPeerGroup" runat="server" Text="Peer Group:"></ext:Label>
                            <ext:ComboBox Style="margin: 10px 10px 10px 10px" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" QueryMode="Remote" ID="ddlPeerGroup" runat="server" DisplayField="PeerGroupDescription" ValueField="IdPeerGroup" Width="300">
                                <ListConfig LoadingText="Searching..." MinWidth="300">
                                    <ItemTpl ID="ItemTpl3" runat="server">
                                        <Html>
                                            <div class="search-item">
							                    <h3>{PeerGroupDescription}</h3>
							                    <span>Code: {PeerGroupCode}</span>
						                    </div>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Store>
                                    <ext:Store ID="storePeerGroup" runat="server" OnReadData="StorePeerGroup_ReadData">
                                        <Model>
                                            <ext:Model ID="Model3" runat="server" IDProperty="IdPeerGroup">
                                                <Fields>
                                                    <ext:ModelField Name="IdPeerGroup" />
                                                    <ext:ModelField Name="PeerGroupCode" />
                                                    <ext:ModelField Name="PeerGroupDescription" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                                <Reader>
                                                    <ext:JsonReader />
                                                </Reader>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Select OnEvent="ddlPeerGroup_Select">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>
                            </ext:ComboBox>
                        </Items>
                    </ext:Panel>
                    <ext:GridPanel ID="grdPeerCriterias" AutoScroll="true" runat="server" Border="false">
                        <TopBar>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btnSaveQtys" Icon="DatabaseSave" ToolTip="Save Peer Criteria Data" runat="server" Text="Save Changes">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveQtys_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="rowsValues" Value="#{grdPeerCriterias}.getRowsValues(false)" Mode="Raw" Encode="true" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarSeparator />
                                    <ext:Button ID="btnImport" Icon="BookAdd" runat="server" Text="Import">
                                        <DirectEvents>
                                            <Click OnEvent="btnImport_Click">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarFill />
                                    <ext:Button ID="btnSaveExcel" runat="server" Text="Export Data To Excel" Icon="PageExcel">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveExcel_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnDownloadTemplate" runat="server" Text="Download Import Template" Icon="PageSave">
                                        <DirectEvents>
                                            <Click OnEvent="btnDownloadTemplate_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="storePeerCriteriaData" runat="server">
                                <Model>
                                    <ext:Model ID="modelPeerCriteriaData" runat="server" IDProperty="IdPeerCriteria">
                                        <Fields>
                                            <ext:ModelField Name="IdPeerCriteria" Type="Int" />
                                            <ext:ModelField Name="IdPeerGroup" Type="Int" />
                                            <ext:ModelField Name="PeerCriteria" Type="String" />
                                            <ext:ModelField Name="TypeUnit" Type="String" />
                                            <ext:ModelField Name="Unit" Type="String" />
                                            <ext:ModelField Name="Quantity" Type="Float" />
                                        </Fields>
                                    </ext:Model>
                                    </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel ID="cmExchange" runat="server">
                            <Columns>
                                <ext:Column Hidden="true" ID="IdPeerCriteria" runat="server" Text="ID" DataIndex="IdPeerCriteria" />
                                <ext:Column Hidden="true" ID="IdPeerGroup" runat="server" Text="ID" DataIndex="IdPeerGroup" />
                                <ext:Column ID="colPeerCriteria" runat="server" Text="Criteria" DataIndex="PeerCriteria" Flex="1" />
                                <ext:Column ID="colTypeUnit" runat="server" Text="Unit Type" DataIndex="TypeUnit" Flex="1" />
                                <ext:Column ID="colUnit" runat="server" Text="Unit" DataIndex="Unit" Flex="1" />
                                <ext:NumberColumn ID="colQuantity" runat="server" Format="0,000.00" Text="Value" DataIndex="Quantity" Flex="1" Align="Right" MaxWidth="200">
                                    <Editor>
                                        <ext:NumberField DecimalPrecision="2" MinValue="0" ID="numValue" runat="server" />
                                    </Editor>
                                </ext:NumberColumn>
                            </Columns>
                        </ColumnModel>
                        <Features>
                            <ext:GridFilters runat="server" ID="GridFilters2" Local="true">
                                <Filters>
                                    <ext:StringFilter DataIndex="PeerCriteria" />
                                    <ext:StringFilter DataIndex="TypeUnit" />
                                    <ext:StringFilter DataIndex="Unit" />
                                    <ext:NumericFilter DataIndex="Quantity" />
                                </Filters>
                            </ext:GridFilters>
                        </Features>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" />
                        </SelectionModel>
                        <Plugins>
                            <ext:CellEditing ID="CellEditing25" runat="server">
                            </ext:CellEditing>
                        </Plugins>
                        <View>
                            <ext:GridView MarkDirty="false" />
                        </View>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="true">
                            </ext:PagingToolbar>
                        </BottomBar>
                         <Listeners>
                            <Resize Handler="applyPageSize(this)" Buffer="250" />            
                        </Listeners>
                   </ext:GridPanel>
                    <ext:Window ID="winImport" runat="server" Title="Import Fields" Hidden="true" Icon="ArrowLeft" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="frmImportFile" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                                <Items>
                                    <ext:Panel ID="pnlImportFile1" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:FileUploadField ID="fileImport" FieldLabel="File" runat="server" Icon="Attach" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnImportFile" runat="server" Text="Import" Icon="TableRefresh" Disabled="true" FormBind="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnImportFile_Click" >
                                                <EventMask ShowMask="true" Target="CustomTarget" CustomTarget="vpMain" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnCancelImport" runat="server" Icon="Decline" Text="Close">
                                        <DirectEvents>
                                            <Click OnEvent="btnCancelImport_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                                <BottomBar>
                                    <ext:StatusBar ID="FormImportStatusBar" runat="server" />
                                </BottomBar>
                                <Listeners>
                                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                            });
                                                            #{btnImportFile}.setDisabled(!valid);" />
                                </Listeners>
                            </ext:FormPanel>
                        </Items>
                    </ext:Window>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="FormStatusBar" runat="server" />
                </BottomBar>
            </ext:Panel>
        </Items>
    </ext:viewport>
</form>
</body>
</html>

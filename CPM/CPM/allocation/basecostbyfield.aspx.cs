﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _basecostbyfield : System.Web.UI.Page
    {
        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }
        private Boolean PageIsLoaded { get; set; }
        private DataReportObject ExportCostByField { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }
        private DataReportObject ExportCostByFieldVol { get { return (DataReportObject)Session["Export02"]; } set { Session["Export02"] = value; } }
        public DataTable dtVolume;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                this.LoadLabel();
                Session["Export01"] = null;
                Session["Export02"] = null;
                this.ddlStructure.SelectedItem.Index = 1;
                PageIsLoaded = false;
                DataTable dtValues = new DataTable();
                this.FillGrid(this.storeBaseCostField, this.grdBaseCostByField, dtValues, null);
                this.FillGridVolume(this.storeVolume, this.grdVolume, dtValues, null);
                PageIsLoaded = true;
            }
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void ddlLevels_Select(object sender, DirectEventArgs e)
        {
            this.ddlField.Reset();
            this.storeField.Reload();
            this.ddlOperationType.Reset();
            this.storeOperationType.Reload();
        }

        protected void btnRun_Click(object sender, DirectEventArgs e)
        {
            this.ModelCheckRecalcModuleAllocation();
            DisableParameterSelection();
            Int32 intCurrency = Convert.ToInt32(ddlCurrency.Value);
            Int32 intCostType = Convert.ToInt32(ddlCostType.Value);
            Double dblCurrencyFactor = 1;

            clsCurrencies objCurrencies = new clsCurrencies();
            objCurrencies.IdCurrency = intCurrency;
            objCurrencies.loadObject();

            DataTable dtCurrencyFactor = objCurrencies.GetCurrencyFactor(IdModel, intCurrency);
            if (dtCurrencyFactor.Rows.Count > 0)
            {
                dblCurrencyFactor = (Double)dtCurrencyFactor.Rows[0]["Value"];
            }

            this.LoadVolumeResults(dblCurrencyFactor, objCurrencies.Code);
            this.LoadBaseCostByField(dblCurrencyFactor, objCurrencies.Code);
        }

        protected void btnReset_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/basecostbyfield.aspx");
        }

        protected void StoreField_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dtField = new DataTable();
            if ((ddlLevels.Value == null) || (ddlLevels.Value.ToString() == "0") || (ddlLevels.Value.ToString() == ""))
            {
                dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
            }
            else
            {
                dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
            }

            DataRow drField = dtField.NewRow();
            drField["IdField"] = 0;
            drField["Name"] = "- All -";
            dtField.Rows.Add(drField);
            dtField.AcceptChanges();
            dtField.DefaultView.Sort = "Name";
            DataTable dtSort = dtField.DefaultView.ToTable();

            this.storeField.DataSource = dtSort;
            this.storeField.DataBind();

            this.ddlField.Value = 0;

            this.storeField.DataBind();
        }

        protected void StoreLevel_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dt = objAggregationLevels.LoadList("IdModel = " + IdModel, "ORDER BY Name");

            DataRow drLevel = dt.NewRow();
            drLevel["IdAggregationLevel"] = 0;
            drLevel["Name"] = "- All -";
            dt.Rows.Add(drLevel);
            dt.AcceptChanges();
            dt.DefaultView.Sort = "Name";
            DataTable dtSort = dt.DefaultView.ToTable();

            this.storeLevels.DataSource = dtSort;
            this.storeLevels.DataBind();

            this.ddlLevels.Value = 0;

            this.storeLevels.DataBind();
        }

        protected void StoreCurr_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsCurrencies objCurrencies = new DataClass.clsCurrencies();
            this.storeBase.DataSource = objCurrencies.LoadList("IdModel IN (" + IdModel + ") AND Output=1", "ORDER BY BaseCurrency DESC,Name");
            this.storeBase.DataBind();
            DataTable dt = objCurrencies.LoadList("BaseCurrency = 1 AND IdModel = " + IdModel, "");
            if (dt.Rows.Count > 0)
            {
                ddlCurrency.Value = dt.Rows[0]["IdCurrency"];
            }
            else
            {
                ddlCurrency.SelectedItem.Index = 0;
            }
        }

        protected void StoreCostType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsCostType objCostTypes = new DataClass.clsCostType();
            DataTable dt = objCostTypes.LoadList("", "ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                ddlCostType.Value = dt.Rows[0]["IdCostType"];
            }
            else
            {
                ddlCostType.SelectedItem.Index = 0;
            }
            foreach (DataRow row in dt.Rows)
            {
                if (idLanguage == 2)
                {
                    row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
                }
            }
            this.storeCostType.DataSource = dt;
            this.storeCostType.DataBind();
        }

        protected void StoreOperationType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsOperationType objoperationTypes = new DataClass.clsOperationType();
            DataTable dtOperation = new DataTable();

            if (((ddlLevels.Value == null) || (ddlLevels.Value.ToString() == "0") || (ddlLevels.Value.ToString() == "")) && ((ddlField.Value == null) || (ddlField.Value.ToString() == "0") || (ddlField.Value.ToString() == "")))
            {
                dtOperation = objoperationTypes.LoadList("IdModel=" + IdModel, "");//"[vListTypesOperation].IdModel=" + IdModel, "");

                DataRow drOperation = dtOperation.NewRow();
                drOperation["IdTypeOperation"] = 0;
                drOperation["Name"] = "- All -";
                dtOperation.Rows.Add(drOperation);
                dtOperation.AcceptChanges();
                dtOperation.DefaultView.Sort = "Name";

                ddlOperationType.Value = 0;
            }
            else if (Convert.ToInt32(ddlLevels.Value) > 0)
            {
                dtOperation = objoperationTypes.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
                if (dtOperation.Rows.Count > 0)
                {
                    ddlOperationType.Value = dtOperation.Rows[0]["IdTypeOperation"];
                }
            }
            else
            {
                dtOperation = objoperationTypes.LoadList("IdModel=" + IdModel + " AND IdField = " + ddlField.Value, " Order by Name");//"[vListTypesOperation].IdModel=" + IdModel + " AND tblFields.IdField = " + ddlField.Value, " Order by Name");
                if (dtOperation.Rows.Count > 0)
                {
                    ddlOperationType.Value = dtOperation.Rows[0]["IdTypeOperation"];
                }
            }

            DataTable dtOperationSort = dtOperation.DefaultView.ToTable();

            this.storeOperationType.DataSource = dtOperationSort;
            this.storeOperationType.DataBind();
        }

        #endregion

        #region Methods

        protected void LoadVolumeResults(Double _dblCurrencyFactor, String _strCurrencyCode)
        {
            DataClass.clsBaseCostByField objBaseCostByField = new DataClass.clsBaseCostByField();

            DataTable dt = objBaseCostByField.LoadVolumeReport(IdModel, Convert.ToInt32(ddlStructure.Value));
            DataTable dtRows = objBaseCostByField.LoadVolumeRows(IdModel, Convert.ToInt32(ddlStructure.Value));
            DataTable dtValues = new DataTable();
            DataTable dtValuesVolume = new DataTable();

            dtValues.Columns.Add("Name");
            dtValuesVolume.Columns.Add("Name");

            DataTable dtField;

            DataClass.clsFields objFields = new DataClass.clsFields();

            if ((ddlLevels.Value == null) || (ddlLevels.Value.ToString() == "0") || (ddlLevels.Value.ToString() == "")) // Aggregation Level is - All - 
            {
                if (this.ddlField.Value == null || this.ddlField.Value.ToString() == "0") // Field selected is - All - 
                {
                    dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
                }
                else // Specific field selected
                {
                    dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField = " + this.ddlField.Value, "ORDER BY Name");
                }
            }
            else //Specific Aggregation Level selected
            {
                if (this.ddlField.Value == null) // Specific Aggregation Level selected with no field
                {
                    dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
                }
                else
                {
                    if (this.ddlField.Value.ToString() == "0") // Specific Aggregation Level with - All - field select
                    {
                        dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
                    }
                    else //Specific field selected
                    {
                        dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField = " + this.ddlField.Value, "ORDER BY Name");
                    }
                }
            }

            foreach (DataRow drField in dtField.Rows)
            {
                dtValues.Columns.Add(drField["Name"].ToString().Replace(" ", "_"), typeof(Double));
                dtValuesVolume.Columns.Add(drField["Name"].ToString().Replace(" ", "_"), typeof(Double));
            }
            dtValues.Columns.Add((String)GetGlobalResourceObject("CPM_Resources", "Grand_Total"), typeof(Double));
            dtValuesVolume.Columns.Add((String)GetGlobalResourceObject("CPM_Resources", "Grand_Total"), typeof(Double));

            foreach (DataRow drRows in dtRows.Rows)
            {
                Int32 intIdAllocationListDriver = (Int32)drRows["IdAllocationListDriver"];

                DataRow drAct = dtValues.NewRow();
                drAct["Name"] = drRows["Name"].ToString();

                foreach (DataColumn dcValues in dtValues.Columns)
                {
                    if (dcValues.ColumnName == (String)GetGlobalResourceObject("CPM_Resources", "Grand_Total"))
                    {
                        DataRow[] FilterRows;
                        FilterRows = dt.Select("IdAllocationListDriver = '" + intIdAllocationListDriver + "'");

                        Double dblValue = 0;

                        foreach (DataRow row in FilterRows)
                        {
                            if (row["Amount"] == System.DBNull.Value)
                                dblValue = dblValue + 0;
                            else
                                dblValue = dblValue + Convert.ToDouble(row["Amount"]);
                        }

                        drAct[dcValues.ColumnName] = dblValue;
                    }
                    else if (dcValues.ColumnName != "Name")
                    {
                        DataRow[] FilterRows;
                        FilterRows = dt.Select("IdField = " + GetIdField(dcValues.ColumnName.Replace("_", " ")) + " AND IdAllocationListDriver = '" + intIdAllocationListDriver + "'");

                        Double dblValue = 0;

                        foreach (DataRow row in FilterRows)
                        {
                            if (row["Amount"] == System.DBNull.Value)
                                dblValue = 1;
                            else
                                dblValue = dblValue + Convert.ToDouble(row["Amount"]);
                        }

                        drAct[dcValues.ColumnName] = dblValue;
                    }
                }
                dtValues.Rows.Add(drAct);
                dtValues.AcceptChanges();
            }

            // Group the Volume
            DataRow drActVolume = dtValuesVolume.NewRow();
            drActVolume["Name"] = "Grouped";

            foreach (DataColumn dcValues in dtValues.Columns)
            {
                if (dcValues.ColumnName == (String)GetGlobalResourceObject("CPM_Resources", "Grand_Total"))
                {
                    Double dblValue = 0;

                    foreach (DataRow row in dt.Rows)
                    {
                        if (row["Amount"] == System.DBNull.Value)
                            dblValue = dblValue + 0;
                        else
                            dblValue = dblValue + Convert.ToDouble(row["Amount"]);
                    }

                    drActVolume[dcValues.ColumnName] = dblValue;
                }
                else if (dcValues.ColumnName != "Name")
                {
                    DataRow[] FilterRows;
                    FilterRows = dt.Select("IdField = " + GetIdField(dcValues.ColumnName.Replace("_", " ")));

                    Double dblValue = 0;

                    foreach (DataRow row in FilterRows)
                    {
                        if (row["Amount"] == System.DBNull.Value)
                            dblValue = 1;
                        else
                            dblValue = dblValue + Convert.ToDouble(row["Amount"]);
                    }
                    // If there are no volume values then we need to make it 1 by default otherwise we get tdivision by zero error
                    drActVolume[dcValues.ColumnName] = dblValue == 0 ? 1 : dblValue ;
                }
            }
            dtValuesVolume.Rows.Add(drActVolume);
            dtValuesVolume.AcceptChanges();

            dtVolume = dtValuesVolume;

            clsModels objModels = new clsModels();
            this.FillGridVolume(this.storeVolume, this.grdVolume, dtValues, _strCurrencyCode);

            // ToDo: can reset language of columns here
            ArrayList datColumn = new ArrayList();
            datColumn.Add(new DataClass.DataTableColumnDisplay("Name", "Amount"));

            String strSelectedParameters = lblLevels.Text + " '" + ddlLevels.SelectedItem.Text + "', " +
                                           lblField.Text + " '" + (String.IsNullOrEmpty(ddlField.SelectedItem.Text) == true ? "- All -" : ddlField.SelectedItem.Text) + "', " +
                                           lblStructure.Text + " '" + ddlStructure.SelectedItem.Text + "', " +
                                           lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "', " +
                                           lblOperationType.Text + " '" + ddlOperationType.SelectedItem.Text + "', " +
                                           lblCostType.Text + " '" + ddlCostType.SelectedItem.Text + "'";

            ExportCostByFieldVol = new DataReportObject(dtValues, "Base Cost References - Volume", "Internal Base Cost References", strSelectedParameters, null, "Name", datColumn, null);
        }

        protected void LoadBaseCostByField(Double _dblCurrencyFactor, String _strCurrencyCode)
        {
            if (Convert.ToInt32(ddlStructure.Value) == 1)
            #region (Solomon Cost Categories)
            {
                DataClass.clsBaseCostByField objBaseCostByField = new DataClass.clsBaseCostByField();

                DataTable dt = objBaseCostByField.LoadReport(IdModel, Convert.ToInt32(ddlStructure.Value));//1);//
                DataTable dtRows = objBaseCostByField.LoadRows(IdModel, Convert.ToInt32(ddlStructure.Value));//1);// 
                DataTable dtValues = new DataTable();

                dtValues.Columns.Add("Activity");
                dtValues.Columns.Add("Cost");

                DataTable dtField;

                DataClass.clsFields objFields = new DataClass.clsFields();
                if ((ddlLevels.Value == null) || (ddlLevels.Value.ToString() == "0") || (ddlLevels.Value.ToString() == ""))
                {
                    if (this.ddlField.Value == null)
                    {
                        dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
                    }
                    else
                    {
                        if (this.ddlField.Value.ToString() == "0")
                        {
                            dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
                        }
                        else
                        {
                            dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField = " + this.ddlField.Value, "ORDER BY Name");
                        }
                    }
                }
                else
                {
                    if (this.ddlField.Value == null)
                    {
                        dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
                    }
                    else
                    {
                        if (this.ddlField.Value.ToString() == "0")
                        {
                            dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
                        }
                        else
                        {
                            dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField = " + this.ddlField.Value, "ORDER BY Name");
                        }
                    }
                }

                foreach (DataRow drField in dtField.Rows)
                {
                    dtValues.Columns.Add(drField["Name"].ToString().Replace(" ", "_"), typeof(Double));
                }
                dtValues.Columns.Add((String)GetGlobalResourceObject("CPM_Resources", "Grand_Total"), typeof(Double));

                foreach (DataRow drRows in dtRows.Rows)
                {
                    Int32 intIdRootZiffAccount = (Int32)drRows["IdRootZiffAccount"];
                    Int32 intIdZiffAccount = (Int32)drRows["IdZiffAccount"];

                    DataRow drAct = dtValues.NewRow();
                    drAct["Activity"] = drRows["RootZiffAccount"].ToString();
                    drAct["Cost"] = drRows["ZiffAccount"].ToString();

                    foreach (DataColumn dcValues in dtValues.Columns)
                    {
                        if (dcValues.ColumnName == (String)GetGlobalResourceObject("CPM_Resources", "Grand_Total"))
                        {
                            DataRow[] FilterRows;
                            FilterRows = dt.Select("IdRootZiffAccount = '" + intIdRootZiffAccount + "' AND IdZiffAccount = '" + intIdZiffAccount + "'");

                            Double dblValue = 0;

                            foreach (DataRow row in FilterRows)
                            {
                                if (row["Amount"] == System.DBNull.Value)
                                    dblValue = dblValue + 0;
                                else
                                    dblValue = dblValue + Convert.ToDouble(row["Amount"]);
                            }

                            if ((String)ddlCostType.Value == "2")
                            {
                                drAct[dcValues.ColumnName] = (dblValue * _dblCurrencyFactor) / ((Double)dtVolume.Rows[0][dcValues.ColumnName]);
                            }
                            else
                            {
                                drAct[dcValues.ColumnName] = dblValue * _dblCurrencyFactor;
                            }
                        }
                        else if (dcValues.ColumnName != "Activity" && dcValues.ColumnName != "Cost")
                        {
                            DataRow[] FilterRows;
                            FilterRows = dt.Select("IdField = " + GetIdField(dcValues.ColumnName.Replace("_", " ")) + " AND IdRootZiffAccount = '" + intIdRootZiffAccount + "' AND IdZiffAccount = '" + intIdZiffAccount + "'");

                            Double dblValue = 0;

                            foreach (DataRow row in FilterRows)
                            {
                                if (row["Amount"] == System.DBNull.Value)
                                    dblValue = dblValue + 0;
                                else
                                    dblValue = dblValue + Convert.ToDouble(row["Amount"]);
                            }

                            if (FilterRows.Count() == 0)
                            {
                                drAct[dcValues.ColumnName] = 0;
                            }
                            else
                            {

                                if ((String)ddlCostType.Value == "2")
                                {
                                    drAct[dcValues.ColumnName] = (dblValue * _dblCurrencyFactor) / ((Double)dtVolume.Rows[0][dcValues.ColumnName]);
                                }
                                else
                                {
                                    drAct[dcValues.ColumnName] = dblValue * _dblCurrencyFactor;
                                }
                            }
                        }
                    }
                    dtValues.Rows.Add(drAct);
                    dtValues.AcceptChanges();
                }

                clsModels objModels = new clsModels();
                this.FillGrid(this.storeBaseCostField, this.grdBaseCostByField, dtValues, _strCurrencyCode);

                // ToDo: can reset language of columns here
                ArrayList datColumn = new ArrayList();
                datColumn.Add(new DataClass.DataTableColumnDisplay("Activity", "Parent Cost Category"));
                datColumn.Add(new DataClass.DataTableColumnDisplay("Cost", "Cost Category"));

                String strSelectedParameters = lblLevels.Text + " '" + ddlLevels.SelectedItem.Text + "', " +
                                               lblField.Text + " '" + (String.IsNullOrEmpty(ddlField.SelectedItem.Text) == true ? "- All -" : ddlField.SelectedItem.Text) + "', " +
                                               lblStructure.Text + " '" + ddlStructure.SelectedItem.Text + "', " +
                                               lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "', " +
                                               lblOperationType.Text + " '" + ddlOperationType.SelectedItem.Text + "', " +
                                               lblCostType.Text + " '" + ddlCostType.SelectedItem.Text + "'";
                ExportCostByField = new DataReportObject(dtValues, "Base Cost References", "Internal Base Cost References", strSelectedParameters, null, "Activity,Cost", datColumn, null);
            }
            #endregion
            else
            #region (Company Structure)
            {
                DataClass.clsBaseCostByField objBaseCostByField = new DataClass.clsBaseCostByField();

                DataTable dt = objBaseCostByField.LoadReport(IdModel, Convert.ToInt32(ddlStructure.Value));
                DataTable dtRows = objBaseCostByField.LoadRows(IdModel, Convert.ToInt32(ddlStructure.Value));
                DataTable dtValues = new DataTable();

                dtValues.Columns.Add("Activity");
                dtValues.Columns.Add("Cost");

                DataTable dtField;

                DataClass.clsFields objFields = new DataClass.clsFields();
                if ((ddlLevels.Value == null) || (ddlLevels.Value.ToString() == "0") || (ddlLevels.Value.ToString() == ""))
                {
                    if (this.ddlField.Value == null)
                    {
                        dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
                    }
                    else
                    {
                        if (this.ddlField.Value.ToString() == "0")
                        {
                            dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
                        }
                        else
                        {
                            dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField = " + this.ddlField.Value, "ORDER BY Name");
                        }
                    }
                }
                else
                {
                    if (this.ddlField.Value == null)
                    {
                        dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
                    }
                    else
                    {
                        if (this.ddlField.Value.ToString() == "0")
                        {
                            dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
                        }
                        else
                        {
                            dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField = " + this.ddlField.Value, "ORDER BY Name");
                        }
                    }
                }

                foreach (DataRow drField in dtField.Rows)
                {
                    dtValues.Columns.Add(drField["Name"].ToString().Replace(" ", "_"), typeof(Double));
                }
                dtValues.Columns.Add((String)GetGlobalResourceObject("CPM_Resources", "Grand_Total"), typeof(Double));

                foreach (DataRow drRows in dtRows.Rows)
                {
                    Int32 intIdActivity = (Int32)drRows["IdActivity"];
                    Int32 intIdResource = (Int32)drRows["IdResources"];

                    DataRow drAct = dtValues.NewRow();
                    drAct["Activity"] = drRows["Activity"].ToString();
                    drAct["Cost"] = drRows["Resource"].ToString();

                    foreach (DataColumn dcValues in dtValues.Columns)
                    {
                        if (dcValues.ColumnName == (String)GetGlobalResourceObject("CPM_Resources", "Grand_Total"))
                        {
                            DataRow[] FilterRows;
                            FilterRows = dt.Select("IdActivity = '" + intIdActivity + "' AND IdResources = '" + intIdResource + "'");

                            Double dblValue = 0;

                            foreach (DataRow row in FilterRows)
                            {
                                if (row["Amount"] == System.DBNull.Value)
                                    dblValue = dblValue + 0;
                                else
                                    dblValue = dblValue + Convert.ToDouble(row["Amount"]);
                            }

                            if ((String)ddlCostType.Value == "2")
                            {
                                if (!DBNull.Value.Equals(dtVolume.Rows[0][dcValues.ColumnName]) || (Double)dtVolume.Rows[0][dcValues.ColumnName] == 0.0)
                                    drAct[dcValues.ColumnName] = (dblValue * _dblCurrencyFactor) / ((Double)dtVolume.Rows[0][dcValues.ColumnName] * _dblCurrencyFactor);
                                else
                                    drAct[dcValues.ColumnName] = (dblValue * _dblCurrencyFactor);
                            }
                            else
                            {
                                drAct[dcValues.ColumnName] = dblValue * _dblCurrencyFactor;
                            }
                        }
                        else if (dcValues.ColumnName != "Activity" && dcValues.ColumnName != "Cost")
                        {
                            DataRow[] FilterRows;
                            FilterRows = dt.Select("IdField = " + GetIdField(dcValues.ColumnName.Replace("_", " ")) + " AND IdActivity = '" + intIdActivity + "' AND IdResources = '" + intIdResource + "'");

                            Double dblValue = 0;

                            foreach (DataRow row in FilterRows)
                            {
                                if (row["Amount"] == System.DBNull.Value)
                                    dblValue = dblValue + 0;
                                else
                                    dblValue = dblValue + Convert.ToDouble(row["Amount"]);
                            }

                            if ((String)ddlCostType.Value == "2")
                            {
                                if (!DBNull.Value.Equals(dtVolume.Rows[0][dcValues.ColumnName]) || (Double)dtVolume.Rows[0][dcValues.ColumnName] == 0.0)
                                    drAct[dcValues.ColumnName] = (dblValue * _dblCurrencyFactor) / ((Double)dtVolume.Rows[0][dcValues.ColumnName] * _dblCurrencyFactor);
                                else
                                    drAct[dcValues.ColumnName] = (dblValue * _dblCurrencyFactor);
                            }
                            else
                            {
                                drAct[dcValues.ColumnName] = dblValue * _dblCurrencyFactor;
                            }
                        }
                    }
                    dtValues.Rows.Add(drAct);
                    dtValues.AcceptChanges();
                }

                clsModels objModels = new clsModels();
                this.FillGrid(this.storeBaseCostField, this.grdBaseCostByField, dtValues, _strCurrencyCode);

                // ToDo: can reset language of columns here
                ArrayList datColumn = new ArrayList();
                datColumn.Add(new DataClass.DataTableColumnDisplay("Activity", "Parent Cost Category"));
                datColumn.Add(new DataClass.DataTableColumnDisplay("Cost", "Cost Category"));

                String strSelectedParameters = lblLevels.Text + " '" + ddlLevels.SelectedItem.Text + "', " +
                                               lblField.Text + " '" + (String.IsNullOrEmpty(ddlField.SelectedItem.Text) == true ? "- All -" : ddlField.SelectedItem.Text) + "', " +
                                               lblStructure.Text + " '" + ddlStructure.SelectedItem.Text + "', " +
                                               lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "', " +
                                               lblOperationType.Text + " '" + ddlOperationType.SelectedItem.Text + "', " +
                                               lblCostType.Text + " '" + ddlCostType.SelectedItem.Text + "'";
                ExportCostByField = new DataReportObject(dtValues, "Base Cost References", "Internal Base Cost References", strSelectedParameters, null, "Activity,Cost", datColumn, null);
            }
            #endregion
        }

        private void FillGridVolume(Ext.Net.Store s, GridPanel g, DataTable dt, String CurrencyCode)
        {
            s.RemoveFields();

            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "Name")
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Auto));
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Float));
                }
            }

            g.Features.Clear();

            GridFilters f = new GridFilters();
            f.Local = true;
            g.Features.Add(f);

            foreach (DataColumn c in dt.Columns)
            {
                if (!c.ColumnName.Contains("Id"))
                {
                    if (c.ColumnName == "Name")
                    {
                        StringFilter sFilter = new StringFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                    else
                    {
                        NumericFilter sFilter = new NumericFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                }
            }

            Column col = new Column();
            col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Fields");
            SummaryColumn colSum = new SummaryColumn();
            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "Name")
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        if (c.ColumnName != "Activity")
                        {
                            colSum = new SummaryColumn();
                            colSum.DataIndex = c.ColumnName;
                            colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", "Volume Driver Name");
                            colSum.Locked = true;
                            colSum.SummaryType = Ext.Net.SummaryType.Count;
                            colSum.TdCls = "task";
                            colSum.Sortable = true;
                            colSum.Groupable = true;
                            colSum.Width = 300;
                            colSum.SummaryRenderer.Handler = "return '(' + value + ' Total)';";
                            colSum.MenuDisabled = false;
                            colSum.Align = Alignment.Left;
                            g.ColumnModel.Columns.Add(colSum);
                        }
                    }
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        colSum = new SummaryColumn();
                        colSum.DataIndex = c.ColumnName;
                        colSum.Text = c.ColumnName.Replace("_", " ");
                        colSum.SummaryType = Ext.Net.SummaryType.Sum;
                        Renderer sumRen = new Renderer();
                        sumRen.Fn = "Ext.util.Format.numberRenderer('0,000.00')";
                        colSum.SummaryRenderer = sumRen;
                        colSum.Renderer = sumRen;
                        colSum.Align = Alignment.Right;
                        colSum.MinWidth = 120;
                        col.Columns.Add(colSum);
                    }
                }
            }

            g.ColumnModel.Columns.Add(col);

            s.DataSource = dt;
            //s.GroupField = "Name";
            if (X.IsAjaxRequest)
            {
                g.Reconfigure();
            }
            s.DataBind();
        }

        private void FillGrid(Ext.Net.Store s, GridPanel g, DataTable dt, String CurrencyCode)
        {
            if (Convert.ToInt32(ddlStructure.Value) == 1)
            #region (Ziff/Third-party Cost Categories)
            {
                s.RemoveFields();

                foreach (DataColumn c in dt.Columns)
                {
                    if (c.ColumnName == "Activity" || c.ColumnName == "Cost")
                    {
                        if (!c.ColumnName.Contains("Id"))
                            s.AddField(new ModelField(c.ColumnName, ModelFieldType.Auto));
                    }
                    else
                    {
                        if (!c.ColumnName.Contains("Id"))
                            s.AddField(new ModelField(c.ColumnName, ModelFieldType.Float));
                    }
                }

                //g.Features.Clear();

                //GroupingSummary gsum = new GroupingSummary();
                //gsum.GroupHeaderTplString = "{name}";
                //gsum.HideGroupedHeader = true;
                //gsum.ShowSummaryRow = true;

                //g.Features.Add(gsum);

                //GridFilters f = new GridFilters();
                //f.Local = true;
                //g.Features.Add(f);

                //Summary smry = new Summary();
                //smry.Dock = new SummaryDock();
                //smry.ID = "Summary" + g.ID.ToString();
                //smry.ShowSummaryRow = false;    //Hide the Grand Summary Totals by setting to false

                //g.Features.Add(smry);

                //foreach (DataColumn c in dt.Columns)
                //{
                //    if (!c.ColumnName.Contains("Id"))
                //    {
                //        if (c.ColumnName == "Activity" || c.ColumnName == "Cost")
                //        {
                //            StringFilter sFilter = new StringFilter();
                //            sFilter.AutoDataBind = true;
                //            sFilter.DataIndex = c.ColumnName;
                //            f.Filters.Add(sFilter);
                //        }
                //        else
                //        {
                //            NumericFilter sFilter = new NumericFilter();
                //            sFilter.AutoDataBind = true;
                //            sFilter.DataIndex = c.ColumnName;
                //            f.Filters.Add(sFilter);
                //        }
                //    }
                //}

                Column col = new Column();
                col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Fields") + " (" + CurrencyCode + ")";
                SummaryColumn colSum = new SummaryColumn();
                foreach (DataColumn c in dt.Columns)
                {
                    if (c.ColumnName == "Activity" || c.ColumnName == "Cost")
                    {
                        if (!c.ColumnName.Contains("Id"))
                        {
                            if (c.ColumnName != "Activity")
                            {
                                colSum = new SummaryColumn();
                                colSum.DataIndex = c.ColumnName;
                                colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", "Cost");
                                //colSum.Locked = true;
                                colSum.SummaryType = Ext.Net.SummaryType.Count;
                                colSum.TdCls = "task";
                                colSum.Sortable = true;
                                colSum.Groupable = true;
                                colSum.Width = 300;
                                colSum.SummaryRenderer.Handler = "return '(' + value + ' Total)';";
                                colSum.MenuDisabled = false;
                                colSum.Align = Alignment.Left;
                                g.ColumnModel.Columns.Add(colSum);
                            }
                        }
                    }
                    else
                    {
                        if (!c.ColumnName.Contains("Id"))
                        {
                            colSum = new SummaryColumn();
                            colSum.DataIndex = c.ColumnName;
                            colSum.Text = c.ColumnName.Replace("_", " ");
                            colSum.SummaryType = Ext.Net.SummaryType.Sum;
                            Renderer sumRen = new Renderer();
                            sumRen.Fn = "Ext.util.Format.numberRenderer('0,000.00')";
                            colSum.SummaryRenderer = sumRen;
                            colSum.Renderer = sumRen;
                            colSum.Align = Alignment.Right;
                            colSum.MinWidth = 120;
                            col.Columns.Add(colSum);
                        }
                    }
                }

                g.ColumnModel.Columns.Add(col);

                s.DataSource = dt;
                s.GroupField = "Activity";
                if (X.IsAjaxRequest)
                {
                    g.Reconfigure();
                }
                s.DataBind();
            }
            #endregion
            else
            #region (Company Structure)
            {
                s.RemoveFields();

                foreach (DataColumn c in dt.Columns)
                {
                    if (c.ColumnName == "Activity" || c.ColumnName == "Cost")
                    {
                        if (!c.ColumnName.Contains("Id"))
                            s.AddField(new ModelField(c.ColumnName, ModelFieldType.Auto));
                    }
                    else
                    {
                        if (!c.ColumnName.Contains("Id"))
                            s.AddField(new ModelField(c.ColumnName, ModelFieldType.Float));
                    }
                }

                g.Features.Clear();

                GroupingSummary gsum = new GroupingSummary();
                gsum.GroupHeaderTplString = "{name}";
                gsum.HideGroupedHeader = true;
                gsum.ShowSummaryRow = true;

                g.Features.Add(gsum);

                GridFilters f = new GridFilters();
                f.Local = true;
                g.Features.Add(f);

                Summary smry = new Summary();
                smry.Dock = new SummaryDock();
                smry.ID = "Summary" + g.ID.ToString();
                smry.ShowSummaryRow = true;    //Hide the Grand Summary Totals by setting to false

                g.Features.Add(smry);

                foreach (DataColumn c in dt.Columns)
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        if (c.ColumnName == "Activity" || c.ColumnName == "Cost")
                        {
                            StringFilter sFilter = new StringFilter();
                            sFilter.AutoDataBind = true;
                            sFilter.DataIndex = c.ColumnName;
                            f.Filters.Add(sFilter);
                        }
                        else
                        {
                            NumericFilter sFilter = new NumericFilter();
                            sFilter.AutoDataBind = true;
                            sFilter.DataIndex = c.ColumnName;
                            f.Filters.Add(sFilter);
                        }
                    }
                }

                Column col = new Column();
                col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Fields") + " (" + CurrencyCode + ")";
                SummaryColumn colSum = new SummaryColumn();
                foreach (DataColumn c in dt.Columns)
                {
                    if (c.ColumnName == "Activity" || c.ColumnName == "Cost")
                    {
                        if (!c.ColumnName.Contains("Id"))
                        {
                            if (c.ColumnName != "Activity")
                            {
                                colSum = new SummaryColumn();
                                colSum.DataIndex = c.ColumnName;
                                colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", "Cost");
                                colSum.Locked = true;
                                colSum.SummaryType = Ext.Net.SummaryType.Count;
                                colSum.TdCls = "task";
                                colSum.Sortable = true;
                                colSum.Groupable = true;
                                colSum.Width = 300;
                                colSum.SummaryRenderer.Handler = "return '(Total ' + value + ' Cost Categories)';";
                                colSum.MenuDisabled = false;
                                colSum.Align = Alignment.Left;
                                g.ColumnModel.Columns.Add(colSum);
                            }
                        }
                    }
                    else
                    {
                        if (!c.ColumnName.Contains("Id"))
                        {
                            colSum = new SummaryColumn();
                            colSum.DataIndex = c.ColumnName;
                            colSum.Text = c.ColumnName.Replace("_", " ");
                            colSum.SummaryType = Ext.Net.SummaryType.Sum;
                            Renderer sumRen = new Renderer();
                            sumRen.Fn = "Ext.util.Format.numberRenderer('0,000.00')";
                            colSum.SummaryRenderer = sumRen;
                            colSum.Renderer = sumRen;
                            colSum.Align = Alignment.Right;
                            colSum.MinWidth = 120;
                            col.Columns.Add(colSum);
                        }
                    }
                }

                g.ColumnModel.Columns.Add(col);

                s.DataSource = dt;
                s.GroupField = "Activity";
                if (X.IsAjaxRequest)
                {
                    g.Reconfigure();
                }
                s.DataBind();
            }
            #endregion
        }

        private Int32 GetIdField(String _Name)
        {
            Int32 intID = 0;

            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dtField = objFields.LoadList("Name = '" + _Name + "' AND IdModel = " + IdModel, "ORDER BY Name");

            if (dtField.Rows.Count > 0)
            {
                intID = (Int32)dtField.Rows[0]["IdField"];
            }

            return intID;
        }

        private Int32 GetIdLevel(String _Name)
        {
            Int32 intID = 0;

            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dtLevel = objAggregationLevels.LoadList("IdModel = " + IdModel + " AND Name = '" + _Name + "'", "ORDER BY Name");

            if (dtLevel.Rows.Count > 0)
            {
                intID = (Int32)dtLevel.Rows[0]["IdAggregationLevel"];
            }

            return intID;
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExportCostByField = new DataReportObject();
            StoredExportCostByField = ExportCostByField;
            DataReportObject StoredExportCostByFieldVol = new DataReportObject();
            StoredExportCostByFieldVol = ExportCostByFieldVol;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExportCostByField);
            objWriter.ExportReportCollection.Add(StoredExportCostByFieldVol);
            objWriter.BuildExportFile(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2007);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=InternalBaseCostReferences.xlsx");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        private void DisableParameterSelection()
        {
            this.pnlExportToolbar.Hidden = false;
            this.pnlReportToolbar.Hidden = true;
            this.ddlLevels.ReadOnly = true;
            this.ddlField.ReadOnly = true;
            this.ddlStructure.ReadOnly = true;
            this.ddlCurrency.ReadOnly = true;
            this.ddlCostType.ReadOnly = true;
            this.ddlOperationType.ReadOnly = true;
        }

        public void ModelCheckRecalcModuleAllocation()
        {
            clsModels objModelRecalc = new clsModels();
            objModelRecalc.IdModel = IdModel;
            objModelRecalc.CheckRecalcModuleParameter(objApplication.MaxRelationLevels);
            objModelRecalc.CheckRecalcModuleAllocation();
            objModelRecalc = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = modMain.strMasterBaseCostbyFieldCompany; 
            this.btnReset.Text = (String)GetGlobalResourceObject("CPM_Resources", "Reset");
            this.btnSaveExcel.Text = modMain.strReportsModuleExportReportDataToExcel;
            this.lblLevels.Text = (String)GetGlobalResourceObject("CPM_Resources", "Aggregation Level") + ":";
            this.lblField.Text = modMain.strTechnicalModuleField;
            this.lblOperationType.Text = (String)GetGlobalResourceObject("CPM_Resources", "Operation Type") + ":";
            this.lblCurrency.Text = (String)GetGlobalResourceObject("CPM_Resources", "Currency")+":";
            this.lblStructure.Text = (String)GetGlobalResourceObject("CPM_Resources", "Cost Structure") + ":";
            this.lblCostType.Text = (String)GetGlobalResourceObject("CPM_Resources", "Cost Type") + ":";
            this.btnRun.Text = (String)GetGlobalResourceObject("CPM_Resources", "Run Report");
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.pnlVolumeResults.Title = (String)GetGlobalResourceObject("CPM_Resources", "Volume Results");
        }

        #endregion

    }
}
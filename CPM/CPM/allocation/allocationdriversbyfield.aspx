﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="allocationdriversbyfield.aspx.cs" Inherits="CPM._allocationdriversbyfield" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function applyPageSize(grid) {
            var headerHeight = 22;
            var rowHeight = 21;

            var gridTopOffset = 100;
            var gridBottomOffset = 50;
            var rowHeight = 20;
            var myHeight = window.innerHeight;

            grid.store.pageSize = Math.round(((myHeight - gridTopOffset - gridBottomOffset) / rowHeight) - 3);
            grid.store.load();
        }
    </script>
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" AutoScroll="true" runat="server" Title="Allocation Drivers Data" IconCls="icon-allocationdriverfield_16" Border="false">
                <Items>
                    <ext:Panel ID="Panel2" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                        <Items>
                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lblFilterDriver" runat="server" Text="Driver:"></ext:Label>
                            <ext:ComboBox Style="margin: 10px 10px 10px 10px" ID="ddlFilterDriver" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" Width="200" MinChars="0" QueryMode="Remote" ValueField="IdAllocationListDriver">
                                <ListConfig LoadingText="Searching..." MinWidth="400">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <div class="search-item-driver">
                                                <h3>{Name}</h3>
							                    <span>Type: {TypeUnit} - Unit: {Unit}</span>
						                    </div>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Store>
                                    <ext:Store ID="storeFilterDriver" runat="server" OnReadData="StoreFilterDriver_ReadData">
                                        <Model>
                                            <ext:Model ID="Model2" runat="server" IDProperty="IdAllocationListDriver">
                                                <Fields>
                                                    <ext:ModelField Name="IdAllocationListDriver" />
                                                    <ext:ModelField Name="Name" />
                                                    <ext:ModelField Name="TypeUnit" />
                                                    <ext:ModelField Name="Unit" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                                <Reader>
                                                    <ext:JsonReader />
                                                </Reader>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Select OnEvent="ddlFilterDriver_Select" />
                                </DirectEvents>
                            </ext:ComboBox>
                        </Items>
                    </ext:Panel>
                    <ext:GridPanel ID="grdField" runat="server" AutoScroll="true" Border="false" Layout="FitLayout">
                        <TopBar>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btnSave" runat="server" Text="Save Changes" Icon="DatabaseSave" Disabled="false" FormBind="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnSave_Click">
                                                <ExtraParams>
                                                    <ext:Parameter Name="rowsValues" Value="#{grdField}.getRowsValues(false)" Mode="Raw" Encode="true" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarSeparator />
                                    <ext:Button ID="btnDelete" Icon="Delete" ToolTip="Delete All" runat="server" Text="Delete All">
                                        <DirectEvents>
                                            <Click OnEvent="btnDeleteAll_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarSeparator />
                                    <ext:Button ID="btnImport" Icon="BookAdd" runat="server" Text="Import">
                                        <DirectEvents>
                                            <Click OnEvent="btnImport_Click">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarFill />
                                    <ext:Button ID="btnSaveExcel" runat="server" Text="Export Data To Excel" Icon="PageExcel">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveExcel_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnDownloadTemplate" runat="server" Text="Download Import Template" Icon="PageSave">
                                        <DirectEvents>
                                            <Click OnEvent="btnDownloadTemplate_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="storeField" OnReadData="StoreField_ReadData" runat="server">
                                <Model>
                                    <ext:Model ID="modelField" runat="server">
                                        <Fields>
                                            <ext:ModelField Name="IdField" Type="Int" />
                                            <ext:ModelField Name="IdAllocationDriversByField" Type="Int" />
                                            <ext:ModelField Name="Code" Type="String" />
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="Amount" Type="Float" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel ID="cmRoles" runat="server">
                            <Columns>
                                <ext:Column Hidden="true" ID="colIdField" runat="server" Text="IdField" DataIndex="IdField" />
                                <ext:Column Hidden="true" ID="colIdAllocationDriversByField" runat="server" Text="IdAllocationDriversByField" DataIndex="IdAllocationDriversByField" />
                                <ext:Column ID="colName" runat="server" Text="Name" DataIndex="Name" Flex="1" />
                                <ext:NumberColumn ID="columAmount" runat="server" Format="0,000.00" Text="Amount" DataIndex="Amount" Flex="1" Align="Right" MaxWidth="200">
                                    <Editor>
                                        <ext:NumberField DecimalPrecision="2" MinValue="0" ID="numValue" runat="server" />
                                    </Editor>
                                </ext:NumberColumn>
                            </Columns>
                        </ColumnModel>
                        <Features>
                            <ext:GridFilters runat="server" ID="GridFilters2" Local="true">
                                <Filters>
                                    <ext:StringFilter DataIndex="Name" />
                                    <ext:NumericFilter DataIndex="Amount" />
                                </Filters>
                            </ext:GridFilters>
                        </Features>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" />
                        </SelectionModel>
                        <Plugins>
                            <ext:CellEditing ID="CellEditing1" runat="server">
                            </ext:CellEditing>
                        </Plugins>
                        <View>
                            <ext:GridView MarkDirty="false" />
                        </View>                        
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="true">
                            </ext:PagingToolbar>
                        </BottomBar>
                        <Listeners>
                            <Resize Handler="applyPageSize(this)" Buffer="250" />            
                        </Listeners>
                    </ext:GridPanel>
                    <ext:Window ID="winImport" runat="server" Title="Import Allocation Drivers Data" Hidden="true" Icon="ArrowLeft" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="frmImportFile" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                                <Items>
                                    <ext:Panel ID="pnlImportFile1" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:FileUploadField ID="fileImport" FieldLabel="File" runat="server" Icon="Attach" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnImportFile" runat="server" Text="Import" Icon="TableRefresh" Disabled="true" FormBind="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnImportFile_Click">
                                                <EventMask ShowMask="true" Target="CustomTarget" CustomTarget="vpMain" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnCancelImport" runat="server" Icon="Decline" Text="Close">
                                        <DirectEvents>
                                            <Click OnEvent="btnCancelImport_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                                <BottomBar>
                                    <ext:StatusBar ID="FormImportStatusBar" runat="server" />
                                </BottomBar>
                                <Listeners>
                                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                            });
                                                            #{btnImportFile}.setDisabled(!valid);" />
                                </Listeners>
                            </ext:FormPanel>
                        </Items>
                    </ext:Window>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="FormStatusBar" runat="server" />
                </BottomBar>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>

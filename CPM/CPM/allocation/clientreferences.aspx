﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="clientreferences.aspx.cs" Inherits="CPM._clientreferences" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_iconxl.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Border="false" Layout="FormLayout" OverflowY="Scroll">
        <Items>
            <ext:Panel ID="pnlParameters1" runat="server" Title="Company Cost References" IconCls="icon-clientreferences_16" BodyPadding="10" Border="false" Layout="Column">
                <Items>
                    <ext:FieldSet ID="FieldSet1" runat="server" Title="1. Company Cost Structure" Flex="1" Cls="buttongroup-bordertop" Layout="AnchorLayout" ColumnWidth="1">
                        <Defaults>
                            <ext:Parameter Name="HideEmptyLabel" Value="false" Mode="Raw" />
                        </Defaults>
                        <Items>
                            <ext:Button ID="btnActivities" runat="server" Disabled="true" Cls="icon-activities_48" Text="Activities" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnActivities_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnResources" runat="server" Disabled="true" Cls="icon-resources_48" Text="Resources" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnResources_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnCostCenters" runat="server" Disabled="true" Cls="icon-clientcostcenters_48" Text="Cost Objects" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnClientCostCenters_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnClientAccounts" runat="server" Disabled="true" Cls="icon-clientaccounts_48" Text="Chart of Accounts" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnClientAccounts_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:FieldSet>
                    <ext:FieldSet ID="FieldSet2" runat="server" Title="2. Company Cost Input" Flex="1" Cls="buttongroup-bordertop" Layout="AnchorLayout" ColumnWidth="1">
                        <Defaults>
                            <ext:Parameter Name="HideEmptyLabel" Value="false" Mode="Raw" />
                        </Defaults>
                        <Items>
                            <ext:Button ID="btnClientCostData" runat="server" Disabled="true" Cls="icon-clientcostdata_48" Text="Base Cost Data" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnClientCostData_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:FieldSet>
                    <ext:FieldSet ID="FieldSet3" runat="server" Title="3. Cost Allocation Process" Flex="1" Cls="buttongroup-bordertop" Layout="AnchorLayout" ColumnWidth="1">
                        <Defaults>
                            <ext:Parameter Name="HideEmptyLabel" Value="false" Mode="Raw" />
                        </Defaults>
                        <Items>
                            <ext:Button ID="btnDirect" runat="server" Disabled="true" Cls="icon-direct_48" Text="Direct" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnDirect_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnShare" runat="server" Disabled="true" Cls="icon-share_48" Text="Shared" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnShare_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnHierarchy" runat="server" Disabled="true" Cls="icon-hierarchy_48" Text="Hierarchy" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnHierarchy_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:FieldSet>
                    <ext:FieldSet ID="FieldSet4" runat="server" Title="4. Cost Allocation Drivers" Flex="1" Cls="buttongroup-bordertop" Layout="AnchorLayout" ColumnWidth="1">
                        <Defaults>
                            <ext:Parameter Name="HideEmptyLabel" Value="false" Mode="Raw" />
                        </Defaults>
                        <Items>
                            <ext:Button ID="btnListDrivers" runat="server" Disabled="true" Cls="icon-listallocationdrivers_48" Text="List of Cost Allocation Drivers" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnListDrivers_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnAllocationDrivers" runat="server" Disabled="true" Cls="icon-allocationdrivercost_48" Text="Cost Allocation Drivers Criteria" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnAllocationDriversByCostCenter_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnAllocationDriversData" runat="server" Disabled="true" Cls="icon-allocationdriverfield_48" Text="Cost Allocation Drivers Data" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnAllocationDriversData_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:FieldSet>
                    <ext:FieldSet ID="FieldSet5" runat="server" Title="5. Base Cost" Flex="1" Cls="buttongroup-bordertop" Layout="AnchorLayout" ColumnWidth="1">
                        <Defaults>
                            <ext:Parameter Name="HideEmptyLabel" Value="false" Mode="Raw" />
                        </Defaults>
                        <Items>
                            <ext:Button ID="btnZiffMapping" runat="server" Disabled="true" Cls="icon-mapping_48" Text="Mapping (Company Structure vs. Cost Projection Categories)" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnZiffMapping_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBaseCostByField" runat="server" Disabled="true" Cls="icon-clientcostfieldclient_48" Text="Internal Base Cost References" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnBaseCostByField_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBaseCostByFieldZiff" runat="server" Disabled="true" Cls="icon-clientcostfieldziff_48" Text="Base Cost By Field<br>(Ziff/Third-party Categories)" IconAlign="Top" Scale="Large" Visible="false">
                                <DirectEvents>
                                    <Click OnEvent="btnBaseCostByFieldZiff_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:FieldSet>
                    <ext:FieldSet ID="FieldSet6" runat="server" Title="6. Internal Benchmarks Files Repository" Flex="1" Cls="buttongroup-bordertop" Layout="AnchorLayout" ColumnWidth="1" Visible="false">
                        <Defaults>
                            <ext:Parameter Name="HideEmptyLabel" Value="false" Mode="Raw" />
                        </Defaults>
                        <Items>
                            <ext:Button ID="btnInternalBenchmarkRepository" runat="server" Disabled="true" Text="Internal Benchmarks Files Repository" IconAlign="Top" Scale="Large" Cls="icon-zifftechnicalanalysis_48">
                                <DirectEvents>
                                    <Click OnEvent="btnInternalBenchmarkRepository_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:FieldSet>
                </Items>
            </ext:Panel>
        </Items>
    </ext:Viewport>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ziffpeercriteria.aspx.cs" Inherits="CPM._ziffpeercriteria" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function applyPageSize(grid) {
            var headerHeight = 22;
            var rowHeight = 21;

            var gridTopOffset = 100;
            var gridBottomOffset = 50;
            var rowHeight = 20;
            var myHeight = window.innerHeight;

            grid.store.pageSize = Math.round(((myHeight - gridTopOffset - gridBottomOffset) / rowHeight)); // - 3);
            grid.store.load();
        }
    </script>
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" Layout="FitLayout" AutoScroll="true" runat="server">
                <Items>
                    <ext:GridPanel ID="grdPeerCriteria" AutoScroll="true" runat="server" Border="false" Title="List of Peer Criteria" IconCls="icon-listpeercriteria_16">
                        <TopBar>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btnAdd" Icon="Add" ToolTip="Add New Peer Criteria" runat="server" Text="Add New Peer Criteria">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarSeparator />
                                    <ext:Button ID="btnImport" Icon="BookAdd" runat="server" Text="Import">
                                        <DirectEvents>
                                            <Click OnEvent="btnImport_Click">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarFill />
                                    <ext:Button ID="btnSaveExcel" runat="server" Text="Export Data To Excel" Icon="PageExcel">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveExcel_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnDownloadTemplate" runat="server" Text="Download Import Template" Icon="PageSave">
                                        <DirectEvents>
                                            <Click OnEvent="btnDownloadTemplate_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="storePeerCriteria" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="IdActivity">
                                        <Fields>
                                            <ext:ModelField Name="IdPeerCriteria" Type="Int" />
                                            <ext:ModelField Name="Description" Type="String" />
                                            <ext:ModelField Name="IdUnitPeerCriteria" Type="Int" />
                                            <ext:ModelField Name="Unit" Type="String" />
                                            <ext:ModelField Name="IdTypeUnit" Type="Int" />
                                            <ext:ModelField Name="TypeUnit" Type="String" />
                                            <ext:ModelField Name="TotalUnitCostDenominator" Type="Boolean" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel ID="ColumnModel1" runat="server">
                            <Columns>
                                <ext:Column Hidden="true" ID="colIdPeerCriteria" runat="server" Text="ID" DataIndex="IdPeerCriteria"></ext:Column>
                                <ext:Column ID="colDescription" runat="server" Text="Criteria" DataIndex="Description" Flex="1"></ext:Column>
                                <ext:Column Hidden="true" ID="colIdTypeUnit" runat="server" DataIndex="IdTypeUnit"></ext:Column>
                                <ext:Column Hidden="true" ID="colIdUnitPeerCriteria" runat="server" DataIndex="IdUnitPeerCriteria"></ext:Column>
                                <ext:Column ID="colTypeUnit" runat="server" Text="Unit Type" DataIndex="TypeUnit" Flex="1"></ext:Column>
                                <ext:Column ID="colUnit" runat="server" Text="Unit" DataIndex="Unit" Flex="1"></ext:Column>
                                <ext:ComponentColumn ID="colTotalUnitCostDenominator" runat="server" Editor="true" Align="Center" DataIndex="TotalUnitCostDenominator" Flex="1" Text="Total Unit Cost Denominator">
                                    <Component>
                                        <ext:Checkbox ID="Checkbox1" Checked="true" runat="server" ReadOnly="True" />
                                    </Component>
                                </ext:ComponentColumn>
                               <ext:ImageCommandColumn Align="Center" Width="150" Text="Functions" ID="ImageCommandColumn1" runat="server">
                                    <Commands>
                                        <ext:ImageCommand CommandName="Edit" IconCls="icon-edit_16" Text="&nbsp;Edit" />
                                    </Commands>
                                    <Commands>
                                        <ext:ImageCommand CommandName="Delete" IconCls="icon-delete_16" Text="&nbsp;Delete" />
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="grdPeerCriteria_Command">
                                            <ExtraParams>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="Id" Value="record.data.IdPeerCriteria" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="Name" Value="record.data.Description" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:ImageCommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" />
                        </SelectionModel>
                        <Features>
                            <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                                <Filters>
                                    <ext:StringFilter DataIndex="Description" />
                                    <ext:StringFilter DataIndex="TypeUnit" />
                                    <ext:StringFilter DataIndex="Unit" />
                                </Filters>
                            </ext:GridFilters>
                        </Features>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="true">
                            </ext:PagingToolbar>
                        </BottomBar>
                        <Listeners>
                            <Resize Handler="applyPageSize(this)" Buffer="250" />            
                        </Listeners>
                    </ext:GridPanel>
                    <ext:Window ID="winPeerCriteriaEdit" runat="server" Title="Peer Criteria" Hidden="true" IconCls="icon-listpeercriteria_16" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="pnlEditActivity" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                                <Items>
                                    <ext:Panel ID="Panel2" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:TextField ID="txtDescription" runat="server" FieldLabel="Description" AnchorHorizontal="92%" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel3" runat="server" Border="false" Layout="Form" ColumnWidth=".5" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:ComboBox ID="ddlTypeUnit" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" FieldLabel="Unit Type" ValueField="IdTypeUnit" Cls="PopupFormField">
                                                <ListConfig LoadingText="Searching..." MinWidth="200">
                                                    <ItemTpl ID="ItemTpl1" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeTypeUnit" runat="server" OnReadData="StoreTypeUnit_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model2" runat="server" IDProperty="IdTypeUnit">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdTypeUnit" />
                                                                    <ext:ModelField Name="Name" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                                <DirectEvents>
                                                    <Select OnEvent="ddlTypeUnit_Select" />
                                                </DirectEvents>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel1" runat="server" Border="false" Header="false" ColumnWidth=".5" Layout="Form" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:ComboBox ID="ddlUnit" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" FieldLabel="Unit" ValueField="IdUnit" Cls="PopupFormField" ReadOnly="true">
                                                <ListConfig LoadingText="Searching..." MinWidth="200">
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeUnit" runat="server" OnReadData="StoreUnit_ReadData">
                                                        <Model>
                                                            <ext:Model ID="mdlLabels" runat="server" IDProperty="IdUnit">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdUnit" />
                                                                    <ext:ModelField Name="Name" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel  ID="Panel4" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel" ColSpan="4">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:Checkbox ID="chkTotalUnitCostDenominator" runat="server" FieldLabel="Total Unit Cost Denominator" Cls="PopupFormField" ReadOnly="False">
                                            </ext:Checkbox>
                                        </Items>
                                     </ext:Panel>
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnSave" runat="server" Text="Save" Icon="DatabaseSave" Disabled="true" FormBind="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnSave_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnCancel" runat="server" Icon="Decline" Text="Close">
                                        <DirectEvents>
                                            <Click OnEvent="btnCancel_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                                <BottomBar>
                                    <ext:StatusBar ID="FormStatusBar" runat="server" />
                                </BottomBar>
                                <Listeners>
                                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                            });
                                            #{btnSave}.setDisabled(!valid);" />
                                </Listeners>
                            </ext:FormPanel>
                        </Items>
                    </ext:Window>
                    <ext:Window ID="winImport" runat="server" Title="Import Peer Criteria Data" Hidden="true" Icon="ArrowLeft" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="frmImportFile" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                                <Items>
                                    <ext:Panel ID="pnlImportFile1" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:FileUploadField ID="fileImport" FieldLabel="File" runat="server" Icon="Attach" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnImportFile" runat="server" Text="Import" Icon="TableRefresh" Disabled="true" FormBind="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnImportFile_Click" >
                                                <EventMask ShowMask="true" Target="CustomTarget" CustomTarget="vpMain" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnCancelImport" runat="server" Icon="Decline" Text="Close">
                                        <DirectEvents>
                                            <Click OnEvent="btnCancelImport_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                                <BottomBar>
                                    <ext:StatusBar ID="FormImportStatusBar" runat="server" />
                                </BottomBar>
                                <Listeners>
                                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                            });
                                                            #{btnImportFile}.setDisabled(!valid);" />
                                </Listeners>
                            </ext:FormPanel>
                        </Items>
                    </ext:Window>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="statusDialogs" runat="server" />
                </BottomBar>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _clientreferences : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUser { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public String NameUser { get { return (String)Session["NameUser"]; } set { Session["NameUser"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"]; }  }//  set { Session["idLanguage"] = value; } } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }

        private static modMain objMain = new modMain();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                this.LoadLanguage();
                this.LoadModules();
            }
        }

        /// <summary> Activities </summary>
        protected void btnActivities_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/activities.aspx");
        }

        /// <summary> Resources </summary>
        protected void btnResources_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/resources.aspx");
        }

        /// <summary> Cost Objects </summary>
        protected void btnClientCostCenters_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/clientcostcenters.aspx");
        }

        /// <summary> Chart of Accounts </summary>
        protected void btnClientAccounts_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/clientaccounts.aspx");
        }

        /// <summary> Base Cost Data </summary>
        protected void btnClientCostData_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/clientcostdata.aspx");
        }

        /// <summary> Direct </summary>
        protected void btnDirect_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/directcostallocation.aspx");
        }

        /// <summary> Shared </summary>
        protected void btnShare_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/sharedcostallocation.aspx");
        }

        /// <summary> Hierarchy </summary>
        protected void btnHierarchy_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/hierarchycostallocation.aspx");
        }

        /// <summary> List of Cost Allocation Drivers </summary>
        protected void btnListDrivers_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/allocationlistdrivers.aspx");
        }

        /// <summary> Cost Allocation Drivers Criteria </summary>
        protected void btnAllocationDriversByCostCenter_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/allocationdriverdata.aspx");
        }

        /// <summary> Cost Allocation Drivers Data </summary>
        protected void btnAllocationDriversData_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/allocationdriversbyfield.aspx");
        }

        /// <summary> Mapping (Company Structure vs. Cost Projection Categories) </summary>
        protected void btnZiffMapping_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/mappingziffvsclient.aspx");
        }

        /// <summary> Internal Base Cost References </summary>
        protected void btnBaseCostByField_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/basecostbyfield.aspx");
        }

        /// <summary> NOT USED </summary>
        protected void btnBaseCostByFieldZiff_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/basecostbyfieldziff.aspx");
        }

        /// <summary> Internal Benchmarks Files Repository (NOT USED) </summary>
        protected void btnInternalBenchmarkRepository_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/internalfilesrepository.aspx");
        }

        protected void btnObject_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/ObjectCostAllocation.aspx");
        }

        #endregion

        #region Methods

        protected void LoadLanguage()
        {
            DataClass.clsLabelsLanguages objLabelsLanguages = new DataClass.clsLabelsLanguages();
            DataTable dt = objLabelsLanguages.LoadList("LanguageName = '" + Language + "' AND FormName = 'ClientReferences'", "ORDER BY IdLabel");
            foreach (DataRow row in dt.Rows)
            {
                //idLanguage = (Int32)row["IdLanguage"];
                switch ((String)row["LabelName"])
                {
                    case "1. Company Cost Input":
                        modMain.strClientReferenceCompanyCostInput = (String)row["LabelText"];
                        break;
                    case "2. Cost Allocation Process":
                        modMain.strClientReferenceCostAllocationProcess = (String)row["LabelText"];
                        break;
                    case "3. Allocation Drivers":
                        modMain.strClientReferenceAllocationDrivers = (String)row["LabelText"];
                        break;
                    case "4. Base Cost":
                        modMain.strClientReferenceBaseCost = (String)row["LabelText"];
                        break;
                    case "Mapping (Company Structure vs. Ziff/Third-party)":
                        modMain.strClientReferenceMappingShort = (String)row["LabelText"];
                        break;
                    case "Base Cost By Field (Ziff/Third-party Categories)":
                        modMain.strClientReferenceBaseCostbyFieldZiffShort = (String)row["LabelText"];
                        break;
                    case "2. Company Cost Structure":
                        modMain.strParameterModuleFormSubTitle2 = (String)row["LabelText"];
                        break;
                }
            }
            this.LoadLabel();
        }

        protected void LoadModules()
        {
            // Allocation Module
            if (objApplication.AllowedRoles.Contains(13))
            {
                // Activities
                if (objApplication.AllowedRoles.Contains(9))
                {
                    this.btnActivities.Disabled = false;
                }
                // Resources
                if (objApplication.AllowedRoles.Contains(10))
                {
                    this.btnResources.Disabled = false;
                }
                // Cost Objects
                if (objApplication.AllowedRoles.Contains(11))
                {
                    this.btnCostCenters.Disabled = false;
                }
                // Chart of Accounts
                if (objApplication.AllowedRoles.Contains(12))
                {
                    this.btnClientAccounts.Disabled = false;
                }
                // Base Cost Data
                if (objApplication.AllowedRoles.Contains(14))
                {
                    this.btnClientCostData.Disabled = false;
                }
                // Direct
                if (objApplication.AllowedRoles.Contains(15))
                {
                    this.btnDirect.Disabled = false;
                }
                // Shared
                if (objApplication.AllowedRoles.Contains(16))
                {
                    this.btnShare.Disabled = false;
                }
                // Hierarchy
                if (objApplication.AllowedRoles.Contains(17))
                {
                    this.btnHierarchy.Disabled = false;
                }
                // List of Cost Allocation Drivers
                if (objApplication.AllowedRoles.Contains(18))
                {
                    this.btnListDrivers.Disabled = false;
                }
                // Cost Allocation Drivers Criteria
                if (objApplication.AllowedRoles.Contains(19))
                {
                    this.btnAllocationDrivers.Disabled = false;
                }
                // Cost Allocation Drivers Data
                if (objApplication.AllowedRoles.Contains(20))
                {
                    this.btnAllocationDriversData.Disabled = false;
                }
                // Base Cost by Field (Company Structure)
                if (objApplication.AllowedRoles.Contains(20))
                {
                    this.btnBaseCostByField.Disabled = false;
                }
                // Mapping (Company Cost Structure vs. Cost Projection Categories)
                if (objApplication.AllowedRoles.Contains(22))
                {
                    this.btnZiffMapping.Disabled = false;
                }
                // Internal Base Cost References
                if (objApplication.AllowedRoles.Contains(23))
                {
                    this.btnBaseCostByFieldZiff.Disabled = false;
                }
            }
        }

        protected void LoadLabel()
        {
            this.pnlParameters1.Title = modMain.strMasterCompanyCostReferences;
            this.FieldSet2.Title = modMain.strClientReferenceCompanyCostInput;
            this.btnClientCostData.Text = modMain.strMasterBaseCostData;
            this.FieldSet3.Title = modMain.strClientReferenceCostAllocationProcess;
            this.btnDirect.Text = modMain.strMasterDirect;
            this.btnShare.Text = modMain.strMasterShared;
            this.btnHierarchy.Text = modMain.strMasterHierarchy;
            this.FieldSet4.Title = modMain.strClientReferenceAllocationDrivers;
            this.btnListDrivers.Text = objMain.FormatButtonText(modMain.strMasterListofAllocationDrivers);
            this.btnAllocationDrivers.Text = objMain.FormatButtonText(modMain.strMasterAllocationDriversCriteria);
            this.btnAllocationDriversData.Text = objMain.FormatButtonText(modMain.strMasterAllocationDriversData);
            this.FieldSet5.Title = modMain.strClientReferenceBaseCost;
            this.btnZiffMapping.Text = objMain.FormatButtonText(modMain.strClientReferenceMappingShort);
            this.btnBaseCostByField.Text = objMain.FormatButtonText(modMain.strMasterBaseCostbyFieldCompany);
            this.FieldSet6.Title = "6. " + (String)GetGlobalResourceObject("CPM_Resources", "Internal Benchmarks Files Repository");
            this.btnInternalBenchmarkRepository.Text = objMain.FormatButtonText((String)GetGlobalResourceObject("CPM_Resources", "Internal Benchmarks Files Repository"));
            this.FieldSet1.Title = modMain.strParameterModuleFormSubTitle2;
            this.btnActivities.Text = modMain.strMasterActivities;
            this.btnResources.Text = modMain.strMasterResources;
            this.btnCostCenters.Text = modMain.strMasterCostObjects;
            this.btnClientAccounts.Text = modMain.strMasterChartAccounts;
        }

        #endregion

    }
}
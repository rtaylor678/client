﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="allocationmodule.aspx.cs" Inherits="CPM._allocationmodule" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>


<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_iconxl.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Border="false" Layout="FormLayout" OverflowY="Scroll">
        <Items>
            <ext:Panel ID="pnlAllocationModule" runat="server" Title="Allocation Module" IconCls="icon-allocation_16" BodyPadding="10" Border="false" Layout="Column">
                <Items>
                    <ext:FieldSet ID="FieldSet3" runat="server" Title="1. References" Flex="1" Cls="buttongroup-bordertop" Layout="AnchorLayout" ColumnWidth="1">
                        <Defaults>
                            <ext:Parameter Name="HideEmptyLabel" Value="false" Mode="Raw" />
                        </Defaults>
                        <Items>
                            <ext:Button ID="btnClientReferences" runat="server" Disabled="true" Cls="icon-clientreferences_48" Text="Internal Cost References" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnClientReferences_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnZiffReferences" runat="server" Disabled="true" Cls="icon-ziffreferences_48" Text="External Cost References" IconAlign="Top" Scale="Large">
                                 <DirectEvents>
                                    <Click OnEvent="btnZiffReferences_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:FieldSet>   
                    <ext:FieldSet ID="FieldSet4" runat="server" Title="2. Assumptions" Flex="1" Cls="buttongroup-bordertop" Layout="AnchorLayout" ColumnWidth="1" Visible="false">
                        <Defaults>
                            <ext:Parameter Name="HideEmptyLabel" Value="false" Mode="Raw" />
                        </Defaults>
                        <Items>
                            <ext:Button ID="btnCostStructureAssumptions" runat="server" Disabled="true" Cls="icon-coststructure_48" Text="Cost Structure Assumptions" IconAlign="Top" Scale="Large">
                                    <DirectEvents>
                                    <Click OnEvent="btnCostStructureAssumptions_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:FieldSet>   
                    <ext:FieldSet ID="FieldSet5" runat="server" Title="2.Reports" Flex="1" Cls="buttongroup-bordertop" Layout="AnchorLayout" ColumnWidth="1">
                        <Defaults>
                            <ext:Parameter Name="HideEmptyLabel" Value="false" Mode="Raw" />
                        </Defaults>
                        <Items>
                            <ext:Button ID="btnCostBenchmarking" runat="server" Disabled="true" Cls="icon-opex_48" Text="CostBenchmarking" IconAlign="Top" Scale="Large">
                                    <DirectEvents>
                                    <Click OnEvent="btnCostBenchmarking_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:FieldSet>   
                </Items>
            </ext:Panel>
        </Items>
    </ext:Viewport>
</body>
</html>
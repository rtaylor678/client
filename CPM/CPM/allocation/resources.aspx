﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="resources.aspx.cs" Inherits="CPM._resources" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function applyPageSize(grid) {
            var headerHeight = 22;
            var rowHeight = 21;

            var gridTopOffset = 100;
            var gridBottomOffset = 50;
            var rowHeight = 20;
            var myHeight = window.innerHeight;

            grid.store.pageSize = Math.round(((myHeight - gridTopOffset - gridBottomOffset) / rowHeight)); // - 3);
            grid.store.load();
        }
    </script>
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" Layout="FitLayout" AutoScroll="true" runat="server">
                <Items>
                    <ext:GridPanel ID="grdResources" Height="500" AutoScroll="true" runat="server" Border="false" Title="Resources" IconCls="icon-resources_16">
                        <TopBar>
                            <ext:Toolbar ID="Toolbar2" runat="server">
                                <Items>
                                    <ext:Button ID="btnNew" Icon="Add" runat="server" Text="Add New Resource">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Click">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarSeparator />
                                    <ext:Button ID="btnDelete" Icon="Delete" ToolTip="Delete All" runat="server" Text="Delete All">
                                        <DirectEvents>
                                            <Click OnEvent="btnDeleteAll_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarSeparator />
                                    <ext:Button ID="btnImport" Icon="BookAdd" runat="server" Text="Import">
                                        <DirectEvents>
                                            <Click OnEvent="btnImport_Click">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarFill />
                                    <ext:Button ID="btnSaveExcel" runat="server" Text="Export Data To Excel" Icon="PageExcel">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveExcel_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnDownloadTemplate" runat="server" Text="Download Import Template" Icon="PageSave">
                                        <DirectEvents>
                                            <Click OnEvent="btnDownloadTemplate_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="storeResources" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="IdResources">
                                        <Fields>
                                            <ext:ModelField Name="IdResources" Type="Int" />
                                            <ext:ModelField Name="Code" Type="String" />
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="NameActivity" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel ID="ColumnModel1" runat="server">
                            <Columns>
                                <ext:Column ID="colID" Hidden="true" runat="server" Text="ID" DataIndex="IdActivity">
                                </ext:Column>
                                <ext:Column ID="colCode" runat="server" Text="Code" DataIndex="Code" Flex="1">
                                </ext:Column>
                                <ext:Column ID="colName" runat="server" Text="Name" DataIndex="Name" Flex="1">
                                </ext:Column>
                                <ext:Column ID="colActivityName" runat="server" Text="Activity" DataIndex="NameActivity" Flex="1">
                                </ext:Column>
                                <ext:ImageCommandColumn Align="Center" Width="150" Text="Functions" ID="ImageCommandColumn1"
                                    runat="server">
                                    <Commands>
                                        <ext:ImageCommand CommandName="Edit" IconCls="icon-edit_16" Text="&nbsp;Edit" />
                                    </Commands>
                                    <Commands>
                                        <ext:ImageCommand CommandName="Delete" IconCls="icon-delete_16" Text="&nbsp;Delete" />
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="grdResources_Command">
                                            <ExtraParams>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="Id" Value="record.data.IdResources" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="Name" Value="record.data.Name" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:ImageCommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" />
                        </SelectionModel>
                        <Features>
                            <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                                <Filters>
                                    <ext:StringFilter DataIndex="Name" />
                                    <ext:StringFilter DataIndex="Code" />
                                    <ext:StringFilter DataIndex="ActivityName" />
                                </Filters>
                            </ext:GridFilters>
                        </Features>
                        <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="true">
                            </ext:PagingToolbar>
                        </BottomBar>
                        <Listeners>
                            <Resize Handler="applyPageSize(this)" Buffer="250" />            
                        </Listeners>
                    </ext:GridPanel>
                    <ext:Window ID="winResourcesEdit" runat="server" Title="Resources" Hidden="true" IconCls="icon-resources_16" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="pnlwResources" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                                <Items>
                                    <ext:Panel ID="pnlwResources1" runat="server" Border="false" Header="false" ColumnWidth=".5" Layout="Form" LabelAlign="Top" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:TextField ID="txtCode" Width="250" MaxLength="15" runat="server" FieldLabel="Code" AnchorHorizontal="92%" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="pnlwResources2" runat="server" Border="false" Layout="Form" ColumnWidth=".5" LabelAlign="Top" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:TextField ID="txtName" runat="server" FieldLabel="Name" AnchorHorizontal="92%" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel1" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:ComboBox ID="ddlActivity" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" FieldLabel="Activity" ValueField="IdActivity" Cls="PopupFormField">
                                                <ListConfig LoadingText="Searching...">
                                                    <ItemTpl ID="ItemTpl2" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
							                                    <span>Code: </span>{Code}</h3>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="stroreActivity" runat="server" OnReadData="StoreAct_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model2" runat="server" IDProperty="IdActivity">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdActivity">
                                                                    </ext:ModelField>
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="Code" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnSave" runat="server" Text="Save" Icon="DatabaseSave" Disabled="true" FormBind="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnSave_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnCancel" runat="server" Icon="Decline" Text="Close">
                                        <DirectEvents>
                                            <Click OnEvent="btnCancel_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                                <BottomBar>
                                    <ext:StatusBar ID="FormStatusBar" runat="server" />
                                </BottomBar>
                                <Listeners>
                                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                        text : valid ? 'Form is valid' : 'Form is invalid', 
                                        iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                    });
                                    #{btnSave}.setDisabled(!valid);" />
                                </Listeners>
                            </ext:FormPanel>
                        </Items>
                    </ext:Window>
                    <ext:Window ID="winImport" runat="server" Title="Import Resources" Hidden="true" Icon="ArrowLeft" Resizable="false" Width="600" Height="140" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="frmImportFile" runat="server" Title=" " BodyPadding="5" Hidden="false" ButtonAlign="Center" Layout="Column">
                                <Items>
                                    <ext:Panel ID="pnlImportFile1" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" LabelAlign="Top" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:FileUploadField ID="fileImport" FieldLabel="File" runat="server" Icon="Attach" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnImportFile" runat="server" Text="Import" Icon="TableRefresh">
                                        <DirectEvents>
                                            <Click OnEvent="btnImportFile_Click">
                                                <EventMask ShowMask="true" Target="CustomTarget" CustomTarget="vpMain" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnCancelImport" runat="server" Icon="Decline" Text="Close">
                                        <DirectEvents>
                                            <Click OnEvent="btnCancelImport_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                                <BottomBar>
                                    <ext:StatusBar ID="FormImportStatusBar" runat="server" />
                                </BottomBar>
                                <Listeners>
                                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                        text : valid ? 'Form is valid' : 'Form is invalid', 
                                        iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                    });
                                    #{btnImportFile}.setDisabled(!valid);" />
                                </Listeners>
                            </ext:FormPanel>
                        </Items>
                    </ext:Window>
                </Items>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>
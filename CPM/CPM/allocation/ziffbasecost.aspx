﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ziffbasecost.aspx.cs" Inherits="CPM._ziffbasecost" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var formatZero = function (value, metaData, record, rowIndex, colIndex, store, view) {
            var str = record.data.Code;
            var pos = str.indexOf('-');

            if (pos != 4) {
                metaData.tdAttr = 'style="background-color:LightGray;font-weight:bold;"Disabled="true";';
                value = Ext.util.Format.number(value, '0,000.00');
                return value;
            }

            value = Ext.util.Format.number(value, '0,000.00');
            return value;
        }

        var formatCol = function (value, metaData, record, rowIndex, colIndex, store, view) {
            var str = record.data.Code;
            var pos = str.indexOf('-');
            if (pos != 4) {
                metaData.tdAttr = 'style="background-color:LightGray;font-weight:bold;"Enabled="false";';
                return value;
            }
            return value;
        }

        var prepare = function (grid, toolbar, rowIndex, record) {
            var firstButton = toolbar.items.get(0);
            var secondButton = toolbar.items.get(1);

            if (record.data.UnitCost == -99) {
                //firstButton.setDisabled(true);
                //firstButton.setTooltip("Disabled");
                firstButton.setVisible(false);
                secondButton.setVisible(false);
            }

            //you can return false to cancel toolbar for this record
        };

        var beforeCellEditHandler = function (e) {
            var str = e.record.raw.Code;
            var pos = str.indexOf('-');
            if (pos != 4 || e.originalValue == -1) {
                App.CellEditing1.cancelEdit();
                return false;
            }
        }

        var cellEditingHandler = function (e) {
            if (e.value < 0) {
                Ext.Msg.alert('Error!!!', 'Value must be greater than 0. (El valor debe estar mas que 0.)');
                e.record.data[e.field] = e.originalValue;
                e.cancel = true;
            }
        }

        Ext.num = function (v, defaultValue) {
            v = Number(Ext.isEmpty(v) || Ext.isArray(v) || typeof v == 'boolean' || (typeof v == 'string' && v.trim().length == 0) ? NaN : v);
            return isNaN(v) ? defaultValue : v;
        }

        Ext.util.Format.number = function (v, format) {
            if (!format) {
                return v;
            }
            v = Ext.num(v, NaN);
            if (isNaN(v)) {
                return '';
            }
            var comma = ',',
                dec = '.',
                i18n = false,
                neg = v < 0;

            v = Math.abs(v);
            if (format.substr(format.length - 2) == '/i') {
                format = format.substr(0, format.length - 2);
                i18n = true;
                comma = '.';
                dec = ',';
            }

            var hasComma = format.indexOf(comma) != -1,
                psplit = (i18n ? format.replace(/[^\d\,]/g, '') : format.replace(/[^\d\.]/g, '')).split(dec);

            if (1 < psplit.length) {
                v = v.toFixed(psplit[1].length);
            } else if (2 < psplit.length) {
                throw ('NumberFormatException: invalid format, formats should have no more than 1 period: ' + format);
            } else {
                v = v.toFixed(0);
            }

            var fnum = v.toString();

            psplit = fnum.split('.');

            if (hasComma) {
                var cnum = psplit[0],
                    parr = [],
                    j = cnum.length,
                    m = Math.floor(j / 3),
                    n = cnum.length % 3 || 3,
                    i;

                for (i = 0; i < j; i += n) {
                    if (i != 0) {
                        n = 3;
                    }

                    parr[parr.length] = cnum.substr(i, n);
                    m -= 1;
                }
                fnum = parr.join(comma);
                if (psplit[1]) {
                    fnum += dec + psplit[1];
                }
            } else {
                if (psplit[1]) {
                    fnum = psplit[0] + dec + psplit[1];
                }
            }

            return (neg ? '-' : '') + format.replace(/[\d,?\.?]+/, fnum);
        }
    </script>
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" 
                runat="server" 
                Title="External Base Cost References" 
                IconCls="icon-ziffbasecost_16" 
                AutoScroll="true" 
                Border="false"        
                Layout="BorderLayout" >
                <Items>
                    <%-- Peer Group drop down list --%>
                    <ext:Panel ID="Panel4" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0" Region="North">
                        <Items>
                            <ext:Label Style="margin: 10px 0 10px 10px" ID="lblFilterPeerGroup" runat="server" Text="Peer Group:"></ext:Label>
                            <ext:ComboBox Style="margin: 10px 0 10px 10px" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" QueryMode="Remote" ID="ddlPeerGroup" runat="server" DisplayField="PeerGroupDescription" ValueField="IdPeerGroup" Width="300">
                                <ListConfig LoadingText="Searching..." MinWidth="300">
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <div class="search-item">
							                    <h3>{PeerGroupDescription}</h3>
							                    <span>Code: {PeerGroupCode}</span>
						                    </div>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Store>
                                    <ext:Store ID="storePeerGroup" runat="server" OnReadData="StorePeerGroup_ReadData">
                                        <Model>
                                            <ext:Model ID="Model1" runat="server" IDProperty="IdPeerGroup">
                                                <Fields>
                                                    <ext:ModelField Name="IdPeerGroup" />
                                                    <ext:ModelField Name="PeerGroupCode" />
                                                    <ext:ModelField Name="PeerGroupDescription" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                                <Reader>
                                                    <ext:JsonReader />
                                                </Reader>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Select OnEvent="ddlPeerGroup_Select">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>
                            </ext:ComboBox>
                        </Items>
                    </ext:Panel>
                    <%-- Peer Group Criteria Grid --%>
                    <ext:GridPanel ID="grdParameters" 
                        runat="server" 
                        AutoScroll="true" 
                        Border="false"
                        Layout="AutoLayout"
                        EnableLocking="true"
                        Header="true" 
                        Region="North">
                        <%-- Import, Export and Template buttons --%>
                        <TopBar>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btnImport" Icon="BookAdd" runat="server" Text="Import">
                                        <DirectEvents>
                                            <Click OnEvent="btnImport_Click">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarFill />
                                    <ext:Button ID="btnSaveExcel" runat="server" Text="Export Data To Excel" Icon="PageExcel">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveExcel_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnDownloadTemplate" runat="server" Text="Download Import Template" Icon="PageSave">
                                        <DirectEvents>
                                            <Click OnEvent="btnDownloadTemplate_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="storeParameters" runat="server">
                                <Model>
                                    <ext:Model ID="Model2" runat="server">
                                        <Fields>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" />
                        </SelectionModel>
                    </ext:GridPanel>
                    <%-- Unit Cost Benchmark Grid --%>
                    <ext:GridPanel ID="grdBaseCost" 
                            runat="server"
                            Title="Base Cost" 
                            AutoScroll="true" 
                            Border="false"
                            Layout="AutoLayout"
                            Header="true"
                            EnableLocking="true"
                            Region="South"
                            Flex="100">
                            <TopBar>
                                <ext:Toolbar ID="Toolbar2" runat="server">
                                    <Items>
                                        <ext:Button ID="btnSaveGrid" Icon="DataBaseSave" ToolTip="Save Changes" runat="server" Text="Save Changes" TextAlign="Left">
                                            <DirectEvents>
                                                <Click OnEvent="btnSaveGrid_Click" Timeout="3600000" ShowWarningOnFailure="false">
                                                    <EventMask ShowMask="true" Msg="Saving changes..." Target="Body" CustomTarget="vpMain" />
                                                    <ExtraParams>
                                                        <ext:Parameter Name="rowsValues" Value="#{grdBaseCost}.getRowsValues(false)" Mode="Raw" Encode="true" />
                                                    </ExtraParams>
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </Items>
                                </ext:Toolbar>
                            </TopBar>
                            <Store>
                                <ext:Store ID="storeBaseCost" runat="server">
                                    <Model>
                                        <ext:Model ID="Model4" runat="server">
                                            <Fields>
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <ColumnModel>
                                <Columns>
                                </Columns>
                            </ColumnModel>
                            <SelectionModel>
                                <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" />
                            </SelectionModel>
                            <Listeners>
                                <Reconfigure Handler="this.setHeight(this.container.getHeight()+20);" Delay="100" />
                            </Listeners>
                            <Plugins>
                                <ext:CellEditing ID="CellEditing1" runat="server">
                                    <Listeners>
                                        <BeforeEdit Handler="return beforeCellEditHandler(e)"></BeforeEdit>
                                        <ValidateEdit Handler="cellEditingHandler(e);"></ValidateEdit>
                                    </Listeners>
                                </ext:CellEditing>
                            </Plugins>
                    </ext:GridPanel>
                </Items>
            </ext:Panel>
            <ext:Window ID="winSetBaseCost" runat="server" Title="Ziff/Third-party Base Cost" Hidden="true" Icon="ArrowLeft" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                <Items>
                    <ext:FormPanel ID="pnlCostCenterEdit" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                        <Items>
                            <ext:Panel ID="Panel2" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                <Defaults>
                                    <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                    <ext:Parameter Name="MsgTarget" Value="side" />
                                </Defaults>
                                <Items>
                                    <ext:ComboBox ID="ddlCriteria" runat="server" DisplayField="Description" Editable="false" TypeAhead="false" QueryMode="Local" ForceSelection="true" FieldLabel="Criteria" ValueField="IdPeerCriteria" EmptyText="Select..." Cls="PopupFormField">
                                        <ListConfig LoadingText="Searching...">
                                            <ItemTpl ID="ItemTpl3" runat="server">
                                                <Html>
                                                    <div class="search-item">
							                            <h3>{Description}</h3>
						                            </div>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Store>
                                            <ext:Store ID="storePeerCriteria" runat="server" OnReadData="StorePeerCriteria_ReadData">
                                                <Model>
                                                    <ext:Model ID="Model3" runat="server" IDProperty="IdPeerCriteria">
                                                        <Fields>
                                                            <ext:ModelField Name="IdPeerCriteria" />
                                                            <ext:ModelField Name="Description" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                                <Proxy>
                                                    <ext:PageProxy>
                                                        <Reader>
                                                            <ext:JsonReader />
                                                        </Reader>
                                                    </ext:PageProxy>
                                                </Proxy>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="Panel6" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                <Defaults>
                                    <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                    <ext:Parameter Name="MsgTarget" Value="side" />
                                </Defaults>
                                <Items>
                                    <ext:NumberField DecimalPrecision="2" MinValue="0" ID="txtCost" FieldLabel="Cost" runat="server" Width="250" Cls="PopupFormField" />
                                    <ext:Hidden ID="txtIDPeer" runat="server" />
                                    <ext:Hidden ID="txtIDField" runat="server" />
                                    <ext:Hidden ID="txtIDZiffAcnt" runat="server" />
                                </Items>
                            </ext:Panel>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSave" runat="server" Text="Save" Icon="DatabaseSave" Disabled="true" FormBind="true">
                                <DirectEvents>
                                    <Click OnEvent="btnSavePeer_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnCancel" runat="server" Icon="Decline" Text="Close">
                                <DirectEvents>
                                    <Click OnEvent="btnCancelPeer_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                        <BottomBar>
                            <ext:StatusBar ID="FormStatusBar" runat="server" />
                        </BottomBar>
                        <Listeners>
                            <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                        text : valid ? 'Form is valid' : 'Form is invalid', 
                                                        iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                    });
                                                    #{btnSave}.setDisabled(!valid);" />
                        </Listeners>
                    </ext:FormPanel>
                </Items>
            </ext:Window>
            <ext:Window ID="winImport" runat="server" Title="Import Base Costs" Hidden="true" Icon="ArrowLeft" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                <Items>
                    <ext:FormPanel ID="frmImportFile" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                        <Items>
                            <ext:Panel ID="pnlImportFile1" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                <Defaults>
                                    <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                    <ext:Parameter Name="MsgTarget" Value="side" />
                                </Defaults>
                                <Items>
                                    <ext:FileUploadField ID="fileImport" FieldLabel="File" runat="server" Icon="Attach" Cls="PopupFormField" />
                                </Items>
                            </ext:Panel>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnImportFile" runat="server" Text="Import" Icon="TableRefresh" Disabled="true" FormBind="true">
                                <DirectEvents>
                                    <Click OnEvent="btnImportFile_Click" >
                                        <EventMask ShowMask="true" Target="CustomTarget" CustomTarget="vpMain" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnCancelImport" runat="server" Icon="Decline" Text="Close">
                                <DirectEvents>
                                    <Click OnEvent="btnCancelImport_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                        <BottomBar>
                            <ext:StatusBar ID="FormImportStatusBar" runat="server" />
                        </BottomBar>
                        <Listeners>
                            <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                        text : valid ? 'Form is valid' : 'Form is invalid', 
                                                        iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                    });
                                                    #{btnImportFile}.setDisabled(!valid);" />
                        </Listeners>
                    </ext:FormPanel>
                </Items>
            </ext:Window>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>

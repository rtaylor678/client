﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _allocationlistdrivers : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdAllocationListDriver { get { return (Int32)Session["IdAllocationListDriver"]; } set { Session["IdAllocationListDriver"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private DataReportObject ExportAllocationListDrivers { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                this.LoadLabel();
                this.storeTypeUnit.Reload();
                IdAllocationListDriver = 0;
                this.LoadAllocationListDriver();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winAllocationListDriverEdit.Show();
        }

        protected void btnDeleteAll_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete all records?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDeleteAllYES()",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.Save();
                this.LoadAllocationListDriver();
            }
            catch (Exception ex)
            {
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving") + " " + ex.Message, IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            winAllocationListDriverEdit.Hide();
            //this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = "Form is valid", IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.AllocationListDriver);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Driver Desc"].ToString(), row["Unit Desc"].ToString(), row["Unit Cost Denominator"].ToString().ToUpper() == "YES" ? true : false);
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadAllocationListDriver();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        protected void Clear()
        {
            this.IdAllocationListDriver = 0;
            this.txtName.Text = "";
            this.txtName.Reset();
            this.ddlTypeUnit.Reset();
            this.ddlUnit.Reset();
            this.ddlUnit.ReadOnly = true;
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void grdAllocationListDriver_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            IdAllocationListDriver = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            if (Command == "Edit")
            {
                this.winAllocationListDriverEdit.Show();
                this.EditAllocationListDriverLoad();
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + _Name + "?", new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedDeleteYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
        }

        protected void Save()
        {
            DataClass.clsAllocationListDriver objAllocationListDriver = new DataClass.clsAllocationListDriver();
            objAllocationListDriver.IdAllocationListDriver = (Int32)IdAllocationListDriver;
            objAllocationListDriver.loadObject();

            if (IdAllocationListDriver == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (objAllocationListDriver.Name.ToString().ToUpper() != txtName.Text.ToString().ToUpper())
                {
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }
            objAllocationListDriver.Name = txtName.Text;
            objAllocationListDriver.IdModel = IdModel;
            objAllocationListDriver.IdUnit = Convert.ToInt32(this.ddlUnit.Value);
            if (chkUnitCostDenominator.Checked)
                objAllocationListDriver.UnitCostDenominator = true;
            else
                objAllocationListDriver.UnitCostDenominator = false;
            if (IdAllocationListDriver == 0)
            {
                objAllocationListDriver.DateCreation = DateTime.Now;
                objAllocationListDriver.UserCreation = (Int32)IdUserCreate;
                IdAllocationListDriver = objAllocationListDriver.Insert();
            }
            else
            {
                objAllocationListDriver.DateModification = DateTime.Now;
                objAllocationListDriver.UserModification = (Int32)IdUserCreate;
                objAllocationListDriver.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winAllocationListDriverEdit.Hide();
            this.ModelUpdateTimestamp();
        }

        protected Int32 LoadIdUnit(String _Unit)
        {
            Int32 IdUnit = 0;
            DataClass.clsUnit objUnit = new DataClass.clsUnit();
            DataTable dt = objUnit.LoadList("Name LIKE '%" + ddlUnit.Text + "%'", "ORDER BY Name");

            if (dt.Rows.Count > 0)
            {
                IdUnit = (Int32)dt.Rows[0]["IdUnit"];
            }
            return IdUnit;
        }

        protected void SaveImport(String _Name, String _Unit, Boolean _UnitCostDenominator)
        {
            Int32 _IdUnit = GetIdUnit(_Unit);

            if (!String.IsNullOrEmpty(_Name) && _IdUnit != 0)
            {
                DataClass.clsAllocationListDriver objAllocationListDriver = new DataClass.clsAllocationListDriver();
                objAllocationListDriver.IdAllocationListDriver = (Int32)IdAllocationListDriver;
                objAllocationListDriver.loadObject();
                objAllocationListDriver.Name = _Name;
                objAllocationListDriver.IdModel = IdModel;
                objAllocationListDriver.DateCreation = DateTime.Now;
                objAllocationListDriver.UserCreation = (Int32)IdUserCreate;
                objAllocationListDriver.IdUnit = _IdUnit;
                objAllocationListDriver.UnitCostDenominator = _UnitCostDenominator;
                IdAllocationListDriver = objAllocationListDriver.Insert();
                objAllocationListDriver = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected Int32 GetIdUnit(String _Unit)
        {
            Int32 Value = 0;
            DataClass.clsUnit objUnit = new DataClass.clsUnit();
            DataTable dt = objUnit.LoadList("Name = '" + _Unit + "'", "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdUnit"];
            }
            return Value;
        }

        protected String GetUnit(Int32 _IdUnit)
        {
            String Value = "";
            DataClass.clsUnit objUnit = new DataClass.clsUnit();
            DataTable dt = objUnit.LoadComboBox("IdUnit = " + _IdUnit, "");

            if (dt.Rows.Count > 0)
            {
                Value = (String)dt.Rows[0]["Name"];
            }
            return Value;
        }

        protected Int32 GetIdTypeUnit(String _TypeUnit)
        {
            Int32 Value = 0;
            DataClass.clsTypeUnit objUnit = new DataClass.clsTypeUnit();
            DataTable dt = objUnit.LoadComboBox("Name = '" + _TypeUnit + "'", "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["ID"];
            }
            return Value;
        }

        protected void EditAllocationListDriverLoad()
        {
            DataClass.clsAllocationListDriver objAllocationListDriver = new DataClass.clsAllocationListDriver();
            DataTable dt = objAllocationListDriver.LoadList("IdAllocationListDriver = " + IdAllocationListDriver, "");
            if (dt.Rows.Count > 0)
            {
                this.txtName.Text = (String)dt.Rows[0]["Name"];
                this.ddlTypeUnit.Value = Convert.ToInt32(dt.Rows[0]["IdTypeUnit"]);
                this.ddlUnit.Value = Convert.ToInt32(dt.Rows[0]["IdUnit"]);
                this.storeUnit.Reload();
                this.ddlUnit.ReadOnly = false;
                this.chkUnitCostDenominator.Checked = Convert.ToBoolean(dt.Rows[0]["UnitCostDenominator"]);
            }
        }

        protected void StoreTypeUnit_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsTypeUnit objTypeUnit = new DataClass.clsTypeUnit();
            this.storeTypeUnit.DataSource = objTypeUnit.LoadList("", "ORDER BY Name");
            this.storeTypeUnit.DataBind();
        }

        protected void StoreUnit_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsUnit objUnit = new DataClass.clsUnit();
            if (ddlTypeUnit.Value == null)
            {
                this.storeUnit.DataSource = objUnit.LoadList("Name LIKE '%" + ddlUnit.Text + "%'", "ORDER BY Name");
            }
            else
            {
                this.storeUnit.DataSource = objUnit.LoadList("IdTypeUnit = " + ddlTypeUnit.Value, "ORDER BY Name");
            }
            this.storeUnit.DataBind();
        }

        protected void ddlTypeUnit_Select(object sender, DirectEventArgs e)
        {
            if (ddlTypeUnit.Value != null && ddlTypeUnit.Value.ToString() != "")
            {
                this.ddlUnit.ReadOnly = false;
            }
            else
            {
                this.ddlUnit.ReadOnly = true;
            }
            this.ddlUnit.Text = null;
            this.storeUnit.Reload();
        }

        public void Delete()
        {
            DataClass.clsAllocationListDriver objAllocationListDriver = new DataClass.clsAllocationListDriver();
            objAllocationListDriver.IdAllocationListDriver = (Int32)IdAllocationListDriver;
            objAllocationListDriver.Delete();
            this.ModelUpdateTimestamp();
        }

        public void DeleteAll()
        {
            DataClass.clsAllocationListDriver objAllocationListDriver = new DataClass.clsAllocationListDriver();
            objAllocationListDriver.IdModel = (Int32)IdModel;
            objAllocationListDriver.DeleteAll();
            this.ModelUpdateTimestamp();
        }

        [DirectMethod]
        public void ClickedDeleteYES()
        {
            this.Delete();
            this.LoadAllocationListDriver();
        }

        [DirectMethod]
        public void ClickedDeleteAllYES()
        {
            this.DeleteAll();
            this.LoadAllocationListDriver();
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsAllocationListDriver objAllocationListDriver = new DataClass.clsAllocationListDriver();
            DataTable dt = objAllocationListDriver.LoadList("IdModel = " + IdModel + " AND Name = '" + txtName.Text + "'", "");
            Value = dt.Rows.Count;

            return Value;
        }

        protected void LoadAllocationListDriver()
        {
            DataClass.clsAllocationListDriver objAllocationListDriver = new DataClass.clsAllocationListDriver();

            DataTable dt = objAllocationListDriver.LoadList("IdModel = " + IdModel, "ORDER BY Name");

            storeAllocationListDriver.DataSource = dt;
            storeAllocationListDriver.DataBind();

            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Name", "Driver Desc"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Unit", "Unit Desc"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("UnitCostDenominator", "Unit Cost Denominator"));
            String strSelectedParameters = "";
            String[] columnsToCopy = { "Name", "Unit", "UnitCostDenominator" };
            System.Data.DataView view = new System.Data.DataView(dt);
            DataTable dtAllocationListDrivers = view.ToTable(true, columnsToCopy);
            ExportAllocationListDrivers = new DataReportObject(dtAllocationListDrivers, "DATASHEET", null, strSelectedParameters, null, "Name", datColumnTD, null);
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportAllocationListDrivers;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=2.3.1 ExportListOfAllocationDrivers.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportListOfAllocationDrivers.xls", "template");
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        protected void LoadLabel()
        {
            this.grdListDrivers.Title = modMain.strMasterListofAllocationDrivers;
            this.btnAdd.Text = (String)GetGlobalResourceObject("CPM_Resources", "Add New Driver");
            this.btnAdd.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Add New Driver");
            this.btnDelete.Text = modMain.strCommonDeleteAll;
            this.btnDelete.ToolTip = modMain.strCommonDeleteAll;
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.Name.Text = modMain.strCommonName;
            this.colTypeUnit.Text = (String)GetGlobalResourceObject("CPM_Resources", "Unit Type");
            this.colUnit.Text = (String)GetGlobalResourceObject("CPM_Resources", "Unit");
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = "&nbsp;" + modMain.strCommonEdit;
            this.ImageCommandColumn1.Commands[1].Text = "&nbsp;" + modMain.strCommonDelete;
            this.winAllocationListDriverEdit.Title = (String)GetGlobalResourceObject("CPM_Resources", "Allocation Driver");
            this.txtName.FieldLabel = modMain.strCommonName;
            this.ddlTypeUnit.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Unit Type");
            this.ddlUnit.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Unit");
            this.chkUnitCostDenominator.FieldLabel= (String)GetGlobalResourceObject("CPM_Resources", "Unit Cost Denominator");
            this.btnSave.Text = modMain.strCommonSave;
            this.btnSave.ToolTip = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.btnCancel.ToolTip = modMain.strCommonClose;
            this.winImport.Title = modMain.strCommonImport + " " + (String)GetGlobalResourceObject("CPM_Resources", "Allocation Drivers");
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnImportFile.ToolTip = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.btnCancelImport.ToolTip = modMain.strCommonClose;
            this.UnitCostDenominator.Text = (String)GetGlobalResourceObject("CPM_Resources", "Unit Cost Denominator");
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        #endregion

    }
}
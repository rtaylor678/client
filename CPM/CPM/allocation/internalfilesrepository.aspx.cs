﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using System.Xml.Xsl;
using CD;
using DataClass;

namespace CPM
{
    public partial class _internalfilesrepository : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdZiffInternalAnalysis { get { return (Int32)Session["IdZiffInternalAnalysis"]; } set { Session["IdZiffInternalAnalysis"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        public DataTable datViewData { get; set; }
        public static Object fileObj;
        protected static byte[] Filebyte;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                IdZiffInternalAnalysis = 0;
                this.LoadZiffInternalAnalysis();
                this.LoadLabel();
            }
        }
    
        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.IdZiffInternalAnalysis = 0;
                this.ddlZiffAccount.Value = null;
                this.txtFileName.Text = "";
                Filebyte = new Byte[0];
                this.winInternalFilesRepositoryEdit.Show();                
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = ex.Message, IconCls = "icon-exclamation", Clear2 = false });
            }
        }
        
        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                Save();
                this.LoadZiffInternalAnalysis();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.StatusBar1.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            this.winInternalFilesRepositoryEdit.Hide();
        }

        #endregion

        #region Methods

        protected void Save()
        {
            DataClass.clsZiffTechnicalAnalysis objZiffInternalAnalysis = new DataClass.clsZiffTechnicalAnalysis();
            objZiffInternalAnalysis.IdZiffInternalAnalysis = Convert.ToInt32(IdZiffInternalAnalysis);
            objZiffInternalAnalysis.loadObjectInternal();

            this.fileImport = (FileUploadField)fileObj;
            if (IdZiffInternalAnalysis == 0 || fileObj != null)
            {
                if (this.fileImport.HasFile)
                {
                    this.fileImport = (FileUploadField)fileObj;
                    BufferedStream reader = new BufferedStream(fileImport.PostedFile.InputStream);
                    Filebyte = new byte[fileImport.PostedFile.InputStream.Length];
                    reader.Read(Filebyte, 0, Filebyte.Length);
                }
                else if (Filebyte.Length == 0 || txtFileName.Text.Trim() == "")
                {
                    this.StatusBar1.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Upload file failed"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                Filebyte = objZiffInternalAnalysis.AttachFile;
            }
            objZiffInternalAnalysis.IdZiffAccount = Convert.ToInt32(ddlZiffAccount.Value);
            objZiffInternalAnalysis.IdModel = IdModel;
            objZiffInternalAnalysis.AttachFile = Filebyte;
            objZiffInternalAnalysis.FileName = txtFileName.Text;
            if (objZiffInternalAnalysis.IdZiffInternalAnalysis == 0)
            {
                objZiffInternalAnalysis.DateCreation = DateTime.Now;
                objZiffInternalAnalysis.UserCreation = (Int32)IdUserCreate;
                IdZiffInternalAnalysis = objZiffInternalAnalysis.InsertInternal();
            }
            else
            {
                objZiffInternalAnalysis.DateModification = DateTime.Now;
                objZiffInternalAnalysis.UserModification = (Int32)IdUserCreate;
                objZiffInternalAnalysis.UpdateInternal();
            }
            this.StatusBar1.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Record saved successfully!"), IconCls = "icon-accept", Clear2 = false });
            this.winInternalFilesRepositoryEdit.Hide();
        }

        protected void EditZiffInternalAnalysisLoad()
        {
            DataClass.clsZiffTechnicalAnalysis objZiffInternalAnalysis = new DataClass.clsZiffTechnicalAnalysis();
            DataTable dt = objZiffInternalAnalysis.LoadListInternal("IdZiffInternalAnalysis = " + IdZiffInternalAnalysis, "");
            if (dt.Rows.Count > 0)
            {
                this.ddlZiffAccount.Value = (Int32)dt.Rows[0]["IdZiffAccount"];
                this.txtFileName.Text = (String)dt.Rows[0]["FileName"];
                this.fileImport.Text = (String)dt.Rows[0]["FileName"];
                Filebyte = (Byte[])dt.Rows[0]["AttachFile"];

            }
        }

        protected void fileImport_Change(object sender, DirectEventArgs e)
        {
            if (this.fileImport.HasFile)
            {
                fileObj = fileImport;
                txtFileName.Text = fileImport.FileName;
            }
        }

        protected void grdInternalFilesRepository_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            IdZiffInternalAnalysis = Convert.ToInt32(e.ExtraParams["Id"].ToString());
            switch (Command)
            {
                case "Edit":
                    fileObj = null;
                    this.EditZiffInternalAnalysisLoad();
                    this.winInternalFilesRepositoryEdit.Show();

                    break;
                case "Delete":
                    X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + IdZiffInternalAnalysis + "?", new Ext.Net.MessageBoxButtonsConfig
                    {
                        Yes = new MessageBoxButtonConfig
                        {
                            Handler = "UsersX.ClickedDeleteYES()",
                            Text = modMain.strCommonYes
                        },
                        No = new MessageBoxButtonConfig
                        {
                            Text = modMain.strCommonNo
                        }
                    }).Show();
                    break;
                default:
                    Download();
                    break;
            }
        }

        protected void StoreZiffAccount_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsZiffAccount objZiffAccount = new DataClass.clsZiffAccount();
            DataTable dtZiffAccount = objZiffAccount.LoadComboBox("IdModel = " + IdModel + " AND Parent IS NULL", " ORDER BY Name");
            DataRow drAccount = dtZiffAccount.NewRow();
            drAccount["IdZiffAccount"] = 0;
            drAccount["Name"] = "- Any -";
            dtZiffAccount.Rows.Add(drAccount);
            dtZiffAccount.AcceptChanges();
            this.storeZiffAccount.DataSource = dtZiffAccount;
            this.storeZiffAccount.DataBind();
        }

        public void DeleteInternal()
        {
            DataClass.clsZiffTechnicalAnalysis objZiffInternalAnalysis = new DataClass.clsZiffTechnicalAnalysis();

            objZiffInternalAnalysis.IdZiffInternalAnalysis = (Int32)IdZiffInternalAnalysis;
            objZiffInternalAnalysis.DeleteInternal();
        }

        [DirectMethod]
        public void ClickedDeleteYES()
        {
            this.DeleteInternal();
            this.LoadZiffInternalAnalysis();
        }

        protected void LoadZiffInternalAnalysis()
        {
            DataClass.clsZiffTechnicalAnalysis objZiffInternalAnalysis= new DataClass.clsZiffTechnicalAnalysis();
            DataTable dt = objZiffInternalAnalysis.LoadListInternal("IdModel = " + IdModel,"");
            storeInternalFilesRepository.DataSource = dt;
            storeInternalFilesRepository.DataBind();
        }

        protected void Download()
        {
            DataClass.clsZiffTechnicalAnalysis objZiffInternalAnalysis = new clsZiffTechnicalAnalysis();
            objZiffInternalAnalysis.IdZiffInternalAnalysis = IdZiffInternalAnalysis;
            objZiffInternalAnalysis.loadObjectInternal();
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, objZiffInternalAnalysis.FileName, objZiffInternalAnalysis.AttachFile);
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = (String)GetGlobalResourceObject("CPM_Resources", "Internal Benchmarks Files Repository");
            this.btnNew.Text = (String)GetGlobalResourceObject("CPM_Resources", "Add New File");
            this.btnNew.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "New Analysis");
            this.FileName.Text = (String)GetGlobalResourceObject("CPM_Resources", "Filename");
            this.cZiffAccount.Text = (String)GetGlobalResourceObject("CPM_Resources", "ZiffAccount");
            this.Column1.Text = (String)GetGlobalResourceObject("CPM_Resources", "Ziff Account Code");
            this.Column2.Text = (String)GetGlobalResourceObject("CPM_Resources", "Ziff Account Name");
            this.ImageCommandColumn2.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn2.Commands[0].Text = "&nbsp;" + modMain.strCommonEdit;
            this.ImageCommandColumn2.Commands[1].Text = "&nbsp;" + modMain.strCommonDelete;
            this.ImageCommandColumn2.Commands[2].Text = "&nbsp;" + (String)GetGlobalResourceObject("CPM_Resources", "Download");
            this.winInternalFilesRepositoryEdit.Title = (String)GetGlobalResourceObject("CPM_Resources", "Internal Benchmarks Files Repository");
            this.ddlZiffAccount.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "ZiffAccount");
            this.txtFileName.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "NewFile");
            this.fileImport.ButtonText = (String)GetGlobalResourceObject("CPM_Resources", "AttachNewFile");
            this.btnSave.Text = modMain.strCommonSave;
            this.btnSave.ToolTip = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.btnCancel.ToolTip = modMain.strCommonClose;
        }

        #endregion

    }
}
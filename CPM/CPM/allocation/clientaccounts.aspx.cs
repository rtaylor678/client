﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _clientaccounts : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdClientAccount { get { return (Int32)Session["IdClientAccount"]; } set { Session["IdClientAccount"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private DataReportObject ExportClientAccounts { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                IdClientAccount = 0;
                this.LoadLabel();
                this.LoadClientAccounts();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winClientAccountsEdit.Show();
        }

        protected void btnDeleteAll_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete all records?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDeleteAllYES()",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.Save();
                this.LoadClientAccounts();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            winClientAccountsEdit.Hide();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.CostAccounts);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Account Code"].ToString(), row["Account Desc"].ToString(), row["Resource Code"].ToString());
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadClientAccounts();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        protected void Clear()
        {
            this.IdClientAccount = 0;
            this.txtName.Text = "";
            this.txtName.Reset();
            this.txtAccount.Text = "";
            this.txtAccount.Reset();
            this.ddlResource.Reset();
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void grdClientAccounts_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            IdClientAccount = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            if (Command == "Edit")
            {
                this.winClientAccountsEdit.Show();
                this.EditClientAccountsLoad();
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + _Name + "?", new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedDeleteYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }      
        }

        protected void Save()
        {
            DataClass.clsClientAccounts objClientAccounts = new DataClass.clsClientAccounts();
            objClientAccounts.IdClientAccount = (Int32)IdClientAccount;
            objClientAccounts.loadObject();

            if (IdClientAccount == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (objClientAccounts.Account.ToString().ToUpper() != txtAccount.Text.ToString().ToUpper())
                {
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }

            objClientAccounts.Account = txtAccount.Text;
            objClientAccounts.Name = txtName.Text;
            objClientAccounts.IdModel = IdModel;

            Int32 intResource = Convert.ToInt32(this.ddlResource.Value);
            if (intResource==0)
            {
                objClientAccounts.IdResource = null;
            }
            else
            {
                objClientAccounts.IdResource = intResource;
            }
            if (IdClientAccount == 0)
            {
                objClientAccounts.DateCreation = DateTime.Now;
                objClientAccounts.UserCreation = (Int32)IdUserCreate;
                IdClientAccount = objClientAccounts.Insert();
            }
            else
            {
                objClientAccounts.DateModification = DateTime.Now;
                objClientAccounts.UserModification = (Int32)IdUserCreate;
                objClientAccounts.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winClientAccountsEdit.Hide();
            this.ModelUpdateTimestamp();
        }

        protected Int32 LoadIdResource(String _Resource)
        {
            Int32 IdOperation = 0;
            DataClass.clsResources objResources = new DataClass.clsResources();
            DataTable dt = objResources.LoadList("IdModel = " + IdModel + " AND Code = '" + _Resource + "'", "ORDER BY Name");    

            if (dt.Rows.Count>0)
            {
                IdOperation = (Int32)dt.Rows[0]["IdResources"];
            }
            return IdOperation;
        }

        protected void SaveImport(String _Account, String _Name, String _Resource)
        {
            Int32 _IdResource = GetIdResource(_Resource);

            if (!String.IsNullOrEmpty(_Account) && !String.IsNullOrEmpty(_Name) && _IdResource != 0)
            {
                DataClass.clsClientAccounts objClientAccounts = new DataClass.clsClientAccounts();
                objClientAccounts.IdClientAccount = (Int32)IdClientAccount;
                objClientAccounts.loadObject();
                objClientAccounts.Account = _Account;
                objClientAccounts.Name = _Name;
                objClientAccounts.IdModel = IdModel;
                objClientAccounts.DateCreation = DateTime.Now;
                objClientAccounts.UserCreation = (Int32)IdUserCreate;
                objClientAccounts.IdResource = _IdResource;
                IdClientAccount = objClientAccounts.Insert();
                objClientAccounts = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected Int32 GetIdResource(String _Resource)
        {
            Int32 IdOperation = 0;
            DataClass.clsResources objResources = new DataClass.clsResources();
            DataTable dt = objResources.LoadList("IdModel = " + IdModel + " AND Code = '" + _Resource + "'", "ORDER BY Name");

            if (dt.Rows.Count > 0)
            {
                IdOperation = (Int32)dt.Rows[0]["IdResources"];
            }
            return IdOperation;
        }

        protected void EditClientAccountsLoad()
        {
            DataClass.clsClientAccounts objClientAccounts = new DataClass.clsClientAccounts();
            DataTable dt = objClientAccounts.LoadList("IdClientAccount = " + IdClientAccount, "");
            if (dt.Rows.Count > 0)
            {
                this.txtAccount.Text = (String)dt.Rows[0]["Account"];
                this.txtName.Text = (String)dt.Rows[0]["Name"];
                this.ddlResource.Value = Convert.ToInt32(dt.Rows[0]["IdResource"]);
            }
        }

        protected void StoreResource_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsResources objResources = new DataClass.clsResources();
            this.storeResource.DataSource = objResources.LoadList("IdModel = " + IdModel, "ORDER BY Name");
            this.storeResource.DataBind();
        }

        public void Delete()
        {
            DataClass.clsClientAccounts objClientAccounts = new DataClass.clsClientAccounts();
            objClientAccounts.IdClientAccount = (Int32)IdClientAccount;
            objClientAccounts.Delete();
            this.ModelUpdateTimestamp();
        }

        public void DeleteAll()
        {
            DataClass.clsClientAccounts objClientAccounts = new DataClass.clsClientAccounts();
            objClientAccounts.IdModel = (Int32)IdModel;
            objClientAccounts.DeleteAll();
            this.ModelUpdateTimestamp();
        }

        [DirectMethod]
        public void ClickedDeleteYES()
        {
            this.Delete();
            this.LoadClientAccounts();
        }

        [DirectMethod]
        public void ClickedDeleteAllYES()
        {
            this.DeleteAll();
            this.LoadClientAccounts();
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsClientAccounts objClientAccounts = new DataClass.clsClientAccounts();
            DataTable dt = objClientAccounts.LoadList("IdModel = " + IdModel + " AND Account = '" + txtAccount.Text + "'", "");
            Value = dt.Rows.Count;

            return Value;
        }

        protected void LoadClientAccounts()
        {
            DataClass.clsClientAccounts objClientAccounts = new DataClass.clsClientAccounts();

            DataTable dt = objClientAccounts.LoadList("IdModel = " + IdModel, "ORDER BY Name");

            storeClientAccounts.DataSource = dt;
            storeClientAccounts.DataBind();

            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Name", "Account Desc"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Account", "Account Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("ResourceCode", "Resource Code"));
            String strSelectedParameters = "";
            String[] columnsToCopy = { "Account", "Name", "ResourceCode" };
            System.Data.DataView view = new System.Data.DataView(dt);
            DataTable dtClientAccounts = view.ToTable(true, columnsToCopy);
            ExportClientAccounts = new DataReportObject(dtClientAccounts, "DATASHEET", null, strSelectedParameters, null, "Name", datColumnTD, null);
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportClientAccounts;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=1.7 ExportChartOfAccounts.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportChartOfAccounts.xls", "template");
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, true, false, false);
            objModelUpdate = null;
        }

        protected void LoadLabel()
        {
            this.grdClientAccounts.Title = modMain.strMasterChartAccounts;
            this.btnAdd.Text = (String)GetGlobalResourceObject("CPM_Resources", "Add New Account");
            this.btnAdd.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Add New Account");
            this.btnDelete.Text = modMain.strCommonDeleteAll;
            this.btnDelete.ToolTip = modMain.strCommonDeleteAll;
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.Account.Text = (String)GetGlobalResourceObject("CPM_Resources", "Account");
            this.Name.Text = modMain.strCommonName;
            this.ResourceName.Text=(String)GetGlobalResourceObject("CPM_Resources", "Resource");
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = "&nbsp;" + modMain.strCommonEdit;
            this.ImageCommandColumn1.Commands[1].Text = "&nbsp;" + modMain.strCommonDelete;
            this.winClientAccountsEdit.Title=(String)GetGlobalResourceObject("CPM_Resources", "Account");
            this.txtAccount.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Account");
            this.txtName.FieldLabel = modMain.strCommonName;
            this.ddlResource.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Resource");
            this.btnSave.Text = modMain.strCommonSave;
            this.btnSave.ToolTip = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.btnCancel.ToolTip = modMain.strCommonClose;
            this.winImport.Title = modMain.strCommonImport + " " + modMain.strMasterChartAccounts;
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnImportFile.ToolTip = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.btnCancelImport.ToolTip = modMain.strCommonClose;
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        #endregion

    }
}
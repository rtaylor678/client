﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _sharedcostallocation : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdSharedCostAllocation { get { return (Int32)Session["IdSharedCostAllocation"]; } set { Session["IdSharedCostAllocation"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        public Boolean SkipReload { get; set; }
        private DataReportObject ExportSharedCostAllocation { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                clsModels objModels = new clsModels();
                this.colCost.Text = (String)GetGlobalResourceObject("CPM_Resources", "Cost") + " " + objModels.GetDefaultCurrencySymbol(IdModel);
                IdSharedCostAllocation = 0;
                this.LoadSharedCostAllocation();
                this.LoadLabel();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winSharedCostAllocationEdit.Show();
            this.storeField.Reload();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            Boolean blnHasFieldSelected = false;
            try
            {
                String rowsValues = e.ExtraParams["rowsValues"];

                //Make sure at least 1 field is selected
                DataTable dtCheck = JSON.Deserialize<DataTable>(rowsValues);
                foreach (DataRow objRow in dtCheck.Rows)
                {
                    if ((Boolean)objRow["Selected"] == true)
                    {
                        blnHasFieldSelected = true;
                        break;
                    }
                }
                dtCheck = null;

                if (!blnHasFieldSelected)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "You must select at least one field from the list."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
                else
                {
                    DataClass.clsSharedCostAllocation objSharedCostAllocation = new DataClass.clsSharedCostAllocation();
                    objSharedCostAllocation.IdSharedCostAllocation = (Int32)IdSharedCostAllocation;
                    objSharedCostAllocation.loadObject();
                    if (IdSharedCostAllocation == 0)
                    {
                        if (this.Valid() > 0)
                        {
                            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                            return;
                        }
                    }
                    else
                    {
                        if (objSharedCostAllocation.IdClientCostCenter != Convert.ToInt32(this.ddlCostCenter.Value))
                        {
                            if (this.Valid() > 0)
                            {
                                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                                return;
                            }
                        }
                    }
                    this.Save();

                    this.SaveFields(rowsValues);
                    this.storeField.Reload();
                    this.LoadSharedCostAllocation();
                }
            }
            catch (Exception ex)
            {
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving") + " " + ex.Message, IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            winSharedCostAllocationEdit.Hide();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.SharedCostAllocation);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Cost Object Code"].ToString(), row["Field Code"].ToString());
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadSharedCostAllocation();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        protected void Clear()
        {
            this.IdSharedCostAllocation = 0;
            this.ddlCostCenter.Reset();
            this.storeCostCenter.Reload();
            this.storeField.RemoveAll();
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void grdSharedCostAllocation_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            IdSharedCostAllocation = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            if (Command == "Edit")
            {
                SkipReload = true;
                this.winSharedCostAllocationEdit.Show();
                this.EditSharedLoad();
                SkipReload = false;
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + _Name + "?", new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedDeleteYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
        }

        protected void Save()
        {
            DataClass.clsSharedCostAllocation objSharedCostAllocation = new DataClass.clsSharedCostAllocation();
            objSharedCostAllocation.IdSharedCostAllocation = (Int32)IdSharedCostAllocation;
            objSharedCostAllocation.loadObject();

            if (IdSharedCostAllocation == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            objSharedCostAllocation.IdModel = IdModel;
            objSharedCostAllocation.IdClientCostCenter = Convert.ToInt32(this.ddlCostCenter.Value);
            if (IdSharedCostAllocation == 0)
            {
                objSharedCostAllocation.DateCreation = DateTime.Now;
                objSharedCostAllocation.UserCreation = (Int32)IdUserCreate;
                IdSharedCostAllocation = objSharedCostAllocation.Insert();
            }
            else
            {
                objSharedCostAllocation.DateModification = DateTime.Now;
                objSharedCostAllocation.UserModification = (Int32)IdUserCreate;
                objSharedCostAllocation.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winSharedCostAllocationEdit.Hide();
            this.ModelUpdateTimestamp();
        }

        protected void SaveFields(String strValue)
        {
            DataTable dt = JSON.Deserialize<DataTable>(strValue);
            foreach (DataRow row in dt.Rows)
            {
                DataClass.clsSharedCostField objSharedCostField = new DataClass.clsSharedCostField();

                objSharedCostField.IdSharedCostField = Convert.ToInt32(row["IdSharedCostField"]);
                objSharedCostField.loadObject();

                objSharedCostField.IdSharedCostAllocation = IdSharedCostAllocation;
                objSharedCostField.IdField = Convert.ToInt32(row["IdField"]);

                if (Convert.ToInt32(row["IdSharedCostField"]) == 0)
                {
                    if ((Boolean)row["Selected"] == true)
                    {
                        objSharedCostField.Insert();
                    }
                }
                else
                {
                    if ((Boolean)row["Selected"] == false)
                    {
                        objSharedCostField.Delete();
                    }
                    else
                    {
                        objSharedCostField.Update();
                    }
                }
            }
            this.ModelUpdateTimestamp();
        }

        protected Int32 LoadIdCostCenter(String _Value)
        {
            Int32 IdClientCostCenter = 0;
            DataClass.clsClientCostCenter objClientCostCenter = new DataClass.clsClientCostCenter();
            DataTable dt = objClientCostCenter.LoadList("IdModel = " + IdModel + " AND Name LIKE '%" + _Value + "%'", "ORDER BY Name");

            if (dt.Rows.Count > 0)
            {
                IdClientCostCenter = (Int32)dt.Rows[0]["IdClientCostCenter"];
            }
            return IdClientCostCenter;
        }

        protected Int32 LoadIdActivity(String _Value)
        {
            Int32 IdAccount = 0;
            DataClass.clsActivities objActivities = new DataClass.clsActivities();
            DataTable dt = objActivities.LoadList("IdModel = " + IdModel + " AND Name LIKE '%" + _Value + "%'", "ORDER BY Name");

            if (dt.Rows.Count > 0)
            {
                IdAccount = (Int32)dt.Rows[0]["IdActivity"];
            }
            return IdAccount;
        }

        protected Int32 LoadIdField(String _Value)
        {
            Int32 IdAccount = 0;
            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dt = objFields.LoadList("IdModel = " + IdModel + " AND Name LIKE '%" + _Value + "%'", "ORDER BY Name");

            if (dt.Rows.Count > 0)
            {
                IdAccount = (Int32)dt.Rows[0]["IdField"];
            }
            return IdAccount;
        }

        protected void SaveImport(String _CostCenter, String _Field)
        {
            Int32 _IdCostCenter = GetIdCostCenter(_CostCenter);
            Int32 _IdField = GetIdField(_Field);

            if (_IdCostCenter != 0 && _IdField != 0)
            {
                DataClass.clsSharedCostAllocation objSharedCostAllocation = new DataClass.clsSharedCostAllocation();
                DataTable dtAux = objSharedCostAllocation.LoadList("IdModel = " + IdModel + " And IdClientCostCenter = " + _IdCostCenter, "");
                if (dtAux.Rows.Count > 0)
                {
                    IdSharedCostAllocation = Convert.ToInt32(dtAux.Rows[0]["IdSharedCostAllocation"]);
                }
                else
                {
                    IdSharedCostAllocation = 0;
                    objSharedCostAllocation.IdSharedCostAllocation = (Int32)IdSharedCostAllocation;
                    objSharedCostAllocation.loadObject();
                    objSharedCostAllocation.IdModel = IdModel;
                    objSharedCostAllocation.IdClientCostCenter = _IdCostCenter;
                    objSharedCostAllocation.DateCreation = DateTime.Now;
                    objSharedCostAllocation.UserCreation = (Int32)IdUserCreate;
                    IdSharedCostAllocation = objSharedCostAllocation.Insert();
                }
                dtAux.Dispose();
                dtAux = null;
                objSharedCostAllocation = null;
                DataClass.clsSharedCostField objSharedCostField = new DataClass.clsSharedCostField();
                DataTable dtAux2 = objSharedCostField.LoadList("IdSharedCostAllocation = " + IdSharedCostAllocation + " And IdField = " + _IdField, "");
                if (dtAux2.Rows.Count == 0)
                {
                    objSharedCostField.IdSharedCostField = 0;
                    objSharedCostField.loadObject();
                    objSharedCostField.IdSharedCostAllocation = IdSharedCostAllocation;
                    objSharedCostField.IdField = _IdField;
                    Int32 _IdSharedCostField = objSharedCostField.Insert();
                }
                dtAux2.Dispose();
                dtAux2 = null;
                objSharedCostField = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected Int32 GetIdCostCenter(String _CostCenter)
        {
            Int32 Value = 0;
            DataClass.clsClientCostCenter objClientCostCenter = new DataClass.clsClientCostCenter();
            DataTable dt = objClientCostCenter.LoadList("Code = '" + _CostCenter + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdClientCostCenter"];
            }
            return Value;
        }

        protected Int32 GetIdField(String _Field)
        {
            Int32 Value = 0;
            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dt = objFields.LoadList("Code = '" + _Field + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdField"];
            }
            return Value;
        }

        protected void EditSharedLoad()
        {
            DataClass.clsSharedCostAllocation objSharedCostAllocation = new DataClass.clsSharedCostAllocation();
            DataTable dt = objSharedCostAllocation.LoadList("IdSharedCostAllocation = " + IdSharedCostAllocation, "");
            if (dt.Rows.Count > 0)
            {
                this.ddlCostCenter.Value = Convert.ToInt32(dt.Rows[0]["IdClientCostCenter"]);
                this.storeCostCenter.Reload();
                this.storeField.Reload();
            }
        }

        protected void ddlCostCenter_Select(object sender, DirectEventArgs e)
        {
            if (!SkipReload)
            {
                this.storeField.Reload();
            }
        }

        protected void StoreCostCenter_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsClientCostCenter objClientCostCenter = new DataClass.clsClientCostCenter();
            if (IdSharedCostAllocation == 0)
            {
                this.storeCostCenter.DataSource = objClientCostCenter.LoadList("IdModel = " + IdModel + " AND TypeAllocation = 2 AND IdClientCostCenter NOT IN (SELECT IdClientCostCenter FROM tblSharedCostAllocation WHERE IdModel = " + IdModel + ")", "ORDER BY Name");
            }
            else
            {
                this.storeCostCenter.DataSource = objClientCostCenter.LoadList("IdModel = " + IdModel + " AND TypeAllocation = 2 AND IdClientCostCenter = " + ddlCostCenter.Value, "ORDER BY Name");
            }
            this.storeCostCenter.DataBind();
        }

        protected void StoreField_ReadData(object sender, StoreReadDataEventArgs e)
        {
            if (ddlCostCenter.Value != null && ddlCostCenter.Value.ToString() != "")
            {
                DataClass.clsSharedCostAllocation objSharedCostAllocation = new DataClass.clsSharedCostAllocation();

                objSharedCostAllocation.IdSharedCostAllocation = (Int32)IdSharedCostAllocation;
                objSharedCostAllocation.IdModel = (Int32)IdModel;
                DataTable dt = objSharedCostAllocation.LoadField(Convert.ToInt32(ddlCostCenter.Value));

                this.storeField.DataSource = dt;
                this.storeField.DataBind();
            }
            else
            {
                this.storeField.RemoveAll();
            }
        }

        public void DeleteFields()
        {
            DataClass.clsSharedCostField objSharedCostField = new DataClass.clsSharedCostField();
            objSharedCostField.IdSharedCostAllocation = (Int32)IdSharedCostAllocation;
            objSharedCostField.DeleteAll();
            this.ModelUpdateTimestamp();
        }

        public void Delete()
        {
            DataClass.clsSharedCostAllocation objSharedCostAllocation = new DataClass.clsSharedCostAllocation();
            objSharedCostAllocation.IdSharedCostAllocation = (Int32)IdSharedCostAllocation;
            objSharedCostAllocation.Delete();
            this.ModelUpdateTimestamp();
        }

        [DirectMethod]
        public void ClickedDeleteYES()
        {
            this.DeleteFields();
            this.Delete();
            this.LoadSharedCostAllocation();
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsSharedCostAllocation objSharedCostAllocation = new DataClass.clsSharedCostAllocation();
            DataTable dt = objSharedCostAllocation.LoadList("IdModel = " + IdModel + " AND IdClientCostCenter = " + this.ddlCostCenter.Value, "");
            Value = dt.Rows.Count;
            return Value;
        }

        protected void LoadSharedCostAllocation()
        {
            DataClass.clsSharedCostAllocation objSharedCostAllocation = new DataClass.clsSharedCostAllocation();

            DataTable dt = objSharedCostAllocation.LoadList("IdModel = " + IdModel, "ORDER BY ClientCostCenter");

            storeSharedCostAllocation.DataSource = dt;
            storeSharedCostAllocation.DataBind();

            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("CostObjectCode", "Cost Object Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("FieldCode", "Field Code"));
            String strSelectedParameters = "";
            DataTable dtSharedCostAllocation = objSharedCostAllocation.LoadExportList("IdModel = " + IdModel, "ORDER BY CostObjectCode, FieldCode");
            ExportSharedCostAllocation = new DataReportObject(dtSharedCostAllocation, "DATASHEET", null, strSelectedParameters, null, "CostObjectCode", datColumnTD, null);
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportSharedCostAllocation;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=2.2.2 ExportSharedCostAllocation.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportSharedCostAllocation.xls", "template");
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        protected void LoadLabel()
        {
            this.grdSharedCostAllocation.Title = (String)GetGlobalResourceObject("CPM_Resources", "Shared Cost Allocation");
            this.btnAdd.Text = (String)GetGlobalResourceObject("CPM_Resources", "Add New Shared Allocation");
            this.btnAdd.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Add New Shared Allocation");
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.colClientCostCenter.Text = (String)GetGlobalResourceObject("CPM_Resources", "Company Cost Object");
            this.colActivity.Text = (String)GetGlobalResourceObject("CPM_Resources", "Activity");
            this.colFields.Text = (String)GetGlobalResourceObject("CPM_Resources", "Fields");
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = "&nbsp;" + modMain.strCommonEdit;
            this.ImageCommandColumn1.Commands[1].Text = "&nbsp;" + modMain.strCommonDelete;
            this.winSharedCostAllocationEdit.Title = (String)GetGlobalResourceObject("CPM_Resources", "Shared Cost Allocation");
            this.ddlCostCenter.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Cost Object");
            this.FieldSet1.Title = (String)GetGlobalResourceObject("CPM_Resources", "Select Fields");
            this.colCode.Text = modMain.strCommonCode;
            this.colName.Text = modMain.strCommonName;
            this.chkSelected.Text = (String)GetGlobalResourceObject("CPM_Resources", "Select");
            this.btnSave.Text = modMain.strCommonSave;
            this.btnSave.ToolTip = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.btnCancel.ToolTip = modMain.strCommonClose;
            this.winImport.Title = modMain.strCommonImport + " " + (String)GetGlobalResourceObject("CPM_Resources", "Shared Cost Allocation") +"s";
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnImportFile.ToolTip = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.btnCancelImport.ToolTip = modMain.strCommonClose;
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        #endregion

    }
}
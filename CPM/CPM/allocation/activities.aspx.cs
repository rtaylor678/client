﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _activities : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdActivity { get { return (Int32)Session["IdActivity"]; } set { Session["IdActivity"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private DataReportObject ExportActivities { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                IdActivity = 0;
                this.LoadActivities();
                this.LoadLabel();
            }
        }

        protected void btnParameterModule_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/parameter/parametermodule.aspx");
        }

        protected void btnModel_Click(object sender, DirectEventArgs e)
        {
            if (this.ModelProcessing())
            {
                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Do you want to finish the case:") + " " + NameModel + "?", new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedNO()",
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
            else
            {
                X.Redirect("/caseselection.aspx");
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winActivitiesEdit.Show();
        }

        protected void btnDeleteAll_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete all records?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDeleteAllYES()",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.Save();
                this.LoadActivities();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            winActivitiesEdit.Hide();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.Activities);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Activity Code"].ToString(), row["Activity Desc"].ToString(), row["Type of Operation"].ToString());
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadActivities();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }
        #endregion

        #region Methods

        protected void Clear()
        {
            this.IdActivity = 0;
            this.txtName.Text = "";
            this.txtName.Reset();
            this.txtCode.Text = "";
            this.txtCode.Reset();
            this.ddlTypeOperation.Reset();
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void grdActivities_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            IdActivity = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            if (Command == "Edit")
            {
                this.winActivitiesEdit.Show();
                this.EditActivityLoad();
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + _Name + "?", new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedDeleteYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }      
        }

        protected void Save()
        {
            DataClass.clsActivities objActivities = new DataClass.clsActivities();
            objActivities.IdActivity = (Int32)IdActivity;
            objActivities.loadObject();

            if (IdActivity == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (objActivities.Code.ToString().ToUpper() != txtCode.Text.ToString().ToUpper())
                {
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }

            objActivities.Code = txtCode.Text;
            objActivities.Name = txtName.Text;
            objActivities.IdModel = IdModel;
            objActivities.IdTypeOperation = Convert.ToInt32(this.ddlTypeOperation.Value);
            if (IdActivity == 0)
            {
                objActivities.DateCreation = DateTime.Now;
                objActivities.UserCreation = (Int32)IdUserCreate;
                IdActivity = objActivities.Insert();
            }
            else
            {
                objActivities.DateModification = DateTime.Now;
                objActivities.UserModification = (Int32)IdUserCreate;
                objActivities.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winActivitiesEdit.Hide();
            this.ModelUpdateTimestamp();
        }

        protected Int32 LoadIdOperation(String _Operation)
        {
            Int32 IdOperation = 0;
            DataClass.clsTypesOperation objTypesOperation = new DataClass.clsTypesOperation();
            DataTable dt = objTypesOperation.LoadList("IdModel = " + IdModel + " AND LastNode = 1 AND Name LIKE '%" + ddlTypeOperation.Text + "%'", "ORDER BY Name");    

            if (dt.Rows.Count>0)
            {
                IdOperation = (Int32)dt.Rows[0]["IdTypeOperation"];
            }
            return IdOperation;
        }

        protected void SaveImport(String _Code, String _Name, String _TypeOperation)
        {
            Int32 _IdTypeOperation = -1;
            if (String.IsNullOrEmpty(_TypeOperation))
            {
                _IdTypeOperation = 0;
            }
            else
            {
                _IdTypeOperation = GetIdOperation(_TypeOperation);
            }

            if (!String.IsNullOrEmpty(_Code) && !String.IsNullOrEmpty(_Name) && _IdTypeOperation != -1)
            {
                DataClass.clsActivities objActivities = new DataClass.clsActivities();
                objActivities.IdActivity = (Int32)IdActivity;
                objActivities.loadObject();
                objActivities.Code = _Code;
                objActivities.Name = _Name;
                objActivities.IdModel = IdModel;
                objActivities.DateCreation = DateTime.Now;
                objActivities.UserCreation = (Int32)IdUserCreate;
                objActivities.IdTypeOperation = _IdTypeOperation;
                IdActivity = objActivities.Insert();
                objActivities = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected Int32 GetIdOperation(String _TypeOperation)
        {
            Int32 Value = 0;
            DataClass.clsTypesOperation objTypesOperation = new DataClass.clsTypesOperation();
            DataTable dt = objTypesOperation.LoadList("ParentName + ' - ' + Name = '" + _TypeOperation + "'", "");
            
            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdTypeOperation"];
            }
            return Value;
        }

        protected void EditActivityLoad()
        {
            DataClass.clsActivities objActivities = new DataClass.clsActivities();
            DataTable dt = objActivities.LoadList("IdActivity = " + IdActivity, "");
            if (dt.Rows.Count > 0)
            {
                this.txtCode.Text = (String)dt.Rows[0]["Code"];
                this.txtName.Text = (String)dt.Rows[0]["Name"];
                this.ddlTypeOperation.Value = Convert.ToInt32(dt.Rows[0]["IdTypeOperation"]);
            }
        }

        protected void StoreOperation_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsTypesOperation objTypesOperation = new DataClass.clsTypesOperation();
            this.storeOperation.DataSource = objTypesOperation.LoadListWithGeneral("IdModel = " + IdModel + " AND LastNode = 1", "ORDER BY SortOrder, Name");
            this.storeOperation.DataBind();
        }

        public void DeleteAll()
        {
            DataClass.clsActivities objActivities = new DataClass.clsActivities();
            objActivities.IdModel = (Int32)IdModel;
            objActivities.DeleteAll();
            this.ModelUpdateTimestamp();
        }

        public void Delete()
        {
            DataClass.clsActivities objActivities = new DataClass.clsActivities();
            objActivities.IdActivity = (Int32)IdActivity;
            objActivities.Delete();
            this.ModelUpdateTimestamp();
        }

        [DirectMethod]
        public void ClickedDeleteYES()
        {
            this.Delete();
            this.LoadActivities();
        }

        [DirectMethod]
        public void ClickedDeleteAllYES()
        {
            this.DeleteAll();
            this.LoadActivities();
        }

        [DirectMethod]
        public void ClickedYES()
        {
            this.UpdateStatus();
            X.Redirect("/caseselection.aspx");
        }

        [DirectMethod]
        public void ClickedNO()
        {
            X.Redirect("/caseselection.aspx");
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsActivities objActivities = new DataClass.clsActivities();
            DataTable dt = objActivities.LoadList("IdModel = " + IdModel + " AND Code = '" + txtCode.Text + "'", "");
            Value = dt.Rows.Count;

            return Value;
        }

        protected Boolean ModelProcessing()
        {
            Boolean Value = false;
            DataClass.clsModels objModels = new DataClass.clsModels();

            DataTable dt = objModels.LoadComboBox("IdModel = " + IdModel + " AND Status = 1", " ORDER BY Name");

            if (dt.Rows.Count > 0)
            {
                Value = true;
            }
            return Value;
        }

        public void UpdateStatus()
        {
            DataClass.clsModels objModels = new DataClass.clsModels();
            objModels.IdModel = IdModel;
            objModels.loadObject();

            objModels.Status = 2;
            objModels.Update();
        }

        protected void LoadActivities()
        {
            DataClass.clsActivities objActivities = new DataClass.clsActivities();

            DataTable dt = objActivities.LoadList("IdModel = " + IdModel, "ORDER BY Name");

            storeActivities.DataSource = dt;
            storeActivities.DataBind();

            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Code", "Activity Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Name", "Activity Desc"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("NameTypeOperation", "Type of Operation"));
            String strSelectedParameters = "";
            String[] columnsToCopy = { "Code", "Name", "NameTypeOperation" };
            System.Data.DataView view = new System.Data.DataView(dt);
            DataTable dtActivities = view.ToTable(true, columnsToCopy);

            //Need to modify NameTypeOperation data where it equal -All- make it blank
            foreach (DataRow row in dtActivities.Rows)
            {
                string oldX = row.Field<String>("NameTypeOperation");
                string newX = "- All -".Equals(oldX, StringComparison.OrdinalIgnoreCase) ? "" : row["NameTypeOperation"].ToString();
                row.SetField("NameTypeOperation", newX);
            }

            ExportActivities = new DataReportObject(dtActivities, "DATASHEET", null, strSelectedParameters, null, "Name", datColumnTD, null);
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportActivities;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=1.4 ExportActivities.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportActivities.xls", "template");
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, true, false, false);
            objModelUpdate = null;
        }

        protected void LoadLabel()
        {
            this.grdActivities.Title = modMain.strMasterActivities;
            this.btnAdd.Text = (String)GetGlobalResourceObject("CPM_Resources", "Add New Activity");
            this.btnAdd.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Add New Activity");
            this.btnDelete.Text = modMain.strCommonDeleteAll;
            this.btnDelete.ToolTip = modMain.strCommonDeleteAll;
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.colCode.Text = modMain.strCommonCode;
            this.colName.Text = modMain.strCommonName;
            this.colTypeOperation.Text = (String)GetGlobalResourceObject("CPM_Resources", "Type of Operation");
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = "&nbsp;" + modMain.strCommonEdit;
            this.ImageCommandColumn1.Commands[1].Text = "&nbsp;" + modMain.strCommonDelete;
            this.winActivitiesEdit.Title = (String)GetGlobalResourceObject("CPM_Resources", "Activity");
            this.txtCode.FieldLabel = modMain.strCommonCode;
            this.txtName.FieldLabel = modMain.strCommonName;
            this.ddlTypeOperation.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Operation Type");
            this.btnSave.Text = modMain.strCommonSave;
            this.btnSave.ToolTip = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.btnCancel.ToolTip = modMain.strCommonClose;
            this.winImport.Title = modMain.strCommonImport + " " + modMain.strMasterActivities;
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnImportFile.ToolTip = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.btnCancelImport.ToolTip = modMain.strCommonClose;
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        #endregion

    }
}
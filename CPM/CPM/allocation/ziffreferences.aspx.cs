﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _ziffreferences : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }

        private static modMain objMain = new modMain();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                this.LoadModules();
                LoadLabel();
            }
        }
        /// <summary> List of Peer Group </summary>
        protected void btnListOfPeerGroup_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/peergroup.aspx");
        }

        /// <summary> List of Peer Criteria </summary>
        protected void btnZiffListOfCriteria_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/ziffpeercriteria.aspx");
        }

        /// <summary> Peer Criteria Data </summary>
        protected void btnZiffPeerCriteriaData_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/ziffpeercriteriadata.aspx");
        }

        /// <summary> External Base Cost References </summary>
        protected void btnZiffBaseCost_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/ziffbasecost.aspx");
        }

        /// <summary> NOT USED </summary>
        protected void btnExternalBenchmarkRepository_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/externalfilesrepository.aspx");
        }

        #endregion

        #region Methods

        protected void LoadModules()
        {
            // Allocation Module
            if (objApplication.AllowedRoles.Contains(13))
            {
                // List of Peer Group
                if (objApplication.AllowedRoles.Contains(51))
                {
                    this.btnListOfPeerGroup.Disabled = false;
                }
                // List of Peer Criteria
                if (objApplication.AllowedRoles.Contains(24))
                {
                    this.btnZiffListOfCriteria.Disabled = false;
                }
                // Peer Criteria Data
                if (objApplication.AllowedRoles.Contains(25))
                {
                    this.btnZiffPeerCriteriaData.Disabled = false;
                }
                // External Base Cost References
                if (objApplication.AllowedRoles.Contains(26))
                {
                    this.btnZiffBaseCost.Disabled = false;
                }
            }
        }

        protected void LoadLabel()
        {
            this.pnlZiffReferences.Title = modMain.strMasterZiff_Third_partyCostReferences;
            this.btnZiffListOfCriteria.Text = objMain.FormatButtonText(modMain.strMasterListofPeerCriteria);
            this.btnZiffPeerCriteriaData.Text = objMain.FormatButtonText(modMain.strMasterPeerCriteriaData);
            this.btnZiffBaseCost.Text = objMain.FormatButtonText(modMain.strMasterZiff_Third_partyBaseCost);
            this.btnExternalBenchmarkRepository.Text = objMain.FormatButtonText((String)GetGlobalResourceObject("CPM_Resources", "External Benchmarks Files Repository"));
            this.btnListOfPeerGroup.Text = objMain.FormatButtonText((String)GetGlobalResourceObject("CPM_Resources", "ListOfPeerGroup"));
        }

        #endregion

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _allocationmodule : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"]; }  }//  set { Session["idLanguage"] = value; } } }

        private static modMain objMain = new modMain();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                LoadLanguage();
                this.LoadModules();
            }
        }

        protected void btnParameterModule_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/parameter/parametermodule.aspx");
        }

        protected void btnAllocationModule_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/allocationmodule.aspx");
        }

        protected void btnTechnicalModule_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/technical/technicalmodule.aspx");
        }

        protected void btnEconomicModule_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/economic/economicmodule.aspx");
        }

        protected void btnReportModule_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/report/reportmodule.aspx");
        }

        protected void btnZiffReferences_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/ziffreferences.aspx");
        }

        protected void btnClientReferences_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/clientreferences.aspx");
        }

        protected void btnCostStructureAssumptions_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/coststructureassumptions.aspx");
        }

        /// Cost Benchmarking
        protected void btnCostBenchmarking_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/report/costbenchmarking.aspx");
        }

        #endregion

        #region Methods

        protected void LoadLanguage()
        {
            DataClass.clsLabelsLanguages objLabelsLanguages = new DataClass.clsLabelsLanguages();
            DataTable dt = objLabelsLanguages.LoadList("LanguageName = '" + Language + "' AND FormName = 'AllocationModule'", "ORDER BY IdLabel");
            foreach (DataRow row in dt.Rows)
            {
                //idLanguage = (Int32)row["IdLanguage"];
                switch ((String)row["LabelName"])
                {
                    case "Allocation Module":
                        modMain.strAllocationModuleFormTitle = (String)row["LabelText"];
                        break;
                    case "1. References":
                        modMain.strAllocationModuleFormSubTitle1 = (String)row["LabelText"];
                        break;
                    case "2. Assumptions":
                        modMain.strAllocationModuleFormSubTitle2 = (String)row["LabelText"];
                        break;
                }
            }
            this.LoadLabel();
        }

        protected void LoadModules()
        {
            // Allocation Module
            if (objApplication.AllowedRoles.Contains(13))
            {
                // Internal Cost References (Group)
                if (objApplication.AllowedRoles.Contains(9) || objApplication.AllowedRoles.Contains(10) || objApplication.AllowedRoles.Contains(11) || objApplication.AllowedRoles.Contains(12) || objApplication.AllowedRoles.Contains(14) || objApplication.AllowedRoles.Contains(15) || objApplication.AllowedRoles.Contains(16) || objApplication.AllowedRoles.Contains(17) || objApplication.AllowedRoles.Contains(18) || objApplication.AllowedRoles.Contains(19) || objApplication.AllowedRoles.Contains(20) || objApplication.AllowedRoles.Contains(21) || objApplication.AllowedRoles.Contains(22) || objApplication.AllowedRoles.Contains(23))
                {
                    this.btnClientReferences.Disabled = false;
                }
                // External Cost References (Group)
                if (objApplication.AllowedRoles.Contains(24) || objApplication.AllowedRoles.Contains(25) || objApplication.AllowedRoles.Contains(26) || objApplication.AllowedRoles.Contains(51))
                {
                    this.btnZiffReferences.Disabled = false;
                }
                //// Cost Structure Assumptions
                //if (objApplication.AllowedRoles.Contains(27))
                //{
                //    this.btnCostStructureAssumptions.Disabled = false;
                //}
                // Competitiveness Cost Benchmarking
                if (objApplication.AllowedRoles.Contains(54))
                {
                    this.btnCostBenchmarking.Disabled = false;
                }
            }
        }

        protected void LoadLabel()
        {
            this.pnlAllocationModule.Title = modMain.strAllocationModuleFormTitle;
            this.FieldSet3.Title = modMain.strAllocationModuleFormSubTitle1;
            this.btnClientReferences.Text = objMain.FormatButtonText(modMain.strMasterCompanyCostReferences);
            this.btnZiffReferences.Text = objMain.FormatButtonText(modMain.strMasterZiff_Third_partyCostReferences);
            this.FieldSet4.Title = modMain.strAllocationModuleFormSubTitle2;
            this.btnCostStructureAssumptions.Text = objMain.FormatButtonText(modMain.strMasterCostStructureAssumptions);
            this.FieldSet5.Title = (String)GetGlobalResourceObject("CPM_Resources", "2.Reports");
            this.btnCostBenchmarking.Text = objMain.FormatButtonText((String)GetGlobalResourceObject("CPM_Resources", "CostBenchmarking"));

        }

        #endregion  

    }
}
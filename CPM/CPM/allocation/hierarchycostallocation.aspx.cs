﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _hierarchycostallocation : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdHierarchyCostAllocation { get { return (Int32)Session["IdHierarchyCostAllocation"]; } set { Session["IdHierarchyCostAllocation"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private DataReportObject ExportHierarchyCostAllocation { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                clsModels objModels = new clsModels();
                this.colCost.Text = (String)GetGlobalResourceObject("CPM_Resources", "Cost") + " " + objModels.GetDefaultCurrencySymbol(IdModel);
                IdHierarchyCostAllocation = 0;
                this.LoadHierarchyCostAllocation();
                this.LoadLabel();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winHierarchyCostAllocationEdit.Show();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.Save();
                this.LoadHierarchyCostAllocation();
            }
            catch (Exception ex)
            {
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving") + " " + ex.Message, IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            winHierarchyCostAllocationEdit.Hide();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.HierarchyCostAllocation);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport((String)row["Cost Object Code"], (String)row["Aggregation Level"]);
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadHierarchyCostAllocation();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        protected void Clear()
        {
            this.IdHierarchyCostAllocation = 0;
            this.ddlCostCenter.Reset();
            this.ddlLevels.Reset();
            this.storeCostCenter.Reload();
            this.storeLevels.Reload();
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void grdHierarchyCostAllocation_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            IdHierarchyCostAllocation = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            if (Command == "Edit")
            {
                this.winHierarchyCostAllocationEdit.Show();
                this.EditActivityLoad();
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + _Name + "?", new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedDeleteYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }      
        }

        protected void Save()
        {
            DataClass.clsHierarchyCostAllocation objHierarchyCostAllocation = new DataClass.clsHierarchyCostAllocation();
            objHierarchyCostAllocation.IdHierarchyCostAllocation = (Int32)IdHierarchyCostAllocation;
            objHierarchyCostAllocation.loadObject();

            if (IdHierarchyCostAllocation == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (objHierarchyCostAllocation.IdClientCostCenter != Convert.ToInt32(ddlCostCenter.Value))
                {
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }
            objHierarchyCostAllocation.IdModel = IdModel;
            objHierarchyCostAllocation.IdClientCostCenter = Convert.ToInt32(this.ddlCostCenter.Value);
            objHierarchyCostAllocation.IdAggregationLevel = Convert.ToInt32(this.ddlLevels.Value);
            if (IdHierarchyCostAllocation == 0)
            {
                objHierarchyCostAllocation.DateCreation = DateTime.Now;
                objHierarchyCostAllocation.UserCreation = (Int32)IdUserCreate;
                IdHierarchyCostAllocation = objHierarchyCostAllocation.Insert();
            }
            else
            {
                objHierarchyCostAllocation.DateModification = DateTime.Now;
                objHierarchyCostAllocation.UserModification = (Int32)IdUserCreate;
                objHierarchyCostAllocation.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winHierarchyCostAllocationEdit.Hide();
            this.ModelUpdateTimestamp();
        }

        protected Int32 LoadIdCostCenter(String _Value)
        {
            Int32 IdClientCostCenter = 0;
            DataClass.clsClientCostCenter objClientCostCenter = new DataClass.clsClientCostCenter();
            DataTable dt = objClientCostCenter.LoadList("IdModel = " + IdModel + " AND Name LIKE '%" + _Value + "%'", "ORDER BY Name");    

            if (dt.Rows.Count>0)
            {
                IdClientCostCenter = (Int32)dt.Rows[0]["IdClientCostCenter"];
            }
            return IdClientCostCenter;
        }

        protected Int32 LoadIdActivity(String _Value)
        {
            Int32 IdAccount = 0;
            DataClass.clsActivities objActivities = new DataClass.clsActivities();
            DataTable dt = objActivities.LoadList("IdModel = " + IdModel + " AND Name LIKE '%" + _Value + "%'", "ORDER BY Name");

            if (dt.Rows.Count > 0)
            {
                IdAccount = (Int32)dt.Rows[0]["IdActivity"];
            }
            return IdAccount;
        }

        protected Int32 LoadIdAggregationLevel(String _Value)
        {
            Int32 IdAccount = 0;
            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dt = objAggregationLevels.LoadList("IdModel = " + IdModel + " AND Name LIKE '%" + _Value + "%'", "ORDER BY Name");

            if (dt.Rows.Count > 0)
            {
                IdAccount = (Int32)dt.Rows[0]["IdAggregationLevel"];
            }
            return IdAccount;
        }

        protected void SaveImport(String _CostCenter, String _AggregationLevel)
        {
            Int32 _IdCostCenter = GetIdCostCenter(_CostCenter);
            Int32 _IdAggregationLevel = GetIdAggregationLevel(_AggregationLevel);

            if (_IdCostCenter != 0 && _IdAggregationLevel != 0)
            {
                DataClass.clsHierarchyCostAllocation objHierarchyCostAllocation = new DataClass.clsHierarchyCostAllocation();
                DataTable dtAux = objHierarchyCostAllocation.LoadList("IdModel = " + IdModel + " AND IdClientCostCenter = " + _IdCostCenter + " AND IdAggregationLevel = " + _IdAggregationLevel, "");
                if (dtAux.Rows.Count > 0)
                {
                    IdHierarchyCostAllocation = Convert.ToInt32(dtAux.Rows[0]["IdHierarchyCostAllocation"]);
                }
                else
                {
                    IdHierarchyCostAllocation = 0;
                }
                objHierarchyCostAllocation.IdHierarchyCostAllocation = (Int32)IdHierarchyCostAllocation;
                objHierarchyCostAllocation.loadObject();
                objHierarchyCostAllocation.IdModel = IdModel;
                objHierarchyCostAllocation.IdClientCostCenter = _IdCostCenter;
                objHierarchyCostAllocation.IdAggregationLevel = _IdAggregationLevel;
                objHierarchyCostAllocation.DateCreation = DateTime.Now;
                objHierarchyCostAllocation.UserCreation = (Int32)IdUserCreate;
                if (objHierarchyCostAllocation.IdHierarchyCostAllocation == 0)
                {
                    objHierarchyCostAllocation.DateCreation = DateTime.Now;
                    objHierarchyCostAllocation.UserCreation = (Int32)IdUserCreate;
                    IdHierarchyCostAllocation = objHierarchyCostAllocation.Insert();
                }
                else
                {
                    objHierarchyCostAllocation.DateModification = DateTime.Now;
                    objHierarchyCostAllocation.UserModification = (Int32)IdUserCreate;
                    objHierarchyCostAllocation.Update();
                }
                dtAux.Dispose();
                dtAux = null;
                objHierarchyCostAllocation = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected Int32 GetIdCostCenter(String _CostCenter)
        {
            Int32 Value = 0;
            DataClass.clsClientCostCenter objClientCostCenter = new DataClass.clsClientCostCenter();
            DataTable dt = objClientCostCenter.LoadList("Code = '" + _CostCenter + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdClientCostCenter"];
            }
            return Value;
        }

        protected Int32 GetIdAggregationLevel(String _AggregationLevel)
        {
            Int32 Value = 0;
            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dt = objAggregationLevels.LoadList("Code = '" + _AggregationLevel + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdAggregationLevel"];
            }
            return Value;
        }

        protected void EditActivityLoad()
        {
            DataClass.clsHierarchyCostAllocation objClientCostData = new DataClass.clsHierarchyCostAllocation();
            DataTable dt = objClientCostData.LoadList("IdHierarchyCostAllocation = " + IdHierarchyCostAllocation, "");
            if (dt.Rows.Count > 0)
            {
                this.ddlCostCenter.Value = (Int32)(dt.Rows[0]["IdClientCostCenter"]);
                this.ddlLevels.Value = (Int32)(dt.Rows[0]["IdAggregationLevel"]);
                this.storeCostCenter.Reload();
                this.storeLevels.Reload();
            }
        }

        protected void StoreCostCenter_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsClientCostCenter objClientCostCenter = new DataClass.clsClientCostCenter();
            if (IdHierarchyCostAllocation == 0)
            {
                this.storeCostCenter.DataSource = objClientCostCenter.LoadList("IdModel = " + IdModel + " AND TypeAllocation = 3 AND IdClientCostCenter NOT IN (SELECT IdClientCostCenter FROM tblHierarchyCostAllocation WHERE IdModel = " + IdModel + ")", "ORDER BY Name");
            }
            else
            {
                this.storeCostCenter.DataSource = objClientCostCenter.LoadList("IdModel = " + IdModel + " AND TypeAllocation = 3 AND IdClientCostCenter = " + ddlCostCenter.Value, "ORDER BY Name");
            }
            this.storeCostCenter.DataBind();
        }

        protected void StoreLevels_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            this.storeLevels.DataSource = objAggregationLevels.LoadList("IdModel = " + IdModel, "ORDER BY Name");
            this.storeLevels.DataBind();
        }

        public void Delete()
        {
            DataClass.clsHierarchyCostAllocation objClientCostData = new DataClass.clsHierarchyCostAllocation();
            objClientCostData.IdHierarchyCostAllocation = (Int32)IdHierarchyCostAllocation;
            objClientCostData.Delete();
            this.ModelUpdateTimestamp();
        }

        [DirectMethod]
        public void ClickedDeleteYES()
        {
            this.Delete();
            this.LoadHierarchyCostAllocation();
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsHierarchyCostAllocation objHierarchyCostAllocation = new DataClass.clsHierarchyCostAllocation();
            DataTable dt = objHierarchyCostAllocation.LoadList("IdModel = " + IdModel + " AND IdClientCostCenter = " + this.ddlCostCenter.Value, "");
            Value = dt.Rows.Count;

            return Value;
        }

        protected void LoadHierarchyCostAllocation()
        {
            DataClass.clsHierarchyCostAllocation objHierarchyCostAllocation = new DataClass.clsHierarchyCostAllocation();
            DataTable dt = objHierarchyCostAllocation.LoadList("IdModel = " + IdModel, "ORDER BY ClientCostCenter");
            storeHierarchyCostAllocation.DataSource = dt;
            storeHierarchyCostAllocation.DataBind();

            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("CostObjectCode", "Cost Object Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Code", "Aggregation Level"));
            String strSelectedParameters = "";
            String[] columnsToCopy = { "CostObjectCode", "Code" };
            System.Data.DataView view = new System.Data.DataView(dt);
            DataTable dtHierarchyCostAllocation = view.ToTable(true, columnsToCopy);
            ExportHierarchyCostAllocation = new DataReportObject(dtHierarchyCostAllocation, "DATASHEET", null, strSelectedParameters, null, "CostObjectCode", datColumnTD, null);
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportHierarchyCostAllocation;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=2.2.3 ExportHierarchyCostAllocation.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportHierarchyCostAllocation.xls", "template");
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        protected void LoadLabel()
        {
            this.grdHierarchyCostAllocation.Title = (String)GetGlobalResourceObject("CPM_Resources", "Hierarchy Cost Allocation");
            this.btnAdd.Text = (String)GetGlobalResourceObject("CPM_Resources", "Add New Hierarchy Allocation");
            this.btnAdd.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Add New Hierarchy Allocation");
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.colClientCostCenter.Text = (String)GetGlobalResourceObject("CPM_Resources", "Company Cost Object");
            this.colActivity.Text = (String)GetGlobalResourceObject("CPM_Resources", "Activity");
            this.colAggregationLevel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Aggregation Level");
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = "&nbsp;" + modMain.strCommonEdit;
            this.ImageCommandColumn1.Commands[1].Text = "&nbsp;" + modMain.strCommonDelete;
            this.winHierarchyCostAllocationEdit.Title = (String)GetGlobalResourceObject("CPM_Resources", "Hierarchy Cost Allocation");
            this.ddlCostCenter.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Cost Object");
            this.ddlLevels.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Aggregation");
            this.btnSave.Text = modMain.strCommonSave;
            this.btnSave.ToolTip = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.btnCancel.ToolTip = modMain.strCommonClose;
            this.winImport.Title = modMain.strCommonImport + " " + (String)GetGlobalResourceObject("CPM_Resources", "Hierarchy Cost Allocation");
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnImportFile.ToolTip = modMain.strCommonImport;
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        #endregion

    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _ziffpeercriteria : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdPeerCriteria { get { return (Int32)Session["IdPeerCriteria"]; } set { Session["IdPeerCriteria"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private DataReportObject ExportPeerCriteria { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                this.storeTypeUnit.Reload();
                IdPeerCriteria = 0;
                this.LoadPeerCriteria();
                this.LoadLabel();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winPeerCriteriaEdit.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.Save();
                this.LoadPeerCriteria();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            winPeerCriteriaEdit.Hide();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.ZiffPeerCriteria);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Criteria Desc"].ToString(), row["Unit Desc"].ToString(), row["Total Unit Cost Denominator"].ToString().ToUpper() == "YES" ? true : false);
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadPeerCriteria();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void grdPeerCriteria_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            IdPeerCriteria = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            if (Command == "Edit")
            {
                this.winPeerCriteriaEdit.Show();
                this.EditPeerCriteriaLoad();
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + _Name + "?", new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedDeleteYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
        }

        protected void StoreTypeUnit_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsTypeUnit objTypeUnit = new DataClass.clsTypeUnit();
            this.storeTypeUnit.DataSource = objTypeUnit.LoadList("", "ORDER BY Name");
            this.storeTypeUnit.DataBind();
        }

        protected void StoreUnit_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsUnit objUnit = new DataClass.clsUnit();

            if (ddlTypeUnit.Value == null)
            {
                this.storeUnit.DataSource = objUnit.LoadList("Name LIKE '%" + ddlUnit.Text + "%'", "ORDER BY Name");
            }
            else
            {
                this.storeUnit.DataSource = objUnit.LoadList("IdTypeUnit = " + ddlTypeUnit.Value, "ORDER BY Name");
            }
            this.storeUnit.DataBind();
        }

        protected void ddlTypeUnit_Select(object sender, DirectEventArgs e)
        {
            if (ddlTypeUnit.Value != null && ddlTypeUnit.Value.ToString() != "")
            {
                this.ddlUnit.ReadOnly = false;
            }
            else
            {
                this.ddlUnit.ReadOnly = true;
            }
            this.ddlUnit.Text = null;
            this.storeUnit.Reload();
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void Clear()
        {
            this.IdPeerCriteria = 0;
            this.txtDescription.Text = "";
            this.txtDescription.Reset();
            this.ddlTypeUnit.Reset();
            this.ddlUnit.Reset();
            this.ddlUnit.ReadOnly = true;
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsPeerCriteria objPeerCriteria = new DataClass.clsPeerCriteria();
            DataTable dt = objPeerCriteria.LoadList("IdModel = " + IdModel + " AND Description = '" + txtDescription.Text + "'", "");
            Value = dt.Rows.Count;

            return Value;
        }

        protected void Save()
        {
            DataClass.clsPeerCriteria objPeerCriteria = new DataClass.clsPeerCriteria();
            objPeerCriteria.IdPeerCriteria = (Int32)IdPeerCriteria;
            objPeerCriteria.loadObject();

            if (IdPeerCriteria == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (objPeerCriteria.Description.ToString().ToUpper() != txtDescription.Text.ToString().ToUpper())
                {
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }

            objPeerCriteria.IdModel = IdModel;
            objPeerCriteria.Description = txtDescription.Text.ToString();
            objPeerCriteria.IdUnitPeerCriteria = Convert.ToInt32(this.ddlUnit.Value);
            if (chkTotalUnitCostDenominator.Checked)
                objPeerCriteria.TotalUnitCostDenominator = true;
            else
                objPeerCriteria.TotalUnitCostDenominator = false;

            if (IdPeerCriteria == 0)
            {
                IdPeerCriteria = objPeerCriteria.Insert();
            }
            else
            {
                objPeerCriteria.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winPeerCriteriaEdit.Hide();
            this.ModelUpdateTimestamp();
        }

        protected void SaveImport(String _Description, String _Unit, Boolean _TotalUnitCostDenominator)
        {
            Int32 _IdUnit = GetIdUnit(_Unit);

            if (!String.IsNullOrEmpty(_Description) && _IdUnit != 0)
            {
                DataClass.clsPeerCriteria objPeerCriteria = new DataClass.clsPeerCriteria();
                objPeerCriteria.IdPeerCriteria = (Int32)IdPeerCriteria;
                objPeerCriteria.loadObject();
                objPeerCriteria.Description = _Description;
                objPeerCriteria.IdUnitPeerCriteria = _IdUnit;
                objPeerCriteria.IdModel = IdModel;
                objPeerCriteria.TotalUnitCostDenominator = _TotalUnitCostDenominator;
                IdPeerCriteria = objPeerCriteria.Insert();
                objPeerCriteria = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected Int32 GetIdUnit(String _Unit)
        {
            Int32 Value = 0;
            DataClass.clsUnit objUnit = new DataClass.clsUnit();
            DataTable dt = objUnit.LoadList("Name = '" + _Unit + "'", "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdUnit"];
            }
            return Value;
        }

        protected void EditPeerCriteriaLoad()
        {
            DataClass.clsPeerCriteria objPeerCriteria = new DataClass.clsPeerCriteria();
            DataTable dt = objPeerCriteria.LoadList("IdModel = " + IdModel + " AND IdPeerCriteria = " + IdPeerCriteria, "");
            if (dt.Rows.Count > 0)
            {
                this.txtDescription.Text = (String)dt.Rows[0]["Description"];
                this.ddlTypeUnit.Value = Convert.ToInt32(dt.Rows[0]["IdTypeUnit"]);
                this.ddlUnit.Value = Convert.ToInt32(dt.Rows[0]["IdUnitPeerCriteria"]);
                this.chkTotalUnitCostDenominator.Checked = Convert.ToBoolean(dt.Rows[0]["TotalUnitCostDenominator"]);
                this.storeUnit.Reload();
                this.ddlUnit.ReadOnly = false;
            }
        }

        public void Delete()
        {
            DataClass.clsPeerCriteria objPeerCriteria = new DataClass.clsPeerCriteria();
            objPeerCriteria.IdPeerCriteria = (Int32)IdPeerCriteria;
            objPeerCriteria.Delete();
            objPeerCriteria = null;
        }

        [DirectMethod]
        public void ClickedDeleteYES()
        {
            this.Delete();
            this.LoadPeerCriteria();
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportListOfPeerCriteria.xls", "template");
        }

        protected void LoadPeerCriteria()
        {
            DataClass.clsPeerCriteria objPeerCriteria = new DataClass.clsPeerCriteria();
            DataTable dt = objPeerCriteria.LoadList("IdModel = " + IdModel, "ORDER BY Description");
            storePeerCriteria.DataSource = dt;
            storePeerCriteria.DataBind();

            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Description", "Criteria Desc"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Unit", "Unit Desc"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("TotalUnitCostDenominator", "Total Unit Cost Denominator"));
            String strSelectedParameters = "";
            String[] columnsToCopy = { "Description", "Unit", "TotalUnitCostDenominator" };
            System.Data.DataView view = new System.Data.DataView(dt);
            DataTable dtPeerCriteria = view.ToTable(true, columnsToCopy);
            ExportPeerCriteria = new DataReportObject(dtPeerCriteria, "DATASHEET", null, strSelectedParameters, null, "Description", datColumnTD, null);
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportPeerCriteria;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=2.5.1 ExportListOfPeerCriteria.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        protected void LoadLabel()
        {
            this.grdPeerCriteria.Title = modMain.strMasterListofPeerCriteria;
            this.btnAdd.Text = (String)GetGlobalResourceObject("CPM_Resources", "Add New Peer Criteria");
            this.btnAdd.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Add New Peer Criteria");
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.colDescription.Text = (String)GetGlobalResourceObject("CPM_Resources", "Criteria");
            this.colTypeUnit.Text = (String)GetGlobalResourceObject("CPM_Resources", "Unit Type");
            this.colUnit.Text = (String)GetGlobalResourceObject("CPM_Resources", "Unit");
            this.colTotalUnitCostDenominator.Text = (String)GetGlobalResourceObject("CPM_Resources", "Total Unit Cost Denominator");
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = "&nbsp;" + modMain.strCommonEdit;
            this.ImageCommandColumn1.Commands[1].Text = "&nbsp;" + modMain.strCommonDelete;
            this.winPeerCriteriaEdit.Title = (String)GetGlobalResourceObject("CPM_Resources", "Peer Criteria");
            this.txtDescription.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Description");
            this.ddlTypeUnit.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Unit Type");
            this.ddlUnit.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Unit");
            this.chkTotalUnitCostDenominator.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Total Unit Cost Denominator");
            this.btnSave.Text = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.winImport.Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Peer Criteria");
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Import");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        #endregion

    }
}
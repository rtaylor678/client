﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;
using System.ComponentModel;
using System.Web.Script.Serialization;

namespace CPM
{
    public partial class _allocationdriversbyfield : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdAllocationDriversByField { get { return (Int32)Session["IdAllocationDriversByField"]; } set { Session["IdAllocationDriversByField"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private DataReportObject ExportAllocationDriversByField { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                this.LoadLabel();
                IdAllocationDriversByField = 0;
                this.ddlFilterDriver.Select(0);
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
        }

        protected void btnDeleteAll_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete all records?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDeleteAllYES()",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                String rowsValues = e.ExtraParams["rowsValues"];
                this.Save(rowsValues);
                this.storeField.Reload();
            }
            catch (Exception ex)
            {
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving") + " " + ex.Message, IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.AllocationDriverByField);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Driver Desc"].ToString(), row["Field Code"].ToString(), Convert.ToDouble(row["Amount"]));
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadAllocationDriver();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void ddlFilterDriver_Select(object sender, DirectEventArgs e)
        {
            this.storeField.Reload();
        }

        protected void grdAllocationDriversByField_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            Int32 _IdDriver = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            if (Command == "Edit")
            {
                this.EditAllocationListDriverLoad(_IdDriver);
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + _Name + "?", new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedDeleteYES('" + _Name + "')",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
        }

        protected void StoreFilterDriver_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsAllocationListDriver objAllocationListDriver = new DataClass.clsAllocationListDriver();
            this.storeFilterDriver.DataSource = objAllocationListDriver.LoadList("IdModel = " + IdModel + " AND Name LIKE '%" + ddlFilterDriver.Text + "%'", "ORDER BY Name");
            this.storeFilterDriver.DataBind();
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void StoreField_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsAllocationDriversByField objAllocationDriversByField = new DataClass.clsAllocationDriversByField();
            Int32 intIdDriver = 0;

            DataTable dt = new DataTable();
            try
            {
                intIdDriver = Convert.ToInt32(ddlFilterDriver.Value);
                objAllocationDriversByField.IdAllocationListDriver = intIdDriver;
                dt = objAllocationDriversByField.LoadFields(IdModel);
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
            }

            storeField.DataSource = dt;
            storeField.DataBind();


            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("DriverDesc", "Driver Desc"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("FieldCode", "Field Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Amount", "Amount"));
            String strSelectedParameters = "";
            DataTable dtAllocationDriversByField = objAllocationDriversByField.LoadExportDriversDataList("IdModel=" + IdModel, "ORDER BY DriverDesc, FieldCode");
            ExportAllocationDriversByField = new DataReportObject(dtAllocationDriversByField, "DATASHEET", null, strSelectedParameters, null, "DriverDesc", datColumnTD, null);
        }

        #endregion

        #region Methods

        protected void Clear()
        {
            this.IdAllocationDriversByField = 0;
            this.ddlFilterDriver.Select(0);
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void Save(String _Values)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("IdField", typeof(int));
            dt.Columns.Add("IdAllocationDriversByField", typeof(int));
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Code", typeof(string));
            dt.Columns.Add("Amount", typeof(float));

            List<Object> data = JSON.Deserialize<List<Object>>(_Values);

            foreach (Object value in data)
            {
                Object _ObjectRow = new Object();
                _ObjectRow = value.ToString();

                JavaScriptSerializer ser = new JavaScriptSerializer();

                Dictionary<string, object> _Row = ser.Deserialize<Dictionary<string, object>>(_ObjectRow.ToString());
                DataRow _NewRow = dt.NewRow();

                _NewRow["IdField"] = _Row["IdField"];
                _NewRow["IdAllocationDriversByField"] = _Row["IdAllocationDriversByField"];
                _NewRow["Name"] = _Row["Name"].ToString();
                _NewRow["Code"] = _Row["Code"].ToString();
                _NewRow["Amount"] = _Row["Amount"].ToString();

                dt.Rows.Add(_NewRow);
            }

            foreach (DataRow row in dt.Rows)
            {
                DataClass.clsAllocationDriversByField objAllocationDriversData = new DataClass.clsAllocationDriversByField();



                IdAllocationDriversByField = Convert.ToInt32(row["IdAllocationDriversByField"]);

                objAllocationDriversData.IdAllocationDriversByField = IdAllocationDriversByField;
                objAllocationDriversData.loadObject();

                objAllocationDriversData.IdAllocationDriversByField = IdAllocationDriversByField;
                objAllocationDriversData.IdField = Convert.ToInt32(row["IdField"]);
                objAllocationDriversData.IdAllocationListDriver = Convert.ToInt32(ddlFilterDriver.Value);
                objAllocationDriversData.Amount = Convert.ToDouble(row["Amount"]);

                if (IdAllocationDriversByField == 0)
                {
                    if (Convert.ToDouble(row["Amount"]) > 0)
                    {
                        objAllocationDriversData.DateCreation = DateTime.Now;
                        objAllocationDriversData.UserCreation = (Int32)IdUserCreate;
                        objAllocationDriversData.Insert();
                    }
                    else
                    {
                        objAllocationDriversData.Delete();
                    }
                }
                else
                {
                    if (Convert.ToDouble(row["Amount"]) == 0)
                    {
                        objAllocationDriversData.Delete();
                    }
                    else
                    {
                        objAllocationDriversData.DateModification = DateTime.Now;
                        objAllocationDriversData.UserModification = (Int32)IdUserCreate;
                        objAllocationDriversData.Update();
                    }
                }
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            //ToDo: Does a manual entry form need to be created for this?
            this.ModelUpdateTimestamp();
        }

        protected Int32 LoadIdUnit(String _Unit)
        {
            Int32 IdUnit = 0;
            DataClass.clsUnit objUnit = new DataClass.clsUnit();
            DataTable dt = objUnit.LoadList("Name LIKE '%" + _Unit + "%'", "ORDER BY Name");    

            if (dt.Rows.Count>0)
            {
                IdUnit = (Int32)dt.Rows[0]["IdUnit"];
            }
            return IdUnit;
        }

        protected void SaveImport(String _Driver, String _Field, Double _Amount)
        {
            Int32 _IdDriver = GetIdDriver(_Driver);
            Int32 _IdField = GetIdField(_Field);

            if (_IdDriver != 0 && _IdField != 0 && _Amount != 0)
            {
                DataClass.clsAllocationDriversByField objAllocationDriversByField = new DataClass.clsAllocationDriversByField();
                objAllocationDriversByField.IdAllocationDriversByField = (Int32)IdAllocationDriversByField;
                objAllocationDriversByField.loadObject();
                objAllocationDriversByField.IdAllocationListDriver = _IdDriver;
                objAllocationDriversByField.IdField = _IdField;
                objAllocationDriversByField.Amount = _Amount;
                objAllocationDriversByField.DateCreation = DateTime.Now;
                objAllocationDriversByField.UserCreation = (Int32)IdUserCreate;
                IdAllocationDriversByField = objAllocationDriversByField.Insert();
                objAllocationDriversByField = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected String GetUnit(Int32 _IdUnit)
        {
            String Value = "";
            DataClass.clsUnit objUnit = new DataClass.clsUnit();
            DataTable dt = objUnit.LoadComboBox("IdUnit = " + _IdUnit, "");

            if (dt.Rows.Count > 0)
            {
                Value = (String)dt.Rows[0]["Name"];
            }
            return Value;
        }

        protected Int32 GetIdDriver(String _Driver)
        {
            Int32 Value = 0;
            DataClass.clsAllocationListDriver objAllocationListDriver = new DataClass.clsAllocationListDriver();
            DataTable dt = objAllocationListDriver.LoadList("Name = '" + _Driver + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdAllocationListDriver"];
            }
            return Value;
        }

        protected Int32 GetIdField(String _Field)
        {
            Int32 Value = 0;
            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dt = objFields.LoadList("Code = '" + _Field + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdField"];
            }
            return Value;
        }

        protected void EditAllocationListDriverLoad(Int32 _IdDriver)
        {
            DataClass.clsAllocationDriversByField objAllocationDriversByField = new DataClass.clsAllocationDriversByField();
            DataTable dt = objAllocationDriversByField.LoadList("IdModel = " + IdModel + " AND IdAllocationListDriver = " + _IdDriver, "");
            if (dt.Rows.Count > 0)
            {
                this.ddlFilterDriver.Value = Convert.ToInt32(dt.Rows[0]["IdAllocationListDriver"]);
            }
        }

        protected void LoadAllocationDriver()
        {
            DataClass.clsAllocationListDriver objAllocationListDriver = new DataClass.clsAllocationListDriver();
            this.storeFilterDriver.DataSource = objAllocationListDriver.LoadList("IdModel = " + IdModel + " AND Name LIKE '%" + ddlFilterDriver.Text + "%'", "ORDER BY Name");
            this.storeFilterDriver.DataBind();
            this.storeFilterDriver.Reload();
        }
        
        public void DeleteAll(String _Driver)
        {
            DataClass.clsAllocationDriversByField objAllocationDriversByField = new DataClass.clsAllocationDriversByField();
            objAllocationDriversByField.IdAllocationListDriver = (Int32)GetIdDriver(_Driver);
            objAllocationDriversByField.DeleteAll();
            this.ModelUpdateTimestamp();
        }

        public void DeleteAllByModel()
        {
            DataClass.clsAllocationDriversByField objAllocationDriversByField = new DataClass.clsAllocationDriversByField();
            objAllocationDriversByField.DeleteAllByModel(IdModel);
            this.ModelUpdateTimestamp();
            this.storeField.Reload();
        }

        [DirectMethod]
        public void ClickedDeleteYES(String _Driver)
        {
            this.DeleteAll(_Driver);
        }

        [DirectMethod]
        public void ClickedDeleteAllYES()
        {
            this.DeleteAllByModel();
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsAllocationDriversByField objAllocationDriversByField = new DataClass.clsAllocationDriversByField();
            DataTable dt = objAllocationDriversByField.LoadList("Name = '" + ddlFilterDriver.Text + "'", "");
            Value = dt.Rows.Count;

            return Value;
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportAllocationDriversData.xls", "template");
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = modMain.strMasterAllocationDriversData;
            this.lblFilterDriver.Text=(String)GetGlobalResourceObject("CPM_Resources", "Driver") + ":";
            this.btnSave.Text=modMain.strCommonSaveChanges;
            this.btnSave.ToolTip=modMain.strCommonSaveChanges;
            this.btnDelete.Text = modMain.strCommonDeleteAll;
            this.btnDelete.ToolTip = modMain.strCommonDeleteAll;
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.colName.Text = modMain.strCommonName;
            this.columAmount.Text = (String)GetGlobalResourceObject("CPM_Resources", "Amount");
            this.winImport.Title = modMain.strCommonImport + " " + modMain.strMasterAllocationDriversData;
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnImportFile.ToolTip = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.btnCancelImport.ToolTip = modMain.strCommonClose;
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportAllocationDriversByField;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=2.3.3 ExportAllocationDriversData.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        #endregion

    }
}
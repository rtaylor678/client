﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="mappingziffvsclient.aspx.cs" Inherits="CPM._mappingziffvsclient" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function applyPageSize(grid) {
            var headerHeight = 22;
            var rowHeight = 21;

            var gridTopOffset = 100;
            var gridBottomOffset = 50;
            var rowHeight = 20;
            var myHeight = window.innerHeight;

            grid.store.pageSize = Math.round(((myHeight - gridTopOffset - gridBottomOffset) / rowHeight)); // - 3);
            grid.store.load();
        }
    </script>
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:GridPanel ID="GridPanel2" runat="server" Title="Mapping (Company Structure vs. Ziff/Third-party Categories)" IconCls="icon-mapping_16" AutoScroll="true" Border="true" Header="true">
                <TopBar>
                    <ext:Toolbar ID="Toolbar1" runat="server">
                        <Items>
                            <ext:Button ID="btnDelete" Icon="Delete" ToolTip="Unset All" runat="server" Text="Unset All">
                                <DirectEvents>
                                    <Click OnEvent="btnDeleteAll_Click">
                                        <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:ToolbarSeparator />
                            <ext:Button ID="btnImport" Icon="BookAdd" runat="server" Text="Import">
                                <DirectEvents>
                                    <Click OnEvent="btnImport_Click">
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:ToolbarFill />
                            <ext:Button ID="btnSaveExcel" runat="server" Text="Export Data To Excel" Icon="PageExcel">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveExcel_Click" IsUpload="true">
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnDownloadTemplate" runat="server" Text="Download Import Template" Icon="PageSave">
                                <DirectEvents>
                                    <Click OnEvent="btnDownloadTemplate_Click" IsUpload="true">
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <Store>
                    <ext:Store ID="storeMappZiffVsClient" runat="server">
                        <Model>
                            <ext:Model ID="Model4" runat="server">
                                <Fields>
                                    <ext:ModelField Name="IdClientAccount" />
                                    <ext:ModelField Name="IdClientCostCenter" />
                                    <ext:ModelField Name="Account" />
                                    <ext:ModelField Name="Name" />
                                    <ext:ModelField Name="IdResources" />
                                    <ext:ModelField Name="ResourceName" />
                                    <ext:ModelField Name="IdField" />
                                    <ext:ModelField Name="ClientAccount" />
                                    <ext:ModelField Name="CliGenAccountDesc" />
                                    <ext:ModelField Name="IdZiffAccount" />
                                    <ext:ModelField Name="ZiffCode" />
                                    <ext:ModelField Name="ZiffName" />
                                    <ext:ModelField Name="Amount" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel>
                    <Columns>
                        <ext:Column ID="Column8" runat="server" Text="Company Cost Object & Account" DataIndex="ClientAccount" Flex="1" />
                        <ext:Column ID="Column9" runat="server" Text="Company Cost Object & Account Description" DataIndex="CliGenAccountDesc" Flex="1" />
                        <ext:NumberColumn ID="colBaseCost" runat="server" Format="0,000.00" Text="Base Cost Data" DataIndex="Amount" Flex="1" Align="Right" MaxWidth="200" />
                        <ext:Column ID="Column10" runat="server" Text="Ziff Account" DataIndex="ZiffCode" Flex="1" />
                        <ext:Column ID="Column11" runat="server" Text="Ziff Account Description" DataIndex="ZiffName" Flex="1" />
                        <ext:ImageCommandColumn Align="Center" Text="Functions" ID="ImageCommandColumn1" runat="server">
                            <Commands>
                                <ext:ImageCommand CommandName="Set" Icon="WorldGo" Text="&nbsp;Set" />
                            </Commands>
                            <DirectEvents>
                                <Command OnEvent="grdMappingZiffVSClient_Command">
                                    <ExtraParams>
                                        <ext:Parameter Name="command" Value="command" Mode="Raw">
                                        </ext:Parameter>
                                        <ext:Parameter Name="IdClientCostCenter" Value="record.data.IdClientCostCenter" Mode="Raw">
                                        </ext:Parameter>
                                        <ext:Parameter Name="IdClientAccount" Value="record.data.IdClientAccount" Mode="Raw">
                                        </ext:Parameter>
                                        <ext:Parameter Name="ClientAccount" Value="record.data.ClientAccount" Mode="Raw">
                                        </ext:Parameter>
                                        <ext:Parameter Name="CliGenAccountDesc" Value="record.data.CliGenAccountDesc" Mode="Raw">
                                        </ext:Parameter>
                                        <ext:Parameter Name="IdZiffAccount" Value="record.data.IdZiffAccount" Mode="Raw">
                                        </ext:Parameter>
                                    </ExtraParams>
                                </Command>
                            </DirectEvents>
                        </ext:ImageCommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" />
                </SelectionModel>
                <Features>
                    <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                        <Filters>
                            <ext:StringFilter DataIndex="ClientAccount" />
                            <ext:StringFilter DataIndex="CliGenAccountDesc" />
                            <ext:NumericFilter DataIndex="Amount" />
                            <ext:StringFilter DataIndex="ZiffCode" />
                            <ext:StringFilter DataIndex="ZiffName" />
                        </Filters>
                    </ext:GridFilters>
                </Features>
                <BottomBar>
                    <ext:PagingToolbar ID="PagingToolbar1" runat="server" HideRefresh="true">
                    </ext:PagingToolbar>
                </BottomBar>
                <Listeners>
                    <Resize Handler="applyPageSize(this)" Buffer="250" />            
                </Listeners>
            </ext:GridPanel>
            <ext:Window ID="winSetMap" runat="server" Title="Ziff/Third-party Base Cost" Hidden="true" IconCls="icon-mapping_16" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                <Items>
                    <ext:FormPanel ID="pnlCostCenterEdit" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                        <Items>
                            <ext:Panel ID="Panel6" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                <Defaults>
                                    <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                    <ext:Parameter Name="MsgTarget" Value="side" />
                                </Defaults>
                                <Items>
                                    <ext:Hidden ID="txtIDCliAccount" FieldLabel="ID Company Account" runat="server" Cls="PopupFormField" />
                                    <ext:Hidden ID="txtIDCliCostCenter" runat="server" />
                                    <ext:TextField ID="txtCliAccount" FieldLabel="Company Account" Hidden="false" ReadOnly="true" runat="server" Cls="PopupFormField" />
                                    <ext:TextField ID="txtCliAccountDes" FieldLabel="Company Gen.Account Desc." Hidden="false" ReadOnly="true" runat="server" Cls="PopupFormField" />
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="Panel2" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" LabelAlign="Top" Cls="PopupFormColumnPanel">
                                <Defaults>
                                    <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                    <ext:Parameter Name="MsgTarget" Value="side" />
                                </Defaults>
                                <Items>
                                    <ext:ComboBox ID="ddlZiffAccount" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" FieldLabel="Ziff Account" ValueField="IdZiffAccount" Cls="PopupFormField">
                                        <ListConfig LoadingText="Searching...">
                                            <ItemTpl ID="ItemTpl3" runat="server">
                                                <Html>
                                                    <div class="search-item">
							                            <h3><span>{Code}</span> - {Name}</h3>
						                            </div>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Store>
                                            <ext:Store ID="storeZiffAccounts" runat="server" OnReadData="ZiffAccounts_ReadData">
                                                <Model>
                                                    <ext:Model ID="Model3" runat="server" IDProperty="IdZiffAccount">
                                                        <Fields>
                                                            <ext:ModelField Name="IdZiffAccount" />
                                                            <ext:ModelField Name="Code" />
                                                            <ext:ModelField Name="Name" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                                <Proxy>
                                                    <ext:PageProxy>
                                                        <Reader>
                                                            <ext:JsonReader />
                                                        </Reader>
                                                    </ext:PageProxy>
                                                </Proxy>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </Items>
                            </ext:Panel>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSave" runat="server" Text="Save" Icon="DatabaseSave" Disabled="true" FormBind="true">
                                <DirectEvents>
                                    <Click OnEvent="btnSaveMap_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnCancel" runat="server" Icon="Decline" Text="Close">
                                <DirectEvents>
                                    <Click OnEvent="btnCancelMap_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                        <BottomBar>
                            <ext:StatusBar ID="FormStatusBar" runat="server" />
                        </BottomBar>
                        <Listeners>
                            <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                            });
                                                            #{btnSave}.setDisabled(!valid);" />
                        </Listeners>
                    </ext:FormPanel>
                </Items>
            </ext:Window>
            <ext:Window ID="winImport" runat="server" Title="Import Mapping (Company Structure vs. Ziff/Third-party Categories)" Hidden="true" Icon="ArrowLeft" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                <Items>
                    <ext:FormPanel ID="frmImportFile" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                        <Items>
                            <ext:Panel ID="pnlImportFile1" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                <Defaults>
                                    <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                    <ext:Parameter Name="MsgTarget" Value="side" />
                                </Defaults>
                                <Items>
                                    <ext:FileUploadField ID="fileImport" FieldLabel="File" runat="server" Icon="Attach" Cls="PopupFormField" />
                                </Items>
                            </ext:Panel>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnImportFile" runat="server" Text="Import" Icon="TableRefresh" Disabled="true" FormBind="true">
                                <DirectEvents>
                                    <Click OnEvent="btnImportFile_Click">
                                        <EventMask ShowMask="true" Target="CustomTarget" CustomTarget="vpMain" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnCancelImport" runat="server" Icon="Decline" Text="Close">
                                <DirectEvents>
                                    <Click OnEvent="btnCancelImport_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                        <BottomBar>
                            <ext:StatusBar ID="FormImportStatusBar" runat="server" />
                        </BottomBar>
                        <Listeners>
                            <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                            });
                                            #{btnImportFile}.setDisabled(!valid);" />
                        </Listeners>
                    </ext:FormPanel>
                </Items>
            </ext:Window>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>

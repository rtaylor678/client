﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="case.aspx.cs" Inherits="CPM._case" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_iconxl.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_menu.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color transparent;" Layout="BorderLayout" MinWidth="800">
        <Items>
            <ext:Panel ID="pnlTitlePanel" runat="server" Region="North" Border="false" Header="false" BodyStyle="background-color: transparent;" Margins="0 0 0 0" ButtonAlign="Right">
                <TopBar>
                    <ext:Toolbar ID="tbMenu" runat="server" MinWidth="800" Height="25">
                        <Items>
                            <ext:ToolbarFill />
                            <ext:Label Icon="Time" ID="lblLastLogin" Text="Last Login" runat="server">
                            </ext:Label>
                            <ext:ToolbarSeparator runat="server" ID="sepLastLogin" Cls="WideSeparator" />
                            <ext:Button ID="btnMainUser" runat="server" Icon="User" Text="User">
                                <Menu>
                                    <ext:Menu ID="Menu6" runat="server">
                                        <Items>
                                            <ext:MenuItem ID="btnAdmin" runat="server" Icon="UserEdit" Text="Admin">
                                                <DirectEvents>
                                                    <Click OnEvent="btnAdmin_Click">
                                                    </Click>
                                                </DirectEvents>
                                            </ext:MenuItem>
                                            <ext:MenuItem ID="btnSetting" runat="server" Text="Roles and Permissions" Icon="TableKey">
                                                <DirectEvents>
                                                    <Click OnEvent="btnSetting_Click">
                                                    </Click>
                                                </DirectEvents>
                                            </ext:MenuItem>
                                            <ext:MenuItem ID="btnUser" runat="server" Icon="Group" Text="User Management">
                                                <DirectEvents>
                                                    <Click OnEvent="btnUser_Click">
                                                    </Click>
                                                </DirectEvents>
                                            </ext:MenuItem>
                                            <ext:MenuSeparator ID="MenuSeparator1" runat="server" />
                                            <ext:MenuItem ID="btnLogon" Text="log on" Icon="KeyDelete">
                                                <DirectEvents>
                                                    <Click OnEvent="btnLogon_Click">
                                                    </Click>
                                                </DirectEvents>
                                            </ext:MenuItem>
                                        </Items>
                                    </ext:Menu>
                                </Menu>
                            </ext:Button>
                            <ext:ToolbarSeparator runat="server" ID="ToolbarSeparator1" Cls="WideSeparator" />
                            <ext:Button ID="btnHelp" runat="server" Icon="Help" Text="Help">
                                <DirectEvents>
                                    <Click OnEvent="btnHelp_Click" IsUpload="true" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:ToolbarSeparator runat="server" ID="ToolbarSeparator2" Cls="WideSeparator" />
                            <ext:Button ID="btnRepositories" runat="server" Icon="BasketAdd" Text="Files Repository">
                                <DirectEvents>
                                    <Click OnEvent="btnRepositories_Click" IsUpload="true" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <BottomBar>
                    <ext:Toolbar Height="40" ID="tbMenuTop" runat="server" Flat="true" Cls="menubackground">
                        <Items>
                            <ext:ToolbarSeparator runat="server" ID="sepParameters" Border="false" Width="2" />
                            <ext:Button ID="btnParameterModule" runat="server" Disabled="true" ToolTip="Parameters Module" Text="<b>Parameters</b>" IconCls="menu-parameters" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnParameterModule_Click">
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:ToolbarSeparator runat="server" ID="sepAllocation" />
                            <ext:Button ID="btnAllocationModule" runat="server" Disabled="true" ToolTip="Cost Benchmark Module" Text="<b>Cost Benchmark</b>" IconCls="menu-allocation" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnAllocationModule_Click">
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:ToolbarSeparator runat="server" ID="sepTechnical" />
                            <ext:Button ID="btnTechnicalModule" runat="server" Disabled="true" ToolTip="Technical Module" Text="<b>Technical</b>" IconCls="menu-technical" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnTechnicalModule_Click">
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:ToolbarSeparator runat="server" ID="sepEconomic" Visible="false"/>
                            <ext:Button ID="btnEconomicModule" runat="server" Disabled="true" ToolTip="Economic Module" Text="<b>Economic</b>" IconCls="menu-economic" Scale="Large" visible="false">
                                <DirectEvents>
                                    <Click OnEvent="btnEconomicModule_Click">
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:ToolbarSeparator runat="server" ID="sepReports" Cls="WideSeparator" />
                            <ext:Button ID="btnReportModule" runat="server" Disabled="true" ToolTip="Reports Module" Text="<b>Reports</b>" IconCls="menu-reports" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnReportModule_Click">
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:ToolbarFill />
                            <ext:Button ID="btnModels" runat="server" Disabled="false" ToolTip="Case Selection" Text="<b>Case Selection</b>" IconCls="menu-modelselection" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnModel_Click">
                                        <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:ToolbarSeparator runat="server" ID="sepModels" Border="false" Width="2" />
                        </Items>
                    </ext:Toolbar>
                </BottomBar>
                <Items>
                    <ext:Panel ID="pnlHeader" runat="server" Margins="0 0 0 0" Cls="image-banner">
                        <Content>
                            <div id="HeadPanel">
                                <div id="HeadLeft"></div>
                                <div id="HeadRight"></div>
                                <div id="HeadCenter" class="x-panel-modelname"><ext:Label ID="lblModelName" Cls="x-text-modelname" Text="" runat="server"></ext:Label></div>
                            </div>
                        </Content>
                    </ext:Panel>
                </Items>
            </ext:Panel>
            <ext:Panel runat="server" Region="West" Title="Modules" Width="240" ID="pnlSettings" BodyStyle="background-color: #d8d8d8;" Collapsible="true" HeaderPosition="Top" Split="true" MinWidth="175" MaxWidth="400" AnimCollapse="true" Layout="AccordionLayout">
                <Items>
                    <ext:MenuPanel ID="mnuParameter" runat="server" Disabled="true" Title="Parameters" IconCls="icon-parameters_16">
                        <DirectEvents>
                            <Expand OnEvent="btnParameterModule_Click"></Expand>
                        </DirectEvents>
                        <Menu ID="mnuParameterItems" runat="server" Cls="x-my-menu">
                            <Items>
                                <ext:MenuItem ID="btnAggregationLevels" Text="Aggregation Levels" IconCls="icon-aggregation_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnAggregationLevel_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnOperations" Text="Types of Operations" IconCls="icon-operations_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnTypeOperation_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnFields" Text="List of Fields" IconCls="icon-fields_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnField_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnProjects" Text="Field Projects" IconCls="icon-projects_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnProjects_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnCurrency" Text="Currency" IconCls="icon-currency_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnCurrencies_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuSeparator></ext:MenuSeparator>
                                <ext:MenuItem ID="btnZiffStructure" Text="Cost Projection Categories" IconCls="icon-ziffstructure_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnZiffStructure_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                            </Items>
                        </Menu>
                    </ext:MenuPanel>
                    <ext:MenuPanel ID="mnuAllocation" runat="server" Disabled="true" Title="Allocation" IconCls="icon-allocation_16">
                        <DirectEvents>
                            <Expand OnEvent="btnAllocationModule_Click"></Expand>
                        </DirectEvents>
                        <Menu ID="mnuAllocationItems" runat="server" Cls="x-my-menu">
                            <Items>
                                <ext:MenuItem ID="btnClientReferences" Text="Internal Cost References" IconCls="icon-clientreferences_16" runat="server" Disabled="true" MenuHideDelay="5">
                                    <DirectEvents>
                                        <Click OnEvent="btnClientReferences_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                    <Menu>
                                        <ext:Menu ID="btnClientReferencesItems" runat="server">
                                            <Listeners>
                                                <Show Handler="this.getEl().on('mouseleave', function() { this.hide(); })" Single="true" />
                                                <%--  <Hide Handler="Ext.get('mnuAllocation').DeactivateActiveItem();" Single="true" /> --%>
                                            </Listeners>
                                            <Items>
                                                <ext:MenuItem ID="btnActivities" Text="Activities" IconCls="icon-activities_16" runat="server" Disabled="true">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnActivities_Click">
                                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:MenuItem>
                                                <ext:MenuItem ID="btnResources" Text="Resources" IconCls="icon-resources_16" runat="server" Disabled="true">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnResources_Click">
                                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:MenuItem>
                                                <ext:MenuItem ID="btnCostCenters" Text="Cost Objects" IconCls="icon-clientcostcenters_16" runat="server" Disabled="true">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnClientCostCenters_Click">
                                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:MenuItem>
                                                <ext:MenuItem ID="btnClientAccounts" Text="Chart of Accounts" IconCls="icon-clientaccounts_16" runat="server" Disabled="true">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnClientAccounts_Click">
                                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:MenuItem>
                                                <ext:MenuSeparator></ext:MenuSeparator>
                                                <ext:MenuItem ID="btnClientCostData" Text="Base Cost Data" IconCls="icon-clientcostdata_16" runat="server" Disabled="true">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnClientCostData_Click">
                                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:MenuItem>
                                                <ext:MenuSeparator></ext:MenuSeparator>
                                                <ext:MenuItem ID="btnDirect" Text="Direct" IconCls="icon-direct_16" runat="server" Disabled="true">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnDirect_Click">
                                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:MenuItem>
                                                <ext:MenuItem ID="btnShare" Text="Shared" IconCls="icon-share_16" runat="server" Disabled="true">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnShare_Click">
                                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:MenuItem>
                                                <ext:MenuItem ID="btnHierarchy" Text="Hierarchy" IconCls="icon-hierarchy_16" runat="server" Disabled="true">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnHierarchy_Click">
                                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:MenuItem>
                                                <ext:MenuSeparator></ext:MenuSeparator>
                                                <ext:MenuItem ID="btnListDrivers" Text="List of Allocation Drivers" IconCls="icon-listallocationdrivers_16" runat="server" Disabled="true">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnListDrivers_Click">
                                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:MenuItem>
                                                <ext:MenuItem ID="btnAllocationDrivers" Text="Allocation Drivers Criteria" IconCls="icon-allocationdrivercost_16" runat="server" Disabled="true">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnAllocationDriversByCostCenter_Click">
                                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:MenuItem>
                                                <ext:MenuItem ID="btnAllocationDriversData" Text="Allocation Drivers Data" IconCls="icon-allocationdriverfield_16" runat="server" Disabled="true">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnAllocationDriversData_Click">
                                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:MenuItem>
                                                <ext:MenuSeparator></ext:MenuSeparator>
                                                <ext:MenuItem ID="btnZiffMapping" Text="Mapping (Company Cost Structure vs. Cost Projection Categories)" IconCls="icon-mapping_16" runat="server" Disabled="true">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnZiffMapping_Click">
                                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:MenuItem>
                                                <ext:MenuItem ID="btnBaseCostByField" Text="Company Base Cost" IconCls="icon-clientcostfieldclient_16" runat="server" Disabled="true">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnBaseCostByField_Click">
                                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:MenuItem>
                                                <ext:MenuItem ID="btnBaseCostByFieldZiff" Text="Base Cost by Field (Cost Projection Categories)" IconCls="icon-clientcostfieldziff_16" runat="server" Disabled="true" Visible="false">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnBaseCostByFieldZiff_Click">
                                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:MenuItem>
                                                <%--<ext:MenuSeparator></ext:MenuSeparator>--%>
                                                <ext:MenuItem ID="btnInternalBenchmarkRepository" Text="Internal Benchmarks Files Repository" IconCls="icon-zifftechnicalanalysis_16" runat="server" Disabled="true" Visible="false">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnInternalBenchmarkRepository_Click">
                                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:MenuItem>
                                            </Items>
                                        </ext:Menu>
                                    </Menu>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnZiffReferences" Text="External Cost References" IconCls="icon-ziffreferences_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnZiffReferences_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                    <Menu>
                                        <ext:Menu ID="btnZiffReferencesItems" runat="server">
                                            <Listeners>
                                                <Show Handler="this.getEl().on('mouseleave', function() { this.hide(); })" Single="true" />
                                            </Listeners>
                                            <Items>
                                                <ext:MenuItem ID="btnListOfPeerGroup" Text="List of Peer Group" IconCls="icon-listpeercriteria_16" runat="server" Disabled="true">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnListOfPeerGroup_Click">
                                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:MenuItem>
                                                <ext:MenuItem ID="btnZiffListOfCriteria" Text="List of Peer Criteria" IconCls="icon-listpeercriteria_16" runat="server" Disabled="true">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnZiffListOfCriteria_Click">
                                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:MenuItem>
                                                <ext:MenuItem ID="btnZiffPeerCriteriaData" Text="Peer Criteria Data" IconCls="icon-peercriteriadata_16" runat="server" Disabled="true">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnZiffPeerCriteriaData_Click">
                                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:MenuItem>
                                                <ext:MenuItem ID="btnZiffBaseCost" Text="External Base Cost References" IconCls="icon-ziffbasecost_16" runat="server" Disabled="true">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnZiffBaseCost_Click">
                                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:MenuItem>
                                                <%--<ext:MenuSeparator></ext:MenuSeparator>--%>
                                                <ext:MenuItem ID="btnExternalBenchmarkRepository" Text="External Benchmarks Files Repository" IconCls="icon-zifftechnicalanalysis_16" runat="server" Disabled="true" Visible="false">
                                                    <DirectEvents>
                                                        <Click OnEvent="btnExternalBenchmarkRepository_Click">
                                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:MenuItem>
                                            </Items>
                                        </ext:Menu>
                                    </Menu>
                                </ext:MenuItem>
                                <ext:MenuSeparator></ext:MenuSeparator>
                                <ext:MenuItem ID="btnCostBenchmarkingAllocation" Text="Cost Benchmarking" IconCls="icon-opex_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnCostBenchmarking_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                            </Items>
                        </Menu>
                    </ext:MenuPanel>
                    <ext:MenuPanel ID="mnuTechnical" runat="server" Disabled="true" Title="Technical" IconCls="icon-technical_16">
                        <DirectEvents>
                            <Expand OnEvent="btnTechnicalModule_Click">
                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                            </Expand>
                        </DirectEvents>
                        <Menu ID="mnuTechnicalItems" runat="server" Cls="x-my-menu">
                            <Items>
                                <ext:MenuItem ID="btnListTechnicaDrivers" Text="List of Technical Drivers" IconCls="icon-technicaldrivers_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnListTechnicaDrivers_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnTechnicalDriversData" Text="Technical Drivers Data" IconCls="icon-technicaldriversdata_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnTechnicalDriversData_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnTechnicalAssumptions" Text="Technical Assumptions" IconCls="icon-technicalassumptions_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnTechnicalAssumptions_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnUtilizationCapacity" Text="Utilization Capacity" IconCls="icon-technicaldrivers_16" runat="server" Disabled="true" >
                                    <DirectEvents>
                                        <Click OnEvent="btnUtilizationCapacity_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnBaseCostTechnicalModule" Text="Base Cost Rates" IconCls="icon-basecostrates_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnBaseCostTechnicalModule_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnBaseRateByDriversTechnicalModule" Text="Base Rates By Drivers" IconCls="icon-basecostrates_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnBaseRatesByDrivers_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnZiffTechnicalAnalysis" Text="Ziff Technical Analysis" IconCls="icon-zifftechnicalanalysis_16" runat="server" Disabled="true" Visible="false">
                                    <DirectEvents>
                                        <Click OnEvent="btnZiffTechnicalAnalysis_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuSeparator></ext:MenuSeparator>
                                <ext:MenuItem ID="btnCostStructureAssumptions" Text="Cost Structure Assumptions" IconCls="icon-coststructure_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnCostStructureAssumptions_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnPriceScenarios" Text="Price Scenarios" IconCls="icon-currency_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnPriceScenarios_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnPrices" Text="Prices" IconCls="icon-currency_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnPrices_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnEconomicDrivers" Text="List of Economic Drivers" IconCls="icon-economicdrivers_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnEconomicDrivers_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnEconomicDriverData" Text="Economic Drivers Data" IconCls="icon-economicdriversdata_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnEconomicDriverData_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnEconomicAssumptions" Text="Economic Assumptions" IconCls="icon-economicassumptions_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnEconomicAssumptions_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnPriceForecast" Text="Base Cost Rates Forecast" IconCls="icon-pricesforecast_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnPriceForecast_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuSeparator></ext:MenuSeparator>
                                <ext:MenuItem ID="btnOperationalMarginsProjectionCPM" Text="Operational Margins Projection" IconCls="icon-opex_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnOperationalMarginsProjection_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnCostvsDriversProjectionCPM" Text="Cost vs Drivers Projection" IconCls="icon-opex_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnCostvsDriversProjection_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnOpexReportCPM" Text="Projection by Cost Category" IconCls="icon-opex_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnOpexReport_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnOpexReportDetailedCPM" Text="Projection by Case" IconCls="icon-opex_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnOpexReportDetailed_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnMultiFieldsProjectionCPM" Text="Multiple Fields Projection" IconCls="icon-opex_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnMultiFieldsProjection_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                           </Items>
                        </Menu>
                    </ext:MenuPanel>
                    <ext:MenuPanel ID="mnuEconomic" runat="server" Disabled="true" Title="Economic" IconCls="icon-economic_16" Visible="false">
                        <DirectEvents>
                            <Expand OnEvent="btnEconomicModule_Click">
                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                            </Expand>
                        </DirectEvents>
                        <Menu ID="mnuEconomicItems" runat="server" Cls="x-my-menu">
                            <Items>
                                <ext:MenuItem ID="btnZiffEconomicAnalysis" Text="Ziff Economic Analysis" IconCls="icon-ziffeconomicanalysis_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnZiffEconomicAnalysis_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>                                
                            </Items>
                        </Menu>
                    </ext:MenuPanel>
                    <ext:MenuPanel ID="mnuReport" runat="server" Disabled="true" Title="Reports" IconCls="icon-reports_16">
                        <DirectEvents>
                            <Expand OnEvent="btnReportModule_Click">
                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                            </Expand>
                        </DirectEvents>
                        <Menu ID="mnuReportItems" runat="server" Cls="x-my-menu">
                            <Items>
                                <ext:MenuItem ID="btnOperationalMarginsProjection" Text="Operational Margins Projection" IconCls="icon-opex_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnOperationalMarginsProjection_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnCostvsDriversProjection" Text="Cost vs Drivers Projection" IconCls="icon-opex_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnCostvsDriversProjection_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnOpexReport" Text="Projection by Cost Category" IconCls="icon-opex_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnOpexReport_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnOpexReportDetailed" Text="Projection by Case" IconCls="icon-opex_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnOpexReportDetailed_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnMultiFieldsProjection" Text="Multiple Fields Projection" IconCls="icon-opex_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnMultiFieldsProjection_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuSeparator></ext:MenuSeparator>
                                <ext:MenuItem ID="btnDriverReport" Text="Technical Drivers Forecast" IconCls="icon-driverprofiles_16" runat="server" Disabled="true" Visible="false">
                                    <DirectEvents>
                                        <Click OnEvent="btnDriverReport_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnKPIReport" Text="KPI Estimates" IconCls="icon-kpitarget_16" runat="server" Disabled="true" visible="false">
                                    <DirectEvents>
                                        <Click OnEvent="btnKPIReport_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnBaseCostByFieldReport" Text="Base Cost by Field (Company Structure)" IconCls="icon-clientcostfieldclient_16" runat="server" Disabled="true" Visible="false">
                                    <DirectEvents>
                                        <Click OnEvent="btnBaseCostByField_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnBaseCostByFieldZiffReport" Text="Base Cost by Field (Cost Projection Categories)" IconCls="icon-clientcostfieldziff_16" runat="server" Disabled="true" Visible="false">
                                    <DirectEvents>
                                        <Click OnEvent="btnBaseCostByFieldZiff_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnBaseCostTechnicalModuleReport" Text="Base Rates By Cost Category" IconCls="icon-basecostrates_16" runat="server" Disabled="true" Visible="false">
                                    <DirectEvents>
                                        <Click OnEvent="btnBaseCostTechnicalModule_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnBaseRatesByDrivers" Text="Base Rates By Drivers" IconCls="icon-basecostrates_16" runat="server" Disabled="true" Visible="false">
                                    <DirectEvents>
                                        <Click OnEvent="btnBaseRatesByDrivers_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
<%--                                <ext:MenuSeparator></ext:MenuSeparator>--%>
                                <ext:MenuItem ID="btnBaseCostByFieldReportMerged" Text="Internal Base Cost References" IconCls="icon-clientcostfieldclient_16" runat="server" Disabled="true" Visible="false">
                                    <DirectEvents>
                                        <Click OnEvent="btnBaseCostByField_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnZiffBaseCostReport" Text="External Base Cost References" IconCls="icon-ziffbasecost_16" runat="server" Disabled="true" Visible="false">
                                    <DirectEvents>
                                        <Click OnEvent="btnZiffBaseCost_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                                <ext:MenuItem ID="btnCostBenchmarking" Text="Cost Benchmarking" IconCls="icon-opex_16" runat="server" Disabled="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnCostBenchmarking_Click">
                                            <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                        </Click>
                                    </DirectEvents>
                                </ext:MenuItem>
                            </Items>
                        </Menu>
                    </ext:MenuPanel>
                </Items>
            </ext:Panel>
            <ext:Panel ID="pnlBody" AnimCollapse="true" AutoFocus="false" runat="server" Region="Center" CollapseMode="Default" Layout="FormLayout" Margins="1 1 1 1" Closable="false">
                <Loader runat="server" AutoLoad="true" DisableCaching="true" Mode="Frame" Url="">
                </Loader>
            </ext:Panel>
            <ext:Panel ID="pnlFooter" runat="server" Region="South" Resizable="false" Header="false" Margins="5 1 1 1" Collapsible="false">
                <FooterBar>
                    <ext:Toolbar ID="StatusBar1" runat="server">
                        <Items>
                            <ext:Label ID="lblVersion" runat="server" Text=" " Width="100" Style="text-align:left;padding-left:5px;" />
                            <ext:Label ID="lblCopyright" runat="server" Text="© HSB Solomon Associates LLC" Flex="1" Style="text-align:center;" />
                            <ext:ToolbarTextItem ID="clock" runat="server" Text=" " Width="100" Style="text-align:right;padding-right:5px;" CtCls="x-status-text-panel" />
                        </Items>
                    </ext:Toolbar>
                </FooterBar>
            </ext:Panel>
            <ext:Window ID="winUserEdit" runat="server" Title="User" Hidden="true" Icon="UserEdit" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                <Items>
                    <ext:FormPanel ID="pnlUser" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                        <Items>
                            <ext:Panel ID="Panel2" runat="server" Border="false" Header="false" ColumnWidth=".5" Layout="Form" Cls="PopupFormColumnPanel">
                                <Defaults>
                                    <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                    <ext:Parameter Name="MsgTarget" Value="side" />
                                </Defaults>
                                <Items>
                                    <ext:TextField ID="txtUserName" runat="server" LabelWidth="75" FieldLabel="Username" AnchorHorizontal="92%" Cls="PopupFormField" ReadOnly="true" />
                                    <ext:TextField ID="txtPassword" runat="server" LabelWidth="75" FieldLabel="Password" AnchorHorizontal="92%" InputType="Password" Cls="PopupFormField" />
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="Panel3" runat="server" Border="false" Layout="Form" ColumnWidth=".5" Cls="PopupFormColumnPanel">
                                <Defaults>
                                    <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                    <ext:Parameter Name="MsgTarget" Value="side" />
                                </Defaults>
                                <Items>
                                    <ext:TextField ID="txtName" runat="server" LabelWidth="75" FieldLabel="Name" AnchorHorizontal="92%" Cls="PopupFormField" ReadOnly="true" />
                                    <ext:TextField ID="txtEmail" runat="server" LabelWidth="75" FieldLabel="Email" AnchorHorizontal="92%" Vtype="email" Cls="PopupFormField" ReadOnly="true" />
                                </Items>
                            </ext:Panel>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSave" runat="server" Text="Save" Icon="DatabaseSave" Disabled="true" FormBind="true">
                                <DirectEvents>
                                    <Click OnEvent="btnSave_Click">
                                        <EventMask ShowMask="true" Target="CustomTarget" CustomTarget="vpMain" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnCancel" runat="server" Icon="Decline" Text="Cancel">
                                <DirectEvents>
                                    <Click OnEvent="btnCancel_Click">
                                        <EventMask ShowMask="true" Target="CustomTarget" CustomTarget="vpMain" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                        <BottomBar>
                            <ext:StatusBar ID="FormStatusBar" runat="server" />
                        </BottomBar>
                        <Listeners>
                            <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                         text : valid ? 'Form is valid' : 'Form is invalid', 
                                                         iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                     });
                                                     #{btnSave}.setDisabled(!valid);" />
                        </Listeners>
                    </ext:FormPanel>
                </Items>
            </ext:Window>
        </Items>
    </ext:Viewport>
    <ext:TaskManager ID="TaskManager1" runat="server">
        <Tasks>
            <ext:Task AutoRun="true" Interval="1000">
                <Listeners>
                    <Update Handler="#{clock}.setText(Ext.Date.format(new Date(), 'g:i:s A'));" />
                </Listeners>
            </ext:Task>
        </Tasks>
    </ext:TaskManager>
    </form>
</body>
</html>
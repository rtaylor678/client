﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Xml;
using CD;
using DataClass;
using System.Globalization;

namespace CPM
{     
    public partial class _users : System.Web.UI.Page
    {

        #region Definitions

        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdUserForm { get { return (Int32)Session["IdUserForm"]; } set { Session["IdUserForm"] = value; } }
        public String NameUser { get { return (String)Session["NameUser"]; } set { Session["NameUser"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        public DateTime LastLogin { get { return (DateTime)Session["LastLogin"]; } set { Session["LastLogin"] = value; } }

        #endregion

        #region '" Enumerator Declaration "'
        public enum PasswordScore
        {
            Blank = 0,
            TooShort = 1,
            RequirementsNotMet = 2,
            VeryWeak = 3,
            Weak = 4,
            Fair = 5,
            Medium = 6,
            Strong = 7,
            VeryStrong = 8,
            Requirement1NotMet = 9,
            Requirement2NotMet = 10,
            Requirement3NotMet = 11,
            Requirement4NotMet = 12
        }
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                this.LoadUser();
                this.LoadLanguage();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winUserEdit.Show();
        }

        protected void btnModel_Click(object sender, DirectEventArgs e)
        {
            Response.Redirect("/caseselection.aspx");
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            this.SaveUser(Convert.ToInt32(IdUserForm));
            this.LoadUser();
        }

        protected void CheckField(object sender, RemoteValidationEventArgs e)
        {
            if (this.txtPassword.Text == this.txtRepeatPassword.Text)
            {
                e.Success = true;
            }
            else
            {
                e.Success = false;
                e.ErrorMessage = (String)GetGlobalResourceObject("CPM_Resources", "Passwords do not match").ToString();
            }

            System.Threading.Thread.Sleep(1000);
        }

        protected void CheckStrength(object sender, RemoteValidationEventArgs e)
        {
            DataClass.clsUsers objUsers = new DataClass.clsUsers();

            PasswordScore checkScore = (PasswordScore)objUsers.CheckStrength(this.txtPassword.Text);

            if (checkScore== PasswordScore.TooShort)
            {
                e.Success = false;
                e.ErrorMessage = "Password needs to be a minimum of 8 characters!!!";
            }
            else if (checkScore == PasswordScore.RequirementsNotMet)
            {
                e.Success = false;
                e.ErrorMessage = "Password requirements not met!!!";
            }
            else if (checkScore == PasswordScore.Requirement1NotMet)
            {
                e.Success = false;
                e.ErrorMessage = "Password needs 1 Numeric!!!";
            }
            else if (checkScore == PasswordScore.Requirement2NotMet)
            {
                e.Success = false;
                e.ErrorMessage = "Password needs 1 Lower Alpha!!!";
            }
            else if (checkScore == PasswordScore.Requirement3NotMet)
            {
                e.Success = false;
                e.ErrorMessage = "Password needs 1 Upper Alpha!!!";
            }
            else if (checkScore == PasswordScore.Requirement4NotMet)
            {
                e.Success = false;
                e.ErrorMessage = "Password needs 1 Special Character!!!";
            }
            else
            {
                e.Success = true;
            }
        }

        protected void grdUser_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            IdUserForm = int.Parse(e.ExtraParams["IdUser"].ToString());
            String _Name = (String)e.ExtraParams["Name"].ToString();
            if (Command == "Edit")
            {
                
                this.winUserEdit.Show();
                this.EditUserLoad(IdUserForm);
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + _Name + "?", new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
        }

        [DirectMethod]
        public void ClickedYES()
        {
            this.DeleteUser(IdUserForm);
            this.LoadUser();
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            this.winUserEdit.Hide();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void StoreProfile_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsProfiles objProfiles = new DataClass.clsProfiles();
            this.storeProfile.DataSource = objProfiles.LoadComboBox("", "ORDER BY Name");
            this.storeProfile.DataBind();
        }

        #endregion

        #region Methods

        protected void EditUserLoad(Int32 IdUser)
        {
            DataClass.clsUsers objUsers = new DataClass.clsUsers();
            CD.DAC DAC = new CD.DAC();
            DataTable dt = objUsers.LoadList("IdUser = " + IdUser, "");
            if (dt.Rows.Count > 0)
            {
                this.IdUserForm = IdUser;
                this.txtUserName.Text = (String)dt.Rows[0]["UserName"];
                this.txtName.Text = (String)dt.Rows[0]["Name"];
                this.txtPassword.Text = CD.DAC.Decrypt((String)dt.Rows[0]["Password"]);
                this.txtRepeatPassword.Text = CD.DAC.Decrypt((String)dt.Rows[0]["Password"]);
                this.txtEmail.Text = (String)dt.Rows[0]["Email"];
                this.ddlProfile.SetValue((Int32)dt.Rows[0]["IdProfile"]);
            }
        }

        protected void LoadUser()
        {
            DataClass.clsUsers objUser = new DataClass.clsUsers();
            DataTable dt = objUser.LoadList("", "ORDER BY Name");
            storeUsers.DataSource = dt;
            storeUsers.DataBind();
        }

        protected void Clear()
        {
            this.IdUserForm = 0;
            this.txtName.Text = "";
            this.txtName.Reset();
            this.txtUserName.Text = "";
            this.txtUserName.Reset();
            this.txtEmail.Text = "";
            this.txtEmail.Reset();
            this.txtPassword.Text = "";
            this.txtPassword.Reset();
            this.txtRepeatPassword.Text = "";
            this.txtRepeatPassword.Reset();
            this.ddlProfile.Reset();
        }

        protected void SaveUser(Int32 ID)
        {
            DataClass.clsUsers objUsers = new DataClass.clsUsers();
            CD.DAC DAC = new CD.DAC();

            objUsers.IdUser = (Int32)ID;
            objUsers.loadObject();

            if (ID == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (objUsers.UserName != txtUserName.Text)
                {
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }

            objUsers.UserName = this.txtUserName.Text;
            objUsers.Name = this.txtName.Text;
            objUsers.Password = CD.DAC.Encrypt(this.txtPassword.Text);
            objUsers.Email = this.txtEmail.Text;
            objUsers.LastLogin = DateTime.Now;
            objUsers.IdProfile = Convert.ToInt32(this.ddlProfile.SelectedItem.Value);
            
            if (ID == 0)
            {
                objUsers.DateCreation = DateTime.Now;
                objUsers.UserCreation = (Int32)IdUserCreate;
                IdUserForm = objUsers.Insert();
            }
            else
            {
                objUsers.DateModification = DateTime.Now;
                objUsers.UserModification = (Int32)IdUserCreate;
                objUsers.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winUserEdit.Hide();
        }

        public void DeleteUser(Int32 ID)
        {
            DataClass.clsUsers objUsers = new DataClass.clsUsers();

            objUsers.IdUser = (Int32)ID;
            objUsers.Delete();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","Record deleted successfully!"), IconCls = "icon-accept", Clear2 = false });
        }

        #endregion

        #region Funtions

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsUsers objUsers = new DataClass.clsUsers();
            DataTable dt = objUsers.LoadList("UserName = '" + txtUserName.Text + "'", "");
            Value = dt.Rows.Count;

            return Value;
        }

        protected String ProfileValue(Int32 IdProfile)
        {
            String Value = "";
            DataClass.clsProfiles objProfiles = new DataClass.clsProfiles();

            DataTable dt = objProfiles.LoadComboBox("IdProfile = " + IdProfile, "ORDER BY Name");

            if (dt.Rows.Count > 0)
            {
                Value = dt.Rows[0]["Name"].ToString();
            }
            return Value;
        }

        protected void LoadLanguage()
        {
            DataClass.clsLabelsLanguages objLabelsLanguages = new DataClass.clsLabelsLanguages();
            DataTable dt = objLabelsLanguages.LoadList("LanguageName = '" + Language + "' AND FormName = 'UserManagement'", "ORDER BY IdLabel");
            foreach (DataRow row in dt.Rows)
            {
                switch ((String)row["LabelName"])
                {
                    case "Add New User":
                        modMain.strUsersAddNewUser = (String)row["LabelText"];
                        break;
                    case "Case Selection":
                        modMain.strUsersCaseSelection = (String)row["LabelText"];
                        break;
                    case "Users":
                        modMain.strUsersUsers = (String)row["LabelText"];
                        break;
                    case "User Name":
                        modMain.strUsersUserName = (String)row["LabelText"];
                        break;
                    case "Email":
                        modMain.strUsersEmail = (String)row["LabelText"];
                        break;
                    case "Last Login":
                        modMain.strUsersLastLogin = (String)row["LabelText"];
                        break;
                    case "User":
                        modMain.strUsersUser = (String)row["LabelText"];
                        break;
                    case "Password":
                        modMain.strUsersPassword = (String)row["LabelText"];
                        break;
                    case "Repeat Password":
                        modMain.strUsersRepeatPassword = (String)row["LabelText"];
                        break;
                    case "Select Role":
                        modMain.strUsersSelectRole = (String)row["LabelText"];
                        break;
                }
            }
            this.LoadLabel();
        }

        protected void LoadLabel()
        {
            //TopBar
            this.btnAdd.Text = "<B>" + modMain.strUsersAddNewUser + "</B>"; 
            this.btnAdd.ToolTip = modMain.strUsersAddNewUser;
            this.btnModels.Text = "<B>" + modMain.strUsersCaseSelection + "</B>";
            this.btnModels.ToolTip = modMain.strUsersCaseSelection;

            //GridPanel
            this.grdUser.Title = modMain.strUsersUsers;
            this.UserName.Text = modMain.strUsersUserName;
            this.Name.Text = modMain.strCommonName;
            this.Email.Text = modMain.strUsersEmail;
            this.LastLogindt.Text = modMain.strUsersLastLogin;
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = modMain.strCommonEdit; //Edit option
            this.ImageCommandColumn1.Commands[1].Text = modMain.strCommonDelete; //Delete option

            //Window Pop Up
            this.winUserEdit.Title = modMain.strUsersUser;
            this.pnlUser.Title = modMain.strUsersUser;
            this.txtUserName.FieldLabel = modMain.strUsersUserName;
            this.txtPassword.FieldLabel = modMain.strUsersPassword;
            this.txtPassword.ToolTip = "Password: Minimum 8 characters (1 Upper Alpha, 1 Lower Alpha, 1 Numeric, and 1 Special Character)";
            this.txtRepeatPassword.FieldLabel = modMain.strUsersRepeatPassword;
            this.txtName.FieldLabel = modMain.strCommonName;
            this.txtEmail.FieldLabel = modMain.strUsersEmail;
            this.ddlProfile.FieldLabel = modMain.strUsersSelectRole;
            this.btnSave.Text = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
        }

        #endregion

    }
}
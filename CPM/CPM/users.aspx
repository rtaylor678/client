﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_master/default.master" AutoEventWireup="true" CodeBehind="users.aspx.cs" Inherits="CPM._users" %>

<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="cntCenter" runat="server">
    
    <script type="text/javascript">
        var prepareCommand = function (grid, command, record, row) {
            if (command.command == 'Delete' && record.data.BuiltIn == false) {
                command.disabled = false;
            }
        };
    </script>
    <ext:Panel ID="pnlTitlePanel" Layout="FitLayout" AutoScroll="true" runat="server" BodyStyle="background-color: transparent;">
        <TopBar>
            <ext:Toolbar Height="40" ID="tbMenu" runat="server" Flat="true" Cls="menubackground">
                <Items>
                    <ext:ToolbarSeparator runat="server" ID="sepAddSetting" Border="false" Width="2" />
                    <ext:Button ID="btnAdd" runat="server" IconCls="menu-addnew" Scale="Large">
                        <DirectEvents>
                            <Click OnEvent="btnAdd_Click">
                                    <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:ToolbarFill />
                    <ext:Button ID="btnModels" runat="server" IconCls="menu-modelselection" Scale="Large">
                        <DirectEvents>
                            <Click OnEvent="btnModel_Click">
                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </TopBar>
        <Items>
            <ext:GridPanel ID="grdUser" runat="server" Border="false" Icon="Group">
                <Store>
                    <ext:Store ID="storeUsers" runat="server">
                        <Model>
                            <ext:Model ID="Model1" runat="server" IDProperty="IdUser">
                                <Fields>
                                    <ext:ModelField Name="IdUser" Type="Int" />
                                    <ext:ModelField Name="UserName" Type="String" />
                                    <ext:ModelField Name="Name" Type="String" />
                                    <ext:ModelField Name="Email" Type="String" />
                                    <ext:ModelField Name="LastLogin" Type="Date" />
                                    <ext:ModelField Name="BuiltIn" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:Column ID="colID" Hidden="true" runat="server" DataIndex="IdUser" />
                        <ext:Column ID="UserName" runat="server" DataIndex="UserName" />
                        <ext:Column ID="Name" runat="server" DataIndex="Name" Flex="1" />
                        <ext:Column ID="Email" runat="server" DataIndex="Email" Flex="1" />
                        <ext:DateColumn Align="Center" ID="LastLogindt" Format="yyyy-MM-dd HH:mm" Width="150" runat="server" DataIndex="LastLogin" />
                        <ext:ImageCommandColumn Align="Center" Width="150" Text="Functions" ID="ImageCommandColumn1" runat="server">
                            <Commands>
                                <ext:ImageCommand CommandName="Edit" IconCls="icon-edit_16" />
                                <ext:ImageCommand CommandName="Delete" IconCls="icon-delete_16" Disabled="true"/>
                            </Commands>
                            <PrepareCommand Fn="prepareCommand" />
                            <DirectEvents>                                       
                                <Command OnEvent="grdUser_Command">
                                    <ExtraParams>  
                                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>  
                                            <ext:Parameter Name="IdUser" Value="record.data.IdUser" Mode="Raw"></ext:Parameter> 
                                            <ext:Parameter Name="Name" Value="record.data.Name" Mode="Raw"></ext:Parameter> 
                                        </ExtraParams> 
                                </Command>
                            </DirectEvents>
                        </ext:ImageCommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" />
                </SelectionModel>
                    <Features>
                    <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                        <Filters>
                            <ext:StringFilter DataIndex="Name" />
                            <ext:StringFilter DataIndex="Email" />
                                   
                                <ext:DateFilter DataIndex="LastLogindt"></ext:DateFilter>
                                <ext:NumericFilter DataIndex="IdUser" />
                        </Filters>
                    </ext:GridFilters>
                </Features>
        
            </ext:GridPanel>            
        </Items>
    </ext:Panel>
    <ext:Window ID="winUserEdit" runat="server" Title="User" Hidden="true" icon="Group" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
        <Items>
            <ext:FormPanel ID="pnlUser" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
            <Items>
                <ext:Panel ID="Panel2" runat="server" Border="false" Header="false" ColumnWidth=".5" Layout="Form" LabelAlign="Top" Cls="PopupFormColumnPanel">
                    <Defaults>
                        <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                        <ext:Parameter Name="MsgTarget" Value="side" />
                    </Defaults>
                    <Items>
                        <ext:TextField ID="txtUserName" runat="server" FieldLabel="User Name" AnchorHorizontal="92%" Cls="PopupFormField" />
                        <ext:TextField ID="txtPassword" runat="server" InputType="Password" FieldLabel="Password" AnchorHorizontal="92%" IsRemoteValidation="true"  Cls="PopupFormField">
                            <RemoteValidation OnValidation="CheckStrength" />
                        </ext:TextField>
                        <ext:TextField ID="txtRepeatPassword" runat="server" InputType="Password" FieldLabel="Repeat Password" AnchorHorizontal="92%" IsRemoteValidation="true" Cls="PopupFormField">
                            <RemoteValidation OnValidation="CheckField" />
                        </ext:TextField>
                    </Items>
                </ext:Panel>
                <ext:Panel ID="Panel3" runat="server" Border="false" Layout="Form" ColumnWidth=".5" LabelAlign="Top" Cls="PopupFormColumnPanel">
                    <Defaults>
                        <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                        <ext:Parameter Name="MsgTarget" Value="side" />
                    </Defaults>
                    <Items>
                        <ext:TextField ID="txtName" runat="server" FieldLabel="Name" AnchorHorizontal="92%" Cls="PopupFormField" />
                        <ext:TextField ID="txtEmail" runat="server" FieldLabel="Email" Vtype="email" AnchorHorizontal="92%" Cls="PopupFormField"/>
                        <ext:ComboBox ID="ddlProfile" runat="server" DisplayField="Name" Editable="false" TriggerAction="All" FieldLabel="Select Role" ValueField="ID" Cls="PopupFormField">
                            <Store>
                                <ext:Store ID="storeProfile" runat="server" OnReadData="StoreProfile_ReadData">
                                    <Model>
                                        <ext:Model ID="mdlProfile" runat="server" IDProperty="ID">
                                            <Fields>
                                                <ext:ModelField Name="ID" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                    <Proxy>
                                        <ext:PageProxy>
                                            <Reader>
                                                <ext:JsonReader />
                                            </Reader>
                                        </ext:PageProxy>
                                    </Proxy>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </Items>
                </ext:Panel>
            </Items>
            <Buttons>
                <ext:Button ID="btnSave" runat="server" Text="Save" icon="DatabaseSave" Disabled="true" FormBind="true">
                    <DirectEvents>
                        <Click OnEvent="btnSave_Click" />
                    </DirectEvents>
                </ext:Button>
                <ext:Button ID="btnCancel" runat="server" icon="Decline" Text="Close">
                    <DirectEvents>
                        <Click OnEvent="btnCancel_Click" />
                    </DirectEvents>
                </ext:Button>
            </Buttons>
            <BottomBar>
                <ext:StatusBar ID="FormStatusBar" runat="server" />
            </BottomBar>
            <Listeners>
                <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                            });
                                            #{btnSave}.setDisabled(!valid);" />
            </Listeners>
        </ext:FormPanel>
        </Items>
    </ext:Window> 

</asp:Content>
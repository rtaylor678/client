﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using CD;
using DataClass;
using System.Globalization;

namespace CPM
{
    public partial class _caseselection : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } set { Session["Application"] = value; } }
        public Int32 IdUser { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String StatusModel { get { return (String)Session["StatusModel"]; } set { Session["StatusModel"] = value; } }
        public String NameUser { get { return (String)Session["NameUser"]; } set { Session["NameUser"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }

        private static modMain objMain = new modMain();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                this.HideBaseModel();
                if (this.IsModelAddAllowed())
                {
                    this.btnAdd.Disabled = false;
                }
                this.LoadModels();
                if (this.ModelCount() == 0)
                {
                    this.btnAdd.Show();
                }

                this.LoadLanguage();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winModelEdit.Show();
            this.HideBaseModel();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            if (ValidForm())
            {
                this.SaveModels(Convert.ToInt32(IdModel));
                this.LoadModels();
                this.HideBaseModel();
            }
            else
            {
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Enter records correctly!"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void chkBaseModel_OnChange(object sender, DirectEventArgs e)
        {
            if (this.chkBaseModel.Checked)
            {
                this.DisabledFields();
            }
            else
            {
                this.EnabledFields();
            }
        }

        protected void ddlBaseModel_Select(object sender, DirectEventArgs e)
        {
            if (this.chkBaseModel.Checked || this.FieldConteinerBaseModel.Hidden)
            {
                this.ModelBaseLoad(Convert.ToInt32(this.ddlBaseModel.Value));
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            this.winModelEdit.Hide();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void grdModels_Command(object sender, DirectEventArgs e)
        {
            try
            {
                String Command = e.ExtraParams["command"].ToString();
                IdModel = int.Parse(e.ExtraParams["IdModel"].ToString());
                NameModel = (String)e.ExtraParams["Name"].ToString();
                StatusModel = (String)e.ExtraParams["Status"].ToString();
                if (Command == "Edit")
                {
                    this.winModelEdit.Show();
                    this.FieldConteinerBaseModel.Hide();
                    this.chkBaseModel.Checked = false;
                    this.EditModelLoad(IdModel);
                    this.DisabledFields();
                }
                else if (Command == "Select")
                {
                    X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure to want to open the case:") + " " + NameModel + "?", new Ext.Net.MessageBoxButtonsConfig
                    {
                        Yes = new MessageBoxButtonConfig
                        {
                            Handler = "UsersX.ClickedModelYES()",
                            Text = modMain.strCommonYes
                        },
                        No = new MessageBoxButtonConfig
                        {
                            Text = modMain.strCommonNo
                        }
                    }).Show();
                    
                }
                else
                {
                    X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the case:") + " " + NameModel + "?", new Ext.Net.MessageBoxButtonsConfig
                    {
                        Yes = new MessageBoxButtonConfig
                        {
                            Handler = "UsersX.ClickedYES()",
                            Text = modMain.strCommonYes
                        },
                        No = new MessageBoxButtonConfig
                        {
                            Text = modMain.strCommonNo
                        }
                    }).Show();
                }
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;            
            }
        }

        protected void StoreModel_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsModels objModels = new DataClass.clsModels();
            this.storeBaseModel.DataSource = objModels.LoadComboBox("", "ORDER BY Name");
            this.storeBaseModel.DataBind();
        }

        protected void StoreBaseYear_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(Int32));
            dt.Columns.Add("Name", typeof(Int32));

            DateTime Now = DateTime.Now;


            Int32 intIni = Now.Year - 10;
            Int32 intEnd = Now.Year + 10;

            for (int i = intIni; i <= intEnd; i++)
            {
                DataRow dr = dt.NewRow();
                dr[0] = i;
                dr[1] = i;
                dt.Rows.Add(dr);
            }
            this.storeBaseYear.DataSource = dt;
            this.storeBaseYear.DataBind();
        }

        protected void StoreProjection_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(Int32));
            dt.Columns.Add("Name", typeof(String));

            DateTime Now = DateTime.Now;

            for (int i = 1; i <= 60; i++)
            {
                DataRow dr = dt.NewRow();
                dr[0] = i;
                dr[1] = Convert.ToString(i) + " Years";
                dt.Rows.Add(dr);
            }
            this.storeProjection.DataSource = dt;
            this.storeProjection.DataBind();
        }

        protected void StoreStatus_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsParameters objParameters = new DataClass.clsParameters();
            this.storeStatus.DataSource = objParameters.LoadList("IdLanguage = " + idLanguage + " AND Type = 'ModelStatus'", "ORDER BY Name");
            this.storeStatus.DataBind();
        }

        protected void StoreLevel_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsParameters objParameters = new DataClass.clsParameters();
            this.storeLevel.DataSource = objParameters.LoadList("IdLanguage = " + idLanguage + " AND Type = 'ModelLevel'", "ORDER BY Name");
            this.storeLevel.DataBind();
        }

        /// Multi Fields Projection
        protected void btnMultiFieldsProjection_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/multifieldsopexreportCPM.aspx");
        }

        #endregion

        #region Methods

        protected void Clear()
        {
            this.IdModel = 0;
            this.chkBaseModel.Checked = false;
            this.txtName.Text = "";
            this.txtCreateBy.Text = NameUser;
            this.txtCreateDate.Text = Convert.ToString(DateTime.Now);
            this.txtNote.Text = "";
            this.ddlBaseModel.Reset();
            this.ddlBaseYear.Select(9);
            this.ddlProjection.Select(0);
            this.ddlStatus.Select(1);
            this.ddlLevel.Select(0);

            this.EnabledFields();
        }

        protected void EditModelLoad(Int32 IdModel)
        {
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("IdModel = " + IdModel, "");
            if (dt.Rows.Count > 0)
            {
                this.txtName.Text = (String)dt.Rows[0]["Name"];
                this.txtCreateBy.Text = (String)dt.Rows[0]["CreatedBy"];
                this.txtCreateDate.Text = Convert.ToString(dt.Rows[0]["DateCreation"]);
                this.txtNote.Text = (String)dt.Rows[0]["Notes"];
                this.ddlBaseYear.SetValue(Convert.ToInt32(dt.Rows[0]["BaseYear"]));
                this.ddlProjection.Value = Convert.ToInt32(dt.Rows[0]["IdProjection"]);
                this.ddlStatus.Value = Convert.ToInt32(dt.Rows[0]["Status"]);
                this.ddlLevel.Value = Convert.ToInt32(dt.Rows[0]["Level"]);

                this.DisabledFields();
            }
        }

        protected void ModelBaseLoad(Int32 IdModel)
        {
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("IdModel = " + IdModel, "");
            if (dt.Rows.Count > 0)
            {
                this.txtName.Text = (String)dt.Rows[0]["Name"];
                this.txtCreateBy.Text = NameUser;
                this.txtCreateDate.Text = Convert.ToString(DateTime.Now);
                this.txtNote.Text = (String)dt.Rows[0]["Notes"];
                this.ddlBaseYear.SetValue(Convert.ToInt32(dt.Rows[0]["BaseYear"]));
                this.ddlProjection.Value = Convert.ToInt32(dt.Rows[0]["IdProjection"]);
                this.ddlStatus.Value = Convert.ToInt32(dt.Rows[0]["Status"]);
                this.ddlLevel.Value = Convert.ToInt32(dt.Rows[0]["Level"]);

                this.DisabledFields();
            }
        }

        protected void EnabledFields()
        {
            this.ddlBaseYear.ReadOnly = false;
            this.ddlProjection.ReadOnly = false;
        }

        protected void DisabledFields()
        {
            this.ddlBaseYear.ReadOnly = true;
            this.ddlProjection.ReadOnly = true;
        }

        [DirectMethod]
        public void ClickedModelYES()
        {
            DataClass.clsUsers objUsers = new DataClass.clsUsers();
            objUsers.IdUser = IdUser;
            objUsers.LogActivity(UserLog.UserActivity.CaseOpened, IdModel, null);

            Int32 intIdProfile = this.GetIdProfile();
            DataClass.clsProfileRoles objProfileRoles = new DataClass.clsProfileRoles();
            DataTable dtPermissions = objProfileRoles.LoadPermission(IdModel, intIdProfile);
            List<Int32> intPermissions = new List<Int32>();
            foreach (DataRow objRow in dtPermissions.Rows)
            {
                intPermissions.Add(Convert.ToInt32(objRow["IdRole"]));
            }
            objApplication.AllowedRoles = intPermissions;
            dtPermissions = null;
            objProfileRoles = null;

            X.Redirect("/case.aspx");
        }

        [DirectMethod]
        public void ClickedYES()
        {
            DataClass.clsUsers objUsers = new DataClass.clsUsers();
            objUsers.IdUser = IdUser;
            objUsers.LogActivity(UserLog.UserActivity.CaseDeleted, IdModel, null);
            this.DeleteModel(IdModel);
            this.LoadModels();
        }

        protected Int32 GetIdProfile()
        {
            Int32 Value = 0;
            DataClass.clsUsers objUsers = new DataClass.clsUsers();

            DataTable dt = objUsers.LoadList("IdUser = " + IdUser, "");
            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdProfile"];
            }
            return Value;
        }

        protected void LoadModels()
        {
            DataClass.clsModels objModels = new DataClass.clsModels();

            DataTable dt = objModels.LoadList("IdLanguage = " + idLanguage, "ORDER BY Name");

            storeModel.DataSource = dt;
            storeModel.DataBind();
        }

        protected Int32 ModelCount()
        {
            DataClass.clsModels objModels = new DataClass.clsModels();

            DataTable dt = objModels.LoadList("", "ORDER BY Name");

            return dt.Rows.Count;
        }

        protected void HideBaseModel()
        {
            this.FieldConteinerBaseModel.Hide();
            this.btnAdd.Hide();
            this.cmdAdminModels.Hide();
            DataClass.clsProfileRoles objProfileRoles = new DataClass.clsProfileRoles();
            DataTable dt = objProfileRoles.LoadList("IdRole = 10 AND IdProfile = " + this.LoadProfile(), "");
            Int32 intCount = 0;
            foreach (DataRow row in dt.Rows)
            {
                if (Convert.ToInt32(row["GrantAccess"]) == 1)
                {
                    this.btnAdd.Show();
                    this.cmdAdminModels.Show();
                    this.FieldConteinerBaseModel.Show();
                    intCount = intCount + 1;
                }
                else
                {
                    if (Convert.ToInt32(row["DenyAccess"]) == 1)
                    {
                        intCount = intCount + 1;
                    }
                }
            }

            if (intCount == 0 || this.FieldConteinerBaseModel.Hidden)
            {
                this.btnAdd.Hide();
                this.cmdAdminModels.Hide();
            }
            else
            {
                this.btnAdd.Show();
                this.cmdAdminModels.Show();
            }
        }

        protected Int32 LoadProfile()
        {
            Int32 IdPerfil = 0;
            DataClass.clsUsers objUsers = new DataClass.clsUsers();

            DataTable dt = objUsers.LoadList("IdUser = " + IdUser, "ORDER BY Name");
            if (dt.Rows.Count>0)
            {
                IdPerfil = Convert.ToInt32(dt.Rows[0]["IdProfile"]);
            }

            return IdPerfil;
        }

        protected Boolean ValidForm()
        {
            Boolean Valid = true;
            if (this.txtName.Text == "")
            {
                Valid = false;
            }
            return Valid;
        }

        protected void SaveModels(Int32 ID)
        {
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataClass.clsUsers objUsers = new DataClass.clsUsers();
            objUsers.IdUser = IdUser;

            objModels.IdModel = (Int32)ID;
            objModels.loadObject();

            if (IdModel == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (objModels.Name.ToString().ToUpper() != txtName.Text.ToString().ToUpper())
                {
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }

            objModels.Name = this.txtName.Text;
            objModels.Notes = this.txtNote.Text;
            objModels.BaseYear = Convert.ToInt32(this.ddlBaseYear.Value);
            objModels.Projection = Convert.ToInt32(this.ddlProjection.Value);
            objModels.Status = Convert.ToInt32(this.ddlStatus.Value);
            objModels.Level = Convert.ToInt32(this.ddlLevel.Value);

            if (ID == 0)
            {
                if (this.chkBaseModel.Checked || this.FieldConteinerBaseModel.Hidden)
                {
                    objModels.DateCreation = DateTime.Now;
                    objModels.UserCreation = (Int32)IdUser;
                    Int32 intIdModelBase = 0;
                    if (this.ddlBaseModel.Value != null)
                        intIdModelBase = Convert.ToInt32(this.ddlBaseModel.Value);
                    IdModel = objModels.InsertModelBase(intIdModelBase, objApplication.MaxRelationLevels);
                }
                else
                {
                    objModels.DateCreation = DateTime.Now;
                    objModels.UserCreation = (Int32)IdUser;
                    IdModel = objModels.Insert();
                }
                objUsers.LogActivity(UserLog.UserActivity.CaseCreated, IdModel, null);
            }
            else
            {
                objModels.DateModification = DateTime.Now;
                objModels.UserModification = (Int32)IdUser;
                objModels.Update();
                objUsers.LogActivity(UserLog.UserActivity.CaseEdited, IdModel, null);
            }
            if (this.chkBaseModel.Checked || this.FieldConteinerBaseModel.Hidden)
            {
                Int32 intModelBAse = Convert.ToInt32(ddlBaseModel.Value);
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","Model Base") + " " + this.txtName.ToString() + " " + (String)GetGlobalResourceObject("CPM_Resources","saved successfully!"), IconCls = "icon-accept", Clear2 = false });
            }
            else
            {
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
                this.winModelEdit.Hide();
            }
            this.ddlBaseModel.Reset();
            this.winModelEdit.Close();
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("Name = '" + txtName.Text + "'", "");
            Value = dt.Rows.Count;

            return Value;
        }

        public void DeleteModel(Int32 ID)
        {
            try
            {
                DataClass.clsModels objModels = new DataClass.clsModels();
                DataClass.clsUsers objUsers = new DataClass.clsUsers();
                objUsers.IdUser = IdUser;

                objModels.IdModel = (Int32)ID;
                objModels.Delete();
                objUsers.LogActivity(UserLog.UserActivity.CaseDeleted, IdModel, null);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("FK_tblProfilesRoles_tblModels"))
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Title = modMain.strCommonDelete,
                        Message = ex.Message,
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.ERROR
                    });
                }
            }
        }

        protected Boolean IsModelAddAllowed()
        {
            Boolean Value = false;
            DataClass.clsProfiles objProfiles = new DataClass.clsProfiles();

            DataTable dt = objProfiles.LoadListRoles(this.IdUser.ToString());

            if (dt.Rows.Count > 0)
            {
                Value = true;
            }
            return Value;
        }

        protected void LoadLanguage()
        {
            DataClass.clsLabelsLanguages objLabelsLanguages = new DataClass.clsLabelsLanguages();
            DataTable dt = objLabelsLanguages.LoadList("LanguageName = '" + Language + "' AND FormName = 'CaseDetails'", "ORDER BY IdLabel");

            foreach (DataRow row in dt.Rows)
            {
                switch ((String)row["LabelName"])
                {
                    case "Case Details":
                        modMain.strCaseDetailsTitleForm = (String)row["LabelText"];
                        break;
                    case "Add New Case":
                        modMain.strCaseDetailsNewCase = (String)row["LabelText"];
                        break;
                    case "Base Case":
                        modMain.strCaseDetailsBaseCase = (String)row["LabelText"];
                        break;
                    case "Created By":
                        modMain.strCaseDetailsCreatedBy = (String)row["LabelText"];
                        break;
                    case "Created":
                        modMain.strCaseDetailsCreated = (String)row["LabelText"];
                        break;
                    case "Base Year":
                        modMain.strCaseDetailsBaseYear = (String)row["LabelText"];
                        break;
                    case "Projection":
                        modMain.strCaseDetailsProjection = (String)row["LabelText"];
                        break;
                    case "Status":
                        modMain.strCaseDetailsStatus = (String)row["LabelText"];
                        break;
                    case "Level":
                        modMain.strCaseDetailsLevel = (String)row["LabelText"];
                        break;
                    case "Note":
                        modMain.strCaseDetailsNote = (String)row["LabelText"];
                        break;
                    case "Form Status":
                        modMain.strCaseDetailsFormStatus = (String)row["LabelText"];
                        break;
                    case "Date":
                        modMain.strCaseDetailsDate = (String)row["LabelText"];
                        break;
                    case "Parameters":
                        modMain.strMasterParametersModule = (String)row["LabelText"];
                        break;
                    case "Allocation":
                        modMain.strMasterAllocationModule = (String)row["LabelText"];
                        break;
                    case "Close":
                        modMain.strCommonClose = (String)row["LabelText"];
                        break;
                    case "Delete":
                        modMain.strCommonDelete = (String)row["LabelText"];
                        break;
                    case "Edit":
                        modMain.strCommonEdit = (String)row["LabelText"];
                        break;
                    case "Functions":
                        modMain.strCommonFunctions = (String)row["LabelText"];
                        break;
                    case "Name":
                        modMain.strCommonName = (String)row["LabelText"];
                        break;
                    case "Open":
                        modMain.strCommonOpen = (String)row["LabelText"];
                        break;
                    case "Save":
                        modMain.strCommonSave = (String)row["LabelText"];
                        break;
                    case "Select":
                        modMain.strCommonSelect = (String)row["LabelText"];
                        break;
                    case "Save Changes":
                        modMain.strCommonSaveChanges = (String)row["LabelText"];
                        break;
                    case "Delete All":
                        modMain.strCommonDeleteAll = (String)row["LabelText"];
                        break;
                    case "Confirm":
                        modMain.strCommonConfirm = (String)row["LabelText"];
                        break;
                    case "Yes":
                        modMain.strCommonYes = (String)row["LabelText"];
                        break;
                    case "No":
                        modMain.strCommonNo = (String)row["LabelText"];
                        break;
                    case "Form is valid":
                        modMain.strCommonFormValid = (String)row["LabelText"];
                        break;
                    case "Form is invalid":
                        modMain.strCommonFormInvalid = (String)row["LabelText"];
                        break;
                    case "Import":
                        modMain.strCommonImport = (String)row["LabelText"];
                        break;
                    case "Code":
                        modMain.strCommonCode = (String)row["LabelText"];
                        break;
                    case "Help":
                        modMain.strCommonHelp = (String)row["LabelText"];
                        break;
                    case "Technical Driver:":
                        modMain.strTechnicalModuleTechnicalDriver = (String)row["LabelText"];
                        break;
                    case "Field:":
                        modMain.strTechnicalModuleField = (String)row["LabelText"];
                        break;
                    case "Project:":
                        modMain.strTechnicalModuleProject = (String)row["LabelText"];
                        break;
                    case "Base":
                        modMain.strTechnicalModuleBase = (String)row["LabelText"];
                        break;
                    case "Pessimistic":
                        modMain.strTechnicalModulePessimistic = (String)row["LabelText"];
                        break;
                    case "Optimistic":
                        modMain.strTechnicalModuleOptimistic = (String)row["LabelText"];
                        break;
                    case "Year":
                        modMain.strTechnicalModuleYear = (String)row["LabelText"];
                        break;
                }
            }
            this.LoadLabel();
        }

        protected void LoadLabel()
        {
            //TopBar
            this.btnAdd.Text = "<B>" + modMain.strCaseDetailsNewCase + "</B>";
            this.btnAdd.ToolTip = modMain.strCaseDetailsNewCase;
            this.btnSave.Text = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.btnMultiFieldsProjection.Text = "<B>" + objMain.FormatButtonText((String)GetGlobalResourceObject("CPM_Resources", "Multiple Models Projection")) + "</B>";


            //GridPanel
            this.gName.Text = modMain.strCommonName;
            this.gCreatedBy.Text = modMain.strCaseDetailsCreatedBy;
            this.gDateCreations.Text = modMain.strCaseDetailsDate;
            this.gBaseYear.Text = modMain.strCaseDetailsBaseYear;
            this.gProjection.Text = modMain.strCaseDetailsProjection;
            this.gNotes.Text = modMain.strCaseDetailsNote;
            this.gStatusName.Text = modMain.strCaseDetailsStatus;
            this.cmdAdminModels.Text = modMain.strCommonFunctions;
            this.cmdAdminModels.Commands[0].Text = modMain.strCommonEdit; //Edit option
            this.cmdAdminModels.Commands[1].Text = modMain.strCommonDelete; //Delete option

            //Window Popup
            this.winModelEdit.Title = modMain.strCaseDetailsTitleForm;
            this.ImageCommandColumn1.Text = modMain.strCommonSelect;
            this.ImageCommandColumn1.Commands[0].Text= modMain.strCommonOpen; //Open option
            this.chkBaseModel.FieldLabel = modMain.strCaseDetailsBaseCase;
            this.txtName.FieldLabel = modMain.strCommonName;
            this.txtCreateBy.FieldLabel = modMain.strCaseDetailsCreatedBy;
            this.txtCreateDate.FieldLabel = modMain.strCaseDetailsCreated;
            this.ddlBaseYear.FieldLabel = modMain.strCaseDetailsBaseYear;
            this.ddlProjection.FieldLabel = modMain.strCaseDetailsProjection;
            this.ddlStatus.FieldLabel = modMain.strCaseDetailsStatus;
            this.ddlLevel.FieldLabel = modMain.strCaseDetailsLevel;
            this.txtNote.FieldLabel = modMain.strCaseDetailsNote;
        }

        #endregion        

    }
}
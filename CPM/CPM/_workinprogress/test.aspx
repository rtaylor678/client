﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="test.aspx.cs" Inherits="CPM._workinprogress.test" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_iconxl.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_menu.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color transparent;" Layout="BorderLayout" MinWidth="800">
        <Items>
            <ext:Panel ID="pnlTitlePanel" runat="server" Region="North" Border="false" Header="false" BodyStyle="background-color: transparent;" Margins="0 0 0 0" ButtonAlign="Right">
                <Content>
                    <img src="/image/cpm_logo.png" />
                </Content>
            </ext:Panel>
            <ext:Panel runat="server" Region="West" Title="Left" Width="200" ID="pnlSettings" BodyStyle="background-color: #d8d8d8;" Collapsible="true" HeaderPosition="Top" Split="true" MinWidth="175" MaxWidth="400" AnimCollapse="true" Layout="AccordionLayout" />
            <ext:Panel ID="pnlBody" runat="server" Region="Center" Margins="1 1 1 1" Closable="false" OverflowY="Auto">
                <Content>
                    <p>Body Viewport</p>
                </Content>
                <Items>
                    <ext:Panel ID="MyFit1" runat="server" Layout="FitLayout">
                        <Items>
                            <ext:Image ID="img01" runat="server" ImageUrl="/image/cpm_logo.png" Height="200" Width="200" />
                        </Items>
                    </ext:Panel>
                            <ext:Panel ID="MyHbox1" runat="server" Layout="HBoxLayout" AutoScroll="true">
                                <Content>
                                    <ext:Image ID="img02" runat="server" ImageUrl="/image/cpm_logo.png" Height="200" Width="2000" />
                                </Content>
                            </ext:Panel>
                            <ext:GridPanel ID="MyGridPanel" runat="server" Header="false" Border="false" Height="400">
                                <Store>
                                    <ext:Store ID="Store1" runat="server" PageSize="10">
                                        <Model>
                                            <ext:Model runat="server" IDProperty="ID">
                                                <Fields>
                                                    <ext:ModelField Name="ID" />
                                                    <ext:ModelField Name="Name" />
                                                    <ext:ModelField Name="Start" Type="Date" />
                                                    <ext:ModelField Name="End" Type="Date" />
                                                    <ext:ModelField Name="Completed" Type="Boolean" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ColumnModel runat="server">
                                    <Columns>
                                        <ext:Column runat="server" Text="ID" Width="400" Sortable="true" DataIndex="ID" />
                                        <ext:Column runat="server" Text="Job Name" Width="800" Sortable="true" DataIndex="Name" />
                                        <ext:DateColumn runat="server" Text="Start" Width="420" Sortable="true" DataIndex="Start" Format="yyyy-MM-dd" />
                                        <ext:DateColumn runat="server" Text="End" Width="420" Sortable="true" DataIndex="End" Format="yyyy-MM-dd" />
                                        <ext:Column runat="server" Text="Completed" Width="480" Sortable="true" DataIndex="Completed">
                                            <Renderer Handler="return (value) ? 'Yes':'No';" />
                                        </ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <View>
                                    <ext:GridView runat="server" LoadMask="false" />
                                </View>
                                <Features>
                                    <ext:GridFilters runat="server" Local="true">
                                        <Filters>
                                            <ext:NumericFilter DataIndex="ID" />
                                            <ext:StringFilter DataIndex="Name" />
                                            <ext:DateFilter DataIndex="Start">
                                                <DatePickerOptions runat="server" TodayText="Now" />
                                            </ext:DateFilter>
                                            <ext:DateFilter DataIndex="End">
                                                <DatePickerOptions runat="server" TodayText="Now" />
                                            </ext:DateFilter>                        
                                            <ext:BooleanFilter DataIndex="Completed" />
                                        </Filters>
                                    </ext:GridFilters>
                                </Features>
                                <BottomBar>
                                    <ext:PagingToolbar runat="server" DisplayInfo="true" DisplayMsg="Displaying Jobs {0} - {1} of {2}" />
                                </BottomBar>
                            </ext:GridPanel>
                        </Items>
            </ext:Panel>
            <ext:Panel ID="pnlFooter" runat="server" Region="South" Resizable="false" Header="false" Margins="5 1 1 1" Collapsible="false">
                <FooterBar>
                    <ext:Toolbar ID="StatusBar1" runat="server">
                        <Items>
                            <ext:Label ID="lblVersion" runat="server" Text=" " Width="100" Style="text-align:left;padding-left:5px;" />
                            <ext:Label ID="lblCopyright" runat="server" Text="© HSB Solomon Associates LLC" Flex="1" Style="text-align:center;" />
                            <ext:ToolbarTextItem ID="clock" runat="server" Text=" " Width="100" Style="text-align:right;padding-right:5px;" CtCls="x-status-text-panel" />
                        </Items>
                    </ext:Toolbar>
                </FooterBar>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    <ext:TaskManager ID="TaskManager1" runat="server">
        <Tasks>
            <ext:Task AutoRun="true" Interval="1000">
                <Listeners>
                    <Update Handler="#{clock}.setText(Ext.Date.format(new Date(), 'g:i:s A'));" />
                </Listeners>
            </ext:Task>
        </Tasks>
    </ext:TaskManager>
    </form>
</body>
</html>
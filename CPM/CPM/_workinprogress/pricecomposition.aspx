﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pricecomposition.aspx.cs" Inherits="CPM._pricecomposition" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
    <script>
        var edit = function (editor, e, options) {

            e.record.set('NonTradable', (100 - e.record.getData().Tradable));

        }
    </script>
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" Layout="FitLayout" AutoScroll="true" runat="server">
                <Items>
                    <ext:GridPanel ID="grdPriceComposition" runat="server" Border="false" Header="false">
                        <TopBar>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                    <ext:Button  ID="btnSave" Width="100" Icon="DataBaseSave" ToolTip="Save Changes" runat="server" Text="Save Changes">
                                        <DirectEvents>
                                            <Click OnEvent="btnSave_Click">
                                                    <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="rowsValues" Value="#{grdPriceComposition}.getRowsValues(false)" Mode="Raw" Encode="true"/>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="storePriceComposition" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="IdPriceComposition">
                                        <Fields>
                                            <ext:ModelField Name="IdPriceComposition" Type="Int" />
                                            <ext:ModelField Name="IdZiffAccount" Type="Int" />
                                            <ext:ModelField Name="CodeZiffAccount" Type="String" />
                                            <ext:ModelField Name="NameZiffAccount" Type="String" />
                                            <ext:ModelField Name="Tradable" Type="Float" />
                                            <ext:ModelField Name="NonTradable" Type="Float" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel ID="ColumnModel1" runat="server">
                            <Columns>
                                <ext:Column ID="colID" Hidden="true" runat="server" Text="ID" DataIndex="IdTypePriceEconomic"></ext:Column>
                                <ext:Column ID="IdZiff" Hidden="true" runat="server" Text="IDZiff" DataIndex="IdZiffAccount"></ext:Column>
                                <ext:Column ID="CodeZiffAccount" runat="server" Text="Code" DataIndex="CodeZiffAccount" Flex="1">
                                </ext:Column>
                                <ext:Column ID="NameZiffAccount" runat="server" Text="Description" DataIndex="NameZiffAccount" Flex="1">
                                </ext:Column>
                                <ext:NumberColumn Format="0.00"  ID="NumTradable"
                                    runat="server" 
                                    DataIndex="Tradable"
                                    Text="Tradable">
                                    <Editor>
                                        <ext:NumberField ID="nfTradable" runat="server" AllowBlank="false" MinValue="0" MaxValue="100">
                                            
                                        </ext:NumberField>
                                    </Editor>
                                </ext:NumberColumn>
                                <ext:NumberColumn Format="0.00" ID="numNonTradable" runat="server" DataIndex="NonTradable" Text="Non Tradable" Sortable="false" Groupable="false">
                                </ext:NumberColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server"   />
                        </SelectionModel>
                        <Features>
                            <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                                <Filters>
                                    <ext:StringFilter DataIndex="CodeZiffAccount" />
                                    <ext:StringFilter DataIndex="NameZiffAccount" />
                                    <ext:NumericFilter DataIndex="Tradable" />
                                    <ext:NumericFilter DataIndex="NonTradable" />
                                </Filters>
                            </ext:GridFilters>
                        </Features>
                    </ext:GridPanel>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="FormStatusBar" runat="server" />
                </BottomBar>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
    
</body>
</html>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;


namespace CPM
{
    public partial class _pricecomposition : System.Web.UI.Page
    {
        #region Definitions

        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdPriceComposition { get { return (Int32)Session["IdPriceComposition"]; } set { Session["IdPriceComposition"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; }}// set { Session["Language"] = value; } }
        public DataTable datViewData { get; set; }

        #endregion

        #region Events

               

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                IdPriceComposition = 0;
                this.LoadPriceComposition();
            }
        }
        #endregion

        #region Methods

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                String strData= e.ExtraParams["rowsValues"];
                datViewData = JSON.Deserialize<DataTable>(strData);
                this.Save();
                this.LoadPriceComposition();
                
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void Save()
        {
            foreach (DataRow row in datViewData.Rows)
            {
                DataClass.clsPriceComposition objPriceComposition = new DataClass.clsPriceComposition();
                objPriceComposition.IdPriceComposition = Convert.ToInt32(row["IdPriceComposition"]);
                objPriceComposition.loadObject();
                objPriceComposition.IdModel = IdModel;
                objPriceComposition.IdZiffAccount = Convert.ToInt32(row["IdZiffAccount"]);
                objPriceComposition.Tradable = Convert.ToDouble(row["Tradable"]);
                objPriceComposition.NonTradable = Convert.ToDouble(row["NonTradable"]);
                if (objPriceComposition.IdPriceComposition == 0)
                {
                    objPriceComposition.DateCreation = DateTime.Now;
                    objPriceComposition.UserCreation = (Int32)IdUserCreate;
                    IdPriceComposition = objPriceComposition.Insert();
                }
                else
                {
                    objPriceComposition.DateModification = DateTime.Now;
                    objPriceComposition.UserModification = (Int32)IdUserCreate;
                    objPriceComposition.Update();
                }
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = "Record saved successfully!", IconCls = "icon-accept", Clear2 = false });
        }

        protected void nfTradable_Select(object sender, DirectEventArgs e)
        {
            storePriceComposition.SelfRender();
        }
        
        protected void LoadPriceComposition()
        {
            
            DataClass.clsPriceComposition objPriceComposition = new DataClass.clsPriceComposition();
            objPriceComposition.IdModel = IdModel;
            DataTable dt = objPriceComposition.LoadList();
            storePriceComposition.DataSource = dt;
            storePriceComposition.DataBind();
        }
        #endregion

    } 
}
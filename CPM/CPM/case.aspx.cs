﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _case : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUser { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public String NameUser { get { return (String)Session["NameUser"]; } set { Session["NameUser"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public DateTime LastLogin { get { return (DateTime)Session["LastLogin"]; } set { Session["LastLogin"] = value; } }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if ((Session["LastLogin"] == null || Session["IdUser"] == null) && Session["IdModel"] == null)
                {
                    X.Redirect("/account/login.aspx");
                }
                if (!X.IsAjaxRequest)
                {
                    if (Session["StatusModel"].ToString().ToLower() == "finished")
                    {
                        this.mnuReport.Expand();
                        this.mnuReport.ClearSelection();
                        pnlBody.Loader.Url = "/report/reportmodule.aspx";
                        pnlBody.Loader.DataBind();
                    }
                    else
                    {
                        this.mnuParameter.Expand();
                        this.mnuParameter.ClearSelection();
                        pnlBody.Loader.Url = "/parameter/parametermodule.aspx";
                        pnlBody.Loader.DataBind();
                    }
                    this.LoadLanguage();
                    this.lblModelName.Text = modMain.strMasterSelectedCase + ": " + NameModel;
                    this.CreateMenu();
                    this.LoadModules();
                    if (this.GetProject())
                    {
                        this.btnProjects.Disable();
                    }
                    if (this.GetEconomicModule())
                    {
                        this.btnEconomicModule.Disable();
                        this.mnuEconomic.Disable();
                        this.btnTechnicalModule.Disable();
                        this.mnuTechnical.Disable();
                        this.btnCostvsDriversProjection.Disable();
                        this.btnOperationalMarginsProjection.Disable();
                        this.btnOpexReport.Disable();
                        this.btnOpexReportDetailed.Disable();
                        this.btnMultiFieldsProjection.Disable();
                        this.btnProjects.Disable();
                    }
                    this.LoadApplication();
                }
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                X.Redirect("/account/login.aspx");
            }
        }

        protected void btnHelp_Click(object sender, DirectEventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (this.idLanguage == 1) // English
            {
                objApplication.StreamFile(context, "cpm_en.pdf", "help");
            }
            else if (this.idLanguage == 2) // Spanish
            {
                objApplication.StreamFile(context, "cpm_es.pdf", "help");
            }
        }

        protected void btnAdmin_Click(object sender, DirectEventArgs e)
        {
            this.winUserEdit.Show();
            this.UserLoad();
        }

        protected void btnUser_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/users.aspx");
        }

        protected void btnSetting_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/settings.aspx");
        }

        protected void btnLogon_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/account/login.aspx");
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            if (ValidForm())
            {
                this.SaveUser();
            }
        }

        protected void btnRepositories_Click(object sender, DirectEventArgs e)
        {
            //X.Redirect("/filesrepository.aspx");
            this.pnlBody.LoadContent("/filesrepository.aspx");
            this.pnlBody.Reload();
        }
        
        #region *** Parameters Menu *** (button events)
        protected void btnParameterModule_Click(object sender, DirectEventArgs e)
        {
            this.mnuParameter.Expand();
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/parameter/parametermodule.aspx");
            this.pnlBody.Reload();
        }

        protected void btnAggregationLevel_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/parameter/aggregationlevels.aspx");
            this.pnlBody.Reload();
        }

        protected void btnTypeOperation_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/parameter/typesoperation.aspx");
            this.pnlBody.Reload();
        }

        protected void btnField_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/parameter/fields.aspx");
            this.pnlBody.Reload();
        }

        protected void btnProjects_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/parameter/projects.aspx");
            this.pnlBody.Reload();
        }

        protected void btnCurrencies_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/parameter/currencies.aspx");
            this.pnlBody.Reload();
        }

        protected void btnZiffStructure_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/parameter/ziffstructure.aspx");
            this.pnlBody.Reload();
        }
        #endregion

        #region *** Allocation Menu ***  (button events)
        protected void btnAllocationModule_Click(object sender, DirectEventArgs e)
        {
            this.mnuAllocation.Expand();
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/allocationmodule.aspx");
            this.pnlBody.Reload();
        }

        // *** Internal Cost References ***
        protected void btnClientReferences_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/clientreferences.aspx");
            this.pnlBody.Reload();
        }

        protected void btnActivities_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/activities.aspx");
            this.pnlBody.Reload();
        }

        protected void btnResources_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/resources.aspx");
            this.pnlBody.Reload();
        }

        protected void btnClientCostCenters_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/clientcostcenters.aspx");
            this.pnlBody.Reload();
        }

        protected void btnClientAccounts_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/clientaccounts.aspx");
            this.pnlBody.Reload();
        }

        protected void btnClientCostData_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/clientcostdata.aspx");
            this.pnlBody.Reload();
        }

        protected void btnDirect_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/directcostallocation.aspx");
            this.pnlBody.Reload();
        }

        protected void btnShare_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/sharedcostallocation.aspx");
            this.pnlBody.Reload();
        }

        protected void btnHierarchy_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/hierarchycostallocation.aspx");
            this.pnlBody.Reload();
        }

        protected void btnListDrivers_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/allocationlistdrivers.aspx");
            this.pnlBody.Reload();
        }

        protected void btnAllocationDriversByCostCenter_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/allocationdriverdata.aspx");
            this.pnlBody.Reload();
        }

        protected void btnAllocationDriversData_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/allocationdriversbyfield.aspx");
            this.pnlBody.Reload();
        }

        protected void btnZiffMapping_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/mappingziffvsclient.aspx");
            this.pnlBody.Reload();
        }

        protected void btnBaseCostByField_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/basecostbyfield.aspx");
            this.pnlBody.Reload();
        }

        protected void btnBaseCostByFieldZiff_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/basecostbyfieldziff.aspx");
            this.pnlBody.Reload();
        }

        protected void btnInternalBenchmarkRepository_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/internalfilesrepository.aspx");
            this.pnlBody.Reload();

        }

        //*** External Cost References ***
        protected void btnZiffReferences_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/ziffreferences.aspx");
            this.pnlBody.Reload();
        }

        protected void btnListOfPeerGroup_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/peergroup.aspx");
            this.pnlBody.Reload();
        }

        protected void btnZiffListOfCriteria_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/ziffpeercriteria.aspx");
            this.pnlBody.Reload();
        }

        protected void btnZiffPeerCriteriaData_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/ziffpeercriteriadata.aspx");
            this.pnlBody.Reload();
        }

        protected void btnZiffBaseCost_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/ziffbasecost.aspx");
            this.pnlBody.Reload();
        }

        protected void btnExternalBenchmarkRepository_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/externalfilesrepository.aspx");
            this.pnlBody.Reload();
        }

        protected void btnCostStructureAssumptions_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/allocation/coststructureassumptions.aspx");
            this.pnlBody.Reload();
        }
        #endregion

        #region *** Technical Menu *** (button events)
        protected void btnTechnicalModule_Click(object sender, DirectEventArgs e)
        {
            this.mnuTechnical.Expand();
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/technical/technicalmodule.aspx");
            this.pnlBody.Reload();
        }

        protected void btnListTechnicaDrivers_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/technical/technicaldrivers.aspx");
            this.pnlBody.Reload();
        }

        protected void btnTechnicalDriversData_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/technical/technicaldriverdata.aspx");
            this.pnlBody.Reload();
        }

        protected void btnTechnicalAssumptions_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/technical/technicalassumption.aspx");
            this.pnlBody.Reload();
        }

        protected void btnUtilizationCapacity_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/technical/utilizationcapacity.aspx");
            this.pnlBody.Reload();
        }

        protected void btnBaseCostTechnicalModule_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/technical/basecosttechnicalmodule.aspx");
            this.pnlBody.Reload();
        }

        protected void btnBaseRateByDriversTechnicalModule_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/technical/baseratesbydrivers.aspx");
            this.pnlBody.Reload();
        }

        protected void btnZiffTechnicalAnalysis_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/technical/zifftechnicalanalysis.aspx");
            this.pnlBody.Reload();
        }
        #endregion

        #region *** Economic Menu ***  (button events)
        protected void btnPriceScenarios_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/economic/pricescenarios.aspx");
            this.pnlBody.Reload();
        }

        protected void btnPrices_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/economic/prices.aspx");
            this.pnlBody.Reload();
        }

        protected void btnEconomicModule_Click(object sender, DirectEventArgs e)
        {
            this.mnuEconomic.Expand();
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/economic/economicmodule.aspx");
            this.pnlBody.Reload();
        }

        protected void btnEconomicDrivers_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/economic/economicdriver.aspx");
            this.pnlBody.Reload();
        }

        protected void btnEconomicDriverData_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/economic/economicdriverdata.aspx");
            this.pnlBody.Reload();
        }

        protected void btnEconomicAssumptions_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/economic/typepriceeconomic.aspx");
            this.pnlBody.Reload();
        }

        protected void btnPriceForecast_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/economic/priceforecast.aspx");
            this.pnlBody.Reload();
        }

        protected void btnZiffEconomicAnalysis_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/economic/ziffeconomicanalysis.aspx");
            this.pnlBody.Reload();
        }
        #endregion

        #region *** Reports Menu *** (button events)
        protected void btnReportModule_Click(object sender, DirectEventArgs e)
        {
            this.mnuReport.Expand();
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/report/reportmodule.aspx");
            this.pnlBody.Reload();
        }

        protected void btnOpexReport_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/report/opexreport.aspx");
            this.pnlBody.Reload();
        }

        protected void btnOpexReportDetailed_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/report/opexreportdetailed.aspx");
            this.pnlBody.Reload();
        }

        protected void btnMultiFieldsProjection_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/report/multifieldsopexreport.aspx");
            this.pnlBody.Reload();
        }

        protected void btnCostvsDriversProjection_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/report/costvsdriversprojection.aspx");
            this.pnlBody.Reload();
        }

        protected void btnOperationalMarginsProjection_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/report/operationalmarginsprojection.aspx");
            this.pnlBody.Reload();
        }

        protected void btnDriverReport_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/report/driverreport.aspx");
            this.pnlBody.Reload();
        }

        protected void btnKPIReport_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/report/kpireport.aspx");
            this.pnlBody.Reload();
        }

        protected void btnBaseRatesByDrivers_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/technical/baseratesbydrivers.aspx");
            this.pnlBody.Reload();
        }

        protected void btnCostBenchmarking_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/report/costbenchmarking.aspx");
            this.pnlBody.Reload();
        }
        #endregion

        protected void mnuMainShore_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/technical/technicalmodule.aspx");
            this.pnlBody.Reload();
        }

        protected void btnEconomicDriverGroup_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/economic/economicdrivergroup.aspx");
            this.pnlBody.Reload();
        }

        protected void btnPricesComposition_Click(object sender, DirectEventArgs e)
        {
            this.ClearMenuSelection();
            this.pnlBody.LoadContent("/PriceComposition.aspx");
            this.pnlBody.Reload();
        }

        protected void btnModel_Click(object sender, DirectEventArgs e)
        {
            if (this.ModelProcessing())
            {
                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Do you want to close case:") + " " + NameModel + "?", new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        //Handler = "UsersX.ClickedNO()",
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
            else
            {
                Response.Redirect("/caseselection.aspx");
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            winUserEdit.Hide();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        #endregion

        #region Methods

        public Boolean GetProject()
        {
            Boolean Value = false;
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("Level IN (1,3) AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = true;
            }

            return Value;
        }

        public Boolean GetEconomicModule()
        {
            Boolean Value = false;
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("Level IN (1,2) AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = true;
            }

            return Value;
        }

        [DirectMethod]
        public void ClickedYES()
        {
            //this.UpdateStatus();
            X.Redirect("/caseselection.aspx");
        }

        //[DirectMethod]
        //public void ClickedNO()
        //{
        //    X.Redirect("/caseselection.aspx");
        //}

        protected Boolean ModelProcessing()
        {
            Boolean Value = false;
            DataClass.clsModels objModels = new DataClass.clsModels();

            DataTable dt = objModels.LoadComboBox("IdModel = " + IdModel + " AND Status = 1", " ORDER BY Name");

            if (dt.Rows.Count > 0)
            {
                Value = true;
            }
            return Value;
        }

        //public void UpdateStatus()
        //{
        //    DataClass.clsModels objModels = new DataClass.clsModels();
        //    objModels.IdModel = IdModel;
        //    objModels.loadObject();

        //    objModels.Status = 2;
        //    objModels.Update();
        //}

        protected void LoadModules()
        {
            
            #region Parameters

            // Parameters Module

            if (objApplication.AllowedRoles.Contains(3))
            {
                this.btnParameterModule.Disabled = false;
                this.mnuParameter.Disabled = false;
                // Aggregation Levels
                if (objApplication.AllowedRoles.Contains(4))
                {
                    this.btnAggregationLevels.Disabled = false;
                }
                // Types of Operations
                if (objApplication.AllowedRoles.Contains(5))
                {
                    this.btnOperations.Disabled = false;
                }
                // List of Fields
                if (objApplication.AllowedRoles.Contains(6))
                {
                    this.btnFields.Disabled = false;
                }
                // List of Cases
                if (objApplication.AllowedRoles.Contains(7))
                {
                    this.btnProjects.Disabled = false;
                }
                // Currency
                if (objApplication.AllowedRoles.Contains(8))
                {
                    this.btnCurrency.Disabled = false;
                }
                // Cost Projection Categories
                if (objApplication.AllowedRoles.Contains(44))
                {
                    this.btnZiffStructure.Disabled = false;
                }
            }
            else
            {
                this.btnParameterModule.Disabled = true;
                this.mnuParameter.Disabled = true;
            }

            #endregion

            #region Allocation

            // Allocation Module
            if (objApplication.AllowedRoles.Contains(13))
            {
                this.btnAllocationModule.Disabled = false;
                this.mnuAllocation.Disabled = false;
                // Internal Cost References (Group)
                if (objApplication.AllowedRoles.Contains(9) || objApplication.AllowedRoles.Contains(10) || objApplication.AllowedRoles.Contains(11) || objApplication.AllowedRoles.Contains(12) || objApplication.AllowedRoles.Contains(14) || objApplication.AllowedRoles.Contains(15) || objApplication.AllowedRoles.Contains(16) || objApplication.AllowedRoles.Contains(17) || objApplication.AllowedRoles.Contains(18) || objApplication.AllowedRoles.Contains(19) || objApplication.AllowedRoles.Contains(20) || objApplication.AllowedRoles.Contains(21) || objApplication.AllowedRoles.Contains(22) || objApplication.AllowedRoles.Contains(23))
                {
                    this.btnClientReferences.Disabled = false;
                }
                // Activities
                if (objApplication.AllowedRoles.Contains(9))
                {
                    this.btnActivities.Disabled = false;
                }
                // Resources
                if (objApplication.AllowedRoles.Contains(10))
                {
                    this.btnResources.Disabled = false;
                }
                // Cost Objects
                if (objApplication.AllowedRoles.Contains(11))
                {
                    this.btnCostCenters.Disabled = false;
                }
                // Chart of Accounts
                if (objApplication.AllowedRoles.Contains(12))
                {
                    this.btnClientAccounts.Disabled = false;
                }
                // Base Cost Data
                if (objApplication.AllowedRoles.Contains(14))
                {
                    this.btnClientCostData.Disabled = false;
                }
                // Direct
                if (objApplication.AllowedRoles.Contains(15))
                {
                    this.btnDirect.Disabled = false;
                }
                // Shared
                if (objApplication.AllowedRoles.Contains(16))
                {
                    this.btnShare.Disabled = false;
                }
                // Hierarchy
                if (objApplication.AllowedRoles.Contains(17))
                {
                    this.btnHierarchy.Disabled = false;
                }
                // List of Cost Allocation Drivers
                if (objApplication.AllowedRoles.Contains(18))
                {
                    this.btnListDrivers.Disabled = false;
                }
                // Cost Allocation Drivers Criteria
                if (objApplication.AllowedRoles.Contains(19))
                {
                    this.btnAllocationDrivers.Disabled = false;
                }
                // Cost Allocation Drivers Data
                if (objApplication.AllowedRoles.Contains(20))
                {
                    this.btnAllocationDriversData.Disabled = false;
                }
                // Mapping (Company Cost Structure vs. Cost Projection Categories)
                if (objApplication.AllowedRoles.Contains(22))
                {
                    this.btnZiffMapping.Disabled = false;
                }
                // Internal Base Cost References
                if (objApplication.AllowedRoles.Contains(23))
                {
                    this.btnBaseCostByField.Disabled = false;
                }
                // External Cost References (Group)
                if (objApplication.AllowedRoles.Contains(24) || objApplication.AllowedRoles.Contains(25) || objApplication.AllowedRoles.Contains(26) || objApplication.AllowedRoles.Contains(51))
                {
                    this.btnZiffReferences.Disabled = false;
                }
                // List of Peer Group
                if (objApplication.AllowedRoles.Contains(51))
                {
                    this.btnListOfPeerGroup.Disabled = false;
                }
                // List of Peer Criteria
                if (objApplication.AllowedRoles.Contains(24))
                {
                    this.btnZiffListOfCriteria.Disabled = false;
                }
                // Peer Criteria Data
                if (objApplication.AllowedRoles.Contains(25))
                {
                    this.btnZiffPeerCriteriaData.Disabled = false;
                }
                // External Base Cost References
                if (objApplication.AllowedRoles.Contains(26))
                {
                    this.btnZiffBaseCost.Disabled = false;
                }
                // Competitiveness Cost Benchmarking
                if (objApplication.AllowedRoles.Contains(54))
                {
                    this.btnCostBenchmarkingAllocation.Disabled = false;
                }
            }
            else
            {
                this.btnAllocationModule.Disabled = true;
                this.mnuAllocation.Disabled = true;
            }

            #endregion

            #region Technical

            // Technical Module
            if (objApplication.AllowedRoles.Contains(28))
            {
                this.btnTechnicalModule.Disabled = false;
                this.mnuTechnical.Disabled = false;
                // List of Technical Drivers
                if (objApplication.AllowedRoles.Contains(29))
                {
                    this.btnListTechnicaDrivers.Disabled = false;
                }
                // Technical Drivers Data
                if (objApplication.AllowedRoles.Contains(30))
                {
                    this.btnTechnicalDriversData.Disabled = false;
                }
                // Technical Assumptions
                if (objApplication.AllowedRoles.Contains(31))
                {
                    this.btnTechnicalAssumptions.Disabled = false;
                }
                // Utilization Capacity
                if (objApplication.AllowedRoles.Contains(45))
                {
                    this.btnUtilizationCapacity.Disabled = false;
                }
                // Base Rates By Cost category
                if (objApplication.AllowedRoles.Contains(32))
                {
                    this.btnBaseCostTechnicalModule.Disabled = false;
                }
                // Base Rates By Drivers
                if (objApplication.AllowedRoles.Contains(46))
                {
                    this.btnBaseRateByDriversTechnicalModule.Disabled = false;
                }
                // Cost Structure Assumptions
                if (objApplication.AllowedRoles.Contains(27))
                {
                    this.btnCostStructureAssumptions.Disabled = false;
                }
                // Price Scenarios
                if (objApplication.AllowedRoles.Contains(52))
                {
                    this.btnPriceScenarios.Disabled = false;
                }
                // Prices Data
                if (objApplication.AllowedRoles.Contains(53))
                {
                    this.btnPrices.Disabled = false;
                }
                // List of Economic Drivers
                if (objApplication.AllowedRoles.Contains(35))
                {
                    this.btnEconomicDrivers.Disabled = false;
                }
                // Economic Drivers Data
                if (objApplication.AllowedRoles.Contains(36))
                {
                    this.btnEconomicDriverData.Disabled = false;
                }
                // Economic Assumptions
                if (objApplication.AllowedRoles.Contains(37))
                {
                    this.btnEconomicAssumptions.Disabled = false;
                }
                // Base Cost Rates Forecast
                if (objApplication.AllowedRoles.Contains(38))
                {
                    this.btnPriceForecast.Disabled = false;
                }
                // Operational Margins Projection
                if (objApplication.AllowedRoles.Contains(55))
                {
                    this.btnOperationalMarginsProjectionCPM.Disabled = false;
                }
                // Projection by Cost Category
                if (objApplication.AllowedRoles.Contains(56))
                {
                    this.btnCostvsDriversProjectionCPM.Disabled = false;
                }
                // Projection by Development Case
                if (objApplication.AllowedRoles.Contains(57))
                {
                    this.btnOpexReportCPM.Disabled = false;
                }
                // Projection by Cost Type
                if (objApplication.AllowedRoles.Contains(58))
                {
                    this.btnOpexReportDetailedCPM.Disabled = false;
                }
                // Multiple Fields Projection
                if (objApplication.AllowedRoles.Contains(59))
                {
                    this.btnMultiFieldsProjectionCPM.Disabled = false;
                }
            }
            else
            {
                this.btnTechnicalModule.Disabled = true;
                this.mnuTechnical.Disabled = true;
            }

            #endregion

            #region Economic

            // Economic Module
            //if (objApplication.AllowedRoles.Contains(34))
            //{
            //    this.btnEconomicModule.Disabled = false;
            //    this.mnuEconomic.Disabled = false;
            //    // List of Economic Drivers
            //    if (objApplication.AllowedRoles.Contains(35))
            //    {
            //        this.btnEconomicDrivers.Disabled = false;
            //    }
            //    // Economic Drivers Data
            //    if (objApplication.AllowedRoles.Contains(36))
            //    {
            //        this.btnEconomicDriverData.Disabled = false;
            //    }
            //    // Economic Assumptions
            //    if (objApplication.AllowedRoles.Contains(37))
            //    {
            //        this.btnEconomicAssumptions.Disabled = false;
            //    }
            //    // Base Cost Rates Forecast
            //    if (objApplication.AllowedRoles.Contains(38))
            //    {
            //        this.btnPriceForecast.Disabled = false;
            //    }
            //    // Ziff Economic Analysis
            //    if (objApplication.AllowedRoles.Contains(39))
            //    {
            //        this.btnZiffEconomicAnalysis.Disabled = false;
            //    }
            //}
            //else
            //{
            //    this.btnEconomicModule.Disabled = true;
            //    this.mnuEconomic.Disabled = true;
            //}

            #endregion

            #region Reports

            // Reports Module
            if (objApplication.AllowedRoles.Contains(40))
            {
                this.btnReportModule.Disabled = false;
                this.mnuReport.Disabled = false;
                // Operational Margins Projection
                if (objApplication.AllowedRoles.Contains(60))
                {
                    this.btnOperationalMarginsProjection.Disabled = false;
                }
                // Projection by Cost Category
                if (objApplication.AllowedRoles.Contains(61))
                {
                    this.btnCostvsDriversProjection.Disabled = false;
                }
                // Projection by Development Case
                if (objApplication.AllowedRoles.Contains(41))
                {
                    this.btnOpexReport.Disabled = false;
                }
                // Projection by Cost Type
                if (objApplication.AllowedRoles.Contains(62))
                {
                    this.btnOpexReportDetailed.Disabled = false;
                }
                // Multiple Fields Projection
                if (objApplication.AllowedRoles.Contains(63))
                {
                    this.btnMultiFieldsProjection.Disabled = false;
                }
                // Competitiveness Cost Benchmarking
                if (objApplication.AllowedRoles.Contains(64))
                {
                    this.btnCostBenchmarking.Disabled = false;
                }
            }
            else
            {
                this.btnReportModule.Disabled = true;
                this.mnuReport.Disabled = true;
            }

            #endregion

        }

        protected void UserLoad()
        {
            DataClass.clsUsers objUsers = new DataClass.clsUsers();
            DataTable dt = objUsers.LoadList("IdUser = " + IdUser, "");
            if (dt.Rows.Count > 0)
            {
                this.txtUserName.Text = (String)dt.Rows[0]["UserName"];
                this.txtName.Text = (String)dt.Rows[0]["Name"];
                this.txtPassword.Text = CD.DAC.Decrypt((String)dt.Rows[0]["Password"]);
                this.txtEmail.Text = (String)dt.Rows[0]["Email"];
            }
        }

        protected void CreateMenu()
        {
            try
            {
                btnAdmin.Text = NameUser;
                btnAdmin.Icon = Icon.UserEdit;
                btnAdmin.ToolTip = modMain.strMasterAdmin;
                btnMainUser.Text = NameUser;
                btnMainUser.Icon = Icon.User;
                btnMainUser.ToolTip = modMain.strMasterAdmin;
                btnUser.Text = modMain.strMasterUser;
                btnUser.Icon = Icon.Group;
                btnUser.ToolTip = modMain.strMasterUser;
                btnSetting.Text = modMain.strMasterSetting;
                btnSetting.Icon = Icon.TableKey;
                btnSetting.ToolTip = modMain.strMasterSetting;
                btnLogon.Text = modMain.strMasterLogon;
                btnLogon.Icon = Icon.KeyDelete;
                btnLogon.ToolTip = modMain.strMasterLogon;

                if (!this.IsAdministrator())
                {
                    btnUser.Hidden = true;
                    btnSetting.Hidden = true;
                }
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
            }
        }

        protected void LoadLanguage()
        {
            DataClass.clsLabelsLanguages objLabelsLanguages = new DataClass.clsLabelsLanguages();
            DataTable dt = objLabelsLanguages.LoadList("LanguageName = '" + Language + "' AND FormName = 'MasterPage'", "ORDER BY IdLabel");
            foreach (DataRow row in dt.Rows)
            {
                //idLanguage = (Int32)row["IdLanguage"];
                switch ((String)row["LabelName"])
                {
                    case "Title Form":
                        modMain.strMasterTitleForm = (String)row["LabelText"];
                        break;
                    case "Application Home":
                        modMain.strMasterHome = (String)row["LabelText"];
                        break;
                    case "Admin":
                        modMain.strMasterAdmin = (String)row["LabelText"];
                        break;
                    case "User":
                        modMain.strMasterUser = (String)row["LabelText"];
                        break;
                    case "Setting":
                        modMain.strMasterSetting = (String)row["LabelText"];
                        break;
                    case "Logon":
                        modMain.strMasterLogon = (String)row["LabelText"];
                        break;
                    case "LastLogin":
                        modMain.strMasterLastLogin = (String)row["LabelText"];
                        break;
                    case "Windows Title User":
                        modMain.strMasterWindowsTitleUser = (String)row["LabelText"];
                        break;
                    case "Form UserName":
                        modMain.strMasterFormUserName = (String)row["LabelText"];
                        break;
                    case "Form Name":
                        modMain.strMasterFormName = (String)row["LabelText"];
                        break;
                    case "Form Password":
                        modMain.strMasterFormPassword = (String)row["LabelText"];
                        break;
                    case "Form Email":
                        modMain.strMasterFormEmail = (String)row["LabelText"];
                        break;
                    case "Form Accept Button":
                        modMain.strMasterFormAcceptButton = (String)row["LabelText"];
                        break;
                    case "Form Cancel Button":
                        modMain.strMasterFormCancelButton = (String)row["LabelText"];
                        break;
                    case "Valid":
                        modMain.strMasterValid = (String)row["LabelText"];
                        break;
                    case "Parameters":
                        modMain.strMasterParametersModule = (String)row["LabelText"];
                        break;
                    case "Allocation":
                        modMain.strMasterAllocationModule = (String)row["LabelText"];
                        break;
                    case "Technical":
                        modMain.strMasterTechnicalModule = (String)row["LabelText"];
                        break;
                    case "Economic":
                        modMain.strMasterEconomicModule = (String)row["LabelText"];
                        break;
                    case "Reports":
                        modMain.strMasterReportsModule = (String)row["LabelText"];
                        break;
                    case "Case Selection":
                        modMain.strMasterCaseSelection = (String)row["LabelText"];
                        break;
                    case "Modules":
                        modMain.strMasterModules = (String)row["LabelText"];
                        break;
                    case "Selected Case":
                        modMain.strMasterSelectedCase = (String)row["LabelText"];
                        break;
                    case "Aggregation Levels":
                        modMain.strMasterAggregationLevels = (String)row["LabelText"];
                        break;
                    case "Types of Operations":
                        modMain.strMasterTypesofOperations = (String)row["LabelText"];
                        break;
                    case "List of Fields":
                        modMain.strMasterListofFields = (String)row["LabelText"];
                        break;
                    case "Field Projects":
                        modMain.strMasterFieldProjects = (String)row["LabelText"];
                        break;
                    case "Currency":
                        modMain.strMasterCurrency = (String)row["LabelText"];
                        break;
                    case "Activities":
                        modMain.strMasterActivities = (String)row["LabelText"];
                        break;
                    case "Resources":
                        modMain.strMasterResources = (String)row["LabelText"];
                        break;
                    case "Cost Objects":
                        modMain.strMasterCostObjects = (String)row["LabelText"];
                        break;
                    case "Chart of Accounts":
                        modMain.strMasterChartAccounts = (String)row["LabelText"];
                        break;
                    case "Ziff/Third-party Cost Categories":
                        modMain.strMasterZiff_Third_partyCostCategories = (String)row["LabelText"];
                        break;
                    case "Internal Cost References":
                        modMain.strMasterCompanyCostReferences = (String)row["LabelText"];
                        break;
                    case "External Cost References":
                        modMain.strMasterZiff_Third_partyCostReferences = (String)row["LabelText"];
                        break;
                    case "Cost Structure Assumptions":
                        modMain.strMasterCostStructureAssumptions = (String)row["LabelText"];
                        break;
                    case "Base Cost Data":
                        modMain.strMasterBaseCostData = (String)row["LabelText"];
                        break;
                    case "Direct":
                        modMain.strMasterDirect = (String)row["LabelText"];
                        break;
                    case "Shared":
                        modMain.strMasterShared = (String)row["LabelText"];
                        break;
                    case "Hierarchy":
                        modMain.strMasterHierarchy = (String)row["LabelText"];
                        break;
                    case "List of Allocation Drivers":
                        modMain.strMasterListofAllocationDrivers = (String)row["LabelText"];
                        break;
                    case "Allocation Drivers Criteria":
                        modMain.strMasterAllocationDriversCriteria = (String)row["LabelText"];
                        break;
                    case "Allocation Drivers Data":
                        modMain.strMasterAllocationDriversData = (String)row["LabelText"];
                        break;
                    case "Base Cost by Field (Company Structure)":
                        modMain.strMasterBaseCostbyFieldCompany = (String)row["LabelText"];
                        break;
                    case "Mapping (Company Cost Structure vs. Ziff/Third-party Cost Categories)":
                        modMain.strMasterMapping = (String)row["LabelText"];
                        break;
                    case "Base Cost by Field (Ziff/Third-party Cost Categories)":
                        modMain.strMasterBaseCostbyFieldZiff = (String)row["LabelText"];
                        break;
                    case "List of Peer Criteria":
                        modMain.strMasterListofPeerCriteria = (String)row["LabelText"];
                        break;
                    case "Peer Criteria Data":
                        modMain.strMasterPeerCriteriaData = (String)row["LabelText"];
                        break;
                    case "Ziff/Third-party Base Cost":
                        modMain.strMasterZiff_Third_partyBaseCost = (String)row["LabelText"];
                        break;
                    case "List Technical Drivers":
                        modMain.strMasterListTechnicalDrivers = (String)row["LabelText"];
                        break;
                    case "Technical Drivers Data":
                        modMain.strMasterTechnicalDriversData = (String)row["LabelText"];
                        break;
                    case "Technical Assumptions":
                        modMain.strMasterTechnicalAssumptions = (String)row["LabelText"];
                        break;
                    case "Base Cost Rates":
                        modMain.strMasterBaseCostRates = (String)row["LabelText"];
                        break;
                    case "Technical Files Repository":
                        modMain.strMasterZiffTechnicalAnalysis = (String)row["LabelText"];
                        break;
                    case "List of Economic Drivers":
                        modMain.strMasterListofEconomicDrivers = (String)row["LabelText"];
                        break;
                    case "Economic Drivers Data":
                        modMain.strMasterEconomicDriversData = (String)row["LabelText"];
                        break;
                    case "Economic Assumptions":
                        modMain.strMasterEconomicAssumptions = (String)row["LabelText"];
                        break;
                    case "Base Cost Rates Forecast":
                        modMain.strMasterBaseCostRatesForecast = (String)row["LabelText"];
                        break;
                    case "Economic File Repository":
                        modMain.strMasterZiffEconomicAnalysis = (String)row["LabelText"];
                        break;
                    case "Projection by Cost Category":
                        modMain.strMasterOperatingCostProjection = (String)row["LabelText"];
                        break;
                    case "Technical Drivers Forecast":
                        modMain.strMasterTechnicalDriversForecast = (String)row["LabelText"];
                        break;
                    case "KPI Estimates":
                        modMain.strMasterKPIEstimates = (String)row["LabelText"];
                        break;
                    case "Utilization Capacity":
                        modMain.strTechnicalModuleUtilizationCapacity = (String)row["LabelText"];
                        break;
                    case "Base Rates By Drivers":
                        modMain.strTechnicalModuleBaseRatesByDrivers = (String)row["Labeltext"];
                        break;
                    case "Projection by Field Project":
                        modMain.strReportsModuleOperatingCostProjectionDetailed = (String)row["Labeltext"];
                        break;
                    case "Base Rates By Cost Category":
                        modMain.strTechnicalModuleBaseRatesByCostCategory = (String)row["LabelText"];
                        break;
                    case "Internal Base Cost References":
                        modMain.strReportsModuleCompanyBaseCostReferences = (String)row["LabelText"];
                        break;
                    case "External Base Cost References":
                        modMain.strReportsModuleZiffBaseCostReferences = (String)row["LabelText"];
                        break;
                    case "Detailed Projection":
                        modMain.strReportsModuleDetailedProjection = (String)row["LabelText"];
                        break;
                }
            }

            //Economic Module
            dt = objLabelsLanguages.LoadList("LanguageName = '" + Language + "' AND FormName = 'EconomicModule'", "ORDER BY IdLabel");
            foreach (DataRow row in dt.Rows)
            {
                //idLanguage = (Int32)row["IdLanguage"];
                switch ((String)row["LabelName"])
                {
                    case "Economic Module":
                        modMain.strEconomicModuleFormTitle = (String)row["LabelText"];
                        break;
                    case "Economic Drivers":
                        modMain.strEconomicModuleEconomicDrivers = (String)row["LabelText"];
                        break;
                    case "Economic Driver Group":
                        modMain.strEconomicModuleEconomicDriverGroup = (String)row["LabelText"];
                        break;
                    case "Manage Economic Driver Groups":
                        modMain.strEconomicModuleManageEconomicDriverGroups = (String)row["LabelText"];
                        break;
                    case "Add New Economic Driver":
                        modMain.strEconomicModuleAddNewEconomicDriver = (String)row["LabelText"];
                        break;
                    case "Driver Group":
                        modMain.strEconomicModuleDriverGroup = (String)row["LabelText"];
                        break;
                    case "Economic Driver":
                        modMain.strEconomicModuleEconomicDriver = (String)row["LabelText"];
                        break;
                    case "Import Economic Drivers":
                        modMain.strEconomicModuleImportEconomicDrivers = (String)row["LabelText"];
                        break;
                    case "Add New Economic Driver Group":
                        modMain.strEconomicModuleAddNewEconomicDriverGroup = (String)row["LabelText"];
                        break;
                    case "List of Economic Drivers":
                        modMain.strEconomicModuleListofEconomicDrivers = (String)row["LabelText"];
                        break;
                    case "Pessimistic":
                        modMain.strEconomicModulePessimistic = (String)row["LabelText"];
                        break;
                    case "Optimistic":
                        modMain.strEconomicModuleOptimistic = (String)row["LabelText"];
                        break;
                    case "Import Economic Driver Data":
                        modMain.strEconomicModuleImportEconomicDriverData = (String)row["LabelText"];
                        break;
                    case "Economic Scenarios:":
                        modMain.strEconomicModuleEconomicScenarios = (String)row["LabelText"];
                        break;
                }
            }
            //Report Module
            dt = objLabelsLanguages.LoadList("LanguageName = '" + Language + "' AND FormName = 'ReportsModule'", "ORDER BY IdLabel");
            foreach (DataRow row in dt.Rows)
            {
                //idLanguage = (Int32)row["IdLanguage"];
                switch ((String)row["LabelName"])
                {
                    case "Reports Module":
                        modMain.strReportsModuleFormTitle = (String)row["LabelText"];
                        break;
                    case "Key Performance Indicators Estimates":
                        modMain.strReportsModuleKPIEstimatesLong = (String)row["LabelText"];
                        break;
                    case "Driver:":
                        modMain.strReportsModuleDriver = (String)row["LabelText"];
                        break;
                    case "Technical Driver #":
                        modMain.strReportsModuleTechnicalDriverNum = (String)row["LabelText"];
                        break;
                    case "Key Performance Cost Indicators Estimates":
                        modMain.strReportsModuleSubFormTitle3 = (String)row["LabelText"];
                        break;
                    case "Aggregation Level:":
                        modMain.strReportsModuleAggregationLevel = (String)row["LabelText"];
                        break;
                    case "Cost Structure:":
                        modMain.strReportsModuleCostStructure = (String)row["LabelText"];
                        break;
                    case "From:":
                        modMain.strReportsModuleFrom = (String)row["LabelText"];
                        break;
                    case "To:":
                        modMain.strReportsModuleTo = (String)row["LabelText"];
                        break;
                    case "Technical Scenario:":
                        modMain.strReportsModuleTechnicalScenario = (String)row["LabelText"];
                        break;
                    case "Economic Scenario:":
                        modMain.strReportsModuleEconomicScenario = (String)row["LabelText"];
                        break;
                    case "Currency:":
                        modMain.strReportsModuleCurrency = (String)row["LabelText"];
                        break;
                    case "Term:":
                        modMain.strReportsModuleTerm = (String)row["LabelText"];
                        break;
                    case "Run Report":
                        modMain.strReportsModuleRunReport = (String)row["LabelText"];
                        break;
                    case "KPI Target":
                        modMain.strReportsModuleKPITarget = (String)row["LabelText"];
                        break;
                    case "Years":
                        modMain.strReportsModuleYears = (String)row["LabelText"];
                        break;
                    case "Reset":
                        modMain.strReportsModuleReset = (String)row["LabelText"];
                        break;
                    case "Export Report Data To Excel":
                        modMain.strReportsModuleExportReportDataToExcel = (String)row["LabelText"];
                        break;
                    case "Technical Drivers Forecast Indicator":
                        modMain.strReportsModuleResultsTitle = (String)row["LabelText"];
                        break;
                    case "Total Projection":
                        modMain.strReportsModuleOpexResultsTitle1 = (String)row["LabelText"];
                        break;
                    case "Projection By Project":
                        modMain.strReportsModuleOpexResultsTitle2 = (String)row["LabelText"];
                        break;
                    case "Baseline":
                        modMain.strReportsModuleOpexResultsSubTitle1 = (String)row["LabelText"];
                        break;
                    case "Business Opportunities":
                        modMain.strReportsModuleOpexResultsSubTitle2 = (String)row["LabelText"];
                        break;
                    case "1. Cost Projection Reports":
                        modMain.strReportsModuleFormSubTitle1 = (String)row["LabelText"];
                        break;
                    case "2. Projection Base Rates Reports":
                        modMain.strReportsModuleFormSubTitle2 = (String)row["LabelText"];
                        break;
                    case "3. Historical Base Cost Reports":
                        modMain.strReportsModuleFormSubTitle3 = (String)row["LabelText"];
                        break;
                    case "Type of Operation":
                        modMain.strReportsModuleFormOperationType = (String)row["LabelText"];
                        break;
                    case "Cost Type":
                        modMain.strReportsModuleFormTypeCost = (String)row["Labeltext"];
                        break;
                }
            }
            this.LoadLabel();
        }

        protected void LoadLabel()
        {
            //<TopBar>
            this.btnHelp.Text = modMain.strCommonHelp;
            //this.pnlUser.Title = modMain.strMasterWindowsTitleUser;
            this.lblLastLogin.Text = modMain.strMasterLastLogin + " " + LastLogin.ToString("yyyy-MM-dd H:mm:ss tt");
            this.winUserEdit.Title = modMain.strMasterWindowsTitleUser;

            this.txtUserName.EmptyText = modMain.strMasterFormUserName;
            this.txtUserName.FieldLabel = modMain.strMasterFormUserName;
            this.txtUserName.BlankText = modMain.strMasterValid;
            this.txtUserName.Reset();

            this.txtName.EmptyText = modMain.strMasterFormName;
            this.txtName.FieldLabel = modMain.strMasterFormName;
            this.txtName.BlankText = modMain.strMasterValid;
            this.txtName.Reset();

            this.txtPassword.EmptyText = modMain.strMasterFormPassword;
            this.txtPassword.FieldLabel = modMain.strMasterFormPassword;
            this.txtPassword.BlankText = modMain.strMasterValid;
            this.txtPassword.Reset();

            this.txtEmail.EmptyText = modMain.strMasterFormEmail;
            this.txtEmail.FieldLabel = modMain.strMasterFormEmail;
            this.txtEmail.BlankText = modMain.strMasterValid;
            this.txtEmail.Reset();

            this.btnSave.Text = modMain.strMasterFormAcceptButton;
            this.btnSave.Icon = Icon.DatabaseSave;
            this.btnCancel.Text = modMain.strMasterFormCancelButton;
            this.btnCancel.Icon = Icon.Decline;

            this.btnRepositories.Text = (String)GetGlobalResourceObject("CPM_Resources", "Files Repository");

            //<BottomBar>
            this.btnParameterModule.Text = "<B>" + modMain.strMasterParametersModule + "</B>";
            this.btnParameterModule.ToolTip = modMain.strMasterParametersModule;
            this.btnAllocationModule.Text = "<B>" + modMain.strMasterAllocationModule + "</B>";
            this.btnAllocationModule.ToolTip = modMain.strMasterAllocationModule;
            this.btnTechnicalModule.Text = "<B>" + modMain.strMasterTechnicalModule + "</B>";
            this.btnTechnicalModule.ToolTip = modMain.strMasterTechnicalModule;
            this.btnEconomicModule.Text = "<B>" + modMain.strMasterEconomicModule + "</B>";
            this.btnEconomicModule.ToolTip = modMain.strMasterEconomicModule;
            this.btnReportModule.Text = "<B>" + modMain.strMasterReportsModule + "</B>";
            this.btnReportModule.ToolTip = modMain.strMasterReportsModule;
            this.btnModels.Text = "<B>" + modMain.strMasterCaseSelection + "</B>";
            this.btnModels.ToolTip = modMain.strMasterCaseSelection;

            //<MenuPanel>
            this.pnlSettings.Title = modMain.strMasterModules;
            this.mnuParameter.Title = modMain.strMasterParametersModule;
            this.btnAggregationLevels.Text = modMain.strMasterAggregationLevels;
            this.btnOperations.Text = modMain.strMasterTypesofOperations;
            this.btnFields.Text = modMain.strMasterListofFields;
            this.btnProjects.Text = modMain.strMasterFieldProjects;
            this.btnCurrency.Text = modMain.strMasterCurrency;
            this.btnPrices.Text = (String)GetGlobalResourceObject("CPM_Resources", "Prices");
            this.btnPriceScenarios.Text = (String)GetGlobalResourceObject("CPM_Resources", "PriceScenarios");
            this.btnZiffStructure.Text = modMain.strMasterZiff_Third_partyCostCategories;

            this.mnuAllocation.Title = modMain.strMasterAllocationModule;
            this.btnActivities.Text = modMain.strMasterActivities;
            this.btnResources.Text = modMain.strMasterResources;
            this.btnCostCenters.Text = modMain.strMasterCostObjects;
            this.btnClientAccounts.Text = modMain.strMasterChartAccounts;
            this.btnClientReferences.Text = modMain.strMasterCompanyCostReferences;
            this.btnClientCostData.Text = modMain.strMasterBaseCostData;
            this.btnDirect.Text = modMain.strMasterDirect;
            this.btnShare.Text = modMain.strMasterShared;
            this.btnHierarchy.Text = modMain.strMasterHierarchy;
            this.btnListDrivers.Text = modMain.strMasterListofAllocationDrivers;
            this.btnAllocationDrivers.Text = modMain.strMasterAllocationDriversCriteria;
            this.btnAllocationDriversData.Text = modMain.strMasterAllocationDriversData;
            this.btnBaseCostByField.Text = modMain.strMasterBaseCostbyFieldCompany;
            this.btnZiffMapping.Text = modMain.strMasterMapping;
            this.btnBaseCostByFieldZiff.Text = modMain.strMasterBaseCostbyFieldZiff;
            this.btnZiffReferences.Text = modMain.strMasterZiff_Third_partyCostReferences;
            this.btnListOfPeerGroup.Text = (String)GetGlobalResourceObject("CPM_Resources", "ListOfPeerGroup");
            this.btnZiffListOfCriteria.Text = modMain.strMasterListofPeerCriteria;
            this.btnZiffPeerCriteriaData.Text = modMain.strMasterPeerCriteriaData;
            this.btnZiffBaseCost.Text = modMain.strMasterZiff_Third_partyBaseCost;
            this.btnInternalBenchmarkRepository.Text = (String)GetGlobalResourceObject("CPM_Resources", "Internal Benchmarks Files Repository");
            this.btnExternalBenchmarkRepository.Text = (String)GetGlobalResourceObject("CPM_Resources", "External Benchmarks Files Repository");
            this.btnCostStructureAssumptions.Text = modMain.strMasterCostStructureAssumptions;
            this.btnCostBenchmarkingAllocation.Text = (String)GetGlobalResourceObject("CPM_Resources", "CostBenchmarking");

            this.mnuTechnical.Title = modMain.strMasterTechnicalModule;
            this.btnListTechnicaDrivers.Text = modMain.strMasterListTechnicalDrivers;
            this.btnTechnicalDriversData.Text = modMain.strMasterTechnicalDriversData;
            this.btnTechnicalAssumptions.Text = modMain.strMasterTechnicalAssumptions;
            this.btnBaseCostTechnicalModule.Text = modMain.strMasterBaseCostRates;
            this.btnZiffTechnicalAnalysis.Text = modMain.strMasterZiffTechnicalAnalysis;
            this.btnUtilizationCapacity.Text = modMain.strTechnicalModuleUtilizationCapacity;
            this.btnBaseRateByDriversTechnicalModule.Text = modMain.strTechnicalModuleBaseRatesByDrivers;
            this.btnOpexReportCPM.Text = (String)GetGlobalResourceObject("CPM_Resources", "Projection By Cost Category");
            this.btnOpexReportDetailedCPM.Text = (String)GetGlobalResourceObject("CPM_Resources", "Projection By Case");
            this.btnMultiFieldsProjectionCPM.Text = (String)GetGlobalResourceObject("CPM_Resources", "Multiple Fields Projection");
            this.btnCostvsDriversProjectionCPM.Text = (String)GetGlobalResourceObject("CPM_Resources", "CostVSDriversProjection");
            this.btnOperationalMarginsProjectionCPM.Text = (String)GetGlobalResourceObject("CPM_Resources", "OperationalMarginsProjection");

            this.mnuEconomic.Title = modMain.strMasterEconomicModule;
            this.btnEconomicDrivers.Text = modMain.strMasterListofEconomicDrivers;
            this.btnEconomicDriverData.Text = modMain.strMasterEconomicDriversData;
            this.btnEconomicAssumptions.Text = modMain.strMasterEconomicAssumptions;
            this.btnPriceForecast.Text = modMain.strMasterBaseCostRatesForecast;
            this.btnZiffEconomicAnalysis.Text = modMain.strMasterZiffEconomicAnalysis;

            this.mnuReport.Title = modMain.strMasterReportsModule;
            this.btnOpexReport.Text = (String)GetGlobalResourceObject("CPM_Resources", "Projection By Cost Category");
            this.btnOpexReportDetailed.Text = (String)GetGlobalResourceObject("CPM_Resources", "Projection By Case");
            this.btnMultiFieldsProjection.Text = (String)GetGlobalResourceObject("CPM_Resources", "Multiple Fields Projection");
            this.btnCostvsDriversProjection.Text = (String)GetGlobalResourceObject("CPM_Resources", "CostVSDriversProjection");
            this.btnOperationalMarginsProjection.Text = (String)GetGlobalResourceObject("CPM_Resources", "OperationalMarginsProjection");
            this.btnBaseCostTechnicalModuleReport.Text = modMain.strTechnicalModuleBaseRatesByCostCategory;
            this.btnBaseRatesByDrivers.Text = modMain.strTechnicalModuleBaseRatesByDrivers;
            this.btnBaseCostByFieldReportMerged.Text = modMain.strReportsModuleCompanyBaseCostReferences;
            this.btnZiffBaseCostReport.Text = modMain.strReportsModuleZiffBaseCostReferences;
            this.btnCostBenchmarking.Text = (String)GetGlobalResourceObject("CPM_Resources", "CostBenchmarking");
        }

        protected void SaveUser()
        {
            DataClass.clsUsers objUsers = new DataClass.clsUsers();
            CD.DAC DAC = new CD.DAC();

            objUsers.IdUser = IdUser;
            objUsers.loadObject();

            if (IdUser == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (objUsers.UserName != txtUserName.Text)
                {
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }

            objUsers.UserName = this.txtUserName.Text;
            objUsers.Name = this.txtName.Text;
            objUsers.Password = CD.DAC.Encrypt(this.txtPassword.Text);
            objUsers.Email = this.txtEmail.Text;
            objUsers.DateModification = DateTime.Now;
            objUsers.UserModification = IdUser;
            objUsers.Update();
            NameUser = this.txtName.Text;
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winUserEdit.Hide();

        }

        #endregion

        #region Functions

        protected void ClearMenuSelection()
        {
            this.mnuParameter.ClearSelection();
            this.mnuAllocation.ClearSelection();
            this.mnuTechnical.ClearSelection();
            this.mnuEconomic.ClearSelection();
            this.mnuReport.ClearSelection();
        }

        protected void LoadApplication()
        {
            if (!objApplication.ModuleParameter)
            {
                this.mnuParameter.Hidden = true;
                this.btnParameterModule.Hidden = true;
            }
            if (!objApplication.ModuleAllocation)
            {
                this.mnuAllocation.Hidden = true;
                this.btnAllocationModule.Hidden = true;
                this.sepAllocation.Hidden = true;
            }
            if (!objApplication.ModuleTechnical)
            {
                this.mnuTechnical.Hidden = true;
                this.btnTechnicalModule.Hidden = true;
                this.sepTechnical.Hidden = true;
            }
            if (!objApplication.ModuleEconomic)
            {
                this.mnuEconomic.Hidden = true;
                this.btnEconomicModule.Hidden = true;
                //this.sepEconomic.Hidden = true;
            }
            if (!objApplication.ModuleReports)
            {
                this.mnuReport.Hidden = true;
                this.btnReportModule.Hidden = true;
                this.sepReports.Hidden = true;
            }
            this.lblVersion.Text = objApplication.AppVersion;
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsUsers objUsers = new DataClass.clsUsers();
            DataTable dt = objUsers.LoadList("UserName = '" + txtUserName.Text + "'", "");
            Value = dt.Rows.Count;

            return Value;
        }

        protected Boolean IsAdministrator()
        {
            Boolean Value = false;
            DataClass.clsProfiles objProfiles = new DataClass.clsProfiles();

            DataTable dt = objProfiles.LoadList("Type = 1 AND IdProfile = " + this.IdProfile(), "ORDER BY Name");

            if (dt.Rows.Count > 0)
            {
                Value = true;
            }
            return Value;
        }

        protected Int32 IdProfile()
        {
            Int32 Value = 0;
            DataClass.clsUsers objUsers = new DataClass.clsUsers();

            DataTable dt = objUsers.LoadList("IdUser = " + IdUser, "ORDER BY Name");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdProfile"];
            }
            return Value;
        }

        protected Boolean ValidForm()
        {
            Boolean Valid = true;
            if (this.txtUserName.Text == "")
            {
                Valid = false;
            }
            else if (this.txtName.Text == "")
            {
                Valid = false;
            }
            else if (this.txtPassword.Text == "")
            {
                Valid = false;
            }
            else if (this.txtEmail.Text == "")
            {
                Valid = false;
            }
            return Valid;
        }

        #endregion

    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="technicalassumption.aspx.cs" Inherits="CPM._technicalassumption" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var pinEditors = function (btn, pressed) {
            var columnConfig = btn.column,
                column = columnConfig.column;

            if (pressed) {
                column.pinOverComponent();
                column.showComponent(columnConfig.record, true);
            } else {
                column.unpinOverComponent();
                column.hideComponent(true);
            }
        };

        var criteriarenderer = function (value) {
            var r = App.storeProjectionCriteria.getById(value);
            if (Ext.isEmpty(r)) {
                return "";
            }
            return r.data.Name;
        }

        var sizedriverrenderer = function (value) {
            var r = App.storeSizeDriver.getById(value);
            if (Ext.isEmpty(r)) {
                return "";
            }
            return r.data.Name;
        }

        var performancedriverrenderer = function (value) {
            var r = App.storePerformanceDriver.getById(value);
            if (Ext.isEmpty(r)) {
                return "";
            }
            return r.data.Name;
        }

        var beforeCellEditHandler = function (e) {
            if (e.record.data["DisplayName"].substring(0, 5) != '    -') {
                App.CellEditing1.cancelEdit();
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" 
                AutoScroll="true" 
                runat="server" 
                Title="Technical Assumptions" 
                IconCls="icon-technicalassumptions_16" 
                Border="false" 
                Layout="BorderLayout">
                <Items>
                    <ext:Panel ID="Panel1" 
                        runat="server" 
                        Layout="HBoxLayout" 
                        Border="false" 
                        Style="margin: 0 0 0 0" 
                        region="North">
                        <Items>
                            <ext:Label Style="margin: 10px 0 10px 10px" ID="lblFilterTypeOperation" runat="server" Text="Type of Operation:"></ext:Label>
                            <ext:ComboBox Style="margin: 10px 0 10px 10px" ID="ddlFilterTypeOperation" runat="server" Editable="true" ForceSelection="true" TypeAhead="false" Width="200" MinChars="0" DisplayField="VisualName" ValueField="IdTypeOperation">
                                <ListConfig LoadingText="Searching..." MinWidth="300">
                                    <ItemTpl ID="ItemTpl2" runat="server">
                                        <Html>
                                            <div class="search-item">
							                    <h3><span>Type: {ShoreDesc} | {TypeDesc}</span>{Name}</h3>
							                    <span>Parent: </span>{ParentName}</h3>
						                    </div>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Store>
                                    <ext:Store ID="storeFilterTypeOperation" runat="server" OnReadData="StoreFilterTypeOperation_ReadData">
                                        <Model>
                                            <ext:Model ID="Model6" runat="server" IDProperty="IdTypeOperation">
                                                <Fields>
                                                    <ext:ModelField Name="VisualName" />
                                                    <ext:ModelField Name="ParentName" />
                                                    <ext:ModelField Name="ShoreDesc" />
                                                    <ext:ModelField Name="TypeDesc" />
                                                    <ext:ModelField Name="IdTypeOperation" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                                <Reader>
                                                    <ext:JsonReader />
                                                </Reader>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <DirectEvents>
                                    <Select OnEvent="ddlFilterTypeOperation_Select">
                                        <EventMask ShowMask="true" />
                                    </Select>
                                </DirectEvents>
                            </ext:ComboBox>
                        </Items>
                    </ext:Panel>
                    <ext:GridPanel ID="grdTechnicalAssumptions" 
                        runat="server" 
                        AutoScroll="true" 
                        Border="false" 
                        Layout="AutoLayout" 
                        StripeRows="true" 
                        EnableLocking="true" 
                        Header="true" 
                        Region="Center">
                        <TopBar>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btnSave" Icon="DataBaseSave" ToolTip="Save Changes" runat="server" Text="Save Changes">
                                        <DirectEvents>
                                            <Click OnEvent="btnSave_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="rowsValues" Value="#{grdTechnicalAssumptions}.getRowsValues(false)" Mode="Raw" Encode="true" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarSeparator />
                                    <ext:Button ID="btnImport" Icon="BookAdd" TextAlign="Center" ToolTip="Import Technical Driver Data" runat="server" Text="Import">
                                        <DirectEvents>
                                            <Click OnEvent="btnImport_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="rowsValues" Value="#{grdTechnicalAssumptions}.getRowsValues(false)"
                                                        Mode="Raw" Encode="true" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarFill />
                                    <ext:Button ID="btnSaveExcel" runat="server" Text="Export Data To Excel" Icon="PageExcel">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveExcel_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnDownloadTemplate" runat="server" Text="Download Import Template" Icon="PageSave">
                                        <DirectEvents>
                                            <Click OnEvent="btnDownloadTemplate_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="storeTechnicalAssumption" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="IdTechnicalAssumption">
                                        <Fields>
                                            <ext:ModelField Name="IdTechnicalAssumption" Type="Int" />
                                            <ext:ModelField Name="IdZiffAccount" Type="Int" />
                                            <ext:ModelField Name="DisplayName" Type="String" />
                                            <ext:ModelField Name="CodeZiffAccount" Type="String" />
                                            <ext:ModelField Name="NameZiffAccount" Type="String" />
                                            <ext:ModelField Name="ProjectionCriteria" Type="Int" />
                                            <ext:ModelField Name="IdTechnicalDriverSize" Type="int" />
                                            <ext:ModelField Name="IdTechnicalDriverPerformance" Type="int" />
                                            <ext:ModelField Name="CycleValue" Type="int" />
                                            <ext:ModelField Name="NameProjectionCriteria" Type="String" />
                                            <ext:ModelField Name="NameTechnicalSize" Type="String" />
                                            <ext:ModelField Name="NameTechnicalPerformance" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel ID="ColumnModel1" runat="server">
                            <Columns>
                                <ext:Column ID="colID" runat="server" Text="ID" DataIndex="IdTechnicalAssumption" Hidden="true" />
                                <ext:Column ID="IdZiff" runat="server" Text="IDZiff" DataIndex="IdZiffAccount" Hidden="true" />
                                <ext:Column ID="CodeZiffAccount" runat="server" Text="Code" DataIndex="CodeZiffAccount" Hidden="true"/>
                                <ext:Column ID="NameZiffAccount" runat="server" Text="Description" DataIndex="NameZiffAccount" Hidden="true" />
                                <ext:SummaryColumn ID="DisplayName" runat="server" Text="Account Code/Name" DataIndex="DisplayName" Flex="1" />
                                <ext:Column ID="Criteria" runat="server" DataIndex="ProjectionCriteria" Text="Projection Criteriam" Width="200">
                                    <Renderer Fn="criteriarenderer" />
                                    <Editor>
                                        <ext:ComboBox ID="ddlProjectionCriteria" runat="server" DisplayField="Name" ValueField="Code" Width="180" Enabled="false" QueryMode="Local">
                                            <Store>
                                                <ext:Store ID="storeProjectionCriteria" runat="server">
                                                    <Model>
                                                        <ext:Model ID="Model5" runat="server" IDProperty="Code">
                                                            <Fields>
                                                                <ext:ModelField Name="Name" />
                                                                <ext:ModelField Name="Code" />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                </ext:Store>
                                            </Store>
                                            <DirectEvents>
                                                <Select OnEvent="ddlProjectionCriteria_Select">
                                                    <%-- <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" /> --%>
                                                    <ExtraParams>
                                                        <ext:Parameter Name="CriteriaValue" Value="#{ddlProjectionCriteria}.getValue()" Mode="Raw" Encode="true" />
                                                    </ExtraParams>
                                                </Select>
                                            </DirectEvents>
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="NameTechnicalSize" runat="server" DataIndex="IdTechnicalDriverSize" Text="Size Drivers" Flex="1">
                                    <Renderer Fn="sizedriverrenderer" />
                                    <Editor>
                                        <ext:ComboBox ID="ddlSizeDriver" TypeAhead="true" TriggerAction="All" LazyMode="Instance" runat="server" DisplayField="Name" ValueField="ID" Width="180" QueryMode="Local">
                                            <Store>
                                                <ext:Store ID="storeSizeDriver" runat="server">
                                                    <Model>
                                                        <ext:Model ID="Model7" runat="server" IDProperty="ID">
                                                            <Fields>
                                                                <ext:ModelField Name="Name" />
                                                                <ext:ModelField Name="ID" />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                </ext:Store>
                                            </Store>
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="NameTechnicalPerformance" runat="server" DataIndex="IdTechnicalDriverPerformance" Text="Performance Drivers" Flex="1">
                                    <Renderer Fn="performancedriverrenderer" />
                                    <Editor>
                                        <ext:ComboBox ID="ddlPerformanceDriver" TriggerAction="All" LazyMode="Instance" runat="server" DisplayField="Name" ValueField="ID" Width="180" QueryMode="Local">
                                            <Store>
                                                <ext:Store ID="storePerformanceDriver" runat="server">
                                                    <Model>
                                                        <ext:Model ID="Model8" runat="server" IDProperty="ID">
                                                            <Fields>
                                                                <ext:ModelField Name="Name" />
                                                                <ext:ModelField Name="ID" />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                </ext:Store>
                                            </Store>
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="IdCycleValue" runat="server" DataIndex="CycleValue" Text="Cycle Term" Width="100">
                                    <%--<Renderer Handler="if (record.data['DisplayName'].substring(0, 5) != '    -') return ''; else return value;"></Renderer>--%>
                                    <Renderer Handler="if (record.data['NameProjectionCriteria'] != 'Cyclical') return '';  else return value;"></Renderer> 
                                    <Editor>
                                        <ext:NumberField ID="txtCycleValue" runat="server" AllowBlank="false" MinValue="2" MaxValue="10">
                                        </ext:NumberField>
                                    </Editor>
                                </ext:Column>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" mode="Single"/>
                        </SelectionModel>
                        <Plugins>
                            <ext:CellEditing ID="CellEditing1" runat="server">
                                <Listeners>
                                    <BeforeEdit Handler="return beforeCellEditHandler(e)"></BeforeEdit>
                                </Listeners>
                            </ext:CellEditing>
                        </Plugins>
                        <Features>
                            <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                                <Filters>
                                    <ext:StringFilter DataIndex="ZiffAccount" />
                                    <ext:StringFilter DataIndex="ProjectionCriteria" />
                                    <ext:StringFilter DataIndex="IdTechnicalDriverSize" />
                                    <ext:StringFilter DataIndex="IdTechnicalDriverPerformance" />
                                    <ext:NumericFilter DataIndex="IdTechnicalAssumption" />
                                </Filters>
                            </ext:GridFilters>
                        </Features>
                        <DirectEvents>
                            <BeforeEdit OnEvent="Technical_BeforeEdit">
                                <EventMask ShowMask="true" Target="This">
                                </EventMask>
                                <ExtraParams>
                                    <ext:Parameter Name="Criteria" Value="e.record.data.ProjectionCriteria" Mode="Raw">
                                    </ext:Parameter>
                                </ExtraParams>
                            </BeforeEdit>
                        </DirectEvents>
                   </ext:GridPanel>
                    <ext:Window ID="winImport" runat="server" Title="Import Technical Driver Data" Hidden="true" Icon="ArrowLeft" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="frmImportFile" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                                <Items>
                                    <ext:Panel ID="pnlImportFile1" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:FileUploadField ID="fileImport" FieldLabel="File" runat="server" Icon="Attach" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnImportFile" runat="server" Text="Import" Icon="TableRefresh" Disabled="true" FormBind="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnImportFile_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="rowsValues" Value="#{grdTechnicalAssumptions}.getRowsValues(false)" Mode="Raw" Encode="true" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnCancelImport" runat="server" Icon="Decline" Text="Close">
                                        <DirectEvents>
                                            <Click OnEvent="btnCancelImport_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                                <BottomBar>
                                    <ext:StatusBar ID="FormImportStatusBar" runat="server" />
                                </BottomBar>
                                <Listeners>
                                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                                            });
                                                                            #{btnImportFile}.setDisabled(!valid);" />
                                </Listeners>
                            </ext:FormPanel>
                        </Items>
                    </ext:Window>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="FormStatusBar" BusyText="I'm loading"  runat="server" />
                </BottomBar>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>

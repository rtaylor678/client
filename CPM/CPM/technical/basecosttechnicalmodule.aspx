﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="basecosttechnicalmodule.aspx.cs" Inherits="CPM._basecosttechnicalmodule" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var rendererPerformance = function (value) {
            if (value == -1) {
                return "";
            }
            else {
                return Ext.util.Format.number(value, '0,000');
            }
        }

        var rendererSize = function (value) {
            if (value == 0) {
                return "";
            } else {
                return Ext.util.Format.number(value, '0,000');
            }
        }

        Ext.num = function (v, defaultValue) {
            v = Number(Ext.isEmpty(v) || Ext.isArray(v) || typeof v == 'boolean' || (typeof v == 'string' && v.trim().length == 0) ? NaN : v);
            return isNaN(v) ? defaultValue : v;
        }

        Ext.util.Format.number = function (v, format) {
            if (!format) {
                return v;
            }
            v = Ext.num(v, NaN);
            if (isNaN(v)) {
                return '';
            }
            var comma = ',',
                dec = '.',
                i18n = false,
                neg = v < 0;

            v = Math.abs(v);
            if (format.substr(format.length - 2) == '/i') {
                format = format.substr(0, format.length - 2);
                i18n = true;
                comma = '.';
                dec = ',';
            }

            var hasComma = format.indexOf(comma) != -1,
                psplit = (i18n ? format.replace(/[^\d\,]/g, '') : format.replace(/[^\d\.]/g, '')).split(dec);

            if (1 < psplit.length) {
                v = v.toFixed(psplit[1].length);
            } else if (2 < psplit.length) {
                throw ('NumberFormatException: invalid format, formats should have no more than 1 period: ' + format);
            } else {
                v = v.toFixed(0);
            }

            var fnum = v.toString();

            psplit = fnum.split('.');

            if (hasComma) {
                var cnum = psplit[0],
                    parr = [],
                    j = cnum.length,
                    m = Math.floor(j / 3),
                    n = cnum.length % 3 || 3,
                    i;

                for (i = 0; i < j; i += n) {
                    if (i != 0) {
                        n = 3;
                    }

                    parr[parr.length] = cnum.substr(i, n);
                    m -= 1;
                }
                fnum = parr.join(comma);
                if (psplit[1]) {
                    fnum += dec + psplit[1];
                }
            } else {
                if (psplit[1]) {
                    fnum = psplit[0] + dec + psplit[1];
                }
            }

            return (neg ? '-' : '') + format.replace(/[\d,?\.?]+/, fnum);
        }
    </script>
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" 
                runat="server" 
                Title="Base Rates by Cost Category" 
                IconCls="icon-basecostrates_16" 
                AutoScroll="true" 
                Border="false"        
                Layout="BorderLayout" >
                <Items>
                    <%-- Reset and Export buttons --%>
                    <ext:Panel ID="pnlExportToolbar" runat="server" Layout="HBoxLayout" Border="false" Hidden="true" Region="North">
                        <TopBar>
                            <ext:Toolbar ID="tbrExport" Border="false" Height="30" runat="server">
                                <Items>
                                    <ext:Button ID="btnReset" runat="server" Text="Reset" Icon="Reload">
                                        <DirectEvents>
                                            <Click OnEvent="btnReset_Click">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarFill />
                                    <ext:Button ID="btnSaveExcel" runat="server" Text="Export Report Data To Excel" Icon="PageExcel">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveExcel_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                    </ext:Panel>
                    <%-- Field and Currency drop down lists --%>
                    <ext:Panel ID="pnlButtons01" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0" region="North">
                        <Items>
                            <ext:Label Style="margin: 10px 0 10px 10px" ID="lblFilterField" runat="server" Text="Field:"></ext:Label>
                            <ext:ComboBox Style="margin: 10px 0 10px 10px" MinChars="0" ID="ddlField" runat="server" Width="200" DisplayField="Name" ValueField="ID">
                                <ListConfig LoadingText="Searching..." MinWidth="300">
                                </ListConfig>
                                <Store>
                                    <ext:Store ID="storeField" runat="server" OnReadData="StoreField_ReadData">
                                        <Model>
                                            <ext:Model ID="Model6" runat="server" IDProperty="ID">
                                                <Fields>
                                                    <ext:ModelField Name="Name" />
                                                    <ext:ModelField Name="ID" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                                <Reader>
                                                    <ext:JsonReader />
                                                </Reader>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <%--<DirectEvents>
                                    <Select OnEvent="ddlField_Select" />
                                </DirectEvents>--%>
                            </ext:ComboBox>
                            <ext:Label Style="margin: 10px 0 10px 10px" ID="lblCurrency" runat="server" Text="Currency:"></ext:Label>
                            <ext:ComboBox Style="margin: 10px 0 10px 10px" ID="ddlCurrency" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" ValueField="IdCurrency" Width="200">
                                <ListConfig LoadingText="Searching..." MinWidth="250">
                                    <ItemTpl ID="ItemTpl4" runat="server">
                                        <Html>
                                            <div class="search-item">
							                    <h3>{Name}</h3>
							                    <span>Code: {Code}</span>
						                    </div>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Store>
                                    <ext:Store ID="storeBase" runat="server" OnReadData="StoreCurr_ReadData">
                                        <Model>
                                            <ext:Model ID="Model4" runat="server" IDProperty="IdCurrency">
                                                <Fields>
                                                    <ext:ModelField Name="IdCurrency" />
                                                    <ext:ModelField Name="Name" />
                                                    <ext:ModelField Name="Code" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                                <Reader>
                                                    <ext:JsonReader />
                                                </Reader>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                        </Items>
                    </ext:Panel>
                    <%-- Base Cost Reference and Scenario drop down lists --%>
                    <ext:Panel ID="pnlButtons02" runat="server" Layout="HBoxLayout" Border="true" Style="margin: 0 0 0 0" region="North">
                        <Items>
                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lblFilterBaseCost" runat="server" Text="Base Cost Reference:"></ext:Label>
                            <ext:ComboBox Style="margin: 10px 10px 10px 10px" ID="ddlFilterBaseCost" MinChars="0" runat="server" Width="200" DisplayField="Name" ValueField="Code">
                                <ListConfig LoadingText="Searching..." MinWidth="300">
                                </ListConfig>
                                <Store>
                                    <ext:Store ID="storeBaseCostCond" runat="server" OnReadData="StoreFilterBaseCost_ReadData">
                                        <Model>
                                            <ext:Model ID="Model5" runat="server" IDProperty="Code">
                                                <Fields>
                                                    <ext:ModelField Name="Name" />
                                                    <ext:ModelField Name="Code" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                                <Reader>
                                                    <ext:JsonReader />
                                                </Reader>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lblFilterStage" runat="server" Text="Scenario:"></ext:Label>
                            <ext:ComboBox Style="margin: 10px 10px 10px 10px" MinChars="0" ID="ddlFilterStage" runat="server" Width="200" DisplayField="Name" ValueField="ID">
                                <ListConfig LoadingText="Searching..." MinWidth="300">
                                </ListConfig>
                                <Store>
                                    <ext:Store ID="storeFilterStage" runat="server" OnReadData="StoreFilterStage_ReadData">
                                        <Model>
                                            <ext:Model ID="Model7" runat="server" IDProperty="ID">
                                                <Fields>
                                                    <ext:ModelField Name="Name" />
                                                    <ext:ModelField Name="ID" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                        <Proxy>
                                            <ext:PageProxy>
                                                <Reader>
                                                    <ext:JsonReader />
                                                </Reader>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:Store>
                                </Store>
                                <%--<DirectEvents>
                                    <Select OnEvent="ddlFilterStage_Select" />
                                </DirectEvents>--%>
                            </ext:ComboBox>
                        </Items>
                    </ext:Panel>
                    <%-- Run and Direct Export buttons --%>
                    <ext:Panel ID="pnlReportToolbar" runat="server" Layout="HBoxLayout" Border="false" Hidden="false" Region="North">
                        <TopBar>
                            <ext:Toolbar ID="tbrReport" Border="false" Height="30" runat="server">
                                <Items>
                                    <ext:Button ID="btnRun" runat="server" Text="Execute Base Rates by Drivers" Icon="PlayGreen">
                                        <DirectEvents>
                                            <Click Timeout="3600000" ShowWarningOnFailure="false"  OnEvent="btnRun_Click">
                                                <EventMask ShowMask="true">
                                                </EventMask>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarFill />
                                    <ext:Button ID="btnDirectExcel" runat="server" Text="Export Data To Excel" Icon="PageExcel">
                                        <DirectEvents>
                                            <Click Timeout="3600000" OnEvent="btnSaveExcelDirect_Click" IsUpload="true">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                    </ext:Panel>
                    <ext:GridPanel ID="grdBaseCostTechnicalModule" 
                        runat="server" 
                        AutoScroll="true" 
                        Border="false"
                        Layout="AutoLayout"
                        StripeRows="true"
                        EnableLocking="true"
                        Header="true" 
                        Region="Center">
                        <Store>
                            <ext:Store ID="storeBaseCostTechnicalModuleData" runat="server" GroupField="RootZiffAccount" RemoteGroup="true" RemoteSort="true">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="IdZiffAccount">
                                        <Fields>
                                            <ext:ModelField Name="IdZiffAccount" Type="Int" />
                                            <ext:ModelField Name="DisplayCode" Type="String"/>
                                            <ext:ModelField Name="ZiffAccount" Type="String"/>
                                            <ext:ModelField Name="RootZiffAccount" Type="String"/>
                                            <ext:ModelField Name="BaseCost" Type="Float" UseNull="true"/>
                                            <ext:ModelField Name="ProjectionCriteria" Type="int" UseNull="true"/>
                                            <ext:ModelField Name="NameProjectionCriteria" Type="String" />
                                            <ext:ModelField Name="SizeName" Type="String" />
                                            <ext:ModelField Name="SizeValue" Type="Float" UseNull="true" />
                                            <ext:ModelField Name="PerformanceName" Type="String" />
                                            <ext:ModelField Name="PerformanceValue" Type="Float" UseNull="true" />
                                            <ext:ModelField Name="CycleValue" Type="Int" UseNull="true" />
                                            <ext:ModelField Name="BaseCostRate" Type="Float" UseNull="true" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel ID="ColumnModel1" runat="server">
                            <Columns>
                                <ext:Column ID="colDisplayCode" runat="server" Text="Code" Width="175" Resizable="false" DataIndex="DisplayCode" Locked="true" />
                                <ext:Column ID="colZiffAccount" runat="server" Text="Name" Width="300" DataIndex="ZiffAccount"  Locked="true" />
                                <ext:NumberColumn ID="colBaseCost" runat="server" Format="0,000.00" DataIndex="BaseCost" Flex="1" Text="Base<br>Cost" Align="Right" Width="150" MaxWidth="200" />
                                <ext:Column ID="colProjectionCriteria" runat="server" Text="Projection<br>Criteria" DataIndex="NameProjectionCriteria" />
                                <ext:Column ID="colSize" runat="server" Text="Size">
                                    <Columns>
                                        <ext:Column ID="colSizeName" runat="server" Text="Driver" DataIndex="SizeName" Width="150"/>
                                        <ext:NumberColumn ID="colSizeValue" runat="server" Format="0,000.00" DataIndex="SizeValue" Text="Value" Flex="1" Align="Right" MaxWidth="400" Width="100">
                                            <Renderer Fn="rendererSize" />
                                        </ext:NumberColumn>
                                    </Columns>
                                </ext:Column>
                                <ext:Column ID="colPerformance" runat="server" Text="Performance">
                                    <Columns>
                                        <ext:Column ID="colPerformanceName" runat="server" Text="Driver" DataIndex="PerformanceName" />
                                        <ext:NumberColumn ID="colPerformanceValue" runat="server" Format="0,000.00" DataIndex="PerformanceValue" Text="Value" Flex="1" Align="Right" MaxWidth="200">
                                            <Renderer Fn="rendererPerformance" />
                                        </ext:NumberColumn>
                                    </Columns>
                                </ext:Column>
                                <ext:NumberColumn ID="colBaseCostRate" Format="0,000.00" runat="server" DataIndex="BaseCostRate" Text="Base<br>Cost<br>Rate" Flex="1" Align="Right" MaxWidth="200" />
                                <ext:NumberColumn ID="colCycleValue" Format="0" runat="server" DataIndex="CycleValue" Text="Cycle<br>Value" Flex="1" MaxWidth="200" />
                                <ext:Column ID="Filler" runat="server" Text=" " Flex="1" />
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" />
                        </SelectionModel>
                        <Features>
                            <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                                <Filters>
                                    <ext:StringFilter DataIndex="ZiffAccount" />
                                    <ext:StringFilter DataIndex="NameProjectionCriteria" />
                                    <ext:StringFilter DataIndex="SizeName" />
                                    <ext:StringFilter DataIndex="PerformanceName" />
                                </Filters>
                            </ext:GridFilters>
                            <ext:GroupingSummary ID="RootZiffAccount" runat="server" GroupHeaderTplString="{name}" HideGroupedHeader="true" EnableGroupingMenu="false" />
                        </Features>
                    </ext:GridPanel>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="FormStatusBar" runat="server" />
                </BottomBar>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="zifftechnicalanalysis.aspx.cs" Inherits="CPM._zifftechnicalanalysis" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" AutoScroll="true" runat="server" Title="Technical Files Repository" IconCls="icon-zifftechnicalanalysis_16" Border="false">
                <Items>
                    <ext:GridPanel ID="grdZiffTechnicalAnalysis" runat="server" AutoScroll="true" Border="false" Header="false">
                        <TopBar>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btnNew" Icon="Add" ToolTip="New Analysis" runat="server" Text="Add New File">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="storeZiffTechnicalAnalysis" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="IdZiffTechnicalAnalysis">
                                        <Fields>
                                            <ext:ModelField Name="IdZiffTechnicalAnalysis" Type="Int" />
                                            <ext:ModelField Name="IdModel" Type="Int" />
                                            <ext:ModelField Name="IdZiffAccount" Type="Int" />
                                            <ext:ModelField Name="AttachFile" Type="Object" />
                                            <ext:ModelField Name="FileName" Type="String" />
                                            <ext:ModelField Name="CodeZiffAccount" Type="String" />
                                            <ext:ModelField Name="NameZiffAccount" Type="String" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel ID="ColumnModel1" runat="server">
                            <Columns>
                                <ext:Column ID="cZiffTechnicalAnalysis" Hidden="true" runat="server" Text="ID" DataIndex="IdZiffTechnicalAnalysis"></ext:Column>
                                <ext:Column ID="cModel" Hidden="true" runat="server" Text="ID" DataIndex="IdModel"></ext:Column>
                                <ext:Column ID="FileName" runat="server" Text="Filename" DataIndex="FileName" Flex="1"></ext:Column>
                                <ext:Column ID="cZiffAccount" Hidden="true" runat="server" Text="Ziff Account" DataIndex="IdZiffAccount"></ext:Column>
                                <ext:Column ID="Column1" runat="server" Text="Ziff Account Code" DataIndex="CodeZiffAccount" Flex="1"></ext:Column>
                                <ext:Column ID="Column2" runat="server" Text="Ziff Account Name" DataIndex="NameZiffAccount" Flex="1"></ext:Column>
                                <ext:Column ID="cAttachFile" Hidden="true" runat="server" Text="Attach File" DataIndex="AttachFile"></ext:Column>
                                <ext:ImageCommandColumn Align="Center" Width="200" Text="Functions" ID="ImageCommandColumn2" runat="server">
                                    <Commands>
                                        <ext:ImageCommand CommandName="Edit" IconCls="icon-edit_16" Text="&nbsp;Edit" />
                                    </Commands>
                                    <Commands>
                                        <ext:ImageCommand CommandName="Delete" IconCls="icon-delete_16" Text="&nbsp;Delete" />
                                    </Commands>
                                    <Commands>
                                        <ext:ImageCommand CommandName="Download" Icon="DiskDownload" Text="&nbsp;Download" />
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="grdZiffTechnicalAnalysis_Command">
                                            <ExtraParams>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>
                                                <ext:Parameter Name="Id" Value="record.data.IdZiffTechnicalAnalysis" Mode="Raw"></ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:ImageCommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server">
                            </ext:RowSelectionModel>
                        </SelectionModel>
                        <Features>
                            <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                                <Filters>
                                    <ext:StringFilter DataIndex="CodeZiffAccount" />
                                    <ext:StringFilter DataIndex="NameZiffAccount" />
                                    <ext:StringFilter DataIndex="FileName" />
                                </Filters>
                            </ext:GridFilters>
                        </Features>
                    </ext:GridPanel>
                    <ext:Window ID="winZiffTechnicalAnalysisEdit" runat="server" Title="Ziff Technical Analysis" Hidden="true" IconCls="icon-zifftechnicalanalysis_16" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="pnlEditTechnicalDriver" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                                <Items>
                                    <ext:Panel ID="Panel2" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:ComboBox ID="ddlZiffAccount" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" MinChars="0" FieldLabel="Ziff Account" ValueField="IdZiffAccount" Cls="PopupFormField">
                                                <ListConfig LoadingText="Searching...">
                                                    <ItemTpl ID="ItemTpl1" runat="server">
                                                        <Html>
														    <div class="search-item">
														        <h3>{Name}</h3>
														        <span>Code: {Code}</span>
														    </div>
													    </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeZiffAccount" runat="server" OnReadData="StoreZiffAccount_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model3" runat="server" IDProperty="IdZiffAccount">
                                                                <Fields>
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="IdZiffAccount" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel1" runat="server" Border="false" Header="false" ColumnWidth="0.69" Layout="Form" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:TextField ID="txtFileName" runat="server" FieldLabel="New File:" ReadOnly="true" AnchorHorizontal="80%" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel3" runat="server" Border="false" Header="false" ColumnWidth="0.31" Layout="Form" Cls="PopupFormColumnPanel">
                                        <Items>
                                            <ext:FileUploadField ID="fileImport" runat="server" Icon="Attach" ButtonOnly="true" ButtonText="Attach New File" Dock="Right" Cls="PopupFormField">
                                                <DirectEvents>
                                                    <Change OnEvent="fileImport_Change" />
                                                </DirectEvents>
                                            </ext:FileUploadField>
                                        </Items>
                                    </ext:Panel>
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnSave" runat="server" Text="Save" Icon="DatabaseSave" Disabled="true" FormBind="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnSave_Click">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnCancel" runat="server" Icon="Decline" Text="Close">
                                        <DirectEvents>
                                            <Click OnEvent="btnCancel_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                                <BottomBar>
                                    <ext:StatusBar ID="StatusBar1" runat="server" />
                                </BottomBar>
                                <Listeners>
                                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                            });
                                                            #{btnSave}.setDisabled(!valid);" />
                                </Listeners>
                            </ext:FormPanel>
                        </Items>
                    </ext:Window>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="FormStatusBar" runat="server" />
                </BottomBar>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>
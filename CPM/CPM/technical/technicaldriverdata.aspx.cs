﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using System.Xml.Xsl;
using CD;
using DataClass;
using System.ComponentModel;
using System.Web.Script.Serialization;

namespace CPM
{
    public partial class _technicaldriverdata : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdTechnicalDriverData { get { return (Int32)Session["IdTechnicalDriverData"]; } set { Session["IdTechnicalDriverData"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"]; } set { Session["idLanguage"] = value; } }
        public DataTable datViewData { get; set; }
        private DataReportObject ExportTechnicalDriversData { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                IdTechnicalDriverData = 0;
                if (this.GetProject())
                {
                    this.lblFilterProject.Hide();
                    this.ddlFilterProject.Hide();
                }
                LoadLabel();
                
            }
        }
     
        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                String _Values = e.ExtraParams["rowsValues"];

                datViewData = new DataTable();
                datViewData.Columns.Add("IdTechnicalDriverData", typeof(int));
                datViewData.Columns.Add("IdTechnicalDriver", typeof(int));
                datViewData.Columns.Add("IdProject", typeof(int));
                datViewData.Columns.Add("IdField", typeof(int));
                datViewData.Columns.Add("Year", typeof(int));
                datViewData.Columns.Add("Normal", typeof(float));
                datViewData.Columns.Add("Optimistic", typeof(float));
                datViewData.Columns.Add("Pessimistic", typeof(float));

                List<Object> data = JSON.Deserialize<List<Object>>(_Values);

                foreach (Object value in data)
                {
                    Object _ObjectRow = new Object();
                    _ObjectRow = value.ToString();

                    JavaScriptSerializer ser = new JavaScriptSerializer();

                    Dictionary<string, object> _Row = ser.Deserialize<Dictionary<string, object>>(_ObjectRow.ToString());
                    DataRow _NewRow = datViewData.NewRow();

                    _NewRow["IdTechnicalDriverData"] = _Row["IdTechnicalDriverData"];
                    _NewRow["IdTechnicalDriver"] = _Row["IdTechnicalDriver"];
                    _NewRow["IdProject"] = _Row["IdProject"];
                    _NewRow["IdField"] = _Row["IdField"];
                    _NewRow["Year"] = _Row["Year"];
                    _NewRow["Normal"] = _Row["Normal"];
                    _NewRow["Optimistic"] = _Row["Optimistic"];
                    _NewRow["Pessimistic"] = _Row["Pessimistic"];

                    datViewData.Rows.Add(_NewRow);
                }

                this.Save();
                this.LoadTechnicalDriverData();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                //this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnDeleteAll_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete all records?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDeleteAllYES()",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            try
            {
                String strReturnMessage = null;
                if (this.fileImport.HasFile)
                {
                    DataFileReader objReader = null;
                    DataFileType objType = new DataFileType();
                    objType.CheckFileType(fileImport.PostedFile.FileName);

                    if (!objType.IsValidType)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }

                    if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                    {
                        objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.TechnicalDriverData);
                        objReader.GetDataTable(ref strReturnMessage);
                    }
                    else
                    {
                        strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                    }

                    if (String.IsNullOrEmpty(strReturnMessage))
                    {
                        intFailedImportRowCount = 0;
                        strImportErrorMessage = "";
                        intCurrentImportRow = 0;
                        foreach (DataRow row in objReader.ImportTable.Rows)
                        {
                            intCurrentImportRow++;
                            this.SaveImport(row["Driver Desc"].ToString(), row["Field Code"].ToString(), row["Project Code"].ToString(), Convert.ToInt32(row["Year"]), Convert.ToDouble(row["Normal"]), Convert.ToDouble(row["Pessimistic"]), Convert.ToDouble(row["Optimistic"]));
                        }
                        if (intFailedImportRowCount != 0)
                        {
                            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                        }
                        else
                        {
                            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                        }
                        this.winImport.Hide();
                        this.ModelUpdateTimestamp();
                        //Trim Error if too long
                        if (strImportErrorMessage.Length > 1024)
                        {
                            strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                        }
                        X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                    }

                    this.LoadTechnicalDriverData();
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
                }
            }
            catch
            {
            }
        }

        protected void StoreField_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsFields objFields = new DataClass.clsFields();
            if (ddlFilterTechnicalDriver.Value.ToString() == "")
            {
                this.storeField.DataSource = objFields.LoadListDrivers("IdModel = " + IdModel + " AND IdTechnicalDriver = 0 AND Name LIKE '%" + this.ddlField.Text + "%'", "ORDER BY Name");
            }
            else
            {
                this.storeField.DataSource = objFields.LoadListDrivers("IdModel = " + IdModel + " AND IdTechnicalDriver = " + ddlFilterTechnicalDriver.Value + " AND Name LIKE '%" + this.ddlField.Text + "%'", "ORDER BY Name");
            }
            this.storeField.DataBind();
        }

        protected void StoreFilterTechnicalDriver_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsTechnicalDrivers objTechnicalDrivers = new DataClass.clsTechnicalDrivers();
            this.storeFilterTechnicalDriver.DataSource = objTechnicalDrivers.LoadComboBox("IdModel = " + IdModel + " AND tblTechnicalDrivers.Name LIKE '%" + this.ddlFilterTechnicalDriver.Text + "%'", "ORDER BY Name");
            this.storeFilterTechnicalDriver.DataBind();
        }

        protected void StoreFilterProject_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsProjects objProjects = new DataClass.clsProjects();
            if (ddlField.SelectedItem.Value == null)
            {
                this.storeFilterProject.DataSource = objProjects.LoadList("IdModel = " + IdModel + " And IdField = 0 AND Name +' | '+ OperationName LIKE '%" + this.ddlFilterProject.Text + "%' AND IdLanguage = " + idLanguage, "ORDER BY Name");
            }
            else
            {
                this.storeFilterProject.DataSource = objProjects.LoadList("IdModel = " + IdModel + " And IdField = " + ddlField.SelectedItem.Value + " AND Name +' | '+ OperationName LIKE '%" + this.ddlFilterProject.Text + "%' AND IdLanguage = " + idLanguage, "ORDER BY Name");
            }
            this.storeFilterProject.DataBind();
        }

        protected void ddlFilterTechnicalDriver_Select(object sender, DirectEventArgs e)
        {
            storeTechnicalDriverData.RemoveAll();

            this.ddlField.Value = null;
            this.ddlFilterProject.Value = null;

            DataClass.clsTechnicalDrivers objTechnicalDrivers = new DataClass.clsTechnicalDrivers();
            DataClass.clsUnit objUnit = new DataClass.clsUnit();
            if (ddlFilterTechnicalDriver.Value != null)
            {
                int number;
                if (Int32.TryParse(ddlFilterTechnicalDriver.SelectedItem.Value, out number))
                {
                    objTechnicalDrivers.IdTechnicalDriver = Convert.ToInt32(ddlFilterTechnicalDriver.SelectedItem.Value);
                    this.storeField.Reload();
                }
                else
                {
                    objTechnicalDrivers.IdTechnicalDriver = 0;
                }
                objTechnicalDrivers.loadObject();
                objUnit.IdUnit = objTechnicalDrivers.IdUnit;
                objUnit.loadObject();
            }
            LoadTechnicalDriverData();
        }

        protected void ddlField_Select(object sender, DirectEventArgs e)
        {
            storeTechnicalDriverData.RemoveAll();
            this.ddlFilterProject.Value = null;
            int number;
            if (Int32.TryParse(ddlField.SelectedItem.Value, out number))
            {
                storeFilterProject.Reload();
            }
            LoadTechnicalDriverData();
        }

        protected void ddlFilterProject_Select(object sender, DirectEventArgs e)
        {
            LoadTechnicalDriverData();
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        protected void Save()
        {
            foreach (DataRow row in datViewData.Rows)
            {
                DataClass.clsTechnicalDriverData objTechnicalDriverData = new DataClass.clsTechnicalDriverData();
                objTechnicalDriverData.IdTechnicalDriverData = Convert.ToInt32(row["IdTechnicalDriverData"]);
                objTechnicalDriverData.IdTechnicalDriver = Convert.ToInt32(row["IdTechnicalDriver"]);
                objTechnicalDriverData.loadObject();
                objTechnicalDriverData.IdModel = IdModel;
                objTechnicalDriverData.IdField = Convert.ToInt32(row["IdField"]);
                objTechnicalDriverData.IdProject = Convert.ToInt32(row["IdProject"]);
                objTechnicalDriverData.Year = Convert.ToInt32(row["Year"]);
                objTechnicalDriverData.Normal = Convert.ToDouble(row["Normal"]);
                objTechnicalDriverData.Pessimistic = Convert.ToDouble(row["Pessimistic"]);
                objTechnicalDriverData.Optimistic = Convert.ToDouble(row["Optimistic"]);
                if (objTechnicalDriverData.Normal == 0 && objTechnicalDriverData.Optimistic == 0 && objTechnicalDriverData.Pessimistic == 0)
                {
                    if (objTechnicalDriverData.IdTechnicalDriverData != 0)
                    {
                        objTechnicalDriverData.Delete();
                    }
                }
                else
                {
                    if (objTechnicalDriverData.IdTechnicalDriverData == 0)
                    {
                        objTechnicalDriverData.DateCreation = DateTime.Now;
                        objTechnicalDriverData.UserCreation = (Int32)IdUserCreate;
                        IdTechnicalDriverData = objTechnicalDriverData.Insert();
                    }
                    else
                    {
                        objTechnicalDriverData.DateModification = DateTime.Now;
                        objTechnicalDriverData.UserModification = (Int32)IdUserCreate;
                        objTechnicalDriverData.Update();
                    }
                }

            }
            //this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Record saved successfully!"), IconCls = "icon-accept", Clear2 = false });
            this.ModelUpdateTimestamp();
        }

        public Boolean GetProject()
        {
            Boolean Value = false;
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("Level IN (1,3) AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = true;
            }

            return Value;
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
            this.fileImport.Reset();
        }

        protected void SaveImport(String _Driver, String _Field, String _Project, Int32 _Year, Double _Normal, Double _Pessimistic, Double _Optimistic)
        {
            Int32 _IdDriver = GetIdDriver(_Driver);
            Int32 _IdField = GetIdField(_Field);
            Int32 _IdProject = GetIdProject(_Project);

            if (_IdDriver != 0 && _IdField != 0 && _IdProject != 0 && _Year != 0)
            {
                DataClass.clsTechnicalDriverData objTechnicalDriverData = new DataClass.clsTechnicalDriverData();
                DataTable dtAux = objTechnicalDriverData.LoadList("IdModel = " + IdModel + " And IdField = " + _IdField + " And IdProject=" + _IdProject + " And IdTechnicalDriver = " + _IdDriver + " And Year = " + _Year, "");
                if (dtAux.Rows.Count > 0)
                {
                    IdTechnicalDriverData = Convert.ToInt32(dtAux.Rows[0]["IdTechnicalDriverData"]);
                }
                else
                {
                    IdTechnicalDriverData = 0;
                }
                objTechnicalDriverData.IdTechnicalDriverData = (Int32)IdTechnicalDriverData;
                objTechnicalDriverData.loadObject();
                objTechnicalDriverData.IdField = _IdField;
                objTechnicalDriverData.IdProject = _IdProject;
                objTechnicalDriverData.IdTechnicalDriver = _IdDriver;
                objTechnicalDriverData.Year = _Year;
                objTechnicalDriverData.IdModel = IdModel;
                objTechnicalDriverData.Normal = _Normal;
                objTechnicalDriverData.Pessimistic = _Pessimistic;
                objTechnicalDriverData.Optimistic = _Optimistic;
                if (objTechnicalDriverData.IdTechnicalDriverData == 0)
                {
                    objTechnicalDriverData.DateCreation = DateTime.Now;
                    objTechnicalDriverData.UserCreation = (Int32)IdUserCreate;
                    IdTechnicalDriverData = objTechnicalDriverData.Insert();
                }
                else
                {
                    objTechnicalDriverData.DateModification = DateTime.Now;
                    objTechnicalDriverData.UserModification = (Int32)IdUserCreate;
                    objTechnicalDriverData.Update();
                }
                dtAux.Dispose();
                dtAux = null;
                objTechnicalDriverData = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected Int32 GetIdDriver(String _Driver)
        {
            Int32 Value = 0;
            DataClass.clsTechnicalDrivers objTechnicalDrivers = new DataClass.clsTechnicalDrivers();
            DataTable dt = objTechnicalDrivers.LoadList("Name = '" + _Driver + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdTechnicalDriver"];
            }
            return Value;
        }

        protected Int32 GetIdProject(String _Project)
        {
            Int32 Value = 0;
            DataClass.clsProjects objProjects = new DataClass.clsProjects();
            DataTable dt = objProjects.LoadList("Code = '" + _Project + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdProject"];
            }
            return Value;
        }

        protected Int32 GetIdField(String _Field)
        {
            Int32 Value = 0;
            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dt = objFields.LoadList("Code = '" + _Field + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdField"];
            }
            return Value;
        }

        [DirectMethod]
        public void ClickedDeleteAllYES()
        {
            this.DeleteAll();
            this.LoadTechnicalDriverData();
        }

        public void DeleteAll()
        {
            DataClass.clsTechnicalDriverData objTechnicalDriverData = new DataClass.clsTechnicalDriverData();
            objTechnicalDriverData.IdModel = IdModel;
            objTechnicalDriverData.DeleteAll();
            this.ModelUpdateTimestamp();
        }

        protected void LoadTechnicalDriverData()
        {
            //this.FormStatusBar.ClearStatus();
            storeTechnicalDriverData.RemoveAll();

            DataClass.clsTechnicalDriverData objTechnicalDriverData = new DataClass.clsTechnicalDriverData();

            if (ddlField.SelectedItem.Value != null && ddlField.SelectedItem.Text != null)
            {
                if (ddlFilterTechnicalDriver.SelectedItem.Value != null && ddlFilterTechnicalDriver.SelectedItem.Text != null)
                {
                    if (this.GetProject() == false)
                    {
                        if (ddlFilterProject.SelectedItem.Value == null && ddlFilterProject.SelectedItem.Text == null)
                        {
                            //this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select project please"), IconCls = "icon-exclamation", Clear2 = false });
                            return;
                        }
                    }                    
                    objTechnicalDriverData.IdTechnicalDriver = Convert.ToInt32(ddlFilterTechnicalDriver.SelectedItem.Value);
                    objTechnicalDriverData.IdModel = Convert.ToInt32(IdModel);
                    objTechnicalDriverData.IdField = Convert.ToInt32(ddlField.SelectedItem.Value);
                    objTechnicalDriverData.IdProject= Convert.ToInt32(ddlFilterProject.SelectedItem.Value);
                    DataTable dt = objTechnicalDriverData.LoadList();
                    storeTechnicalDriverData.DataSource = dt;
                    storeTechnicalDriverData.DataBind();
                }
                else
                {
                    //this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select Technical Driver please"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                //this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select Field please"), IconCls = "icon-exclamation", Clear2 = false });
                return;
            }
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataClass.clsTechnicalDriverData objTechnicalDriverData = new DataClass.clsTechnicalDriverData();
            DataTable dt = objTechnicalDriverData.LoadListExport("IdModel = " + IdModel, " ORDER BY FieldCode, ProjectCode, DriverName, Year");

            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("DriverName", "Driver Desc"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("FieldCode", "Field Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("ProjectCode", "Project Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Year", "Year"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Normal", "Normal"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Optimistic", "Optimistic"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Pessimistic", "Pessimistic"));
            String strSelectedParameters = "";

            ExportTechnicalDriversData = new DataReportObject(dt, "DATASHEET", null, strSelectedParameters, null, "FieldCode, ProjectCode, DriverName, Year", datColumnTD, null);

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportTechnicalDriversData;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=3.2 ExportTechnicalDriversData.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportTechnicalDriversData.xls", "template");
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = modMain.strMasterTechnicalDriversData;
            this.lblFilterTechnicalDriver.Text = modMain.strTechnicalModuleTechnicalDriver;
            this.lblFilterField.Text = modMain.strTechnicalModuleField;
            this.lblFilterProject.Text = modMain.strTechnicalModuleProject;
            this.btnSave.Text = modMain.strCommonSaveChanges;
            this.btnSave.ToolTip = modMain.strCommonSaveChanges;
            this.btnDelete.Text = modMain.strCommonDeleteAll;
            this.btnDelete.ToolTip = modMain.strCommonDeleteAll;
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text=(String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.Year.Text = modMain.strTechnicalModuleYear;
            this.Column1.Text = (String)GetGlobalResourceObject("CPM_Resources", "Scenarios");
            this.Columns1.Text = modMain.strTechnicalModuleBase;
            this.Columns2.Text = modMain.strTechnicalModuleOptimistic;
            this.Columns3.Text = modMain.strTechnicalModulePessimistic;
            this.winImport.Title = modMain.strTechnicalModuleImportTechnicalDriverData;
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnImportFile.ToolTip = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.btnCancelImport.ToolTip = modMain.strCommonClose;
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }
        
        #endregion

    }
}
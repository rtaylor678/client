﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using System.Xml.Xsl;
using CD;
using DataClass;
using System.ComponentModel;
using System.Web.Script.Serialization;

namespace CPM
{
    public partial class _basecosttechnicalmodule : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        public DataTable datViewData { get; set; }
        private DataReportObject ExportBaseCostTechnical { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                clsModels objModels = new clsModels();
                this.colBaseCost.Text = "Base Cost<br>" + objModels.GetDefaultCurrencySymbol(IdModel);
                this.colBaseCostRate.Text = "Base Cost Rate<br>" + objModels.GetDefaultCurrencySymbol(IdModel);
                this.LoadLabel();
            }
        }

        protected void btnReset_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/technical/basecosttechnicalmodule.aspx");
        }

        protected void btnSaveExcelDirect_Click(object sender, DirectEventArgs e)
        {
            try
            {
                
                this.ModelCheckRecalcModuleTechnical();
                DisableParameterSelection();

                DataClass.clsBaseCostTechnicalModule objBaseCostTechnicalModule = new DataClass.clsBaseCostTechnicalModule();
                this.FormStatusBar.ClearStatus();
                if (!String.IsNullOrEmpty(ddlField.Value.ToString()) && !String.IsNullOrEmpty(ddlField.Text))
                {
                    if (!String.IsNullOrEmpty(ddlFilterBaseCost.Value.ToString()) && !String.IsNullOrEmpty(ddlFilterBaseCost.Text))
                    {
                        if (!String.IsNullOrEmpty(ddlFilterStage.Value.ToString()) && !String.IsNullOrEmpty(ddlFilterStage.Text))
                        {
                            DataTable dt = objBaseCostTechnicalModule.LoadBaseCostTechnicalModule(IdModel, Convert.ToInt32(ddlField.Value), Convert.ToInt32(ddlFilterStage.Value), Convert.ToInt32(ddlFilterBaseCost.Value), Convert.ToInt32(ddlCurrency.Value), idLanguage);
                            storeBaseCostTechnicalModuleData.DataSource = dt;
                            storeBaseCostTechnicalModuleData.DataBind();
                            Int32 intCurrency = Convert.ToInt32(ddlCurrency.Value);

                            clsCurrencies objCurrencies = new clsCurrencies();
                            objCurrencies.IdCurrency = intCurrency;
                            objCurrencies.loadObject();
                            String strCurrencyCode = "(" + objCurrencies.Code + ")";
                            colBaseCost.Text = (String)GetGlobalResourceObject("CPM_Resources", "Base Cost") + "<br>" + strCurrencyCode;
                            colBaseCostRate.Text = this.colBaseCostRate.Text = (String)GetGlobalResourceObject("CPM_Resources", "BaseCostRate") + "<br>" + strCurrencyCode;

                            //Generate Export Details

                            ArrayList datColumnDisplay = new ArrayList();
                            datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("RootZiffAccount", "Ziff Account"));
                            datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("CodeZiff", "Code"));
                            datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("ZiffAccount", "Name"));
                            datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("BaseCost", "Base Cost"));
                            datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("NameProjectionCriteria", "Projection Criteria"));
                            datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("SizeName", "Size Driver"));
                            datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("SizeValue", "Size Value"));
                            datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("PerformanceName", "Performance Driver"));
                            datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("PerformanceValue", "PerformanceValue"));
                            datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("BaseCostRate", "Base Cost Rate"));
                            datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("CycleValue", "Cycle Value"));

                            ArrayList datColumnExclude = new ArrayList();
                            datColumnExclude.Add("SortOrder");

                            String[] columnsToCopy = { "RootZiffAccount", "CodeZiff", "ZiffAccount", "BaseCost", "NameProjectionCriteria", "SizeName", "SizeValue", "PerformanceName", "PerformanceValue", "BaseCostRate", "CycleValue", "SortOrder" };
                            System.Data.DataView view = new System.Data.DataView(dt);
                            DataTable dtBaseCostTechnical = view.ToTable(true, columnsToCopy);

                            String strSelectedParameters = lblFilterField.Text + " '" + ddlField.SelectedItem.Text + "', " +
                                                           lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "', " +
                                                           lblFilterBaseCost.Text + " '" + ddlFilterBaseCost.SelectedItem.Text + "', " +
                                                           lblFilterStage.Text + " '" + ddlFilterStage.SelectedItem.Text + "'";

                            ExportBaseCostTechnical = new DataReportObject(dtBaseCostTechnical, "Results", "Base Rates By Cost Category", strSelectedParameters, datColumnExclude, "SortOrder", datColumnDisplay, null);

                            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Generated Successfully!"), IconCls = "icon-accept", Clear2 = false });
                            X.Mask.Hide();

                            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
                            {
                                Yes = new MessageBoxButtonConfig
                                {
                                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                                    Text = modMain.strCommonYes
                                },
                                No = new MessageBoxButtonConfig
                                {
                                    Text = modMain.strCommonNo
                                }
                            }).Show();
                        }
                        else
                        {
                            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select a Scenario"), IconCls = "icon-exclamation", Clear2 = false });
                            return;
                        }
                    }
                    else
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select a Base Cost Reference"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select a Field"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                if (strMessage.Contains("Timeout"))
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = strMessage.ToString(), IconCls = "icon-exclamation", Clear2 = false });
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select all filters please"), IconCls = "icon-exclamation", Clear2 = false });
                }
            }
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void StoreField_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsFields objFields = new DataClass.clsFields();
            this.storeField.DataSource = objFields.LoadComboBox("IdModel = " + IdModel + " AND Name LIKE '%" + this.ddlField.Text + "%'", "ORDER BY Name");
            this.storeField.DataBind();
        }

        protected void StoreFilterBaseCost_ReadData(object sender, StoreReadDataEventArgs e)
        {
            //DataClass.clsParameters objParameters = new DataClass.clsParameters();
            //this.storeBaseCostCond.DataSource = objParameters.LoadList("Type = 'BaseCost' AND Name LIKE '%" + this.ddlFilterBaseCost.Text + "%'" + " AND IdLanguage = " + idLanguage, "ORDER BY Name");
            if (!String.IsNullOrEmpty(ddlField.Value.ToString()) && !String.IsNullOrEmpty(ddlField.Text))
            {
                DataClass.clsBaseCostTechnicalModule objBaseCostTechnicalModule = new DataClass.clsBaseCostTechnicalModule();
                this.storeBaseCostCond.DataSource = objBaseCostTechnicalModule.LoadList(IdModel, Convert.ToInt32(ddlField.Value));
                this.storeBaseCostCond.DataBind();
            }
        }

        protected void StoreFilterStage_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsStage objStage = new DataClass.clsStage();
            DataTable dt = objStage.LoadComboBox("Name LIKE '%" + this.ddlFilterStage.Text + "%'", "ORDER BY Name");
            foreach (DataRow row in dt.Rows)
            {
                if (idLanguage == 2)
                {
                    row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
                }
            }
            this.storeFilterStage.DataSource = dt;
            this.storeFilterStage.DataBind();
        }

        //protected void ddlField_Select(object sender, DirectEventArgs e)
        //{
        //    LoadBaseCostTechnicalModuleData();
        //}

        //protected void ddlFilterBaseCost_Select(object sender, DirectEventArgs e)
        //{
        //    LoadBaseCostTechnicalModuleData();
        //}

        //protected void ddlFilterStage_Select(object sender, DirectEventArgs e)
        //{
        //    DisableParameterSelection();

        //    LoadBaseCostTechnicalModuleData();
        //}

        protected void btnRun_Click(object sender, DirectEventArgs e)
        {
            this.ModelCheckRecalcModuleTechnical();
            DisableParameterSelection();

            LoadBaseCostTechnicalModuleData();
        }

        protected void StoreCurr_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsCurrencies objCurrencies = new DataClass.clsCurrencies();
            this.storeBase.DataSource = objCurrencies.LoadList("IdModel IN (" + IdModel + ") AND Output=1", "ORDER BY BaseCurrency DESC,Name");
            this.storeBase.DataBind();
            DataTable dt = objCurrencies.LoadList("BaseCurrency = 1 AND IdModel = " + IdModel, "");
            if (dt.Rows.Count > 0)
            {
                ddlCurrency.Value = dt.Rows[0]["IdCurrency"];
            }
            else
            {
                ddlCurrency.SelectedItem.Index = 0;
            }
        }

        #endregion

        #region Methods

        protected void LoadBaseCostTechnicalModuleData()
        {
            DataClass.clsBaseCostTechnicalModule objBaseCostTechnicalModule = new DataClass.clsBaseCostTechnicalModule();
            this.FormStatusBar.ClearStatus();
            if (!String.IsNullOrEmpty(ddlField.Value.ToString()) && !String.IsNullOrEmpty(ddlField.Text))
            {
                if (!String.IsNullOrEmpty(ddlFilterBaseCost.Value.ToString()) && !String.IsNullOrEmpty(ddlFilterBaseCost.Text))
                {
                    if (!String.IsNullOrEmpty(ddlFilterStage.Value.ToString()) && !String.IsNullOrEmpty(ddlFilterStage.Text))
                    {
                        DataTable dt = objBaseCostTechnicalModule.LoadBaseCostTechnicalModule(IdModel, Convert.ToInt32(ddlField.Value), Convert.ToInt32(ddlFilterStage.Value), Convert.ToInt32(ddlFilterBaseCost.Value), Convert.ToInt32(ddlCurrency.Value), idLanguage);
                        storeBaseCostTechnicalModuleData.DataSource = dt;
                        storeBaseCostTechnicalModuleData.DataBind();
                        Int32 intCurrency = Convert.ToInt32(ddlCurrency.Value);

                        clsCurrencies objCurrencies = new clsCurrencies();
                        objCurrencies.IdCurrency = intCurrency;
                        objCurrencies.loadObject();
                        String strCurrencyCode = "(" + objCurrencies.Code + ")";
                        colBaseCost.Text = (String)GetGlobalResourceObject("CPM_Resources", "Base Cost") + "<br>" + strCurrencyCode;
                        colBaseCostRate.Text = this.colBaseCostRate.Text = (String)GetGlobalResourceObject("CPM_Resources", "BaseCostRate") + "<br>" + strCurrencyCode;

                        //Generate Export Details
                        ArrayList datColumnDisplay = new ArrayList();
                        datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("RootZiffAccount", "Parent Cost Category"));
                        datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("CodeZiff", "Code"));
                        datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("ZiffAccount", "Name"));
                        datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("BaseCost", "Base Cost"));
                        datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("NameProjectionCriteria", "Projection Criteria"));
                        datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("SizeName", "Size Driver"));
                        datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("SizeValue", "Size Value"));
                        datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("PerformanceName", "Performance Driver"));
                        datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("PerformanceValue", "PerformanceValue"));
                        datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("BaseCostRate", "Base Cost Rate"));
                        datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("CycleValue", "Cycle Value"));

                        ArrayList datColumnExclude = new ArrayList();
                        datColumnExclude.Add("SortOrder");

                        String[] columnsToCopy = { "RootZiffAccount", "CodeZiff", "ZiffAccount", "BaseCost", "NameProjectionCriteria", "SizeName", "SizeValue", "PerformanceName", "PerformanceValue", "BaseCostRate", "CycleValue", "SortOrder" };
                        System.Data.DataView view = new System.Data.DataView(dt);
                        DataTable dtBaseCostTechnical = view.ToTable(true, columnsToCopy);

                        String strSelectedParameters = lblFilterField.Text + " '" + ddlField.SelectedItem.Text + "', " +
                                                       lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "', " +
                                                       lblFilterBaseCost.Text + " '" + ddlFilterBaseCost.SelectedItem.Text + "', " +
                                                       lblFilterStage.Text + " '" + ddlFilterStage.SelectedItem.Text + "'";

                        ExportBaseCostTechnical = new DataReportObject(dtBaseCostTechnical, "Results", "Base Rates By Cost Category", strSelectedParameters, datColumnExclude, "SortOrder", datColumnDisplay, null);
                    }
                    else
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select a Scenario"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select a Base Cost Reference"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select a Field"), IconCls = "icon-exclamation", Clear2 = false });
                return;
            }
        }

        public void ModelCheckRecalcModuleTechnical()
        {
            clsModels objModelRecalc = new clsModels();
            objModelRecalc.IdModel = IdModel;
            objModelRecalc.CheckRecalcModuleParameter(objApplication.MaxRelationLevels);
            objModelRecalc.CheckRecalcModuleAllocation();
            objModelRecalc.CheckRecalcModuleTechnical();
            objModelRecalc = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = modMain.strMasterBaseCostRates;
            this.lblFilterField.Text = modMain.strTechnicalModuleField;
            this.lblFilterBaseCost.Text = (String)GetGlobalResourceObject("CPM_Resources", "Base Cost Reference:");
            this.lblFilterStage.Text = (String)GetGlobalResourceObject("CPM_Resources", "Scenario");
            this.colDisplayCode.Text = modMain.strCommonCode;
            this.colZiffAccount.Text = modMain.strCommonName;
            this.colBaseCost.Text = (String)GetGlobalResourceObject("CPM_Resources", "Base Cost");
            this.colProjectionCriteria.Text = (String)GetGlobalResourceObject("CPM_Resources", "NameProjectionCriteria");
            this.colSize.Text = (String)GetGlobalResourceObject("CPM_Resources", "Size");
            this.colSizeName.Text = (String)GetGlobalResourceObject("CPM_Resources", "Driver");
            this.colSizeValue.Text = (String)GetGlobalResourceObject("CPM_Resources", "Value");
            this.colPerformance.Text = (String)GetGlobalResourceObject("CPM_Resources", "Performance");
            this.colPerformanceName.Text = (String)GetGlobalResourceObject("CPM_Resources", "Driver");
            this.colPerformanceValue.Text = (String)GetGlobalResourceObject("CPM_Resources", "Value");
            this.colBaseCostRate.Text = (String)GetGlobalResourceObject("CPM_Resources", "BaseCostRate");
            this.colCycleValue.Text = (String)GetGlobalResourceObject("CPM_Resources", "Cycle Value");
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnReset.Text = (String)GetGlobalResourceObject("CPM_Resources", "Reset");
            this.btnReset.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Reset");
            this.lblCurrency.Text = (String)GetGlobalResourceObject("CPM_Resources", "Currency") + ":";
            this.btnRun.Text = (String)GetGlobalResourceObject("CPM_Resources", "Run Report");
            this.btnDirectExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        private void DisableParameterSelection()
        {
            this.pnlExportToolbar.Hidden = false;
            this.pnlReportToolbar.Hidden = true;
            this.ddlField.ReadOnly = true;
            this.ddlCurrency.ReadOnly = true;
            this.ddlFilterBaseCost.ReadOnly = true;
            this.ddlFilterStage.ReadOnly = true;
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportBaseCostTechnical;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportFile(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2007);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=BaseRatesByCostCategory.xlsx");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        //[DirectMethod]
        //public void ClickedSaveExcelDirectYES()
        //{
        //    this.ModelCheckRecalcModuleTechnical();

        //    DataClass.clsBaseCostTechnicalModule objBaseCostTechnicalModule = new DataClass.clsBaseCostTechnicalModule();
        //    this.FormStatusBar.ClearStatus();
        //    if (!String.IsNullOrEmpty(ddlField.Value.ToString()) && !String.IsNullOrEmpty(ddlField.Text))
        //    {
        //        if (!String.IsNullOrEmpty(ddlFilterBaseCost.Value.ToString()) && !String.IsNullOrEmpty(ddlFilterBaseCost.Text))
        //        {
        //            if (!String.IsNullOrEmpty(ddlFilterStage.Value.ToString()) && !String.IsNullOrEmpty(ddlFilterStage.Text))
        //            {
        //                DataTable dt = objBaseCostTechnicalModule.LoadBaseCostTechnicalModule(IdModel, Convert.ToInt32(ddlField.Value), Convert.ToInt32(ddlFilterStage.Value), Convert.ToInt32(ddlFilterBaseCost.Value), Convert.ToInt32(ddlCurrency.Value), idLanguage);
        //                storeBaseCostTechnicalModuleData.DataSource = dt;
        //                storeBaseCostTechnicalModuleData.DataBind();
        //                Int32 intCurrency = Convert.ToInt32(ddlCurrency.Value);

        //                clsCurrencies objCurrencies = new clsCurrencies();
        //                objCurrencies.IdCurrency = intCurrency;
        //                objCurrencies.loadObject();
        //                String strCurrencyCode = "(" + objCurrencies.Code + ")";
        //                colBaseCost.Text = (String)GetGlobalResourceObject("CPM_Resources", "Base Cost") + "<br>" + strCurrencyCode;
        //                colBaseCostRate.Text = this.colBaseCostRate.Text = (String)GetGlobalResourceObject("CPM_Resources", "BaseCostRate") + "<br>" + strCurrencyCode;

        //                //Generate Export Details
        //                ArrayList datColumnDisplay = new ArrayList();
        //                datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("RootZiffAccount", "Ziff Account"));
        //                datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("CodeZiff", "Code"));
        //                datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("ZiffAccount", "Name"));
        //                datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("BaseCost", "Base Cost"));
        //                datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("NameProjectionCriteria", "Projection Criteria"));
        //                datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("SizeName", "Size Driver"));
        //                datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("SizeValue", "Size Value"));
        //                datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("PerformanceName", "Performance Driver"));
        //                datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("PerformanceValue", "PerformanceValue"));
        //                datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("BaseCostRate", "Base Cost Rate"));
        //                datColumnDisplay.Add(new DataClass.DataTableColumnDisplay("CycleValue", "Cycle Value"));

        //                ArrayList datColumnExclude = new ArrayList();
        //                datColumnExclude.Add("SortOrder");

        //                String[] columnsToCopy = { "RootZiffAccount", "CodeZiff", "ZiffAccount", "BaseCost", "NameProjectionCriteria", "SizeName", "SizeValue", "PerformanceName", "PerformanceValue", "BaseCostRate", "CycleValue", "SortOrder" };
        //                System.Data.DataView view = new System.Data.DataView(dt);
        //                DataTable dtBaseCostTechnical = view.ToTable(true, columnsToCopy);

        //                String strSelectedParameters = lblFilterField.Text + " '" + ddlField.SelectedItem.Text + "', " +
        //                                               lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "', " +
        //                                               lblFilterBaseCost.Text + " '" + ddlFilterBaseCost.SelectedItem.Text + "', " +
        //                                               lblFilterStage.Text + " '" + ddlFilterStage.SelectedItem.Text + "'";

        //                ExportBaseCostTechnical = new DataReportObject(dtBaseCostTechnical, "Results", "Base Rates By Cost Category", strSelectedParameters, datColumnExclude, "SortOrder", datColumnDisplay, null);

        //                String strReturnMessage = null;
        //                DataFileWriter objWriter = null;

        //                clsUsers objUser = new clsUsers();
        //                objUser.IdUser = IdUserCreate;
        //                objUser.loadObject();

        //                DataReportObject StoredExport = new DataReportObject();
        //                StoredExport = ExportBaseCostTechnical;

        //                objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
        //                objWriter.ExportReportCollection = new List<DataReportObject>();
        //                objWriter.ExportReportCollection.Add(StoredExport);
        //                objWriter.BuildExportFile(ref strReturnMessage, objUser.Name,DataFileWriter.ExcelType.Excel2007);
        //                if (String.IsNullOrEmpty(strReturnMessage))
        //                {
        //                    HttpContext context = HttpContext.Current;
        //                    context.Response.Clear();
        //                    context.Response.AddHeader("content-disposition", "attachment; filename=BaseRatesByCostCategory.xls");
        //                    context.Response.ContentType = "application/octet-stream";
        //                    objWriter.objExcel.Save(context.Response.OutputStream);
        //                    context.Response.End();
        //                }
        //            }
        //            else
        //            {
        //                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select a Scenario"), IconCls = "icon-exclamation", Clear2 = false });
        //                return;
        //            }
        //        }
        //        else
        //        {
        //            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select a Base Cost Reference"), IconCls = "icon-exclamation", Clear2 = false });
        //            return;
        //        }
        //    }
        //    else
        //    {
        //        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select a Field"), IconCls = "icon-exclamation", Clear2 = false });
        //        return;
        //    }
        //}

        #endregion

    }
}
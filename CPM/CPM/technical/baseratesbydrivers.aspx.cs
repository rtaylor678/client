﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _baseratesbydrivers : System.Web.UI.Page
    {
        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }
        private Boolean PageIsLoaded { get; set; }
        private DataReportObject ExportProjectionCosts { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }
        private DataReportObject ExportDriversList { get { return (DataReportObject)Session["Export02"]; } set { Session["Export02"] = value; } }
        private DataReportObject ExportCostPerDriver { get { return (DataReportObject)Session["Export03"]; } set { Session["Export03"] = value; } }
        private Int32 intBaseYear;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                this.LoadLabel();
                Session["Export01"] = null;
                Session["Export02"] = null;
                Session["Export03"] = null;
                PageIsLoaded = false;
                DataTable dtPC = new DataTable();
                DataTable dtSDL = new DataTable();
                DataTable dtBCR = new DataTable();
                this.FillGrid(this.storeProjectionCosts, this.grdProjectionCosts, dtPC, null);
                this.FillGrid(this.storeSizeDriverList, this.grdSizeDriverList, dtSDL, null);
                this.FillGrid(this.storeCostPerDrivers, this.grdCostPerDrivers, dtBCR, null);
                PageIsLoaded = true;
            }
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnSaveExcelDirect_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.ModelCheckRecalcModuleTechnical();
                DisableParameterSelection();

                DataClass.clsModels objModels = new DataClass.clsModels();
                objModels.IdModel = this.IdModel;
                objModels.loadObject();
                intBaseYear = objModels.BaseYear;

                Int32 intAggregationLevel = Convert.ToInt32(ddlLevels.Value);
                Int32 intField = Convert.ToInt32(ddlField.Value);
                Int32 intBaseCostType = Convert.ToInt32(ddlFilterBaseCost.Value);
                Int32 intStage = Convert.ToInt32(ddlFilterStage.Value);
                Int32 intCurrency = Convert.ToInt32(ddlCurrency.Value);

                clsCurrencies objCurrencies = new clsCurrencies();
                objCurrencies.IdCurrency = intCurrency;
                objCurrencies.loadObject();
                String strCurrencyCode = " (" + objCurrencies.Code + ")";

                DataSet ds = objModels.LoadBaseRatesByDrivers(IdModel, intAggregationLevel, intField, intCurrency, intStage, intBaseCostType, idLanguage);

                DataTable dtPC = new DataTable();
                dtPC = ds.Tables[0];
                DataTable dtSDL = new DataTable();
                dtSDL = ds.Tables[1];
                DataTable dtBCR = new DataTable();
                dtBCR = ds.Tables[2];

                ArrayList datColumnPC = new ArrayList();
                ArrayList datColumnSDL = new ArrayList();
                ArrayList datColumnBCR = new ArrayList();

                String strSelectedParameters = lblLevels.Text + " '" + ddlLevels.SelectedItem.Text + "', " +
                                               lblField.Text + " '" + ddlField.SelectedItem.Text + "', " +
                                               lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "', " +
                                               lblOperationType.Text + " '" + ddlOperationType.SelectedItem.Text + "', " +
                                               lblFilterStage.Text + " '" + ddlFilterStage.SelectedItem.Text + "'";
                ExportProjectionCosts = new DataReportObject(dtPC, "Base Cost By Type", "Base Rates By Drivers", strSelectedParameters, null, "Name", datColumnPC, null);
                ExportDriversList = new DataReportObject(dtSDL, "Technical Drivers", "Base Rates By Drivers", strSelectedParameters, null, "Name", datColumnSDL, null);
                ExportCostPerDriver = new DataReportObject(dtBCR, "Base Cost Rates per Drivers", "Base Rates By Drivers", strSelectedParameters, null, "Name", datColumnBCR, null);

                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Generated Successfully!"), IconCls = "icon-accept", Clear2 = false });
                X.Mask.Hide();

                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                if (strMessage.Contains("Timeout"))
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = strMessage.ToString(), IconCls = "icon-exclamation", Clear2 = false });
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select all filters please"), IconCls = "icon-exclamation", Clear2 = false });
                }
            } 
        }

        protected void ddlLevels_Select(object sender, DirectEventArgs e)
        {
            this.ddlField.Reset();
            this.storeField.Reload();
            this.ddlOperationType.Reset();
            this.storeOperationType.Reload();
        }

        protected void btnRun_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.ModelCheckRecalcModuleTechnical();
                DisableParameterSelection();

                DataClass.clsModels objModels = new DataClass.clsModels();
                objModels.IdModel = this.IdModel;
                objModels.loadObject();
                intBaseYear = objModels.BaseYear;

                Int32 intAggregationLevel = Convert.ToInt32(ddlLevels.Value);
                Int32 intField = Convert.ToInt32(ddlField.Value);
                Int32 intBaseCostType = Convert.ToInt32(ddlFilterBaseCost.Value);
                Int32 intStage = Convert.ToInt32(ddlFilterStage.Value);
                Int32 intCurrency = Convert.ToInt32(ddlCurrency.Value);

                clsCurrencies objCurrencies = new clsCurrencies();
                objCurrencies.IdCurrency = intCurrency;
                objCurrencies.loadObject();
                String strCurrencyCode = " (" + objCurrencies.Code + ")";

                DataSet ds = objModels.LoadBaseRatesByDrivers(IdModel, intAggregationLevel, intField, intCurrency, intStage, intBaseCostType, idLanguage);

                DataTable dtPC = new DataTable();
                dtPC = ds.Tables[0];
                DataTable dtSDL = new DataTable();
                dtSDL = ds.Tables[1];
                DataTable dtBCR = new DataTable();
                dtBCR = ds.Tables[2];

                ArrayList datColumnPC = new ArrayList();
                ArrayList datColumnSDL = new ArrayList();
                ArrayList datColumnBCR = new ArrayList();

                this.FillGrid(this.storeProjectionCosts, this.grdProjectionCosts, dtPC, strCurrencyCode);
                this.FillGrid(this.storeSizeDriverList, this.grdSizeDriverList, dtSDL, strCurrencyCode);
                this.FillGrid(this.storeCostPerDrivers, this.grdCostPerDrivers, dtBCR, strCurrencyCode);

                String strSelectedParameters = lblLevels.Text + " '" + ddlLevels.SelectedItem.Text + "', " +
                                               lblField.Text + " '" + ddlField.SelectedItem.Text + "', " +
                                               lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "', " +
                                               lblOperationType.Text + " '" + ddlOperationType.SelectedItem.Text + "', " +
                                               lblFilterStage.Text + " '" + ddlFilterStage.SelectedItem.Text + "'";
                ExportProjectionCosts = new DataReportObject(dtPC, "Base Cost By Type", "Base Rates By Drivers", strSelectedParameters, null, "Name", datColumnPC, null);
                ExportDriversList = new DataReportObject(dtSDL, "Technical Drivers", "Base Rates By Drivers", strSelectedParameters, null, "Name", datColumnSDL, null);
                ExportCostPerDriver = new DataReportObject(dtBCR, "Base Cost Rates per Drivers", "Base Rates By Drivers", strSelectedParameters, null, "Name", datColumnBCR, null);
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Report Generated Successfully!"), IconCls = "icon-accept", Clear2 = false });
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                if (strMessage.Contains("Timeout"))
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = strMessage.ToString(), IconCls = "icon-exclamation", Clear2 = false });
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select all filters please"), IconCls = "icon-exclamation", Clear2 = false });
                }
            }
        }

        protected void btnReset_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/technical/baseratesbydrivers.aspx");
        }

        protected void StoreField_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dtField = new DataTable();
            if ((ddlLevels.Value == null) || (ddlLevels.Value.ToString() == "0") || (ddlLevels.Value.ToString() == ""))
            {
                dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
            }
            else
            {
                dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
            }

            DataRow drField = dtField.NewRow();
            drField["IdField"] = 0;
            drField["Name"] = "- All -";
            dtField.Rows.Add(drField);
            dtField.AcceptChanges();
            dtField.DefaultView.Sort = "Name";
            DataTable dtSort = dtField.DefaultView.ToTable();

            this.storeField.DataSource = dtSort;
            this.storeField.DataBind();

            this.ddlField.Value = 0;

            this.storeField.DataBind();
        }

        protected void StoreLevel_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dt = objAggregationLevels.LoadList("IdModel = " + IdModel, "ORDER BY Name");

            DataRow drLevel = dt.NewRow();
            drLevel["IdAggregationLevel"] = 0;
            drLevel["Name"] = "- All -";
            dt.Rows.Add(drLevel);
            dt.AcceptChanges();
            dt.DefaultView.Sort = "Name";
            DataTable dtSort = dt.DefaultView.ToTable();

            this.storeLevels.DataSource = dtSort;
            this.storeLevels.DataBind();

            this.ddlLevels.Value = 0;

            this.storeLevels.DataBind();
        }

        protected void StoreCurr_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsCurrencies objCurrencies = new DataClass.clsCurrencies();
            this.storeBase.DataSource = objCurrencies.LoadList("IdModel IN (" + IdModel + ") AND Output=1", "ORDER BY BaseCurrency DESC,Name");
            this.storeBase.DataBind();
            DataTable dt = objCurrencies.LoadList("BaseCurrency = 1 AND IdModel = " + IdModel, "");
            if (dt.Rows.Count > 0)
            {
                ddlCurrency.Value = dt.Rows[0]["IdCurrency"];
            }
            else
            {
                ddlCurrency.SelectedItem.Index = 0;
            }
        }

        //protected void StoreCostType_ReadData(object sender, StoreReadDataEventArgs e)
        //{
        //    DataClass.clsCostType objCostTypes = new DataClass.clsCostType();
        //    this.storeCostType.DataSource = objCostTypes.LoadList("", "ORDER BY Name");
        //    this.storeCostType.DataBind();
        //    DataTable dt = objCostTypes.LoadList("", "");
        //    if (dt.Rows.Count > 0)
        //    {
        //        ddlCostType.Value = dt.Rows[0]["IdCostType"];
        //    }
        //    else
        //    {
        //        ddlCostType.SelectedItem.Index = 0;
        //    }
        //}

        protected void StoreOperationType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsOperationType objoperationTypes = new DataClass.clsOperationType();
            DataTable dtOperation = new DataTable();

            if (((ddlLevels.Value == null) || (ddlLevels.Value.ToString() == "0") || (ddlLevels.Value.ToString() == "")) && ((ddlField.Value == null) || (ddlField.Value.ToString() == "0") || (ddlField.Value.ToString() == "")))
            {
                dtOperation = objoperationTypes.LoadList("IdModel=" + IdModel, "");//"[vListTypesOperation].IdModel=" + IdModel, "");

                DataRow drOperation = dtOperation.NewRow();
                drOperation["IdTypeOperation"] = 0;
                drOperation["Name"] = "- All -";
                dtOperation.Rows.Add(drOperation);
                dtOperation.AcceptChanges();
                dtOperation.DefaultView.Sort = "Name";

                ddlOperationType.Value = 0;
            }
            else if (Convert.ToInt32(ddlLevels.Value) > 0)
            {
                dtOperation = objoperationTypes.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
                if (dtOperation.Rows.Count > 0)
                {
                    ddlOperationType.Value = dtOperation.Rows[0]["IdTypeOperation"];
                }
            }
            else
            {
                dtOperation = objoperationTypes.LoadList("IdModel=" + IdModel + " AND IdField = " + ddlField.Value, " Order by Name");//"[vListTypesOperation].IdModel=" + IdModel + " AND tblFields.IdField = " + ddlField.Value, " Order by Name");
                if (dtOperation.Rows.Count > 0)
                {
                    ddlOperationType.Value = dtOperation.Rows[0]["IdTypeOperation"];
                }
            }

            DataTable dtOperationSort = dtOperation.DefaultView.ToTable();

            this.storeOperationType.DataSource = dtOperationSort;
            this.storeOperationType.DataBind();
        }

        protected void StoreFilterBaseCost_ReadData(object sender, StoreReadDataEventArgs e)
        {
            this.ddlFilterBaseCost.Text = "";
            DataClass.clsParameters objParameters = new DataClass.clsParameters();
            this.storeBaseCostCond.DataSource = objParameters.LoadList("Type = 'BaseCost' AND Name LIKE '%" + this.ddlFilterBaseCost.Text + "%'" + " AND IdLanguage = " + idLanguage, "ORDER BY Name");
            this.storeBaseCostCond.DataBind();
        }

        protected void StoreFilterStage_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsStage objStage = new DataClass.clsStage();
            DataTable dt = objStage.LoadComboBox("Name LIKE '%" + this.ddlFilterStage.Text + "%'", "ORDER BY Name");
            foreach (DataRow row in dt.Rows)
            {
                if (idLanguage == 2)
                {
                    row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
                }
            }
            this.storeFilterStage.DataSource = dt;
            this.storeFilterStage.DataBind();
        }

        #endregion

        #region Methods

        private void FillGrid(Ext.Net.Store s, GridPanel g, DataTable dt, String CurrencyCode)
        {
            s.RemoveFields();

            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "Name")
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Auto));
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Float));
                }
            }

            g.Features.Clear();

            GridFilters f = new GridFilters();
            f.Local = true;
            g.Features.Add(f);

            if (g.ID == "grdProjectionCosts")
            {
                Summary smry = new Summary();
                smry.Dock = new SummaryDock();
                smry.ID = "Summary" + g.ID.ToString();

                g.Features.Add(smry);
            }

            foreach (DataColumn c in dt.Columns)
            {
                if (!c.ColumnName.Contains("Id"))
                {
                    if (c.ColumnName == "Name")
                    {
                        StringFilter sFilter = new StringFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                    else
                    {
                        NumericFilter sFilter = new NumericFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                }
            }

            Column col = new Column();
            switch (g.ID)
            {
                case "grdSizeDriverList":
                    col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Fields");
                    break;
                default:
                    col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Fields") + CurrencyCode;
                    break;
            }
            SummaryColumn colSum = new SummaryColumn();
            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "Name")
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        if (c.ColumnName != "Activity")
                        {
                            colSum = new SummaryColumn();
                            colSum.DataIndex = c.ColumnName;
                            switch (g.ID)
                            {
                                case "grdProjectionCosts":
                                    colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", "Base Cost By Type") + " <BR>(" + intBaseYear + ")";
                                    break;
                                case "grdSizeDriverList":
                                    colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", "Technical Drivers") + " <BR>(" + intBaseYear + ")";
                                    break;
                                case "grdCostPerDrivers":
                                    colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", "Base Cost Rates Per Driver") + "<BR>(" + intBaseYear + ")";
                                    break;
                            }
                            colSum.Locked = true;
                            colSum.SummaryType = Ext.Net.SummaryType.Count;
                            colSum.TdCls = "task";
                            colSum.Sortable = true;
                            colSum.Groupable = true;
                            colSum.Width = 300;
                            colSum.SummaryRenderer.Handler = "return '(' + value + ' Total)';";
                            colSum.MenuDisabled = false;
                            colSum.Align = Alignment.Left;
                            g.ColumnModel.Columns.Add(colSum);
                        }
                    }
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        colSum = new SummaryColumn();
                        colSum.DataIndex = c.ColumnName;
                        colSum.Text = c.ColumnName.Replace("_", " ");
                        colSum.SummaryType = Ext.Net.SummaryType.Sum;
                        Renderer sumRen = new Renderer();
                        sumRen.Fn = "Ext.util.Format.numberRenderer('0,000.00')";
                        colSum.SummaryRenderer = sumRen;
                        colSum.Renderer = sumRen;
                        colSum.Align = Alignment.Right;
                        colSum.MinWidth = 120;
                        col.Columns.Add(colSum);
                    }
                }

            }

            g.ColumnModel.Columns.Add(col);

            s.DataSource = dt;

            if (g.ID == "grdProjectionCosts")
            {
                s.GroupField = "Name";
            }
            if (X.IsAjaxRequest)
            {
                g.Reconfigure();
            }
            s.DataBind();
        }

        private Int32 GetIdField(String _Name)
        {
            Int32 intID = 0;

            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dtField = objFields.LoadList("Name = '" + _Name + "' AND IdModel = " + IdModel, "ORDER BY Name");

            if (dtField.Rows.Count > 0)
            {
                intID = (Int32)dtField.Rows[0]["IdField"];
            }

            return intID;
        }

        private Int32 GetIdLevel(String _Name)
        {
            Int32 intID = 0;

            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dtLevel = objAggregationLevels.LoadList("IdModel = " + IdModel + " AND Name = '" + _Name + "'", "ORDER BY Name");

            if (dtLevel.Rows.Count > 0)
            {
                intID = (Int32)dtLevel.Rows[0]["IdAggregationLevel"];
            }

            return intID;
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExportPC = new DataReportObject();
            StoredExportPC = ExportProjectionCosts;
            DataReportObject StoredExportSDL = new DataReportObject();
            StoredExportSDL = ExportDriversList;
            DataReportObject StoredExportBCR = new DataReportObject();
            StoredExportBCR = ExportCostPerDriver;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExportPC);
            objWriter.ExportReportCollection.Add(StoredExportSDL);
            objWriter.ExportReportCollection.Add(StoredExportBCR);
            objWriter.BuildExportFile(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2007);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=BaseRatesByDrivers.xlsx");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        //[DirectMethod]
        //public void ClickedSaveExcelDirectYES()
        //{
        //    try
        //    {
        //        this.ModelCheckRecalcModuleTechnical();
        //        DisableParameterSelection();

        //        DataClass.clsModels objModels = new DataClass.clsModels();
        //        objModels.IdModel = this.IdModel;
        //        objModels.loadObject();
        //        intBaseYear = objModels.BaseYear;

        //        Int32 intAggregationLevel = Convert.ToInt32(ddlLevels.Value);
        //        Int32 intField = Convert.ToInt32(ddlField.Value);
        //        Int32 intBaseCostType = Convert.ToInt32(ddlFilterBaseCost.Value);
        //        Int32 intStage = Convert.ToInt32(ddlFilterStage.Value);
        //        Int32 intCurrency = Convert.ToInt32(ddlCurrency.Value);

        //        clsCurrencies objCurrencies = new clsCurrencies();
        //        objCurrencies.IdCurrency = intCurrency;
        //        objCurrencies.loadObject();
        //        String strCurrencyCode = " (" + objCurrencies.Code + ")";

        //        DataSet ds = objModels.LoadBaseRatesByDrivers(IdModel, intAggregationLevel, intField, intCurrency, intStage, intBaseCostType);

        //        DataTable dtPC = new DataTable();
        //        dtPC = ds.Tables[0];
        //        DataTable dtSDL = new DataTable();
        //        dtSDL = ds.Tables[1];
        //        DataTable dtBCR = new DataTable();
        //        dtBCR = ds.Tables[2];

        //        ArrayList datColumnPC = new ArrayList();
        //        ArrayList datColumnSDL = new ArrayList();
        //        ArrayList datColumnBCR = new ArrayList();

        //        String strSelectedParameters = lblLevels.Text + " '" + ddlLevels.SelectedItem.Text + "', " +
        //                                       lblField.Text + " '" + ddlField.SelectedItem.Text + "', " +
        //                                       lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "', " +
        //                                       lblOperationType.Text + " '" + ddlOperationType.SelectedItem.Text + "', " +
        //                                       lblFilterStage.Text + " '" + ddlFilterStage.SelectedItem.Text + "'";
        //        ExportProjectionCosts = new DataReportObject(dtPC, "Base Cost By Type", "Projection Costs", strSelectedParameters, null, "Name", datColumnPC, null);
        //        ExportDriversList = new DataReportObject(dtSDL, "Drivers", "Technical Drivers", strSelectedParameters, null, "Name", datColumnSDL, null);
        //        ExportCostPerDriver = new DataReportObject(dtBCR, "Cost Rates", "Base Cost Rates per Drivers", strSelectedParameters, null, "Name", datColumnBCR, null);

        //        String strReturnMessage = null;
        //        DataFileWriter objWriter = null;

        //        clsUsers objUser = new clsUsers();
        //        objUser.IdUser = IdUserCreate;
        //        objUser.loadObject();

        //        DataReportObject StoredExportPC = new DataReportObject();
        //        StoredExportPC = ExportProjectionCosts;
        //        DataReportObject StoredExportSDL = new DataReportObject();
        //        StoredExportSDL = ExportDriversList;
        //        DataReportObject StoredExportBCR = new DataReportObject();
        //        StoredExportBCR = ExportCostPerDriver;

        //        objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
        //        objWriter.ExportReportCollection = new List<DataReportObject>();
        //        objWriter.ExportReportCollection.Add(StoredExportPC);
        //        objWriter.ExportReportCollection.Add(StoredExportSDL);
        //        objWriter.ExportReportCollection.Add(StoredExportBCR);
        //        objWriter.BuildExportFile(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2007);
        //        if (String.IsNullOrEmpty(strReturnMessage))
        //        {
        //            HttpContext context = HttpContext.Current;
        //            context.Response.Clear();
        //            context.Response.AddHeader("content-disposition", "attachment; filename=CostByFieldZiff.xls");
        //            context.Response.ContentType = "application/octet-stream";
        //            objWriter.objExcel.Save(context.Response.OutputStream);
        //            context.Response.End();
        //        }
                
        //        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Report Generated Successfully!"), IconCls = "icon-accept", Clear2 = false });
        //    }
        //    catch (Exception ex)
        //    {
        //        String strMessage = ex.Message;
        //        if (strMessage.Contains("Timeout"))
        //        {
        //            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = strMessage.ToString(), IconCls = "icon-exclamation", Clear2 = false });
        //        }
        //        else
        //        {
        //            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select all filters please"), IconCls = "icon-exclamation", Clear2 = false });
        //        }
        //    }
        //}

        private void DisableParameterSelection()
        {
            this.pnlExportToolbar.Hidden = false;
            this.pnlReportToolbar.Hidden = true;
            this.ddlLevels.ReadOnly = true;
            this.ddlField.ReadOnly = true;
            this.ddlCurrency.ReadOnly = true;
            //this.ddlCostType.ReadOnly = true;
            this.ddlOperationType.ReadOnly = true;
            this.ddlFilterBaseCost.ReadOnly = true;
            this.ddlFilterStage.ReadOnly = true;
        }

        public void ModelCheckRecalcModuleTechnical()
        {
            clsModels objModelRecalc = new clsModels();
            objModelRecalc.IdModel = IdModel;
            objModelRecalc.CheckRecalcModuleParameter(objApplication.MaxRelationLevels);
            objModelRecalc.CheckRecalcModuleTechnical();
            objModelRecalc = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = modMain.strTechnicalModuleBaseRatesByDrivers;
            this.lblLevels.Text = (String)GetGlobalResourceObject("CPM_Resources", "Aggregation Level") + ":";
            this.lblField.Text = modMain.strTechnicalModuleField;
            this.lblCurrency.Text = (String)GetGlobalResourceObject("CPM_Resources", "Currency") + ":";
            //this.lblCostType.Text = (String)GetGlobalResourceObject("CPM_Resources", "Cost Type") + ":";
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnReset.Text = (String)GetGlobalResourceObject("CPM_Resources", "Reset");
            this.btnRun.Text = (String)GetGlobalResourceObject("CPM_Resources", "Run Report");
            this.lblFilterBaseCost.Text = (String)GetGlobalResourceObject("CPM_Resources", "Base Cost Reference:");
            this.lblFilterStage.Text = (String)GetGlobalResourceObject("CPM_Resources", "Technical Scenario:");
            this.btnDirectExcel.Text = modMain.strReportsModuleExportReportDataToExcel;
        }

        #endregion

    }
}
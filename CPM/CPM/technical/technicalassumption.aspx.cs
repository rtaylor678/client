﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using System.Xml.Xsl;
using CD;
using DataClass;
using System.ComponentModel;
using System.Web.Script.Serialization;

namespace CPM
{
    public partial class _technicalassumption : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdTechnicalAssumption { get { return (Int32)Session["IdTechnicalAssumption"]; } set { Session["IdTechnicalAssumption"] = value; } }
        public Int32 IdZiffAccount { get { return (Int32)Session["IdZiffAccount"]; } set { Session["IdZiffAccount"] = value; } }
        public Int32 IdTechnicalDriverSize { get { return (Int32)Session["IdTechnicalDriverSize"]; } set { Session["IdTechnicalDriverSize"] = value; } }
        public Int32 IdTechnicalDriverPerformance { get { return (Int32)Session["IdTechnicalDriverPerformance"]; } set { Session["IdTechnicalDriverPerformance"] = value; } }
        public Int32 ProjectionCriteria { get { return (Int32)Session["ProjectionCriteria"]; } set { Session["ProjectionCriteria"] = value; } }
        public Int32 CycleValue { get { return (Int32)Session["CycleValue"]; } set { Session["CycleValue"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        public DataTable datViewData { get; set; }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }
        private DataReportObject ExportTechnicalAssumption { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                IdTechnicalAssumption = 0;
                this.ModelPrecalcModuleTechnical();
                LoadLabel();
            }
        }

        protected void ddlFilterTypeOperation_Select(object sender, DirectEventArgs e)
        {
            this.ModelPrecalcModuleTechnical();
            String cond = "";
            String cond2 = "";
            DataTable dtSize;
            DataTable dtPerformance;

            if (ddlFilterTypeOperation.SelectedItem.Value != null && ddlFilterTypeOperation.SelectedItem.Text != null)
            {
                if (cond != "")
                {
                    cond = cond + " And ";
                }
                cond = cond + "tblTechnicalDrivers.IdTypeOperation=" + ddlFilterTypeOperation.SelectedItem.Value;
                cond2 = cond;
                this.StoreProjectionCriteria_ReadData();
                this.StorePerformanceDriver_ReadData();
                this.StoreSizeDriver_ReadData();
            }
            else
            {
                storeTechnicalAssumption.RemoveAll();
            }

            LoadTechnicalAssumptions();

            if (cond != "")
            {
                cond = " And " + cond;
            }
            ddlSizeDriver.ClearValue();
            ddlPerformanceDriver.ClearValue();
            DataClass.clsTechnicalDrivers objTechnicalDrivers = new DataClass.clsTechnicalDrivers();
            dtSize = objTechnicalDrivers.LoadComboBox("TypeDriver=2 " + cond, " ORDER BY Name");
            dtPerformance = objTechnicalDrivers.LoadComboBox("TypeDriver=1 " + cond, " ORDER BY Name");
            if (dtPerformance.Rows.Count > 0)
            {
                this.ddlPerformanceDriver.SetValueAndFireSelect(dtPerformance.Rows[0]["Name"]);
            }
            if (dtSize.Rows.Count > 0)
            {
                this.ddlSizeDriver.SetValueAndFireSelect(dtSize.Rows[0]["Name"]);
            }
        }

        protected void ddlProjectionCriteria_Select(object sender, DirectEventArgs e)
        {
            int intValue = Convert.ToInt32(e.ExtraParams["CriteriaValue"]);
            Int32 indexrowsel = RowSelectionModel1.SelectedIndex;
            this.grdTechnicalAssumptions.GetStore().GetAt(indexrowsel).Set("IdTechnicalDriverSize", 0);
            this.grdTechnicalAssumptions.GetStore().GetAt(indexrowsel).Set("IdTechnicalDriverPerformance", 0);
            this.grdTechnicalAssumptions.GetStore().GetAt(indexrowsel).Set("CycleValue", 0);
            switch (intValue.ToString())
            {
                case "1":
                    ddlSizeDriver.Disable();
                    ddlPerformanceDriver.Disable();
                    txtCycleValue.Disable();
                    ddlSizeDriver.Clear();
                    ddlPerformanceDriver.Select(0);
                    txtCycleValue.Value = "0";
                    break;
                case "2":
                    ddlSizeDriver.Enable();
                    ddlPerformanceDriver.Disable();
                    txtCycleValue.Disable();
                    txtCycleValue.Value = "0";
                    break;
                case "3":
                    ddlSizeDriver.Enable();
                    ddlPerformanceDriver.Enable();
                    txtCycleValue.Disable();
                    txtCycleValue.Value = "0";
                    break;
                case "4":
                    ddlSizeDriver.Enable();
                    ddlPerformanceDriver.Enable();
                    txtCycleValue.Enable();
                    txtCycleValue.Value = "2";
                    break;
                default:
                    ddlSizeDriver.Disable();
                    ddlPerformanceDriver.Disable();
                    txtCycleValue.Disable();
                    ddlSizeDriver.ClearValue();
                    ddlPerformanceDriver.Select(0);
                    txtCycleValue.Value = "0";
                    break;
            }
        }

        protected void Technical_BeforeEdit(object sender, DirectEventArgs e)
        {
            int Criteria = Convert.ToInt32(e.ExtraParams["Criteria"]);

            switch (Criteria)
            {
                case 0:
                    this.ddlSizeDriver.Disable();
                    this.ddlPerformanceDriver.Disable();
                    this.txtCycleValue.Disable();
                    this.ddlProjectionCriteria.Value = 98;
                    this.ddlSizeDriver.Value = null;
                    this.ddlPerformanceDriver.Value = null;
                    break;

                case 1:
                    this.ddlSizeDriver.Disable();
                    this.ddlPerformanceDriver.Disable();
                    this.txtCycleValue.Disable();
                    this.ddlSizeDriver.Value = null;
                    this.ddlPerformanceDriver.Value = null;
                    break;

                case 2:
                    this.ddlSizeDriver.Enable();
                    this.ddlPerformanceDriver.Disable();
                    this.txtCycleValue.Disable();
                    this.ddlSizeDriver.Value = null;
                    this.ddlPerformanceDriver.Value = null;
                    break;

                case 3:
                    this.ddlSizeDriver.Enable();
                    this.ddlPerformanceDriver.Enable();
                    this.txtCycleValue.Disable();
                    this.ddlSizeDriver.Value = null;
                    this.ddlPerformanceDriver.Value = null;
                    break;

                case 4:
                    this.ddlSizeDriver.Enable();
                    this.ddlPerformanceDriver.Enable();
                    this.txtCycleValue.Enable();
                    this.ddlSizeDriver.Value = null;
                    this.ddlPerformanceDriver.Value = null;
                    break;

                case 5:
                    this.ddlSizeDriver.Enable();
                    this.ddlPerformanceDriver.Enable();
                    this.txtCycleValue.Enable();
                    this.ddlSizeDriver.Value = null;
                    this.ddlPerformanceDriver.Value = null;
                    break;

                case 98:
                    this.ddlSizeDriver.Disable();
                    this.ddlPerformanceDriver.Disable();
                    this.txtCycleValue.Disable();
                    this.ddlSizeDriver.Value = null;
                    this.ddlPerformanceDriver.Value = null;
                    break;

            }
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                String strData = e.ExtraParams["rowsValues"];
                datViewData = JSON.Deserialize<DataTable>(strData);
                this.Save();
                this.LoadTechnicalAssumptions();

            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void StorePerformanceDriver_ReadData()
        {
            String cond = "";
            if (ddlFilterTypeOperation.SelectedItem.Value != null && ddlFilterTypeOperation.SelectedItem.Text != null)
            {
                if (cond != "")
                {
                    cond = cond + " AND ";
                }
                cond = cond + "(tblTechnicalDrivers.IdTypeOperation=" + ddlFilterTypeOperation.SelectedItem.Value + " OR tblTechnicalDrivers.IdTypeOperation=0)";
            }
            if (cond != "")
            {
                cond = " AND " + cond;
            }
            DataClass.clsTechnicalDrivers objTypesOperation = new DataClass.clsTechnicalDrivers();
            this.storePerformanceDriver.DataSource = objTypesOperation.LoadComboBox("IdModel = " + IdModel + " AND TypeDriver = 2 " + cond, " ORDER BY Name");
            this.storePerformanceDriver.DataBind();
        }

        protected void StoreSizeDriver_ReadData()
        {
            String cond = "";
            if (ddlFilterTypeOperation.SelectedItem.Value != null && ddlFilterTypeOperation.SelectedItem.Text != null && ddlFilterTypeOperation.SelectedItem.Value != "0")
            {
                if (cond != "")
                {
                    cond = cond + " AND ";
                }
                cond = cond + "(tblTechnicalDrivers.IdTypeOperation =" + ddlFilterTypeOperation.SelectedItem.Value + " OR tblTechnicalDrivers.IdTypeOperation=0)";
            }
            if (cond != "")
            {
                cond = " AND " + cond;
            }
            DataClass.clsTechnicalDrivers objTechnicalDrivers = new DataClass.clsTechnicalDrivers();
            this.storeSizeDriver.DataSource = objTechnicalDrivers.LoadComboBox("IdModel = " + IdModel + " AND TypeDriver = 1 " + cond, " ORDER BY Name");
            this.storeSizeDriver.DataBind();
        }

        protected void StoreProjectionCriteria_ReadData()
        {
            DataClass.clsParameters objParameters = new DataClass.clsParameters();

            if (ddlFilterTypeOperation.SelectedItem.Value != null)
            {
                if (Convert.ToInt32(ddlFilterTypeOperation.SelectedItem.Value) == 0)
                {
                    // ToDo: Temporary Fix to remove SemiVariable - Need to rework//
                    //this.storeProjectionCriteria.DataSource = objParameters.LoadList("Type='ProjectionCriteria' AND Name<>'SemiVariable'", "ORDER BY Name");
                    this.storeProjectionCriteria.DataSource = objParameters.LoadList("Type='ProjectionCriteria' AND IdLanguage = " + idLanguage, "ORDER BY Name");
                    this.storeProjectionCriteria.DataBind();
                }
                else
                {
                    // ToDo: Temporary Fix to remove SemiVariable - Need to rework//
                    //this.storeProjectionCriteria.DataSource = objParameters.LoadList("Type='ProjectionCriteria' AND Name<>'SemiVariable' AND Name<>'SemiFixed'", "ORDER BY Name");
                    this.storeProjectionCriteria.DataSource = objParameters.LoadList("Type='ProjectionCriteria' AND Name<>'SemiFixed' AND IdLanguage = " + idLanguage, "ORDER BY Name");
                    this.storeProjectionCriteria.DataBind();
                }
            }
        }

        protected void StoreFilterTypeOperation_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsTypesOperation objTypesOperation = new DataClass.clsTypesOperation();
            this.storeFilterTypeOperation.DataSource = objTypesOperation.LoadListWithGeneral("IdModel = " + IdModel + " AND LastNode = 1", "ORDER BY SortOrder, Name");
            this.storeFilterTypeOperation.DataBind();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            try
            {
                String strReturnMessage = null;
                if (this.fileImport.HasFile)
                {
                    DataFileReader objReader = null;
                    DataFileType objType = new DataFileType();
                    objType.CheckFileType(fileImport.PostedFile.FileName);

                    if (!objType.IsValidType)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }

                    if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                    {
                        objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.TechnicalAssumptions);
                        objReader.GetDataTable(ref strReturnMessage);
                    }
                    else
                    {
                        strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                    }

                    if (String.IsNullOrEmpty(strReturnMessage))
                    {
                        intFailedImportRowCount = 0;
                        strImportErrorMessage = "";
                        intCurrentImportRow = 0;
                        foreach (DataRow row in objReader.ImportTable.Rows)
                        {
                            intCurrentImportRow++;
                            this.SaveImport(row["Type of Operation"].ToString(), row["Cost Category"].ToString(), row["Projection Criteria"].ToString(), row["Size Drivers"].ToString(), row["Performance Drivers"].ToString(), row["Cycle Term"].ToString());
                        }
                        if (intFailedImportRowCount != 0)
                        {
                            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                        }
                        else
                        {
                            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                        }
                        this.winImport.Hide();
                        this.ModelUpdateTimestamp();
                        //Trim Error if too long
                        if (strImportErrorMessage.Length > 1024)
                        {
                            strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                        }
                        X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                    }

                    this.LoadTechnicalAssumptions();
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
                }
            }
            catch
            {
            }
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        protected void Save()
        {
            Boolean blnRunSemifixedCleanup = false;
            foreach (DataRow row in datViewData.Rows)
            {
                DataClass.clsTechnicalAssumptions objTechnicalAssumptions = new DataClass.clsTechnicalAssumptions();
                objTechnicalAssumptions.IdTechnicalAssumption = Convert.ToInt32(row["IdTechnicalAssumption"]);
                objTechnicalAssumptions.loadObject();
                objTechnicalAssumptions.IdModel = IdModel;
                objTechnicalAssumptions.IdTypeOperation = Convert.ToInt32(ddlFilterTypeOperation.SelectedItem.Value);
                objTechnicalAssumptions.IdZiffAccount = Convert.ToInt32(row["IdZiffAccount"]);
                objTechnicalAssumptions.ProjectionCriteria = Convert.ToInt32(row["ProjectionCriteria"]);
                if (objTechnicalAssumptions.ProjectionCriteria != 0 && objTechnicalAssumptions.ProjectionCriteria != 99)
                {
                    switch (objTechnicalAssumptions.ProjectionCriteria)
                    {
                        case 1:
                            objTechnicalAssumptions.IdTechnicalDriverSize = 0;
                            objTechnicalAssumptions.IdTechnicalDriverPerformance = 0;
                            objTechnicalAssumptions.CycleValue = 0;
                            break;
                        case 2:
                            objTechnicalAssumptions.IdTechnicalDriverSize = Convert.ToInt32(row["IdTechnicalDriverSize"]);
                            objTechnicalAssumptions.IdTechnicalDriverPerformance = Convert.ToInt32(row["IdTechnicalDriverPerformance"]);
                            objTechnicalAssumptions.CycleValue = 0;
                            break;
                        case 3:
                            objTechnicalAssumptions.IdTechnicalDriverSize = Convert.ToInt32(row["IdTechnicalDriverSize"]);
                            objTechnicalAssumptions.IdTechnicalDriverPerformance = Convert.ToInt32(row["IdTechnicalDriverPerformance"]);
                            objTechnicalAssumptions.CycleValue = 0;
                            break;
                        case 4:
                            objTechnicalAssumptions.IdTechnicalDriverSize = Convert.ToInt32(row["IdTechnicalDriverSize"]);
                            objTechnicalAssumptions.IdTechnicalDriverPerformance = Convert.ToInt32(row["IdTechnicalDriverPerformance"]);
                            objTechnicalAssumptions.CycleValue = Convert.ToInt32(row["CycleValue"]);
                            break;
                        case 5:
                            objTechnicalAssumptions.IdTechnicalDriverSize = Convert.ToInt32(row["IdTechnicalDriverSize"]);
                            objTechnicalAssumptions.IdTechnicalDriverPerformance = Convert.ToInt32(row["IdTechnicalDriverPerformance"]);
                            objTechnicalAssumptions.CycleValue = 0;
                            break;
                        default:
                            objTechnicalAssumptions.IdTechnicalDriverSize = 0;
                            objTechnicalAssumptions.IdTechnicalDriverPerformance = 0;
                            objTechnicalAssumptions.CycleValue = 0;
                            break;
                    }
                    if (objTechnicalAssumptions.IdTechnicalAssumption == 0)
                    {
                        objTechnicalAssumptions.DateCreation = DateTime.Now;
                        objTechnicalAssumptions.UserCreation = (Int32)IdUserCreate;
                        IdTechnicalAssumption = objTechnicalAssumptions.Insert();
                    }
                    else
                    {
                        objTechnicalAssumptions.DateModification = DateTime.Now;
                        objTechnicalAssumptions.UserModification = (Int32)IdUserCreate;
                        objTechnicalAssumptions.Update();
                    }
                    if (objTechnicalAssumptions.ProjectionCriteria == 2 && objTechnicalAssumptions.IdTypeOperation == 0)
                    {
                        blnRunSemifixedCleanup = true;
                    }
                }
                else
                {
                    if (objTechnicalAssumptions.IdTechnicalAssumption != 0)
                    {
                        objTechnicalAssumptions.Delete();
                    }
                }
            }
            if (blnRunSemifixedCleanup)
            {
                DataClass.clsTechnicalAssumptions objTechnicalAssumptions = new DataClass.clsTechnicalAssumptions();
                objTechnicalAssumptions.CleanupSemifixed();
                objTechnicalAssumptions = null;
            }
            ModelUpdateTimestamp();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Record saved successfully!"), IconCls = "icon-accept", Clear2 = false });
        }

        protected void SaveImport(String _TypeOperation, String _CostCategory, String _ProjectionCriteria, String _SizeDrivers, String _PerformanceDrivers, String _CycleTerm)
        {
            try
            {
                Boolean blnRunSemifixedCleanup = false;
                Int32 _IdTypeOperation = GetIdTypeOperation(_TypeOperation);
                Int32 _IdZiffAccount = GetIdZiffAccount(_CostCategory);
                Int32 _IdProjectionCriteria = GetProjectionCriteria(_ProjectionCriteria);
                Int32 _IdTechnicalDriverSize = GetIdTechnicalDriverSize(_SizeDrivers);
                Int32 _IdTechnicalDriverPerformance = GetIdTechnicalDriverPerformance(_PerformanceDrivers);


                if (_IdZiffAccount != 0 && _IdProjectionCriteria != 0)
                {
                    DataClass.clsTechnicalAssumptions objTechnicalAssumptions = new DataClass.clsTechnicalAssumptions();
                    DataTable dtAux = objTechnicalAssumptions.LoadListImport("IdModel = " + IdModel + " AND IdZiffAccount = " + _IdZiffAccount, "");
                    if (dtAux.Rows.Count > 0)
                    {
                        IdTechnicalAssumption = Convert.ToInt32(dtAux.Rows[0]["IdTechnicalAssumption"]);
                    }
                    else
                    {
                        IdTechnicalAssumption = 0;
                    }
                    if (IdTechnicalAssumption != 0)
                    {
                        objTechnicalAssumptions.IdTechnicalAssumption = (Int32)IdTechnicalAssumption;
                        objTechnicalAssumptions.loadObjectImport();
                    }
                    else
                    {
                        objTechnicalAssumptions.IdModel = IdModel;
                        objTechnicalAssumptions.IdZiffAccount = _IdZiffAccount;
                    }
                    objTechnicalAssumptions.IdTypeOperation = _IdTypeOperation;
                    objTechnicalAssumptions.ProjectionCriteria = _IdProjectionCriteria;
                    if (objTechnicalAssumptions.ProjectionCriteria != 0 && objTechnicalAssumptions.ProjectionCriteria != 99)
                    {
                        switch (objTechnicalAssumptions.ProjectionCriteria)
                        {
                            case 1:
                                objTechnicalAssumptions.IdTechnicalDriverSize = 0;
                                objTechnicalAssumptions.IdTechnicalDriverPerformance = 0;
                                objTechnicalAssumptions.CycleValue = 0;
                                break;
                            case 2:
                                objTechnicalAssumptions.IdTechnicalDriverSize = _IdTechnicalDriverSize;
                                objTechnicalAssumptions.IdTechnicalDriverPerformance = _IdTechnicalDriverPerformance;
                                objTechnicalAssumptions.CycleValue = 0;
                                break;
                            case 3:
                                objTechnicalAssumptions.IdTechnicalDriverSize = _IdTechnicalDriverSize;
                                objTechnicalAssumptions.IdTechnicalDriverPerformance = _IdTechnicalDriverPerformance;
                                objTechnicalAssumptions.CycleValue = 0;
                                break;
                            case 4:
                                objTechnicalAssumptions.IdTechnicalDriverSize = _IdTechnicalDriverSize;
                                objTechnicalAssumptions.IdTechnicalDriverPerformance = _IdTechnicalDriverPerformance;
                                objTechnicalAssumptions.CycleValue = Convert.ToInt32(_CycleTerm);
                                break;
                            case 5:
                                objTechnicalAssumptions.IdTechnicalDriverSize = _IdTechnicalDriverSize;
                                objTechnicalAssumptions.IdTechnicalDriverPerformance = _IdTechnicalDriverPerformance;
                                objTechnicalAssumptions.CycleValue = 0;
                                break;
                            default:
                                objTechnicalAssumptions.IdTechnicalDriverSize = 0;
                                objTechnicalAssumptions.IdTechnicalDriverPerformance = 0;
                                objTechnicalAssumptions.CycleValue = 0;
                                break;
                        }
                        if (objTechnicalAssumptions.IdTechnicalAssumption == 0)
                        {
                            objTechnicalAssumptions.DateCreation = DateTime.Now;
                            objTechnicalAssumptions.UserCreation = (Int32)IdUserCreate;
                            IdTechnicalAssumption = objTechnicalAssumptions.Insert();
                        }
                        else
                        {
                            objTechnicalAssumptions.DateModification = DateTime.Now;
                            objTechnicalAssumptions.UserModification = (Int32)IdUserCreate;
                            objTechnicalAssumptions.Update();
                        }
                        if (objTechnicalAssumptions.ProjectionCriteria == 2 && objTechnicalAssumptions.IdTypeOperation == 0)
                        {
                            blnRunSemifixedCleanup = true;
                        }
                    }
                    else
                    {
                        if (objTechnicalAssumptions.IdTechnicalAssumption != 0)
                        {
                            objTechnicalAssumptions.Delete();
                        }
                    }

                }
                else
                {
                    intFailedImportRowCount++;
                    strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
                }
                if (blnRunSemifixedCleanup)
                {
                    DataClass.clsTechnicalAssumptions objTechnicalAssumptions = new DataClass.clsTechnicalAssumptions();
                    objTechnicalAssumptions.CleanupSemifixed();
                    objTechnicalAssumptions = null;
                }
                //ModelUpdateTimestamp();
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
            }
            catch
            {
            }
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
            this.fileImport.Reset();
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataClass.clsTechnicalAssumptions objTechnicalAssumptions = new DataClass.clsTechnicalAssumptions();
            objTechnicalAssumptions.IdModel = IdModel;
            objTechnicalAssumptions.IdTypeOperation = Convert.ToInt32(ddlFilterTypeOperation.SelectedItem.Value);
            DataTable dt = objTechnicalAssumptions.LoadExport();


            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Type of Operation", "Type of Operation"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Cost Category", "Cost Category"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Projection Criteria", "Projection Criteria"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Size Drivers", "Size Drivers"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Performance Drivers", "Performance Drivers"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Cycle Term", "Cycle Term"));
            String strSelectedParameters = "";

            ExportTechnicalAssumption = new DataReportObject(dt, "DATASHEET", null, strSelectedParameters, null, null, datColumnTD, null);

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportTechnicalAssumption;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=4.3 ExportTechnicalAssumptions.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportTechnicalAssumptions.xls", "template");
        }

        protected Int32 GetIdTypeOperation(String _TypeOperation)
        {
            Int32 Value = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT IdTypeOperation FROM tblTypesOperation WHERE UPPER(Name) = '" + _TypeOperation.ToUpper() + "'").Tables[0];
            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdTypeOperation"];
            }
            return Value;
        }

        protected Int32 GetIdZiffAccount(String _CostCategory)
        {
            Int32 Value = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT IdZiffAccount FROM tblZiffAccounts WHERE IdModel = " + IdModel + " AND UPPER(Code) = '" + _CostCategory.ToUpper() + "'").Tables[0];
            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdZiffAccount"];
            }
            return Value;
        }

        protected Int32 GetProjectionCriteria(String _ProjectionCriteria)
        {
            Int32 Value = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT Code FROM tblParameters WHERE Type = 'ProjectionCriteria' AND IdLanguage = " + idLanguage + " AND UPPER(Name) = '" + _ProjectionCriteria.ToUpper() + "'").Tables[0];
            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["Code"];
            }
            return Value;
        }

        protected Int32 GetIdTechnicalDriverSize(String _SizeDrivers)
        {
            Int32 Value = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT IdTechnicalDriver FROM tblTechnicalDrivers WHERE IdModel = " + IdModel + " AND TypeDriver = 1 AND UPPER(Name) = '" + _SizeDrivers.ToUpper() + "'").Tables[0];
            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdTechnicalDriver"];
            }
            return Value;
        }

        protected Int32 GetIdTechnicalDriverPerformance(String _PerformanceDrivers)
        {
            Int32 Value = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT IdTechnicalDriver FROM tblTechnicalDrivers WHERE IdModel = " + IdModel + " AND TypeDriver = 2 AND UPPER(Name) = '" + _PerformanceDrivers.ToUpper() + "'").Tables[0];
            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdTechnicalDriver"];
            }
            return Value;
        }

        protected void LoadTechnicalAssumptions()
        {
            DataClass.clsTechnicalAssumptions objTechnicalAssumptions = new DataClass.clsTechnicalAssumptions();
            DataTable dt = objTechnicalAssumptions.LoadList(IdModel, 0, Convert.ToInt32(ddlFilterTypeOperation.SelectedItem.Value));
            storeTechnicalAssumption.DataSource = dt;
            storeTechnicalAssumption.DataBind();
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        public void ModelPrecalcModuleTechnical()
        {
            clsModels objModelPrecalc = new clsModels();
            objModelPrecalc.IdModel = IdModel;
            objModelPrecalc.CheckRecalcModuleParameter(objApplication.MaxRelationLevels);
            objModelPrecalc.CheckRecalcModuleAllocation();
            objModelPrecalc.PrecalcModuleTechnical();
            objModelPrecalc = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = modMain.strMasterTechnicalAssumptions;
            this.lblFilterTypeOperation.Text = (String)GetGlobalResourceObject("CPM_Resources", "Type of Operation") + ":";
            this.btnSave.Text = modMain.strCommonSaveChanges;
            this.btnSave.ToolTip = modMain.strCommonSaveChanges;
            this.DisplayName.Text = (String)GetGlobalResourceObject("CPM_Resources", "Account Code/Name");
            this.Criteria.Text = (String)GetGlobalResourceObject("CPM_Resources", "Projection Criteria");
            this.NameTechnicalSize.Text = (String)GetGlobalResourceObject("CPM_Resources", "Size Drivers");
            this.NameTechnicalPerformance.Text = (String)GetGlobalResourceObject("CPM_Resources", "Performance Drivers");
            this.IdCycleValue.Text = (String)GetGlobalResourceObject("CPM_Resources", "Cycle Term");
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        #endregion

    } 
}
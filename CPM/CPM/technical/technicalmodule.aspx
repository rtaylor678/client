﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="technicalmodule.aspx.cs" Inherits="CPM._technicalmodule" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_iconxl.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Border="false" Layout="FormLayout" OverflowY="Scroll">
        <Items>
            <ext:Panel ID="pnlTechnicalModule" runat="server" Title="Technical Module" IconCls="icon-technical_16" BodyPadding="10" Border="false" Layout="Column">
                <Items>
                    <ext:FieldSet ID="FieldSet1" runat="server" Title="1.TechnicalInputs"  Flex="1" Layout="AnchorLayout" Cls="buttongroup-bordertop" ColumnWidth="1">
                        <Defaults>
                            <ext:Parameter Name="HideEmptyLabel" Value="false" Mode="Raw" />
                        </Defaults>
                        <Items>
                            <ext:Button ID="btnListTechnicaDrivers" runat="server" Disabled="true" Cls="icon-technicaldrivers_48" Text="List of Technical Drivers" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnListTechnicaDrivers_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnTechnicalDriversData" runat="server" Disabled="true" Cls="icon-technicaldriversdata_48" Text="Technical Drivers Data" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnTechnicalDriversData_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnTechnicalAssumptions" runat="server" Disabled="true" Cls="icon-technicalassumptions_48" Text="Technical Assumptions" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnTechnicalAssumptions_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnUtilizationCapacity" runat="server" Disabled="true" Cls="icon-technicaldrivers_48" Text="Utilization Capacity" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnUtilizationCapacity_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBaseCostTechnicalModule" runat="server" Disabled="true" Cls="icon-basecostrates_48" Text="Base Rates by Cost Category" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnBaseCostTechnicalModule_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBaseRatesByDrivers" runat="server" Disabled="true" Cls="icon-basecostrates_48" Text="Base Rates By Drivers" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnBaseRatesByDrivers_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnZiffTechnicalAnalysis" runat="server" Disabled="true" Cls="icon-zifftechnicalanalysis_48" Text="Technical Files Repository" IconAlign="Top" Scale="Large" Visible="false">
                                <DirectEvents>
                                    <Click OnEvent="btnZiffTechnicalAnalysis_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:FieldSet>
                    <ext:FieldSet ID="FieldSet2" runat="server" Title="2.EconomicInputs"  Flex="1" Layout="AnchorLayout" Cls="buttongroup-bordertop" ColumnWidth="1">
                        <Defaults>
                            <ext:Parameter Name="HideEmptyLabel" Value="false" Mode="Raw" />
                        </Defaults>
                        <Items>
                            <ext:Button ID="btnCostStructureAssumptions" runat="server" Disabled="true" Cls="icon-coststructure_48" Text="Cost Structure Assumptions" IconAlign="Top" Scale="Large">
                                    <DirectEvents>
                                    <Click OnEvent="btnCostStructureAssumptions_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnPriceScenarios" runat="server" Disabled="true" Cls="icon-currency_48" Text="Price Scenarios" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnPriceScenarios_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnPrices" runat="server" Disabled="true" Cls="icon-currency_48" Text="Prices" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnPrices_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnEconomicDrivers" runat="server" Disabled="true" Cls="icon-economicdrivers_48" Text="List of Economic Drivers" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnEconomicDrivers_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnEconomicDriverData" runat="server" Disabled="true" Cls="icon-economicdriversdata_48" Text="Economic Drivers Data" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnEconomicDriverData_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnPricesEconomic" runat="server" Disabled="true" Cls="icon-economicassumptions_48" Text="Economic Assumptions" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnPricesEconomic_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnPriceForecast" runat="server" Disabled="true" Cls="icon-pricesforecast_48" Text="Base Cost Rates Forecast" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnPriceForecast_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnZiffEconomicAnalysis" runat="server" Disabled="true" Cls="icon-ziffeconomicanalysis_48" Text="Economic Files Repository" IconAlign="Top" Scale="Large" Visible="false">
                                <DirectEvents>
                                    <Click OnEvent="btnZiffEconomicAnalysis_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:FieldSet>
                    <ext:FieldSet ID="FieldSet3" runat="server" Title="3.CostProjectionReports"  Flex="1" Layout="AnchorLayout" Cls="buttongroup-bordertop" ColumnWidth="1">
                        <Defaults>
                            <ext:Parameter Name="HideEmptyLabel" Value="false" Mode="Raw" />
                        </Defaults>
                        <Items>
                            <ext:Button ID="btnCostvsDriversProjection" runat="server" Disabled="true" Cls="icon-opex_48" Text="CostVSDriversProjection" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnCostvsDriversProjection_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnOperationalMarginsProjection" runat="server" Disabled="true" Cls="icon-opex_48" Text="OperationalMarginsProjection" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnOperationalMarginsProjection_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnOpexReport" runat="server" Disabled="true" Cls="icon-opex_48" Text="Projection By Cost Category" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnOpexReport_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnOpexReportDetailed" runat="server" Disabled="true" Cls="icon-opex_48" Text="Projection By Case" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnOpexReportDetailed_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnMultiFieldsProjection" runat="server" Disabled="true" Cls="icon-opex_48" Text="Multiple Fields Projection" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnMultiFieldsProjection_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:FieldSet>
                </Items>
            </ext:Panel>  
        </Items>
    </ext:Viewport>
</body>
</html>
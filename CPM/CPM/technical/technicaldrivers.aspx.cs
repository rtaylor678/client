﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _technicaldrivers : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdTechnicalDriver { get { return (Int32)Session["IdTechnicalDriver"]; } set { Session["IdTechnicalDriver"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private DataReportObject ExportTechnicalDrivers { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                IdTechnicalDriver = 0;
                this.LoadTechnicalDrivers();
                LoadLabel();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winTechnicalDriverEdit.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.Save();
                this.LoadTechnicalDrivers();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            winTechnicalDriverEdit.Hide();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnDeleteAll_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete all records?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDeleteAllYES()",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void grdTechnicalDriver_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            IdTechnicalDriver = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            if (Command == "Edit")
            {
                this.winTechnicalDriverEdit.Show();
                this.EditTechnicalDriverLoad();
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + _Name + "?", new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedDeleteYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
        }

        protected void StoreTypeUnit_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsTypeUnit objTypeUnit = new DataClass.clsTypeUnit();
            this.storeTypeUnit.DataSource = objTypeUnit.LoadList("", "ORDER BY Name");
            this.storeTypeUnit.DataBind();
        }

        protected void StoreUnit_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsUnit objUnit = new DataClass.clsUnit();
            if (ddlTypeUnit.Value == null)
            {
                this.storeUnit.DataSource = objUnit.LoadList("Name LIKE '%" + ddlUnit.Text + "%'", "ORDER BY Name");
            }
            else
            {
                this.storeUnit.DataSource = objUnit.LoadList("IdTypeUnit = " + ddlTypeUnit.Value, "ORDER BY Name");
            }
            this.storeUnit.DataBind();
        }

        protected void StoreTypeDriver_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsParameters objParameters = new DataClass.clsParameters();
            this.storeTypeDriver.DataSource = objParameters.LoadList("Type='TypeDriver'", "ORDER BY Name");
            this.storeTypeDriver.DataBind();
        }

        protected void StoreTypeOperation_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsTypesOperation objTypesOperation = new DataClass.clsTypesOperation();
            this.storeTypeOperation.DataSource = objTypesOperation.LoadListWithGeneral("IdModel = " + IdModel + " AND LastNode = 1", "ORDER BY SortOrder, Name");
            this.storeTypeOperation.DataBind();
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.TechnicalListDriver);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport((String)row["Driver Desc"], (String)row["Unit Desc"], (String)row["Type"], String.IsNullOrEmpty(row["Type of Operation"].ToString()) ? "" : (String)row["Type of Operation"], row["Unit Cost Denominator"].ToString().ToUpper() == "YES" ? true : false, row["Baseline Allocation"].ToString().ToUpper() == "YES" ? true : false);
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadTechnicalDrivers();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void ddlTypeOperationCond_Select(object sender, DirectEventArgs e)
        {
            this.LoadTechnicalDrivers();
        }

        protected void ddlTypeDriverCond_Select(object sender, DirectEventArgs e)
        {
            this.LoadTechnicalDrivers();
        }

        protected void ddlTypeUnit_Select(object sender, DirectEventArgs e)
        {
            if (ddlTypeUnit.Value != null && ddlTypeUnit.Value.ToString() != "")
            {
                this.ddlUnit.ReadOnly = false;
            }
            else
            {
                this.ddlUnit.ReadOnly = true;
            }
            this.ddlUnit.Text = null;
            this.storeUnit.Reload();
        }

        #endregion

        #region Methods

        protected void Clear()
        {
            this.IdTechnicalDriver = 0;
            this.txtName.Text = "";
            this.txtName.Reset();
            this.ddlTypeUnit.Reset();
            this.ddlUnit.Reset();
            this.ddlUnit.ReadOnly = true;
            this.ddlTypeDriver.Reset();
            this.ddlTypeOperation.Reset();
        }

        protected void Save()
        {
            DataClass.clsTechnicalDrivers objTechnicalDrivers = new DataClass.clsTechnicalDrivers();
            objTechnicalDrivers.IdTechnicalDriver = (Int32)IdTechnicalDriver;
            objTechnicalDrivers.loadObject();

            if (IdTechnicalDriver == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (objTechnicalDrivers.Name.ToString().ToUpper() != txtName.Text.ToString().ToUpper())
                {
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }

            objTechnicalDrivers.Name = txtName.Text;
            objTechnicalDrivers.IdModel = IdModel;
            objTechnicalDrivers.IdUnit = Convert.ToInt32(ddlUnit.SelectedItem.Value);
            objTechnicalDrivers.TypeDriver = Convert.ToInt32(ddlTypeDriver.SelectedItem.Value);
            objTechnicalDrivers.IdTypeOperation = Convert.ToInt32(LoadIdOperation(this.ddlTypeOperation.SelectedItem.Value));
            if (chkUnitCostDenominator.Checked)
                objTechnicalDrivers.UnitCostDenominator = true;
            else
                objTechnicalDrivers.UnitCostDenominator = false;
            if (chkBaselineAllocation.Checked)
                objTechnicalDrivers.BaselineAllocation = true;
            else
                objTechnicalDrivers.BaselineAllocation = false;
            if (IdTechnicalDriver == 0)
            {
                objTechnicalDrivers.DateCreation = DateTime.Now;
                objTechnicalDrivers.UserCreation = (Int32)IdUserCreate;
                IdTechnicalDriver = objTechnicalDrivers.Insert();
            }
            else
            {
                objTechnicalDrivers.DateModification = DateTime.Now;
                objTechnicalDrivers.UserModification = (Int32)IdUserCreate;
                objTechnicalDrivers.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winTechnicalDriverEdit.Hide();
            this.ModelUpdateTimestamp();
        }

        protected void SaveImport(String _Name, String _Unit, String _TypeDriver, String _TypeOperation, Boolean _UnitCostDenominator, Boolean _BaselineAllocation)
        {
            Int32 _IdUnit = GetIdUnit(_Unit);
            Int32 _IdTypeDriver = GetIdType(_TypeDriver);
            Int32 _IdTypeOperation = -1;
            if (String.IsNullOrEmpty(_TypeOperation))
            {
                _IdTypeOperation = 0;
            }
            else
            {
                _IdTypeOperation = GetIdOperation(_TypeOperation);
            }

            if (!String.IsNullOrEmpty(_Name) && _IdUnit != 0 && _IdTypeDriver != -1)
            {
                DataClass.clsTechnicalDrivers objTechnicalDrivers = new DataClass.clsTechnicalDrivers();
                objTechnicalDrivers.IdTechnicalDriver = (Int32)IdTechnicalDriver;
                objTechnicalDrivers.loadObject();
                objTechnicalDrivers.Name = _Name;
                objTechnicalDrivers.IdModel = IdModel;
                objTechnicalDrivers.DateCreation = DateTime.Now;
                objTechnicalDrivers.UserCreation = (Int32)IdUserCreate;
                objTechnicalDrivers.IdUnit = _IdUnit;
                objTechnicalDrivers.IdTypeOperation = _IdTypeOperation;
                objTechnicalDrivers.TypeDriver = _IdTypeDriver;
                objTechnicalDrivers.UnitCostDenominator = _UnitCostDenominator;
                objTechnicalDrivers.BaselineAllocation = _BaselineAllocation;
                IdTechnicalDriver = objTechnicalDrivers.Insert();
                objTechnicalDrivers = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected Int32 GetIdType(String _Type)
        {
            Int32 Value = 0;
            DataClass.clsParameters objParameters = new DataClass.clsParameters();
            DataTable dt = objParameters.LoadList("Type = 'TypeDriver' AND Name = '" + _Type + "'", "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["Code"];
            }
            return Value;
        }

        protected Int32 GetIdOperation(String _TypeOperation)
        {
            Int32 Value = 0;
            DataClass.clsTypesOperation objTypesOperation = new DataClass.clsTypesOperation();
            DataTable dt = objTypesOperation.LoadList("ParentName  + ' - ' + Name = '" + _TypeOperation + "'", "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdTypeOperation"];
            }
            return Value;
        }

        protected Int32 GetIdUnit(String _Unit)
        {
            Int32 Value = 0;
            DataClass.clsUnit objUnit = new DataClass.clsUnit();
            DataTable dt = objUnit.LoadList("Name = '" + _Unit + "'", "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdUnit"];
            }
            return Value;
        }

        protected Int32 LoadIdOperation(String _TypeOperation)
        {
            Int32 IdOperation = 0;
            DataClass.clsTypesOperation objTypesOperation = new DataClass.clsTypesOperation();
            DataTable dt = objTypesOperation.LoadList("IdModel = " + IdModel + " AND LastNode = 'True' AND ParentName + ' - ' + Name LIKE '%" + _TypeOperation + "%'", "ORDER BY Name");    

            if (dt.Rows.Count>0)
            {
                IdOperation = (Int32)dt.Rows[0]["IdTypeOperation"];
            }
            return IdOperation;
        }

        protected void EditTechnicalDriverLoad()
        {
            DataClass.clsTechnicalDrivers objTechnicalDrivers= new DataClass.clsTechnicalDrivers();
            DataTable dt = objTechnicalDrivers.LoadList("IdTechnicalDriver = " + IdTechnicalDriver, "");
            if (dt.Rows.Count > 0)
            {
                this.txtName.Text = (String)dt.Rows[0]["Name"];
                this.ddlTypeUnit.SetValue((Int32)dt.Rows[0]["IdTypeUnit"]);
                this.ddlUnit.SetValue((Int32)dt.Rows[0]["IdUnit"]);
                this.ddlTypeDriver.SetValue((Int32)dt.Rows[0]["TypeDriver"]);
                this.ddlTypeOperation.SetValue((String)dt.Rows[0]["NameTypeOperation"]);
                this.chkUnitCostDenominator.Checked = Convert.ToBoolean(dt.Rows[0]["UnitCostDenominator"]);
                this.chkBaselineAllocation.Checked = Convert.ToBoolean(dt.Rows[0]["BaselineAllocation"]);
                this.storeUnit.Reload();
                this.ddlUnit.ReadOnly = false;
            }
        }

        public void Delete()
        {
            DataClass.clsTechnicalDrivers objTechnicalDrivers = new DataClass.clsTechnicalDrivers();
            objTechnicalDrivers.IdTechnicalDriver = (Int32)IdTechnicalDriver;
            objTechnicalDrivers.Delete();
            this.ModelUpdateTimestamp();
        }

        public void DeleteAll()
        {
            DataClass.clsTechnicalDrivers objTechnicalDrivers = new DataClass.clsTechnicalDrivers();
            objTechnicalDrivers.IdModel = IdModel;
            objTechnicalDrivers.DeleteAll();
            this.ModelUpdateTimestamp();
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
            this.fileImport.Reset();
        }

        [DirectMethod]
        public void ClickedDeleteYES()
        {
            this.Delete();
            this.LoadTechnicalDrivers();
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsTechnicalDrivers objTechnicalDrivers = new DataClass.clsTechnicalDrivers();
            DataTable dt = objTechnicalDrivers.LoadList("IdModel = " + IdModel  + " AND Name = '" + txtName.Text + "'", "");
            Value = dt.Rows.Count;

            return Value;
        }

        [DirectMethod]
        public void ClickedDeleteAllYES()
        {
            this.DeleteAll();
            this.LoadTechnicalDrivers();
        }

        protected void LoadTechnicalDrivers()
        {
            DataClass.clsTechnicalDrivers objTechnicalDrivers = new DataClass.clsTechnicalDrivers();
            DataTable dt = objTechnicalDrivers.LoadList("IdModel = " + IdModel + " AND IdLanguage = " + idLanguage, " ORDER BY Name");
            storeTechnicalDriver.DataSource = dt;
            storeTechnicalDriver.DataBind();

            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Name", "Driver Desc"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("NameUnit", "Unit Desc"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("NameTypeDriver", "Type"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("NameTypeOperation", "Type of Operation"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("UnitCostDenominator", "Unit Cost Denominator"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("BaselineAllocation", "Baseline Allocation"));
            String strSelectedParameters = "";
            String[] columnsToCopy = { "Name", "NameUnit", "NameTypeDriver", "NameTypeOperation", "UnitCostDenominator" , "BaselineAllocation"};
            System.Data.DataView view = new System.Data.DataView(dt);
            DataTable dtTechDrivers = view.ToTable(true, columnsToCopy);

            //Need to modify NameTypeOperation data where it equal -All- make it All was blank ORIGINALLY
            foreach (DataRow row in dtTechDrivers.Rows)
            {
                string oldX = row.Field<String>("NameTypeOperation");
                string newX = "- All -".Equals(oldX, StringComparison.OrdinalIgnoreCase) ? "All" : row["NameTypeOperation"].ToString();
                row.SetField("NameTypeOperation", newX);
            }

            ExportTechnicalDrivers = new DataReportObject(dtTechDrivers, "DATASHEET", null, strSelectedParameters, null, "Name", datColumnTD, null);
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportTechnicalDrivers;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=3.1 ExportListOfTechnicalDrivers.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportListOfTechnicalDrivers.xls", "template");
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = modMain.strMasterListTechnicalDrivers;
            this.btnAdd.Text = (String)GetGlobalResourceObject("CPM_Resources", "Add New Technical Driver");
            this.btnAdd.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Add New Technical Driver");
            this.btnDelete.Text = modMain.strCommonDeleteAll;
            this.btnDelete.ToolTip = modMain.strCommonDeleteAll;
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.Name.Text = (String)GetGlobalResourceObject("CPM_Resources", "Name");
            this.UnitType.Text = (String)GetGlobalResourceObject("CPM_Resources", "Unit Type");
            this.Unit.Text = (String)GetGlobalResourceObject("CPM_Resources", "Unit");
            this.TypeDriver.Text = (String)GetGlobalResourceObject("CPM_Resources", "Type of Driver");
            this.TypeOperation.Text = (String)GetGlobalResourceObject("CPM_Resources", "Type of Operation");
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = "&nbsp;" + modMain.strCommonEdit;
            this.ImageCommandColumn1.Commands[1].Text = "&nbsp;" + modMain.strCommonDelete;
            this.winTechnicalDriverEdit.Title= (String)GetGlobalResourceObject("CPM_Resources", "Technical Driver");
            this.txtName.FieldLabel = modMain.strCommonName;
            this.ddlTypeUnit.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Unit Type");
            this.ddlUnit.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Unit");
            this.ddlTypeDriver.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Type of Driver");
            this.chkUnitCostDenominator.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Unit Cost Denominator");
            this.chkBaselineAllocation.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "BaselineAllocation");
            this.ddlTypeOperation.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Operation");
            this.btnSave.Text = modMain.strCommonSave;
            this.btnSave.ToolTip = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.btnCancel.ToolTip = modMain.strCommonClose;
            this.winImport.Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Technical Drivers");
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.UnitCostDenominator.Text = (String)GetGlobalResourceObject("CPM_Resources", "Unit Cost Denominator");
            this.BaselineAllocation.Text = (String)GetGlobalResourceObject("CPM_Resources", "BaselineAllocation");
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        #endregion

    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="technicaldrivers.aspx.cs" Inherits="CPM._technicaldrivers" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function applyPageSize(grid) {
            var headerHeight = 22;
            var rowHeight = 21;

            var gridTopOffset = 100;
            var gridBottomOffset = 50;
            var rowHeight = 20;
            var myHeight = window.innerHeight;

            grid.store.pageSize = Math.round(((myHeight - gridTopOffset - gridBottomOffset) / rowHeight)); // - 3);
            grid.store.load();
        }
    </script>
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" AutoScroll="true" runat="server" Title="List of Technical Drivers" IconCls="icon-technicaldrivers_16" Border="false">
                <Items>
                    <ext:GridPanel ID="grdTechnicalDriver" runat="server" AutoScroll="true" Border="false" Header="false">
                        <TopBar>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btnAdd" Icon="Add" ToolTip="Add New Technical Driver" runat="server" Text="Add New Technical Driver">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarSeparator />
                                    <ext:Button ID="btnDelete" Icon="Delete" ToolTip="Delete All" runat="server" Text="Delete All">
                                        <DirectEvents>
                                            <Click OnEvent="btnDeleteAll_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarSeparator />
                                    <ext:Button ID="btnImport" Icon="BookAdd" runat="server" Text="Import">
                                        <DirectEvents>
                                            <Click OnEvent="btnImport_Click">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarFill />
                                    <ext:Button ID="btnSaveExcel" runat="server" Text="Export Data To Excel" Icon="PageExcel">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveExcel_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnDownloadTemplate" runat="server" Text="Download Import Template" Icon="PageSave">
                                        <DirectEvents>
                                            <Click OnEvent="btnDownloadTemplate_Click" IsUpload="true" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="storeTechnicalDriver" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="IdTechnicalDriver">
                                        <Fields>
                                            <ext:ModelField Name="IdTechnicalDriver" Type="Int" />
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="NameTypeUnit" Type="String" />
                                            <ext:ModelField Name="NameUnit" Type="String" />
                                            <ext:ModelField Name="NameTypeDriver" Type="String" />
                                            <ext:ModelField Name="NameTypeOperation" Type="String" />
                                            <ext:ModelField Name="UnitCostDenominator" Type="Boolean" />
                                            <ext:ModelField Name="BaselineAllocation" Type="Boolean" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel ID="ColumnModel1" runat="server">
                            <Columns>
                                <ext:Column ID="colID" Hidden="true" runat="server" Text="ID" DataIndex="IdTechnicalDriver" />
                                <ext:Column ID="Name" runat="server" Text="Name" DataIndex="Name" Flex="1" />
                                <ext:Column ID="UnitType" runat="server" Text="Unit Type" DataIndex="NameTypeUnit" Flex="1" />
                                <ext:Column ID="Unit" runat="server" Text="Unit" DataIndex="NameUnit" Flex="1" />
                                <ext:Column ID="TypeDriver" runat="server" Text="Type of Driver" DataIndex="NameTypeDriver" Flex="1" />
                                <ext:ComponentColumn ID="UnitCostDenominator" runat="server" Editor="true" Align="Center" DataIndex="UnitCostDenominator" Flex="1" Text="Unit Cost Denominator">
                                    <Component>
                                        <ext:Checkbox ID="Checkbox1" Checked="true" runat="server" ReadOnly="True" />
                                    </Component>
                                </ext:ComponentColumn>
                                <ext:ComponentColumn ID="BaselineAllocation" runat="server" Editor="true" Align="Center" DataIndex="BaselineAllocation" Flex="1" Text="Baseline Allocation">
                                    <Component>
                                        <ext:Checkbox ID="Checkbox2" Checked="true" runat="server" ReadOnly="True" />
                                    </Component>
                                </ext:ComponentColumn>
                                <ext:Column ID="TypeOperation" runat="server" Text="Type of Operation" DataIndex="NameTypeOperation" Flex="1" />
                                <ext:ImageCommandColumn ID="ImageCommandColumn1" Align="Center" Width="150" Text="Functions" runat="server">
                                    <Commands>
                                        <ext:ImageCommand CommandName="Edit" IconCls="icon-edit_16" Text="&nbsp;Edit" />
                                    </Commands>
                                    <Commands>
                                        <ext:ImageCommand CommandName="Delete" IconCls="icon-delete_16" Text="&nbsp;Delete" />
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="grdTechnicalDriver_Command">
                                            <ExtraParams>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="Id" Value="record.data.IdTechnicalDriver" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="Name" Value="record.data.Name" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:ImageCommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" />
                        </SelectionModel>
                        <Features>
                            <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                                <Filters>
                                    <ext:StringFilter DataIndex="Name" />
                                    <ext:StringFilter DataIndex="NameTypeUnit" />
                                    <ext:StringFilter DataIndex="NameUnit" />
                                    <ext:StringFilter DataIndex="NameTypeDriver" />
                                    <ext:StringFilter DataIndex="NameTypeOperation" />
                                </Filters>
                            </ext:GridFilters>
                        </Features>
                    </ext:GridPanel>
                    <ext:Window ID="winTechnicalDriverEdit" runat="server" Title="Technical Driver" Hidden="true" IconCls="icon-technicaldrivers_16" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="pnlEditTechnicalDriver" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                                <Items>
                                    <ext:Panel ID="Panel3" runat="server" Border="false" Layout="Form" ColumnWidth="1" LabelAlign="Top" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:TextField ID="txtName" runat="server" FieldLabel="Name" AnchorHorizontal="92%" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel2" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" LabelAlign="Top" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:ComboBox ID="ddlTypeUnit" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" MinChars="0" FieldLabel="Unit Type" ValueField="IdTypeUnit" Cls="PopupFormField">
                                                <Store>
                                                    <ext:Store ID="storeTypeUnit" runat="server" OnReadData="StoreTypeUnit_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model2" runat="server" IDProperty="IdTypeUnit">
                                                                <Fields>
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="IdTypeUnit" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                                <DirectEvents>
                                                    <Select OnEvent="ddlTypeUnit_Select" />
                                                </DirectEvents>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel5" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" LabelAlign="Top" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:ComboBox ID="ddlUnit" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" MinChars="0" FieldLabel="Unit" ValueField="IdUnit" Cls="PopupFormField" ReadOnly="true">
                                                <Store>
                                                    <ext:Store ID="storeUnit" runat="server" OnReadData="StoreUnit_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model3" runat="server" IDProperty="IdUnit">
                                                                <Fields>
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="IdUnit" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel6" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" LabelAlign="Top" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:ComboBox ID="ddlTypeDriver" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" MinChars="0" FieldLabel="Type of Driver" ValueField="Code" Cls="PopupFormField">
                                                <Store>
                                                    <ext:Store ID="storeTypeDriver" runat="server" OnReadData="StoreTypeDriver_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model4" runat="server" IDProperty="Code">
                                                                <Fields>
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="Code" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel  ID="Panel1" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel" ColSpan=2>
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:Checkbox ID="chkUnitCostDenominator" runat="server" FieldLabel="Unit Cost Denominator" Cls="PopupFormField" ReadOnly="False">
                                            </ext:Checkbox>
                                        </Items>

                                    </ext:Panel>
                                    <ext:Panel  ID="Panel7" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel" ColSpan=2>
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:Checkbox ID="chkBaselineAllocation" runat="server" FieldLabel="Baseline Allocation" Cls="PopupFormField" ReadOnly="False">
                                            </ext:Checkbox>
                                        </Items>

                                    </ext:Panel>
                                    <ext:Panel ID="Panel4" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" LabelAlign="Top" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:ComboBox ID="ddlTypeOperation" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" MinChars="0" FieldLabel="Operation" ValueField="Name" Cls="PopupFormField">
                                                <ListConfig LoadingText="Searching...">
                                                    <ItemTpl ID="ItemTpl1" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3><span>Type: {ShoreDesc} | {TypeDesc}</span>{Name}</h3>
							                                    <span>Parent: </span>{ParentName}</h3>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeTypeOperation" runat="server" OnReadData="StoreTypeOperation_ReadData">
                                                        <Model>
                                                            <ext:Model ID="mdlLabels" runat="server" IDProperty="IdTypeOperation">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdTypeOperation" />
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="ParentName" />
                                                                    <ext:ModelField Name="ShoreDesc" />
                                                                    <ext:ModelField Name="TypeDesc" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnSave" runat="server" Text="Save" Icon="DatabaseSave" Disabled="true" FormBind="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnSave_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnCancel" runat="server" Icon="Decline" Text="Close">
                                        <DirectEvents>
                                            <Click OnEvent="btnCancel_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                                <BottomBar>
                                    <ext:StatusBar ID="FormStatusBar" runat="server" />
                                </BottomBar>
                                <Listeners>
                                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                                    text : valid ? 'Form is valid' : 'Form is invalid', 
                                                                    iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                                });
                                                                #{btnSave}.setDisabled(!valid);" />
                                </Listeners>
                            </ext:FormPanel>
                        </Items>
                    </ext:Window>
                    <ext:Window ID="winImport" runat="server" Title="Import Technical Drivers" Hidden="true" Icon="ArrowLeft" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="frmImportFile" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                                <Items>
                                    <ext:Panel ID="pnlImportFile1" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:FileUploadField ID="fileImport" FieldLabel="File" runat="server" Icon="Attach" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnImportFile" runat="server" Text="Import" Icon="TableRefresh" Disabled="true" FormBind="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnImportFile_Click">
                                                <EventMask ShowMask="true" Target="CustomTarget" CustomTarget="vpMain" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnCancelImport" runat="server" Icon="Decline" Text="Close">
                                        <DirectEvents>
                                            <Click OnEvent="btnCancelImport_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                                <BottomBar>
                                    <ext:StatusBar ID="FormImportStatusBar" runat="server" />
                                </BottomBar>
                                <Listeners>
                                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                            });
                                            #{btnImportFile}.setDisabled(!valid);" />
                                </Listeners>
                            </ext:FormPanel>
                        </Items>
                    </ext:Window>
                </Items>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>
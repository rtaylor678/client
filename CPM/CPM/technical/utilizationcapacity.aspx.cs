﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _utilizationcapacity : System.Web.UI.Page
    {
        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }
        private Boolean PageIsLoaded { get; set; }
        private DataReportObject ExportUtilizationCapacity { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }
        public DataTable datViewData { get; set; }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                this.LoadLabel();
                Session["Export01"] = null;
                PageIsLoaded = false;
                //We need to fire the event of the dummy button
                btnDummy.FireEvent("click");
                DataTable dtDCC = new DataTable();
                ArrayList datColumnOpex = new ArrayList();
                this.FillGrid(this.storeUtilizationCapacity, this.grdUtilizationCapacity, dtDCC, ref datColumnOpex);
                PageIsLoaded = true;
            }
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                String strData = e.ExtraParams["rowsValues"];
                datViewData = JSON.Deserialize<DataTable>(strData);
                this.Save();
                this.LoadData();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        /// <summary>
        /// This is a dummy button click event to have the mask execute when the page loads for the first time
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDummy_Click(object sender, DirectEventArgs e)
        {
            LoadData();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            try
            {
                String strReturnMessage = null;
                if (this.fileImport.HasFile)
                {
                    DataFileReader objReader = null;
                    DataFileType objType = new DataFileType();
                    objType.CheckFileType(fileImport.PostedFile.FileName);

                    if (!objType.IsValidType)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }

                    if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                    {
                        objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.UtilizationCapacity);
                        objReader.GetDataTable(ref strReturnMessage);
                    }
                    else
                    {
                        strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                    }

                    if (String.IsNullOrEmpty(strReturnMessage))
                    {
                        intFailedImportRowCount = 0;
                        strImportErrorMessage = "";
                        intCurrentImportRow = 0;
                        foreach (DataRow row in objReader.ImportTable.Rows)
                        {
                            intCurrentImportRow++;
                            this.SaveImport(row["Field"].ToString(), row["Cost Category"].ToString(), Convert.ToInt32(row["Value"].ToString()));
                        }
                        if (intFailedImportRowCount != 0)
                        {
                            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                        }
                        else
                        {
                            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                        }
                        this.winImport.Hide();
                        this.ModelUpdateTimestamp();
                        //Trim Error if too long
                        if (strImportErrorMessage.Length > 1024)
                        {
                            strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                        }
                        X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                    }

                    this.LoadData();
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
                }
            }
            catch
            {
            }
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        protected void Save()
        {
            foreach (DataRow row in datViewData.Rows)
            {
                if (row[0].ToString().IndexOf("    - ") != -1) //Skip rows that are just headers
                {
                    //go through every column starting at column 2 until there are no more columns, first column is now SortOrder
                    for (int w = 3; w < this.datViewData.Columns.Count; w++)
                    {
                        DataClass.clsUtilizationCapacity objUtilizationCapacity = new clsUtilizationCapacity();
                        objUtilizationCapacity.IdModel = IdModel;
                        objUtilizationCapacity.IdField = objUtilizationCapacity.GetIdField((String)this.datViewData.Columns[w].ColumnName, IdModel);
                        objUtilizationCapacity.CodeZiff = row[0].ToString().Substring(0, row[0].ToString().IndexOf(':')).Remove(0, row[0].ToString().IndexOf('-') + 2); //row[0].ToString().Substring(0, row[0].ToString().IndexOf(':'));
                        objUtilizationCapacity.IdStage = 0;
                        objUtilizationCapacity.BaseCostType = 0;
                        objUtilizationCapacity.IdUtilization = objUtilizationCapacity.GetIdUtilization(IdModel, objUtilizationCapacity.CodeZiff, objUtilizationCapacity.IdField, 0, 0);
                        objUtilizationCapacity.UtilizationValue = Convert.ToInt32(row[w]);
                        if (objUtilizationCapacity.UtilizationValue != 100 || (objUtilizationCapacity.UtilizationValue == 100 && objUtilizationCapacity.CurrentValue != 100))
                        {
                            objUtilizationCapacity.DateModification = DateTime.Now;
                            objUtilizationCapacity.UserModification = (Int32)IdUserCreate;
                            objUtilizationCapacity.Update();
                        }
                    }
                }
            }
            ModelUpdateTimestamp();
        }

        protected void SaveImport(String _Field, String _CostCategory, Int32 _Value)
        {
            try
            {
                Int32 _IdField = 0;
                DataClass.clsFields objFields = new clsFields();
                DataTable dt = objFields.LoadList("IdModel = " + IdModel + " AND Name = '" + _Field + "'", "");
                if (dt.Rows.Count > 0)
                {
                    _IdField = (Int32)dt.Rows[0]["IdField"];
                }

                Int32 _IdZiffAccount = GetIdZiffAccount(_CostCategory);


                if (_IdZiffAccount != 0 && _IdField != 0)
                {
                    DataClass.clsUtilizationCapacity objUtilizationCapacity = new clsUtilizationCapacity();
                    objUtilizationCapacity.IdModel = IdModel;
                    objUtilizationCapacity.IdField = objUtilizationCapacity.GetIdField(_Field, IdModel);
                    objUtilizationCapacity.IdStage = 0;
                    objUtilizationCapacity.BaseCostType = 0;
                    objUtilizationCapacity.IdUtilization = objUtilizationCapacity.GetIdUtilization(IdModel, objUtilizationCapacity.IdField, _IdZiffAccount);
                    objUtilizationCapacity.UtilizationValue = _Value;
                    if (objUtilizationCapacity.UtilizationValue != 100 || (objUtilizationCapacity.UtilizationValue == 100 && objUtilizationCapacity.CurrentValue != 100))
                    {
                        objUtilizationCapacity.DateModification = DateTime.Now;
                        objUtilizationCapacity.UserModification = (Int32)IdUserCreate;
                        objUtilizationCapacity.Update();
                    }
                }
                else
                {
                    intFailedImportRowCount++;
                    strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
                }

                //ModelUpdateTimestamp();
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
            }
            catch
            {
            }
        }

        protected void LoadData()
        {
            try
            {
                this.ModelCheckRecalcModuleTechnical();
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Loading data..."), IconCls = "icon-accept", Clear2 = false });
                DataClass.clsUtilizationCapacity objUtilizationCapacity = new DataClass.clsUtilizationCapacity();

                Int32 intBaseCostType = 1;
                Int32 intStage = 1;

                DataSet ds = objUtilizationCapacity.LoadUtilizationCapacity(IdModel, intStage, intBaseCostType);

                DataTable dtDCC = new DataTable();
                dtDCC = ds.Tables[0];

                ArrayList datColumnPC = new ArrayList();
                ArrayList datColumnOpex = new ArrayList();
                datColumnPC.Add(new DataClass.DataTableColumnDisplay("CodeZiffExport", "Cost Category"));

                if (ds.Tables[1].Rows[0].Field<Int32>(0) != 0) //dtDCC.Rows.Count != 0)
                {
                    this.FillGrid(this.storeUtilizationCapacity, this.grdUtilizationCapacity, dtDCC, ref datColumnOpex);
                }
                else
                {
                    //Need to create the rows with default 100 as Capacity value
                    objUtilizationCapacity.IdModel = IdModel;
                    objUtilizationCapacity.DateCreation = DateTime.Now;
                    objUtilizationCapacity.UserCreation = (Int32)IdUserCreate;
                    objUtilizationCapacity.Insert();
                }

                //Clean up output data for export, as we don't want to show -1 in export
                DataTable dtExport = dtDCC.Clone();
                for (int i = 3; i < dtExport.Columns.Count; i++)
                {
                    dtExport.Columns[i].DataType = typeof(String);
                }
                foreach (DataRow row in dtDCC.Rows)
                {
                    dtExport.ImportRow(row);
                }
                int numColumns = dtExport.Columns.Count;
                for (int j = 0; j < dtDCC.Rows.Count; j++)
                {
                    DataRow dr = dtExport.Rows[j];
                    for (int k = 3; k < numColumns; k++)
                    {
                        if (Convert.ToInt32(dr[k]) == -1)
                        {
                            dr[k] = "";
                        }
                    }
                }

                ArrayList datColumnExclude = new ArrayList();
                datColumnExclude.Add("CodeZiff");
                datColumnExclude.Add("SortOrder");

                String strSelectedParameters = "";
                ExportUtilizationCapacity = new DataReportObject(dtExport, "Utilization Capacity By Field", "Utilization Capacity By Field", strSelectedParameters, datColumnExclude, "SortOrder", datColumnPC, null);
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Data loaded."), IconCls = "icon-accept", Clear2 = false });
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                if (strMessage.Contains("Timeout"))
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = strMessage.ToString(), IconCls = "icon-exclamation", Clear2 = false });
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select all filters please"), IconCls = "icon-exclamation", Clear2 = false });
                }
            }
        }

        private void FillGrid(Ext.Net.Store s, GridPanel g, DataTable dt, ref ArrayList colVisible)
        {
            try
            {
                s.RemoveFields();

                foreach (DataColumn c in dt.Columns)
                {
                    if (c.ColumnName == "CodeZiff")
                    {
                        if (!c.ColumnName.Contains("Id"))
                            s.AddField(new ModelField(c.ColumnName, ModelFieldType.Auto));
                    }
                    else
                    {
                        if (!c.ColumnName.Contains("Id"))
                            s.AddField(new ModelField(c.ColumnName, ModelFieldType.Float));
                    }
                }

                g.Features.Clear();

                GroupingSummary gsum = new GroupingSummary();
                gsum.GroupHeaderTplString = "{name}";
                gsum.HideGroupedHeader = true;
                gsum.ShowSummaryRow = false;    //Hide the grouping Summary Totals by setting to false
 
                g.Features.Add(gsum);

                GridFilters f = new GridFilters();
                f.Local = true;
                g.Features.Add(f);

                Summary smry = new Summary();
                smry.Dock = new SummaryDock();
                smry.ID = "Summary" + g.ID.ToString();
                smry.ShowSummaryRow = false;    //Hide the Grand Summary Totals by setting to false

                g.Features.Add(smry);

                foreach (DataColumn c in dt.Columns)
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        if (c.ColumnName == "CodeZiff")
                        {
                            StringFilter sFilter = new StringFilter();
                            sFilter.AutoDataBind = true;
                            sFilter.DataIndex = c.ColumnName;
                            f.Filters.Add(sFilter);
                        }
                        else
                        {
                            NumericFilter sFilter = new NumericFilter();
                            sFilter.AutoDataBind = true;
                            sFilter.DataIndex = c.ColumnName;
                            f.Filters.Add(sFilter);
                        }
                    }
                }

                Column col = new Column();
                col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Fields") + " (%)";

                SummaryColumn colSum = new SummaryColumn();
                foreach (DataColumn c in dt.Columns)
                {
                    if (c.ColumnName == "CodeZiff")
                    {
                        if (!c.ColumnName.Contains("Id"))
                        {
                            if (c.ColumnName != "Activity")
                            {
                                colSum = new SummaryColumn();
                                colSum.DataIndex = c.ColumnName;
                                switch (g.ID)
                                {
                                    case "grdUtilizationCapacity":
                                        colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", "Cost Category");
                                        break;
                                }
                                colSum.Locked = true;
                                colVisible.Add(new DataClass.DataTableColumnDisplay(c.ColumnName, colSum.Text));
                                colSum.SummaryType = Ext.Net.SummaryType.Count;
                                colSum.TdCls = "task";
                                colSum.Sortable = true;
                                colSum.Groupable = true;
                                colSum.Width = 250;
                                colSum.SummaryRenderer.Handler = "return '(' + value + ' Total)';";
                                colSum.MenuDisabled = false;
                                colSum.Align = Alignment.Left;
                                g.ColumnModel.Columns.Add(colSum);
                            }
                        }
                    }
                    else
                    {
                        if (!c.ColumnName.Contains("Id") && !c.ColumnName.Contains("SortOrder") && !c.ColumnName.Contains("CodeZiffExport"))
                        {
                            colSum = new SummaryColumn();
                            colSum.DataIndex = c.ColumnName;
                            colSum.Text = c.ColumnName;
                            colVisible.Add(new DataClass.DataTableColumnDisplay(c.ColumnName, colSum.Text));
                            colSum.SummaryType = Ext.Net.SummaryType.Sum;
                            Renderer sumRen = new Renderer();
                            sumRen.Fn = "formatZero";
                            colSum.SummaryRenderer = sumRen;
                            colSum.Renderer = sumRen;
                            colSum.Align = Alignment.Right;
                            colSum.MinWidth = 80;
                            col.Columns.Add(colSum);
                        }
                    }

                }

                g.ColumnModel.Columns.Add(col);
                //Add the editor to each column
                for (int x = 0; x < g.ColumnModel.Columns[1].Columns.Count; x++)
                {
                    g.ColumnModel.Columns[1].Columns[x].Editor.Add(new Ext.Net.NumberField());
                }
                s.DataSource = dt;
                if (X.IsAjaxRequest)
                {
                    g.Reconfigure();
                }
                s.DataBind();
            }
            catch(Exception ex)
            {
            }
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
            this.fileImport.Reset();
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExportUC = new DataReportObject();
            StoredExportUC = ExportUtilizationCapacity;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExportUC);
            objWriter.BuildExportFile(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2007);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=UtilizationCapacityByField.xlsx");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportUtilizationCapacity.xls", "template");
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        public void ModelCheckRecalcModuleTechnical()
        {
            clsModels objModelRecalc = new clsModels();
            objModelRecalc.IdModel = IdModel;
            objModelRecalc.CheckRecalcModuleParameter(objApplication.MaxRelationLevels);
            objModelRecalc.CheckRecalcModuleTechnical();
            objModelRecalc = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = (String)GetGlobalResourceObject("CPM_Resources", "Utilization Capacity");
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSave.Text = modMain.strCommonSaveChanges;
            this.btnSave.ToolTip = modMain.strCommonSaveChanges;
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
        }

        protected Int32 GetIdZiffAccount(String _CostCategory)
        {
            Int32 Value = 0;
            DataTable dt = CD.DAC.ConsultSQL("SELECT IdZiffAccount FROM tblZiffAccounts WHERE IdModel = " + IdModel + " AND UPPER(Code) = '" + _CostCategory.ToUpper() + "'").Tables[0];
            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdZiffAccount"];
            }
            return Value;
        }

        #endregion

    }
}
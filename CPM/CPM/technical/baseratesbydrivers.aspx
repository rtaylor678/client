﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="baseratesbydrivers.aspx.cs" Inherits="CPM._baseratesbydrivers" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        Ext.num = function (v, defaultValue) {
            v = Number(Ext.isEmpty(v) || Ext.isArray(v) || typeof v == 'boolean' || (typeof v == 'string' && v.trim().length == 0) ? NaN : v);
            return isNaN(v) ? defaultValue : v;
        }

        Ext.util.Format.number = function (v, format) {
            if (!format) {
                return v;
            }
            v = Ext.num(v, NaN);
            if (isNaN(v)) {
                return '';
            }
            var comma = ',',
                dec = '.',
                i18n = false,
                neg = v < 0;

            v = Math.abs(v);
            if (format.substr(format.length - 2) == '/i') {
                format = format.substr(0, format.length - 2);
                i18n = true;
                comma = '.';
                dec = ',';
            }

            var hasComma = format.indexOf(comma) != -1,
                psplit = (i18n ? format.replace(/[^\d\,]/g, '') : format.replace(/[^\d\.]/g, '')).split(dec);

            if (1 < psplit.length) {
                v = v.toFixed(psplit[1].length);
            } else if (2 < psplit.length) {
                throw ('NumberFormatException: invalid format, formats should have no more than 1 period: ' + format);
            } else {
                v = v.toFixed(0);
            }

            var fnum = v.toString();

            psplit = fnum.split('.');

            if (hasComma) {
                var cnum = psplit[0],
                    parr = [],
                    j = cnum.length,
                    m = Math.floor(j / 3),
                    n = cnum.length % 3 || 3,
                    i;

                for (i = 0; i < j; i += n) {
                    if (i != 0) {
                        n = 3;
                    }

                    parr[parr.length] = cnum.substr(i, n);
                    m -= 1;
                }
                fnum = parr.join(comma);
                if (psplit[1]) {
                    fnum += dec + psplit[1];
                }
            } else {
                if (psplit[1]) {
                    fnum = psplit[0] + dec + psplit[1];
                }
            }

            return (neg ? '-' : '') + format.replace(/[\d,?\.?]+/, fnum);
        }
    </script>
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" Layout="FitLayout" Title="Base Rates by Drivers" IconCls="icon-basecostrates_16" AutoScroll="true" runat="server" Border="false">
                <Items>
                    <ext:Panel ID="Panel3" runat="server" AutoScroll="true">
                        <Items>
                            <ext:Panel ID="pnlExportToolbar" runat="server" Border="false" Hidden="true">
                                <TopBar>
                                    <ext:Toolbar ID="tbrExport" Border="false" Height="30" runat="server">
                                        <Items>
                                            <ext:Button ID="btnReset" runat="server" Text="Reset" Icon="Reload">
                                                <DirectEvents>
                                                    <Click OnEvent="btnReset_Click">
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                            <ext:ToolbarFill />
                                            <ext:Button ID="btnSaveExcel" runat="server" Text="Export Data To Excel" Icon="PageExcel">
                                                <DirectEvents>
                                                    <Click OnEvent="btnSaveExcel_Click" IsUpload="true">
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                            </ext:Panel>
                            <ext:Panel ID="pnlButtons01" runat="server" Layout="Column" Border="false" Style="margin: 0 0 0 0">
                                <Items>
                                    <ext:Panel ID="Panel100" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                                        <Items>
                                            <ext:Label Style="margin: 10px 0 10px 10px" ID="lblLevels" runat="server" Text="Aggregation Level:"></ext:Label>
                                            <ext:ComboBox Style="margin: 10px 0 10px 10px" ID="ddlLevels" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" ValueField="IdAggregationLevel" Width="300">
                                                <ListConfig LoadingText="Searching..." MinWidth="350">
                                                    <ItemTpl ID="ItemTpl2" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
							                                    <span>Parent: {ParentName}</span>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeLevels" runat="server" OnReadData="StoreLevel_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model2" runat="server" IDProperty="IdAggregationLevel">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdAggregationLevel" />
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="ParentName" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                                <DirectEvents>
                                                    <Select OnEvent="ddlLevels_Select" />
                                                </DirectEvents>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel101" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                                        <Items>
                                            <ext:Label Style="margin: 10px 0 10px 10px" ID="lblField" runat="server" Text="Field:"></ext:Label>
                                            <ext:ComboBox Style="margin: 10px 0 10px 10px" ID="ddlField" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" ValueField="IdField" Width="200">
                                                <ListConfig LoadingText="Searching..." MinWidth="300">
                                                    <ItemTpl ID="ItemTpl1" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
							                                    <span>Code: {Code}</span>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeField" runat="server" OnReadData="StoreField_ReadData">
                                                        <Model>
                                                            <ext:Model ID="mdlLabels" runat="server" IDProperty="IdField">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdField" />
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="Code" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel102" runat="server" Layout="HBoxLayut" Border="false" Style="margin: 0 0 0 0" visible="false">
                                        <Items>
                                            <ext:Label Style="margin: 10px 0 10px 10px" ID="lblOperationType" runat="server" Text="Operation Type:"></ext:Label>
                                            <ext:ComboBox Style="margin: 10px 0 10px 10px" ID="ddlOperationType" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" ValueField="IdTypeOperation">
                                                <ListConfig LoadingText="Searching..." MinWidth="250">
                                                    <ItemTpl ID="ItemTpl5" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
							                                    <span>Code: {IdTypeOperation}</span>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeOperationType" runat="server" OnReadData="StoreOperationType_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model6" runat="server" IDProperty="IdTypeOperation">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdTypeOperation" />
                                                                    <ext:ModelField Name="Name" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel103" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                                        <Items>
                                            <ext:Label Style="margin: 10px 0 10px 10px" ID="lblCurrency" runat="server" Text="Currency:"></ext:Label>
                                            <ext:ComboBox Style="margin: 10px 0 10px 10px" ID="ddlCurrency" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" ValueField="IdCurrency" Width="150">
                                                <ListConfig LoadingText="Searching..." MinWidth="250">
                                                    <ItemTpl ID="ItemTpl4" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
							                                    <span>Code: {Code}</span>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeBase" runat="server" OnReadData="StoreCurr_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model4" runat="server" IDProperty="IdCurrency">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdCurrency" />
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="Code" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                               </Items>
                            </ext:Panel>
                            <%-- Line between lists --%>
                            <ext:Panel ID="Panel2" runat="server" Border="false"></ext:Panel>
                            <ext:Panel ID="pnlButtons02" runat="server" Layout="Column" Border="true" Style="margin: 0 0 0 0">
                                <Items>
                                    <ext:Panel ID="Panel200" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                                        <Items>
                                            <ext:Label Style="margin: 10px 0 10px 10px" ID="lblFilterBaseCost" runat="server" Text="Base Cost Reference:"></ext:Label>
                                            <ext:ComboBox Style="margin: 10px 0 10px 10px" ID="ddlFilterBaseCost" MinChars="0" runat="server" Width="200" DisplayField="Name" ValueField="Code">
                                                <ListConfig LoadingText="Searching..." MinWidth="300">
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeBaseCostCond" runat="server" OnReadData="StoreFilterBaseCost_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model5" runat="server" IDProperty="Code">
                                                                <Fields>
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="Code" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                           </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel201" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                                        <Items>
                                            <ext:Label Style="margin: 10px 0 10px 10px" ID="lblFilterStage" runat="server" Text="Technical Scenario:"></ext:Label>
                                            <ext:ComboBox Style="margin: 10px 0 10px 10px" MinChars="0" ID="ddlFilterStage" runat="server" Width="200" DisplayField="Name" ValueField="ID">
                                                <ListConfig LoadingText="Searching..." MinWidth="300">
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeFilterStage" runat="server" OnReadData="StoreFilterStage_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model7" runat="server" IDProperty="ID">
                                                                <Fields>
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="ID" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="pnlReportToolbar" runat="server" Border="false" Hidden="false">
                                <TopBar>
                                    <ext:Toolbar ID="tbrReport" Border="false" Height="30" runat="server">
                                        <Items>
                                            <ext:Button ID="btnRun" runat="server" Text="Execute Base Rates by Drivers" Icon="PlayGreen">
                                                <DirectEvents>
                                                    <Click Timeout="3600000" ShowWarningOnFailure="false" OnEvent="btnRun_Click">
                                                        <EventMask ShowMask="true">
                                                        </EventMask>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                            <ext:ToolbarFill />
                                            <ext:Button ID="btnDirectExcel" runat="server" Text="Export Data To Excel" Icon="PageExcel">
                                                <DirectEvents>
                                                    <Click Timeout="3600000" OnEvent="btnSaveExcelDirect_Click" IsUpload="true">
                                                        <EventMask ShowMask="true" />
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                            </ext:Panel>
                            <ext:Panel ID="ReportTitle01" runat="server" Border="false" Title="" Cls="reportheader"></ext:Panel>
                            <ext:Panel ID="pnlCostPerDrivers" Border="false" runat="server">
                                <Items>
                                    <ext:GridPanel ID="grdCostPerDrivers" runat="server" Border="false" Header="false" EnableLocking="true" Title="Projection" Icon="CogGo">
                                        <Store>
                                            <ext:Store ID="storeCostPerDrivers" runat="server">
                                                <Model>
                                                    <ext:Model ID="modelCostPerDrivers" runat="server">
                                                        <Fields>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel RenderColumnsOnly="False" RenderXType="True" IDMode="Explicit" Namespace="App" IsDynamic="False">
                                            <Columns>
                                            </Columns>
                                        </ColumnModel>
                                        <Listeners>
                                            <Reconfigure Handler="this.setHeight(this.container.getHeight()+20);" Delay="100" />
                                        </Listeners>
                                    </ext:GridPanel>
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="pnlProjectionCosts" runat="server" Border="false">
                                <Items>
                                    <ext:GridPanel ID="grdProjectionCosts" runat="server" Border="false" Header="false" EnableLocking="true">
                                        <Store>
                                            <ext:Store ID="storeProjectionCosts" runat="server">
                                                <Model>
                                                    <ext:Model ID="modelProjectionCosts" runat="server">
                                                        <Fields>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel RenderColumnsOnly="False" RenderXType="True" IDMode="Explicit" Namespace="App" IsDynamic="False">
                                            <Columns>
                                            </Columns>
                                        </ColumnModel>
                                        <Listeners>
                                            <Reconfigure Handler="this.setHeight(this.container.getHeight()+20);" Delay="100" />
                                        </Listeners>
                                    </ext:GridPanel>
                                </Items>
                                
                            </ext:Panel>
                            <ext:Panel ID="pnlSizeDriverList" runat="server" AutoScroll="true">
                                <Items>
                                    <ext:GridPanel ID="grdSizeDriverList" runat="server" Border="false" Header="false" EnableLocking="true">
                                        <Store>
                                            <ext:Store ID="storeSizeDriverList" runat="server">
                                                <Model>
                                                    <ext:Model ID="modelDrivers" runat="server">
                                                        <Fields>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel RenderColumnsOnly="False" RenderXType="True" IDMode="Explicit" Namespace="App" IsDynamic="False">
                                            <Columns>
                                            </Columns>
                                        </ColumnModel>
                                        <Listeners>
                                            <Reconfigure Handler="this.setHeight(this.container.getHeight()+20);" Delay="100" />
                                        </Listeners>
                                    </ext:GridPanel>
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="pnlForceFit" runat="server" Border="false" Title=" " Height="1" Cls="reportheader"></ext:Panel>
                        </Items>
                    </ext:Panel>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="FormStatusBar" runat="server" />
                </BottomBar>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>
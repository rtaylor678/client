﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _technicalmodule : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];} set { Session["idLanguage"] = value; } } 

        private static modMain objMain = new modMain();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                LoadLanguage();
                this.LoadModules();
            }
        }

        //List of Technical Drivers
        protected void btnListTechnicaDrivers_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/technical/technicaldrivers.aspx");
        }

        //Technical Drivers Data
        protected void btnTechnicalDriversData_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/technical/technicaldriverdata.aspx");
        }

        //Technical Assumptions
        protected void btnTechnicalAssumptions_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/technical/technicalassumption.aspx");
        }

        //Utilization Capacity
        protected void btnUtilizationCapacity_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/technical/utilizationcapacity.aspx");
        }

        //Base Rates by Cost Category
        protected void btnBaseCostTechnicalModule_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/technical/basecosttechnicalmodule.aspx");
        }

        //Base Rates By Drivers
        protected void btnBaseRatesByDrivers_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/technical/baseratesbydrivers.aspx");
        }

        //Technical Files Repository (Hidden)
        protected void btnZiffTechnicalAnalysis_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/technical/zifftechnicalanalysis.aspx");
        }

        //Cost Structure Assumptions
        protected void btnCostStructureAssumptions_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/coststructureassumptions.aspx");
        }

        // Economic Buttons
        /// Price Scenarios
        protected void btnPriceScenarios_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/economic/pricescenarios.aspx");
        }

        /// Prices
        protected void btnPrices_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/economic/prices.aspx");
        }

        /// List of Economic Drivers
        protected void btnEconomicDrivers_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/economic/economicdriver.aspx");
        }

        /// Economic Drivers Data
        protected void btnEconomicDriverData_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/economic/economicdriverdata.aspx");
        }

        /// Economic Assumptions
        protected void btnPricesEconomic_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/economic/typepriceeconomic.aspx");
        }

        /// Base Cost Rates Forecast
        protected void btnPriceForecast_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/economic/priceforecast.aspx");
        }

        /// Economic Files Repository (Hidden)
        protected void btnZiffEconomicAnalysis_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/economic/ziffeconomicanalysis.aspx");
        }

        /// CostVSDriversProjection (Projection by Cost category)
        protected void btnCostvsDriversProjection_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/report/costvsdriversprojection.aspx");
        }

        /// OperationalMarginsProjection (Operational Margins Projection)
        protected void btnOperationalMarginsProjection_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/report/operationalmarginsprojection.aspx");
        }

        /// Projection By Cost Category (new Projection By Base and Incremental Line)
        protected void btnOpexReport_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/report/opexreport.aspx");
        }

        /// Projection By Case (new Projection By Cost Type)
        protected void btnOpexReportDetailed_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/report/opexreportdetailed.aspx");
        }

        /// Multi Fields Projection
        protected void btnMultiFieldsProjection_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/report/multifieldsopexreport.aspx");
        }

        #endregion

        #region Methods

        protected void LoadLanguage()
        {
            DataClass.clsLabelsLanguages objLabelsLanguages = new DataClass.clsLabelsLanguages();
            DataTable dt = objLabelsLanguages.LoadList("LanguageName = '" + Language + "' AND FormName = 'TechnicalModule'", "ORDER BY IdLabel");
            foreach (DataRow row in dt.Rows)
            {
                idLanguage = (Int32)row["IdLanguage"];
                switch ((String)row["LabelName"])
                {
                    case "Technical Module":
                        modMain.strTechnicalModuleFormTitle = (String)row["LabelText"];
                        break;
                    case "Import Technical Driver Data":
                        modMain.strTechnicalModuleImportTechnicalDriverData = (String)row["LabelText"];
                        break;
                }
            }

            //Economic Module
            dt = objLabelsLanguages.LoadList("LanguageName = '" + Language + "' AND FormName = 'EconomicModule'", "ORDER BY IdLabel");
            foreach (DataRow row in dt.Rows)
            {
                //idLanguage = (Int32)row["IdLanguage"];
                switch ((String)row["LabelName"])
                {
                    case "Economic Module":
                        modMain.strEconomicModuleFormTitle = (String)row["LabelText"];
                        break;
                    case "Economic Drivers":
                        modMain.strEconomicModuleEconomicDrivers = (String)row["LabelText"];
                        break;
                    case "Economic Driver Group":
                        modMain.strEconomicModuleEconomicDriverGroup = (String)row["LabelText"];
                        break;
                    case "Manage Economic Driver Groups":
                        modMain.strEconomicModuleManageEconomicDriverGroups = (String)row["LabelText"];
                        break;
                    case "Add New Economic Driver":
                        modMain.strEconomicModuleAddNewEconomicDriver = (String)row["LabelText"];
                        break;
                    case "Driver Group":
                        modMain.strEconomicModuleDriverGroup = (String)row["LabelText"];
                        break;
                    case "Economic Driver":
                        modMain.strEconomicModuleEconomicDriver = (String)row["LabelText"];
                        break;
                    case "Import Economic Drivers":
                        modMain.strEconomicModuleImportEconomicDrivers = (String)row["LabelText"];
                        break;
                    case "Add New Economic Driver Group":
                        modMain.strEconomicModuleAddNewEconomicDriverGroup = (String)row["LabelText"];
                        break;
                    case "List of Economic Drivers":
                        modMain.strEconomicModuleListofEconomicDrivers= (String)row["LabelText"];
                        break;
                    case "Pessimistic":
                        modMain.strEconomicModulePessimistic= (String)row["LabelText"];
                        break;
                    case "Optimistic":
                        modMain.strEconomicModuleOptimistic= (String)row["LabelText"];
                        break;
                    case "Import Economic Driver Data":
                        modMain.strEconomicModuleImportEconomicDriverData = (String)row["LabelText"];
                        break;
                    case "Economic Scenarios:":
                        modMain.strEconomicModuleEconomicScenarios = (String)row["LabelText"];
                        break;
                }
            }
            //Report Module
            dt = objLabelsLanguages.LoadList("LanguageName = '" + Language + "' AND FormName = 'ReportsModule'", "ORDER BY IdLabel");
            foreach (DataRow row in dt.Rows)
            {
                //idLanguage = (Int32)row["IdLanguage"];
                switch ((String)row["LabelName"])
                {
                    case "Reports Module":
                        modMain.strReportsModuleFormTitle = (String)row["LabelText"];
                        break;
                    case "Key Performance Indicators Estimates":
                        modMain.strReportsModuleKPIEstimatesLong = (String)row["LabelText"];
                        break;
                    case "Driver:":
                        modMain.strReportsModuleDriver = (String)row["LabelText"];
                        break;
                    case "Technical Driver #":
                        modMain.strReportsModuleTechnicalDriverNum = (String)row["LabelText"];
                        break;
                    case "Key Performance Cost Indicators Estimates":
                        modMain.strReportsModuleSubFormTitle3 = (String)row["LabelText"];
                        break;
                    case "Aggregation Level:":
                        modMain.strReportsModuleAggregationLevel = (String)row["LabelText"];
                        break;
                    case "Cost Structure:":
                        modMain.strReportsModuleCostStructure = (String)row["LabelText"];
                        break;
                    case "From:":
                        modMain.strReportsModuleFrom = (String)row["LabelText"];
                        break;
                    case "To:":
                        modMain.strReportsModuleTo = (String)row["LabelText"];
                        break;
                    case "Technical Scenario:":
                        modMain.strReportsModuleTechnicalScenario = (String)row["LabelText"];
                        break;
                    case "Economic Scenario:":
                        modMain.strReportsModuleEconomicScenario = (String)row["LabelText"];
                        break;
                    case "Currency:":
                        modMain.strReportsModuleCurrency = (String)row["LabelText"];
                        break;
                    case "Term:":
                        modMain.strReportsModuleTerm = (String)row["LabelText"];
                        break;
                    case "Run Report":
                        modMain.strReportsModuleRunReport = (String)row["LabelText"];
                        break;
                    case "KPI Target":
                        modMain.strReportsModuleKPITarget = (String)row["LabelText"];
                        break;
                    case "Years":
                        modMain.strReportsModuleYears = (String)row["LabelText"];
                        break;
                    case "Reset":
                        modMain.strReportsModuleReset = (String)row["LabelText"];
                        break;
                    case "Export Report Data To Excel":
                        modMain.strReportsModuleExportReportDataToExcel = (String)row["LabelText"];
                        break;
                    case "Technical Drivers Forecast Indicator":
                        modMain.strReportsModuleResultsTitle = (String)row["LabelText"];
                        break;
                    case "Total Projection":
                        modMain.strReportsModuleOpexResultsTitle1 = (String)row["LabelText"];
                        break;
                    case "Projection By Project":
                        modMain.strReportsModuleOpexResultsTitle2 = (String)row["LabelText"];
                        break;
                    case "Baseline":
                        modMain.strReportsModuleOpexResultsSubTitle1 = (String)row["LabelText"];
                        break;
                    case "Business Opportunities":
                        modMain.strReportsModuleOpexResultsSubTitle2 = (String)row["LabelText"];
                        break;
                    case "1. Cost Projection Reports":
                        modMain.strReportsModuleFormSubTitle1 = (String)row["LabelText"];
                        break;
                    case "2. Projection Base Rates Reports":
                        modMain.strReportsModuleFormSubTitle2 = (String)row["LabelText"];
                        break;
                    case "3. Historical Base Cost Reports":
                        modMain.strReportsModuleFormSubTitle3 = (String)row["LabelText"];
                        break;
                    case "Type of Operation":
                        modMain.strReportsModuleFormOperationType = (String)row["LabelText"];
                        break;
                    case "Cost Type":
                        modMain.strReportsModuleFormTypeCost = (String)row["Labeltext"];
                        break;
                }
            }
            this.LoadLabel();
        }

        protected void LoadModules()
        {
            // Technical Module
            if (objApplication.AllowedRoles.Contains(28))
            {
                // List of Technical Drivers
                if (objApplication.AllowedRoles.Contains(29))
                {
                    this.btnListTechnicaDrivers.Disabled = false;
                }
                // Technical Drivers Data
                if (objApplication.AllowedRoles.Contains(30))
                {
                    this.btnTechnicalDriversData.Disabled = false;
                }
                // Technical Assumptions
                if (objApplication.AllowedRoles.Contains(31))
                {
                    this.btnTechnicalAssumptions.Disabled = false;
                }
                // Utilization Capacity
                if (objApplication.AllowedRoles.Contains(45))
                {
                    this.btnUtilizationCapacity.Disabled = false;
                }
                // Base Rates By Cost category
                if (objApplication.AllowedRoles.Contains(32))
                {
                    this.btnBaseCostTechnicalModule.Disabled = false;
                }
                // Base Rates By Drivers
                if (objApplication.AllowedRoles.Contains(46))
                {
                    this.btnBaseRatesByDrivers.Disabled = false;
                }
                // Cost Structure Assumptions
                if (objApplication.AllowedRoles.Contains(27))
                {
                    this.btnCostStructureAssumptions.Disabled = false;
                }
                //}

                //// Economic Module
                //if (objApplication.AllowedRoles.Contains(34))
                //{
                // Price Scenarios
                if (objApplication.AllowedRoles.Contains(52))
                {
                    this.btnPriceScenarios.Disabled = false;
                }
                // Prices Data
                if (objApplication.AllowedRoles.Contains(53))
                {
                    this.btnPrices.Disabled = false;
                }
                // List of Economic Drivers
                if (objApplication.AllowedRoles.Contains(35))
                {
                    this.btnEconomicDrivers.Disabled = false;
                }
                // Economic Drivers Data
                if (objApplication.AllowedRoles.Contains(36))
                {
                    this.btnEconomicDriverData.Disabled = false;
                }
                // Economic Assumptions
                if (objApplication.AllowedRoles.Contains(37))
                {
                    this.btnPricesEconomic.Disabled = false;
                }
                // Base Cost Rates Forecast
                if (objApplication.AllowedRoles.Contains(38))
                {
                    this.btnPriceForecast.Disabled = false;
                }
            //}

            //// Reports Module
            //if (objApplication.AllowedRoles.Contains(40))
            //{
                // Projection by Cost Category
                if (objApplication.AllowedRoles.Contains(56))
                {
                    this.btnCostvsDriversProjection.Disabled = false;
                }
                // Operational Margins Projection
                if (objApplication.AllowedRoles.Contains(55))
                {
                    this.btnOperationalMarginsProjection.Disabled = false;
                }
                // Projection by Development Case
                if (objApplication.AllowedRoles.Contains(57))
                {
                    this.btnOpexReport.Disabled = false;
                }
                // Projection by Cost Type
                if (objApplication.AllowedRoles.Contains(58))
                {
                    this.btnOpexReportDetailed.Disabled = false;
                }
                // Multiple Fields Projection
                if (objApplication.AllowedRoles.Contains(59))
                {
                    this.btnMultiFieldsProjection.Disabled = false;
                }
            }
        }

        protected void LoadLabel()
        {
            this.pnlTechnicalModule.Title = modMain.strTechnicalModuleFormTitle;
            this.btnListTechnicaDrivers.Text = objMain.FormatButtonText(modMain.strMasterListTechnicalDrivers);
            this.btnTechnicalDriversData.Text = objMain.FormatButtonText(modMain.strMasterTechnicalDriversData);
            this.btnTechnicalAssumptions.Text = objMain.FormatButtonText(modMain.strMasterTechnicalAssumptions);
            this.btnBaseCostTechnicalModule.Text = objMain.FormatButtonText(modMain.strMasterBaseCostRates);
            this.btnZiffTechnicalAnalysis.Text = objMain.FormatButtonText(modMain.strMasterZiffTechnicalAnalysis);
            this.btnUtilizationCapacity.Text = objMain.FormatButtonText(modMain.strTechnicalModuleUtilizationCapacity);
            this.btnBaseRatesByDrivers.Text = objMain.FormatButtonText(modMain.strTechnicalModuleBaseRatesByDrivers);
            this.FieldSet1.Title = "1. " + (String)GetGlobalResourceObject("CPM_Resources", "TechnicalInputs");
            this.FieldSet2.Title = "2. " + (String)GetGlobalResourceObject("CPM_Resources", "EconomicInputs");
            this.FieldSet3.Title = "3. " + (String)GetGlobalResourceObject("CPM_Resources", "CostProjectionReports");
            //Economic Module
            this.btnCostStructureAssumptions.Text = objMain.FormatButtonText(modMain.strMasterCostStructureAssumptions);
            this.btnEconomicDrivers.Text = objMain.FormatButtonText(modMain.strMasterListofEconomicDrivers);
            this.btnEconomicDriverData.Text = objMain.FormatButtonText(modMain.strMasterEconomicDriversData);
            this.btnPricesEconomic.Text = objMain.FormatButtonText(modMain.strMasterEconomicAssumptions);
            this.btnPriceForecast.Text = objMain.FormatButtonText(modMain.strMasterBaseCostRatesForecast);
            this.btnZiffEconomicAnalysis.Text = objMain.FormatButtonText(modMain.strMasterZiffEconomicAnalysis);
            this.btnPrices.Text = (String)GetGlobalResourceObject("CPM_Resources", "Prices");
            this.btnPriceScenarios.Text = (String)GetGlobalResourceObject("CPM_Resources", "PriceScenarios");
            //Report Module
            this.btnOpexReport.Text = objMain.FormatButtonText((String)GetGlobalResourceObject("CPM_Resources", "Projection By Cost Category"));
            this.btnOpexReportDetailed.Text = objMain.FormatButtonText((String)GetGlobalResourceObject("CPM_Resources", "Projection By Case"));
            this.btnMultiFieldsProjection.Text = objMain.FormatButtonText((String)GetGlobalResourceObject("CPM_Resources", "Multiple Fields Projection"));
            this.btnCostvsDriversProjection.Text = objMain.FormatButtonText((String)GetGlobalResourceObject("CPM_Resources", "CostVSDriversProjection"));
            this.btnOperationalMarginsProjection.Text = objMain.FormatButtonText((String)GetGlobalResourceObject("CPM_Resources", "OperationalMarginsProjection"));
        }

        #endregion 

    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="technicaldriverdata.aspx.cs" Inherits="CPM._technicaldriverdata" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" AutoScroll="true" runat="server" Title="Technical Driver Data" IconCls="icon-technicaldriversdata_16" Border="false" Resizable="false" Layout="BorderLayout">
                <Items>
                    <%--<ext:Panel ID="Panel3" runat="server" AutoScroll="true">
                        <Items>--%>
                            <ext:Panel ID="pnlButtons01" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0" region="North">
                                <Items>
                                    <ext:Panel ID="Panel100" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 10px 0 10px 10px">
                                        <Items>
                                            <ext:Label Style="margin: 0 0 0 10px" ID="lblFilterTechnicalDriver" runat="server" Text="Technical Driver:">
                                            </ext:Label>
                                            <ext:ComboBox Style="margin: 0 0 0 10px" MinChars="0" ID="ddlFilterTechnicalDriver" runat="server" Width="200" DisplayField="Name" ValueField="ID">
                                                <ListConfig LoadingText="Searching..." MinWidth="300">
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeFilterTechnicalDriver" runat="server" OnReadData="StoreFilterTechnicalDriver_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model6" runat="server" IDProperty="ID">
                                                                <Fields>
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="ID" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                                <DirectEvents>
                                                    <Select OnEvent="ddlFilterTechnicalDriver_Select">
                                                        <EventMask ShowMask="true" />
                                                    </Select>
                                                </DirectEvents>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel101" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                                        <Items>
                                            <ext:Label Style="margin: 10px 0 10px 10px" ID="lblFilterField" runat="server" Text="Field:">
                                            </ext:Label>
                                            <ext:ComboBox Style="margin: 10px 0 10px 10px" ID="ddlField" MinChars="0" runat="server" Width="200" DisplayField="Name" ValueField="IdField">
                                                <ListConfig LoadingText="Searching..." MinWidth="300">
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeField" runat="server" OnReadData="StoreField_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model5" runat="server" IDProperty="IdField">
                                                                <Fields>
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="IdField" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                                <DirectEvents>
                                                    <Select OnEvent="ddlField_Select" />
                                                </DirectEvents>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel102" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                                        <Items>
                                            <ext:Label Style="margin: 10px 0 10px 10px" ID="lblFilterProject" runat="server" Text="Project:">
                                            </ext:Label>
                                            <ext:ComboBox Style="margin: 10px 0 10px 10px" MinChars="0" ID="ddlFilterProject" runat="server" Width="500" DisplayField="VisualName" ValueField="IdProject">
                                                <ListConfig LoadingText="Searching..." MinWidth="300">
                                                    <ItemTpl ID="ItemTpl1" runat="server">
                                                        <Html>
                                                            <div>{Name}<span> | </span> {OperationName}</div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeFilterProject" runat="server" OnReadData="StoreFilterProject_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model7" runat="server" IDProperty="IdProject">
                                                                <Fields>
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="IdProject" />
                                                                    <ext:ModelField Name="OperationName" />
                                                                    <ext:ModelField Name="VisualName" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                                <DirectEvents>
                                                    <Select OnEvent="ddlFilterProject_Select" />
                                                </DirectEvents>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                </Items>
                            </ext:Panel>
                            <ext:GridPanel ID="grdTechnicalDriverData" 
                                runat="server" 
                                AutoScroll="true" 
                                Border="false" 
                                Layout="AutoLayout" 
                                StripeRows="true" 
                                EnableLocking="true" 
                                Header="true" 
                                Region="Center">
                                <TopBar>
                                    <ext:Toolbar ID="Toolbar1" runat="server">
                                        <Items>
                                            <ext:Button ID="btnSave" Icon="DataBaseSave" TextAlign="Center" ToolTip="Save Changes" runat="server" Text="Save Changes">
                                                <DirectEvents>
                                                    <Click OnEvent="btnSave_Click">
                                                        <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        <ExtraParams>
                                                            <ext:Parameter Name="rowsValues" Value="#{grdTechnicalDriverData}.getRowsValues(false)"
                                                                Mode="Raw" Encode="true" />
                                                        </ExtraParams>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                            <ext:ToolbarSeparator />
                                            <ext:Button ID="btnDelete" Icon="Delete" TextAlign="Center" ToolTip="Delete All" runat="server" Text="Delete All">
                                                <DirectEvents>
                                                    <Click OnEvent="btnDeleteAll_Click">
                                                        <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                            <ext:ToolbarSeparator />
                                            <ext:Button ID="btnImport" Icon="BookAdd" TextAlign="Center" ToolTip="Import Technical Driver Data" runat="server" Text="Import">
                                                <DirectEvents>
                                                    <Click OnEvent="btnImport_Click">
                                                        <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        <ExtraParams>
                                                            <ext:Parameter Name="rowsValues" Value="#{grdTechnicalDriverData}.getRowsValues(false)"
                                                                Mode="Raw" Encode="true" />
                                                        </ExtraParams>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                            <ext:ToolbarFill />
                                            <ext:Button ID="btnSaveExcel" runat="server" Text="Export Data To Excel" Icon="PageExcel">
                                                <DirectEvents>
                                                    <Click OnEvent="btnSaveExcel_Click" IsUpload="true">
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                            <ext:Button ID="btnDownloadTemplate" runat="server" Text="Download Import Template" Icon="PageSave">
                                                <DirectEvents>
                                                    <Click OnEvent="btnDownloadTemplate_Click" IsUpload="true">
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                                <Store>
                                    <ext:Store ID="storeTechnicalDriverData" runat="server">
                                        <Model>
                                            <ext:Model ID="Model1" runat="server" IDProperty="IdTechnicalDriverData">
                                                <Fields>
                                                    <ext:ModelField Name="IdTechnicalDriver" Type="Int" />
                                                    <ext:ModelField Name="IdProject" Type="Int" />
                                                    <ext:ModelField Name="IdField" Type="Int" />
                                                    <ext:ModelField Name="Year" Type="Int" />
                                                    <ext:ModelField Name="Normal" Type="Float" />
                                                    <ext:ModelField Name="Optimistic" Type="Float" />
                                                    <ext:ModelField Name="Pessimistic" Type="Float" />
                                                    <ext:ModelField Name="IdTechnicalDriverData" Type="Int" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <Plugins>
                                    <ext:CellEditing ID="CellEditing1" runat="server" ClicksToEdit="1" />
                                </Plugins>
                                <ColumnModel ID="ColumnModel1" runat="server">
                                    <Columns>
                                        <ext:Column ID="colID" Hidden="true" runat="server" Text="ID" DataIndex="IdTechnicalDriverData" />
                                        <ext:Column ID="Year" Width="200" runat="server" Text="Year" DataIndex="Year" />
                                        <ext:Column ID="Column1" runat="server" Text="Scenarios" Flex="2">
                                            <Columns>
                                                <ext:NumberColumn ID="Columns1" runat="server" Format="0,000.00" DataIndex="Normal" Text="Base" Flex="1" Align="Right" Width="200">
                                                    <Editor>
                                                        <ext:NumberField ID="NumberField1" runat="server" AllowBlank="false" MinValue="0">
                                                        </ext:NumberField>
                                                    </Editor>
                                                </ext:NumberColumn>
                                                <ext:NumberColumn ID="Columns2" runat="server" Format="0,000.00" DataIndex="Optimistic" Text="Optimistic" Flex="1" Align="Right" Width="200">
                                                    <Editor>
                                                        <ext:NumberField ID="NumberField2" runat="server" AllowBlank="false" MinValue="0">
                                                        </ext:NumberField>
                                                    </Editor>
                                                </ext:NumberColumn>
                                                <ext:NumberColumn ID="Columns3" runat="server" Format="0,000.00" DataIndex="Pessimistic" Text="Pessimistic" Flex="1" Align="Right" Width="200">
                                                    <Editor>
                                                        <ext:NumberField ID="NumberField3" runat="server" AllowBlank="false" MinValue="0">
                                                        </ext:NumberField>
                                                    </Editor>
                                                </ext:NumberColumn>
                                            </Columns>
                                        </ext:Column>
                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" />
                                </SelectionModel>
                                <Features>
                                    <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                                        <Filters>
                                            <ext:StringFilter DataIndex="Year" />
                                            <ext:StringFilter DataIndex="Normal" />
                                            <ext:StringFilter DataIndex="Pessimistic" />
                                            <ext:StringFilter DataIndex="Optimistic" />
                                            <ext:NumericFilter DataIndex="IdTechnicalDriverData" />
                                        </Filters>
                                    </ext:GridFilters>
                                </Features>
                            </ext:GridPanel>
                            <ext:Window ID="winImport" runat="server" Title="Import Technical Driver Data" Hidden="true" Icon="ArrowLeft" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                                <Items>
                                    <ext:FormPanel ID="frmImportFile" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                                        <Items>
                                            <ext:Panel ID="pnlImportFile1" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                                <Defaults>
                                                    <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                                    <ext:Parameter Name="MsgTarget" Value="side" />
                                                </Defaults>
                                                <Items>
                                                    <ext:FileUploadField ID="fileImport" FieldLabel="File" runat="server" Icon="Attach" Cls="PopupFormField" />
                                                </Items>
                                            </ext:Panel>
                                        </Items>
                                        <Buttons>
                                            <ext:Button ID="btnImportFile" runat="server" Text="Import" Icon="TableRefresh" Disabled="true" FormBind="true">
                                                <DirectEvents>
                                                    <Click OnEvent="btnImportFile_Click">
                                                        <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        <ExtraParams>
                                                            <ext:Parameter Name="rowsValues" Value="#{grdTechnicalDriverData}.getRowsValues(false)" Mode="Raw" Encode="true" />
                                                        </ExtraParams>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                            <ext:Button ID="btnCancelImport" runat="server" Icon="Decline" Text="Close">
                                                <DirectEvents>
                                                    <Click OnEvent="btnCancelImport_Click" />
                                                </DirectEvents>
                                            </ext:Button>
                                        </Buttons>
                                        <BottomBar>
                                            <ext:StatusBar ID="FormImportStatusBar" runat="server" />
                                        </BottomBar>
                                        <Listeners>
                                            <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                                                        text : valid ? 'Form is valid' : 'Form is invalid', 
                                                                                        iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                                                    });
                                                                                    #{btnImportFile}.setDisabled(!valid);" />
                                        </Listeners>
                                    </ext:FormPanel>
                                </Items>
                            </ext:Window>
                        <%--</Items>
                    </ext:Panel>--%>
                </Items>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="kpireport.aspx.cs" Inherits="CPM._kpireport" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:ChartTheme ID="ChartThemeSA" runat="server" ThemeName="SATheme" Colors="#4A7DC6,#CE3018,#39BE39,#FFCB4A,#7B498C,#7BCBFF,#E79642,#B53C6B,#4A4D4A,#BDD7EF,#104110,#F7BACE,#395D73" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" Layout="FitLayout" Title="Key Performance Cost Indicators Estimates" IconCls="icon-kpitarget_16" AutoScroll="true" runat="server" Border="false">
                <Items>
                    <ext:Panel ID="Panel2" runat="server" AutoScroll="true">
                        <Items>
                            <ext:Panel ID="pnlExportToolbar" runat="server" Border="false" Hidden="true">
                                <TopBar>
                                    <ext:Toolbar ID="tbrExport" Border="false" Height="30" runat="server">
                                        <Items>
                                            <ext:Button ID="btnReset" runat="server" Text="Reset" Icon="Reload">
                                                <DirectEvents>
                                                    <Click OnEvent="btnReset_Click">
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                            <ext:ToolbarFill />
                                            <ext:Button ID="btnSaveExcel" runat="server" Text="Export Report Data To Excel" Icon="PageExcel">
                                                <DirectEvents>
                                                    <Click OnEvent="btnSaveExcel_Click" IsUpload="true">
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                            </ext:Panel>
                            <ext:Panel ID="pnlButtons01" runat="server" Layout="Column" Border="false" Style="margin: 15px 0 0 0">
                                <Items>
                                    <ext:Panel ID="Panel100" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 15px 0">
                                        <Items>
                                            <ext:Label Style="margin: 0 0 0 10px" ID="lblLevels" runat="server" Text="Aggregation Level:" />
                                            <ext:ComboBox Style="margin: 0 0 0 10px" ID="ddlLevels" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" ValueField="IdAggregationLevel">
                                                <ListConfig LoadingText="Searching..." MinWidth="250">
                                                    <ItemTpl ID="ItemTpl2" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
							                                    <span>Parent: {ParentName}</span>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeLevels" runat="server" OnReadData="StoreLevel_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model2" runat="server" IDProperty="IdAggregationLevel">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdAggregationLevel" />
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="ParentName" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                                <DirectEvents>
                                                    <Select OnEvent="ddlLevels_Select" />
                                                </DirectEvents>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel101" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 15px 0">
                                        <Items>
                                            <ext:Label Style="margin: 0 0 0 10px" ID="lblField" runat="server" Text="Field:"></ext:Label>
                                            <ext:ComboBox Style="margin: 0 0 0 10px" ID="ddlField" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" ValueField="IdField">
                                                <ListConfig LoadingText="Searching..." MinWidth="250">
                                                    <ItemTpl ID="ItemTpl1" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
							                                    <span>Code: </span>{Code}</h3>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeField" runat="server" OnReadData="StoreField_ReadData">
                                                        <Model>
                                                            <ext:Model ID="mdlLabels" runat="server" IDProperty="IdField">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdField" />
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="Code" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel102" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 15px 0">
                                        <Items>
                                            <ext:Label Style="margin: 0 0 0 10px" ID="lblStructure" runat="server" Text="Cost Structure:"></ext:Label>
                                            <ext:ComboBox Style="margin: 0 0 0 10px" Editable="false" ID="ddlStructure" runat="server" Width="100">
                                                <Items>
                                                    <ext:ListItem Text="Ziff Energy" Value="1" Index="0" />
                                                    <ext:ListItem Text="Company" Value="2" />
                                                </Items>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel103" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 15px 0">
                                        <Items>
                                            <ext:Label Style="margin: 0 0 0 10px" ID="lblFrom" runat="server" Text="From:"></ext:Label>
                                            <ext:NumberField Style="margin: 0 0 0 10px" ID="txtFrom" DecimalPrecision="0" Width="55" MinValue="0" Text="2014" runat="server">
                                            </ext:NumberField>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel104" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 15px 0">
                                        <Items>
                                            <ext:Label Style="margin: 0 0 0 10px" ID="lblTo" runat="server" Text="To:"></ext:Label>
                                            <ext:NumberField Style="margin: 0 0 0 10px" ID="txtTo" DecimalPrecision="0" Width="55" MinValue="0" Text="2023" runat="server">
                                                <DirectEvents>
                                                    <Change OnEvent="txtTo_Select" />
                                                </DirectEvents>
                                            </ext:NumberField>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel105" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 15px 0">
                                        <Items>
                                            <ext:Label Style="margin: 0 0 0 10px" ID="lblTechDriver" runat="server" Text="Technical Driver:"></ext:Label>
                                            <ext:ComboBox Style="margin: 0 0 0 10px" ID="ddlTechDriver" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" ValueField="ID">
                                                <ListConfig MinWidth="250" />
                                                <Store>
                                                    <ext:Store ID="storeTechDriver" runat="server" OnReadData="StoreTechDriver_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model23" runat="server" IDProperty="ID">
                                                                <Fields>
                                                                    <ext:ModelField Name="ID" />
                                                                    <ext:ModelField Name="Name" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="pnlButtons02" runat="server" Layout="Column" Border="false" Style="margin: 0 0 0 0">
                                <Items>
                                    <ext:Panel ID="Panel200" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 15px 0">
                                        <Items>
                                        <ext:Label Style="margin: 0 0 0 10px" ID="lblScenarioTechnical" runat="server" Text="Technical Scenario:"></ext:Label>
                                        <ext:ComboBox Style="margin: 0 0 0 10px" Editable="false" ID="ddlScenarioTechnical" runat="server" Width="80">
                                            <Items>
                                                <ext:ListItem Text="Base" Value="1" Index="1" />
                                                <ext:ListItem Text="Optimistic" Value="2" />
                                                <ext:ListItem Text="Pessimistic" Value="3" />
                                            </Items>
                                        </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel201" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 15px 0">
                                        <Items>
                                            <ext:Label Style="margin: 0 0 0 10px" ID="lblScenarioEconomic" runat="server" Text="Economic Scenario:"></ext:Label>
                                            <ext:ComboBox Style="margin: 0 0 0 10px" Editable="false" ID="ddlScenarioEconomic" runat="server" Width="80">
                                                <Items>
                                                    <ext:ListItem Text="Base" Value="1" />
                                                    <ext:ListItem Text="Optimistic" Value="2" />
                                                    <ext:ListItem Text="Pessimistic" Value="3" />
                                                </Items>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel202" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 15px 0">
                                        <Items>
                                            <ext:Label Style="margin: 0 0 0 10px" ID="lblCurrency" runat="server" Text="Currency:"></ext:Label>
                                            <ext:ComboBox Style="margin: 0 0 0 10px" ID="ddlCurrency" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" ValueField="IdCurrency" Width="150">
                                                <ListConfig LoadingText="Searching..." MinWidth="250">
                                                    <ItemTpl ID="ItemTpl4" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                            <h3>{Name}</h3>
							                                            <span>Code: {Code}</span>
						                                            </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeBase" runat="server" OnReadData="StoreCurr_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model4" runat="server" IDProperty="IdCurrency">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdCurrency" />
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="Code" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel203" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 15px 0">
                                        <Items>
                                            <ext:Label Style="margin: 0 0 0 10px" ID="lblTerm" runat="server" Text="Term:"></ext:Label>
                                            <ext:ComboBox Style="margin: 0 0 0 10px" Editable="false" ID="ddlTerm" runat="server" Width="80">
                                                <Items>
                                                    <ext:ListItem Text="Real" Value="1" Index="0" />
                                                    <ext:ListItem Text="Nominal" Value="2" />
                                                </Items>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="pnlReportToolbar" runat="server" Border="false" Hidden="false">
                                <TopBar>
                                    <ext:Toolbar ID="tbrReport" Border="false" Height="30" runat="server">
                                        <Items>
                                            <ext:Button ID="btnRun" runat="server" Text="Run Report" Icon="PlayGreen">
                                                <DirectEvents>
                                                    <Click Timeout="3600000" ShowWarningOnFailure="false" OnEvent="btnRun_Click">
                                                        <EventMask ShowMask="true">
                                                        </EventMask>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                            </ext:Panel>
                            <ext:Panel ID="ReportTitle01" runat="server" IconCls="icon-kpitarget_16" Border="false" Title="KPI Target" Cls="reportheader">
                            </ext:Panel>
                            <ext:Panel runat="server" Border="false">
                                <Items>
                                    <ext:GridPanel ID="grdOpexProjection" runat="server" Border="false" Header="false">
                                        <Store>
                                            <ext:Store ID="storeBaseCostField" runat="server">
                                                <Model>
                                                    <ext:Model ID="modelField" runat="server">
                                                        <Fields>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel RenderColumnsOnly="False" RenderXType="True" IDMode="Explicit" Namespace="App" IsDynamic="False">
                                            <Columns>
                                            </Columns>
                                        </ColumnModel>
                                        <Listeners>
                                            <Reconfigure Handler="this.setHeight(this.container.getHeight()+20);" Delay="100" />
                                        </Listeners>
                                    </ext:GridPanel>
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="pnlMainChartPanel" Border="false" ManageHeight="true" runat="server" Hidden="true">
                                <Items>
                                    <ext:Panel ID="pnlChartObject" runat="server" Border="false" MaxHeight="600" Icon="ChartBar" Title="Operating Cost Projection" Layout="FitLayout" Header="false">
                                        <Items>
                                            <ext:Chart ID="chtChart" runat="server" Border="false" Animate="true" MinHeight="0" MaxHeight="700" MaxWidth="1350" Theme="SATheme" InsetPadding="30" Layout="FitLayout" AutoSize="true">
                                                <LegendConfig Position="Right" LabelFont="8px" />
                                                <Store>
                                                    <ext:Store ID="storeChart" runat="server">
                                                        <Model>
                                                            <ext:Model ID="modelChart" runat="server">
                                                                <Fields>
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                            </ext:Chart>
                                        </Items>
                                    </ext:Panel>
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="pnlForceFit" runat="server" Border="false" Title=" " Height="1" Cls="reportheader">
                            </ext:Panel>
                        </Items>
                    </ext:Panel>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="FormStatusBar" runat="server" />
                </BottomBar>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>
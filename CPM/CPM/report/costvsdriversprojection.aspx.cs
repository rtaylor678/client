﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using Ext.Net.Utilities;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Diagnostics;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CD;
using DataClass;
using System.Web.Script.Serialization;

namespace CPM
{
    public partial class _costvsdriversprojection : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdActivity { get { return (Int32)Session["IdActivity"]; } set { Session["IdActivity"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }
        private Boolean PageIsLoaded { get; set; }
        private DataReportObject ExportOpexTOT { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }
        private DataReportObject ExportOpexBL { get { return (DataReportObject)Session["Export02"]; } set { Session["Export02"] = value; } }
        private DataReportObject ExportOpexBO { get { return (DataReportObject)Session["Export03"]; } set { Session["Export03"] = value; } }
        private Int32 ReportDriversCount = 0;
        private Int32 ReportCategoriesCount = 0;
        private Int32 ReportEconomicDriversCount = 0;
        private String[] ucDrivers = new String[0];
        private String[] ucCategories = new String[0];
        private String[] ucEconomicDrivers = new String[0];
        private String[] strDrivers = new String[0];
        private String[] strCategories = new String[0];
        private String[] strEconomicDrivers = new String[0];

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (GetProject())
            {
                this.lblScenarioEconomic.Hide();
                this.ddlScenarioEconomic.Hide();
                this.lblTerm.Hide();
                this.ddlTerm.Hide();
            }

            if (!X.IsAjaxRequest)
            {
                LoadLabel();
                Session["Export01"] = null;
                Session["Export02"] = null;
                Session["Export03"] = null;
                PageIsLoaded = false;
                IdActivity = 0;
                this.ddlStructure.SelectedItem.Index = 0;
                this.ddlTerm.SelectedItem.Index = 1;
                this.ddlScenarioTechnical.SelectedItem.Index = 0;
                this.ddlScenarioEconomic.SelectedItem.Index = 0;
                this.ddlOperationType.SelectedItem.Index = 0;
                this.ddlCostType.SelectedItem.Index = 0;
                this.ddlFactor.SelectedItem.Index = 1;
                this.FromTo();
                DataTable dtOpex = new DataTable();
                ArrayList datColumnOpex = new ArrayList();
                this.FillGrid(this.storeBaseCostField, this.grdOpexProjection, dtOpex, ref datColumnOpex, false, null);
                PageIsLoaded = true;
            }
        }

        protected void ddlLevels_Select(object sender, DirectEventArgs e)
        {
            this.ddlField.Reset();
            this.storeField.Reload();
            this.ddlOperationType.Reset();
            this.storeOperationType.Reload();
        }

        protected void ddlField_Select(object sender, DirectEventArgs e)
        {
            this.ddlOperationType.Reset();
            this.storeOperationType.Reload();
        }

        protected void ddlCostCategory_Select(object sender, DirectEventArgs e)
        {
            bool All = false;
            ddlCostCategory.DeselectItem(0);
            int i = 0;

            foreach (Ext.Net.ListItem costCategory in this.ddlCostCategory.SelectedItems)
            {
                i++;
                if (Convert.ToInt32(costCategory.Value) == 0 && i == this.ddlCostCategory.SelectedItems.Count)
                    All = true;
            }

            if (All)
            {
                ddlCostCategory.SelectItem(0);
                foreach (Ext.Net.ListItem items in this.ddlCostCategory.SelectedItems)
                {
                    if (Convert.ToInt32(items.Value) != 0)
                        this.ddlCostCategory.DeselectItem(items.Index);
                }

            }
        }

        protected void btnReset_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/report/costvsdriversprojection.aspx");
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            btnSaveExcelDirect_Click(null, null);
        }

        protected void btnSaveExcelDirect_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.ModelCheckRecalcModuleReport();
                DisableParameterSelection();

                DataClass.clsModels objModels = new DataClass.clsModels();

                Int32 intLevel = Convert.ToInt32(ddlLevels.Value);
                Int32 intField = Convert.ToInt32(ddlField.Value);

                //// Check that at least one field has been selected, if none default to 'All'
                //if (this.ddlField.SelectedItems.Count == 0)
                //    ddlField.SelectedItems.Add(new Ext.Net.ListItem("0"));

                //string fields = "";
                //foreach (Ext.Net.ListItem field in this.ddlField.SelectedItems)
                //{
                //    fields += field.Value + ",";
                //}

                ////Need to remove trailing "," from the fields list
                //fields = fields.ReplaceLastInstanceOf(",", "");

                Int32 intCurrency = Convert.ToInt32(ddlCurrency.Value);
                Int32 intTerm = Convert.ToInt32(ddlTerm.Value);
                Int32 intTechnicalScenario = Convert.ToInt32(ddlScenarioTechnical.Value);
                Int32 intEconomicScenario = 0;
                Int32 intCostType = Convert.ToInt32(ddlCostType.Value);
                Int32 intTypeOPeration = Convert.ToInt32(ddlOperationType.Value);
                Int32 intFactor = Convert.ToInt32(ddlFactor.SelectedItem.Text);
                String strCurrencyCode = "";

                if (this.ddlScenarioEconomic.Hidden == false)
                {
                    intEconomicScenario = Convert.ToInt32(ddlScenarioEconomic.Value);
                }
                Int32 intFrom = Convert.ToInt32(txtFrom.Value);
                Int32 intTo = Convert.ToInt32(txtTo.Value);

                clsCurrencies objCurrencies = new clsCurrencies();
                objCurrencies.IdCurrency = intCurrency;
                objCurrencies.loadObject();
                if (intFactor == 1000)
                {
                    strCurrencyCode = "(1M_" + objCurrencies.Code + ")";
                }
                else
                {
                    strCurrencyCode = "(" + objCurrencies.Code + ")";
                }

                // Check that at least one cost category has been selected, if none default to 'All'
                if (this.ddlCostCategory.SelectedItems.Count == 0)
                    ddlCostCategory.SelectedItems.Add(new Ext.Net.ListItem("0"));

                string costCategories = "";
                foreach (Ext.Net.ListItem costCategory in this.ddlCostCategory.SelectedItems)
                {
                    costCategories += costCategory.Value + ",";
                }

                //Need to remove trailing "," from the fields list
                costCategories = costCategories.ReplaceLastInstanceOf(",", "");

                Int32 intTechnicalDriver = Convert.ToInt32(ddlFilterTechnicalDriver1.Value);
                Int32 intEconomicDriver = Convert.ToInt32(ddlFilterEconomicDriver.Value);

                DataSet ds = objModels.LoadCostVsDriversProjection(IdModel, intLevel, intField, intFrom, intTo, intTechnicalScenario, intEconomicScenario, intCurrency, intTerm, intTechnicalDriver, intEconomicDriver, intCostType, intFactor, costCategories);

                DataTable dtOpex = new DataTable();
                dtOpex = ds.Tables[0];

                ArrayList datColumnOpex = new ArrayList();
                ArrayList datColumnExport = new ArrayList();

                String strSelectedParameters = lblLevels.Text + ": '" + ddlLevels.SelectedItem.Text + "', " + 
                                               lblField.Text + " '" + ddlField.SelectedItem.Text + "', " +
                                               lblCostCategory.Text + " '" + ddlCostCategory.SelectedItem.Text + "', " +
                                               lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "', " +
                                               lblFactor.Text + " '" + ddlFactor.SelectedItem.Text + "', " +
                                               lblCostType.Text + " '" + ddlCostType.SelectedItem.Text + "', " +
                                               lblScenarioTechnical.Text + " '" + ddlScenarioTechnical.SelectedItem.Text + "', " +
                                               lblScenarioEconomic.Text + " '" + ddlScenarioEconomic.SelectedItem.Text + "', " +
                                               lblTerm.Text + " '" + ddlTerm.SelectedItem.Text + "', " +
                                               lblFrom.Text + " '" + txtFrom.Text + "', " +
                                               lblTo.Text + " '" + txtTo.Text + "', " +
                                               lblFilterTechnicalDriver1.Text + " '" + ddlFilterTechnicalDriver1.SelectedItem.Text + "', '" +
                                               lblFilterEconomicDriver.Text + " '" + ddlFilterEconomicDriver.SelectedItem.Text + "'";
                ExportOpexTOT = new DataReportObject(dtOpex, "Projection By Cost Category", "Projection By Cost Category", strSelectedParameters, null, "SortOrder", datColumnExport, null);
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Report Generated Successfully!"), IconCls = "icon-accept", Clear2 = false });
                X.Mask.Hide();

                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                if (strMessage.Contains("Timeout"))
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = strMessage.ToString(), IconCls = "icon-exclamation", Clear2 = false });
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select all filters please"), IconCls = "icon-exclamation", Clear2 = false });
                }
            }
        }

        protected void btnRun_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.ModelCheckRecalcModuleReport();
                DisableParameterSelection();

                DataClass.clsModels objModels = new DataClass.clsModels();

                Int32 intLevel = Convert.ToInt32(ddlLevels.Value);
                Int32 intField = Convert.ToInt32(ddlField.Value);

                Int32 intStructure = 1;// Convert.ToInt32(ddlStructure.Value); *** WILL ALAWAYS USE THE ZIFF STRUCTURE UNTIL OTHERWISE TOLD TO CHANGE IT ***
                Int32 intCurrency = Convert.ToInt32(ddlCurrency.Value);
                Int32 intTerm = Convert.ToInt32(ddlTerm.Value);
                Int32 intTechnicalScenario = Convert.ToInt32(ddlScenarioTechnical.Value);
                Int32 intEconomicScenario = 0;
                Int32 intCostType = Convert.ToInt32(ddlCostType.Value);
                Int32 intTypeOPeration = Convert.ToInt32(ddlOperationType.Value);
                Int32 intFactor = Convert.ToInt32(ddlFactor.SelectedItem.Text);
                String strCurrencyCode = "";

                if (this.ddlScenarioEconomic.Hidden == false)
                {
                    intEconomicScenario = Convert.ToInt32(ddlScenarioEconomic.Value);
                }
                Int32 intFrom = Convert.ToInt32(txtFrom.Value);
                Int32 intTo = Convert.ToInt32(txtTo.Value);

                clsCurrencies objCurrencies = new clsCurrencies();
                objCurrencies.IdCurrency = intCurrency;
                objCurrencies.loadObject();
                if (intFactor == 1000)
                {
                    strCurrencyCode = "(1M_" + objCurrencies.Code + ")";
                }
                else
                {
                    strCurrencyCode = "(" + objCurrencies.Code + ")";
                }

                // Check that at least one cost category has been selected, if none default to 'All'
                if (this.ddlCostCategory.SelectedItems.Count == 0)
                    ddlCostCategory.SelectedItems.Add(new Ext.Net.ListItem("0"));

                string costCategories = "";
                foreach (Ext.Net.ListItem costCategory in this.ddlCostCategory.SelectedItems)
                {
                    costCategories += costCategory.Value + ",";
                }

                //Need to remove trailing "," from the fields list
                costCategories = costCategories.ReplaceLastInstanceOf(",", "");

                Int32 intTechnicalDriver = Convert.ToInt32(ddlFilterTechnicalDriver1.Value);
                Int32 intEconomicDriver = Convert.ToInt32(ddlFilterEconomicDriver.Value);

                DataSet ds = objModels.LoadCostVsDriversProjection(IdModel, intLevel, intField, intFrom, intTo, intTechnicalScenario, intEconomicScenario, intCurrency, intTerm, intTechnicalDriver, intEconomicDriver, intCostType, intFactor, costCategories);

                DataTable dtOpex = new DataTable();
                dtOpex = ds.Tables[0];

                DataTable dtChart = new DataTable();
                dtChart = ds.Tables[1];

                DataTable dtStats = new DataTable();
                dtStats = ds.Tables[2];

                if (dtStats != null)
                {
                    if (dtStats.Rows.Count > 0)
                    {
                        ReportDriversCount = Convert.ToInt32(dtStats.Rows[0]["DriversCount"].ToString());
                        ReportCategoriesCount = Convert.ToInt32(dtStats.Rows[0]["CategoriesCount"].ToString());
                        ReportEconomicDriversCount = Convert.ToInt32(dtStats.Rows[0]["EconomicDriversCount"].ToString());
                    }
                }
                dtStats = null;

                DataTable dtUnits = new DataTable();
                dtUnits = ds.Tables[3];

                //Resize the arrays
                Array.Resize(ref ucDrivers, ReportDriversCount);
                Array.Resize(ref ucCategories, ReportCategoriesCount);
                Array.Resize(ref ucEconomicDrivers, ReportEconomicDriversCount);
                Array.Resize(ref strDrivers, ReportDriversCount);
                Array.Resize(ref strCategories, ReportCategoriesCount);
                Array.Resize(ref strEconomicDrivers, ReportEconomicDriversCount);

                int l = 0, m = 0, n = 0;
                foreach (DataRow strField in dtUnits.Rows)
                {
                    switch (Convert.ToInt32(strField["SortOrder"].ToString()))
                    {
                        case 2:
                            ucCategories[m] = strField["UnitName"].ToString();
                            strCategories[m] = strField["UnitName"].ToString();
                            m++;
                            break;
                        case 5:
                        case 7:
                            ucDrivers[l] = strField["UnitName"].ToString();
                            strDrivers[l] = strField["UnitName"].ToString();
                            l++;
                            break;
                        case 9:
                            ucEconomicDrivers[n] = strField["UnitName"].ToString();
                            strEconomicDrivers[n] = strField["UnitName"].ToString();
                            n++;
                            break;
                    }
                }

                ArrayList datColumnOpex = new ArrayList();
                ArrayList datColumnExport = new ArrayList();

                this.FillGrid(this.storeBaseCostField, this.grdOpexProjection, dtOpex, ref datColumnOpex, false, strCurrencyCode);
                this.FillChart(this.storeChart, this.chtChart, dtChart, strCurrencyCode);
                this.FillDriversChart(this.storeDriversChart, this.chtDriversChart, dtChart, strCurrencyCode);
                pnlChartObject.Render();
                pnlDriversChartObject.Render();

                String strSelectedParameters = lblLevels.Text + ": '" + ddlLevels.SelectedItem.Text + "', " +
                                               lblField.Text + " '" + ddlField.SelectedItem.Text + "', " +
                                               lblCostCategory.Text + " '" + ddlCostCategory.SelectedItem.Text + "', " +
                                               lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "', " +
                                               lblFactor.Text + " '" + ddlFactor.SelectedItem.Text + "', " +
                                               lblCostType.Text + " '" + ddlCostType.SelectedItem.Text + "', " +
                                               lblScenarioTechnical.Text + " '" + ddlScenarioTechnical.SelectedItem.Text + "', " +
                                               lblScenarioEconomic.Text + " '" + ddlScenarioEconomic.SelectedItem.Text + "', " +
                                               lblTerm.Text + " '" + ddlTerm.SelectedItem.Text + "', " +
                                               lblFrom.Text + " '" + txtFrom.Text + "', " +
                                               lblTo.Text + " '" + txtTo.Text + "', " +
                                               lblFilterTechnicalDriver1.Text + " '" + ddlFilterTechnicalDriver1.SelectedItem.Text + "', '" +
                                               lblFilterEconomicDriver.Text + " '" + ddlFilterEconomicDriver.SelectedItem.Text + "'";
                ExportOpexTOT = new DataReportObject(dtOpex, "Projection By Cost Category", "Projection By Cost Category", strSelectedParameters, null, "SortOrder", datColumnExport, null);
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Report Generated Successfully!"), IconCls = "icon-accept", Clear2 = false });
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                if (strMessage.Contains("Timeout"))
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = strMessage.ToString(), IconCls = "icon-exclamation", Clear2 = false });
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select all filters please"), IconCls = "icon-exclamation", Clear2 = false });
                }
            }
        }

        protected void StoreLevel_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dt = objAggregationLevels.LoadList("IdModel = " + IdModel, "ORDER BY Name");

            DataRow drLevel = dt.NewRow();
            drLevel["IdAggregationLevel"] = 0;
            drLevel["Name"] = "- All -";
            dt.Rows.Add(drLevel);
            dt.AcceptChanges();
            dt.DefaultView.Sort = "Name";
            DataTable dtSort = dt.DefaultView.ToTable();

            this.storeLevels.DataSource = dtSort;
            this.storeLevels.DataBind();

            this.ddlLevels.Value = 0;

            this.storeLevels.DataBind();
        }

        protected void StoreCurr_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsCurrencies objCurrencies = new DataClass.clsCurrencies();
            this.storeBase.DataSource = objCurrencies.LoadList("IdModel IN (" + IdModel + ") AND Output=1", "ORDER BY BaseCurrency DESC,Name");
            this.storeBase.DataBind();
            DataTable dt = objCurrencies.LoadList("BaseCurrency = 1 AND IdModel = " + IdModel, "");
            if (dt.Rows.Count > 0)
            {
                ddlCurrency.Value = dt.Rows[0]["IdCurrency"];
            }
            else
            {
                ddlCurrency.SelectedItem.Index = 0;
            }
        }

        protected void StoreField_ReadData(object sender, StoreReadDataEventArgs e)
        {

            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dtField = new DataTable();
            if ((ddlLevels.Value == null) || (ddlLevels.Value.ToString() == "0") || (ddlLevels.Value.ToString() == ""))
            {
                dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
            }
            else
            {
                dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
            }
            DataRow drField = dtField.NewRow();
            drField["IdField"] = 0;
            drField["Name"] = "- All -";//drField["CodeName"] = "- All -"; //
            dtField.Rows.Add(drField);
            dtField.AcceptChanges();
            dtField.DefaultView.Sort = "Name";
            DataTable dtFieldSort = dtField.DefaultView.ToTable();

            //this.storeField.DataSource = dtFieldSort;
            this.storeField.DataSource = dtField;
            this.storeField.DataBind();

            this.ddlField.Value = 0;
        }

        protected void StoreCostType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsCostType objCostTypes = new DataClass.clsCostType();
            DataTable dt = objCostTypes.LoadList("", "ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                ddlCostType.Value = dt.Rows[0]["IdCostType"];
            }
            else
            {
                ddlCostType.SelectedItem.Index = 0;
            }
            foreach (DataRow row in dt.Rows)
            {
                if (idLanguage == 2)
                {
                    row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
                }
            }
            this.storeCostType.DataSource = dt;
            this.storeCostType.DataBind();
        }

        protected void StoreTechnicalScenarioType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsScenarios objScenarioTypes = new DataClass.clsScenarios();
            DataTable dt = objScenarioTypes.LoadList("", "ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                ddlCostType.Value = dt.Rows[0]["IdScenarioType"];
            }
            else
            {
                ddlCostType.SelectedItem.Index = 0;
            }
            foreach (DataRow row in dt.Rows)
            {
                if (idLanguage == 2)
                {
                    row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
                }
            }
            this.storeTechnicalScenarioType.DataSource = dt;
            this.storeTechnicalScenarioType.DataBind();
        }

        protected void StoreEconomicScenarioType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsScenarios objScenarioTypes = new DataClass.clsScenarios();
            DataTable dt = objScenarioTypes.LoadList("", "ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                ddlCostType.Value = dt.Rows[0]["IdScenarioType"];
            }
            else
            {
                ddlCostType.SelectedItem.Index = 0;
            }
            foreach (DataRow row in dt.Rows)
            {
                if (idLanguage == 2)
                {
                    row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
                }
            }
            this.storeEconomicScenarioType.DataSource = dt;
            this.storeEconomicScenarioType.DataBind();
        }

        protected void StoreOperationType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsOperationType objoperationTypes = new DataClass.clsOperationType();
            DataTable dtOperation = new DataTable();

            if (((ddlLevels.Value == null) || (ddlLevels.Value.ToString() == "0") || (ddlLevels.Value.ToString() == "")) && ((ddlField.Value == null) || (ddlField.Value.ToString() == "0") || (ddlField.Value.ToString() == "")))
            {
                dtOperation = objoperationTypes.LoadList("IdModel=" + IdModel, "");//"[vListTypesOperation].IdModel=" + IdModel, "");

                DataRow drOperation = dtOperation.NewRow();
                drOperation["IdTypeOperation"] = 0;
                drOperation["Name"] = "- All -";
                dtOperation.Rows.Add(drOperation);
                dtOperation.AcceptChanges();
                dtOperation.DefaultView.Sort = "Name";

                ddlOperationType.Value = 0;
            }
            else if (Convert.ToInt32(ddlLevels.Value) > 0)
            {
                dtOperation = objoperationTypes.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
                if (dtOperation.Rows.Count > 0)
                {
                    ddlOperationType.Value = dtOperation.Rows[0]["IdTypeOperation"];
                }
            }
            else
            {
                dtOperation = objoperationTypes.LoadList("IdModel=" + IdModel + " AND IdField = " + ddlField.Value, " Order by Name");//"[vListTypesOperation].IdModel=" + IdModel + " AND tblFields.IdField = " + ddlField.Value, " Order by Name");
                if (dtOperation.Rows.Count > 0)
                {
                    ddlOperationType.Value = dtOperation.Rows[0]["IdTypeOperation"];
                }
            }

            DataTable dtOperationSort = dtOperation.DefaultView.ToTable();

            this.storeOperationType.DataSource = dtOperationSort;
            this.storeOperationType.DataBind();
        }

        protected void StoreCostCategory_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsZiffAccount objZiffAccount = new DataClass.clsZiffAccount();
            DataTable dtZiffAccount = objZiffAccount.LoadComboBox("IdModel = " + IdModel + " AND Parent IS NULL", " ORDER BY Parent");
            DataRow drAccount = dtZiffAccount.NewRow();
            drAccount["IdZiffAccount"] = 0;
            drAccount["Name"] = "- All -";
            drAccount["Code"] = "- All -";
            drAccount["Parent"] = 0;
            dtZiffAccount.Rows.Add(drAccount);
            dtZiffAccount.AcceptChanges();
            dtZiffAccount.DefaultView.Sort = "Code";//Parent";
            this.storeCostCategory.DataSource = dtZiffAccount;
            this.storeCostCategory.DataBind();
            ddlCostCategory.Value = 0;
        }

        protected void StoreFilterTechnicalDriver1_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsTechnicalDrivers objTechnicalDrivers = new DataClass.clsTechnicalDrivers();
            //DataTable dtTecnicalDriver = objTechnicalDrivers.LoadComboBox("IdModel = " + IdModel + " AND tblTechnicalDrivers.Name LIKE '%" + this.ddlFilterTechnicalDriver1.Text + "%'", "ORDER BY Name");
            DataTable dtTecnicalDriver = objTechnicalDrivers.LoadComboBox("IdModel = " + IdModel, "ORDER BY Name");
            DataRow drTecnicalDriver = dtTecnicalDriver.NewRow();
            drTecnicalDriver["ID"] = 0;
            drTecnicalDriver["Name"] = "- All -";
            dtTecnicalDriver.Rows.Add(drTecnicalDriver);
            dtTecnicalDriver.AcceptChanges();
            dtTecnicalDriver.DefaultView.Sort = "Name";
            this.storeFilterTechnicalDriver1.DataSource = dtTecnicalDriver;
            this.storeFilterTechnicalDriver1.DataBind();
            ddlFilterTechnicalDriver1.Value = 0;
        }

        protected void StoreFilterEconomicDriver_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsEconomicDriver objEconomicDrivers = new DataClass.clsEconomicDriver();
            DataTable dtEconomicDriver = objEconomicDrivers.LoadComboBox("IdModel = " + IdModel, "ORDER BY Name");
            DataRow drEconomicDriver = dtEconomicDriver.NewRow();
            drEconomicDriver["ID"] = 0;
            drEconomicDriver["Name"] = "- All -";
            dtEconomicDriver.Rows.Add(drEconomicDriver);
            dtEconomicDriver.AcceptChanges();
            dtEconomicDriver.DefaultView.Sort = "Name";
            this.storeFilterEconomicDriver.DataSource = dtEconomicDriver;
            this.storeFilterEconomicDriver.DataBind();
            ddlFilterEconomicDriver.Value = 0;
        }

        protected void ddlFilterTechnicalDriver1_Select(object sender, DirectEventArgs e)
        {
        }

        protected void ddlFilterEconomicDriver_Select(object sender, DirectEventArgs e)
        {
        }

        #endregion

        #region Methods

        public Boolean GetProject()
        {
            Boolean Value = false;
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("Level IN (1,2) AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = true;
            }

            return Value;
        }

        protected void ClearReport()
        {
            if (PageIsLoaded)
            {
                this.storeBaseCostField.RemoveAll();
                this.pnlChartObject.ClearContent();
            }
        }

        protected void FromTo()
        {
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                this.txtFrom.Value = (Int32)dt.Rows[0]["BaseYear"];
                this.txtTo.Value = Convert.ToInt32(dt.Rows[0]["BaseYear"]) + Convert.ToInt32(dt.Rows[0]["IdProjection"]);

                this.txtFrom.MinValue = (Int32)dt.Rows[0]["BaseYear"];
                this.txtFrom.MaxValue = (Int32)dt.Rows[0]["BaseYear"] + (Int32)dt.Rows[0]["IdProjection"];

                this.txtFrom.MinValue = (Int32)dt.Rows[0]["BaseYear"];
                this.txtTo.MaxValue = Convert.ToInt32(dt.Rows[0]["BaseYear"]) + Convert.ToInt32(dt.Rows[0]["IdProjection"]);

                this.txtFrom.DataBind();
                this.txtTo.DataBind();

            }
        }

        protected Int32 GetIdOperation(String _TypeOperation)
        {
            Int32 Value = 0;
            DataClass.clsTypesOperation objTypesOperation = new DataClass.clsTypesOperation();
            DataTable dt = objTypesOperation.LoadList("Name = '" + _TypeOperation + "'", "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdTypeOperation"];
            }
            return Value;
        }

        private void FillGrid(Ext.Net.Store s, Ext.Net.GridPanel g, DataTable dt, ref ArrayList colVisible, Boolean IsBLBO, String CurrencyCode)
        {
            s.RemoveFields();

            foreach (DataColumn c in dt.Columns)
            {
                    if (c.ColumnName == "SortOrder" || c.ColumnName == "Unit" || c.ColumnName == "CostCategory" || c.ColumnName == "UnitSubname")
                    {
                        if (!c.ColumnName.Contains("Id"))
                            s.AddField(new ModelField(c.ColumnName, ModelFieldType.Auto));
                    }
                    else
                    {
                        if (!c.ColumnName.Contains("Id"))
                            s.AddField(new ModelField(c.ColumnName, ModelFieldType.Float));
                    }
            }
            g.Features.Clear();

            if (!IsBLBO)
            {
                GroupingSummary gsum = new GroupingSummary();
                gsum.GroupHeaderTplString = "{name}";
                gsum.HideGroupedHeader = true;
                gsum.ShowSummaryRow = false;
                g.Features.Add(gsum);
            }
            
            GridFilters f = new GridFilters();
            f.Local = true;
            g.Features.Add(f);

            Summary smry = new Summary();
            smry.Dock = new SummaryDock();
            smry.ID = "Summary" + g.ID.ToString();
            smry.ShowSummaryRow = false; 

            g.Features.Add(smry);

            foreach (DataColumn c in dt.Columns)
            {
                if (!c.ColumnName.Contains("Id"))
                {
                    if (c.ColumnName == "SortOrder" || c.ColumnName == "Unit" || c.ColumnName == "CostCategory" || c.ColumnName == "UnitSubname")
                    {
                        StringFilter sFilter = new StringFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                    else
                    {
                        NumericFilter sFilter = new NumericFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                }
            }

            Column col = new Column();
            col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Years");

            SummaryColumn colSum = new SummaryColumn();
            foreach (DataColumn c in dt.Columns)
            {
                if ( c.ColumnName == "SortOrder" || c.ColumnName == "Unit" || c.ColumnName == "CostCategory" || c.ColumnName == "UnitSubname")
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        if (c.ColumnName != "SortOrder")// && c.ColumnName != "Unit")
                        {
                            colSum = new SummaryColumn();
                            colSum.DataIndex = c.ColumnName;
                            if (c.ColumnName != "Unit")
                                colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", c.ColumnName);
                            else
                                colSum.Text = "";
                            colSum.Locked = true;
                            colVisible.Add(new DataClass.DataTableColumnDisplay(c.ColumnName, colSum.Text));
                            colSum.SummaryType = Ext.Net.SummaryType.Count;
                            colSum.TdCls = "task";
                            colSum.Sortable = true;
                            colSum.Groupable = true;
                            colSum.Width = 250;
                            colSum.SummaryRenderer.Handler = "return '(' + value + ' Total)';";
                            colSum.Renderer.Fn = "formatCol";
                            colSum.MenuDisabled = false;
                            colSum.Align = Alignment.Left;
                            g.ColumnModel.Columns.Add(colSum);
                        }
                    }
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        colSum = new SummaryColumn();
                        colSum.DataIndex = c.ColumnName;
                        colSum.Text = c.ColumnName;
                        colVisible.Add(new DataClass.DataTableColumnDisplay(c.ColumnName, colSum.Text));
                        colSum.SummaryType = Ext.Net.SummaryType.Sum;
                        colSum.Renderer.Fn = "formatColData";
                        colSum.Align = Alignment.Right;
                        colSum.MinWidth = 80;
                        col.Columns.Add(colSum);
                    }
                }
            }

            g.ColumnModel.Columns.Add(col);
            s.DataSource = dt;
            //s.GroupField = "ParentName";
            
            if (X.IsAjaxRequest)
            {
                g.Reconfigure();
            }
            s.DataBind();
        }

        private void FillChart(Ext.Net.Store s, Ext.Net.Chart c, DataTable dt, String CurrencyCode)
        {
            try
            {
                string[] str = new string[dt.Columns.Count - 1];
                int i = 0;
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName != "Year")
                    {
                        str[i] = col.ColumnName;
                        i = i + 1;
                    }
                }
                string[] strcat = new string[] { "Year" };

                s.RemoveFields();

                foreach (DataColumn col in dt.Columns)
                {
                    s.AddField(new ModelField(col.ColumnName, ModelFieldType.Auto));
                }
                s.DataSource = dt;
                s.DataBind();

                #region Prepare Axis

                AxisLabel AxisL = new AxisLabel();
                AxisL.Renderer.Handler = "return Ext.util.Format.number(value, '0,000.00');";

                AxisLabel AxisR = new AxisLabel();
                AxisR.Renderer.Handler = "return Ext.util.Format.number(value, '0,000');";

                AxisLabel AxisB = new AxisLabel();
                AxisB.Renderer.Handler = "return Ext.util.Format.number(value, '0000');";
                AxisB.Rotate = new RotateAttribute()
                {
                    Degrees = 90,
                };

                //Left Axis
                c.Axes.Add(new NumericAxis()
                {
                    Title = "Cost ($)",
                    Fields = strCategories,
                    MajorTickSteps = 9,
                    Grid = true,
                    Position = Position.Left,
                    GridConfig = new AxisGrid()
                    {
                        Odd = new SpriteAttributes()
                        {
                            Opacity = 1,
                            StrokeWidth = 0.5
                        }
                    },
                    Label = AxisL
                });

                //Bottom Axis
                c.Axes.Add(new CategoryAxis()
                {
                    Title = (String)GetGlobalResourceObject("CPM_Resources", "Year"),
                    Fields = strcat,
                    Position = Position.Bottom,
                    Grid = true,
                    Label = AxisB
                });

                #endregion Prepare Axis

                #region ColumnSeries (Drivers)

                ChartTip chrtTip2 = new ChartTip();
                chrtTip2.TrackMouse = true;
                chrtTip2.Width = 140;
                chrtTip2.StyleSpec = "background:#fff; text-align:center;";
                chrtTip2.Height = 20;
                chrtTip2.Renderer.Handler = "this.setTitle(storeItem.get('" + strcat[0] + "') + ': ' + String(Ext.util.Format.number(item.value[1], '0,000.00')));";

                c.Series.Add(new ColumnSeries()
                {
                    Titles = strCategories,
                    XField = strcat,
                    YField = ucCategories,
                    Axis = Position.Left,
                    Tips = chrtTip2,
                    Stacked = true,
                });

                #endregion

                #region LineSeries (Costs and Drivers)

                //Ext.Net.SpriteType[] strSpriteTypes = new Ext.Net.SpriteType[] { Ext.Net.SpriteType.Circle, Ext.Net.SpriteType.Cross, Ext.Net.SpriteType.Diamond, Ext.Net.SpriteType.Square, Ext.Net.SpriteType.Triangle };
                //int j = 0;

                ///** Costs **/
                //foreach (string strField in strCategories)
                //{
                //    ChartTip chrtTip = new ChartTip();
                //    chrtTip.TrackMouse = true;
                //    chrtTip.Width = 140;
                //    chrtTip.StyleSpec = "background:#fff; text-align:center;";
                //    chrtTip.Height = 20;
                //    chrtTip.Renderer.Handler = "this.setTitle(storeItem.get('" + strcat[0] + "') + ': ' + String(Ext.util.Format.number(item.value[1], '0,000.00')));";

                //    c.Series.Add(new LineSeries()
                //    {
                //        Titles = new string[] { strField + " (Cost)" },
                //        XField = strcat,
                //        YField = new string[] { strField },
                //        Axis = Position.Left,
                //        Tips = chrtTip,
                //        MarkerConfig = new SpriteAttributes()
                //        {
                //            Type = strSpriteTypes[j],
                //            Size = 4,
                //            Radius = 4,
                //            StrokeWidth = 0,
                //        },
                //        HighlightConfig = new SpriteAttributes()
                //        {
                //            Size = 5,
                //            Radius = 5
                //        },
                //    });
                //    if (j < 4)
                //        j++;
                //    else
                //        j = 0;
                //}

                /////** Drivers **/
                ////foreach (string strField in strDrivers)
                ////{
                ////    ChartTip chrtTip = new ChartTip();
                ////    chrtTip.TrackMouse = true;
                ////    chrtTip.Width = 140;
                ////    chrtTip.StyleSpec = "background:#fff; text-align:center;";
                ////    chrtTip.Height = 20;
                ////    chrtTip.Renderer.Handler = "this.setTitle(storeItem.get('" + strcat[0] + "') + ': ' + String(Ext.util.Format.number(item.value[1], '0,000')));";

                ////    c.Series.Add(new LineSeries()
                ////    {
                ////        Titles = new string[] { strField + " (Driver)" },
                ////        XField = strcat,
                ////        YField = new string[] { strField },
                ////        Axis = Position.Right,
                ////        Tips = chrtTip,
                ////        MarkerConfig = new SpriteAttributes()
                ////        {
                ////            Type = strSpriteTypes[j],
                ////            Size = 4,
                ////            Radius = 4,
                ////            StrokeWidth = 0,
                ////        },
                ////        HighlightConfig = new SpriteAttributes()
                ////        {
                ////            Size = 5,
                ////            Radius = 5
                ////        },
                ////    });
                ////    if (j < 4)
                ////        j++;
                ////    else
                ////        j = 0;
                ////}
                #endregion

                c.Height = 600;
                c.Animate = true;
                c.Shadow = true;
                c.Frame = true;
                c.StyleSpec = "background:#fff;";
            }
            catch
            { }
        }

        private void FillDriversChart(Ext.Net.Store s, Ext.Net.Chart c, DataTable dt, String CurrencyCode)
        {
            string[] str = new string[dt.Columns.Count - 1];
            int i = 0;
            foreach (DataColumn col in dt.Columns)
            {
                if (col.ColumnName != "Year")
                {
                    str[i] = col.ColumnName;
                    i = i + 1;
                }
            }
            string[] strcat = new string[] { "Year" };

            s.RemoveFields();

            foreach (DataColumn col in dt.Columns)
            {
                s.AddField(new ModelField(col.ColumnName, ModelFieldType.Auto));
            }
            s.DataSource = dt;
            s.DataBind();

            #region Prepare Axis

            AxisLabel AxisL = new AxisLabel();
            AxisL.Renderer.Handler = "return Ext.util.Format.number(value, '0,000');";

            AxisLabel AxisR = new AxisLabel();
            AxisR.Renderer.Handler = "return Ext.util.Format.number(value, '0,000');";

            //Left Axis
            c.Axes.Add(new NumericAxis()
            {
                Title = "Technical Driver (#)",
                Fields = strDrivers,
                MajorTickSteps = 9,
                Grid = true,
                Position = Position.Left,
                GridConfig = new AxisGrid()
                {
                    Odd = new SpriteAttributes()
                    {
                        Opacity = 1,
                        StrokeWidth = 0.5
                    }
                },
                Label = AxisL
            });

            //Right Axis
            if (ReportEconomicDriversCount != 0)
            {
                c.Axes.Add(new NumericAxis()
                {
                    Title = "EconomicDriver (#)",
                    Fields = strEconomicDrivers,
                    MajorTickSteps = 9,
                    Grid = true,
                    Position = Position.Right,
                    GridConfig = new AxisGrid()
                    {
                        Odd = new SpriteAttributes()
                        {
                            Opacity = 1,
                            StrokeWidth = 0.5
                        }
                    },
                    Label = AxisR
                });
            }

            //Bottom Axis
            c.Axes.Add(new CategoryAxis()
            {
                Title = (String)GetGlobalResourceObject("CPM_Resources", "Year"),
                Fields = strcat,
                Position = Position.Bottom,
                Grid = true
            });

            #endregion Prepare Axis

            #region LineSeries (Drivers)

            Ext.Net.SpriteType[] strSpriteTypes = new Ext.Net.SpriteType[] { Ext.Net.SpriteType.Circle, Ext.Net.SpriteType.Cross, Ext.Net.SpriteType.Diamond, Ext.Net.SpriteType.Square, Ext.Net.SpriteType.Triangle };
            int j = 0;

            /** Tecnical Drivers **/
            foreach (string strField in strDrivers)
            {
                ChartTip chrtTip = new ChartTip();
                chrtTip.TrackMouse = true;
                chrtTip.Width = 140;
                chrtTip.StyleSpec = "background:#fff; text-align:center;";
                chrtTip.Height = 20;
                chrtTip.Renderer.Handler = "this.setTitle(storeItem.get('" + strcat[0] + "') + ': ' + String(Ext.util.Format.number(item.value[1], '0,000')));";

                c.Series.Add(new LineSeries()
                {
                    Titles = new string[] { strField + " (Technical)" },
                    XField = strcat,
                    YField = new string[] { strField },
                    Axis = Position.Left,
                    Tips = chrtTip,
                    MarkerConfig = new SpriteAttributes()
                    {
                        Type = strSpriteTypes[j],
                        Size = 4,
                        Radius = 4,
                        StrokeWidth = 0,
                    },
                    HighlightConfig = new SpriteAttributes()
                    {
                        Size = 5,
                        Radius = 5
                    },
                });
                if (j < 4)
                    j++;
                else
                    j = 0;
            }

            /** Economic Drivers **/
            foreach (string strField in strEconomicDrivers)
            {
                ChartTip chrtTip = new ChartTip();
                chrtTip.TrackMouse = true;
                chrtTip.Width = 140;
                chrtTip.StyleSpec = "background:#fff; text-align:center;";
                chrtTip.Height = 20;
                chrtTip.Renderer.Handler = "this.setTitle(storeItem.get('" + strcat[0] + "') + ': ' + String(Ext.util.Format.number(item.value[1], '0,000.00')));";

                c.Series.Add(new LineSeries()
                {
                    Titles = new string[] { strField + " (Economic)" },
                    XField = strcat,
                    YField = new string[] { strField },
                    Axis = Position.Right,
                    Tips = chrtTip,
                    MarkerConfig = new SpriteAttributes()
                    {
                        Type = strSpriteTypes[j],
                        Size = 4,
                        Radius = 4,
                        StrokeWidth = 0,
                    },
                    HighlightConfig = new SpriteAttributes()
                    {
                        Size = 5,
                        Radius = 5
                    },
                });
                if (j < 4)
                    j++;
                else
                    j = 0;
            }
            
            #endregion

            c.Height = ReportCategoriesCount * 75 > 320 ? ReportCategoriesCount * 75 : 320;
            c.Animate = true;
            c.Shadow = true;
            c.Frame = true;
            c.AutoSize = true;
            c.StyleSpec = "background:#fff;";
        }

        private void FillColumnChart(Ext.Net.Store s, Ext.Net.Chart c, DataTable dt, String CurrencyCode)
        {
            string[] str = new string[dt.Columns.Count - 1];
            int i = 0;
            foreach (DataColumn col in dt.Columns)
            {
                if (col.ColumnName != "Year")
                {
                    str[i] = col.ColumnName;
                    i = i + 1;
                }
            }
            string[] strcat = new string[] { "Year" };

            s.RemoveFields();

            foreach (DataColumn col in dt.Columns)
            {
                s.AddField(new ModelField(col.ColumnName, ModelFieldType.Auto));
            }
            s.DataSource = dt;
            s.DataBind();

            AxisLabel AxisL = new AxisLabel();
            AxisL.Renderer.Handler = "return Ext.util.Format.number(value, '0,000.00');";

            NumericAxis numericAxis = new NumericAxis()
            {
                Title = (String)GetGlobalResourceObject("CPM_Resources", "Costs") + " " + CurrencyCode,
                Fields = str,
                MajorTickSteps = 9,
                Grid = true,
                Position = Position.Left,
                GridConfig = new AxisGrid()
                {
                    Odd = new SpriteAttributes()
                    {
                        Opacity = 1,
                        Fill = "#EFEBEF",
                        Stroke = "#EFEBEF",
                        StrokeWidth = 0.5
                    }
                },
                Label = AxisL
            };
            c.Axes.Add(numericAxis);

            CategoryAxis categoryAxis = new CategoryAxis()
            {
                Title = (String)GetGlobalResourceObject("CPM_Resources", "Year"),
                Fields = strcat,
                Position = Position.Bottom
            };
            c.Axes.Add(categoryAxis);

            ChartTip chrtTip = new ChartTip();
            chrtTip.TrackMouse = true;
            chrtTip.Width = 140;
            chrtTip.StyleSpec = "background:#fff; text-align:center;";
            chrtTip.Height = 20;
            chrtTip.Renderer.Handler = "this.setTitle(storeItem.get('" + strcat[0] + "') + ': ' + String(Ext.util.Format.number(item.value[1], '0,000.00')));";

            c.Series.Add(new ColumnSeries()
            {
                Titles = str,
                XField = strcat,
                YField = str,
                Axis = Position.Left,
                Stacked = true,
                Tips = chrtTip,
            });

            c.Height = 600;
            c.Animate = true;
            c.Shadow = true;
            c.Frame = true;
            c.StyleSpec = "background:#fff;";
        }

        protected void txtTo_Select(object sender, DirectEventArgs e)
        {
            this.txtTo.MinValue = Convert.ToInt32(txtFrom.Value);
            this.txtTo.DataBind();
        }

        private Int32 GetIdField(String _Name)
        {
            Int32 intID = 0;

            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dtField = objFields.LoadList("IdModel = " + IdModel + " AND Name = '" + _Name + "'", "ORDER BY Name");

            if (dtField.Rows.Count > 0)
            {
                intID = (Int32)dtField.Rows[0]["IdField"];
            }

            return intID;
        }

        private Int32 GetIdLevel(String _Name)
        {
            Int32 intID = 0;

            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dtLevel = objAggregationLevels.LoadList("IdModel = " + IdModel + " AND Name = '" + _Name + "'", "ORDER BY Name");

            if (dtLevel.Rows.Count > 0)
            {
                intID = (Int32)dtLevel.Rows[0]["IdAggregationLevel"];
            }

            return intID;
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExportOpexTOT = new DataReportObject();
            StoredExportOpexTOT = ExportOpexTOT;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExportOpexTOT);
            objWriter.BuildExportCostVsDriversProjectionReport(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2007, idLanguage);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=ProjectionByCostCategory.xlsx");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        private void DisableParameterSelection()
        {
            this.pnlExportToolbar.Hidden = false;
            this.pnlReportToolbar.Hidden = true;
            this.ddlLevels.ReadOnly = true;
            this.ddlField.ReadOnly = true;
            this.ddlStructure.ReadOnly = true;
            this.txtFrom.ReadOnly = true;
            this.txtTo.ReadOnly = true;
            this.ddlScenarioTechnical.ReadOnly = true;
            this.ddlScenarioEconomic.ReadOnly = true;
            this.ddlCurrency.ReadOnly = true;
            this.ddlTerm.ReadOnly = true;
            this.pnlMainChartPanel.Hidden = false;
            this.ddlCostType.ReadOnly=true;
            this.ddlOperationType.ReadOnly=true;
            this.ddlFactor.ReadOnly = true;
            this.ddlCostCategory.ReadOnly = true;
            this.ddlFilterEconomicDriver.ReadOnly = true;
            this.ddlFilterTechnicalDriver1.ReadOnly = true;
            this.PanelDriversChartPanel.Hidden = false;
        }

        //private System.Drawing.Bitmap ChartImage(Ext.Net.Chart c)
        //{
        ////    HttpUtility.HtmlEncode(c.CallSurface)
        ////    Ext.Net.Utilities
        ////    var svg = Ext.htmlEncode(Ext.draw.engine.SvgExporter.generate(btn.up('panel').down('chart').surface));
        //}

        private void SavePNG(Stream SaveStream, string SVGDoc)
        {
            //XmlDocument xd = new XmlDocument();
            //xd.XmlResolver = null;
            //xd.LoadXml(SVGDoc);
            //var svgGraph = Svg.SvgDocument.Open(xd);
            //svgGraph.Draw().Save(SaveStream, System.Drawing.Imaging.ImageFormat.Png);
        }

        public void ModelCheckRecalcModuleReport()
        {
            clsModels objModelRecalc = new clsModels();
            objModelRecalc.IdModel = IdModel;
            objModelRecalc.CheckRecalcModuleParameter(objApplication.MaxRelationLevels);
            objModelRecalc.CheckRecalcModuleAllocation();
            objModelRecalc.CheckRecalcModuleTechnical();
            objModelRecalc.CheckRecalcModuleEconomic();
            objModelRecalc = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = (String)GetGlobalResourceObject("CPM_Resources", "CostVSDriversProjection");
            this.btnReset.Text = modMain.strReportsModuleReset;
            this.btnSaveExcel.Text = modMain.strReportsModuleExportReportDataToExcel;
            this.lblLevels.Text = modMain.strReportsModuleAggregationLevel;
            this.lblField.Text = modMain.strTechnicalModuleField;
            this.lblStructure.Text = modMain.strReportsModuleCostStructure;
            this.lblFrom.Text = modMain.strReportsModuleFrom;
            this.lblTo.Text = modMain.strReportsModuleTo;
            this.lblScenarioTechnical.Text = modMain.strReportsModuleTechnicalScenario;
            this.lblScenarioEconomic.Text = modMain.strReportsModuleEconomicScenario;
            this.lblCurrency.Text = modMain.strReportsModuleCurrency;
            this.lblTerm.Text = modMain.strReportsModuleTerm;
            this.btnRun.Text = (String)GetGlobalResourceObject("CPM_Resources", "Run Report"); 
            this.ReportTitle01.Title = (String)GetGlobalResourceObject("CPM_Resources", "TotalProjection");
            this.lblOperationType.Text = modMain.strReportsModuleFormOperationType + ":";
            this.lblCostType.Text = modMain.strReportsModuleFormTypeCost + ":";
            this.btnDirectExcel.Text = modMain.strReportsModuleExportReportDataToExcel;
            this.lblFilterTechnicalDriver1.Text = (String)GetGlobalResourceObject("CPM_Resources", "Technical Driver") + ":";
            this.lblFilterEconomicDriver.Text=(String)GetGlobalResourceObject("CPM_Resources", "Economic Driver") + ":";
            this.lblCostCategory.Text = (String)GetGlobalResourceObject("CPM_Resources", "Cost Category") + ":";
        }

        #endregion

    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="reportmodule.aspx.cs" Inherits="CPM._reportmodule" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_iconxl.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Border="false" Layout="FormLayout" OverflowY="Scroll">
        <Items>
            <ext:Panel ID="pnlParameters1" runat="server" Title="Reports Module" IconCls="icon-reports_16" BodyPadding="10" Border="false" Layout="Column">
                <Items>
                    <ext:FieldSet ID="FieldSet1"  Title="CostProjectionReports"  runat="server" 
                        Flex="1" 
                        Layout="AnchorLayout"
                        Cls="buttongroup-bordertop"
                        ColumnWidth="1">
                        <Defaults>
                            <ext:Parameter Name="HideEmptyLabel" Value="false" Mode="Raw" />
                        </Defaults>
                        <Items>
                            <ext:Button ID="btnDriverReport" runat="server" Disabled="true" Cls="icon-driverprofiles_48" Text="Technical Drivers Forecast (NOT USED)" IconAlign="Top" Scale="Large" Visible="false">
                                <DirectEvents>
                                    <Click OnEvent="btnDriverReport_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnKPIReport" runat="server" Disabled="true" Cls="icon-kpitarget_48" Text="Key Performance<br>Indicators Estimates (NOT USED)" IconAlign="Top" Scale="Large" visible="false">
                                <DirectEvents>
                                    <Click OnEvent="btnKPIReport_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnOperationalMarginsProjection" runat="server" Disabled="true" Cls="icon-opex_48" Text="OperationalMarginsProjection" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnOperationalMarginsProjection_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnCostvsDriversProjection" runat="server" Disabled="true" Cls="icon-opex_48" Text="CostVSDriversProjection" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnCostvsDriversProjection_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnOpexReport" runat="server" Disabled="true" Cls="icon-opex_48" Text="Projection By Cost Category" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnOpexReport_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnOpexReportDetailed" runat="server" Disabled="true" Cls="icon-opex_48" Text="Projection By Case" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnOpexReportDetailed_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnMultiFieldsProjection" runat="server" Disabled="true" Cls="icon-opex_48" Text="Multiple Fields Projection" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnMultiFieldsProjection_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:FieldSet>
                    <ext:FieldSet ID="FieldSet2"  Title="CostBenchmarkingReports"  runat="server" 
                        Flex="1" 
                        Layout="AnchorLayout"
                        Cls="buttongroup-bordertop"
                        ColumnWidth="1">
                        <Defaults>
                            <ext:Parameter Name="HideEmptyLabel" Value="false" Mode="Raw" />
                        </Defaults>
                        <Items>
                            <ext:Button ID="btnCostBenchmarking" runat="server" Disabled="true" Cls="icon-opex_48" Text="CostBenchmarking" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnCostBenchmarking_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBaseCostTechnicalModule" runat="server" Disabled="true" Cls="icon-basecostrates_48" Text="Base Rates By Cost Category" IconAlign="Top" Scale="Large" Visible="false">
                                <DirectEvents>
                                    <Click OnEvent="btnBaseCostTechnicalModule_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBaseRatesByDrivers" runat="server" Disabled="false" Cls="icon-basecostrates_48" Text="Base Rates By Drivers" IconAlign="Top" Scale="Large" Visible="false">
                                <DirectEvents>
                                    <Click OnEvent="btnBaseRatesByDrivers_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:FieldSet>
                    <ext:FieldSet ID="FieldSet3"  Title="3. Historical Base Cost Reports"  runat="server" 
                        Flex="1" 
                        Layout="AnchorLayout"
                        Cls="buttongroup-bordertop"
                        ColumnWidth="1"
                        Visible="false">
                        <Defaults>
                            <ext:Parameter Name="HideEmptyLabel" Value="false" Mode="Raw" />
                        </Defaults>
                        <Items>
                            <ext:Button ID="btnBaseCostByField" runat="server" Disabled="true" Cls="icon-clientcostfieldclient_48" Text="Internal Base Cost References" IconAlign="Top" Scale="Large" Visible="false">
                                <DirectEvents>
                                    <Click OnEvent="btnBaseCostByField_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnBaseCostByFieldZiff" runat="server" Disabled="true" Cls="icon-clientcostfieldziff_48" Text="Base Cost By Field<br>(Ziff/Third-party Categories) (NOT USED)" IconAlign="Top" Scale="Large" Visible="false">
                                <DirectEvents>
                                    <Click OnEvent="btnBaseCostByFieldZiff_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnZiffBaseCost" runat="server" Disabled="true" Cls="icon-ziffbasecost_48" Text="External Base Cost References" IconAlign="Top" Scale="Large" Visible="false">
                                <DirectEvents>
                                    <Click OnEvent="btnZiffBaseCost_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:FieldSet>
                </Items>
            </ext:Panel>  
        </Items>
    </ext:Viewport>
</body>
</html>
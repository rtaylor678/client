﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using Ext.Net.Utilities;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Diagnostics;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CD;
using DataClass;
using System.Web.Script.Serialization;

namespace CPM
{
    public partial class _operationalmarginsprojection : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdActivity { get { return (Int32)Session["IdActivity"]; } set { Session["IdActivity"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }
        private Boolean PageIsLoaded { get; set; }
        private DataReportObject ExportOpexTOT { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }
        private DataReportObject ExportOpexBL { get { return (DataReportObject)Session["Export02"]; } set { Session["Export02"] = value; } }
        private DataReportObject ExportOpexBO { get { return (DataReportObject)Session["Export03"]; } set { Session["Export03"] = value; } }
        private Int32 ReportFieldCount = 0;
        private String ChartCurrencyCode = "";

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (GetProject())
            {
                this.lblScenarioEconomic.Hide();
                this.ddlScenarioEconomic.Hide();
                this.lblTerm.Hide();
                this.ddlTerm.Hide();
            }

            if (!X.IsAjaxRequest)
            {
                LoadLabel();
                Session["Export01"] = null;
                Session["Export02"] = null;
                Session["Export03"] = null;
                PageIsLoaded = false;
                IdActivity = 0;
                this.ddlStructure.SelectedItem.Index = 0;
                this.ddlTerm.SelectedItem.Index = 1;
                this.ddlScenarioTechnical.SelectedItem.Index = 0;
                this.ddlScenarioEconomic.SelectedItem.Index = 0;
                this.ddlOperationType.SelectedItem.Index = 0;
                this.ddlCostType.SelectedItem.Index = 0;
                this.ddlFactor.SelectedItem.Index = 1;
                this.FromTo();
                DataTable dt = new DataTable();
                ArrayList datColumnOpex = new ArrayList();
                this.FillGrid(this.storeOpexProjectionOperationalMargins, this.grdOpexProjectionOperationalMargins, dt, ref datColumnOpex, false, null);
                PageIsLoaded = true;
            }
        }

        protected void ddlLevels_Select(object sender, DirectEventArgs e)
        {
            this.ddlField.Reset();
            this.storeField.Reload();
            this.ddlOperationType.Reset();
            this.storeOperationType.Reload();
        }

        protected void ddlField_Select(object sender, DirectEventArgs e)
        {
            this.ddlOperationType.Reset();
            this.storeOperationType.Reload();
        }

        protected void ddlCostCategory_Select(object sender, DirectEventArgs e)
        {
            bool nonAllCC = false;
            foreach (Ext.Net.ListItem costCategory in this.ddlCostCategory.SelectedItems)
            {
                if (Convert.ToInt32(costCategory.Value) != 0)
                    nonAllCC = true;
            }

            if (nonAllCC)
                ddlCostCategory.DeselectItem(0);
        }

        protected void btnReset_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/report/operationalmarginsprojection.aspx");
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            btnSaveExcelDirect_Click(null, null);
        }

        protected void btnSaveExcelDirect_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.ModelCheckRecalcModuleReport();
                DisableParameterSelection();

                DataClass.clsModels objModels = new DataClass.clsModels();

                // Check that at least one field has been selected, if none default to 'All'
                if (this.ddlField.SelectedItems.Count == 0)
                    ddlField.SelectedItems.Add(new Ext.Net.ListItem("0"));

                string fields = "";
                foreach (Ext.Net.ListItem field in this.ddlField.SelectedItems)
                {
                    fields += field.Value + ",";
                }

                //Need to remove trailing "," from the fields list
                fields = fields.ReplaceLastInstanceOf(",", "");

                Int32 intCurrency = Convert.ToInt32(ddlCurrency.Value);
                Int32 intTerm = Convert.ToInt32(ddlTerm.Value);
                Int32 intTechnicalScenario = Convert.ToInt32(ddlScenarioTechnical.Value);
                Int32 intEconomicScenario = 0;
                Int32 intTypeOPeration = Convert.ToInt32(ddlOperationType.Value);
                String strCurrencyCode = "";
                clsCurrencies objCurrencies = new clsCurrencies();
                objCurrencies.IdCurrency = intCurrency;
                objCurrencies.loadObject();
                strCurrencyCode = "(" + objCurrencies.Code + ")";

                if (this.ddlScenarioEconomic.Hidden == false)
                {
                    intEconomicScenario = Convert.ToInt32(ddlScenarioEconomic.Value);
                }
                Int32 intFrom = Convert.ToInt32(txtFrom.Value);
                Int32 intTo = Convert.ToInt32(txtTo.Value);

                Int32 intPriceScenario = Convert.ToInt32(ddlFilterNetPriceScenario.Value);

                DataSet ds = objModels.LoadOperationalMarginsOpexProjection(IdModel, fields, intFrom, intTo, intTechnicalScenario, intEconomicScenario, intCurrency, intTerm, intTypeOPeration, intPriceScenario);

                DataTable dtExport = new DataTable();
                dtExport = ds.Tables[0];

                ArrayList datColumnExport = new ArrayList();
                ArrayList datColumnExclude = new ArrayList();
                datColumnExclude.Insert(0, "SortOrder");

                String strSelectedParameters = lblLevels.Text + " '" + ddlLevels.SelectedItem.Text + "', " +
                                               lblField.Text + " '" + ddlField.SelectedItem.Text + "', " +
                                               lblStructure.Text + " '" + ddlStructure.SelectedItem.Text + "', " +
                                               lblFrom.Text + " '" + txtFrom.Text + "', " +
                                               lblTo.Text + " '" + txtTo.Text + "', " +
                                               lblScenarioTechnical.Text + " '" + ddlScenarioTechnical.SelectedItem.Text + "', " +
                                               lblScenarioEconomic.Text + " '" + ddlScenarioEconomic.SelectedItem.Text + "', " +
                                               lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "', " +
                                               lblTerm.Text + " '" + ddlTerm.SelectedItem.Text + "', " +
                                               lblOperationType.Text + " '" + ddlOperationType.SelectedItem.Text + "', " +
                                               lblCostType.Text + " '" + ddlCostType.SelectedItem.Text + "', " +
                                               lblFactor.Text + " '" + ddlFactor.SelectedItem.Text + "'";
                ExportOpexTOT = new DataReportObject(dtExport, "OperationalMarginProjection", "Operational Margin Projection", strSelectedParameters, datColumnExclude, "FieldName,SortOrder", datColumnExport, null);

                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Report Generated Successfully!"), IconCls = "icon-accept", Clear2 = false });
                X.Mask.Hide();

                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                if (strMessage.Contains("Timeout"))
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = strMessage.ToString(), IconCls = "icon-exclamation", Clear2 = false });
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select all filters please"), IconCls = "icon-exclamation", Clear2 = false });
                }
            }
        }

        protected void btnRun_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.ModelCheckRecalcModuleReport();
                DisableParameterSelection();

                DataClass.clsModels objModels = new DataClass.clsModels();

                // Check that at least one field has been selected, if none default to 'All'
                if (this.ddlField.SelectedItems.Count == 0)
                    ddlField.SelectedItems.Add(new Ext.Net.ListItem("0"));

                string fields = "";
                foreach (Ext.Net.ListItem field in this.ddlField.SelectedItems)
                {
                    fields += field.Value + ",";
                }
                
                //Need to remove trailing "," from the fields list
                fields = fields.ReplaceLastInstanceOf(",", "");

                Int32 intCurrency = Convert.ToInt32(ddlCurrency.Value);
                Int32 intTerm = Convert.ToInt32(ddlTerm.Value);
                Int32 intTechnicalScenario = Convert.ToInt32(ddlScenarioTechnical.Value);
                Int32 intEconomicScenario = 0;
                Int32 intTypeOPeration = Convert.ToInt32(ddlOperationType.Value);
                String strCurrencyCode = "";
                clsCurrencies objCurrencies = new clsCurrencies();
                objCurrencies.IdCurrency = intCurrency;
                objCurrencies.loadObject();
                strCurrencyCode = "(" + objCurrencies.Code + ")";
                ChartCurrencyCode = objCurrencies.Code;

                if (this.ddlScenarioEconomic.Hidden == false)
                {
                    intEconomicScenario = Convert.ToInt32(ddlScenarioEconomic.Value);
                }
                Int32 intFrom = Convert.ToInt32(txtFrom.Value);
                Int32 intTo = Convert.ToInt32(txtTo.Value);

                Int32 intPriceScenario = Convert.ToInt32(ddlFilterNetPriceScenario.Value);

                DataSet ds = objModels.LoadOperationalMarginsOpexProjection(IdModel, fields, intFrom, intTo, intTechnicalScenario, intEconomicScenario, intCurrency, intTerm, intTypeOPeration, intPriceScenario);

                DataTable dt = new DataTable();
                dt = ds.Tables[0];
                DataTable dtChart = new DataTable();
                dtChart = ds.Tables[1];
                DataTable dtStats = new DataTable();
                dtStats = ds.Tables[2];

                if (dtStats != null)
                {
                    if (dtStats.Rows.Count > 0)
                    {
                        ReportFieldCount = Convert.ToInt32(dtStats.Rows[0]["FieldCount"].ToString());
                    }
                }
                dtStats = null;

                ArrayList datColumnOpex = new ArrayList();
                ArrayList datColumnExport = new ArrayList();
                ArrayList datColumnExclude = new ArrayList();

                datColumnExclude.Insert(0,"SortOrder");

                this.FillGrid(this.storeOpexProjectionOperationalMargins, this.grdOpexProjectionOperationalMargins, dt, ref datColumnOpex, false, strCurrencyCode);
                this.FillChart(this.storeChart, this.chtChart, dtChart, strCurrencyCode);
                pnlChartObject.Render();

                String strSelectedParameters = lblLevels.Text + " '" + ddlLevels.SelectedItem.Text + "', " +
                                               lblField.Text + " '" + fields + "', " +
                                               lblFrom.Text + " '" + txtFrom.Text + "', " +
                                               lblTo.Text + " '" + txtTo.Text + "', " +
                                               lblScenarioTechnical.Text + " '" + ddlScenarioTechnical.SelectedItem.Text + "', " +
                                               lblScenarioEconomic.Text + " '" + ddlScenarioEconomic.SelectedItem.Text + "', " +
                                               lblFilterNetPriceScenario.Text + " '" + ddlFilterNetPriceScenario.SelectedItem.Text + "', " +
                                               lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "', " +
                                               lblTerm.Text + " '" + ddlTerm.SelectedItem.Text + "', " +
                                               lblOperationType.Text + " '" + ddlOperationType.SelectedItem.Text + "'";
                ExportOpexTOT = new DataReportObject(dt, "OperationalMarginProjection", "Operational Margin Projection", strSelectedParameters, datColumnExclude, "FieldName,SortOrder", datColumnExport, null);
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Report Generated Successfully!"), IconCls = "icon-accept", Clear2 = false });
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                if (strMessage.Contains("Timeout"))
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = strMessage.ToString(), IconCls = "icon-exclamation", Clear2 = false });
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select all filters please"), IconCls = "icon-exclamation", Clear2 = false });
                }
            }
        }

        protected void StoreLevel_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dt = objAggregationLevels.LoadList("IdModel = " + IdModel, "ORDER BY Name");

            DataRow drLevel = dt.NewRow();
            drLevel["IdAggregationLevel"] = 0;
            drLevel["Name"] = "- All -";
            dt.Rows.Add(drLevel);
            dt.AcceptChanges();
            dt.DefaultView.Sort = "Name";
            DataTable dtSort = dt.DefaultView.ToTable();

            this.storeLevels.DataSource = dtSort;
            this.storeLevels.DataBind();

            this.ddlLevels.Value = 0;

            this.storeLevels.DataBind();
        }

        protected void StoreCurr_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsCurrencies objCurrencies = new DataClass.clsCurrencies();
            this.storeBase.DataSource = objCurrencies.LoadList("IdModel IN (" + IdModel + ") AND Output=1", "ORDER BY BaseCurrency DESC,Name");
            this.storeBase.DataBind();
            DataTable dt = objCurrencies.LoadList("BaseCurrency = 1 AND IdModel = " + IdModel, "");
            if (dt.Rows.Count > 0)
            {
                ddlCurrency.Value = dt.Rows[0]["IdCurrency"];
            }
            else
            {
                ddlCurrency.SelectedItem.Index = 0;
            }
        }

        protected void StoreField_ReadData(object sender, StoreReadDataEventArgs e)
        {

            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dtField = new DataTable();
            if ((ddlLevels.Value == null) || (ddlLevels.Value.ToString() == "0") || (ddlLevels.Value.ToString() == ""))
            {
                dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
            }
            else
            {
                dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
            }
            //DataRow drField = dtField.NewRow();
            //drField["IdField"] = 0;
            //drField["Name"] = "- All -";//drField["CodeName"] = "- All -"; //
            //dtField.Rows.Add(drField);
            //dtField.AcceptChanges();
            //dtField.DefaultView.Sort = "Name";
            //DataTable dtFieldSort = dtField.DefaultView.ToTable();

            //this.storeField.DataSource = dtFieldSort;
            this.storeField.DataSource = dtField;
            this.storeField.DataBind();

            this.ddlField.Value = 0;
        }

        protected void StoreCostType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsCostType objCostTypes = new DataClass.clsCostType();
            DataTable dt = objCostTypes.LoadList("", "ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                ddlCostType.Value = dt.Rows[0]["IdCostType"];
            }
            else
            {
                ddlCostType.SelectedItem.Index = 0;
            }
            foreach (DataRow row in dt.Rows)
            {
                if (idLanguage == 2)
                {
                    row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
                }
            }
            this.storeCostType.DataSource = dt;
            this.storeCostType.DataBind();
        }

        protected void StoreTechnicalScenarioType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsScenarios objScenarioTypes = new DataClass.clsScenarios();
            DataTable dt = objScenarioTypes.LoadList("", "ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                ddlCostType.Value = dt.Rows[0]["IdScenarioType"];
            }
            else
            {
                ddlCostType.SelectedItem.Index = 0;
            }
            foreach (DataRow row in dt.Rows)
            {
                if (idLanguage == 2)
                {
                    row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
                }
            }
            this.storeTechnicalScenarioType.DataSource = dt;
            this.storeTechnicalScenarioType.DataBind();
        }

        protected void StoreEconomicScenarioType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsScenarios objScenarioTypes = new DataClass.clsScenarios();
            DataTable dt = objScenarioTypes.LoadList("", "ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                ddlCostType.Value = dt.Rows[0]["IdScenarioType"];
            }
            else
            {
                ddlCostType.SelectedItem.Index = 0;
            }
            foreach (DataRow row in dt.Rows)
            {
                if (idLanguage == 2)
                {
                    row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
                }
            }
            this.storeEconomicScenarioType.DataSource = dt;
            this.storeEconomicScenarioType.DataBind();
        }

        protected void StoreOperationType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsOperationType objoperationTypes = new DataClass.clsOperationType();
            DataTable dtOperation = new DataTable();

            if (((ddlLevels.Value == null) || (ddlLevels.Value.ToString() == "0") || (ddlLevels.Value.ToString() == "")) && ((ddlField.Value == null) || (ddlField.Value.ToString() == "0") || (ddlField.Value.ToString() == "")))
            {
                dtOperation = objoperationTypes.LoadList("IdModel=" + IdModel, "");//"[vListTypesOperation].IdModel=" + IdModel, "");

                DataRow drOperation = dtOperation.NewRow();
                drOperation["IdTypeOperation"] = 0;
                drOperation["Name"] = "- All -";
                dtOperation.Rows.Add(drOperation);
                dtOperation.AcceptChanges();
                dtOperation.DefaultView.Sort = "Name";

                ddlOperationType.Value = 0;
            }
            else if (Convert.ToInt32(ddlLevels.Value) > 0)
            {
                dtOperation = objoperationTypes.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
                if (dtOperation.Rows.Count > 0)
                {
                    ddlOperationType.Value = dtOperation.Rows[0]["IdTypeOperation"];
                }
            }
            else
            {
                dtOperation = objoperationTypes.LoadList("IdModel=" + IdModel + " AND IdField = " + ddlField.Value, " Order by Name");//"[vListTypesOperation].IdModel=" + IdModel + " AND tblFields.IdField = " + ddlField.Value, " Order by Name");
                if (dtOperation.Rows.Count > 0)
                {
                    ddlOperationType.Value = dtOperation.Rows[0]["IdTypeOperation"];
                }
            }

            DataTable dtOperationSort = dtOperation.DefaultView.ToTable();

            this.storeOperationType.DataSource = dtOperationSort;
            this.storeOperationType.DataBind();
        }

        protected void StoreCostCategory_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsZiffAccount objZiffAccount = new DataClass.clsZiffAccount();
            DataTable dtZiffAccount = objZiffAccount.LoadComboBox("IdModel = " + IdModel + " AND Parent IS NOT NULL", " ORDER BY Parent");
            DataRow drAccount = dtZiffAccount.NewRow();
            drAccount["IdZiffAccount"] = 0;
            drAccount["Name"] = "- All -";
            drAccount["Code"] = "- All -";
            drAccount["Parent"] = 0;
            dtZiffAccount.Rows.Add(drAccount);
            dtZiffAccount.AcceptChanges();
            dtZiffAccount.DefaultView.Sort = "Parent";
            this.storeCostCategory.DataSource = dtZiffAccount;
            this.storeCostCategory.DataBind();
            ddlCostCategory.Value = 0;
        }

        protected void StoreFilterNetPriceScenario_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsPriceScenarios objPriceScenarios = new DataClass.clsPriceScenarios();
            this.storeFilterNetPriceScenario.DataSource = objPriceScenarios.LoadComboBox("IdModel = " + IdModel, "ORDER BY Name");
            this.storeFilterNetPriceScenario.DataBind();
        }

        protected void ddlFilterNetPriceScenario_Select(object sender, DirectEventArgs e)
        {
        }

        #endregion

        #region Methods

        public Boolean GetProject()
        {
            Boolean Value = false;
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("Level IN (1,2) AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = true;
            }

            return Value;
        }

        protected void ClearReport()
        {
            if (PageIsLoaded)
            {
                this.storeOpexProjectionOperationalMargins.RemoveAll();
                this.pnlChartObject.ClearContent();
            }
        }

        protected void FromTo()
        {
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                this.txtFrom.Value = (Int32)dt.Rows[0]["BaseYear"];
                this.txtTo.Value = Convert.ToInt32(dt.Rows[0]["BaseYear"]) + Convert.ToInt32(dt.Rows[0]["IdProjection"]);

                this.txtFrom.MinValue = (Int32)dt.Rows[0]["BaseYear"];
                this.txtFrom.MaxValue = (Int32)dt.Rows[0]["BaseYear"] + (Int32)dt.Rows[0]["IdProjection"];

                this.txtFrom.MinValue = (Int32)dt.Rows[0]["BaseYear"];
                this.txtTo.MaxValue = Convert.ToInt32(dt.Rows[0]["BaseYear"]) + Convert.ToInt32(dt.Rows[0]["IdProjection"]);

                this.txtFrom.DataBind();
                this.txtTo.DataBind();

            }
        }

        protected Int32 GetIdOperation(String _TypeOperation)
        {
            Int32 Value = 0;
            DataClass.clsTypesOperation objTypesOperation = new DataClass.clsTypesOperation();
            DataTable dt = objTypesOperation.LoadList("Name = '" + _TypeOperation + "'", "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdTypeOperation"];
            }
            return Value;
        }

        private void FillGrid(Ext.Net.Store s, Ext.Net.GridPanel g, DataTable dt, ref ArrayList colVisible, Boolean IsBLBO, String CurrencyCode)
        {
            s.RemoveFields();

            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "SortOrder" || c.ColumnName == "FieldName" || c.ColumnName == "Unit")
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Auto));
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Float));
                }
            }
            g.Features.Clear();

            if (!IsBLBO)
            {
                GroupingSummary gsum = new GroupingSummary();
                gsum.GroupHeaderTplString = "{name}";
                gsum.HideGroupedHeader = true;
                gsum.ShowSummaryRow = false;    //Hide the grouping Summary Totals
                g.Features.Add(gsum);
            }

            GridFilters f = new GridFilters();
            f.Local = true;
            g.Features.Add(f);

            Summary smry = new Summary();
            smry.Dock = new SummaryDock();
            smry.ID = "Summary" + g.ID.ToString();
            smry.ShowSummaryRow = false;    //Hide the Grand Summary Totals

            g.Features.Add(smry);

            foreach (DataColumn c in dt.Columns)
            {
                if (!c.ColumnName.Contains("Id"))
                {
                    if (c.ColumnName == "SortOrder" || c.ColumnName == "FieldName" || c.ColumnName == "Unit")
                    {
                        StringFilter sFilter = new StringFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                    else
                    {
                        NumericFilter sFilter = new NumericFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                }
            }

            Column col = new Column();
            col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Years");
            SummaryColumn colSum = new SummaryColumn();
            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "SortOrder" || c.ColumnName == "FieldName" || c.ColumnName == "Unit")
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        if (c.ColumnName != "SortOrder" && c.ColumnName != "FieldName")
                        {
                            colSum = new SummaryColumn();
                            colSum.DataIndex = c.ColumnName;
                            colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", "Unit");
                            colSum.Locked = true;
                            colVisible.Add(new DataClass.DataTableColumnDisplay(c.ColumnName, colSum.Text));
                            colSum.SummaryType = Ext.Net.SummaryType.Count;
                            colSum.TdCls = "task";
                            colSum.Sortable = true;
                            colSum.Groupable = true;
                            colSum.Width = 250;
                            colSum.SummaryRenderer.Handler = "return '(' + value + ' Total)';";
                            colSum.Renderer.Fn = "formatCol";
                            colSum.MenuDisabled = false;
                            colSum.Align = Alignment.Left;
                            g.ColumnModel.Columns.Add(colSum);
                        }
                    }
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        colSum = new SummaryColumn();
                        colSum.DataIndex = c.ColumnName;
                        colSum.Text = c.ColumnName;
                        colVisible.Add(new DataClass.DataTableColumnDisplay(c.ColumnName, colSum.Text));
                        colSum.SummaryType = Ext.Net.SummaryType.Sum;
                        colSum.Renderer.Fn = "formatColData";
                        colSum.Align = Alignment.Right;
                        colSum.MinWidth = 80;
                        col.Columns.Add(colSum);
                    }
                }
            }

            g.ColumnModel.Columns.Add(col);

            s.DataSource = dt;
            s.GroupField = "FieldName";
            
            if (X.IsAjaxRequest)
            {
                g.Reconfigure();
            }
            s.DataBind();
        }

        private void FillChart(Ext.Net.Store s, Ext.Net.Chart c, DataTable dt, String CurrencyCode)
        {
            try
            {
                string[] str = new string[dt.Columns.Count - 1];
                int i = 0;
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName != "Year")
                    {
                        str[i] = col.ColumnName;
                        i = i + 1;
                    }
                }
                string[] strcat = new string[] { "Year" };

                s.RemoveFields();

                foreach (DataColumn col in dt.Columns)
                {
                    s.AddField(new ModelField(col.ColumnName, ModelFieldType.Auto));
                }
                s.DataSource = dt;
                s.DataBind();


                #region Prepare Axis

                AxisLabel AxisL = new AxisLabel();
                AxisL.Renderer.Handler = "return Ext.util.Format.number(value, '0,000.00');";
                //AxisL.Renderer.Fn = "formatAxis";

                AxisLabel AxisB = new AxisLabel();
                AxisB.Renderer.Handler = "return Ext.util.Format.number(value, '0000');";
                AxisB.Rotate = new RotateAttribute()
                {
                    Degrees = 90,
                };
                //Left Axis
                c.Axes.Add(new NumericAxis()
                {
                    Title = "($"+ChartCurrencyCode+"/BOE)",
                    Fields = str,
                    MajorTickSteps = 10,
                    Grid = true,
                    Position = Position.Left,
                    GridConfig = new AxisGrid()
                    {
                        Odd = new SpriteAttributes()
                        {
                            Opacity = 1,
                            StrokeWidth = 0.5
                        }
                    },
                    Label = AxisL
                });

                //Bottom Axis
                c.Axes.Add(new CategoryAxis()
                {
                    Title = (String)GetGlobalResourceObject("CPM_Resources", "Year"),
                    Fields = strcat,
                    Position = Position.Bottom,
                    Grid = true,
                    Label=AxisB,
                });

                #endregion Prepare Axis

                #region ColumnSeries (Operating Unit Cost)

                String[] ucFields = new String[ReportFieldCount];
                String[] ucFieldTitles = new String[ReportFieldCount];
                int l = 0;
                foreach (string strField in str)
                {
                    if (strField.IndexOf("_UC", 0) > 0)
                    {
                        ucFields[l] = strField;
                        ucFieldTitles[l] = strField.Substring(0, strField.IndexOf("_UC", 0)) + " (Unit Cost)";
                        l++;
                    }
                }

                ChartTip chrtTip2 = new ChartTip();
                chrtTip2.TrackMouse = true;
                chrtTip2.Width = 140;
                chrtTip2.StyleSpec = "background:#fff; text-align:center;";
                chrtTip2.Height = 20;
                chrtTip2.Renderer.Handler = "this.setTitle(storeItem.get('" + strcat[0] + "') + ': ' + String(Ext.util.Format.number(item.value[1], '0,000.00')));";

                c.Series.Add(new ColumnSeries()
                {
                    Titles = ucFieldTitles,
                    XField = strcat,
                    YField = ucFields,
                    Axis = Position.Left,
                    Tips = chrtTip2,
                });

                #endregion

                #region LineSeries (Unit Cost and Operational Margin)

                Ext.Net.SpriteType[] strSpriteTypes = new Ext.Net.SpriteType[] { Ext.Net.SpriteType.Circle, Ext.Net.SpriteType.Cross, Ext.Net.SpriteType.Diamond, Ext.Net.SpriteType.Square, Ext.Net.SpriteType.Triangle };
                int j = 0;

                /** Operational Margin **/
                foreach (string strField in str)
                {
                    if (strField.IndexOf("_OM", 0) > 0)
                    {
                        ChartTip chrtTip = new ChartTip();
                        chrtTip.TrackMouse = true;
                        chrtTip.Width = 140;
                        chrtTip.StyleSpec = "background:#fff; text-align:center;";
                        chrtTip.Height = 20;
                        chrtTip.Renderer.Handler = "this.setTitle(storeItem.get('" + strcat[0] + "') + ': ' + String(Ext.util.Format.number(item.value[1], '0,000.00')));";

                        c.Series.Add(new LineSeries()
                        {
                            Titles = new string[] { strField.Substring(0, strField.IndexOf("_OM", 0)) + " (Operational Margin)" },
                            XField = strcat,
                            YField = new string[] { strField },
                            Axis = Position.Left,
                            Tips = chrtTip,
                            MarkerConfig = new SpriteAttributes()
                            {
                                Type = strSpriteTypes[j],
                                Size = 4,
                                Radius = 4,
                                StrokeWidth = 0,
                            },
                            HighlightConfig = new SpriteAttributes()
                            {
                                Size = 5,
                                Radius = 5
                            },
                        });
                        if (j < 4)
                            j++;
                        else
                            j = 0;
                    }
                }

                /** Net Price **/
                foreach (string strField in str)
                {
                    if (strField.IndexOf("_NP", 0) > 0)
                    {
                        ChartTip chrtTip = new ChartTip();
                        chrtTip.TrackMouse = true;
                        chrtTip.Width = 140;
                        chrtTip.StyleSpec = "background:#fff; text-align:center;";
                        chrtTip.Height = 20;
                        chrtTip.Renderer.Handler = "this.setTitle(storeItem.get('" + strcat[0] + "') + ': ' + String(Ext.util.Format.number(item.value[1], '0,000.00')));";

                        c.Series.Add(new LineSeries()
                        {
                            Titles = new string[] { strField.Substring(0, strField.IndexOf("_NP", 0)) + " (Net Revenue)" },
                            XField = strcat,
                            YField = new string[] { strField },
                            Axis = Position.Left,
                            Tips = chrtTip,
                            MarkerConfig = new SpriteAttributes()
                            {
                                Type = strSpriteTypes[j],
                                Size = 4,
                                Radius = 4,
                                StrokeWidth = 0,
                            },
                            HighlightConfig = new SpriteAttributes()
                            {
                                Size = 5,
                                Radius = 5
                            },
                        });
                        if (j < 4)
                            j++;
                        else
                            j = 0;
                    }
                }

                #endregion

                c.Height = 600;
                c.Animate = true;
                c.Shadow = true;
                c.Frame = true;
                c.StyleSpec = "background:#fff;";
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
            }
        }

        public static DataTable Pivot(IDataReader dataValues, string keyColumn, string pivotNameColumn, string pivotValueColumn)
        {

            DataTable tmp = new DataTable();
            DataRow r;
            int i, pValIndex, pNameIndex;
            string s;

            // Add non-pivot columns to the data table: 
            pValIndex = dataValues.GetOrdinal(pivotValueColumn);
            pNameIndex = dataValues.GetOrdinal(pivotNameColumn);

            // Loop through columns 
            for (i = 0; i <= dataValues.FieldCount - 1; i++)
            {
                if (i != pValIndex && i != pNameIndex)
                {
                    DataColumn dc = new DataColumn(dataValues.GetName(i), dataValues.GetFieldType(i));
                    tmp.Columns.Add(dc);

                    // Add key column 
                    if (dc.ColumnName == keyColumn)
                        tmp.PrimaryKey = new DataColumn[] { dc };
                }
            }

            // now, fill up the table with the data 
            while (dataValues.Read())
            {
                // assign the pivot values to the proper column; add new columns if needed: 
                s = dataValues[pNameIndex].ToString();
                if (!tmp.Columns.Contains(s))
                    tmp.Columns.Add(s, dataValues.GetFieldType(pValIndex));

                // Create new row after adding any additional columns 
                r = tmp.NewRow();

                // Add pivot value 
                r[s] = dataValues[pValIndex];

                // Add all non-pivot column values to the new row: 
                for (i = 0; i <= dataValues.FieldCount - 3; i++)
                    r[i] = dataValues[tmp.Columns[i].ColumnName];

                // Look for key 
                DataRow rowFound = tmp.Rows.Find(r[keyColumn]);

                // Add new record if not found 
                if (null == rowFound)
                {
                    tmp.Rows.Add(r);
                }
                else // Key already exists .. just update it 
                {
                    rowFound[s] = dataValues[pValIndex];
                }
            }
            // Close the DataReader 
            dataValues.Close();
            // and that's it! 
            return tmp; 
        }

        protected void txtTo_Select(object sender, DirectEventArgs e)
        {
            this.txtTo.MinValue = Convert.ToInt32(txtFrom.Value);
            this.txtTo.DataBind();
        }

        private Int32 GetIdField(String _Name)
        {
            Int32 intID = 0;

            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dtField = objFields.LoadList("IdModel = " + IdModel + " AND Name = '" + _Name + "'", "ORDER BY Name");

            if (dtField.Rows.Count > 0)
            {
                intID = (Int32)dtField.Rows[0]["IdField"];
            }

            return intID;
        }

        private Int32 GetIdLevel(String _Name)
        {
            Int32 intID = 0;

            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dtLevel = objAggregationLevels.LoadList("IdModel = " + IdModel + " AND Name = '" + _Name + "'", "ORDER BY Name");

            if (dtLevel.Rows.Count > 0)
            {
                intID = (Int32)dtLevel.Rows[0]["IdAggregationLevel"];
            }

            return intID;
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExportOpexTOT = new DataReportObject();
            StoredExportOpexTOT = ExportOpexTOT;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExportOpexTOT);
            objWriter.BuildExportOperationalMarginProjectionReport(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2007);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=OperationalMarginProjection.xlsx");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        private void DisableParameterSelection()
        {
            this.pnlExportToolbar.Hidden = false;
            this.pnlReportToolbar.Hidden = true;
            this.ddlLevels.ReadOnly = true;
            this.ddlField.ReadOnly = true;
            this.ddlStructure.ReadOnly = true;
            this.txtFrom.ReadOnly = true;
            this.txtTo.ReadOnly = true;
            this.ddlScenarioTechnical.ReadOnly = true;
            this.ddlScenarioEconomic.ReadOnly = true;
            this.ddlCurrency.ReadOnly = true;
            this.ddlTerm.ReadOnly = true;
            this.pnlMainChartPanel.Hidden = false;
            this.ddlCostType.ReadOnly=true;
            this.ddlOperationType.ReadOnly=true;
            this.ddlFactor.ReadOnly = true;
            this.ddlFilterNetPriceScenario.ReadOnly = true;
        }

        private void SavePNG(Stream SaveStream, string SVGDoc)
        {
            //XmlDocument xd = new XmlDocument();
            //xd.XmlResolver = null;
            //xd.LoadXml(SVGDoc);
            //var svgGraph = Svg.SvgDocument.Open(xd);
            //svgGraph.Draw().Save(SaveStream, System.Drawing.Imaging.ImageFormat.Png);
        }

        public void ModelCheckRecalcModuleReport()
        {
            clsModels objModelRecalc = new clsModels();
            objModelRecalc.IdModel = IdModel;
            objModelRecalc.CheckRecalcModuleParameter(objApplication.MaxRelationLevels);
            objModelRecalc.CheckRecalcModuleAllocation();
            objModelRecalc.CheckRecalcModuleTechnical();
            objModelRecalc.CheckRecalcModuleEconomic();
            objModelRecalc = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = (String)GetGlobalResourceObject("CPM_Resources", "OperationalMarginsProjection");
            this.btnReset.Text = modMain.strReportsModuleReset;
            this.btnSaveExcel.Text = modMain.strReportsModuleExportReportDataToExcel;
            this.lblLevels.Text = modMain.strReportsModuleAggregationLevel;
            this.lblField.Text = modMain.strTechnicalModuleField;
            this.lblStructure.Text = modMain.strReportsModuleCostStructure;
            this.lblFrom.Text = modMain.strReportsModuleFrom;
            this.lblTo.Text = modMain.strReportsModuleTo;
            this.lblScenarioTechnical.Text = modMain.strReportsModuleTechnicalScenario;
            this.lblScenarioEconomic.Text = modMain.strReportsModuleEconomicScenario;
            this.lblCurrency.Text = modMain.strReportsModuleCurrency;
            this.lblTerm.Text = modMain.strReportsModuleTerm;
            this.btnRun.Text = (String)GetGlobalResourceObject("CPM_Resources", "Run Report"); //modMain.strReportsModuleRunReport;
            this.ReportTitle01.Title = (String)GetGlobalResourceObject("CPM_Resources", "TotalProjection");
            this.lblOperationType.Text = modMain.strReportsModuleFormOperationType + ":";//(String)GetGlobalResourceObject("CPM_Resources", "Operation Type") + ":";
            this.lblCostType.Text = modMain.strReportsModuleFormTypeCost + ":";//(String)GetGlobalResourceObject("CPM_Resources", "Cost Type") + ":";
            this.btnDirectExcel.Text = modMain.strReportsModuleExportReportDataToExcel;
            this.lblFilterNetPriceScenario.Text = (String)GetGlobalResourceObject("CPM_Resources", "Price Scenario") + ":";
        }

        #endregion

    }
}
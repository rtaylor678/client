﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="driverreport.aspx.cs" Inherits="CPM._driverreport" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:ChartTheme ID="ChartThemeSA" runat="server" ThemeName="SATheme" Colors="#4A7DC6,#CE3018,#39BE39,#FFCB4A,#7B498C,#7BCBFF,#E79642,#B53C6B,#4A4D4A,#BDD7EF,#104110,#F7BACE,#395D73" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" Layout="FitLayout" Title="Technical Drivers Forecast" IconCls="icon-driverprofiles_16" AutoScroll="true" runat="server" Border="false">
                <Items>
                    <ext:Panel ID="Panel10" runat="server" AutoScroll="true">
                        <Items>
                            <ext:Panel ID="pnlExportToolbar" runat="server" Border="false" Hidden="true">
                                <TopBar>
                                    <ext:Toolbar ID="tbrExport" Border="false" Height="30" runat="server">
                                        <Items>
                                            <ext:Button ID="btnReset" runat="server" Text="Reset" Icon="Reload">
                                                <DirectEvents>
                                                    <Click OnEvent="btnReset_Click">
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                            <ext:ToolbarFill />
                                            <ext:Button ID="btnSaveExcel" runat="server" Text="Export Report Data To Excel" Icon="PageExcel">
                                                <DirectEvents>
                                                    <Click OnEvent="btnSaveExcel_Click" IsUpload="true">
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                            </ext:Panel>
                            <ext:Panel ID="Panel1" runat="server" AutoScroll="true" Title="Technical Driver #1">
                                <Items>
                                    <ext:Panel ID="pnlButtons01" runat="server" Layout="Column" Border="false" Style="margin: 15px 0 0 0">
                                        <Items>
                                            <ext:Panel ID="Panel100" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 15px 0">
                                                <Items>
                                                    <ext:Label Style="margin: 0 0 0 10px" ID="lblFilterTechnicalDriver1" runat="server" Text="Driver:" />
                                                    <ext:ComboBox Style="margin: 0 0 0 10px" MinChars="0" ID="ddlFilterTechnicalDriver1" runat="server" Width="200" DisplayField="Name" ValueField="ID">
                                                        <ListConfig LoadingText="Searching..." MinWidth="300">
                                                        </ListConfig>
                                                        <Store>
                                                            <ext:Store ID="storeFilterTechnicalDriver1" runat="server" OnReadData="StoreFilterTechnicalDriver1_ReadData">
                                                                <Model>
                                                                    <ext:Model ID="Model6" runat="server" IDProperty="ID">
                                                                        <Fields>
                                                                            <ext:ModelField Name="Name" />
                                                                            <ext:ModelField Name="ID" />
                                                                        </Fields>
                                                                    </ext:Model>
                                                                </Model>
                                                                <Proxy>
                                                                    <ext:PageProxy>
                                                                        <Reader>
                                                                            <ext:JsonReader />
                                                                        </Reader>
                                                                    </ext:PageProxy>
                                                                </Proxy>
                                                            </ext:Store>
                                                        </Store>
                                                        <DirectEvents>
                                                            <Select OnEvent="ddlFilterTechnicalDriver1_Select" />
                                                        </DirectEvents>
                                                    </ext:ComboBox>
                                                    <ext:Label Style="padding: 0 0 0 10px" ID="lblFilterTechnicalDriverUnit" runat="server" Width="150" Text=""></ext:Label>
                                                </Items>
                                            </ext:Panel>
                                            <ext:Panel ID="Panel101" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 15px 0">
                                                <Items>
                                                    <ext:Label Style="margin: 0 0 0 10px" ID="lblFilterField1" runat="server" Text="Field:"></ext:Label>
                                                    <ext:ComboBox Style="margin: 0 0 0 10px" ID="ddlField1" MinChars="0" runat="server" Width="200" DisplayField="Name" ValueField="IdField">
                                                        <ListConfig LoadingText="Searching..." MinWidth="300">
                                                        </ListConfig>
                                                        <Store>
                                                            <ext:Store ID="storeField1" runat="server" OnReadData="storeField1_ReadData">
                                                                <Model>
                                                                    <ext:Model ID="Model5" runat="server" IDProperty="IdField">
                                                                        <Fields>
                                                                            <ext:ModelField Name="Name" />
                                                                            <ext:ModelField Name="IdField" />
                                                                        </Fields>
                                                                    </ext:Model>
                                                                </Model>
                                                                <Proxy>
                                                                    <ext:PageProxy>
                                                                        <Reader>
                                                                            <ext:JsonReader />
                                                                        </Reader>
                                                                    </ext:PageProxy>
                                                                </Proxy>
                                                            </ext:Store>
                                                        </Store>
                                                        <DirectEvents>
                                                            <Select OnEvent="ddlField1_Select" />
                                                        </DirectEvents>
                                                    </ext:ComboBox>
                                                </Items>
                                            </ext:Panel>
                                            <ext:Panel ID="Panel102" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 15px 0">
                                                <Items>
                                                    <ext:Label Style="margin: 0 0 0 10px" ID="lblFilterProject1" runat="server" Text="Project:"></ext:Label>
                                                    <ext:ComboBox Style="margin: 0 0 0 10px" MinChars="0" ID="ddlFilterProject1" runat="server" Width="200" DisplayField="VisualName" ValueField="IdProject">
                                                        <ListConfig LoadingText="Searching..." MinWidth="300">
                                                            <ItemTpl ID="ItemTpl1" runat="server">
                                                                <Html>
                                                                    <div>{Name}<span> | </span> {OperationName}</div>
                                                                </Html>
                                                            </ItemTpl>
                                                        </ListConfig>
                                                        <Store>
                                                            <ext:Store ID="storeFilterProject1" runat="server" OnReadData="StoreFilterProject1_ReadData">
                                                                <Model>
                                                                    <ext:Model ID="Model7" runat="server" IDProperty="IdProject">
                                                                        <Fields>
                                                                            <ext:ModelField Name="Name" />
                                                                            <ext:ModelField Name="IdProject" />
                                                                            <ext:ModelField Name="OperationName" />
                                                                            <ext:ModelField Name="VisualName" />
                                                                        </Fields>
                                                                    </ext:Model>
                                                                </Model>
                                                                <Proxy>
                                                                    <ext:PageProxy>
                                                                        <Reader>
                                                                            <ext:JsonReader />
                                                                        </Reader>
                                                                    </ext:PageProxy>
                                                                </Proxy>
                                                            </ext:Store>
                                                        </Store>
                                                        <DirectEvents>
                                                            <Select OnEvent="ddlFilterProject1_Select" />
                                                        </DirectEvents>
                                                    </ext:ComboBox>
                                                </Items>
                                            </ext:Panel>
                                        </Items>
                                    </ext:Panel>
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="Panel7" runat="server" Border="false">
                                <TopBar>
                                    <ext:Toolbar ID="Toolbar1" Border="false" Height="30" runat="server">
                                        <Items>
                                            <ext:Button ID="btnSave1" Icon="DataBaseSave" Disabled="true" TextAlign="Center" ToolTip="Save Changes" runat="server" Text="Save Changes">
                                                <DirectEvents>
                                                    <Click OnEvent="btnSave1_Click">
                                                        <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        <ExtraParams>
                                                            <ext:Parameter Name="rowsValues" Value="#{grdTechnicalDriverData}.getRowsValues(false)" Mode="Raw" Encode="true" />
                                                        </ExtraParams>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                            </ext:Panel>
                            <ext:Panel runat="server" Border="false">
                                <Items>
                                    <ext:GridPanel ID="grdTechnicalDriverData" runat="server" Border="false" Header="false">
                                        <Store>
                                            <ext:Store ID="storeTechnicalDriverData" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model1" runat="server" IDProperty="IdTechnicalDriverData">
                                                        <Fields>
                                                            <ext:ModelField Name="IdTechnicalDriver" Type="Int" />
                                                            <ext:ModelField Name="IdProject" Type="Int" />
                                                            <ext:ModelField Name="IdField" Type="Int" />
                                                            <ext:ModelField Name="Year" Type="Int" />
                                                            <ext:ModelField Name="Normal" Type="Float" />
                                                            <ext:ModelField Name="Optimistic" Type="Float" />
                                                            <ext:ModelField Name="Pessimistic" Type="Float" />
                                                            <ext:ModelField Name="IdTechnicalDriverData" Type="Int" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <Plugins>
                                            <ext:CellEditing ID="CellEditing1" runat="server" ClicksToEdit="1" />
                                        </Plugins>
                                        <ColumnModel ID="ColumnModel1" runat="server">
                                            <Columns>
                                                <ext:Column ID="colID" Hidden="true" runat="server" Text="ID" DataIndex="IdTechnicalDriverData">
                                                </ext:Column>
                                                <ext:Column ID="Year" runat="server" Text="Year" DataIndex="Year" Width="100">
                                                </ext:Column>
                                                <ext:NumberColumn ID="Columns1" Format="0,000.00" runat="server" DataIndex="Normal" Flex="1" Text="Base" Align="Right" MaxWidth="200">
                                                    <Editor>
                                                        <ext:NumberField ID="NumberField1" runat="server" AllowBlank="false" MinValue="0">
                                                        </ext:NumberField>
                                                    </Editor>
                                                </ext:NumberColumn>
                                                <ext:NumberColumn ID="Columns3" Format="0,000.00" runat="server" DataIndex="Pessimistic" Flex="1" Text="Pessimistic" Align="Right" MaxWidth="200">
                                                    <Editor>
                                                        <ext:NumberField ID="NumberField3" runat="server" AllowBlank="false" MinValue="0">
                                                        </ext:NumberField>
                                                    </Editor>
                                                </ext:NumberColumn>
                                                <ext:NumberColumn ID="Columns2" Format="0,000.00" runat="server" DataIndex="Optimistic" Flex="1" Text="Optimistic" Align="Right" MaxWidth="200">
                                                    <Editor>
                                                        <ext:NumberField ID="NumberField2" runat="server" AllowBlank="false" MinValue="0">
                                                        </ext:NumberField>
                                                    </Editor>
                                                </ext:NumberColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <SelectionModel>
                                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" />
                                        </SelectionModel>
                                        <Features>
                                            <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                                                <Filters>
                                                    <ext:StringFilter DataIndex="Year" />
                                                    <ext:StringFilter DataIndex="Normal" />
                                                    <ext:StringFilter DataIndex="Pessimistic" />
                                                    <ext:StringFilter DataIndex="Optimistic" />
                                                    <ext:NumericFilter DataIndex="IdTechnicalDriverData" />
                                                </Filters>
                                            </ext:GridFilters>
                                        </Features>
                                    </ext:GridPanel>
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="Panel2" runat="server" AutoScroll="true" Title="Technical Driver #2">
                                <Items>
                                    <ext:Panel ID="pnlButtons02" runat="server" Layout="Column" Border="false" Style="margin: 15px 0 0 0">
                                        <Items>
                                            <ext:Panel ID="Panel200" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 15px 0">
                                                <Items>
                                                    <ext:Label Style="margin: 0 0 0 10px" ID="lblFilterTechnicalDriver2" runat="server" Text="Driver:"></ext:Label>
                                                    <ext:ComboBox Style="margin: 0 0 0 10px" MinChars="0" ID="ddlFilterTechnicalDriver2" runat="server" Width="200" DisplayField="Name" ValueField="ID">
                                                        <ListConfig LoadingText="Searching..." MinWidth="300">
                                                        </ListConfig>
                                                        <Store>
                                                            <ext:Store ID="storeFilterTechnicalDriver2" runat="server" OnReadData="StoreFilterTechnicalDriver2_ReadData">
                                                                <Model>
                                                                    <ext:Model ID="Model8" runat="server" IDProperty="ID">
                                                                        <Fields>
                                                                            <ext:ModelField Name="Name" />
                                                                            <ext:ModelField Name="ID" />
                                                                        </Fields>
                                                                    </ext:Model>
                                                                </Model>
                                                                <Proxy>
                                                                    <ext:PageProxy>
                                                                        <Reader>
                                                                            <ext:JsonReader />
                                                                        </Reader>
                                                                    </ext:PageProxy>
                                                                </Proxy>
                                                            </ext:Store>
                                                        </Store>
                                                        <DirectEvents>
                                                            <Select OnEvent="ddlFilterTechnicalDriver2_Select" />
                                                        </DirectEvents>
                                                    </ext:ComboBox>
                                                    <ext:Label Style="padding: 0 0 0 10px" ID="lblFilterTechnicalDriver2Unit" runat="server" Width="150" Text=""></ext:Label>
                                                </Items>
                                            </ext:Panel>
                                            <ext:Panel ID="Panel201" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 15px 0">
                                                <Items>
                                                    <ext:Label Style="margin: 0 0 0 10px" ID="lblFilterField2" runat="server" Text="Field:"></ext:Label>
                                                    <ext:ComboBox Style="margin: 0 0 0 10px" ID="ddlField2" MinChars="0" runat="server" Width="200" DisplayField="Name" ValueField="IdField">
                                                        <ListConfig LoadingText="Searching..." MinWidth="300">
                                                        </ListConfig>
                                                        <Store>
                                                            <ext:Store ID="storeField2" runat="server" OnReadData="storeField2_ReadData">
                                                                <Model>
                                                                    <ext:Model ID="Model9" runat="server" IDProperty="IdField">
                                                                        <Fields>
                                                                            <ext:ModelField Name="Name" />
                                                                            <ext:ModelField Name="IdField" />
                                                                        </Fields>
                                                                    </ext:Model>
                                                                </Model>
                                                                <Proxy>
                                                                    <ext:PageProxy>
                                                                        <Reader>
                                                                            <ext:JsonReader />
                                                                        </Reader>
                                                                    </ext:PageProxy>
                                                                </Proxy>
                                                            </ext:Store>
                                                        </Store>
                                                        <DirectEvents>
                                                            <Select OnEvent="ddlField2_Select" />
                                                        </DirectEvents>
                                                    </ext:ComboBox>
                                                </Items>
                                            </ext:Panel>
                                            <ext:Panel ID="Panel202" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 15px 0">
                                                <Items>
                                                    <ext:Label Style="margin: 0 0 0 10px" ID="lblFilterProject2" runat="server" Text="Project:"></ext:Label>
                                                    <ext:ComboBox Style="margin: 0 0 0 10px" MinChars="0" ID="ddlFilterProject2" runat="server" Width="200" DisplayField="VisualName" ValueField="IdProject">
                                                        <ListConfig LoadingText="Searching..." MinWidth="300">
                                                            <ItemTpl ID="ItemTpl2" runat="server">
                                                                <Html>
                                                                    <div>{Name}<span> | </span> {OperationName}</div>
                                                                </Html>
                                                            </ItemTpl>
                                                        </ListConfig>
                                                        <Store>
                                                            <ext:Store ID="storeFilterProject2" runat="server" OnReadData="StoreFilterProject2_ReadData">
                                                                <Model>
                                                                    <ext:Model ID="Model10" runat="server" IDProperty="IdProject">
                                                                        <Fields>
                                                                            <ext:ModelField Name="Name" />
                                                                            <ext:ModelField Name="IdProject" />
                                                                            <ext:ModelField Name="OperationName" />
                                                                            <ext:ModelField Name="VisualName" />
                                                                        </Fields>
                                                                    </ext:Model>
                                                                </Model>
                                                                <Proxy>
                                                                    <ext:PageProxy>
                                                                        <Reader>
                                                                            <ext:JsonReader />
                                                                        </Reader>
                                                                    </ext:PageProxy>
                                                                </Proxy>
                                                            </ext:Store>
                                                        </Store>
                                                        <DirectEvents>
                                                            <Select OnEvent="ddlFilterProject2_Select" />
                                                        </DirectEvents>
                                                    </ext:ComboBox>
                                                </Items>
                                            </ext:Panel>
                                        </Items>
                                    </ext:Panel>
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="Panel8" runat="server" Border="false">
                                <TopBar>
                                    <ext:Toolbar ID="Toolbar2" Border="false" Height="30" runat="server">
                                        <Items>
                                            <ext:Button ID="btnSave2" Icon="DataBaseSave" Disabled="true" TextAlign="Center" ToolTip="Save Changes" runat="server" Text="Save Changes">
                                                <DirectEvents>
                                                    <Click OnEvent="btnSave2_Click">
                                                        <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                        <ExtraParams>
                                                            <ext:Parameter Name="rowsValues" Value="#{grdTechnicalDriverData2}.getRowsValues(false)" Mode="Raw" Encode="true" />
                                                        </ExtraParams>
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                            </ext:Panel>
                            <ext:Panel runat="server" Border="false">
                                <Items>
                                    <ext:GridPanel ID="grdTechnicalDriverData2" runat="server" Border="false" Header="false">
                                        <Store>
                                            <ext:Store ID="storeTechnicalDriverData2" runat="server">
                                                <Model>
                                                    <ext:Model ID="Model11" runat="server" IDProperty="IdTechnicalDriverData">
                                                        <Fields>
                                                            <ext:ModelField Name="IdTechnicalDriver" Type="Int" />
                                                            <ext:ModelField Name="IdProject" Type="Int" />
                                                            <ext:ModelField Name="IdField" Type="Int" />
                                                            <ext:ModelField Name="Year" Type="Int" />
                                                            <ext:ModelField Name="Normal" Type="Float" />
                                                            <ext:ModelField Name="Optimistic" Type="Float" />
                                                            <ext:ModelField Name="Pessimistic" Type="Float" />
                                                            <ext:ModelField Name="IdTechnicalDriverData" Type="Int" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <Plugins>
                                            <ext:CellEditing ID="CellEditing2" runat="server" ClicksToEdit="1" />
                                        </Plugins>
                                        <ColumnModel ID="ColumnModel2" runat="server">
                                            <Columns>
                                                <ext:Column ID="Column1" Hidden="true" runat="server" Text="ID" DataIndex="IdTechnicalDriverData">
                                                </ext:Column>
                                                <ext:Column ID="Column2" runat="server" Text="Year" DataIndex="Year" Width="100">
                                                </ext:Column>
                                                <ext:NumberColumn ID="NumberColumn1" Format="0,000.00" runat="server" DataIndex="Normal" Flex="1" Text="Base" Align="Right" MaxWidth="200">
                                                    <Editor>
                                                        <ext:NumberField ID="NumberField4" runat="server" AllowBlank="false" MinValue="0">
                                                        </ext:NumberField>
                                                    </Editor>
                                                </ext:NumberColumn>
                                                <ext:NumberColumn ID="NumberColumn3" Format="0,000.00" runat="server" DataIndex="Pessimistic" Flex="1" Text="Pessimistic" Align="Right" MaxWidth="200">
                                                    <Editor>
                                                        <ext:NumberField ID="NumberField6" runat="server" AllowBlank="false" MinValue="0">
                                                        </ext:NumberField>
                                                    </Editor>
                                                </ext:NumberColumn>
                                                <ext:NumberColumn ID="NumberColumn2" Format="0,000.00" runat="server" DataIndex="Optimistic" Flex="1" Text="Optimistic" Align="Right" MaxWidth="200">
                                                    <Editor>
                                                        <ext:NumberField ID="NumberField5" runat="server" AllowBlank="false" MinValue="0">
                                                        </ext:NumberField>
                                                    </Editor>
                                                </ext:NumberColumn>
                                            </Columns>
                                        </ColumnModel>
                                        <SelectionModel>
                                            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" />
                                        </SelectionModel>
                                        <Features>
                                            <ext:GridFilters runat="server" ID="GridFilters2" Local="true">
                                                <Filters>
                                                    <ext:StringFilter DataIndex="Year" />
                                                    <ext:StringFilter DataIndex="Normal" />
                                                    <ext:StringFilter DataIndex="Pessimistic" />
                                                    <ext:StringFilter DataIndex="Optimistic" />
                                                    <ext:NumericFilter DataIndex="IdTechnicalDriverData" />
                                                </Filters>
                                            </ext:GridFilters>
                                        </Features>
                                    </ext:GridPanel>
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="pnlReportToolbar" runat="server" Border="false" Hidden="false">
                                <TopBar>
                                    <ext:Toolbar ID="tbrReport" Border="false" Height="30" runat="server">
                                        <Items>
                                            <ext:Button ID="btnRun" runat="server" Text="Run Report" Icon="PlayGreen">
                                                <DirectEvents>
                                                    <Click OnEvent="btnRun_Click" />
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                            </ext:Panel>
                            <ext:Panel ID="ReportTitle01" runat="server" IconCls="icon-driverprofiles_16" Border="false" Title="Technical Drivers Forecast Indicator" Cls="reportheader">
                            </ext:Panel>
                            <ext:Panel ID="pnlMainChartPanel" Border="false" ManageHeight="true" runat="server" Hidden="true">
                                <Items>
                                    <ext:Panel ID="pnlChartObject" runat="server" Border="false" MaxHeight="600" Icon="ChartBar" Title="Technical Drivers Forecast Indicator" Layout="FitLayout" Header="false">
                                        <Items>
                                            <ext:TabPanel ID="tabCharts" runat="server" Border="false" Layout="FitLayout" ActiveTabIndex="0">
                                                <Items>
                                                    <ext:Panel ID="tabDriver1" runat="server" Title="Driver1" Closable="false" Hidden="true" Border="false" Layout="FitLayout">
                                                        <Items>
                                                            <ext:Chart ID="chtChart1" runat="server" Border="false" Animate="true" MinHeight="0" MaxHeight="700" MaxWidth="1350" Theme="SATheme" InsetPadding="30" Layout="FitLayout" AutoSize="true">
                                                                <LegendConfig Position="Bottom" LabelFont="8px" />
                                                                <Store>
                                                                    <ext:Store ID="storeChart1" runat="server">
                                                                        <Model>
                                                                            <ext:Model ID="modelChart1" runat="server">
                                                                                <Fields>
                                                                                </Fields>
                                                                            </ext:Model>
                                                                        </Model>
                                                                    </ext:Store>
                                                                </Store>
                                                            </ext:Chart>
                                                        </Items>
                                                    </ext:Panel>
                                                    <ext:Panel ID="tabDriver2" runat="server" Title="Driver1" Closable="false" Hidden="true" Border="false" Layout="FitLayout">
                                                        <Items>
                                                            <ext:Chart ID="chtChart2" runat="server" Border="false" Animate="true" MinHeight="0" MaxHeight="700" MaxWidth="1350" Theme="SATheme" InsetPadding="30" Layout="FitLayout" AutoSize="true">
                                                                <LegendConfig Position="Bottom" LabelFont="10px" />
                                                                <Store>
                                                                    <ext:Store ID="storeChart2" runat="server">
                                                                        <Model>
                                                                            <ext:Model ID="modelChart2" runat="server">
                                                                                <Fields>
                                                                                </Fields>
                                                                            </ext:Model>
                                                                        </Model>
                                                                    </ext:Store>
                                                                </Store>
                                                            </ext:Chart>
                                                        </Items>
                                                    </ext:Panel>
                                                    <ext:Panel ID="tabDriver3" runat="server" Title="Driver1" Closable="false" Hidden="true" Border="false" Layout="FitLayout">
                                                        <Items>
                                                            <ext:Chart ID="chtChart3" runat="server" Border="false" Animate="true" MinHeight="0" MaxHeight="700" MaxWidth="1350" Theme="SATheme" InsetPadding="30" Layout="FitLayout" AutoSize="true">
                                                                <LegendConfig Position="Bottom" LabelFont="10px" />
                                                                <Store>
                                                                    <ext:Store ID="storeChart3" runat="server">
                                                                        <Model>
                                                                            <ext:Model ID="modelChart3" runat="server">
                                                                                <Fields>
                                                                                </Fields>
                                                                            </ext:Model>
                                                                        </Model>
                                                                    </ext:Store>
                                                                </Store>
                                                            </ext:Chart>
                                                        </Items>
                                                    </ext:Panel>
                                                </Items>
                                            </ext:TabPanel>
                                        </Items>
                                    </ext:Panel>
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="pnlForceFit" runat="server" Border="false" Title=" " Height="1" Cls="reportheader">
                            </ext:Panel>
                        </Items>
                    </ext:Panel>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="FormStatusBar" runat="server" />
                </BottomBar>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using System.Xml.Xsl;
using CD;
using DataClass;
using System.ComponentModel;
using System.Web.Script.Serialization;

namespace CPM
{
    public partial class _driverreport : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdTechnicalDriverData { get { return (Int32)Session["IdTechnicalDriverData"]; } set { Session["IdTechnicalDriverData"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        public DataTable datViewData { get; set; }
        public static string strTechnicalDriver1Unit;
        public static string strTechnicalDriver2Unit;
        private Boolean PageIsLoaded { get; set; }
        private Boolean Driver1Selected { get; set; }
        private Boolean Driver2Selected { get; set; }
        private DataReportObject ExportDriver01 { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }
        private DataReportObject ExportDriver02 { get { return (DataReportObject)Session["Export02"]; } set { Session["Export02"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }
       
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                this.ModelCheckRecalcModuleReport();
                Session["Export01"] = null;
                Session["Export02"] = null;
                Session["Export03"] = null;
                PageIsLoaded = false;
                LoadLabel();
                IdTechnicalDriverData = 0;
                if (this.GetProject())
                {
                    this.lblFilterProject1.Hide();
                    this.ddlFilterProject1.Hide();
                }
                PageIsLoaded = true;
            }
        }

        protected void ddlFilterTechnicalDriver1_Select(object sender, DirectEventArgs e)
        {
            this.DisableDriverSave1();

            this.ddlField1.Value = null;
            this.ddlFilterProject1.Value = null;

            DataClass.clsTechnicalDrivers objTechnicalDrivers = new DataClass.clsTechnicalDrivers();
            DataClass.clsUnit objUnit = new DataClass.clsUnit();
            if (ddlFilterTechnicalDriver1.Value != null)
            {
                int number;
                if (Int32.TryParse(ddlFilterTechnicalDriver1.SelectedItem.Value, out number))
                {
                    objTechnicalDrivers.IdTechnicalDriver = Convert.ToInt32(ddlFilterTechnicalDriver1.SelectedItem.Value);
                    this.storeField1.Reload();
                }
                else
                {
                    objTechnicalDrivers.IdTechnicalDriver = 0;
                }
                objTechnicalDrivers.loadObject();
                objUnit.IdUnit = objTechnicalDrivers.IdUnit;
                objUnit.loadObject();
                this.lblFilterTechnicalDriverUnit.Text = (String)GetGlobalResourceObject("CPM_Resources", objUnit.Name); 
                strTechnicalDriver1Unit = objUnit.Name;
            }
        }

        protected void ddlFilterTechnicalDriver2_Select(object sender, DirectEventArgs e)
        {
            this.DisableDriverSave2();

            this.ddlField2.Value = null;
            this.ddlFilterProject2.Value = null;

            DataClass.clsTechnicalDrivers objTechnicalDrivers = new DataClass.clsTechnicalDrivers();
            DataClass.clsUnit objUnit = new DataClass.clsUnit();
            if (ddlFilterTechnicalDriver2.Value != null)
            {
                int number;
                if (Int32.TryParse(ddlFilterTechnicalDriver2.SelectedItem.Value, out number))
                {
                    objTechnicalDrivers.IdTechnicalDriver = Convert.ToInt32(ddlFilterTechnicalDriver2.SelectedItem.Value);
                    this.storeField2.Reload();
                }
                else
                {
                    objTechnicalDrivers.IdTechnicalDriver = 0;
                }
                objTechnicalDrivers.loadObject();
                objUnit.IdUnit = objTechnicalDrivers.IdUnit;
                objUnit.loadObject();
                this.lblFilterTechnicalDriver2Unit.Text = (String)GetGlobalResourceObject("CPM_Resources",objUnit.Name);
                strTechnicalDriver2Unit = objUnit.Name;
            }
        }

        protected void ddlField1_Select(object sender, DirectEventArgs e)
        {
            this.DisableDriverSave1();
            this.ddlFilterProject1.Value = null;
            int number;
            if (Int32.TryParse(ddlField1.SelectedItem.Value, out number))
            {
                storeFilterProject1.Reload();
            }
        }

        protected void ddlField2_Select(object sender, DirectEventArgs e)
        {
            this.DisableDriverSave2();
            this.ddlFilterProject2.Value = null;
            int number;
            if (Int32.TryParse(ddlField2.SelectedItem.Value, out number))
            {
                storeFilterProject2.Reload();
            }
        }

        protected void ddlFilterProject1_Select(object sender, DirectEventArgs e)
        {
            this.DisableDriverSave1();
        }

        protected void ddlFilterProject2_Select(object sender, DirectEventArgs e)
        {
            this.DisableDriverSave2();
        }

        protected void btnSave1_Click(object sender, DirectEventArgs e)
        {
            try
            {
                String _Values = e.ExtraParams["rowsValues"];

                datViewData = new DataTable();
                datViewData.Columns.Add("IdTechnicalDriverData", typeof(int));
                datViewData.Columns.Add("IdTechnicalDriver", typeof(int));
                datViewData.Columns.Add("IdProject", typeof(int));
                datViewData.Columns.Add("IdField", typeof(int));
                datViewData.Columns.Add("Year", typeof(int));
                datViewData.Columns.Add("Normal", typeof(float));
                datViewData.Columns.Add("Optimistic", typeof(float));
                datViewData.Columns.Add("Pessimistic", typeof(float));

                List<Object> data = JSON.Deserialize<List<Object>>(_Values);

                foreach (Object value in data)
                {
                    Object _ObjectRow = new Object();
                    _ObjectRow = value.ToString();

                    JavaScriptSerializer ser = new JavaScriptSerializer();

                    Dictionary<string, object> _Row = ser.Deserialize<Dictionary<string, object>>(_ObjectRow.ToString());
                    DataRow _NewRow = datViewData.NewRow();

                    _NewRow["IdTechnicalDriverData"] = _Row["IdTechnicalDriverData"];
                    _NewRow["IdTechnicalDriver"] = _Row["IdTechnicalDriver"];
                    _NewRow["IdProject"] = _Row["IdProject"];
                    _NewRow["IdField"] = _Row["IdField"];
                    _NewRow["Year"] = _Row["Year"];
                    _NewRow["Normal"] = _Row["Normal"];
                    _NewRow["Optimistic"] = _Row["Optimistic"];
                    _NewRow["Pessimistic"] = _Row["Pessimistic"];

                    datViewData.Rows.Add(_NewRow);
                }

                this.Save();
                this.LoadTechnicalDriverData1();
                this.ModelUpdateTimestamp();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Save Error"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnSave2_Click(object sender, DirectEventArgs e)
        {
            try
            {
                String _Values = e.ExtraParams["rowsValues"];

                datViewData = new DataTable();
                datViewData.Columns.Add("IdTechnicalDriverData", typeof(int));
                datViewData.Columns.Add("IdTechnicalDriver", typeof(int));
                datViewData.Columns.Add("IdProject", typeof(int));
                datViewData.Columns.Add("IdField", typeof(int));
                datViewData.Columns.Add("Year", typeof(int));
                datViewData.Columns.Add("Normal", typeof(float));
                datViewData.Columns.Add("Optimistic", typeof(float));
                datViewData.Columns.Add("Pessimistic", typeof(float));

                List<Object> data = JSON.Deserialize<List<Object>>(_Values);

                foreach (Object value in data)
                {
                    Object _ObjectRow = new Object();
                    _ObjectRow = value.ToString();

                    JavaScriptSerializer ser = new JavaScriptSerializer();

                    Dictionary<string, object> _Row = ser.Deserialize<Dictionary<string, object>>(_ObjectRow.ToString());
                    DataRow _NewRow = datViewData.NewRow();

                    _NewRow["IdTechnicalDriverData"] = _Row["IdTechnicalDriverData"];
                    _NewRow["IdTechnicalDriver"] = _Row["IdTechnicalDriver"];
                    _NewRow["IdProject"] = _Row["IdProject"];
                    _NewRow["IdField"] = _Row["IdField"];
                    _NewRow["Year"] = _Row["Year"];
                    _NewRow["Normal"] = _Row["Normal"];
                    _NewRow["Optimistic"] = _Row["Optimistic"];
                    _NewRow["Pessimistic"] = _Row["Pessimistic"];

                    datViewData.Rows.Add(_NewRow);
                }

                this.Save();
                this.LoadTechnicalDriverData2();
                this.ModelUpdateTimestamp();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Save Error"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnReset_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/report/driverreport.aspx");
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources","Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnRun_Click(object sender, DirectEventArgs e)
        {
            try
            {
                DisableParameterSelection();
                DisableDriverSave1();
                DisableDriverSave2();

                this.FormStatusBar.ClearStatus();
                
                if (this.GetProject() == false)
                {
                    if (ddlFilterTechnicalDriver1.SelectedItem.Value != null && ddlField1.SelectedItem.Value != null && ddlFilterProject1.SelectedItem.Value != null)
                    {
                        Driver1Selected = true;
                    }
                    if (ddlFilterTechnicalDriver2.SelectedItem.Value != null && ddlField2.SelectedItem.Value != null && ddlFilterProject2.SelectedItem.Value != null)
                    {
                        Driver2Selected = true;
                    }
                }
                else
                {
                    if (ddlFilterTechnicalDriver1.SelectedItem.Value != null && ddlField1.SelectedItem.Value != null)
                    {
                        Driver1Selected = true;
                    }
                    if (ddlFilterTechnicalDriver2.SelectedItem.Value != null && ddlField2.SelectedItem.Value != null)
                    {
                        Driver2Selected = true;
                    }
                }

                if (Driver1Selected && Driver2Selected)
                {
                    this.LoadTechnicalDriverData1();
                    this.LoadTechnicalDriverData2();
                    this.btnSave1.Disabled = false;
                    this.btnSave2.Disabled = false;
                }
                else if (Driver1Selected)
                {
                    this.LoadTechnicalDriverData1();
                    this.btnSave1.Disabled = false;
                }
                else if (Driver2Selected)
                {
                    this.LoadTechnicalDriverData2();
                    this.btnSave2.Disabled = false;
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","Select filters please"), IconCls = "icon-exclamation", Clear2 = false });
                }
                this.LoadChartGroup();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","Select filters please"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        #endregion

        #region Methods

        public Boolean GetProject()
        {
            Boolean Value = false;
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("Level IN (1,3) AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = true;
            }

            return Value;
        }

        protected void Save()
        {

            foreach (DataRow row in datViewData.Rows)
            {
                DataClass.clsTechnicalDriverData objTechnicalDriverData = new DataClass.clsTechnicalDriverData();
                objTechnicalDriverData.IdTechnicalDriverData = Convert.ToInt32(row["IdTechnicalDriverData"]);
                objTechnicalDriverData.IdTechnicalDriver = Convert.ToInt32(row["IdTechnicalDriver"]);
                objTechnicalDriverData.loadObject();
                objTechnicalDriverData.IdModel = IdModel;
                objTechnicalDriverData.IdField = Convert.ToInt32(row["IdField"]);
                objTechnicalDriverData.IdProject = Convert.ToInt32(row["IdProject"]);
                objTechnicalDriverData.Year = Convert.ToInt32(row["Year"]);
                objTechnicalDriverData.Normal = Convert.ToDouble(row["Normal"]);
                objTechnicalDriverData.Pessimistic = Convert.ToDouble(row["Pessimistic"]);
                objTechnicalDriverData.Optimistic = Convert.ToDouble(row["Optimistic"]);
                if (objTechnicalDriverData.Normal == 0 && objTechnicalDriverData.Optimistic == 0 && objTechnicalDriverData.Pessimistic == 0)
                {
                    if (objTechnicalDriverData.IdTechnicalDriverData != 0)
                    {
                        objTechnicalDriverData.Delete();
                    }
                }
                else
                {
                    if (objTechnicalDriverData.IdTechnicalDriverData == 0)
                    {
                        objTechnicalDriverData.DateCreation = DateTime.Now;
                        objTechnicalDriverData.UserCreation = (Int32)IdUserCreate;
                        IdTechnicalDriverData = objTechnicalDriverData.Insert();
                    }
                    else
                    {
                        objTechnicalDriverData.DateModification = DateTime.Now;
                        objTechnicalDriverData.UserModification = (Int32)IdUserCreate;
                        objTechnicalDriverData.Update();
                    }
                }

            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","Record saved successfully!"), IconCls = "icon-accept", Clear2 = false });
        }

        protected void storeField1_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsFields objFields = new DataClass.clsFields();
            if (ddlFilterTechnicalDriver1.Value.ToString() == "")
            {
                this.storeField1.DataSource = objFields.LoadListDrivers("IdModel = " + IdModel + " AND IdTechnicalDriver = 0 AND Name LIKE '%" + this.ddlField1.Text + "%'", "ORDER BY Name");
            }
            else
            {
                this.storeField1.DataSource = objFields.LoadListDrivers("IdModel = " + IdModel + " AND IdTechnicalDriver = " + ddlFilterTechnicalDriver1.Value + " AND Name LIKE '%" + this.ddlField1.Text + "%'", "ORDER BY Name");
            }
            this.storeField1.DataBind();
        }

        protected void storeField2_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsFields objFields = new DataClass.clsFields();
            if (ddlFilterTechnicalDriver2.Value.ToString() == "")
            {
                this.storeField2.DataSource = objFields.LoadListDrivers("IdModel = " + IdModel + " AND IdTechnicalDriver = 0 AND Name LIKE '%" + this.ddlField2.Text + "%'", "ORDER BY Name");
            }
            else
            {
                this.storeField2.DataSource = objFields.LoadListDrivers("IdModel = " + IdModel + " AND IdTechnicalDriver = " + ddlFilterTechnicalDriver2.Value + " AND Name LIKE '%" + this.ddlField2.Text + "%'", "ORDER BY Name");
            }
            this.storeField2.DataBind();
        }
        
        protected void StoreFilterTechnicalDriver1_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsTechnicalDrivers objTechnicalDrivers = new DataClass.clsTechnicalDrivers();
            this.storeFilterTechnicalDriver1.DataSource = objTechnicalDrivers.LoadComboBox("IdModel = " + IdModel + " AND tblTechnicalDrivers.Name LIKE '%" + this.ddlFilterTechnicalDriver1.Text + "%'", "ORDER BY Name");
            this.storeFilterTechnicalDriver1.DataBind();
        }

        protected void StoreFilterTechnicalDriver2_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsTechnicalDrivers objTechnicalDrivers = new DataClass.clsTechnicalDrivers();
            this.storeFilterTechnicalDriver2.DataSource = objTechnicalDrivers.LoadComboBox("IdModel = " + IdModel + " AND tblTechnicalDrivers.Name LIKE '%" + this.ddlFilterTechnicalDriver2.Text + "%'", "ORDER BY Name");
            this.storeFilterTechnicalDriver2.DataBind();
        }

        protected void StoreFilterProject1_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsProjects objProjects = new DataClass.clsProjects();
            if (ddlField1.SelectedItem.Value == null || ddlField1.SelectedItem.Text == null)
            {
                this.storeFilterProject1.DataSource = objProjects.LoadList("IdModel = " + IdModel + " And IdField = 0 AND Name +' | '+ OperationName LIKE '%" + this.ddlFilterProject1.Text + "%'", "ORDER BY Name");
            }
            else
            {
                this.storeFilterProject1.DataSource = objProjects.LoadList("IdModel = " + IdModel + " And IdField = " + ddlField1.SelectedItem.Value + " AND Name +' | '+ OperationName LIKE '%" + this.ddlFilterProject1.Text + "%'", "ORDER BY Name");
            }
            this.storeFilterProject1.DataBind();
        }

        protected void StoreFilterProject2_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsProjects objProjects = new DataClass.clsProjects();
            if (ddlField2.SelectedItem.Value == null || ddlField2.SelectedItem.Text == null)
            {
                this.storeFilterProject2.DataSource = objProjects.LoadList("IdModel = " + IdModel + " And IdField = 0 AND Name +' | '+ OperationName LIKE '%" + this.ddlFilterProject2.Text + "%'", "ORDER BY Name");
            }
            else
            {
                this.storeFilterProject2.DataSource = objProjects.LoadList("IdModel = " + IdModel + " And IdField = " + ddlField2.SelectedItem.Value + " AND Name +' | '+ OperationName LIKE '%" + this.ddlFilterProject2.Text + "%'", "ORDER BY Name");
            }
            this.storeFilterProject2.DataBind();
        }

        [DirectMethod]
        public void ClickedDeleteAllYES()
        {
            this.DeleteAll();
            this.LoadTechnicalDriverData1();
        }

        public void DeleteAll()
        {
            DataClass.clsTechnicalDriverData objTechnicalDriverData = new DataClass.clsTechnicalDriverData();
            objTechnicalDriverData.IdModel = IdModel;
            objTechnicalDriverData.DeleteAll();
        }

        protected void LoadTechnicalDriverData1()
        {
            DataClass.clsTechnicalDriverData objTechnicalDriverData = new DataClass.clsTechnicalDriverData();
            if (ddlField1.SelectedItem.Value != null && ddlField1.SelectedItem.Text != null)
            {
                if (ddlFilterTechnicalDriver1.SelectedItem.Value != null && ddlFilterTechnicalDriver1.SelectedItem.Text != null)
                {
                    if (this.GetProject() == false)
                    {
                        if (ddlFilterProject1.SelectedItem.Value == null && ddlFilterProject1.SelectedItem.Text == null)
                        {
                            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","Select a Project"), IconCls = "icon-exclamation", Clear2 = false });
                            return;
                        }
                    }
                    objTechnicalDriverData.IdTechnicalDriver = Convert.ToInt32(ddlFilterTechnicalDriver1.SelectedItem.Value);
                    objTechnicalDriverData.IdModel = Convert.ToInt32(IdModel);
                    objTechnicalDriverData.IdField = Convert.ToInt32(ddlField1.SelectedItem.Value);
                    objTechnicalDriverData.IdProject = Convert.ToInt32(ddlFilterProject1.SelectedItem.Value);
                    DataTable dt = objTechnicalDriverData.LoadList();
                    storeTechnicalDriverData.DataSource = dt;
                    storeTechnicalDriverData.DataBind();

                    // ToDo: can reset language of columns here
                    ArrayList datColumn = new ArrayList();
                    datColumn.Add(new DataClass.DataTableColumnDisplay("Year", "Year"));
                    datColumn.Add(new DataClass.DataTableColumnDisplay("Normal", "Normal"));
                    datColumn.Add(new DataClass.DataTableColumnDisplay("Pessimistic", "Pessimistic"));
                    datColumn.Add(new DataClass.DataTableColumnDisplay("Optimistic", "Optimistic"));

                    String strSelectedParameters = lblFilterTechnicalDriver1.Text + " '" + ddlFilterTechnicalDriver1.SelectedItem.Text + "', " + 
                                                   lblFilterField1.Text + " '" + ddlField1.SelectedItem.Text + "', " +
                                                   lblFilterProject1.Text + " '" + ddlFilterProject1.SelectedItem.Text + "'";
                    ExportDriver01 = new DataReportObject(dt, "Driver01", modMain.strReportsModuleTechnicalDriverNum + "1", strSelectedParameters, new ArrayList(new String[] { "UserCreation", "DateCreation", "UserModification", "DateModification" }), "Year", datColumn, null);
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","Select a Technical Driver"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","Select a Field"), IconCls = "icon-exclamation", Clear2 = false });
                return;
            }
        }

        protected void LoadTechnicalDriverData2()
        {
            DataClass.clsTechnicalDriverData objTechnicalDriverData = new DataClass.clsTechnicalDriverData();
            if (ddlField2.SelectedItem.Value != null && ddlField2.SelectedItem.Text != null)
            {
                if (ddlFilterTechnicalDriver2.SelectedItem.Value != null && ddlFilterTechnicalDriver2.SelectedItem.Text != null)
                {
                    if (this.GetProject() == false)
                    {
                        if (ddlFilterProject2.SelectedItem.Value == null && ddlFilterProject2.SelectedItem.Text == null)
                        {
                            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","Select a Project (Driver 2)"), IconCls = "icon-exclamation", Clear2 = false });
                            return;
                        }
                    }

                    objTechnicalDriverData.IdTechnicalDriver = Convert.ToInt32(ddlFilterTechnicalDriver2.SelectedItem.Value);
                    objTechnicalDriverData.IdModel = Convert.ToInt32(IdModel);
                    objTechnicalDriverData.IdField = Convert.ToInt32(ddlField2.SelectedItem.Value);
                    objTechnicalDriverData.IdProject = Convert.ToInt32(ddlFilterProject2.SelectedItem.Value);
                    DataTable dt = objTechnicalDriverData.LoadList();
                    storeTechnicalDriverData2.DataSource = dt;
                    storeTechnicalDriverData2.DataBind();

                    // ToDo: can reset language of columns here
                    ArrayList datColumn = new ArrayList();
                    datColumn.Add(new DataClass.DataTableColumnDisplay("Year", "Year"));
                    datColumn.Add(new DataClass.DataTableColumnDisplay("Normal", "Normal"));
                    datColumn.Add(new DataClass.DataTableColumnDisplay("Pessimistic", "Pessimistic"));
                    datColumn.Add(new DataClass.DataTableColumnDisplay("Optimistic", "Optimistic"));

                    String strSelectedParameters = lblFilterTechnicalDriver2.Text + " '" + ddlFilterTechnicalDriver2.SelectedItem.Text + "', " +
                                                   lblFilterField2.Text + " '" + ddlField2.SelectedItem.Text + "', " +
                                                   lblFilterProject2.Text + " '" + ddlFilterProject2.SelectedItem.Text + "'";
                    ExportDriver02 = new DataReportObject(dt, "Driver02", modMain.strReportsModuleTechnicalDriverNum + "2", strSelectedParameters, new ArrayList(new String[] { "UserCreation", "DateCreation", "UserModification", "DateModification" }), "Year", datColumn, null);
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","Select a Technical Driver"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","Select a Field (Driver 2)"), IconCls = "icon-exclamation", Clear2 = false });
                return;
            }
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExportDriver01 = new DataReportObject();
            StoredExportDriver01 = ExportDriver01;
            DataReportObject StoredExportDriver02 = new DataReportObject();
            StoredExportDriver02 = ExportDriver02;
            
            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExportDriver01);
            objWriter.ExportReportCollection.Add(StoredExportDriver02);
            objWriter.BuildExportFile(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2007);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=TechnicalDriver.xlsx");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        private void DisableParameterSelection()
        {
            this.pnlExportToolbar.Hidden = false;
            this.pnlReportToolbar.Hidden = true;
            this.ddlFilterTechnicalDriver1.ReadOnly = true;
            this.ddlField1.ReadOnly = true;
            this.ddlFilterProject1.ReadOnly = true;
            this.ddlFilterTechnicalDriver2.ReadOnly = true;
            this.ddlField2.ReadOnly = true;
            this.ddlFilterProject2.ReadOnly = true;
            this.pnlMainChartPanel.Hidden = false;
        }

        private void DisableDriverSave1()
        {
            this.btnSave1.Disabled = true;
        }

        private void DisableDriverSave2()
        {
            this.btnSave2.Disabled = true;
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        public void ModelCheckRecalcModuleReport()
        {
            clsModels objModelRecalc = new clsModels();
            objModelRecalc.IdModel = IdModel;
            objModelRecalc.CheckRecalcModuleParameter(objApplication.MaxRelationLevels);
            objModelRecalc.CheckRecalcModuleAllocation();
            objModelRecalc.CheckRecalcModuleTechnical();
            objModelRecalc.CheckRecalcModuleEconomic();
            objModelRecalc = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = modMain.strMasterTechnicalDriversForecast;
            this.btnReset.Text = modMain.strReportsModuleReset;
            this.btnSaveExcel.Text = modMain.strReportsModuleExportReportDataToExcel;
            this.Panel1.Title = modMain.strReportsModuleTechnicalDriverNum + "1";
            this.lblFilterTechnicalDriver1.Text = modMain.strReportsModuleDriver;
            this.lblFilterField1.Text = modMain.strTechnicalModuleField;
            this.lblFilterProject1.Text = modMain.strTechnicalModuleProject;
            this.btnSave1.Text = modMain.strCommonSaveChanges;
            this.Year.Text = modMain.strTechnicalModuleYear;
            this.Columns1.Text = modMain.strTechnicalModuleBase;
            this.Columns3.Text = modMain.strTechnicalModulePessimistic;
            this.Columns2.Text = modMain.strTechnicalModuleOptimistic;
            this.Panel2.Title = modMain.strReportsModuleTechnicalDriverNum + "2";
            this.lblFilterTechnicalDriver2.Text = modMain.strReportsModuleDriver;
            this.lblFilterField2.Text = modMain.strTechnicalModuleField;
            this.lblFilterProject2.Text = modMain.strTechnicalModuleProject;
            this.btnSave2.Text = modMain.strCommonSaveChanges;
            this.Column2.Text = modMain.strTechnicalModuleYear;
            this.NumberColumn1.Text = modMain.strTechnicalModuleBase;
            this.NumberColumn3.Text = modMain.strTechnicalModulePessimistic;
            this.NumberColumn2.Text = modMain.strTechnicalModuleOptimistic;
            this.btnRun.Text = modMain.strReportsModuleRunReport;
            this.ReportTitle01.Title = modMain.strReportsModuleResultsTitle;
        }

        #endregion

        #region "Report Data"

        private void LoadChartGroup()
        {
            if (Driver1Selected && Driver2Selected)
            {
                UpdatePanel(3);
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","Report Generated Successfully!"), IconCls = "icon-accept", Clear2 = false });
                this.tabDriver1.Render();
                this.tabDriver2.Render();
                this.tabDriver3.Render();
                this.tabCharts.SetActiveTab(this.tabDriver1);
            }
            else if (Driver1Selected)
            {
                UpdatePanel(1);
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","Report Generated Successfully!"), IconCls = "icon-accept", Clear2 = false });
                this.tabDriver1.Render();
                this.tabCharts.SetActiveTab(this.tabDriver1);
            }
            else if (Driver2Selected)
            {
                UpdatePanel(2);
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","Report Generated Successfully!"), IconCls = "icon-accept", Clear2 = false });
                this.tabDriver2.Render();
                this.tabCharts.SetActiveTab(this.tabDriver2);
            }
            else
            {
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","Please select values for both driver #1 and driver #2"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        public void UpdatePanel(Int32 intType = 0)
        {
            if (intType == 1)
            {
                this.tabDriver1.Hidden = false;
                this.tabDriver1.Title = ddlFilterTechnicalDriver1.SelectedItem.Text;
                LoadDriverChart(this.storeChart1, this.chtChart1, strTechnicalDriver1Unit, strTechnicalDriver2Unit, 1);
            }
            if (intType == 2)
            {
                this.tabDriver2.Hidden = false;
                this.tabDriver2.Title = ddlFilterTechnicalDriver2.SelectedItem.Text;
                LoadDriverChart(this.storeChart2, this.chtChart2, strTechnicalDriver1Unit, strTechnicalDriver2Unit, 2);
            }
            if (intType == 3)
            {
                this.tabDriver1.Hidden = false;
                this.tabDriver1.Title = ddlFilterTechnicalDriver1.SelectedItem.Text;
                this.tabDriver2.Hidden = false;
                this.tabDriver2.Title = ddlFilterTechnicalDriver2.SelectedItem.Text;
                this.tabDriver3.Hidden = false;
                this.tabDriver3.Title = ddlFilterTechnicalDriver1.SelectedItem.Text + " vs " + ddlFilterTechnicalDriver2.SelectedItem.Text;
                LoadDriverChart(this.storeChart1, this.chtChart1, strTechnicalDriver1Unit, strTechnicalDriver2Unit, 1);
                LoadDriverChart(this.storeChart2, this.chtChart2, strTechnicalDriver1Unit, strTechnicalDriver2Unit, 2);
                LoadDriverChart(this.storeChart3, this.chtChart3, strTechnicalDriver1Unit, strTechnicalDriver2Unit, 3);
            }
        }

        public void LoadDriverChart(Ext.Net.Store s, Ext.Net.Chart c, String strSerie1, String strSerie2, Int32 intID)
        {
            CategoryAxis AxesCat = new CategoryAxis();
            NumericAxis AxesL = new NumericAxis();
            NumericAxis AxesR = new NumericAxis();

            AxisLabel AxisLbl = new AxisLabel();
            AxisLbl.Renderer.Handler = "return Ext.util.Format.number(value, '0,000.00');";

            if (intID == 1)
            {
                s.AddField(new ModelField("Name", ModelFieldType.Auto));
                s.AddField(new ModelField("Normal", ModelFieldType.Float));
                s.AddField(new ModelField("Optimistic", ModelFieldType.Float));
                s.AddField(new ModelField("Pessimistic", ModelFieldType.Float));
                s.DataSource = LineChartData.GenerateData(Convert.ToInt32(ddlFilterTechnicalDriver1.SelectedItem.Value), Convert.ToInt32(ddlField1.SelectedItem.Value), Convert.ToInt32(ddlFilterProject1.SelectedItem.Value));
            }
            if (intID == 2)
            {
                s.AddField(new ModelField("Name", ModelFieldType.Auto));
                s.AddField(new ModelField("Normal", ModelFieldType.Float));
                s.AddField(new ModelField("Optimistic", ModelFieldType.Float));
                s.AddField(new ModelField("Pessimistic", ModelFieldType.Float));
                s.DataSource = LineChartData.GenerateData(Convert.ToInt32(ddlFilterTechnicalDriver2.SelectedItem.Value), Convert.ToInt32(ddlField2.SelectedItem.Value), Convert.ToInt32(ddlFilterProject2.SelectedItem.Value));
            }
            if (intID == 3)
            {
                s.AddField(new ModelField("Name", ModelFieldType.Auto));
                s.AddField(new ModelField("Data1", ModelFieldType.Float));
                s.AddField(new ModelField("Data2", ModelFieldType.Float));
                s.DataSource = LineChartData.GenerateData2(Convert.ToInt32(ddlFilterTechnicalDriver1.SelectedItem.Value), Convert.ToInt32(ddlField1.SelectedItem.Value), Convert.ToInt32(ddlFilterProject1.SelectedItem.Value), Convert.ToInt32(ddlFilterTechnicalDriver2.SelectedItem.Value), Convert.ToInt32(ddlField2.SelectedItem.Value), Convert.ToInt32(ddlFilterProject2.SelectedItem.Value));
            }
            s.DataBind();
            
            if (intID == 3)
            {
                AxesCat.Fields = new string[] { "Name" };
                AxesCat.Title = (String)GetGlobalResourceObject("CPM_Resources","Year");
                AxesCat.Position = Position.Bottom;

                AxesL.Title = (String)GetGlobalResourceObject("CPM_Resources",strSerie1);
                AxesL.Fields = new string[] { "Data1" };
                AxesL.Position = Ext.Net.Position.Left;
                AxesL.Minimum = 0;
                AxesL.Title = (String)GetGlobalResourceObject("CPM_Resources",strTechnicalDriver1Unit);
                AxesL.GridConfig = new AxisGrid()
                {
                    Odd = new SpriteAttributes()
                    {
                        Opacity = 1,
                        Fill = "#EFEBEF",
                        Stroke = "#EFEBEF",
                        StrokeWidth = 0.5
                    }
                };
                AxesL.Label = AxisLbl;

                AxesR.Title = (String)GetGlobalResourceObject("CPM_Resources",strSerie2);
                AxesR.Fields = new string[] { "Data2" };
                AxesR.Position = Ext.Net.Position.Right;
                AxesR.Minimum = 0;
                AxesR.Title = (String)GetGlobalResourceObject("CPM_Resources", strTechnicalDriver2Unit);
                //AxesR.GridConfig = new AxisGrid()
                //{
                //    Odd = new SpriteAttributes()
                //    {
                //        Opacity = 1,
                //        Fill = "#EFEBEF",
                //        Stroke = "#EFEBEF",
                //        StrokeWidth = 0.5
                //    }
                //};
                AxesR.Label = AxisLbl;

                SpriteAttributes SpriteAttributes1 = new SpriteAttributes();
                SpriteAttributes1.Radius = 7;
                SpriteAttributes1.Size = 7;

                LineSeries Line1 = new LineSeries();
                Line1.Axis = Position.Left;
                Line1.Highlight = true;
                Line1.XField = new string[] { "Name" };
                Line1.YField = new string[] { "Data1" };
                Line1.Title = ddlFilterTechnicalDriver1.SelectedItem.Text + " (Base Scenario) ";
                Line1.Smooth = 3;
                Line1.HighlightConfig = SpriteAttributes1;

                LineSeries Line2 = new LineSeries();
                Line2.Axis = Position.Right;
                Line2.Highlight = true;
                Line2.XField = new string[] { "Name" };
                Line2.YField = new string[] { "Data2" };
                Line2.Title = ddlFilterTechnicalDriver2.SelectedItem.Text + " (Base Scenario) ";
                Line2.Smooth = 3;
                Line2.HighlightConfig = SpriteAttributes1;

                c.Series.Add(Line1);
                c.Series.Add(Line2);
                c.Axes.Add(AxesCat);
                c.Axes.Add(AxesL);
                c.Axes.Add(AxesR);
            }
            else
            {
                AxesCat.Fields = new string[] { "Name" };
                AxesCat.Title = (String)GetGlobalResourceObject("CPM_Resources","Year");
                AxesCat.Position = Position.Bottom;

                AxesL.Fields = new string[] { "Normal", "Optimistic", "Pessimistic" }; ;
                if (intID == 1)
                {
                    AxesL.Title = (String)GetGlobalResourceObject("CPM_Resources",strSerie1);
                }
                else
                {
                    AxesL.Title = (String)GetGlobalResourceObject("CPM_Resources",strSerie2);
                }
                AxesL.GridConfig = new AxisGrid()
                {
                    Odd = new SpriteAttributes()
                    {
                        Opacity = 1,
                        Fill = "#EFEBEF",
                        Stroke = "#EFEBEF",
                        StrokeWidth = 0.5
                    }
                };
                AxesL.Label = AxisLbl;

                LineSeries Line11 = new LineSeries();
                Line11.Axis = Position.Left;
                Line11.XField = new string[] { "Name" };
                Line11.YField = new string[] { "Normal" };
                Line11.Title = modMain.strTechnicalModuleBase;//"Base";
                Line11.Smooth = 3;
                Line11.HighlightConfig = new SpriteAttributes()
                {
                    Radius = 7,
                    Size = 7,
                };
                Line11.MarkerConfig = new SpriteAttributes()
                {
                    Type = SpriteType.Circle,
                    Size = 4,
                    Radius = 4,
                    Title = modMain.strTechnicalModuleOptimistic,//"Optimistic",
                    StrokeWidth = 0
                };

                LineSeries Line12 = new LineSeries();
                Line12.Axis = Position.Left;
                Line12.XField = new string[] { "Name" };
                Line12.YField = new string[] { "Optimistic" };
                Line12.Title = modMain.strTechnicalModuleOptimistic;// "Optimistic";
                Line12.Smooth = 3;
                Line12.HighlightConfig = new SpriteAttributes()
                {
                    Radius = 7,
                    Size = 7,
                };
                Line12.MarkerConfig = new SpriteAttributes()
                {
                    Type = SpriteType.Cross,
                    Size = 4,
                    Radius = 4,
                    Title = modMain.strTechnicalModuleOptimistic,//"Optimistic",
                    StrokeWidth = 0
                };

                LineSeries Line13 = new LineSeries();
                Line13.Axis = Position.Left;
                Line13.XField = new string[] { "Name" };
                Line13.YField = new string[] { "Pessimistic" };
                Line13.Title = modMain.strTechnicalModulePessimistic;// "Pessimistic";
                Line13.Smooth = 3;
                Line13.HighlightConfig = new SpriteAttributes()
                {
                    Radius = 7,
                    Size = 7,
                };
                Line12.MarkerConfig = new SpriteAttributes()
                {
                    Type = SpriteType.Diamond,
                    Size = 4,
                    Radius = 4,
                    Title = modMain.strTechnicalModulePessimistic,//"Pessimistic",
                    StrokeWidth = 0
                };

                c.Series.Add(Line11);
                c.Series.Add(Line13);
                c.Series.Add(Line12);
                c.Axes.Add(AxesCat);
                c.Axes.Add(AxesL);
            }
            //if (intID == 3)
            //{
            //    c.StandardTheme = StandardChartTheme.Category5;
            //}
            //else
            //{
            //    c.StandardTheme = StandardChartTheme.Category1;
            //}

            c.Height = 600;
            c.Animate = true;
            c.Shadow = true;
            c.Frame = true;
            c.StyleSpec = "background:#fff;";
            
            VerticalMarker vm = new VerticalMarker();
            vm.Snap = true;
            c.Plugins.Add(vm);
            c.LegendConfig = new ChartLegend() { Position = LegendPosition.Bottom };
        }

        public List<LineChartData> GenerateLineChartData(Int32 IdTechnicalDriverCond, Int32 IdFieldCond, Int32 IdProjectCond)
        {
            return LineChartData.GenerateData(IdTechnicalDriverCond, IdFieldCond, IdProjectCond);
        }

        public class LineChartData
        {

            public string Name { get; set; }
            public double Normal { get; set; }
            public double Optimistic { get; set; }
            public double Pessimistic { get; set; }
            public double Data1 { get; set; }
            public double Data2 { get; set; }
            public double Data3 { get; set; }
            public double Data4 { get; set; }
            public double Data5 { get; set; }
            public double Data6 { get; set; }

            public static List<LineChartData> GenerateData(int _IdTechnicalDriver, int _IdField, int _IdProject)
            {
                DataClass.clsTechnicalDriverData objTechnicalDriverData1 = new DataClass.clsTechnicalDriverData();
                DataTable dt = objTechnicalDriverData1.LoadList("IdProject = '" + _IdProject + "' AND IdField = '" + _IdField + "' AND IdTechnicalDriver = '" + _IdTechnicalDriver + "'", "ORDER BY Year");
                List<LineChartData> data = new List<LineChartData>(dt.Rows.Count);

                foreach (DataRow objRow in dt.Rows)
                {
                    string strDriver1Baseline = objRow["Normal"].ToString() != "" ? objRow["Normal"].ToString() : null;
                    string strDriver1Optimistic = objRow["Optimistic"].ToString() != "" ? objRow["Optimistic"].ToString() : null;
                    string strDriver1Pessimistic = objRow["Pessimistic"].ToString() != "" ? objRow["Pessimistic"].ToString() : null;
                    string intYear = objRow["Year"].ToString() != "" ? objRow["Year"].ToString() : null;
                    data.Add(new LineChartData
                    {
                        Name = intYear,
                        Normal = Convert.ToDouble(strDriver1Baseline),
                        Optimistic = Convert.ToDouble(strDriver1Optimistic),
                        Pessimistic = Convert.ToDouble(strDriver1Pessimistic)
                    });
                }
                return data;
            }

            public static List<LineChartData> GenerateData2(int _IdTechnicalDriver1, int _IdField1, int _IdProject1, int _IdTechnicalDriver2, int _IdField2, int _IdProject2)
            {
                DataClass.clsTechnicalDriverData objTechnicalDriverData1 = new DataClass.clsTechnicalDriverData();
                DataClass.clsModels objModels = new DataClass.clsModels();

                DataTable dt1 = objTechnicalDriverData1.LoadList("IdProject = '" + _IdProject1 + "' AND IdField = '" + _IdField1 + "' AND IdTechnicalDriver = '" + _IdTechnicalDriver1 + "'", "ORDER BY Year");
                DataTable dt2 = objTechnicalDriverData1.LoadList("IdProject = '" + _IdProject2 + "' AND IdField = '" + _IdField2 + "' AND IdTechnicalDriver = '" + _IdTechnicalDriver2 + "'", "ORDER BY Year");
                List<LineChartData> data = new List<LineChartData>(dt1.Rows.Count);

                Int32 intMinYear = 0;
                Int32 intMaxYear = 0;

                Int32 intMinYear1 = 0;
                Int32 intMaxYear1 = 0;

                Int32 _idModel = 0;

                if (dt1.Rows.Count > 0)
                {
                    _idModel = (Int32)dt1.Rows[0]["IdModel"];
                    intMinYear1 = (int)dt1.Compute("min(Year)", "");
                    intMaxYear1 = (int)dt1.Compute("max(Year)", "");
                }

                Int32 intMinYear2 = 0;
                Int32 intMaxYear2 = 0;

                if (dt2.Rows.Count > 0)
                {
                    _idModel = (Int32)dt2.Rows[0]["IdModel"];
                    intMinYear2 = (int)dt2.Compute("min(Year)", "");
                    intMaxYear2 = (int)dt2.Compute("max(Year)", "");
                }


                if (intMinYear1 < intMinYear2)
                {
                    intMinYear = intMinYear1;
                }
                else
                {
                    intMinYear = intMinYear2;
                }

                if (intMaxYear1 > intMaxYear2)
                {
                    intMaxYear = intMaxYear1;
                }
                else
                {
                    intMaxYear = intMaxYear2;
                }

                Int32 intDriver1 = dt1.Rows.Count;
                Int32 intDriver2 = dt2.Rows.Count;

                DataTable dt = new DataTable();
                dt.Columns.Add("Year");
                dt.Columns.Add("Data1");
                dt.Columns.Add("Data2");

                if (intMinYear == 0)
                    intMinYear = (Int32)objModels.LoadList("IdModel =" + _idModel, "").Rows[0]["BaseYear"];

                while (intMinYear <= intMaxYear)
                {
                    DataRow dr = dt.NewRow();
                    dr["Year"] = intMinYear;
                    dr["Data1"] = 0;
                    dr["Data2"] = 0;

                    dt.Rows.Add(dr);
                    dt.AcceptChanges();

                    intMinYear = intMinYear + 1;
                }
                foreach (DataRow dr in dt.Rows)
                {
                    Double dblValue1 = 0;
                    Double dblValue2 = 0;

                    DataRow[] Filter1;
                    Filter1 = dt1.Select("Year = " + dr["Year"].ToString().Trim());


                    foreach (DataRow row in Filter1)
                    {
                        dblValue1 = dblValue1 + Convert.ToDouble(row["Normal"]);
                    }
                    dr["Data1"] = dblValue1;

                    DataRow[] Filter2;
                    Filter2 = dt2.Select("Year = " + dr["Year"].ToString().Trim());


                    foreach (DataRow row in Filter2)
                    {
                        dblValue2 = dblValue2 + Convert.ToDouble(row["Normal"]);
                    }
                    dr["Data2"] = dblValue2;

                }
                Int32 intRowIndex = 0;
                foreach (DataRow objRow in dt.Rows)
                {
                    string intYear = objRow["Year"].ToString() != "" ? objRow["Year"].ToString() : null;
                    string strDriver1Baseline = objRow["Data1"].ToString() != "" ? objRow["Data1"].ToString() : null;
                    string strDriver2Baseline = objRow["Data2"].ToString() != "" ? objRow["Data2"].ToString() : null;

                    data.Add(new LineChartData
                    {
                        Name = Convert.ToString(intYear),
                        Data1 = Convert.ToDouble(strDriver1Baseline),
                        Data2 = Convert.ToDouble(strDriver2Baseline)
                    });
                    intRowIndex++;
                }
                return data;
            }

        }

        #endregion

    }
}
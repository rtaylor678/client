﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="costvsdriversprojection.aspx.cs" Inherits="CPM._costvsdriversprojection" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var formatCol = function (value, metaData, record, rowIndex, colIndex, store, view) {
            var str = record.data.Unit;
            if (value != "") {
                if (record.data.Unit != "") {
                    metaData.tdAttr = 'style="background-color:#0076BC;color:white;font-weight:bold;font-size: 16px;"';
                }
                if (record.data.Unit == "SubTotal") {
                    metaData.tdAttr = 'style="font-weight:bold"';
                    if (value == "SubTotal") {
                        value = "";
                    }
                }

                return value;
            }

            return value;
        }

        var formatColData = function (value, metaData, record, rowIndex, colIndex, store, view) {
            var str = record.data.Unit;
            if (str != "") {
                if (str == "SubTotal") {
                    metaData.tdAttr = 'style="font-weight:bold"';
                    value = Ext.util.Format.number(value, '0,000.00');
                }
                else {
                    value = "";
                    metaData.tdAttr = 'style="background-color:#0076BC;color:white;"';
                }
                return value;
            }

            if (value == 0) {
                return "";
            }

            value = Ext.util.Format.number(value, '0,000.00');
            return value;
        }

        Ext.num = function (v, defaultValue) {
            v = Number(Ext.isEmpty(v) || Ext.isArray(v) || typeof v == 'boolean' || (typeof v == 'string' && v.trim().length == 0) ? NaN : v);
            return isNaN(v) ? defaultValue : v;
        }

        Ext.util.Format.number = function (v, format) {
            if (!format) {
                return v;
            }
            v = Ext.num(v, NaN);
            if (isNaN(v)) {
                return '';
            }
            var comma = ',',
                dec = '.',
                i18n = false,
                neg = v < 0;

            v = Math.abs(v);
            if (format.substr(format.length - 2) == '/i') {
                format = format.substr(0, format.length - 2);
                i18n = true;
                comma = '.';
                dec = ',';
            }

            var hasComma = format.indexOf(comma) != -1,
                psplit = (i18n ? format.replace(/[^\d\,]/g, '') : format.replace(/[^\d\.]/g, '')).split(dec);

            if (1 < psplit.length) {
                v = v.toFixed(psplit[1].length);
            } else if (2 < psplit.length) {
                throw ('NumberFormatException: invalid format, formats should have no more than 1 period: ' + format);
            } else {
                v = v.toFixed(0);
            }

            var fnum = v.toString();

            psplit = fnum.split('.');

            if (hasComma) {
                var cnum = psplit[0],
                    parr = [],
                    j = cnum.length,
                    m = Math.floor(j / 3),
                    n = cnum.length % 3 || 3,
                    i;

                for (i = 0; i < j; i += n) {
                    if (i != 0) {
                        n = 3;
                    }

                    parr[parr.length] = cnum.substr(i, n);
                    m -= 1;
                }
                fnum = parr.join(comma);
                if (psplit[1]) {
                    fnum += dec + psplit[1];
                }
            } else {
                if (psplit[1]) {
                    fnum = psplit[0] + dec + psplit[1];
                }
            }

            return (neg ? '-' : '') + format.replace(/[\d,?\.?]+/, fnum);
        }
    </script>
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:ChartTheme ID="ChartThemeSA" runat="server" ThemeName="SATheme" Colors="#4A7DC6,#CE3018,#39BE39,#FFCB4A,#7B498C,#7BCBFF,#E79642,#B53C6B,#4A4D4A,#BDD7EF,#104110,#F7BACE,#395D73">
        <AxisLabelLeft Font="10px" FontFamily="Arial" />
        <AxisLabelRight Font="10px" FontFamily="Arial" />
        <AxisLabelBottom Font="10px" FontFamily="Arial" />
        <AxisTitleLeft Fill="rgb(8,69,148)" Font="bold 18px Arial" />
        <AxisTitleRight Fill="rgb(8,69,148)" Font="bold 18px Arial" />
        <AxisTitleBottom Fill="rgb(8,69,148)" Font="bold 18px Arial" />
    </ext:ChartTheme>
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" Layout="FitLayout" Title="Cost vs Drivers Projection" IconCls="icon-opex_16" AutoScroll="true" runat="server" Border="false" Cls="reportheader">
                <Items>
                    <ext:Panel ID="Panel2" runat="server" AutoScroll="true">
                        <Items>
                            <ext:Panel ID="pnlExportToolbar" runat="server" Border="false" Hidden="true">
                                <TopBar>
                                    <ext:Toolbar ID="tbrExport" Border="false" Height="30" runat="server">
                                        <Items>
                                            <ext:Button ID="btnReset" runat="server" Text="Reset" Icon="Reload">
                                                <DirectEvents>
                                                    <Click OnEvent="btnReset_Click">
                                                        <EventMask ShowMask="true" />
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                            <ext:ToolbarFill />
                                            <ext:Button ID="btnSaveExcel" runat="server" Text="Export Report Data To Excel" Icon="PageExcel" Disabled="false">
                                                <DirectEvents>
                                                    <Click OnEvent="btnSaveExcel_Click" IsUpload="true">
                                                        <EventMask ShowMask="true" />
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                            </ext:Panel>
                            <ext:Panel ID="pnlButtons01" runat="server" Layout="Column" Border="false" Style="margin: 0 0 0 0">
                                <Items>
                                    <ext:Panel ID="Panel100" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                                        <Items>
                                            <ext:Label Style="margin: 10px 0 10px 10px" ID="lblLevels" runat="server" Text="Aggregation Level:" Visible="true"></ext:Label>
                                            <ext:ComboBox Style="margin: 10px 0 10px 10px" ID="ddlLevels" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" ValueField="IdAggregationLevel" Visible="true">
                                                <ListConfig LoadingText="Searching..." MinWidth="250">
                                                    <ItemTpl ID="ItemTpl2" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
							                                    <span>Parent: {ParentName}</span>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeLevels" runat="server" OnReadData="StoreLevel_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model2" runat="server" IDProperty="IdAggregationLevel">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdAggregationLevel" />
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="ParentName" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                                <DirectEvents>
                                                    <Select OnEvent="ddlLevels_Select" />
                                                </DirectEvents>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel101" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                                        <Items>
                                            <ext:Label Style="margin: 10px 0 10px 10px" ID="lblField" runat="server" Text="Field:"></ext:Label>
                                            <ext:ComboBox Style="margin: 10px 0 10px 10px" ID="ddlField" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" ValueField="IdField">
                                                <ListConfig LoadingText="Searching..." MinWidth="250">
                                                    <ItemTpl ID="ItemTpl9" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
							                                    <span>Code: </span>{Code}</h3>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeField" runat="server" OnReadData="StoreField_ReadData">
                                                        <Model>
                                                            <ext:Model ID="mdlLabels" runat="server" IDProperty="IdField">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdField" />
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="Code" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                                <DirectEvents>
                                                    <Select OnEvent="ddlField_Select" />
                                                </DirectEvents>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel102" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                                        <Items>
                                            <ext:Label Style="margin: 10px 0 10px 10px" ID="lblOperationType" runat="server" Text="Operation Type:" Visible="false"></ext:Label>
                                            <ext:ComboBox Style="margin: 10px 0 10px 10px" ID="ddlOperationType" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" ValueField="IdTypeOperation" Visible="false">
                                                <ListConfig LoadingText="Searching..." MinWidth="250">
                                                    <ItemTpl ID="ItemTpl5" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
							                                    <span>Code: {IdTypeOperation}</span>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeOperationType" runat="server" OnReadData="StoreOperationType_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model6" runat="server" IDProperty="IdTypeOperation">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdTypeOperation" />
                                                                    <ext:ModelField Name="Name" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel105" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                                        <Items>
                                            <ext:Label Style="margin: 10px 0 10px 10px" ID="lblCostCategory" runat="server" Text="Cost Category:"></ext:Label>
                                            <ext:MultiCombo Style="margin: 10px 0 10px 10px" ID="ddlCostCategory" runat="server" SelectionMode="All" MinWidth="150" Width="300"
                                                DisplayField="Name"
                                                ValueField="IdZiffAccount" 
                                                Selectable="true" 
                                                QueryMode="Local" 
                                                ForceSelection="true" 
                                                TriggerAction="All" 
                                                SelectOnFocus="true" 
                                                AllowBlank="true">
                                                <Store>
                                                    <ext:Store ID="storeCostCategory" runat="server" OnReadData="StoreCostCategory_ReadData">
                                                        <Model>
                                                            <ext:Model ID="mdlCostCategories" runat="server" IDProperty="IdZiffAccount">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdZiffAccount" />
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="Code" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                                <DirectEvents>
                                                    <Select OnEvent="ddlCostCategory_Select" />
                                                </DirectEvents>
                                            </ext:MultiCombo>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel103" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                                        <Items>
                                            <ext:Label Style="margin: 10px 0 10px 10px" ID="lblCurrency" runat="server" Text="Currency:"></ext:Label>
                                            <ext:ComboBox Style="margin: 10px 0 10px 10px" ID="ddlCurrency" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" ValueField="IdCurrency" Width="150">
                                                <ListConfig LoadingText="Searching..." MinWidth="250">
                                                    <ItemTpl ID="ItemTpl4" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
							                                    <span>Code: {Code}</span>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeBase" runat="server" OnReadData="StoreCurr_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model4" runat="server" IDProperty="IdCurrency">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdCurrency" />
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="Code" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel104" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                                        <Items>
                                            <ext:Label Style="margin: 10px 0 10px 10px" ID="lblFactor" runat="server" Text="Factor:"></ext:Label>
                                            <ext:ComboBox Style="margin: 10px 0 10px 10px" Editable="false" ID="ddlFactor" runat="server" Width="55">
                                                <Items>
                                                    <ext:ListItem Text="1" Value="1" Index="0" />
                                                    <ext:ListItem Text="1000" Value="2" />
                                                </Items>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                               </Items>
                            </ext:Panel>
                            <ext:Panel runat="server" Border="true"></ext:Panel>
                            <ext:Panel ID="pnlButtons02" runat="server" Layout="Column" Border="false" Style="margin: 0 0 0 0">
                                <Items>
                                    <ext:Panel ID="Panel200" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0" visible="false">
                                        <Items>
                                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lblStructure" runat="server" Text="Cost Structure:"></ext:Label>
                                            <ext:ComboBox Style="margin: 10px 10px 10px 10px" Editable="false" ID="ddlStructure" runat="server" Width="100">
                                                <Items>
                                                    <ext:ListItem Text="Ziff Energy" Value="1" Index="0" />
                                                    <ext:ListItem Text="Company" Value="2" />
                                                </Items>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel201" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                                        <Items>
                                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lblCostType" runat="server" Text="Cost Type:"></ext:Label>
                                            <ext:ComboBox Style="margin: 10px 10px 10px 10px" ID="ddlCostType" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" ValueField="IdCostType">
                                                <ListConfig LoadingText="Searching..." MinWidth="250">
                                                    <ItemTpl ID="ItemTpl3" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
							                                    <span>Code: {Code}</span>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeCostType" runat="server" OnReadData="StoreCostType_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model5" runat="server" IDProperty="IdCostType">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdCostType" />
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="Code" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel202" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                                        <Items>
                                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lblScenarioTechnical" runat="server" Text="Technical Scenario:"></ext:Label>
                                            <ext:ComboBox Style="margin: 10px 10px 10px 10px" ID="ddlScenarioTechnical" runat="server"  DisplayField="Name" Editable="false" TypeAhead="false" PageSize="10" MinChars="0" ValueField="IdScenarioType" Width="80">
                                                <ListConfig LoadingText="Searching..." MinWidth="250">
                                                    <ItemTpl ID="ItemTpl6" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
							                                    <span>Code: {Code}</span>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeTechnicalScenarioType" runat="server" OnReadData="StoreTechnicalScenarioType_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model7" runat="server" IDProperty="IdScenarioType">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdScenarioType" />
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="Code" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel203" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                                        <Items>
                                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lblScenarioEconomic" runat="server" Text="Economic Scenario:"></ext:Label>
                                            <ext:ComboBox Style="margin: 10px 10px 10px 10px" ID="ddlScenarioEconomic" runat="server"  DisplayField="Name" Editable="false" TypeAhead="false" PageSize="10" MinChars="0" ValueField="IdScenarioType" Width="80">
                                                <ListConfig LoadingText="Searching..." MinWidth="250">
                                                    <ItemTpl ID="ItemTpl7" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
							                                    <span>Code: {Code}</span>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeEconomicScenarioType" runat="server" OnReadData="StoreEconomicScenarioType_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model8" runat="server" IDProperty="IdScenarioType">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdScenarioType" />
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="Code" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel204" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                                        <Items>
                                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lblTerm" runat="server" Text="Term:"></ext:Label>
                                            <ext:ComboBox Style="margin: 10px 10px 10px 10px" Editable="false" ID="ddlTerm" runat="server" Width="80">
                                                <Items>
                                                    <ext:ListItem Text="Nominal" Value="1" Index="0" />
                                                    <ext:ListItem Text="Real" Value="2" />
                                                </Items>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel205" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                                        <Items>
                                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lblFrom" runat="server" Text="From:"></ext:Label>
                                            <ext:NumberField Style="margin: 10px 10px 10px 10px" ID="txtFrom" DecimalPrecision="0" MinValue="0" Text="2014" runat="server" Width="55">
                                            </ext:NumberField>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel206" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                                        <Items>
                                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lblTo" runat="server" Text="To:"></ext:Label>
                                            <ext:NumberField Style="margin: 10px 10px 10px 10px" ID="txtTo" DecimalPrecision="0" MinValue="0" Text="2023" runat="server" Width="55">
                                                <DirectEvents>
                                                    <Change OnEvent="txtTo_Select" />
                                                </DirectEvents>
                                            </ext:NumberField>
                                        </Items>
                                    </ext:Panel>
                               </Items>
                            </ext:Panel>
                            <ext:Panel runat="server" Border="true"></ext:Panel>
                            <ext:Panel ID="pnlButtons03" runat="server" Layout="Column" Border="false" Style="margin: 0 0 0 0">
                                <Items>
                                    <ext:Panel ID="Panel207" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                                        <Items>
                                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lblFilterTechnicalDriver1" runat="server" Text="Technical Driver" />
                                            <ext:ComboBox Style="margin: 10px 10px 10px 10px" MinChars="0" ID="ddlFilterTechnicalDriver1" runat="server" Width="200" DisplayField="Name" ValueField="ID">
                                                <ListConfig LoadingText="Searching..." MinWidth="300">
                                                    <ItemTpl ID="ItemTpl1" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
							                                    <span>ID: {ID}</span>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeFilterTechnicalDriver1" runat="server" OnReadData="StoreFilterTechnicalDriver1_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model1" runat="server" IDProperty="ID">
                                                                <Fields>
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="ID" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                                <DirectEvents>
                                                    <Select OnEvent="ddlFilterTechnicalDriver1_Select" />
                                                </DirectEvents>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel208" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                                        <Items>
                                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lblFilterEconomicDriver" runat="server" Text="Economic Driver" />
                                            <ext:ComboBox Style="margin: 10px 10px 10px 10px" MinChars="0" ID="ddlFilterEconomicDriver" runat="server" Width="200" DisplayField="Name" ValueField="ID">
                                                <ListConfig LoadingText="Searching..." MinWidth="300">
                                                    <ItemTpl ID="ItemTpl8" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
							                                    <span>ID: {ID}</span>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeFilterEconomicDriver" runat="server" OnReadData="StoreFilterEconomicDriver_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model3" runat="server" IDProperty="ID">
                                                                <Fields>
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="ID" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                                <DirectEvents>
                                                    <Select OnEvent="ddlFilterEconomicDriver_Select" />
                                                </DirectEvents>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="pnlReportToolbar" runat="server" Border="false" Hidden="false">
                                <TopBar>
                                    <ext:Toolbar ID="tbrReport" Border="false" Height="30" runat="server">
                                        <Items>
                                            <ext:Button ID="btnRun" runat="server" Text="Run Report" Icon="PlayGreen" Disabled="false">
                                                <DirectEvents>
                                                    <Click Timeout="3600000" ShowWarningOnFailure="false" OnEvent="btnRun_Click">
                                                        <EventMask ShowMask="true" />
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                            <ext:ToolbarFill />
                                            <ext:Button ID="btnDirectExcel" runat="server" Text="Export Data To Excel" Icon="PageExcel" Disabled="false">
                                                <DirectEvents>
                                                    <Click Timeout="3600000" OnEvent="btnSaveExcelDirect_Click" IsUpload="true">
                                                        <EventMask ShowMask="true" />
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                            </ext:Panel>
                            <ext:Panel ID="ReportTitle01" runat="server" IconCls="icon-opex_16" Border="false" Title="Total Projection" Cls="reportsubheader"></ext:Panel>
                            <ext:Panel ID="pnlMainChartPanel" Border="false" ManageHeight="true" runat="server" Hidden="true" Visible="true">
                                <Items>
                                    <ext:Panel ID="pnlChartObject" runat="server" Border="false" Icon="ChartBar" Title="Multiple Fields Projection" Layout="FitLayout" Header="false" AutoScroll="true" MinHeight="0" MaxHeight="850" MaxWidth="1750" Resizable="true">
                                        <Items>
                                            <ext:Chart ID="chtChart" runat="server" Border="false" Animate="true" Theme="SATheme" Layout="FitLayout" MinHeight="0" Resizable="true" AutoSize="true">
                                                <LegendConfig Position="Right" LabelFont="10px" ItemSpacing="0" />
                                                <Store>
                                                    <ext:Store ID="storeChart" runat="server">
                                                        <Model>
                                                            <ext:Model ID="modelChart" runat="server">
                                                                <Fields>
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                            </ext:Chart>
                                        </Items>
                                    </ext:Panel>
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="pnlOpexProjection" runat="server" Border="false">
                                <Items>
                                    <ext:GridPanel ID="grdOpexProjection" runat="server" 
                                        Cls="x-grid-custom"
                                        Border="false" 
                                        Header="false" 
                                        StripeRows="true"
                                        EnableLocking="true">
                                        <Store>
                                            <ext:Store ID="storeBaseCostField" runat="server">
                                                <Model>
                                                    <ext:Model ID="modelField" runat="server">
                                                        <Fields>
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel RenderColumnsOnly="False" RenderXType="True" IDMode="Explicit" Namespace="App" IsDynamic="False">
                                            <Columns>
                                            </Columns>
                                        </ColumnModel>
                                        <Listeners>
                                            <Reconfigure Handler="this.setHeight(this.container.getHeight()+20);" Delay="100" />
                                        </Listeners>
                                    </ext:GridPanel>
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="PanelDriversChartPanel" Border="false" ManageHeight="true" runat="server" Hidden="true" Visible="true">
                                <Items>
                                    <ext:Panel ID="pnlDriversChartObject" runat="server" Border="false" MaxHeight="600" Icon="ChartBar" Title="Multiple Fields Projection" Layout="FitLayout" Header="false">
                                        <Items>
                                            <ext:Chart ID="chtDriversChart" runat="server" Border="false" Animate="true" MinHeight="0" MaxHeight="700" MaxWidth="1350" Theme="SATheme" InsetPadding="30" Layout="FitLayout" AutoSize="true">
                                                <LegendConfig Position="Right" LabelFont="10px" />
                                                <Store>
                                                    <ext:Store ID="storeDriversChart" runat="server">
                                                        <Model>
                                                            <ext:Model ID="modelDriversChart" runat="server">
                                                                <Fields>
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                            </ext:Chart>
                                        </Items>
                                    </ext:Panel>
                                </Items>
                            </ext:Panel>
                        </Items>
                    </ext:Panel>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="FormStatusBar" runat="server" />
                </BottomBar>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>

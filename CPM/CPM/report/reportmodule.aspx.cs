﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _reportmodule : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }

        private static modMain objMain = new modMain();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                this.LoadLanguage();
                this.LoadModules();

                if (this.GetEconomicModule())
                {
                    this.btnCostvsDriversProjection.Disable();
                    this.btnOperationalMarginsProjection.Disable();
                    this.btnOpexReport.Disable();
                    this.btnOpexReportDetailed.Disable();
                    this.btnMultiFieldsProjection.Disable();
                }
            }
        }

        /// Projection By Development Case
        protected void btnOpexReport_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/report/opexreport.aspx");
        }

        /// NOT USED
        protected void btnDriverReport_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/report/driverreport.aspx");
        }

        /// NOT USED
        protected void btnKPIReport_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/report/kpireport.aspx");
        }

        /// Projection By Cost Type
        protected void btnOpexReportDetailed_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/report/opexreportdetailed.aspx");
        }

        /// Multi Fields Projection
        protected void btnMultiFieldsProjection_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/report/multifieldsopexreport.aspx");
        }

        /// Cost vs Drivers Projection
        protected void btnCostvsDriversProjection_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/report/costvsdriversprojection.aspx");
        }

        /// Operational Margins Projection
        protected void btnOperationalMarginsProjection_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/report/operationalmarginsprojection.aspx");
        }

        /// Base Rates By Cost Category
        protected void btnBaseCostTechnicalModule_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/technical/basecosttechnicalmodule.aspx");
        }

        /// Base Rates By Drivers
        protected void btnBaseRatesByDrivers_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/technical/baseratesbydrivers.aspx");
        }

        /// Internal Base Cost References
        protected void btnBaseCostByField_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/basecostbyfield.aspx");
        }

        /// NOT USED
        protected void btnBaseCostByFieldZiff_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/basecostbyfieldziff.aspx");
        }

        /// External Base Cost References
        protected void btnZiffBaseCost_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/allocation/ziffbasecost.aspx");
        }

        /// Cost Benchmarking
        protected void btnCostBenchmarking_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/report/costbenchmarking.aspx");
        }
        
        #endregion

        #region Methods

        protected void LoadModules()
        {
            // Reports Module
            if (objApplication.AllowedRoles.Contains(40))
            {
                // Operational Margins Projection
                if (objApplication.AllowedRoles.Contains(60))
                {
                    this.btnOperationalMarginsProjection.Disabled = false;
                }
                // Projection by Cost Category
                if (objApplication.AllowedRoles.Contains(61))
                {
                    this.btnCostvsDriversProjection.Disabled = false;
                }
                // Projection by Development Case
                if (objApplication.AllowedRoles.Contains(41))
                {
                    this.btnOpexReport.Disabled = false;
                }
                // Projection by Cost Type
                if (objApplication.AllowedRoles.Contains(62))
                {
                    this.btnOpexReportDetailed.Disabled = false;
                }
                // Multiple Fields Projection
                if (objApplication.AllowedRoles.Contains(63))
                {
                    this.btnMultiFieldsProjection.Disabled = false;
                }
                // Competitiveness Cost Benchmarking
                if (objApplication.AllowedRoles.Contains(64))
                {
                    this.btnCostBenchmarking.Disabled = false;
                }
            }
        }

        protected void LoadLanguage()
        {
            DataClass.clsLabelsLanguages objLabelsLanguages = new DataClass.clsLabelsLanguages();
            DataTable dt = objLabelsLanguages.LoadList("LanguageName = '" + Language + "' AND FormName = 'ReportsModule'", "ORDER BY IdLabel");
            foreach (DataRow row in dt.Rows)
            {
                //idLanguage = (Int32)row["IdLanguage"];
                switch ((String)row["LabelName"])
                {
                    case "Reports Module":
                        modMain.strReportsModuleFormTitle = (String)row["LabelText"];
                        break;
                    case "Key Performance Indicators Estimates":
                        modMain.strReportsModuleKPIEstimatesLong = (String)row["LabelText"];
                        break;
                    case "Driver:":
                        modMain.strReportsModuleDriver = (String)row["LabelText"];
                        break;
                    case "Technical Driver #":
                        modMain.strReportsModuleTechnicalDriverNum = (String)row["LabelText"];
                        break;
                    case "Key Performance Cost Indicators Estimates":
                        modMain.strReportsModuleSubFormTitle3 = (String)row["LabelText"];
                        break;
                    case "Aggregation Level:":
                        modMain.strReportsModuleAggregationLevel = (String)row["LabelText"];
                        break;
                    case "Cost Structure:":
                        modMain.strReportsModuleCostStructure = (String)row["LabelText"];
                        break;
                    case "From:":
                        modMain.strReportsModuleFrom = (String)row["LabelText"];
                        break;
                    case "To:":
                        modMain.strReportsModuleTo = (String)row["LabelText"];
                        break;
                    case "Technical Scenario:":
                        modMain.strReportsModuleTechnicalScenario = (String)row["LabelText"];
                        break;
                    case "Economic Scenario:":
                        modMain.strReportsModuleEconomicScenario = (String)row["LabelText"];
                        break;
                    case "Currency:":
                        modMain.strReportsModuleCurrency = (String)row["LabelText"];
                        break;
                    case "Term:":
                        modMain.strReportsModuleTerm = (String)row["LabelText"];
                        break;
                    case "Run Report":
                        modMain.strReportsModuleRunReport = (String)row["LabelText"];
                        break;
                    case "KPI Target":
                        modMain.strReportsModuleKPITarget = (String)row["LabelText"];
                        break;
                    case "Years":
                        modMain.strReportsModuleYears = (String)row["LabelText"];
                        break;
                    case "Reset":
                        modMain.strReportsModuleReset = (String)row["LabelText"];
                        break;
                    case "Export Report Data To Excel":
                        modMain.strReportsModuleExportReportDataToExcel = (String)row["LabelText"];
                        break;
                    case "Technical Drivers Forecast Indicator":
                        modMain.strReportsModuleResultsTitle = (String)row["LabelText"];
                        break;
                    case "Total Projection":
                        modMain.strReportsModuleOpexResultsTitle1 = (String)row["LabelText"];
                        break;
                    case "Projection By Project":
                        modMain.strReportsModuleOpexResultsTitle2 = (String)row["LabelText"];
                        break;
                    case "Baseline":
                        modMain.strReportsModuleOpexResultsSubTitle1 = (String)row["LabelText"];
                        break;
                    case "Business Opportunities":
                        modMain.strReportsModuleOpexResultsSubTitle2 = (String)row["LabelText"];
                        break;
                    case "1. Cost Projection Reports":
                        modMain.strReportsModuleFormSubTitle1 = (String)row["LabelText"];
                        break;
                    case "2. Projection Base Rates Reports":
                        modMain.strReportsModuleFormSubTitle2 = (String)row["LabelText"];
                        break;
                    case "3. Historical Base Cost Reports":
                        modMain.strReportsModuleFormSubTitle3 = (String)row["LabelText"];
                        break;
                    case "Type of Operation":
                        modMain.strReportsModuleFormOperationType = (String)row["LabelText"];
                        break;
                    case "Cost Type":
                        modMain.strReportsModuleFormTypeCost = (String)row["Labeltext"];
                        break;
                }
            }
            this.LoadLabel();
        }

        protected void LoadLabel()
        {
            this.pnlParameters1.Title = modMain.strReportsModuleFormTitle;
            this.btnOpexReport.Text = objMain.FormatButtonText((String)GetGlobalResourceObject("CPM_Resources", "Projection By Cost Category"));//modMain.strMasterOperatingCostProjection);
            this.btnOpexReportDetailed.Text = objMain.FormatButtonText((String)GetGlobalResourceObject("CPM_Resources", "Projection By Case"));//modMain.strReportsModuleOperatingCostProjectionDetailed);
            this.btnMultiFieldsProjection.Text = objMain.FormatButtonText((String)GetGlobalResourceObject("CPM_Resources", "Multiple Fields Projection"));
            this.btnCostvsDriversProjection.Text = objMain.FormatButtonText((String)GetGlobalResourceObject("CPM_Resources", "CostVSDriversProjection"));
            this.btnOperationalMarginsProjection.Text = objMain.FormatButtonText((String)GetGlobalResourceObject("CPM_Resources", "OperationalMarginsProjection"));
            this.btnBaseCostTechnicalModule.Text = objMain.FormatButtonText(modMain.strTechnicalModuleBaseRatesByCostCategory);
            this.btnBaseRatesByDrivers.Text = objMain.FormatButtonText(modMain.strTechnicalModuleBaseRatesByDrivers);
            this.btnBaseCostByField.Text = objMain.FormatButtonText(modMain.strReportsModuleCompanyBaseCostReferences);
            this.btnZiffBaseCost.Text = objMain.FormatButtonText(modMain.strReportsModuleZiffBaseCostReferences);
            this.btnCostBenchmarking.Text = objMain.FormatButtonText((String)GetGlobalResourceObject("CPM_Resources", "CostBenchmarking"));
            this.FieldSet1.Title = (String)GetGlobalResourceObject("CPM_Resources", "CostProjectionReports");
            this.FieldSet2.Title = (String)GetGlobalResourceObject("CPM_Resources", "CostBenchmarkingReports");
        }

        public Boolean GetEconomicModule()
        {
            Boolean Value = false;
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("Level IN (1,2) AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = true;
            }

            return Value;
        }

        #endregion

    }
}
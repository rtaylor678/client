﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using Ext.Net.Utilities;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Diagnostics;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CD;
using DataClass;
using System.Web.Script.Serialization;
using System.Globalization;

namespace CPM
{
    public partial class _costbenchmarking : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdActivity { get { return (Int32)Session["IdActivity"]; } set { Session["IdActivity"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"]; } }//  set { Session["idLanguage"] = value; } } }
        private Boolean PageIsLoaded { get; set; }
        private DataReportObject ExportOpexTOT { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }
        private DataReportObject ExportOpexBL { get { return (DataReportObject)Session["Export02"]; } set { Session["Export02"] = value; } }
        private DataReportObject ExportOpexBO { get { return (DataReportObject)Session["Export03"]; } set { Session["Export03"] = value; } }
        private Int32 ReportFieldCount = 0;
        private Int32 ReportCategoriesCount = 0;
        private Int32 ReportPeerGroupCount = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (GetProject())
            {
                this.lblScenarioEconomic.Hide();
                this.ddlScenarioEconomic.Hide();
                this.lblTerm.Hide();
                this.ddlTerm.Hide();
            }

            if (!X.IsAjaxRequest)
            {
                LoadLabel();
                Session["Export01"] = null;
                Session["Export02"] = null;
                Session["Export03"] = null;
                PageIsLoaded = false;
                IdActivity = 0;
                this.ddlStructure.SelectedItem.Index = 0;
                this.ddlTerm.SelectedItem.Index = 1;
                this.ddlScenarioTechnical.SelectedItem.Index = 0;
                this.ddlScenarioEconomic.SelectedItem.Index = 0;
                this.ddlOperationType.SelectedItem.Index = 0;
                this.ddlCostType.SelectedItem.Index = 0;
                this.ddlFactor.SelectedItem.Index = 1;
                this.FromTo();
                DataTable dtRows = new DataTable();
                ArrayList datColumnOpex = new ArrayList();
                this.FillGrid(this.storeBaseCostField, this.grdOpexProjection, dtRows, ref datColumnOpex, false, null);
                PageIsLoaded = true;
            }
        }

        protected void ddlField_Select(object sender, DirectEventArgs e)
        {
            bool All = false;
            ddlField.DeselectItem(0);
            //this.ddlPeerGroup.Disabled = true;
            int i = 0;

            foreach (Ext.Net.ListItem field in this.ddlField.SelectedItems)
            {
                i++;
                if (Convert.ToInt32(field.Value) == 0 && i == this.ddlField.SelectedItems.Count)
                    All = true;
            }

            if (All)
            {
                ddlField.SelectItem(0);
                foreach (Ext.Net.ListItem items in this.ddlField.SelectedItems)
                {
                    if (Convert.ToInt32(items.Value) != 0)
                        this.ddlField.DeselectItem(items.Index);
                }
                //this.ddlPeerGroup.Disabled = false;
            }
        }

        protected void ddlPeerGroup_Select(object sender, DirectEventArgs e)
        {
            bool All = false;
            ddlPeerGroup.DeselectItem(0);
            int i = 0;

            foreach (Ext.Net.ListItem peerGroup in this.ddlPeerGroup.SelectedItems)
            {
                i++;
                if (Convert.ToInt32(peerGroup.Value) == 0 && i == this.ddlPeerGroup.SelectedItems.Count)
                    All = true;
            }

            if (All)
            {
                ddlPeerGroup.SelectItem(0);
                foreach (Ext.Net.ListItem items in this.ddlPeerGroup.SelectedItems)
                {
                    if (Convert.ToInt32(items.Value) != 0)
                        this.ddlPeerGroup.DeselectItem(items.Index);
                }
            }
            ddlField.Reset();
            storeField.Reload();
        }

        protected void btnReset_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/report/costbenchmarking.aspx");
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            btnSaveExcelDirect_Click(null, null);
        }

        protected void btnSaveExcelDirect_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.ModelCheckRecalcModuleReport();
                DisableParameterSelection();

                DataClass.clsModels objModels = new DataClass.clsModels();

                // Check that at least one field has been selected, if none default to 'All'
                if (this.ddlField.SelectedItems.Count == 0)
                    ddlField.SelectedItems.Add(new Ext.Net.ListItem("0"));

                string fields = "";
                string paramFields = "";
                foreach (Ext.Net.ListItem field in this.ddlField.SelectedItems)
                {
                    fields += field.Value + ",";
                    paramFields += field.Text + ",";
                }

                //Need to remove trailing "," from the fields and paramField lists
                fields = fields.ReplaceLastInstanceOf(",", "");
                paramFields = paramFields.ReplaceLastInstanceOf(",", "");

                string peerGroups = "";
                string paramPeerGroups = "";
                //if (this.ddlField.SelectedItems.Count == 1 && this.ddlField.SelectedItem.Value == "0")
                //{
                // Check that at least one peer group has been selected, if none default to 'All'
                if (this.ddlPeerGroup.SelectedItems.Count == 0)
                    ddlPeerGroup.SelectedItems.Add(new Ext.Net.ListItem("0"));

                foreach (Ext.Net.ListItem peerGroup in this.ddlPeerGroup.SelectedItems)
                {
                    peerGroups += peerGroup.Value + ",";
                    paramPeerGroups += peerGroup.Text + ",";
                }

                //Need to remove trailing "," from the peerGroups and paramPeerGroups lists
                peerGroups = peerGroups.ReplaceLastInstanceOf(",", "");
                paramPeerGroups = paramPeerGroups.ReplaceLastInstanceOf(",", "");
                //}
                //else
                //{
                //    peerGroups = "-1";
                //}

                Int32 intCurrency = Convert.ToInt32(ddlCurrency.Value);
                String strCurrencyCode = "";

                clsCurrencies objCurrencies = new clsCurrencies();
                objCurrencies.IdCurrency = intCurrency;
                objCurrencies.loadObject();
                strCurrencyCode = "(" + objCurrencies.Code + ")";

                DataSet ds = objModels.LoadCostBenchmarking(IdModel, fields, peerGroups, intCurrency);

                DataTable dtFields = new DataTable();
                DataTable dtPeerGroups = new DataTable();
                DataTable dtStats = new DataTable();
                DataTable dtExport = new DataTable();
                DataTable dtRows = new DataTable();

                dtRows = ds.Tables[0];
                dtExport = ds.Tables[1];
                dtStats = ds.Tables[3];

                if (dtStats != null)
                {
                    if (dtStats.Rows.Count > 0)
                    {
                        ReportFieldCount = Convert.ToInt32(dtStats.Rows[0]["FieldCount"].ToString());
                    }
                }
                dtStats = null;

                ArrayList datColumnExport = new ArrayList();
                ArrayList datColumnExclude = new ArrayList();
                //datColumnExclude.Add("SortOrderExport");

                String strSelectedParameters = lblField.Text + " '" + paramFields + "'^ " +
                                               lblFilterPeerGroup.Text + " '" + paramPeerGroups + "'^" +
                                               lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "'";
                ExportOpexTOT = new DataReportObject(dtExport, "Cost Benchmarking", "Cost Benchmarking", strSelectedParameters, datColumnExclude, "SortOrderExport", datColumnExport, null);

                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Report Generated Successfully!"), IconCls = "icon-accept", Clear2 = false });
                X.Mask.Hide();

                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                if (strMessage.Contains("Timeout"))
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = strMessage.ToString(), IconCls = "icon-exclamation", Clear2 = false });
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select all filters please"), IconCls = "icon-exclamation", Clear2 = false });
                }
            }
        }

        protected void btnRun_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.ModelCheckRecalcModuleReport();
                DisableParameterSelection();

                DataClass.clsModels objModels = new DataClass.clsModels();

                // Check that at least one field has been selected, if none default to 'All'
                if (this.ddlField.SelectedItems.Count == 0)
                    ddlField.SelectedItems.Add(new Ext.Net.ListItem("0"));

                string fields = "";
                string paramFields = "";
                foreach (Ext.Net.ListItem field in this.ddlField.SelectedItems)
                {
                    fields += field.Value + ",";
                    paramFields += field.Text + ",";
                }

                //Need to remove trailing "," from the fields and paramField lists
                fields = fields.ReplaceLastInstanceOf(",", "");
                paramFields = paramFields.ReplaceLastInstanceOf(",", "");

                string peerGroups = "";
                string paramPeerGroups = "";

                // Check that at least one peer group has been selected, if none default to 'All'
                if (this.ddlPeerGroup.SelectedItems.Count == 0)
                    ddlPeerGroup.SelectedItems.Add(new Ext.Net.ListItem("0"));

                foreach (Ext.Net.ListItem peerGroup in this.ddlPeerGroup.SelectedItems)
                {
                    peerGroups += peerGroup.Value + ",";
                    paramPeerGroups += peerGroup.Text + ",";
                }

                //Need to remove trailing "," from the peerGroups and paramPeerGroups lists
                peerGroups = peerGroups.ReplaceLastInstanceOf(",", "");
                paramPeerGroups = paramPeerGroups.ReplaceLastInstanceOf(",", "");

                Int32 intCurrency = Convert.ToInt32(ddlCurrency.Value);
                String strCurrencyCode = "";

                clsCurrencies objCurrencies = new clsCurrencies();
                objCurrencies.IdCurrency = intCurrency;
                objCurrencies.loadObject();
                strCurrencyCode = "(" + objCurrencies.Code + ")";

                DataSet ds = objModels.LoadCostBenchmarking(IdModel, fields, peerGroups, intCurrency);

                DataTable dtRows = new DataTable();
                dtRows = ds.Tables[0];
                
                DataTable dtExport = new DataTable();
                dtExport = ds.Tables[1];
                
                DataTable dtChart = new DataTable();
                dtChart = ds.Tables[2];
                
                DataTable dtStats = new DataTable();
                dtStats = ds.Tables[3];
                
                if (dtStats != null)
                {
                    if (dtStats.Rows.Count > 0)
                    {
                        ReportFieldCount = Convert.ToInt32(dtStats.Rows[0]["FieldCount"].ToString());
                        ReportCategoriesCount = Convert.ToInt32(dtStats.Rows[0]["CategoriesCount"].ToString());
                        ReportPeerGroupCount = Convert.ToInt32(dtStats.Rows[0]["PeerGroupCount"].ToString());
                    }
                }
                dtStats = null;

                ArrayList datColumnOpex = new ArrayList();
                ArrayList datColumnExport = new ArrayList();
                ArrayList datColumnExclude = new ArrayList();

                this.FillGrid(this.storeBaseCostField, this.grdOpexProjection, dtRows, ref datColumnOpex, false, "");
                this.FillChart(this.storeChart, this.chtChart, dtChart, "");

                String strSelectedParameters = lblField.Text + " '" + paramFields + "'^ " +
                                               lblFilterPeerGroup.Text + " '" + paramPeerGroups + "'^" +
                                               lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "'";
                ExportOpexTOT = new DataReportObject(dtExport, "NewTab", "Cost Benchmarking", strSelectedParameters, datColumnExclude, "SortOrderExport", datColumnExport, null);
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Report Generated Successfully!"), IconCls = "icon-accept", Clear2 = false });
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                if (strMessage.Contains("Timeout"))
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = strMessage.ToString(), IconCls = "icon-exclamation", Clear2 = false });
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select all filters please"), IconCls = "icon-exclamation", Clear2 = false });
                }
            }
        }

        protected void StoreField_ReadData(object sender, StoreReadDataEventArgs e)
        {
            string peerGroups = "";
            foreach (Ext.Net.ListItem peerGroup in this.ddlPeerGroup.SelectedItems)
            {
                peerGroups += peerGroup.Value + ",";
            }

            //Need to remove trailing "," from the peerGroups lists
            peerGroups = peerGroups.ReplaceLastInstanceOf(",", "");

            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dtField = new DataTable();
            if ((ddlPeerGroup.Value == null) || (ddlPeerGroup.Value.ToString() == "0") || (ddlPeerGroup.Value.ToString() == ""))
            {
                //dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
                dtField = objFields.LoadListByPeerGroups("IdModel = " + IdModel + " AND IdPeerGroup IN (SELECT IdPeerGroup FROM tblPeerGroup WHERE IdModel = " + IdModel + ")", "ORDER BY Name");
            }
            else
            {
                //dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
                dtField = objFields.LoadListByPeerGroups("IdModel = " + IdModel + " AND IdPeerGroup IN (" + peerGroups + ")", "ORDER BY Name");
            }
            DataRow drField = dtField.NewRow();
            drField["IdField"] = 0;
            drField["Name"] = "- All -";//drField["CodeName"] = "- All -"; //
            dtField.Rows.Add(drField);
            dtField.AcceptChanges();
            dtField.DefaultView.Sort = "Name";
            DataTable dtFieldSort = dtField.DefaultView.ToTable();

            this.storeField.DataSource = dtFieldSort;
            this.storeField.DataBind();

            this.ddlField.Value = 0;
        }

        protected void StorePeerGroup_ReadData(object sender, StoreReadDataEventArgs e)
        {
            storeField.RemoveAll();
            //// Check that at least one field has been selected, if none default to 'All'
            //if (this.ddlField.SelectedItems.Count == 0)
            //    ddlField.SelectedItems.Add(new Ext.Net.ListItem("0"));

            //string fields = "";
            //foreach (Ext.Net.ListItem field in this.ddlField.SelectedItems)
            //{
            //    fields += field.Value + ",";
            //}

            DataClass.clsPeerGroup objPeerGroup = new DataClass.clsPeerGroup();
            DataTable dtPeerGroup = new DataTable();
            dtPeerGroup = objPeerGroup.LoadComboBox("IdModel = " + IdModel, "ORDER BY PeerGroupCode");

            DataRow drPeerGroup = dtPeerGroup.NewRow();
            drPeerGroup["IdPeerGroup"] = 0;
            drPeerGroup["PeerGroupDescription"] = "- All -";
            dtPeerGroup.Rows.Add(drPeerGroup);
            dtPeerGroup.AcceptChanges();
            dtPeerGroup.DefaultView.Sort = "IdPeerGroup";
            DataTable dtPeerGroupSort = dtPeerGroup.DefaultView.ToTable();

            this.storePeerGroup.DataSource = dtPeerGroupSort;
            this.storePeerGroup.DataBind();

            this.ddlPeerGroup.Value = 0;
        }

        protected void StoreCurr_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsCurrencies objCurrencies = new DataClass.clsCurrencies();
            this.storeBase.DataSource = objCurrencies.LoadList("IdModel IN (" + IdModel + ") AND Output=1", "ORDER BY BaseCurrency DESC,Name");
            this.storeBase.DataBind();
            DataTable dt = objCurrencies.LoadList("BaseCurrency = 1 AND IdModel = " + IdModel, "");
            if (dt.Rows.Count > 0)
            {
                ddlCurrency.Value = dt.Rows[0]["IdCurrency"];
            }
            else
            {
                ddlCurrency.SelectedItem.Index = 0;
            }
        }

        #region UNUSED EVENTS CODE but needed because selections are hidden on page
        protected void ddlLevels_Select(object sender, DirectEventArgs e)
        {
            //this.ddlField.Reset();
            //this.storeField.Reload();
            //this.ddlOperationType.Reset();
            //this.storeOperationType.Reload();
        }

        protected void ddlCostCategory_Select(object sender, DirectEventArgs e)
        {
            //bool nonAllCC = false;
            //foreach (Ext.Net.ListItem costCategory in this.ddlCostCategory.SelectedItems)
            //{
            //    if (Convert.ToInt32(costCategory.Value) != 0)
            //        nonAllCC = true;
            //}

            //if (nonAllCC)
            //    ddlCostCategory.DeselectItem(0);
        }

        protected void StoreLevel_ReadData(object sender, StoreReadDataEventArgs e)
        {
            //DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            //DataTable dt = objAggregationLevels.LoadList("IdModel = " + IdModel, "ORDER BY Name");

            //DataRow drLevel = dt.NewRow();
            //drLevel["IdAggregationLevel"] = 0;
            //drLevel["Name"] = "- All -";
            //dt.Rows.Add(drLevel);
            //dt.AcceptChanges();
            //dt.DefaultView.Sort = "Name";
            //DataTable dtSort = dt.DefaultView.ToTable();

            //this.storeLevels.DataSource = dtSort;
            //this.storeLevels.DataBind();

            //this.ddlLevels.Value = 0;

            //this.storeLevels.DataBind();
        }

        protected void StoreCostType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            //DataClass.clsCostType objCostTypes = new DataClass.clsCostType();
            //DataTable dt = objCostTypes.LoadList("", "ORDER BY Name");
            //if (dt.Rows.Count > 0)
            //{
            //    ddlCostType.Value = dt.Rows[0]["IdCostType"];
            //}
            //else
            //{
            //    ddlCostType.SelectedItem.Index = 0;
            //}
            //foreach (DataRow row in dt.Rows)
            //{
            //    if (idLanguage == 2)
            //    {
            //        row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
            //    }
            //}
            //this.storeCostType.DataSource = dt;
            //this.storeCostType.DataBind();
        }

        protected void StoreTechnicalScenarioType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            //DataClass.clsScenarios objScenarioTypes = new DataClass.clsScenarios();
            //DataTable dt = objScenarioTypes.LoadList("", "ORDER BY Name");
            //if (dt.Rows.Count > 0)
            //{
            //    ddlCostType.Value = dt.Rows[0]["IdScenarioType"];
            //}
            //else
            //{
            //    ddlCostType.SelectedItem.Index = 0;
            //}
            //foreach (DataRow row in dt.Rows)
            //{
            //    if (idLanguage == 2)
            //    {
            //        row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
            //    }
            //}
            //this.storeTechnicalScenarioType.DataSource = dt;
            //this.storeTechnicalScenarioType.DataBind();
        }

        protected void StoreEconomicScenarioType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            //DataClass.clsScenarios objScenarioTypes = new DataClass.clsScenarios();
            //DataTable dt = objScenarioTypes.LoadList("", "ORDER BY Name");
            //if (dt.Rows.Count > 0)
            //{
            //    ddlCostType.Value = dt.Rows[0]["IdScenarioType"];
            //}
            //else
            //{
            //    ddlCostType.SelectedItem.Index = 0;
            //}
            //foreach (DataRow row in dt.Rows)
            //{
            //    if (idLanguage == 2)
            //    {
            //        row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
            //    }
            //}
            //this.storeEconomicScenarioType.DataSource = dt;
            //this.storeEconomicScenarioType.DataBind();
        }

        protected void StoreOperationType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            //DataClass.clsOperationType objoperationTypes = new DataClass.clsOperationType();
            //DataTable dtOperation = new DataTable();

            //if (((ddlLevels.Value == null) || (ddlLevels.Value.ToString() == "0") || (ddlLevels.Value.ToString() == "")) && ((ddlField.Value == null) || (ddlField.Value.ToString() == "0") || (ddlField.Value.ToString() == "")))
            //{
            //    dtOperation = objoperationTypes.LoadList("IdModel=" + IdModel, "");//"[vListTypesOperation].IdModel=" + IdModel, "");

            //    DataRow drOperation = dtOperation.NewRow();
            //    drOperation["IdTypeOperation"] = 0;
            //    drOperation["Name"] = "- All -";
            //    dtOperation.Rows.Add(drOperation);
            //    dtOperation.AcceptChanges();
            //    dtOperation.DefaultView.Sort = "Name";

            //    ddlOperationType.Value = 0;
            //}
            //else if (Convert.ToInt32(ddlLevels.Value) > 0)
            //{
            //    dtOperation = objoperationTypes.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
            //    if (dtOperation.Rows.Count > 0)
            //    {
            //        ddlOperationType.Value = dtOperation.Rows[0]["IdTypeOperation"];
            //    }
            //}
            //else
            //{
            //    dtOperation = objoperationTypes.LoadList("IdModel=" + IdModel + " AND IdField = " + ddlField.Value, " Order by Name");//"[vListTypesOperation].IdModel=" + IdModel + " AND tblFields.IdField = " + ddlField.Value, " Order by Name");
            //    if (dtOperation.Rows.Count > 0)
            //    {
            //        ddlOperationType.Value = dtOperation.Rows[0]["IdTypeOperation"];
            //    }
            //}

            //DataTable dtOperationSort = dtOperation.DefaultView.ToTable();

            //this.storeOperationType.DataSource = dtOperationSort;
            //this.storeOperationType.DataBind();
        }

        protected void StoreCostCategory_ReadData(object sender, StoreReadDataEventArgs e)
        {
            //DataClass.clsZiffAccount objZiffAccount = new DataClass.clsZiffAccount();
            //DataTable dtZiffAccount = objZiffAccount.LoadComboBox("IdModel = " + IdModel + " AND Parent IS NOT NULL", " ORDER BY Parent");
            //DataRow drAccount = dtZiffAccount.NewRow();
            //drAccount["IdZiffAccount"] = 0;
            //drAccount["Name"] = "- All -";
            //drAccount["Code"] = "- All -";
            //drAccount["Parent"] = 0;
            //dtZiffAccount.Rows.Add(drAccount);
            //dtZiffAccount.AcceptChanges();
            //dtZiffAccount.DefaultView.Sort = "Parent";
            //this.storeCostCategory.DataSource = dtZiffAccount;
            //this.storeCostCategory.DataBind();
            //ddlCostCategory.Value = 0;
        }
        #endregion

        #endregion

        #region Methods

        private void FillGrid(Ext.Net.Store s, Ext.Net.GridPanel g, DataTable dt, ref ArrayList colVisible, Boolean IsBLBO, String CurrencyCode)
        {
            //add a blank column to the datatable
            DataTable dtTemp = new DataTable();

            int z = 1;
            foreach (DataColumn c in dt.Columns)
            {
                dtTemp.Columns.Add(c.ColumnName);

                if (z == ReportFieldCount + 2)
                {
                    dtTemp.Columns.Add("Blank");
                }
                z++;
            }

            foreach (DataRow dtRow in dt.Rows)
            {
                DataRow drAct = dtTemp.NewRow();

                foreach (DataColumn c in dt.Columns)
                {
                    if (c.ColumnName != "Blank")
                    {
                        drAct[c.ColumnName] = dtRow[c.ColumnName];
                    }
                }
                dtTemp.Rows.Add(drAct);
                dtTemp.AcceptChanges();

            }
            dt = dtTemp;

            s.RemoveFields();

            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "Code" || c.ColumnName == "Description")
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Auto));
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Auto));
                }
            }
            g.Features.Clear();

            if (!IsBLBO)
            {
                GroupingSummary gsum = new GroupingSummary();
                gsum.GroupHeaderTplString = "{name}";
                gsum.HideGroupedHeader = true;
                gsum.ShowSummaryRow = false;    //Hide the grouping Summary Totals by setting to false
                g.Features.Add(gsum);
            }

            GridFilters f = new GridFilters();
            f.Local = true;
            g.Features.Add(f);

            Summary smry = new Summary();
            smry.Dock = new SummaryDock();
            smry.ID = "Summary" + g.ID.ToString();
            smry.ShowSummaryRow = false;    //Hide the Grand Summary Totals by setting to false

            g.Features.Add(smry);

            foreach (DataColumn c in dt.Columns)
            {
                if (!c.ColumnName.Contains("Id"))
                {
                    if (c.ColumnName == "Code" || c.ColumnName == "Description")
                    {
                        StringFilter sFilter = new StringFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                    else
                    {
                        NumericFilter sFilter = new NumericFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                }
            }

            //Adding the Peer Groups
            Column col2 = new Column();
            col2.Text = (String)GetGlobalResourceObject("CPM_Resources", "Peer Groups");
            SummaryColumn colSum2 = new SummaryColumn();
            int y = 1;
            foreach (DataColumn c in dt.Columns)
            {
                if (!c.ColumnName.Contains("Id") && c.ColumnName != "Code" && c.ColumnName != "Description" && y > ReportFieldCount + 3)
                {
                    colSum2 = new SummaryColumn();
                    colSum2.DataIndex = c.ColumnName;
                    colSum2.Text = c.ColumnName;
                    colVisible.Add(new DataClass.DataTableColumnDisplay(c.ColumnName, colSum2.Text));
                    colSum2.SummaryType = Ext.Net.SummaryType.Sum;
                    colSum2.Width = 150;
                    if (idLanguage == 1)
                        colSum2.Renderer.Fn = "formatColData";
                    else
                        colSum2.Renderer.Fn = "formatColDataESP";
                    colSum2.Align = Alignment.Right;
                    colSum2.MinWidth = 100;
                    col2.Columns.Add(colSum2);
                }
                y++;
            }

            g.ColumnModel.Columns.Add(col2);

            s.DataSource = dt;

            //Adding the Blank column
            Column col3 = new Column();
            col3.Text = " ";
            SummaryColumn colSum3 = new SummaryColumn();
            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "Blank")
                {
                    //do nothing
                }
            }

            g.ColumnModel.Columns.Add(col3);

            s.DataSource = dt;

            //Adding the fields
            Column col = new Column();
            col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Fields");
            SummaryColumn colSum = new SummaryColumn();
            int x = 1;
            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "Code" || c.ColumnName == "Description")
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        colSum = new SummaryColumn();
                        colSum.DataIndex = c.ColumnName;
                        switch (c.ColumnName)
                        {
                            case "Code":
                                colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", "Code");
                                break;
                            case "Description":
                                colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", "Description");
                                break;
                        }
                        colSum.Locked = true;
                        colVisible.Add(new DataClass.DataTableColumnDisplay(c.ColumnName, colSum.Text));
                        colSum.SummaryType = Ext.Net.SummaryType.Count;
                        colSum.TdCls = "task";
                        colSum.Sortable = true;
                        colSum.Groupable = true;
                        colSum.Width = 250;
                        colSum.Renderer.Fn = "formatCol";
                        colSum.MenuDisabled = false;
                        colSum.Align = Alignment.Left;
                        g.ColumnModel.Columns.Add(colSum);
                    }
                }
                else //Adds the Field values
                {
                    if (!c.ColumnName.Contains("Id") && x <= ReportFieldCount + 2)
                    {
                        colSum = new SummaryColumn();
                        colSum.DataIndex = c.ColumnName;
                        colSum.Text = c.ColumnName;
                        colVisible.Add(new DataClass.DataTableColumnDisplay(c.ColumnName, colSum.Text));
                        colSum.SummaryType = Ext.Net.SummaryType.Sum;
                        colSum.Width = 150;
                        if (idLanguage == 1)
                            colSum.Renderer.Fn = "formatColData";
                        else
                            colSum.Renderer.Fn = "formatColDataESP";
                        colSum.Align = Alignment.Right;
                        colSum.MinWidth = 100;
                        col.Columns.Add(colSum);
                    }
                }
                x++;
            }

            g.ColumnModel.Columns.Add(col);

            s.DataSource = dt;

            if (X.IsAjaxRequest)
            {
                g.Reconfigure();
            }
            s.DataBind();
        }

        private void FillChart(Ext.Net.Store s, Ext.Net.Chart c, DataTable dt, String CurrencyCode)
        {
            try
            {
                string[] str = new string[dt.Columns.Count - 2];
                int i = 0;
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName != "Field" && col.ColumnName != "FieldTotal")
                    {
                        str[i] = col.ColumnName;
                        i = i + 1;
                    }
                }
                string[] strcat = new string[] { "Field" };

                s.RemoveFields();

                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName != "Description")
                        s.AddField(new ModelField(col.ColumnName, ModelFieldType.Auto));
                }
                s.DataSource = dt;
                s.DataBind();

                AxisLabel AxisL = new AxisLabel();
                AxisL.Renderer.Handler = "return Ext.util.Format.number(value, '0,000.00');";
                //AxisL.Rotate = new  RotateAttribute()
                //{
                //    Degrees = 90,
                //};

                NumericAxis numericAxis = new NumericAxis()
                {
                    Title = (String)GetGlobalResourceObject("CPM_Resources", "Unit Cost") + " ($/BOE)",
                    Fields = str,
                    Grid = true,
                    Position = Position.Left,
                    GridConfig = new AxisGrid()
                    {
                        Odd = new SpriteAttributes()
                        {
                            Opacity = 1,
                            Fill = "#EFEBEF",
                            Stroke = "#EFEBEF",
                            StrokeWidth = 0.5,
                        }
                    },
                    Label = AxisL,
                };
                c.Axes.Add(numericAxis);

                CategoryAxis categoryAxis = new CategoryAxis()
                {
                    Title = (String)GetGlobalResourceObject("CPM_Resources", "Field/Peer Groups"),
                    Fields = strcat,
                    Position = Position.Bottom,
                    AdjustEnd = true,
                    CalculateCategoryCount = true,
                };
                c.Axes.Add(categoryAxis);

                ChartTip chrtTip = new ChartTip();
                chrtTip.TrackMouse = true;
                chrtTip.Width = 140;
                chrtTip.StyleSpec = "background:#fff; text-align:center;";
                chrtTip.Height = 20;
                chrtTip.Renderer.Handler = "this.setTitle(String(Ext.util.Format.number(item.value[1], '0,000.00')));";

                c.Series.Add(new ColumnSeries()
                {
                    Titles = str,
                    XField = strcat,
                    YField = str,
                    Axis = Position.Bottom,
                    Stacked = true,
                    Tips = chrtTip,
                });

                c.Height = (ReportFieldCount + ReportPeerGroupCount) * 75 < ReportCategoriesCount * 20 ? ReportCategoriesCount * 20 : (ReportFieldCount + ReportPeerGroupCount) * 75;
                c.Animate = true;
                c.Shadow = true;
                c.Frame = true;
                c.AutoSize = true;
                c.StyleSpec = "background:#fff;";
                c.ColumnWidth = .10;
                pnlChartObject.Render();
            }
            catch
            {
            }
        }

        protected void ClearReport()
        {
            if (PageIsLoaded)
            {
                this.storeBaseCostField.RemoveAll();
                this.pnlChartObject.ClearContent();
            }
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExportOpexTOT = new DataReportObject();
            StoredExportOpexTOT = ExportOpexTOT;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExportOpexTOT);
            objWriter.BuildExportCostBenchmarkingReport(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2007);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=CostBenchmarking.xlsx");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        private void DisableParameterSelection()
        {
            this.pnlExportToolbar.Hidden = false;
            this.pnlReportToolbar.Hidden = true;
            this.ddlLevels.ReadOnly = true;
            this.ddlField.ReadOnly = true;
            this.ddlPeerGroup.ReadOnly = true;
            this.ddlStructure.ReadOnly = true;
            this.txtFrom.ReadOnly = true;
            this.txtTo.ReadOnly = true;
            this.ddlScenarioTechnical.ReadOnly = true;
            this.ddlScenarioEconomic.ReadOnly = true;
            this.ddlCurrency.ReadOnly = true;
            this.ddlTerm.ReadOnly = true;
            this.pnlMainChartPanel.Hidden = false;
            this.ddlCostType.ReadOnly = true;
            this.ddlOperationType.ReadOnly = true;
            this.ddlFactor.ReadOnly = true;
        }

        public void ModelCheckRecalcModuleReport()
        {
            clsModels objModelRecalc = new clsModels();
            objModelRecalc.IdModel = IdModel;
            objModelRecalc.CheckRecalcModuleParameter(objApplication.MaxRelationLevels);
            objModelRecalc.CheckRecalcModuleAllocation();
            objModelRecalc.CheckRecalcModuleTechnical();
            objModelRecalc.CheckRecalcModuleEconomic();
            objModelRecalc = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = (String)GetGlobalResourceObject("CPM_Resources", "CostBenchmarking");
            this.btnReset.Text = modMain.strReportsModuleReset;
            this.btnSaveExcel.Text = modMain.strReportsModuleExportReportDataToExcel;
            this.lblLevels.Text = modMain.strReportsModuleAggregationLevel;
            this.lblField.Text = modMain.strTechnicalModuleField;
            this.lblStructure.Text = modMain.strReportsModuleCostStructure;
            this.lblFrom.Text = modMain.strReportsModuleFrom;
            this.lblTo.Text = modMain.strReportsModuleTo;
            this.lblScenarioTechnical.Text = modMain.strReportsModuleTechnicalScenario;
            this.lblScenarioEconomic.Text = modMain.strReportsModuleEconomicScenario;
            this.lblCurrency.Text = modMain.strReportsModuleCurrency;
            this.lblTerm.Text = modMain.strReportsModuleTerm;
            this.btnRun.Text = (String)GetGlobalResourceObject("CPM_Resources", "Run Report");
            this.ReportTitle01.Title = (String)GetGlobalResourceObject("CPM_Resources", "CostBenchmarking");
            this.lblOperationType.Text = modMain.strReportsModuleFormOperationType + ":";
            this.lblCostType.Text = modMain.strReportsModuleFormTypeCost + ":";
            this.btnDirectExcel.Text = modMain.strReportsModuleExportReportDataToExcel;
            this.lblCurrency.Text = (String)GetGlobalResourceObject("CPM_Resources", "Currency");
            this.lblFilterPeerGroup.Text = (String)GetGlobalResourceObject("CPM_Resources", "Peer Group");
        }

        #region UNUSED METHODS CODE
        public Boolean GetProject()
        {
            Boolean Value = false;
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("Level IN (1,2) AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = true;
            }

            return Value;
        }

        protected void FromTo()
        {
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                this.txtFrom.Value = (Int32)dt.Rows[0]["BaseYear"];
                this.txtTo.Value = Convert.ToInt32(dt.Rows[0]["BaseYear"]) + Convert.ToInt32(dt.Rows[0]["IdProjection"]);

                this.txtFrom.MinValue = (Int32)dt.Rows[0]["BaseYear"];
                this.txtFrom.MaxValue = (Int32)dt.Rows[0]["BaseYear"] + (Int32)dt.Rows[0]["IdProjection"];

                this.txtFrom.MinValue = (Int32)dt.Rows[0]["BaseYear"];
                this.txtTo.MaxValue = Convert.ToInt32(dt.Rows[0]["BaseYear"]) + Convert.ToInt32(dt.Rows[0]["IdProjection"]);

                this.txtFrom.DataBind();
                this.txtTo.DataBind();

            }
        }

        protected Int32 GetIdOperation(String _TypeOperation)
        {
            Int32 Value = 0;
            DataClass.clsTypesOperation objTypesOperation = new DataClass.clsTypesOperation();
            DataTable dt = objTypesOperation.LoadList("Name = '" + _TypeOperation + "'", "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdTypeOperation"];
            }
            return Value;
        }

        protected void txtTo_Select(object sender, DirectEventArgs e)
        {
            this.txtTo.MinValue = Convert.ToInt32(txtFrom.Value);
            this.txtTo.DataBind();
        }

        private Int32 GetIdField(String _Name)
        {
            Int32 intID = 0;

            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dtField = objFields.LoadList("IdModel = " + IdModel + " AND Name = '" + _Name + "'", "ORDER BY Name");

            if (dtField.Rows.Count > 0)
            {
                intID = (Int32)dtField.Rows[0]["IdField"];
            }

            return intID;
        }

        private Int32 GetIdLevel(String _Name)
        {
            Int32 intID = 0;

            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dtLevel = objAggregationLevels.LoadList("IdModel = " + IdModel + " AND Name = '" + _Name + "'", "ORDER BY Name");

            if (dtLevel.Rows.Count > 0)
            {
                intID = (Int32)dtLevel.Rows[0]["IdAggregationLevel"];
            }

            return intID;
        }
        #endregion

        #endregion

    }
}
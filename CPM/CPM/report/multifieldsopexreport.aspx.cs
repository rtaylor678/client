﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using Ext.Net.Utilities;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Diagnostics;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CD;
using DataClass;
using System.Web.Script.Serialization;

namespace CPM
{
    public partial class _multifieldopexreport : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdActivity { get { return (Int32)Session["IdActivity"]; } set { Session["IdActivity"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }
        private Boolean PageIsLoaded { get; set; }
        private DataReportObject ExportOpexTOT { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }
        private DataReportObject ExportOpexBL { get { return (DataReportObject)Session["Export02"]; } set { Session["Export02"] = value; } }
        private DataReportObject ExportOpexBO { get { return (DataReportObject)Session["Export03"]; } set { Session["Export03"] = value; } }
        private Int32 ReportFieldCount = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (GetProject())
            {
                this.lblScenarioEconomic.Hide();
                this.ddlScenarioEconomic.Hide();
                this.lblTerm.Hide();
                this.ddlTerm.Hide();
            }

            if (!X.IsAjaxRequest)
            {
                LoadLabel();
                Session["Export01"] = null;
                Session["Export02"] = null;
                Session["Export03"] = null;
                PageIsLoaded = false;
                IdActivity = 0;
                this.ddlStructure.SelectedItem.Index = 0;
                this.ddlTerm.SelectedItem.Index = 1;
                this.ddlScenarioTechnical.SelectedItem.Index = 0;
                this.ddlScenarioEconomic.SelectedItem.Index = 0;
                this.ddlOperationType.SelectedItem.Index = 0;
                this.ddlCostType.SelectedItem.Index = 0;
                this.ddlFactor.SelectedItem.Index = 1;
                this.FromTo();
                DataTable dtOpex = new DataTable();
                DataTable dtBL = new DataTable();
                DataTable dtBO = new DataTable();
                DataTable dtVRG = new DataTable();
                DataTable dtVBL = new DataTable();
                DataTable dtVBO = new DataTable();
                ArrayList datColumnOpex = new ArrayList();
                ArrayList datColumnBL = new ArrayList();
                ArrayList datColumnBO = new ArrayList();
                ArrayList datColumnVRG = new ArrayList();
                ArrayList datColumnVBL = new ArrayList();
                ArrayList datColumnVBO = new ArrayList();
                this.FillGrid(this.storeBaseCostField, this.grdOpexProjection, dtOpex, ref datColumnOpex, false, null);
                //this.FillGrid(this.storeVolumeTotal, this.grdVolumeTotal, dtVRG, ref datColumnVRG, false, null);
                //this.FillGrid(this.storeBaseLine, this.grdBaseLine, dtBL, ref datColumnBL, true, null);
                //this.FillGrid(this.storeVolumeTotalBL, this.grdVolumeTotalBL, dtVBL, ref datColumnVBL, true, null);
                //this.FillGrid(this.storeBusinessOpportunities, this.grdBusinessOpportunities, dtBO, ref datColumnBO, true, null);
                //this.FillGrid(this.storeVolumeTotal, this.grdVolumeTotal, dtVRG, ref datColumnVRG, true, null);
                //this.FillGrid(this.storeVolumeTotalBO, this.grdVolumeTotalBO, dtVBO, ref datColumnVBO, true, null);
                PageIsLoaded = true;
            }
        }

        protected void ddlLevels_Select(object sender, DirectEventArgs e)
        {
            this.ddlField.Reset();
            this.storeField.Reload();
            this.ddlOperationType.Reset();
            this.storeOperationType.Reload();
        }

        protected void ddlField_Select(object sender, DirectEventArgs e)
        {
            bool All = false;
            ddlField.DeselectItem(0);
            int i = 0;

            foreach (Ext.Net.ListItem field in this.ddlField.SelectedItems)
            {
                i++;
                if (Convert.ToInt32(field.Value) == 0 && i == this.ddlField.SelectedItems.Count)
                    All = true;
            }

            if (All)
            {
                ddlField.SelectItem(0);
                foreach (Ext.Net.ListItem items in this.ddlField.SelectedItems)
                {
                    if (Convert.ToInt32(items.Value) != 0)
                        this.ddlField.DeselectItem(items.Index);
                }
            }
            this.ddlOperationType.Reset();
            this.storeOperationType.Reload();
        }

        protected void ddlCostCategory_Select(object sender, DirectEventArgs e)
        {
            bool nonAllCC = false;
            foreach (Ext.Net.ListItem costCategory in this.ddlCostCategory.SelectedItems)
            {
                if (Convert.ToInt32(costCategory.Value) != 0)
                    nonAllCC = true;
            }

            if (nonAllCC)
                ddlCostCategory.DeselectItem(0);
        }

        protected void btnReset_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/report/multifieldsopexreport.aspx");
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            btnSaveExcelDirect_Click(null, null);
        }

        protected void btnSaveExcelDirect_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.ModelCheckRecalcModuleReport();
                DisableParameterSelection();

                DataClass.clsModels objModels = new DataClass.clsModels();

                Int32 intLevel = Convert.ToInt32(ddlLevels.Value);

                // Check that at least one field has been selected, if none default to 'All'
                if (this.ddlField.SelectedItems.Count == 0)
                    ddlField.SelectedItems.Add(new Ext.Net.ListItem("0"));

                string fields = "";
                string paramFields = "";
                foreach (Ext.Net.ListItem field in this.ddlField.SelectedItems)
                {
                    fields += field.Value + ",";
                    paramFields += field.Text + ",";
                }

                //Need to remove trailing "," from the fields and paramField lists
                fields = fields.ReplaceLastInstanceOf(",", "");
                paramFields = paramFields.ReplaceLastInstanceOf(",", "");

                Int32 intStructure = 1;// Convert.ToInt32(ddlStructure.Value); *** WILL ALAWAYS USE THE ZIFF STRUCTURE UNTIL OTHERWISE TOLD TO CHANGE IT ***
                Int32 intCurrency = Convert.ToInt32(ddlCurrency.Value);
                Int32 intTerm = Convert.ToInt32(ddlTerm.Value);
                Int32 intTechnicalScenario = Convert.ToInt32(ddlScenarioTechnical.Value);
                Int32 intEconomicScenario = 0;
                Int32 intCostType = Convert.ToInt32(ddlCostType.Value);
                Int32 intTypeOPeration = Convert.ToInt32(ddlOperationType.Value);
                Int32 intFactor = Convert.ToInt32(ddlFactor.SelectedItem.Text);
                String strCurrencyCode = "";

                if (this.ddlScenarioEconomic.Hidden == false)
                {
                    intEconomicScenario = Convert.ToInt32(ddlScenarioEconomic.Value);
                }
                Int32 intFrom = Convert.ToInt32(txtFrom.Value);
                Int32 intTo = Convert.ToInt32(txtTo.Value);

                clsCurrencies objCurrencies = new clsCurrencies();
                objCurrencies.IdCurrency = intCurrency;
                objCurrencies.loadObject();
                if (intFactor == 1000)
                {
                    strCurrencyCode = "(1M_" + objCurrencies.Code + ")";
                }
                else
                {
                    strCurrencyCode = "(" + objCurrencies.Code + ")";
                }

                // Check that at least one cost category has been selected, if none default to 'All'
                if (this.ddlCostCategory.SelectedItems.Count == 0)
                    ddlCostCategory.SelectedItems.Add(new Ext.Net.ListItem("0"));

                string costCategories = "";
                foreach (Ext.Net.ListItem costCategory in this.ddlCostCategory.SelectedItems)
                {
                    costCategories += costCategory.Value + ",";
                }

                //Need to remove trailing "," from the fields list
                costCategories = costCategories.ReplaceLastInstanceOf(",", "");

                DataSet ds = objModels.LoadMultiFieldsOpexProjection(IdModel, intLevel, fields, intStructure, intFrom, intTo, intTechnicalScenario, intEconomicScenario, intCurrency, intTerm, 0, intCostType, intTypeOPeration, intFactor, costCategories);

                DataTable dtExport = new DataTable();

                if (ds.Tables.Count != 4)
                {
                    dtExport = ds.Tables[1];
                }
                else
                {
                    dtExport = ds.Tables[2];
                }

                ArrayList datColumnExport = new ArrayList();

                String strSelectedParameters = lblLevels.Text + " '" + ddlLevels.SelectedItem.Text + "', " +
                                               lblField.Text + " '" + paramFields + "', " +
                                               lblStructure.Text + " '" + ddlStructure.SelectedItem.Text + "', " +
                                               lblFrom.Text + " '" + txtFrom.Text + "', " +
                                               lblTo.Text + " '" + txtTo.Text + "', " +
                                               lblScenarioTechnical.Text + " '" + ddlScenarioTechnical.SelectedItem.Text + "', " +
                                               lblScenarioEconomic.Text + " '" + ddlScenarioEconomic.SelectedItem.Text + "', " +
                                               lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "', " +
                                               lblTerm.Text + " '" + ddlTerm.SelectedItem.Text + "', " +
                                               lblOperationType.Text + " '" + ddlOperationType.SelectedItem.Text + "', " +
                                               lblCostType.Text + " '" + ddlCostType.SelectedItem.Text + "', " +
                                               lblFactor.Text + " '" + ddlFactor.SelectedItem.Text + "'";
                ExportOpexTOT = new DataReportObject(dtExport, "NewTab", "Multiple Fields Projection", strSelectedParameters, null, null, datColumnExport, null);

                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Report Generated Successfully!"), IconCls = "icon-accept", Clear2 = false });
                X.Mask.Hide();

                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                if (strMessage.Contains("Timeout"))
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = strMessage.ToString(), IconCls = "icon-exclamation", Clear2 = false });
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select all filters please"), IconCls = "icon-exclamation", Clear2 = false });
                }
            }
        }

        protected void btnRun_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.ModelCheckRecalcModuleReport();
                DisableParameterSelection();

                DataClass.clsModels objModels = new DataClass.clsModels();

                Int32 intLevel = Convert.ToInt32(ddlLevels.Value);

                // Check that at least one field has been selected, if none default to 'All'
                if (this.ddlField.SelectedItems.Count == 0)
                    ddlField.SelectedItems.Add(new Ext.Net.ListItem("0"));

                string fields = "";
                string paramFields = "";
                foreach (Ext.Net.ListItem field in this.ddlField.SelectedItems)
                {
                    fields += field.Value + ",";
                    paramFields += field.Text + ",";
                }

                //Need to remove trailing "," from the fields and paramField lists
                fields = fields.ReplaceLastInstanceOf(",", "");
                paramFields = paramFields.ReplaceLastInstanceOf(",", "");

                Int32 intStructure = 1;// Convert.ToInt32(ddlStructure.Value); *** WILL ALAWAYS USE THE ZIFF STRUCTURE UNTIL OTHERWISE TOLD TO CHANGE IT ***
                Int32 intCurrency = Convert.ToInt32(ddlCurrency.Value);
                Int32 intTerm = Convert.ToInt32(ddlTerm.Value);
                Int32 intTechnicalScenario = Convert.ToInt32(ddlScenarioTechnical.Value);
                Int32 intEconomicScenario = 0;
                Int32 intCostType = Convert.ToInt32(ddlCostType.Value);
                Int32 intTypeOPeration = Convert.ToInt32(ddlOperationType.Value);
                Int32 intFactor = Convert.ToInt32(ddlFactor.SelectedItem.Text);
                String strCurrencyCode = "";

                if (this.ddlScenarioEconomic.Hidden == false)
                {
                    intEconomicScenario = Convert.ToInt32(ddlScenarioEconomic.Value);
                }
                Int32 intFrom = Convert.ToInt32(txtFrom.Value);
                Int32 intTo = Convert.ToInt32(txtTo.Value);

                clsCurrencies objCurrencies = new clsCurrencies();
                objCurrencies.IdCurrency = intCurrency;
                objCurrencies.loadObject();
                if (intFactor == 1000)
                {
                    strCurrencyCode = "(1M_" + objCurrencies.Code + ")";
                }
                else
                {
                    strCurrencyCode = "(" + objCurrencies.Code + ")";
                }

                // Check that at least one cost category has been selected, if none default to 'All'
                if (this.ddlCostCategory.SelectedItems.Count == 0)
                    ddlCostCategory.SelectedItems.Add(new Ext.Net.ListItem("0"));

                string costCategories = "";
                foreach (Ext.Net.ListItem costCategory in this.ddlCostCategory.SelectedItems)
                {
                    costCategories += costCategory.Value + ",";
                }

                //Need to remove trailing "," from the fields list
                costCategories = costCategories.ReplaceLastInstanceOf(",", "");

                DataSet ds = objModels.LoadMultiFieldsOpexProjection(IdModel, intLevel, fields, intStructure, intFrom, intTo, intTechnicalScenario, intEconomicScenario, intCurrency, intTerm, 0, intCostType,intTypeOPeration, intFactor, costCategories);

                DataTable dtOpex = new DataTable();
                dtOpex = ds.Tables[0];
                DataTable dtChart = new DataTable();
                DataTable dtStats = new DataTable();
                DataTable dtExport = new DataTable();

                if (ds.Tables.Count != 4)
                {
                    dtChart = null;
                    dtExport = ds.Tables[1];
                    dtStats = ds.Tables[2];
                }
                else
                {
                    dtChart = ds.Tables[1];
                    dtExport = ds.Tables[2];
                    dtStats = ds.Tables[3];
                }
                if (dtStats != null)
                {
                    if (dtStats.Rows.Count > 0)
                    {
                        ReportFieldCount = Convert.ToInt32(dtStats.Rows[0]["FieldCount"].ToString());
                    }
                }
                dtStats = null;

                ArrayList datColumnOpex = new ArrayList();
                ArrayList datColumnExport = new ArrayList();

                this.FillGrid(this.storeBaseCostField, this.grdOpexProjection, dtOpex, ref datColumnOpex, false, strCurrencyCode);
                this.FillChart(this.storeChart, this.chtChart, dtChart, strCurrencyCode);

                String strSelectedParameters = lblLevels.Text + " '" + ddlLevels.SelectedItem.Text + "', " +
                                               lblField.Text + " '" + paramFields + "', " +
                                               lblStructure.Text + " '" + ddlStructure.SelectedItem.Text + "', " +
                                               lblFrom.Text + " '" + txtFrom.Text + "', " +
                                               lblTo.Text + " '" + txtTo.Text + "', " +
                                               lblScenarioTechnical.Text + " '" + ddlScenarioTechnical.SelectedItem.Text + "', " +
                                               lblScenarioEconomic.Text + " '" + ddlScenarioEconomic.SelectedItem.Text + "', " +
                                               lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "', " +
                                               lblTerm.Text + " '" + ddlTerm.SelectedItem.Text + "', " +
                                               lblOperationType.Text + " '" + ddlOperationType.SelectedItem.Text + "', " +
                                               lblCostType.Text + " '" + ddlCostType.SelectedItem.Text + "', " +
                                               lblFactor.Text + " '" + ddlFactor.SelectedItem.Text + "'";
                ExportOpexTOT = new DataReportObject(dtExport, "NewTab", "Multiple Fields Projection", strSelectedParameters, null, null, datColumnExport, null);
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Report Generated Successfully!"), IconCls = "icon-accept", Clear2 = false });
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                if (strMessage.Contains("Timeout"))
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = strMessage.ToString(), IconCls = "icon-exclamation", Clear2 = false });
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select all filters please"), IconCls = "icon-exclamation", Clear2 = false });
                }
            }
        }

        protected void StoreLevel_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dt = objAggregationLevels.LoadList("IdModel = " + IdModel, "ORDER BY Name");

            DataRow drLevel = dt.NewRow();
            drLevel["IdAggregationLevel"] = 0;
            drLevel["Name"] = "- All -";
            dt.Rows.Add(drLevel);
            dt.AcceptChanges();
            dt.DefaultView.Sort = "Name";
            DataTable dtSort = dt.DefaultView.ToTable();

            this.storeLevels.DataSource = dtSort;
            this.storeLevels.DataBind();

            this.ddlLevels.Value = 0;

            this.storeLevels.DataBind();
        }

        protected void StoreCurr_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsCurrencies objCurrencies = new DataClass.clsCurrencies();
            this.storeBase.DataSource = objCurrencies.LoadList("IdModel IN (" + IdModel + ") AND Output=1", "ORDER BY BaseCurrency DESC,Name");
            this.storeBase.DataBind();
            DataTable dt = objCurrencies.LoadList("BaseCurrency = 1 AND IdModel = " + IdModel, "");
            if (dt.Rows.Count > 0)
            {
                ddlCurrency.Value = dt.Rows[0]["IdCurrency"];
            }
            else
            {
                ddlCurrency.SelectedItem.Index = 0;
            }
        }

        protected void StoreField_ReadData(object sender, StoreReadDataEventArgs e)
        {

            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dtField = new DataTable();
            if ((ddlLevels.Value == null) || (ddlLevels.Value.ToString() == "0") || (ddlLevels.Value.ToString() == ""))
            {
                dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
            }
            else
            {
                dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
            }
            DataRow drField = dtField.NewRow();
            drField["IdField"] = 0;
            drField["Name"] = "- All -";//drField["CodeName"] = "- All -"; //
            dtField.Rows.Add(drField);
            dtField.AcceptChanges();
            dtField.DefaultView.Sort = "Name";
            DataTable dtFieldSort = dtField.DefaultView.ToTable();

            this.storeField.DataSource = dtFieldSort;
            this.storeField.DataBind();

            this.ddlField.Value = 0;
        }

        protected void StoreCostType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsCostType objCostTypes = new DataClass.clsCostType();
            DataTable dt = objCostTypes.LoadList("", "ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                ddlCostType.Value = dt.Rows[0]["IdCostType"];
            }
            else
            {
                ddlCostType.SelectedItem.Index = 0;
            }
            foreach (DataRow row in dt.Rows)
            {
                if (idLanguage == 2)
                {
                    row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
                }
            }
            this.storeCostType.DataSource = dt;
            this.storeCostType.DataBind();
        }

        protected void StoreTechnicalScenarioType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsScenarios objScenarioTypes = new DataClass.clsScenarios();
            DataTable dt = objScenarioTypes.LoadList("", "ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                ddlCostType.Value = dt.Rows[0]["IdScenarioType"];
            }
            else
            {
                ddlCostType.SelectedItem.Index = 0;
            }
            foreach (DataRow row in dt.Rows)
            {
                if (idLanguage == 2)
                {
                    row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
                }
            }
            this.storeTechnicalScenarioType.DataSource = dt;
            this.storeTechnicalScenarioType.DataBind();
        }

        protected void StoreEconomicScenarioType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsScenarios objScenarioTypes = new DataClass.clsScenarios();
            DataTable dt = objScenarioTypes.LoadList("", "ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                ddlCostType.Value = dt.Rows[0]["IdScenarioType"];
            }
            else
            {
                ddlCostType.SelectedItem.Index = 0;
            }
            foreach (DataRow row in dt.Rows)
            {
                if (idLanguage == 2)
                {
                    row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
                }
            }
            this.storeEconomicScenarioType.DataSource = dt;
            this.storeEconomicScenarioType.DataBind();
        }

        protected void StoreOperationType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsOperationType objoperationTypes = new DataClass.clsOperationType();
            DataTable dtOperation = new DataTable();

            if (((ddlLevels.Value == null) || (ddlLevels.Value.ToString() == "0") || (ddlLevels.Value.ToString() == "")) && ((ddlField.Value == null) || (ddlField.Value.ToString() == "0") || (ddlField.Value.ToString() == "")))
            {
                dtOperation = objoperationTypes.LoadList("IdModel=" + IdModel, "");//"[vListTypesOperation].IdModel=" + IdModel, "");

                DataRow drOperation = dtOperation.NewRow();
                drOperation["IdTypeOperation"] = 0;
                drOperation["Name"] = "- All -";
                dtOperation.Rows.Add(drOperation);
                dtOperation.AcceptChanges();
                dtOperation.DefaultView.Sort = "Name";

                ddlOperationType.Value = 0;
            }
            else if (Convert.ToInt32(ddlLevels.Value) > 0)
            {
                dtOperation = objoperationTypes.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
                if (dtOperation.Rows.Count > 0)
                {
                    ddlOperationType.Value = dtOperation.Rows[0]["IdTypeOperation"];
                }
            }
            else
            {
                dtOperation = objoperationTypes.LoadList("IdModel=" + IdModel + " AND IdField = " + ddlField.Value, " Order by Name");//"[vListTypesOperation].IdModel=" + IdModel + " AND tblFields.IdField = " + ddlField.Value, " Order by Name");
                if (dtOperation.Rows.Count > 0)
                {
                    ddlOperationType.Value = dtOperation.Rows[0]["IdTypeOperation"];
                }
            }

            DataTable dtOperationSort = dtOperation.DefaultView.ToTable();

            this.storeOperationType.DataSource = dtOperationSort;
            this.storeOperationType.DataBind();
        }

        protected void StoreCostCategory_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsZiffAccount objZiffAccount = new DataClass.clsZiffAccount();
            DataTable dtZiffAccount = objZiffAccount.LoadComboBox("IdModel = " + IdModel + " AND Parent IS NULL", " ORDER BY Parent");
            DataRow drAccount = dtZiffAccount.NewRow();
            drAccount["IdZiffAccount"] = 0;
            drAccount["Name"] = "- All -";
            drAccount["Code"] = "- All -";
            drAccount["Parent"] = 0;
            dtZiffAccount.Rows.Add(drAccount);
            dtZiffAccount.AcceptChanges();
            dtZiffAccount.DefaultView.Sort = "Code";//"Parent";
            this.storeCostCategory.DataSource = dtZiffAccount;
            this.storeCostCategory.DataBind();
            ddlCostCategory.Value = 0;
        }

        #endregion

        #region Methods

        public Boolean GetProject()
        {
            Boolean Value = false;
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("Level IN (1,2) AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = true;
            }

            return Value;
        }

        protected void ClearReport()
        {
            if (PageIsLoaded)
            {
                this.storeBaseCostField.RemoveAll();
                this.pnlChartObject.ClearContent();
            }
        }

        protected void FromTo()
        {
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                this.txtFrom.Value = (Int32)dt.Rows[0]["BaseYear"];
                this.txtTo.Value = Convert.ToInt32(dt.Rows[0]["BaseYear"]) + Convert.ToInt32(dt.Rows[0]["IdProjection"]);

                this.txtFrom.MinValue = (Int32)dt.Rows[0]["BaseYear"];
                this.txtFrom.MaxValue = (Int32)dt.Rows[0]["BaseYear"] + (Int32)dt.Rows[0]["IdProjection"];

                this.txtFrom.MinValue = (Int32)dt.Rows[0]["BaseYear"];
                this.txtTo.MaxValue = Convert.ToInt32(dt.Rows[0]["BaseYear"]) + Convert.ToInt32(dt.Rows[0]["IdProjection"]);

                this.txtFrom.DataBind();
                this.txtTo.DataBind();

            }
        }

        protected Int32 GetIdOperation(String _TypeOperation)
        {
            Int32 Value = 0;
            DataClass.clsTypesOperation objTypesOperation = new DataClass.clsTypesOperation();
            DataTable dt = objTypesOperation.LoadList("Name = '" + _TypeOperation + "'", "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdTypeOperation"];
            }
            return Value;
        }

        private void FillGrid(Ext.Net.Store s, Ext.Net.GridPanel g, DataTable dt, ref ArrayList colVisible, Boolean IsBLBO, String CurrencyCode)
        {
            s.RemoveFields();

            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "ParentName" || c.ColumnName == "Name")
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Auto));
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Float));
                }
            }
            g.Features.Clear();

            if (!IsBLBO)
            {
                GroupingSummary gsum = new GroupingSummary();
                gsum.GroupHeaderTplString = "{name}";
                gsum.HideGroupedHeader = true;
                gsum.ShowSummaryRow = false;    //Hide the grouping Summary Totals by setting to false
                g.Features.Add(gsum);
            }
            
            GridFilters f = new GridFilters();
            f.Local = true;
            g.Features.Add(f);

            Summary smry = new Summary();
            smry.Dock = new SummaryDock();
            smry.ID = "Summary" + g.ID.ToString();
            smry.ShowSummaryRow = false;    //Hide the Grand Summary Totals by setting to false

            g.Features.Add(smry);

            foreach (DataColumn c in dt.Columns)
            {
                if (!c.ColumnName.Contains("Id"))
                {
                    if (c.ColumnName == "ParentName" || c.ColumnName == "Name")
                    {
                        StringFilter sFilter = new StringFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                    else
                    {
                        NumericFilter sFilter = new NumericFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                }
            }

            Column col = new Column();
            switch (s.ID)
            {
                case "storeBaseCostField":
                case "storeBaseLine": 
                case "storeBusinessOpportunities": 
                    col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Years") + " " + CurrencyCode;
                    break;
                default:
                    col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Years");
                    break;
            }
            SummaryColumn colSum = new SummaryColumn();
            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "ParentName" || c.ColumnName == "Name")
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        if (c.ColumnName != "ParentName")
                        {
                            colSum = new SummaryColumn();
                            colSum.DataIndex = c.ColumnName;
                            switch (s.ID)
                            {
                                case "storeBaseCostField":
                                    colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", "Field");
                                    break;
                                case "storeVolumeTotal":
                                    colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", "Scenario").ToString().Substring(0,GetGlobalResourceObject("CPM_Resources", "Scenario").ToString().Length-1);
                                    break;
                                default:
                                    colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", "Project");
                                    break;
                            }
                            colSum.Locked = true;
                            colVisible.Add(new DataClass.DataTableColumnDisplay(c.ColumnName, colSum.Text));
                            colSum.SummaryType = Ext.Net.SummaryType.Count;
                            colSum.TdCls = "task";
                            colSum.Sortable = true;
                            colSum.Groupable = true;
                            colSum.Width = 250;
                            colSum.Renderer.Fn = "formatCol";
                            colSum.MenuDisabled = false;
                            colSum.Align = Alignment.Left;
                            g.ColumnModel.Columns.Add(colSum);
                        }
                    }
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        colSum = new SummaryColumn();
                        colSum.DataIndex = c.ColumnName;
                        colSum.Text = c.ColumnName;
                        colVisible.Add(new DataClass.DataTableColumnDisplay(c.ColumnName, colSum.Text));
                        colSum.SummaryType = Ext.Net.SummaryType.Sum;
                        Renderer sumRen = new Renderer();
                        switch (s.ID)
                        {
                            case "storeBaseCostField":
                            case "storeBaseLine":
                            case "storeBusinessOpportunities":
                                sumRen.Fn = "formatColData";

                                break;
                            default:
                                sumRen.Fn = "Ext.util.Format.numberRenderer('0,000')";
                                break;
                        }
                        colSum.SummaryRenderer = sumRen;
                        colSum.Renderer = sumRen;
                        colSum.Align = Alignment.Right;
                        colSum.MinWidth = 80;
                        col.Columns.Add(colSum);
                    }
                }
            }

            g.ColumnModel.Columns.Add(col);

            s.DataSource = dt;
            //s.GroupField = "ParentName";
            
            if (X.IsAjaxRequest)
            {
                g.Reconfigure();
            }
            s.DataBind();
        }

        private void FillChart(Ext.Net.Store s, Ext.Net.Chart c, DataTable dt, String CurrencyCode)
        {
            string[] str = new string[dt.Columns.Count - 1];
            int i = 0;
            foreach (DataColumn col in dt.Columns)
            {
                if (col.ColumnName != "Year")
                {
                    str[i] = col.ColumnName;
                    i = i + 1;
                }
            }
            string[] strcat = new string[] { "Year" };

            s.RemoveFields();

            foreach (DataColumn col in dt.Columns)
            {
                s.AddField(new ModelField(col.ColumnName, ModelFieldType.Auto));
            }
            s.DataSource = dt;
            s.DataBind();

            AxisLabel AxisL = new AxisLabel();
            AxisL.Renderer.Handler = "return Ext.util.Format.number(value, '0,000.00');";

            c.Axes.Add(new NumericAxis()
            {
                Title = (String)GetGlobalResourceObject("CPM_Resources", "Costs") + " " + CurrencyCode,
                Fields = str,
                MajorTickSteps = 9,
                Grid = true,
                Position = Position.Left,
                GridConfig = new AxisGrid()
                {
                    Odd = new SpriteAttributes()
                    {
                        Opacity = 1,
                        Fill = "#EFEBEF",
                        Stroke = "#EFEBEF",
                        StrokeWidth = 0.5
                    }
                },
                Label = AxisL
            });

            c.Axes.Add(new CategoryAxis()
            {
                Title = (String)GetGlobalResourceObject("CPM_Resources", "Year"),
                Fields = strcat,
                Position = Position.Bottom,
                Grid = true
            });

            foreach (string str1 in str)
            {
                ChartTip chrtTip = new ChartTip();
                chrtTip.TrackMouse = true;
                chrtTip.Width = 140;
                chrtTip.StyleSpec = "background:#fff; text-align:center;";
                chrtTip.Height = 20;
                chrtTip.Renderer.Handler = "this.setTitle(String(Ext.util.Format.number(item.value[1], '0,000.00')));";

                c.Series.Add(new LineSeries()
                {
                    Titles = new string[] { str1 },
                    XField = strcat,
                    YField = new string[] { str1 },
                    Axis = Position.Left,
                    //Fill = true,
                    Tips = chrtTip,
                    MarkerConfig = new SpriteAttributes()
                    {
                        Type = Ext.Net.SpriteType.Circle,
                        Size = 4,
                        Radius = 4,
                        StrokeWidth = 0
                    },
                    HighlightConfig = new SpriteAttributes()
                    {
                        Size = 5,
                        Radius = 5
                    },
                });
            }
            c.Height = ReportFieldCount * 75 < 320 ? 320 : ReportFieldCount * 75;
            c.Animate = true;
            c.Shadow = true;
            c.Frame = true;
            c.StyleSpec = "background:#fff;";
            pnlChartObject.Render();
        }

        protected void txtTo_Select(object sender, DirectEventArgs e)
        {
            this.txtTo.MinValue = Convert.ToInt32(txtFrom.Value);
            this.txtTo.DataBind();
        }

        private Int32 GetIdField(String _Name)
        {
            Int32 intID = 0;

            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dtField = objFields.LoadList("IdModel = " + IdModel + " AND Name = '" + _Name + "'", "ORDER BY Name");

            if (dtField.Rows.Count > 0)
            {
                intID = (Int32)dtField.Rows[0]["IdField"];
            }

            return intID;
        }

        private Int32 GetIdLevel(String _Name)
        {
            Int32 intID = 0;

            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dtLevel = objAggregationLevels.LoadList("IdModel = " + IdModel + " AND Name = '" + _Name + "'", "ORDER BY Name");

            if (dtLevel.Rows.Count > 0)
            {
                intID = (Int32)dtLevel.Rows[0]["IdAggregationLevel"];
            }

            return intID;
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExportOpexTOT = new DataReportObject();
            StoredExportOpexTOT = ExportOpexTOT;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExportOpexTOT);
            objWriter.BuildExportMultiFieldsReport(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2007);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=MultipleFieldsProjection.xlsx");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        private void DisableParameterSelection()
        {
            this.pnlExportToolbar.Hidden = false;
            this.pnlReportToolbar.Hidden = true;
            this.ddlLevels.ReadOnly = true;
            this.ddlField.ReadOnly = true;
            this.ddlStructure.ReadOnly = true;
            this.txtFrom.ReadOnly = true;
            this.txtTo.ReadOnly = true;
            this.ddlScenarioTechnical.ReadOnly = true;
            this.ddlScenarioEconomic.ReadOnly = true;
            this.ddlCurrency.ReadOnly = true;
            this.ddlTerm.ReadOnly = true;
            this.pnlMainChartPanel.Hidden = false;
            this.ddlCostType.ReadOnly=true;
            this.ddlOperationType.ReadOnly=true;
            this.ddlFactor.ReadOnly = true;
            this.ddlCostCategory.ReadOnly = true;
        }

        //private System.Drawing.Bitmap ChartImage(Ext.Net.Chart c)
        //{
        ////    HttpUtility.HtmlEncode(c.CallSurface)
        ////    Ext.Net.Utilities
        ////    var svg = Ext.htmlEncode(Ext.draw.engine.SvgExporter.generate(btn.up('panel').down('chart').surface));
        //}

        private void SavePNG(Stream SaveStream, string SVGDoc)
        {
            //XmlDocument xd = new XmlDocument();
            //xd.XmlResolver = null;
            //xd.LoadXml(SVGDoc);
            //var svgGraph = Svg.SvgDocument.Open(xd);
            //svgGraph.Draw().Save(SaveStream, System.Drawing.Imaging.ImageFormat.Png);
        }

        public void ModelCheckRecalcModuleReport()
        {
            clsModels objModelRecalc = new clsModels();
            objModelRecalc.IdModel = IdModel;
            objModelRecalc.CheckRecalcModuleParameter(objApplication.MaxRelationLevels);
            objModelRecalc.CheckRecalcModuleAllocation();
            objModelRecalc.CheckRecalcModuleTechnical();
            objModelRecalc.CheckRecalcModuleEconomic();
            objModelRecalc = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = (String)GetGlobalResourceObject("CPM_Resources", "Multiple Fields Projection");
            this.btnReset.Text = modMain.strReportsModuleReset;
            this.btnSaveExcel.Text = modMain.strReportsModuleExportReportDataToExcel;
            this.lblLevels.Text = modMain.strReportsModuleAggregationLevel;
            this.lblField.Text = modMain.strTechnicalModuleField;
            this.lblStructure.Text = modMain.strReportsModuleCostStructure;
            this.lblFrom.Text = modMain.strReportsModuleFrom;
            this.lblTo.Text = modMain.strReportsModuleTo;
            this.lblScenarioTechnical.Text = modMain.strReportsModuleTechnicalScenario;
            this.lblScenarioEconomic.Text = modMain.strReportsModuleEconomicScenario;
            this.lblCurrency.Text = modMain.strReportsModuleCurrency;
            this.lblTerm.Text = modMain.strReportsModuleTerm;
            this.btnRun.Text = (String)GetGlobalResourceObject("CPM_Resources", "Run Report"); //modMain.strReportsModuleRunReport;
            this.ReportTitle01.Title = (String)GetGlobalResourceObject("CPM_Resources", "TotalProjection");
            this.lblOperationType.Text = modMain.strReportsModuleFormOperationType + ":";
            this.lblCostType.Text = modMain.strReportsModuleFormTypeCost + ":";
            this.btnDirectExcel.Text = modMain.strReportsModuleExportReportDataToExcel;
            this.lblCostCategory.Text = (String)GetGlobalResourceObject("CPM_Resources", "Cost Category") + ":";
        }

        #endregion

    }
}
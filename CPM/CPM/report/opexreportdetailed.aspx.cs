﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using Ext.Net.Utilities;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Diagnostics;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CD;
using DataClass;
using System.Web.Script.Serialization;

namespace CPM
{
    public partial class _opexreportdetailed : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdActivity { get { return (Int32)Session["IdActivity"]; } set { Session["IdActivity"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }
        private Boolean PageIsLoaded { get; set; }
        private DataReportObject ExportPortfolioBL { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }
        private DataReportObject ExportPortfolioBO { get { return (DataReportObject)Session["Export02"]; } set { Session["Export02"] = value; } }
        private DataReportObject ExportReportAll { get { return (DataReportObject)Session["Export03"]; } set { Session["Export03"] = value; } }
        private DataReportObject ExportReportFields { get { return (DataReportObject)Session["Export04"]; } set { Session["Export04"] = value; } }
        private Int32 ReportCriteriaCount = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (GetProject())
            {
                this.lblScenarioEconomic.Hide();
                this.ddlScenarioEconomic.Hide();
                this.lblTerm.Hide();
                this.ddlTerm.Hide();
            }

            if (!X.IsAjaxRequest)
            {
                LoadLabel();
                Session["Export01"] = null;
                Session["Export02"] = null;
                Session["Export03"] = null;
                Session["Export04"] = null;
                PageIsLoaded = false;
                IdActivity = 0;
                this.ddlStructure.SelectedItem.Index = 0;
                this.ddlTerm.SelectedItem.Index = 1;
                this.ddlScenarioTechnical.SelectedItem.Index = 0;
                this.ddlScenarioEconomic.SelectedItem.Index = 0;
                this.ddlOperationType.SelectedItem.Index = 0;
                this.ddlCostType.SelectedItem.Index = 0;
                this.ddlFactor.SelectedItem.Index = 1;
                this.FromTo();
                DataTable dtOpex = new DataTable();
                DataTable dtBL = new DataTable();
                DataTable dtBO = new DataTable();
                DataTable dtVRG = new DataTable();
                DataTable dtVBL = new DataTable();
                DataTable dtVBO = new DataTable();
                ArrayList datColumnOpex = new ArrayList();
                ArrayList datColumnBL = new ArrayList();
                ArrayList datColumnBO = new ArrayList();
                ArrayList datColumnVRG = new ArrayList();
                ArrayList datColumnVBL = new ArrayList();
                ArrayList datColumnVBO = new ArrayList();
                this.FillGrid(this.storeBaseCostField, this.grdOpexProjection, dtOpex, ref datColumnOpex, false, null);
                this.FillGridTech(this.storeVolumeTotal, this.grdVolumeTotal, dtVRG, ref datColumnVRG, false, null);
                this.FillGrid(this.storeBaseLine, this.grdBaseLine, dtBL, ref datColumnBL, true, null);
                this.FillGridTech(this.storeVolumeTotalBL, this.grdVolumeTotalBL, dtVBL, ref datColumnVBL, true, null);
                this.FillGrid(this.storeBusinessOpportunities, this.grdBusinessOpportunities, dtBO, ref datColumnBO, true, null);
                this.FillGridTech(this.storeVolumeTotal, this.grdVolumeTotal, dtVRG, ref datColumnVRG, true, null);
                this.FillGridTech(this.storeVolumeTotalBO, this.grdVolumeTotalBO, dtVBO, ref datColumnVBO, true, null);
                PageIsLoaded = true;
            }
        }

        protected void ddlLevels_Select(object sender, DirectEventArgs e)
        {
            this.ddlField.Reset();
            this.storeField.Reload();
            this.ddlOperationType.Reset();
            this.storeOperationType.Reload();
        }

        protected void ddlField_Select(object sender, DirectEventArgs e)
        {
            this.ddlOperationType.Reset();
            this.storeOperationType.Reload();
        }

        protected void btnReset_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/report/opexreportdetailed.aspx");
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            btnSaveExcelDirect_Click(null, null);
        }

        protected void btnSaveExcelDirect_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.ModelCheckRecalcModuleReport();
                DisableParameterSelection();

                DataClass.clsModels objModels = new DataClass.clsModels();

                Int32 intLevel = Convert.ToInt32(ddlLevels.Value);
                Int32 intField = Convert.ToInt32(ddlField.Value);
                Int32 intStructure = Convert.ToInt32(ddlStructure.Value);
                Int32 intCurrency = Convert.ToInt32(ddlCurrency.Value);
                Int32 intTerm = 2;
                Int32 intTechnicalScenario = Convert.ToInt32(ddlScenarioTechnical.Value);
                Int32 intEconomicScenario = 0;
                Int32 intCostType = Convert.ToInt32(ddlCostType.Value);
                Int32 intTypeOPeration = 0;
                Int32 intFactor = Convert.ToInt32(ddlFactor.SelectedItem.Text);
                String strCurrencyCode = "";

                if (this.ddlScenarioEconomic.Hidden == false)
                {
                    intEconomicScenario = Convert.ToInt32(ddlScenarioEconomic.Value);
                }
                Int32 intFrom = Convert.ToInt32(txtFrom.Value);
                Int32 intTo = Convert.ToInt32(txtTo.Value);

                clsCurrencies objCurrencies = new clsCurrencies();
                objCurrencies.IdCurrency = intCurrency;
                objCurrencies.loadObject();
                if (intFactor == 1000)
                {
                    strCurrencyCode = "(1M_" + objCurrencies.Code + ")";
                }
                else
                {
                    strCurrencyCode = "(" + objCurrencies.Code + ")";
                }

                DataSet ds = objModels.LoadOpexProjectionDetailed(IdModel, intLevel, intField, intStructure, intFrom, intTo, intTechnicalScenario, intEconomicScenario, intCurrency, intTerm, 0, intCostType, intTypeOPeration, idLanguage, intFactor);

                DataTable dtExportAll = new DataTable();
                dtExportAll = ds.Tables[10];
                DataTable dtExportFields = new DataTable();
                dtExportFields = ds.Tables[11];

                ArrayList datColumnExport = new ArrayList();

                String strSelectedParameters = lblLevels.Text + " '" + ddlLevels.SelectedItem.Text + "', " +
                                               lblField.Text + " '" + ddlField.SelectedItem.Text + "', " +
                                               lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "', " +
                                               lblCostType.Text + " '" + ddlCostType.SelectedItem.Text + "'," +
                                               lblScenarioTechnical.Text + " '" + ddlScenarioTechnical.SelectedItem.Text + "', " +
                                               lblFrom.Text + " '" + txtFrom.Text + "', " +
                                               lblTo.Text + " '" + txtTo.Text + "', " +
                                               lblFactor.Text + " '" + ddlFactor.SelectedItem.Text + "'";
                ExportReportAll = new DataReportObject(dtExportAll, "Grand Total", "Projection By Cost Type", strSelectedParameters, null, null, datColumnExport, null);
                ExportReportFields = new DataReportObject(dtExportFields, "NewTab", "Projection By Cost Type", strSelectedParameters, null, null, datColumnExport, null);

                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Generated Successfully!"), IconCls = "icon-accept", Clear2 = false });
                X.Mask.Hide();

                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                if (strMessage.Contains("Timeout"))
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = strMessage.ToString(), IconCls = "icon-exclamation", Clear2 = false });
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select all filters please"), IconCls = "icon-exclamation", Clear2 = false });
                }
            }
        }

        protected void btnSavePortfolio_Click(object sender, DirectEventArgs e)
        {
            btnSavePortfolioDirect_Click(null, null);
        }

        protected void btnSavePortfolioDirect_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.ModelCheckRecalcModuleReport();
                DisableParameterSelection();

                DataClass.clsModels objModels = new DataClass.clsModels();

                Int32 intLevel = Convert.ToInt32(ddlLevels.Value);
                Int32 intField = Convert.ToInt32(ddlField.Value);
                Int32 intStructure = Convert.ToInt32(ddlStructure.Value);
                Int32 intCurrency = Convert.ToInt32(ddlCurrency.Value);
                Int32 intTerm = 2;
                Int32 intTechnicalScenario = Convert.ToInt32(ddlScenarioTechnical.Value);
                Int32 intEconomicScenario = 0;
                Int32 intCostType = Convert.ToInt32(ddlCostType.Value);
                Int32 intTypeOPeration = 0;
                Int32 intFactor = Convert.ToInt32(ddlFactor.SelectedItem.Text);
                String strCurrencyCode = "";

                if (this.ddlScenarioEconomic.Hidden == false)
                {
                    intEconomicScenario = Convert.ToInt32(ddlScenarioEconomic.Value);
                }
                Int32 intFrom = Convert.ToInt32(txtFrom.Value);
                Int32 intTo = Convert.ToInt32(txtTo.Value);

                clsCurrencies objCurrencies = new clsCurrencies();
                objCurrencies.IdCurrency = intCurrency;
                objCurrencies.loadObject();
                if (intFactor == 1000)
                {
                    strCurrencyCode = "(1M_" + objCurrencies.Code + ")";
                }
                else
                {
                    strCurrencyCode = "(" + objCurrencies.Code + ")";
                }

                DataSet ds = objModels.LoadOpexProjectionDetailed(IdModel, intLevel, intField, intStructure, intFrom, intTo, intTechnicalScenario, intEconomicScenario, intCurrency, intTerm, 0, intCostType, intTypeOPeration, idLanguage, intFactor);

                DataTable dtBLPortfolio = new DataTable();
                dtBLPortfolio = ds.Tables[3];
                DataTable dtBOPortfolio = new DataTable();
                dtBOPortfolio = ds.Tables[4];

                ArrayList datColumnBLExport = new ArrayList();
                ArrayList datColumnBOExport = new ArrayList();

                String strSelectedParameters = lblLevels.Text + " '" + ddlLevels.SelectedItem.Text + "', " +
                                               lblField.Text + " '" + ddlField.SelectedItem.Text + "', " +
                                               lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "', " +
                                               lblCostType.Text + " '" + ddlCostType.SelectedItem.Text + "'," +
                                               lblScenarioTechnical.Text + " '" + ddlScenarioTechnical.SelectedItem.Text + "', " +
                                               lblFrom.Text + " '" + txtFrom.Text + "', " +
                                               lblTo.Text + " '" + txtTo.Text + "', " +
                                               lblFactor.Text + " '" + ddlFactor.SelectedItem.Text + "'";
                ExportPortfolioBL = new DataReportObject(dtBLPortfolio, "Baseline", "Opex Portfolio Report", strSelectedParameters, null, "Field", datColumnBLExport, null);
                ExportPortfolioBO = new DataReportObject(dtBOPortfolio, "Opportunities", "Opex Portfolio Report", strSelectedParameters, null, "Field", datColumnBOExport, null);

                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Generated Successfully!"), IconCls = "icon-accept", Clear2 = false });
                X.Mask.Hide();

                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedSavePortfolioReportYES({isUpload:true})",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                if (strMessage.Contains("Timeout"))
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = strMessage.ToString(), IconCls = "icon-exclamation", Clear2 = false });
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select all filters please"), IconCls = "icon-exclamation", Clear2 = false });
                }
            }
        }

        protected void btnRun_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.ModelCheckRecalcModuleReport();
                DisableParameterSelection();

                DataClass.clsModels objModels = new DataClass.clsModels();

                Int32 intLevel = Convert.ToInt32(ddlLevels.Value);
                Int32 intField = Convert.ToInt32(ddlField.Value);
                Int32 intStructure = 1;// Convert.ToInt32(ddlStructure.Value);
                Int32 intCurrency = Convert.ToInt32(ddlCurrency.Value);
                Int32 intTerm = 2;
                Int32 intTechnicalScenario = Convert.ToInt32(ddlScenarioTechnical.Value);
                Int32 intEconomicScenario = 0;
                Int32 intCostType = Convert.ToInt32(ddlCostType.Value);
                Int32 intTypeOPeration = 0;
                Int32 intFactor = Convert.ToInt32(ddlFactor.SelectedItem.Text);
                String strCurrencyCode = "";

                if (this.ddlScenarioEconomic.Hidden == false)
                {
                    intEconomicScenario = Convert.ToInt32(ddlScenarioEconomic.Value);
                }
                Int32 intFrom = Convert.ToInt32(txtFrom.Value);
                Int32 intTo = Convert.ToInt32(txtTo.Value);

                clsCurrencies objCurrencies = new clsCurrencies();
                objCurrencies.IdCurrency = intCurrency;
                objCurrencies.loadObject();
                if (intFactor == 1000)
                {
                    strCurrencyCode = "(1M_" + objCurrencies.Code + ")";
                }
                else
                {
                    strCurrencyCode = "(" + objCurrencies.Code + ")";
                }

                DataSet ds = objModels.LoadOpexProjectionDetailed(IdModel, intLevel, intField, intStructure, intFrom, intTo, intTechnicalScenario, intEconomicScenario, intCurrency, intTerm, 0, intCostType, intTypeOPeration, idLanguage, intFactor);

                DataTable dtOpex = new DataTable();
                dtOpex = ds.Tables[0];
                DataTable dtBL = new DataTable();
                dtBL = ds.Tables[1];
                DataTable dtBO = new DataTable();
                dtBO = ds.Tables[2];
                DataTable dtBLPortfolio = new DataTable();
                dtBLPortfolio = ds.Tables[3];
                DataTable dtBOPortfolio = new DataTable();
                dtBOPortfolio = ds.Tables[4];
                DataTable dtVRG = new DataTable();
                dtVRG = ds.Tables[5];
                DataTable dtVBL = new DataTable();
                dtVBL = ds.Tables[6];
                DataTable dtVBO = new DataTable();
                dtVBO = ds.Tables[7];
                DataTable dtChart = new DataTable();
                dtChart = ds.Tables[8];
                DataTable dtStats = new DataTable();
                dtStats = ds.Tables[9];
                DataTable dtExport = new DataTable();
                dtExport = ds.Tables[10];

                ReportCriteriaCount = Convert.ToInt32(dtStats.Rows[0]["ReportCriteriaCount"].ToString());

                ArrayList datColumnOpex = new ArrayList();
                ArrayList datColumnBL = new ArrayList();
                ArrayList datColumnBO = new ArrayList();
                ArrayList datColumnVRG = new ArrayList();
                ArrayList datColumnVBL = new ArrayList();
                ArrayList datColumnVBO = new ArrayList();
                ArrayList datColumnBLExport = new ArrayList();
                ArrayList datColumnBOExport = new ArrayList();

                this.FillGrid(this.storeBaseCostField, this.grdOpexProjection, dtOpex, ref datColumnOpex, false, strCurrencyCode);
                this.FillGridTech(this.storeVolumeTotal, this.grdVolumeTotal, dtVRG, ref datColumnVRG, true, strCurrencyCode);
                this.FillGrid(this.storeBaseLine, this.grdBaseLine, dtBL, ref datColumnBL, false, strCurrencyCode);
                this.FillGridTech(this.storeVolumeTotalBL, this.grdVolumeTotalBL, dtVBL, ref datColumnVBL, true, strCurrencyCode);
                this.FillGrid(this.storeBusinessOpportunities, this.grdBusinessOpportunities, dtBO, ref datColumnBO, true, strCurrencyCode);
                this.FillGridTech(this.storeVolumeTotalBO, this.grdVolumeTotalBO, dtVBO, ref datColumnVBO, true, strCurrencyCode);
                this.FillColumnChart(this.storeChart, this.chtChart, dtChart, strCurrencyCode);
                pnlChartObject.Render();

                String strSelectedParameters = lblLevels.Text + " '" + ddlLevels.SelectedItem.Text + "', " +
                                               lblField.Text + " '" + ddlField.SelectedItem.Text + "', " +
                                               lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "', " +
                                               lblCostType.Text + " '" + ddlCostType.SelectedItem.Text + "'," +
                                               lblScenarioTechnical.Text + " '" + ddlScenarioTechnical.SelectedItem.Text + "', " +
                                               lblFrom.Text + " '" + txtFrom.Text + "', " +
                                               lblTo.Text + " '" + txtTo.Text + "', " +
                                               lblFactor.Text + " '" + ddlFactor.SelectedItem.Text + "'";
                ExportPortfolioBL = new DataReportObject(dtBLPortfolio, "Baseline", "Projection By Cost Type", strSelectedParameters, null, "Field", datColumnBLExport, null);
                ExportPortfolioBO = new DataReportObject(dtBOPortfolio, "Opportunities", "Projection By Cost Type", strSelectedParameters, null, "Field", datColumnBOExport, null);
                ExportReportAll = new DataReportObject(dtExport, "Grand Total", "Projection By Cost Type", strSelectedParameters, null, null, datColumnBOExport, null);
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Report Generated Successfully!"), IconCls = "icon-accept", Clear2 = false });
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                if (strMessage.Contains("Timeout"))
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = strMessage.ToString(), IconCls = "icon-exclamation", Clear2 = false });
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select all filters please"), IconCls = "icon-exclamation", Clear2 = false });
                }
            }
        }

        #endregion

        #region Methods

        public Boolean GetProject()
        {
            Boolean Value = false;
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("Level IN (1,2) AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = true;
            }

            return Value;
        }

        protected void ClearReport()
        {
            if (PageIsLoaded)
            {
                this.storeBaseCostField.RemoveAll();
                this.storeBaseLine.RemoveAll();
                this.storeBusinessOpportunities.RemoveAll();
                this.storeVolumeTotal.RemoveAll();
                this.storeVolumeTotalBL.RemoveAll();
                this.storeVolumeTotalBO.RemoveAll();
                this.pnlChartObject.ClearContent();
            }
        }

        protected void FromTo()
        {
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                this.txtFrom.Value = (Int32)dt.Rows[0]["BaseYear"];
                this.txtTo.Value = Convert.ToInt32(dt.Rows[0]["BaseYear"]) + Convert.ToInt32(dt.Rows[0]["IdProjection"]);

                this.txtFrom.MinValue = (Int32)dt.Rows[0]["BaseYear"];
                this.txtFrom.MaxValue = (Int32)dt.Rows[0]["BaseYear"] + (Int32)dt.Rows[0]["IdProjection"];

                this.txtFrom.MinValue = (Int32)dt.Rows[0]["BaseYear"];
                this.txtTo.MaxValue = Convert.ToInt32(dt.Rows[0]["BaseYear"]) + Convert.ToInt32(dt.Rows[0]["IdProjection"]);

                this.txtFrom.DataBind();
                this.txtTo.DataBind();

            }
        }

        protected Int32 GetIdOperation(String _TypeOperation)
        {
            Int32 Value = 0;
            DataClass.clsTypesOperation objTypesOperation = new DataClass.clsTypesOperation();
            DataTable dt = objTypesOperation.LoadList("Name = '" + _TypeOperation + "'", "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdTypeOperation"];
            }
            return Value;
        }

        private void FillGrid(Ext.Net.Store s, Ext.Net.GridPanel g, DataTable dt, ref ArrayList colVisible, Boolean IsBLBO, String CurrencyCode)
        {
            s.RemoveFields();

            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "FieldProject" || c.ColumnName == "ProjectionCriteria")
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Auto));
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Float));
                }
            }
            g.Features.Clear();

            GroupingSummary gsum = new GroupingSummary();
            gsum.GroupHeaderTplString = "{name}";
            gsum.HideGroupedHeader = true;
            gsum.ShowSummaryRow = true;    //Hide the grouping Summary Totals by setting to false
            g.Features.Add(gsum);

            GridFilters f = new GridFilters();
            f.Local = true;
            g.Features.Add(f);

            Summary smry = new Summary();
            smry.Dock = new SummaryDock();
            smry.ID = "Summary" + g.ID.ToString();
            smry.ShowSummaryRow = true;    //Hide the Grand Summary Totals by setting to false
            g.Features.Add(smry);

            foreach (DataColumn c in dt.Columns)
            {
                if (!c.ColumnName.Contains("Id"))
                {
                    if (c.ColumnName == "ProjectionCriteria" || c.ColumnName == "FieldProject")
                    {
                        StringFilter sFilter = new StringFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                    else
                    {
                        NumericFilter sFilter = new NumericFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                }
            }

            Column col = new Column();
            switch (s.ID)
            {
                case "storeBaseCostField":
                case "storeBaseLine":
                case "storeBusinessOpportunities":
                    col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Years") + " " + CurrencyCode;
                    break;
                default:
                    col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Years");
                    break;
            }
            SummaryColumn colSum = new SummaryColumn();
            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "ProjectionCriteria" || c.ColumnName == "FieldProject")
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        if (c.ColumnName != "FieldProject")
                        {
                            colSum = new SummaryColumn();
                            colSum.DataIndex = c.ColumnName;
                            colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", "Projection Criteria");
                            switch (s.ID)
                            {
                                case "storeBaseCostField":
                                case "storeBaseLine":
                                case "storeBusinessOpportunities":
                                    colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", "Projection Criteria");
                                    break;
                                case "storeVolumeTotal":
                                case "storeVolumeTotalBL":
                                case "storeVolumeTotalBO":
                                    colSum.Text = "";
                                    break;
                            }
                            colSum.Locked = true;
                            colVisible.Add(new DataClass.DataTableColumnDisplay(c.ColumnName, colSum.Text));
                            colSum.SummaryType = Ext.Net.SummaryType.Count;
                            colSum.TdCls = "task";
                            colSum.Sortable = true;
                            colSum.Groupable = true;
                            colSum.Width = 350;
                            colSum.SummaryRenderer.Handler = "return 'Total';";
                            colSum.MenuDisabled = false;
                            colSum.Align = Alignment.Left;
                            g.ColumnModel.Columns.Add(colSum);
                        }
                    }
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        colSum = new SummaryColumn();
                        colSum.DataIndex = c.ColumnName;
                        colSum.Text = c.ColumnName;
                        colVisible.Add(new DataClass.DataTableColumnDisplay(c.ColumnName, colSum.Text));
                        colSum.SummaryType = Ext.Net.SummaryType.Sum;
                        Renderer sumRen = new Renderer();
                        sumRen.Fn = "Ext.util.Format.numberRenderer('0,000.00')";
                        colSum.SummaryRenderer = sumRen;
                        colSum.Renderer = sumRen;
                        colSum.Align = Alignment.Right;
                        colSum.MinWidth = 80;
                        col.Columns.Add(colSum);
                    }
                }
            }

            g.ColumnModel.Columns.Add(col);

            s.DataSource = dt;
            //s.GroupField = "FieldProject";

            if (X.IsAjaxRequest)
            {
                g.Reconfigure();
            }
            s.DataBind();
        }

        private void FillGridTech(Ext.Net.Store s, Ext.Net.GridPanel g, DataTable dt, ref ArrayList colVisible, Boolean IsBLBO, String CurrencyCode)
        {
            s.RemoveFields();

            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "FieldProject" || c.ColumnName == "ProjectionCriteria")
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Auto));
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Float));
                }
            }
            g.Features.Clear();

            //if (IsBLBO)
            //{
            GroupingSummary gsum = new GroupingSummary();
            gsum.GroupHeaderTplString = "{name}";
            gsum.HideGroupedHeader = true;
            gsum.ShowSummaryRow = false;    //Hide the grouping Summary Totals by setting to false
            g.Features.Add(gsum);
            //}

            GridFilters f = new GridFilters();
            f.Local = true;
            g.Features.Add(f);

            Summary smry = new Summary();
            smry.Dock = new SummaryDock();
            smry.ID = "Summary" + g.ID.ToString();
            smry.ShowSummaryRow = false;    //Hide the Grand Summary Totals by setting to false
            g.Features.Add(smry);

            foreach (DataColumn c in dt.Columns)
            {
                if (!c.ColumnName.Contains("Id"))
                {
                    if (c.ColumnName == "ProjectionCriteria" || c.ColumnName == "FieldProject")
                    {
                        StringFilter sFilter = new StringFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                    else
                    {
                        NumericFilter sFilter = new NumericFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                }
            }

            Column col = new Column();
            switch (s.ID)
            {
                case "storeBaseCostField":
                case "storeBaseLine":
                case "storeBusinessOpportunities":
                    col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Years") + " " + CurrencyCode;
                    break;
                default:
                    col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Years");
                    break;
            }
            SummaryColumn colSum = new SummaryColumn();
            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "ProjectionCriteria" || c.ColumnName == "FieldProject")
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        if (c.ColumnName != "FieldProject")
                        {
                            colSum = new SummaryColumn();
                            colSum.DataIndex = c.ColumnName;
                            colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", "Projection Criteria");
                            switch (s.ID)
                            {
                                case "storeBaseCostField":
                                case "storeBaseLine":
                                case "storeBusinessOpportunities":
                                    colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", "Projection Criteria");
                                    break;
                                case "storeVolumeTotal":
                                case "storeVolumeTotalBL":
                                case "storeVolumeTotalBO":
                                    colSum.Text = "";
                                    break;
                            }
                            colSum.Locked = true;
                            colVisible.Add(new DataClass.DataTableColumnDisplay(c.ColumnName, colSum.Text));
                            colSum.SummaryType = Ext.Net.SummaryType.Count;
                            colSum.TdCls = "task";
                            colSum.Sortable = true;
                            colSum.Groupable = true;
                            colSum.Width = 350;
                            colSum.SummaryRenderer.Handler = "return 'Total';";
                            colSum.MenuDisabled = false;
                            colSum.Align = Alignment.Left;
                            g.ColumnModel.Columns.Add(colSum);
                        }
                    }
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        colSum = new SummaryColumn();
                        colSum.DataIndex = c.ColumnName;
                        colSum.Text = c.ColumnName;
                        colVisible.Add(new DataClass.DataTableColumnDisplay(c.ColumnName, colSum.Text));
                        colSum.SummaryType = Ext.Net.SummaryType.Sum;
                        Renderer sumRen = new Renderer();
                        sumRen.Fn = "Ext.util.Format.numberRenderer('0,000.00')";
                        colSum.SummaryRenderer = sumRen;
                        colSum.Renderer = sumRen;
                        colSum.Align = Alignment.Right;
                        colSum.MinWidth = 80;
                        col.Columns.Add(colSum);
                    }
                }
            }

            g.ColumnModel.Columns.Add(col);

            s.DataSource = dt;
            //s.GroupField = "FieldProject";

            if (X.IsAjaxRequest)
            {
                g.Reconfigure();
            }
            s.DataBind();
        }

        private void FillColumnChart(Ext.Net.Store s, Ext.Net.Chart c, DataTable dt, String CurrencyCode)
        {
            try
            {
                string[] str = new string[dt.Columns.Count - 1];
                int i = 0;
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName != "Year")
                    {
                        str[i] = col.ColumnName;
                        i = i + 1;
                    }
                }
                string[] strcat = new string[] { "Year" };

                s.RemoveFields();

                foreach (DataColumn col in dt.Columns)
                {
                    s.AddField(new ModelField(col.ColumnName, ModelFieldType.Auto));
                }
                s.DataSource = dt;
                s.DataBind();

                AxisLabel AxisL = new AxisLabel();
                AxisL.Renderer.Handler = "return Ext.util.Format.number(value, '0,000.00');";

                NumericAxis numericAxis = new NumericAxis()
                {
                    Title = (String)GetGlobalResourceObject("CPM_Resources", "Costs") + " " + CurrencyCode,
                    Fields = str,
                    MajorTickSteps = 9,
                    Grid = true,
                    Position = Position.Left,
                    GridConfig = new AxisGrid()
                    {
                        Odd = new SpriteAttributes()
                        {
                            Opacity = 1,
                            Fill = "#EFEBEF",
                            Stroke = "#EFEBEF",
                            StrokeWidth = 0.5
                        }
                    },
                    Label = AxisL
                };
                c.Axes.Add(numericAxis);


                AxisLabel AxisB = new AxisLabel();
                AxisB.Renderer.Handler = "return Ext.util.Format.number(value, '0000');";
                AxisB.Rotate = new RotateAttribute()
                {
                    Degrees = 90,
                };

                CategoryAxis categoryAxis = new CategoryAxis()
                {
                    Title = (String)GetGlobalResourceObject("CPM_Resources", "Year"),
                    Fields = strcat,
                    Position = Position.Bottom,
                    AdjustEnd = true,
                    CalculateCategoryCount = true,
                    Label = AxisB
                };
                c.Axes.Add(categoryAxis);

                ChartTip chrtTip = new ChartTip();
                chrtTip.TrackMouse = true;
                chrtTip.Width = 140;
                chrtTip.StyleSpec = "background:#fff; text-align:center;";
                chrtTip.Height = 20;
                chrtTip.Renderer.Handler = "this.setTitle(storeItem.get('" + strcat[0] + "') + ': ' + String(Ext.util.Format.number(item.value[1], '0,000.00')));";

                c.Series.Add(new ColumnSeries()
                {
                    Titles = str,
                    XField = strcat,
                    YField = str,
                    Axis = Position.Left,
                    Stacked = true,
                    Tips = chrtTip,
                });

                c.Height = ReportCriteriaCount * 75 < 320 ? 320 : ReportCriteriaCount * 75;
                c.Animate = true;
                c.Shadow = true;
                c.Frame = true;
                c.AutoSize = true;
                c.StyleSpec = "background:#fff;";
            }
            catch
            {
            }
        }

        protected void StoreLevel_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dt = objAggregationLevels.LoadList("IdModel = " + IdModel, "ORDER BY Name");

            DataRow drLevel = dt.NewRow();
            drLevel["IdAggregationLevel"] = 0;
            drLevel["Name"] = "- All -";
            dt.Rows.Add(drLevel);
            dt.AcceptChanges();
            dt.DefaultView.Sort = "Name";
            DataTable dtSort = dt.DefaultView.ToTable();

            this.storeLevels.DataSource = dtSort;
            this.storeLevels.DataBind();

            this.ddlLevels.Value = 0;

            this.storeLevels.DataBind();
        }

        protected void StoreCurr_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsCurrencies objCurrencies = new DataClass.clsCurrencies();
            this.storeBase.DataSource = objCurrencies.LoadList("IdModel IN (" + IdModel + ") AND Output=1", "ORDER BY BaseCurrency DESC,Name");
            this.storeBase.DataBind();
            DataTable dt = objCurrencies.LoadList("BaseCurrency = 1 AND IdModel = " + IdModel, "");
            if (dt.Rows.Count > 0)
            {
                ddlCurrency.Value = dt.Rows[0]["IdCurrency"];
            }
            else
            {
                ddlCurrency.SelectedItem.Index = 0;
            }
        }

        protected void StoreField_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dtField = new DataTable();
            if ((ddlLevels.Value == null) || (ddlLevels.Value.ToString() == "0") || (ddlLevels.Value.ToString() == ""))
            {
                dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
            }
            else
            {
                dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
            }
            DataRow drField = dtField.NewRow();
            drField["IdField"] = 0;
            drField["Name"] = "- All -";
            dtField.Rows.Add(drField);
            dtField.AcceptChanges();
            dtField.DefaultView.Sort = "Name";
            DataTable dtFieldSort = dtField.DefaultView.ToTable();

            this.storeField.DataSource = dtFieldSort;
            this.storeField.DataBind();

            this.ddlField.Value = 0;
        }

        protected void StoreCostType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsCostType objCostTypes = new DataClass.clsCostType();
            DataTable dt = objCostTypes.LoadList("", "ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                ddlCostType.Value = dt.Rows[0]["IdCostType"];
            }
            else
            {
                ddlCostType.SelectedItem.Index = 0;
            }
            foreach (DataRow row in dt.Rows)
            {
                if (idLanguage == 2)
                {
                    row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
                }
            }
            this.storeCostType.DataSource = dt;
            this.storeCostType.DataBind();

        }

        protected void StoreTechnicalScenarioType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsScenarios objScenarioTypes = new DataClass.clsScenarios();
            DataTable dt = objScenarioTypes.LoadList("", "ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                ddlCostType.Value = dt.Rows[0]["IdScenarioType"];
            }
            else
            {
                ddlCostType.SelectedItem.Index = 0;
            }
            foreach (DataRow row in dt.Rows)
            {
                if (idLanguage == 2)
                {
                    row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
                }
            }
            this.storeTechnicalScenarioType.DataSource = dt;
            this.storeTechnicalScenarioType.DataBind();
        }

        protected void StoreEconomicScenarioType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsScenarios objScenarioTypes = new DataClass.clsScenarios();
            DataTable dt = objScenarioTypes.LoadList("", "ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                ddlCostType.Value = dt.Rows[0]["IdScenarioType"];
            }
            else
            {
                ddlCostType.SelectedItem.Index = 0;
            }
            foreach (DataRow row in dt.Rows)
            {
                if (idLanguage == 2)
                {
                    row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
                }
            }
            this.storeEconomicScenarioType.DataSource = dt;
            this.storeEconomicScenarioType.DataBind();
        }

        protected void StoreOperationType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsOperationType objoperationTypes = new DataClass.clsOperationType();
            DataTable dtOperation = new DataTable();

            if (((ddlLevels.Value == null) || (ddlLevels.Value.ToString() == "0") || (ddlLevels.Value.ToString() == "")) && ((ddlField.Value == null) || (ddlField.Value.ToString() == "0") || (ddlField.Value.ToString() == "")))
            {
                dtOperation = objoperationTypes.LoadList("IdModel=" + IdModel, "");//"[vListTypesOperation].IdModel=" + IdModel, "");

                DataRow drOperation = dtOperation.NewRow();
                drOperation["IdTypeOperation"] = 0;
                drOperation["Name"] = "- All -";
                dtOperation.Rows.Add(drOperation);
                dtOperation.AcceptChanges();
                dtOperation.DefaultView.Sort = "Name";

                ddlOperationType.Value = 0;
            }
            else if (Convert.ToInt32(ddlLevels.Value) > 0)
            {
                dtOperation = objoperationTypes.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
                if (dtOperation.Rows.Count > 0)
                {
                    ddlOperationType.Value = dtOperation.Rows[0]["IdTypeOperation"];
                }
            }
            else
            {
                dtOperation = objoperationTypes.LoadList("IdModel=" + IdModel + " AND IdField = " + ddlField.Value, " Order by Name");//"[vListTypesOperation].IdModel=" + IdModel + " AND tblFields.IdField = " + ddlField.Value, " Order by Name");
                if (dtOperation.Rows.Count > 0)
                {
                    ddlOperationType.Value = dtOperation.Rows[0]["IdTypeOperation"];
                }
            }

            DataTable dtOperationSort = dtOperation.DefaultView.ToTable();

            this.storeOperationType.DataSource = dtOperationSort;
            this.storeOperationType.DataBind();
        }

        protected void txtTo_Select(object sender, DirectEventArgs e)
        {
            this.txtTo.MinValue = Convert.ToInt32(txtFrom.Value);
            this.txtTo.DataBind();
        }

        private Int32 GetIdField(String _Name)
        {
            Int32 intID = 0;

            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dtField = objFields.LoadList("IdModel = " + IdModel + " AND Name = '" + _Name + "'", "ORDER BY Name");

            if (dtField.Rows.Count > 0)
            {
                intID = (Int32)dtField.Rows[0]["IdField"];
            }

            return intID;
        }

        private Int32 GetIdLevel(String _Name)
        {
            Int32 intID = 0;

            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dtLevel = objAggregationLevels.LoadList("IdModel = " + IdModel + " AND Name = '" + _Name + "'", "ORDER BY Name");

            if (dtLevel.Rows.Count > 0)
            {
                intID = (Int32)dtLevel.Rows[0]["IdAggregationLevel"];
            }

            return intID;
        }

        [DirectMethod]
        public void ClickedSavePortfolioReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExportPortfolioBL = new DataReportObject();
            StoredExportPortfolioBL = ExportPortfolioBL;
            DataReportObject StoredExportPortfolioBO = new DataReportObject();
            StoredExportPortfolioBO = ExportPortfolioBO;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExportPortfolioBL);
            objWriter.ExportReportCollection.Add(StoredExportPortfolioBO);
            objWriter.BuildExportPortfolio(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2007);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=OpexPortfolioReport.xlsx");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExportReportAll = new DataReportObject();
            StoredExportReportAll = ExportReportAll;
            DataReportObject StoredExportReportFields = new DataReportObject();
            StoredExportReportFields = ExportReportFields;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExportReportAll);
            objWriter.ExportReportCollection.Add(StoredExportReportFields);
            objWriter.BuildExportReport(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2007);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=ProjectionByCostType.xlsx");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        private void DisableParameterSelection()
        {
            this.pnlExportToolbar.Hidden = false;
            this.pnlReportToolbar.Hidden = true;
            this.ddlLevels.ReadOnly = true;
            this.ddlField.ReadOnly = true;
            this.ddlStructure.ReadOnly = true;
            this.txtFrom.ReadOnly = true;
            this.txtTo.ReadOnly = true;
            this.ddlScenarioTechnical.ReadOnly = true;
            this.ddlScenarioEconomic.ReadOnly = true;
            this.ddlCurrency.ReadOnly = true;
            this.ddlTerm.ReadOnly = true;
            this.ddlCostType.ReadOnly=true;
            this.ddlOperationType.ReadOnly=true;
            this.ddlFactor.ReadOnly = true;
            this.pnlMainChartPanel.Hidden = false;
        }

        //private System.Drawing.Bitmap ChartImage(Ext.Net.Chart c)
        //{
        ////    HttpUtility.HtmlEncode(c.CallSurface)
        ////    Ext.Net.Utilities
        ////    var svg = Ext.htmlEncode(Ext.draw.engine.SvgExporter.generate(btn.up('panel').down('chart').surface));
        //}

        private void SavePNG(Stream SaveStream, string SVGDoc)
        {
            //XmlDocument xd = new XmlDocument();
            //xd.XmlResolver = null;
            //xd.LoadXml(SVGDoc);
            //var svgGraph = Svg.SvgDocument.Open(xd);
            //svgGraph.Draw().Save(SaveStream, System.Drawing.Imaging.ImageFormat.Png);
        }

        public void ModelCheckRecalcModuleReport()
        {
            clsModels objModelRecalc = new clsModels();
            objModelRecalc.IdModel = IdModel;
            objModelRecalc.CheckRecalcModuleParameter(objApplication.MaxRelationLevels);
            objModelRecalc.CheckRecalcModuleAllocation();
            objModelRecalc.CheckRecalcModuleTechnical();
            objModelRecalc.CheckRecalcModuleEconomic();
            objModelRecalc = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = (String)GetGlobalResourceObject("CPM_Resources", "Projection by Case");//modMain.strReportsModuleOperatingCostProjectionDetailed;
            this.btnReset.Text = modMain.strReportsModuleReset;
            this.btnSaveExcel.Text = modMain.strReportsModuleExportReportDataToExcel;
            this.lblLevels.Text = modMain.strReportsModuleAggregationLevel;
            this.lblField.Text = modMain.strTechnicalModuleField;
            this.lblStructure.Text = modMain.strReportsModuleCostStructure;
            this.lblFrom.Text = modMain.strReportsModuleFrom;
            this.lblTo.Text = modMain.strReportsModuleTo;
            this.lblScenarioTechnical.Text = modMain.strReportsModuleTechnicalScenario;
            this.lblScenarioEconomic.Text = modMain.strReportsModuleEconomicScenario;
            this.lblCurrency.Text = modMain.strReportsModuleCurrency;
            this.lblTerm.Text = modMain.strReportsModuleTerm;
            this.btnRun.Text = (String)GetGlobalResourceObject("CPM_Resources", "Run Report"); //modMain.strReportsModuleRunReport;
            this.pnlBL.Title = (String)GetGlobalResourceObject("CPM_Resources", "Baseline");//modMain.strReportsModuleOpexResultsSubTitle1;
            this.pnlBO.Title = (String)GetGlobalResourceObject("CPM_Resources", "Business Opportunities");//modMain.strReportsModuleOpexResultsSubTitle2;
            this.lblCostType.Text = (String)GetGlobalResourceObject("CPM_Resources", "Cost Type") + ":";
            this.ReportTitle01.Title = (String)GetGlobalResourceObject("CPM_Resources", "TotalProjection");
            this.btnDirectExcel.Text = modMain.strReportsModuleExportReportDataToExcel;
            this.btnPortfolioDirect.Text = (String)GetGlobalResourceObject("CPM_Resources", "Portfolio report");
            this.btnSavePortfolio.Text = (String)GetGlobalResourceObject("CPM_Resources", "Portfolio Report");
            this.pnlVolumeResultsTotal.Title = (String)GetGlobalResourceObject("CPM_Resources", "Volume Results Total projection");
            this.pnlVolumeResultsBLTotal.Title = (String)GetGlobalResourceObject("CPM_Resources", "Volume Results Total baseline");
            this.pnlVolumeResultsBOTotal.Title = (String)GetGlobalResourceObject("CPM_Resources", "Volume Results Total Business Opportunities");
        }

        #endregion

    }
}
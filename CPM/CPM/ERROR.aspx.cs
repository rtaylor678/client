﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CPM
{
    public partial class _ERROR : System.Web.UI.Page
    {

        #region Definitions

        private Int32 idLanguage { get { return (Int32)Session["idLanguage"]; } }
        public String Language { get { return (String)Session["Language"]; } }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            this.LoadLabel();
        }

        #endregion

        #region Methods
        #endregion

        #region Funtions

        protected void LoadLabel()
        {
            this.ErrMessage.Text = (String)Session["GlobalError"];
        }

        #endregion

    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using Ext.Net.Utilities;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Diagnostics;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CD;
using DataClass;
using System.Web.Script.Serialization;

namespace CPM
{
    public partial class _multifieldopexreportCPM : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }
        private Boolean PageIsLoaded { get; set; }
        private DataReportObject ExportOpexTOT { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }
        private Int32 ReportFieldCount = 0;
        public Int32 IdModel = 0;
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (GetProject())
            {
                this.lblScenarioEconomic.Hide();
                this.ddlScenarioEconomic.Hide();
                this.lblTerm.Hide();
                this.ddlTerm.Hide();
            }

            if (!X.IsAjaxRequest)
            {
                this.LoadLanguage();
                LoadLabel();
                Session["Export01"] = null;
                PageIsLoaded = false;
                //IdActivity = 0;
                this.ddlStructure.SelectedItem.Index = 0;
                this.ddlTerm.SelectedItem.Index = 1;
                this.ddlScenarioTechnical.SelectedItem.Index = 0;
                this.ddlScenarioEconomic.SelectedItem.Index = 0;
                this.ddlOperationType.SelectedItem.Index = 0;
                this.ddlCostType.SelectedItem.Index = 0;
                this.ddlFactor.SelectedItem.Index = 1;
                this.FromTo();
                DataTable dtOpex = new DataTable();
                ArrayList datColumnOpex = new ArrayList();
                this.FillGrid(this.storeBaseCostField, this.grdOpexProjection, dtOpex, ref datColumnOpex, false, null);
                PageIsLoaded = true;
            }
        }

        protected void ddlLevels_Select(object sender, DirectEventArgs e)
        {
            this.ddlField.Reset();
            this.storeField.Reload();
            this.ddlOperationType.Reset();
            this.storeOperationType.Reload();
        }

        protected void ddlField_Select(object sender, DirectEventArgs e)
        {
            bool All = false;
            ddlField.DeselectItem(0);
            int i = 0;

            foreach (Ext.Net.ListItem field in this.ddlField.SelectedItems)
            {
                i++;
                if (Convert.ToInt32(field.Value) == 0 && i == this.ddlField.SelectedItems.Count)
                    All = true;
            }

            if (All)
            {
                ddlField.SelectItem(0);
                foreach (Ext.Net.ListItem items in this.ddlField.SelectedItems)
                {
                    if (Convert.ToInt32(items.Value) != 0)
                        this.ddlField.DeselectItem(items.Index);
                }
            }
            this.ddlOperationType.Reset();
            this.storeOperationType.Reload();
        }

        protected void ddlCostCategory_Select(object sender, DirectEventArgs e)
        {
            bool nonAllCC = false;
            foreach (Ext.Net.ListItem costCategory in this.ddlCostCategory.SelectedItems)
            {
                if (costCategory.Value != "- All -")
                    nonAllCC = true;
            }

            if (nonAllCC)
                ddlCostCategory.DeselectItem(0);
        }

        protected void btnReset_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/multifieldsopexreportCPM.aspx");
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            btnSaveExcelDirect_Click(null, null);
        }

        protected void btnSaveExcelDirect_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.ModelCheckRecalcModuleReport();
                DisableParameterSelection();

                DataClass.clsModels objModels = new DataClass.clsModels();

                Int32 intLevel = Convert.ToInt32(ddlLevels.Value);

                // Check that at least one model has been selected
                if (this.ddlModel.SelectedItems.Count == 0)
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = "No Model selected", IconCls = "icon-exclamation", Clear2 = false });

                string models = "";
                string paramModels = "";
                foreach (Ext.Net.ListItem model in this.ddlModel.SelectedItems)
                {
                    models += model.Value + ",";
                    paramModels += model.Text + ",";
                }

                //Need to remove trailing "," from the models and paramModels lists
                models = models.ReplaceLastInstanceOf(",", "");
                paramModels = paramModels.ReplaceLastInstanceOf(",", "");

                Int32 intStructure = 1;
                Int32 intCurrency = Convert.ToInt32(ddlCurrency.Value);
                Int32 intTerm = Convert.ToInt32(ddlTerm.Value);
                Int32 intTechnicalScenario = Convert.ToInt32(ddlScenarioTechnical.Value);
                Int32 intEconomicScenario = 0;
                Int32 intCostType = Convert.ToInt32(ddlCostType.Value);
                Int32 intTypeOPeration = Convert.ToInt32(ddlOperationType.Value);
                Int32 intFactor = Convert.ToInt32(ddlFactor.SelectedItem.Text);
                String strCurrencyCode = "";

                if (this.ddlScenarioEconomic.Hidden == false)
                {
                    intEconomicScenario = Convert.ToInt32(ddlScenarioEconomic.Value);
                }
                Int32 intFrom = Convert.ToInt32(txtFrom.Value);
                Int32 intTo = Convert.ToInt32(txtTo.Value);

                clsCurrencies objCurrencies = new clsCurrencies();
                objCurrencies.IdCurrency = intCurrency;
                objCurrencies.loadObject();
                if (intFactor == 1000)
                {
                    strCurrencyCode = "(1M_" + objCurrencies.Code + ")";
                }
                else
                {
                    strCurrencyCode = "(" + objCurrencies.Code + ")";
                }

                // Check that at least one cost category has been selected, if none default to 'All'
                if (this.ddlCostCategory.SelectedItems.Count == 0)
                    ddlCostCategory.SelectedItems.Add(new Ext.Net.ListItem("0"));

                string costCategories = "";
                string paramCostCategories = "";
                foreach (Ext.Net.ListItem costCategory in this.ddlCostCategory.SelectedItems)
                {
                    if (costCategory.Value != "- All -")
                    {
                        costCategories += costCategory.Value + ",";
                        paramCostCategories += costCategory.Text + ",";
                    }
                    else
                    {
                        costCategories = "0";
                        paramCostCategories += costCategory.Text + ",";
                    }
                }

                //Need to remove trailing "," from the fields list
                if (costCategories.IndexOf(",") != -1)
                {
                    costCategories = costCategories.ReplaceLastInstanceOf(",", "");
                    paramCostCategories = paramCostCategories.ReplaceLastInstanceOf(",", "");
                }

                DataSet ds = objModels.LoadMultiFieldsOpexProjection(models, intStructure, intFrom, intTo, intTechnicalScenario, intEconomicScenario, intCurrency, intTerm, intCostType, intTypeOPeration, intFactor, costCategories);

                DataTable dtExport = new DataTable();

                if (ds.Tables.Count != 4)
                {
                    dtExport = ds.Tables[1];
                }
                else
                {
                    dtExport = ds.Tables[2];
                }

                ArrayList datColumnExport = new ArrayList();

                String strSelectedParameters = lblModel.Text + " '" + models + "', " +
                                               lblOperationType.Text + " '" + ddlOperationType.SelectedItem.Text + "', " +
                                               lblCostCategory.Text + " '" + costCategories + "', " +
                                               lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "', " +
                                               lblFactor.Text + " '" + ddlFactor.SelectedItem.Text + "', " +
                                               lblCostType.Text + " '" + ddlCostType.SelectedItem.Text + "', " +
                                               lblScenarioTechnical.Text + " '" + ddlScenarioTechnical.SelectedItem.Text + "', " +
                                               lblScenarioEconomic.Text + " '" + ddlScenarioEconomic.SelectedItem.Text + "', " +
                                               lblTerm.Text + " '" + ddlTerm.SelectedItem.Text + "', " +
                                               lblFrom.Text + " '" + txtFrom.Text + "', " +
                                               lblTo.Text + " '" + txtTo.Text + "'";
                ExportOpexTOT = new DataReportObject(dtExport, "NewTab", "Multiple Fields Projection (Model Level)", strSelectedParameters, null, null, datColumnExport, null);

                //this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Report Generated Successfully!"), IconCls = "icon-accept", Clear2 = false });
                X.Mask.Hide();

                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                if (strMessage.Contains("Timeout"))
                {
                    //this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = strMessage.ToString(), IconCls = "icon-exclamation", Clear2 = false });
                }
                else
                {
                    //this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select all filters please"), IconCls = "icon-exclamation", Clear2 = false });
                }
            }
        }

        protected void btnRun_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.ModelCheckRecalcModuleReport();
                DisableParameterSelection();

                DataClass.clsModels objModels = new DataClass.clsModels();

                Int32 intLevel = Convert.ToInt32(ddlLevels.Value);

                // Check that at least one model has been selected
                if (this.ddlModel.SelectedItems.Count == 0)
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = "No Model selected", IconCls = "icon-exclamation", Clear2 = false });

                string models = "";
                string paramModels = "";
                foreach (Ext.Net.ListItem model in this.ddlModel.SelectedItems)
                {
                    models += model.Value + ",";
                    paramModels += model.Text + ",";
                }

                //Need to remove trailing "," from the models and paramModels lists
                models = models.ReplaceLastInstanceOf(",", "");
                paramModels = paramModels.ReplaceLastInstanceOf(",", "");

                Int32 intStructure = 1;
                Int32 intCurrency = Convert.ToInt32(ddlCurrency.Value);
                Int32 intTerm = Convert.ToInt32(ddlTerm.Value);
                Int32 intTechnicalScenario = Convert.ToInt32(ddlScenarioTechnical.Value);
                Int32 intEconomicScenario = 0;
                Int32 intCostType = Convert.ToInt32(ddlCostType.Value);
                Int32 intTypeOPeration = Convert.ToInt32(ddlOperationType.Value);
                Int32 intFactor = Convert.ToInt32(ddlFactor.SelectedItem.Text);
                String strCurrencyCode = "";

                if (this.ddlScenarioEconomic.Hidden == false)
                {
                    intEconomicScenario = Convert.ToInt32(ddlScenarioEconomic.Value);
                }
                Int32 intFrom = Convert.ToInt32(txtFrom.Value);
                Int32 intTo = Convert.ToInt32(txtTo.Value);

                clsCurrencies objCurrencies = new clsCurrencies();
                objCurrencies.IdCurrency = intCurrency;
                objCurrencies.loadObject();
                if (intFactor == 1000)
                {
                    strCurrencyCode = "(1M_" + objCurrencies.Code + ")";
                }
                else
                {
                    strCurrencyCode = "(" + objCurrencies.Code + ")";
                }

                // Check that at least one cost category has been selected, if none default to 'All'
                if (this.ddlCostCategory.SelectedItems.Count == 0)
                    ddlCostCategory.SelectedItems.Add(new Ext.Net.ListItem("0"));

                string costCategories = "";
                string paramCostCategories = "";
                foreach (Ext.Net.ListItem costCategory in this.ddlCostCategory.SelectedItems)
                {
                    if (costCategory.Value != "- All -")
                    {
                        costCategories += costCategory.Value + ",";
                        paramCostCategories += costCategory.Text + ",";
                    }
                    else
                    {
                        costCategories = "0";
                        paramCostCategories += costCategory.Text + ",";
                    }
                }

                //Need to remove trailing "," from the fields list
                if (costCategories.IndexOf(",") != -1)
                {
                    costCategories = costCategories.ReplaceLastInstanceOf(",", "");
                    paramCostCategories = paramCostCategories.ReplaceLastInstanceOf(",", "");
                }

                DataSet ds = objModels.LoadMultiFieldsOpexProjection(models, intStructure, intFrom, intTo, intTechnicalScenario, intEconomicScenario, intCurrency, intTerm, intCostType, intTypeOPeration, intFactor, costCategories);

                DataTable dtOpex = new DataTable();
                dtOpex = ds.Tables[0];
                DataTable dtChart = new DataTable();
                DataTable dtStats = new DataTable();
                DataTable dtExport = new DataTable();

                if (ds.Tables.Count != 4)
                {
                    dtChart = null;
                    dtExport = ds.Tables[1];
                    dtStats = ds.Tables[2];
                }
                else
                {
                    dtChart = ds.Tables[1];
                    dtExport = ds.Tables[2];
                    dtStats = ds.Tables[3];
                }
                if (dtStats != null)
                {
                    if (dtStats.Rows.Count > 0)
                    {
                        ReportFieldCount = Convert.ToInt32(dtStats.Rows[0]["FieldCount"].ToString());
                    }
                }
                dtStats = null;

                ArrayList datColumnOpex = new ArrayList();
                ArrayList datColumnExport = new ArrayList();

                this.FillGrid(this.storeBaseCostField, this.grdOpexProjection, dtOpex, ref datColumnOpex, false, strCurrencyCode);
                //this.FillChart(this.storeChart, this.chtChart, dtChart, strCurrencyCode);

                String strSelectedParameters = lblModel.Text + " '" + models + "', " +
                                               lblOperationType.Text + " '" + ddlOperationType.SelectedItem.Text + "', " +
                                               lblCostCategory.Text + " '" + paramCostCategories + "', " +
                                               lblCurrency.Text + " '" + ddlCurrency.SelectedItem.Text + "', " +
                                               lblFactor.Text + " '" + ddlFactor.SelectedItem.Text + "', " +
                                               lblCostType.Text + " '" + ddlCostType.SelectedItem.Text + "', " +
                                               lblScenarioTechnical.Text + " '" + ddlScenarioTechnical.SelectedItem.Text + "', " +
                                               lblScenarioEconomic.Text + " '" + ddlScenarioEconomic.SelectedItem.Text + "', " +
                                               lblTerm.Text + " '" + ddlTerm.SelectedItem.Text + "', " +
                                               lblFrom.Text + " '" + txtFrom.Text + "', " +
                                               lblTo.Text + " '" + txtTo.Text + "'";
                ExportOpexTOT = new DataReportObject(dtExport, "NewTab", "Multiple Fields Projection (Model Level)", strSelectedParameters, null, null, datColumnExport, null);
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Report Generated Successfully!"), IconCls = "icon-accept", Clear2 = false });
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                if (strMessage.Contains("Timeout"))
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = strMessage.ToString(), IconCls = "icon-exclamation", Clear2 = false });
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Select all filters please"), IconCls = "icon-exclamation", Clear2 = false });
                }
            }
        }

        protected void StoreLevel_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dt = objAggregationLevels.LoadList("IdModel = " + IdModel, "ORDER BY Name");

            DataRow drLevel = dt.NewRow();
            drLevel["IdAggregationLevel"] = 0;
            drLevel["Name"] = "- All -";
            dt.Rows.Add(drLevel);
            dt.AcceptChanges();
            dt.DefaultView.Sort = "Name";
            DataTable dtSort = dt.DefaultView.ToTable();

            this.storeLevels.DataSource = dtSort;
            this.storeLevels.DataBind();

            this.ddlLevels.Value = 0;

            this.storeLevels.DataBind();
        }

        protected void StoreCurr_ReadData(object sender, StoreReadDataEventArgs e)
        {
            string models = "";
            foreach (Ext.Net.ListItem model in this.ddlModel.SelectedItems)
            {
                models += model.Value + ",";
            }

            //Need to remove trailing "," from the models and paramModels lists
            models = models.ReplaceLastInstanceOf(",", "");

            // Check that at least one model has been selected
            if (this.ddlModel.SelectedItems.Count == 0)
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = "No Model selected", IconCls = "icon-exclamation", Clear2 = false });
            else
            {
                DataClass.clsCurrencies objCurrencies = new DataClass.clsCurrencies();
                this.storeBase.DataSource = objCurrencies.LoadList("IdModel IN (" + models + ") AND BaseCurrency = 1 AND Output=1", "ORDER BY BaseCurrency DESC,Name");
                this.storeBase.DataBind();
                this.storeBase.LoadData(this.storeBase.DataSource);
            }
        }

        protected void StoreField_ReadData(object sender, StoreReadDataEventArgs e)
        {

            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dtField = new DataTable();
            if ((ddlLevels.Value == null) || (ddlLevels.Value.ToString() == "0") || (ddlLevels.Value.ToString() == ""))
            {
                dtField = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");
            }
            else
            {
                dtField = objFields.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
            }
            DataRow drField = dtField.NewRow();
            drField["IdField"] = 0;
            drField["Name"] = "- All -";//drField["CodeName"] = "- All -"; //
            dtField.Rows.Add(drField);
            dtField.AcceptChanges();
            dtField.DefaultView.Sort = "Name";
            DataTable dtFieldSort = dtField.DefaultView.ToTable();

            this.storeField.DataSource = dtFieldSort;
            this.storeField.DataBind();

            this.ddlField.Value = 0;
        }

        protected void StoreCostType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsCostType objCostTypes = new DataClass.clsCostType();
            DataTable dt = objCostTypes.LoadList("", "ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                ddlCostType.Value = dt.Rows[0]["IdCostType"];
            }
            else
            {
                ddlCostType.SelectedItem.Index = 0;
            }
            foreach (DataRow row in dt.Rows)
            {
                if (idLanguage == 2)
                {
                    row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
                }
            }
            this.storeCostType.DataSource = dt;
            this.storeCostType.DataBind();
        }

        protected void StoreTechnicalScenarioType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsScenarios objScenarioTypes = new DataClass.clsScenarios();
            DataTable dt = objScenarioTypes.LoadList("", "ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                ddlCostType.Value = dt.Rows[0]["IdScenarioType"];
            }
            else
            {
                ddlCostType.SelectedItem.Index = 0;
            }
            foreach (DataRow row in dt.Rows)
            {
                if (idLanguage == 2)
                {
                    row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
                }
            }
            this.storeTechnicalScenarioType.DataSource = dt;
            this.storeTechnicalScenarioType.DataBind();
        }

        protected void StoreEconomicScenarioType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsScenarios objScenarioTypes = new DataClass.clsScenarios();
            DataTable dt = objScenarioTypes.LoadList("", "ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                ddlCostType.Value = dt.Rows[0]["IdScenarioType"];
            }
            else
            {
                ddlCostType.SelectedItem.Index = 0;
            }
            foreach (DataRow row in dt.Rows)
            {
                if (idLanguage == 2)
                {
                    row["name"] = GetGlobalResourceObject("CPM_Resources", row["name"].ToString());
                }
            }
            this.storeEconomicScenarioType.DataSource = dt;
            this.storeEconomicScenarioType.DataBind();
        }

        protected void StoreOperationType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsOperationType objoperationTypes = new DataClass.clsOperationType();
            DataTable dtOperation = new DataTable();

            if (((ddlLevels.Value == null) || (ddlLevels.Value.ToString() == "0") || (ddlLevels.Value.ToString() == "")) && ((ddlField.Value == null) || (ddlField.Value.ToString() == "0") || (ddlField.Value.ToString() == "")))
            {
                dtOperation = objoperationTypes.LoadList("IdModel=" + IdModel, "");//"[vListTypesOperation].IdModel=" + IdModel, "");

                DataRow drOperation = dtOperation.NewRow();
                drOperation["IdTypeOperation"] = 0;
                drOperation["Name"] = "- All -";
                dtOperation.Rows.Add(drOperation);
                dtOperation.AcceptChanges();
                dtOperation.DefaultView.Sort = "Name";

                ddlOperationType.Value = 0;
            }
            else if (Convert.ToInt32(ddlLevels.Value) > 0)
            {
                dtOperation = objoperationTypes.LoadList("IdModel = " + IdModel + " AND IdField IN (SELECT IdField FROM tblCalcAggregationLevelsField WHERE IdAggregationLevel=" + GetIdLevel(ddlLevels.SelectedItem.Text) + ")", "ORDER BY Name");
                if (dtOperation.Rows.Count > 0)
                {
                    ddlOperationType.Value = dtOperation.Rows[0]["IdTypeOperation"];
                }
            }
            else
            {
                dtOperation = objoperationTypes.LoadList("IdModel=" + IdModel + " AND IdField = " + ddlField.Value, " Order by Name");//"[vListTypesOperation].IdModel=" + IdModel + " AND tblFields.IdField = " + ddlField.Value, " Order by Name");
                if (dtOperation.Rows.Count > 0)
                {
                    ddlOperationType.Value = dtOperation.Rows[0]["IdTypeOperation"];
                }
            }

            DataTable dtOperationSort = dtOperation.DefaultView.ToTable();

            this.storeOperationType.DataSource = dtOperationSort;
            this.storeOperationType.DataBind();
        }

        protected void StoreCostCategory_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsZiffAccount objZiffAccount = new DataClass.clsZiffAccount();
            DataTable dtZiffAccount = objZiffAccount.LoadComboBoxCPM("Parent IS NULL GROUP BY Code, Name, Parent", " ORDER BY Code");
            DataRow drAccount = dtZiffAccount.NewRow();
            drAccount["Name"] = "- All -";
            drAccount["Code"] = "- All -";
            drAccount["Parent"] = 0;
            dtZiffAccount.Rows.Add(drAccount);
            dtZiffAccount.AcceptChanges();
            dtZiffAccount.DefaultView.Sort = "Code";
            this.storeCostCategory.DataSource = dtZiffAccount;
            this.storeCostCategory.DataBind();
            ddlCostCategory.Value = 0;
            ddlCostCategory.Text = "- All -";
        }

        protected void btnModel_Click(object sender, DirectEventArgs e)
        {
            Response.Redirect("/caseselection.aspx");
        }

        protected void StoreModel_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsModels objModels = new DataClass.clsModels();
            this.storeBaseModel.DataSource = objModels.LoadComboBox("", "ORDER BY Name");
            this.storeBaseModel.DataBind();
        }

        protected void ddlModel_Select(object sender, DirectEventArgs e)
        {
            int[] models = new int[ddlModel.SelectedItems.Count + 1];
            int i = 0;

            foreach (Ext.Net.ListItem model in this.ddlModel.SelectedItems)
            {
                models[i] = Convert.ToInt32(model.Value);
                i++;
            }

            // Update the models list with same BaseYear Models
            DataClass.clsModels objModels = new DataClass.clsModels();
            this.storeBaseModel.DataSource = objModels.LoadComboBoxCPM("BaseYear = (SELECT BaseYear FROM tblModels WHERE tblModels.IdModel = " + Convert.ToInt32(this.ddlModel.SelectedItem.Value) + ") AND BaseCurrency = 1 AND Output=1 AND IdCurrency = (SELECT IdCurrency FROM tblModelsCurrencies WHERE IdModel=" + Convert.ToInt32(this.ddlModel.SelectedItem.Value) + " AND BaseCurrency = 1 AND Output=1 )", "ORDER BY Name");
            this.storeBaseModel.DataBind();
            this.storeBaseModel.LoadData(this.storeBaseModel.DataSource);

            //Re-select the Models that were selected
            ddlModel.UpdateSelectedItems();

            //Update the currency list
            StoreCurr_ReadData(null, null);

            //Update From and To dates with model click
            IdModel = Convert.ToInt32(this.ddlModel.SelectedItem.Value);
            FromTo();
        }

        protected void ddlModel_BeforeDeselect(object sender, DirectEventArgs e)
        {
            if (this.ddlModel.SelectedItems.Count == 0)
            {
                DataClass.clsModels objModels = new DataClass.clsModels();
                this.storeBaseModel.DataSource = objModels.LoadComboBox("", "ORDER BY Name");
                this.storeBaseModel.DataBind();
                this.storeBaseModel.LoadData(this.storeBaseModel.DataSource);
                ddlModel.UpdateSelectedItems();
            }
        }

        #endregion

        #region Methods

        public Boolean GetProject()
        {
            Boolean Value = false;
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("Level IN (1,2) AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = true;
            }

            return Value;
        }

        protected void ClearReport()
        {
            if (PageIsLoaded)
            {
                this.storeBaseCostField.RemoveAll();
                this.pnlChartObject.ClearContent();
            }
        }

        protected void FromTo()
        {
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt;
            if (ddlModel.SelectedItems.Count > 1)
                dt = objModels.GetBaseYearAndProjection("BaseYear = (SELECT BaseYear FROM tblModels WHERE tblModels.IdModel = " + Convert.ToInt32(this.ddlModel.SelectedItem.Value) + ") AND BaseCurrency = 1 AND Output=1 AND IdCurrency = (SELECT IdCurrency FROM tblModelsCurrencies WHERE IdModel=" + Convert.ToInt32(this.ddlModel.SelectedItem.Value) + " AND BaseCurrency = 1 AND Output=1 )", "");
            else
                if (this.ddlModel.SelectedItem.Value != null)
                    dt = objModels.GetBaseYearAndProjection("tblModels.IdModel = " + this.ddlModel.SelectedItem.Value, "");
                else
                    dt = objModels.GetBaseYearAndProjection("tblModels.IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["BaseYear"] != DBNull.Value)
                {
                    this.txtFrom.Value = (Int32)dt.Rows[0]["BaseYear"];
                    this.txtTo.Value = (Int32)(dt.Rows[0]["BaseYear"]) + (Int32)(dt.Rows[0]["Projection"]);

                    this.txtFrom.MinValue = (Int32)dt.Rows[0]["BaseYear"];
                    this.txtFrom.MaxValue = (Int32)dt.Rows[0]["BaseYear"] + (Int32)dt.Rows[0]["Projection"];

                    this.txtTo.MinValue = (Int32)dt.Rows[0]["BaseYear"];
                    this.txtFrom.MaxValue = (Int32)dt.Rows[0]["BaseYear"] + (Int32)dt.Rows[0]["Projection"];

                    this.txtFrom.DataBind();
                    this.txtTo.DataBind();
                }
            }
        }

        protected Int32 GetIdOperation(String _TypeOperation)
        {
            Int32 Value = 0;
            DataClass.clsTypesOperation objTypesOperation = new DataClass.clsTypesOperation();
            DataTable dt = objTypesOperation.LoadList("Name = '" + _TypeOperation + "'", "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdTypeOperation"];
            }
            return Value;
        }

        private void FillGrid(Ext.Net.Store s, Ext.Net.GridPanel g, DataTable dt, ref ArrayList colVisible, Boolean IsBLBO, String CurrencyCode)
        {
            s.RemoveFields();

            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "ParentName" || c.ColumnName == "Name")
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Auto));
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                        s.AddField(new ModelField(c.ColumnName, ModelFieldType.Float));
                }
            }
            g.Features.Clear();

            if (!IsBLBO)
            {
                GroupingSummary gsum = new GroupingSummary();
                gsum.GroupHeaderTplString = "{name}";
                gsum.HideGroupedHeader = true;
                gsum.ShowSummaryRow = false;    //Hide the grouping Summary Totals by setting to false
                g.Features.Add(gsum);
            }
            
            GridFilters f = new GridFilters();
            f.Local = true;
            g.Features.Add(f);

            Summary smry = new Summary();
            smry.Dock = new SummaryDock();
            smry.ID = "Summary" + g.ID.ToString();
            smry.ShowSummaryRow = false;    //Hide the Grand Summary Totals by setting to false

            g.Features.Add(smry);

            foreach (DataColumn c in dt.Columns)
            {
                if (!c.ColumnName.Contains("Id"))
                {
                    if (c.ColumnName == "ParentName" || c.ColumnName == "Name")
                    {
                        StringFilter sFilter = new StringFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                    else
                    {
                        NumericFilter sFilter = new NumericFilter();
                        sFilter.AutoDataBind = true;
                        sFilter.DataIndex = c.ColumnName;
                        f.Filters.Add(sFilter);
                    }
                }
            }

            Column col = new Column();
            switch (s.ID)
            {
                case "storeBaseCostField":
                case "storeBaseLine": 
                case "storeBusinessOpportunities": 
                    col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Years") + " " + CurrencyCode;
                    break;
                default:
                    col.Text = (String)GetGlobalResourceObject("CPM_Resources", "Years");
                    break;
            }
            SummaryColumn colSum = new SummaryColumn();
            foreach (DataColumn c in dt.Columns)
            {
                if (c.ColumnName == "ParentName" || c.ColumnName == "Name")
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        //if (c.ColumnName != "ParentName")
                        //{
                            colSum = new SummaryColumn();
                            colSum.DataIndex = c.ColumnName;
                            switch (s.ID)
                            {
                                case "storeBaseCostField":
                                    colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", "Field");
                                    break;
                                case "storeVolumeTotal":
                                    colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", "Scenario").ToString().Substring(0,GetGlobalResourceObject("CPM_Resources", "Scenario").ToString().Length-1);
                                    break;
                                default:
                                    colSum.Text = (String)GetGlobalResourceObject("CPM_Resources", "Project");
                                    break;
                            }
                            colSum.Locked = true;
                            colVisible.Add(new DataClass.DataTableColumnDisplay(c.ColumnName, colSum.Text));
                            colSum.SummaryType = Ext.Net.SummaryType.Count;
                            colSum.TdCls = "task";
                            colSum.Sortable = true;
                            colSum.Groupable = true;
                            colSum.Width = 250;
                            colSum.Renderer.Fn = "formatCol";
                            colSum.MenuDisabled = false;
                            colSum.Align = Alignment.Left;
                            g.ColumnModel.Columns.Add(colSum);
                        //}
                    }
                }
                else
                {
                    if (!c.ColumnName.Contains("Id"))
                    {
                        colSum = new SummaryColumn();
                        colSum.DataIndex = c.ColumnName;
                        colSum.Text = c.ColumnName;
                        colVisible.Add(new DataClass.DataTableColumnDisplay(c.ColumnName, colSum.Text));
                        colSum.SummaryType = Ext.Net.SummaryType.Sum;
                        Renderer sumRen = new Renderer();
                        switch (s.ID)
                        {
                            case "storeBaseCostField":
                            case "storeBaseLine":
                            case "storeBusinessOpportunities":
                                sumRen.Fn = "formatColData";

                                break;
                            default:
                                sumRen.Fn = "Ext.util.Format.numberRenderer('0,000')";
                                break;
                        }
                        colSum.SummaryRenderer = sumRen;
                        colSum.Renderer = sumRen;
                        colSum.Align = Alignment.Right;
                        colSum.MinWidth = 80;
                        col.Columns.Add(colSum);
                    }
                }
            }

            g.ColumnModel.Columns.Add(col);

            s.DataSource = dt;
            //s.GroupField = "ParentName";
            
            if (X.IsAjaxRequest)
            {
                g.Reconfigure();
            }
            s.DataBind();
        }

        private void FillChart(Ext.Net.Store s, Ext.Net.Chart c, DataTable dt, String CurrencyCode)
        {
            string[] str = new string[dt.Columns.Count - 1];
            int i = 0;
            foreach (DataColumn col in dt.Columns)
            {
                if (col.ColumnName != "IdModel" && col.ColumnName != "Year")
                {
                    str[i] = col.ColumnName;
                    i = i + 1;
                }
            }
            string[] strcat = new string[] { "Year" };

            s.RemoveFields();

            foreach (DataColumn col in dt.Columns)
            {
                s.AddField(new ModelField(col.ColumnName, ModelFieldType.Auto));
            }
            s.DataSource = dt;
            s.DataBind();

            AxisLabel AxisL = new AxisLabel();
            AxisL.Renderer.Handler = "return Ext.util.Format.number(value, '0,000.00');";

            c.Axes.Add(new NumericAxis()
            {
                Title = (String)GetGlobalResourceObject("CPM_Resources", "Costs") + " " + CurrencyCode,
                Fields = str,
                MajorTickSteps = 9,
                Grid = true,
                Position = Position.Left,
                GridConfig = new AxisGrid()
                {
                    Odd = new SpriteAttributes()
                    {
                        Opacity = 1,
                        Fill = "#EFEBEF",
                        Stroke = "#EFEBEF",
                        StrokeWidth = 0.5
                    }
                },
                Label = AxisL
            });

            c.Axes.Add(new CategoryAxis()
            {
                Title = (String)GetGlobalResourceObject("CPM_Resources", "Year"),
                Fields = strcat,
                Position = Position.Bottom,
                Grid = true
            });

            foreach (string str1 in str)
            {
                ChartTip chrtTip = new ChartTip();
                chrtTip.TrackMouse = true;
                chrtTip.Width = 140;
                chrtTip.StyleSpec = "background:#fff; text-align:center;";
                chrtTip.Height = 20;
                chrtTip.Renderer.Handler = "this.setTitle(String(Ext.util.Format.number(item.value[1], '0,000.00')));";

                c.Series.Add(new LineSeries()
                {
                    Titles = new string[] { str1 },
                    XField = strcat,
                    YField = new string[] { str1 },
                    Axis = Position.Left,
                    //Fill = true,
                    Tips = chrtTip,
                    MarkerConfig = new SpriteAttributes()
                    {
                        Type = Ext.Net.SpriteType.Circle,
                        Size = 4,
                        Radius = 4,
                        StrokeWidth = 0
                    },
                    HighlightConfig = new SpriteAttributes()
                    {
                        Size = 5,
                        Radius = 5
                    },
                });
            }
            c.Height = ReportFieldCount * 75 < 320 ? 320 : ReportFieldCount * 75;
            c.Animate = true;
            c.Shadow = true;
            c.Frame = true;
            c.StyleSpec = "background:#fff;";
            pnlChartObject.Render();
        }

        protected void txtTo_Select(object sender, DirectEventArgs e)
        {
            this.txtTo.MinValue = Convert.ToInt32(txtFrom.Value);
            this.txtTo.DataBind();
        }

        private Int32 GetIdField(String _Name)
        {
            Int32 intID = 0;

            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dtField = objFields.LoadList("IdModel = " + IdModel + " AND Name = '" + _Name + "'", "ORDER BY Name");

            if (dtField.Rows.Count > 0)
            {
                intID = (Int32)dtField.Rows[0]["IdField"];
            }

            return intID;
        }

        private Int32 GetIdLevel(String _Name)
        {
            Int32 intID = 0;

            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dtLevel = objAggregationLevels.LoadList("IdModel = " + IdModel + " AND Name = '" + _Name + "'", "ORDER BY Name");

            if (dtLevel.Rows.Count > 0)
            {
                intID = (Int32)dtLevel.Rows[0]["IdAggregationLevel"];
            }

            return intID;
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExportOpexTOT = new DataReportObject();
            StoredExportOpexTOT = ExportOpexTOT;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExportOpexTOT);
            objWriter.BuildExportMultiFieldsReportCPM(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2007);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=MultipleFieldsProjection(CPM_Level).xlsx");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        private void DisableParameterSelection()
        {
            this.pnlExportToolbar.Hidden = false;
            this.pnlReportToolbar.Hidden = true;
            this.ddlModel.ReadOnly = true;
            this.ddlLevels.ReadOnly = true;
            this.ddlField.ReadOnly = true;
            this.ddlStructure.ReadOnly = true;
            this.txtFrom.ReadOnly = true;
            this.txtTo.ReadOnly = true;
            this.ddlScenarioTechnical.ReadOnly = true;
            this.ddlScenarioEconomic.ReadOnly = true;
            this.ddlCurrency.ReadOnly = true;
            this.ddlTerm.ReadOnly = true;
            this.pnlMainChartPanel.Hidden = true;// false;
            this.ddlCostType.ReadOnly=true;
            this.ddlOperationType.ReadOnly=true;
            this.ddlFactor.ReadOnly = true;
            this.ddlCostCategory.ReadOnly = true;
        }

        public void ModelCheckRecalcModuleReport()
        {
            clsModels objModelRecalc = new clsModels();
            objModelRecalc.IdModel = IdModel;
            objModelRecalc.CheckRecalcModuleParameter(objApplication.MaxRelationLevels);
            objModelRecalc.CheckRecalcModuleAllocation();
            objModelRecalc.CheckRecalcModuleTechnical();
            objModelRecalc.CheckRecalcModuleEconomic();
            objModelRecalc = null;
        }

        protected void LoadLanguage()
        {
            DataClass.clsLabelsLanguages objLabelsLanguages = new DataClass.clsLabelsLanguages();
            DataTable dt = objLabelsLanguages.LoadList("LanguageName = '" + Language + "' AND FormName = 'RolesPermissions'", "ORDER BY IdLabel");
            foreach (DataRow row in dt.Rows)
            {
                switch ((String)row["LabelName"])
                {
                    case "Add New Role":
                        modMain.strSettingsAddNewRole = (String)row["LabelText"];
                        break;
                    case "Case Selection":
                        modMain.strSettingsCaseSelection = (String)row["LabelText"];
                        break;
                    case "Roles":
                        modMain.strSettingsRoles = (String)row["LabelText"];
                        break;
                    case "Role Type":
                        modMain.strSettingsRoleType = (String)row["LabelText"];
                        break;
                    case "Selected Case":
                        modMain.strSettingsSelectedCase = (String)row["LabelText"];
                        break;
                    case "Selected Role Permissions":
                        modMain.strSettingsSelectedRolePermissions = (String)row["LabelText"];
                        break;
                    case "Grant All":
                        modMain.strSettingsGrantAll = (String)row["LabelText"];
                        break;
                    case "Deny All":
                        modMain.strSettingsDenyAll = (String)row["LabelText"];
                        break;
                    case "Clear All":
                        modMain.strSettingsClearAll = (String)row["LabelText"];
                        break;
                    case "Grant Access":
                        modMain.strSettingsGrantAccess = (String)row["LabelText"];
                        break;
                    case "Deny Access":
                        modMain.strSettingsDenyAccess = (String)row["LabelText"];
                        break;
                    case "Role":
                        modMain.strSettingsRole = (String)row["LabelText"];
                        break;
                }
            }

            dt = objLabelsLanguages.LoadList("LanguageName = '" + Language + "' AND FormName = 'ReportsModule'", "ORDER BY IdLabel");
            foreach (DataRow row in dt.Rows)
            {
                //idLanguage = (Int32)row["IdLanguage"];
                switch ((String)row["LabelName"])
                {
                    case "Reports Module":
                        modMain.strReportsModuleFormTitle = (String)row["LabelText"];
                        break;
                    case "Key Performance Indicators Estimates":
                        modMain.strReportsModuleKPIEstimatesLong = (String)row["LabelText"];
                        break;
                    case "Driver:":
                        modMain.strReportsModuleDriver = (String)row["LabelText"];
                        break;
                    case "Technical Driver #":
                        modMain.strReportsModuleTechnicalDriverNum = (String)row["LabelText"];
                        break;
                    case "Key Performance Cost Indicators Estimates":
                        modMain.strReportsModuleSubFormTitle3 = (String)row["LabelText"];
                        break;
                    case "Aggregation Level:":
                        modMain.strReportsModuleAggregationLevel = (String)row["LabelText"];
                        break;
                    case "Cost Structure:":
                        modMain.strReportsModuleCostStructure = (String)row["LabelText"];
                        break;
                    case "From:":
                        modMain.strReportsModuleFrom = (String)row["LabelText"];
                        break;
                    case "To:":
                        modMain.strReportsModuleTo = (String)row["LabelText"];
                        break;
                    case "Technical Scenario:":
                        modMain.strReportsModuleTechnicalScenario = (String)row["LabelText"];
                        break;
                    case "Economic Scenario:":
                        modMain.strReportsModuleEconomicScenario = (String)row["LabelText"];
                        break;
                    case "Currency:":
                        modMain.strReportsModuleCurrency = (String)row["LabelText"];
                        break;
                    case "Term:":
                        modMain.strReportsModuleTerm = (String)row["LabelText"];
                        break;
                    case "Run Report":
                        modMain.strReportsModuleRunReport = (String)row["LabelText"];
                        break;
                    case "KPI Target":
                        modMain.strReportsModuleKPITarget = (String)row["LabelText"];
                        break;
                    case "Years":
                        modMain.strReportsModuleYears = (String)row["LabelText"];
                        break;
                    case "Reset":
                        modMain.strReportsModuleReset = (String)row["LabelText"];
                        break;
                    case "Export Report Data To Excel":
                        modMain.strReportsModuleExportReportDataToExcel = (String)row["LabelText"];
                        break;
                    case "Technical Drivers Forecast Indicator":
                        modMain.strReportsModuleResultsTitle = (String)row["LabelText"];
                        break;
                    case "Total Projection":
                        modMain.strReportsModuleOpexResultsTitle1 = (String)row["LabelText"];
                        break;
                    case "Projection By Project":
                        modMain.strReportsModuleOpexResultsTitle2 = (String)row["LabelText"];
                        break;
                    case "Baseline":
                        modMain.strReportsModuleOpexResultsSubTitle1 = (String)row["LabelText"];
                        break;
                    case "Business Opportunities":
                        modMain.strReportsModuleOpexResultsSubTitle2 = (String)row["LabelText"];
                        break;
                    case "1. Cost Projection Reports":
                        modMain.strReportsModuleFormSubTitle1 = (String)row["LabelText"];
                        break;
                    case "2. Projection Base Rates Reports":
                        modMain.strReportsModuleFormSubTitle2 = (String)row["LabelText"];
                        break;
                    case "3. Historical Base Cost Reports":
                        modMain.strReportsModuleFormSubTitle3 = (String)row["LabelText"];
                        break;
                    case "Type of Operation":
                        modMain.strReportsModuleFormOperationType = (String)row["LabelText"];
                        break;
                    case "Cost Type":
                        modMain.strReportsModuleFormTypeCost = (String)row["Labeltext"];
                        break;
                }
            }

            dt = objLabelsLanguages.LoadList("LanguageName = '" + Language + "' AND FormName = 'CaseDetails'", "ORDER BY IdLabel");

            foreach (DataRow row in dt.Rows)
            {
                switch ((String)row["LabelName"])
                {
                    case "Case Details":
                        modMain.strCaseDetailsTitleForm = (String)row["LabelText"];
                        break;
                    case "Add New Case":
                        modMain.strCaseDetailsNewCase = (String)row["LabelText"];
                        break;
                    case "Base Case":
                        modMain.strCaseDetailsBaseCase = (String)row["LabelText"];
                        break;
                    case "Created By":
                        modMain.strCaseDetailsCreatedBy = (String)row["LabelText"];
                        break;
                    case "Created":
                        modMain.strCaseDetailsCreated = (String)row["LabelText"];
                        break;
                    case "Base Year":
                        modMain.strCaseDetailsBaseYear = (String)row["LabelText"];
                        break;
                    case "Projection":
                        modMain.strCaseDetailsProjection = (String)row["LabelText"];
                        break;
                    case "Status":
                        modMain.strCaseDetailsStatus = (String)row["LabelText"];
                        break;
                    case "Level":
                        modMain.strCaseDetailsLevel = (String)row["LabelText"];
                        break;
                    case "Note":
                        modMain.strCaseDetailsNote = (String)row["LabelText"];
                        break;
                    case "Form Status":
                        modMain.strCaseDetailsFormStatus = (String)row["LabelText"];
                        break;
                    case "Date":
                        modMain.strCaseDetailsDate = (String)row["LabelText"];
                        break;
                    case "Parameters":
                        modMain.strMasterParametersModule = (String)row["LabelText"];
                        break;
                    case "Allocation":
                        modMain.strMasterAllocationModule = (String)row["LabelText"];
                        break;
                    case "Close":
                        modMain.strCommonClose = (String)row["LabelText"];
                        break;
                    case "Delete":
                        modMain.strCommonDelete = (String)row["LabelText"];
                        break;
                    case "Edit":
                        modMain.strCommonEdit = (String)row["LabelText"];
                        break;
                    case "Functions":
                        modMain.strCommonFunctions = (String)row["LabelText"];
                        break;
                    case "Name":
                        modMain.strCommonName = (String)row["LabelText"];
                        break;
                    case "Open":
                        modMain.strCommonOpen = (String)row["LabelText"];
                        break;
                    case "Save":
                        modMain.strCommonSave = (String)row["LabelText"];
                        break;
                    case "Select":
                        modMain.strCommonSelect = (String)row["LabelText"];
                        break;
                    case "Save Changes":
                        modMain.strCommonSaveChanges = (String)row["LabelText"];
                        break;
                    case "Delete All":
                        modMain.strCommonDeleteAll = (String)row["LabelText"];
                        break;
                    case "Confirm":
                        modMain.strCommonConfirm = (String)row["LabelText"];
                        break;
                    case "Yes":
                        modMain.strCommonYes = (String)row["LabelText"];
                        break;
                    case "No":
                        modMain.strCommonNo = (String)row["LabelText"];
                        break;
                    case "Form is valid":
                        modMain.strCommonFormValid = (String)row["LabelText"];
                        break;
                    case "Form is invalid":
                        modMain.strCommonFormInvalid = (String)row["LabelText"];
                        break;
                    case "Import":
                        modMain.strCommonImport = (String)row["LabelText"];
                        break;
                    case "Code":
                        modMain.strCommonCode = (String)row["LabelText"];
                        break;
                    case "Help":
                        modMain.strCommonHelp = (String)row["LabelText"];
                        break;
                    case "Technical Driver:":
                        modMain.strTechnicalModuleTechnicalDriver = (String)row["LabelText"];
                        break;
                    case "Field:":
                        modMain.strTechnicalModuleField = (String)row["LabelText"];
                        break;
                    case "Project:":
                        modMain.strTechnicalModuleProject = (String)row["LabelText"];
                        break;
                    case "Base":
                        modMain.strTechnicalModuleBase = (String)row["LabelText"];
                        break;
                    case "Pessimistic":
                        modMain.strTechnicalModulePessimistic = (String)row["LabelText"];
                        break;
                    case "Optimistic":
                        modMain.strTechnicalModuleOptimistic = (String)row["LabelText"];
                        break;
                    case "Year":
                        modMain.strTechnicalModuleYear = (String)row["LabelText"];
                        break;
                }
            }
            dt = objLabelsLanguages.LoadList("LanguageName = '" + Language + "' AND FormName = 'MasterPage'", "ORDER BY IdLabel");
            foreach (DataRow row in dt.Rows)
            {
                //idLanguage = (Int32)row["IdLanguage"];
                switch ((String)row["LabelName"])
                {
                    case "Title Form":
                        modMain.strMasterTitleForm = (String)row["LabelText"];
                        break;
                    case "Application Home":
                        modMain.strMasterHome = (String)row["LabelText"];
                        break;
                    case "Admin":
                        modMain.strMasterAdmin = (String)row["LabelText"];
                        break;
                    case "User":
                        modMain.strMasterUser = (String)row["LabelText"];
                        break;
                    case "Setting":
                        modMain.strMasterSetting = (String)row["LabelText"];
                        break;
                    case "Logon":
                        modMain.strMasterLogon = (String)row["LabelText"];
                        break;
                    case "LastLogin":
                        modMain.strMasterLastLogin = (String)row["LabelText"];
                        break;
                    case "Windows Title User":
                        modMain.strMasterWindowsTitleUser = (String)row["LabelText"];
                        break;
                    case "Form UserName":
                        modMain.strMasterFormUserName = (String)row["LabelText"];
                        break;
                    case "Form Name":
                        modMain.strMasterFormName = (String)row["LabelText"];
                        break;
                    case "Form Password":
                        modMain.strMasterFormPassword = (String)row["LabelText"];
                        break;
                    case "Form Email":
                        modMain.strMasterFormEmail = (String)row["LabelText"];
                        break;
                    case "Form Accept Button":
                        modMain.strMasterFormAcceptButton = (String)row["LabelText"];
                        break;
                    case "Form Cancel Button":
                        modMain.strMasterFormCancelButton = (String)row["LabelText"];
                        break;
                    case "Valid":
                        modMain.strMasterValid = (String)row["LabelText"];
                        break;
                    case "Parameters":
                        modMain.strMasterParametersModule = (String)row["LabelText"];
                        break;
                    case "Allocation":
                        modMain.strMasterAllocationModule = (String)row["LabelText"];
                        break;
                    case "Technical":
                        modMain.strMasterTechnicalModule = (String)row["LabelText"];
                        break;
                    case "Economic":
                        modMain.strMasterEconomicModule = (String)row["LabelText"];
                        break;
                    case "Reports":
                        modMain.strMasterReportsModule = (String)row["LabelText"];
                        break;
                    case "Case Selection":
                        modMain.strMasterCaseSelection = (String)row["LabelText"];
                        break;
                    case "Modules":
                        modMain.strMasterModules = (String)row["LabelText"];
                        break;
                    case "Selected Case":
                        modMain.strMasterSelectedCase = (String)row["LabelText"];
                        break;
                    case "Aggregation Levels":
                        modMain.strMasterAggregationLevels = (String)row["LabelText"];
                        break;
                    case "Types of Operations":
                        modMain.strMasterTypesofOperations = (String)row["LabelText"];
                        break;
                    case "List of Fields":
                        modMain.strMasterListofFields = (String)row["LabelText"];
                        break;
                    case "Field Projects":
                        modMain.strMasterFieldProjects = (String)row["LabelText"];
                        break;
                    case "Currency":
                        modMain.strMasterCurrency = (String)row["LabelText"];
                        break;
                    case "Activities":
                        modMain.strMasterActivities = (String)row["LabelText"];
                        break;
                    case "Resources":
                        modMain.strMasterResources = (String)row["LabelText"];
                        break;
                    case "Cost Objects":
                        modMain.strMasterCostObjects = (String)row["LabelText"];
                        break;
                    case "Chart of Accounts":
                        modMain.strMasterChartAccounts = (String)row["LabelText"];
                        break;
                    case "Ziff/Third-party Cost Categories":
                        modMain.strMasterZiff_Third_partyCostCategories = (String)row["LabelText"];
                        break;
                    case "Internal Cost References":
                        modMain.strMasterCompanyCostReferences = (String)row["LabelText"];
                        break;
                    case "External Cost References":
                        modMain.strMasterZiff_Third_partyCostReferences = (String)row["LabelText"];
                        break;
                    case "Cost Structure Assumptions":
                        modMain.strMasterCostStructureAssumptions = (String)row["LabelText"];
                        break;
                    case "Base Cost Data":
                        modMain.strMasterBaseCostData = (String)row["LabelText"];
                        break;
                    case "Direct":
                        modMain.strMasterDirect = (String)row["LabelText"];
                        break;
                    case "Shared":
                        modMain.strMasterShared = (String)row["LabelText"];
                        break;
                    case "Hierarchy":
                        modMain.strMasterHierarchy = (String)row["LabelText"];
                        break;
                    case "List of Allocation Drivers":
                        modMain.strMasterListofAllocationDrivers = (String)row["LabelText"];
                        break;
                    case "Allocation Drivers Criteria":
                        modMain.strMasterAllocationDriversCriteria = (String)row["LabelText"];
                        break;
                    case "Allocation Drivers Data":
                        modMain.strMasterAllocationDriversData = (String)row["LabelText"];
                        break;
                    case "Base Cost by Field (Company Structure)":
                        modMain.strMasterBaseCostbyFieldCompany = (String)row["LabelText"];
                        break;
                    case "Mapping (Company Cost Structure vs. Ziff/Third-party Cost Categories)":
                        modMain.strMasterMapping = (String)row["LabelText"];
                        break;
                    case "Base Cost by Field (Ziff/Third-party Cost Categories)":
                        modMain.strMasterBaseCostbyFieldZiff = (String)row["LabelText"];
                        break;
                    case "List of Peer Criteria":
                        modMain.strMasterListofPeerCriteria = (String)row["LabelText"];
                        break;
                    case "Peer Criteria Data":
                        modMain.strMasterPeerCriteriaData = (String)row["LabelText"];
                        break;
                    case "Ziff/Third-party Base Cost":
                        modMain.strMasterZiff_Third_partyBaseCost = (String)row["LabelText"];
                        break;
                    case "List Technical Drivers":
                        modMain.strMasterListTechnicalDrivers = (String)row["LabelText"];
                        break;
                    case "Technical Drivers Data":
                        modMain.strMasterTechnicalDriversData = (String)row["LabelText"];
                        break;
                    case "Technical Assumptions":
                        modMain.strMasterTechnicalAssumptions = (String)row["LabelText"];
                        break;
                    case "Base Cost Rates":
                        modMain.strMasterBaseCostRates = (String)row["LabelText"];
                        break;
                    case "Technical Files Repository":
                        modMain.strMasterZiffTechnicalAnalysis = (String)row["LabelText"];
                        break;
                    case "List of Economic Drivers":
                        modMain.strMasterListofEconomicDrivers = (String)row["LabelText"];
                        break;
                    case "Economic Drivers Data":
                        modMain.strMasterEconomicDriversData = (String)row["LabelText"];
                        break;
                    case "Economic Assumptions":
                        modMain.strMasterEconomicAssumptions = (String)row["LabelText"];
                        break;
                    case "Base Cost Rates Forecast":
                        modMain.strMasterBaseCostRatesForecast = (String)row["LabelText"];
                        break;
                    case "Economic File Repository":
                        modMain.strMasterZiffEconomicAnalysis = (String)row["LabelText"];
                        break;
                    case "Projection by Cost Category":
                        modMain.strMasterOperatingCostProjection = (String)row["LabelText"];
                        break;
                    case "Technical Drivers Forecast":
                        modMain.strMasterTechnicalDriversForecast = (String)row["LabelText"];
                        break;
                    case "KPI Estimates":
                        modMain.strMasterKPIEstimates = (String)row["LabelText"];
                        break;
                    case "Utilization Capacity":
                        modMain.strTechnicalModuleUtilizationCapacity = (String)row["LabelText"];
                        break;
                    case "Base Rates By Drivers":
                        modMain.strTechnicalModuleBaseRatesByDrivers = (String)row["Labeltext"];
                        break;
                    case "Projection by Field Project":
                        modMain.strReportsModuleOperatingCostProjectionDetailed = (String)row["Labeltext"];
                        break;
                    case "Base Rates By Cost Category":
                        modMain.strTechnicalModuleBaseRatesByCostCategory = (String)row["LabelText"];
                        break;
                    case "Internal Base Cost References":
                        modMain.strReportsModuleCompanyBaseCostReferences = (String)row["LabelText"];
                        break;
                    case "External Base Cost References":
                        modMain.strReportsModuleZiffBaseCostReferences = (String)row["LabelText"];
                        break;
                    case "Detailed Projection":
                        modMain.strReportsModuleDetailedProjection = (String)row["LabelText"];
                        break;
                }
            } 
            this.LoadLabel();
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = (String)GetGlobalResourceObject("CPM_Resources", "Multiple Models Projection");
            this.btnModels.Text = "<B>" + modMain.strSettingsCaseSelection + "</B>";
            this.btnModels.ToolTip = modMain.strSettingsCaseSelection;
            this.btnReset.Text = modMain.strReportsModuleReset;
            this.btnSaveExcel.Text = modMain.strReportsModuleExportReportDataToExcel;
            this.lblLevels.Text = modMain.strReportsModuleAggregationLevel;
            this.lblModel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Model") + ":";
            this.lblStructure.Text = modMain.strReportsModuleCostStructure;
            this.lblFrom.Text = modMain.strReportsModuleFrom;
            this.lblTo.Text = modMain.strReportsModuleTo;
            this.lblScenarioTechnical.Text = modMain.strReportsModuleTechnicalScenario;
            this.lblScenarioEconomic.Text = modMain.strReportsModuleEconomicScenario;
            this.lblCurrency.Text = modMain.strReportsModuleCurrency;
            this.lblTerm.Text = modMain.strReportsModuleTerm;
            this.btnRun.Text = (String)GetGlobalResourceObject("CPM_Resources", "Run Report"); 
            this.ReportTitle01.Title = (String)GetGlobalResourceObject("CPM_Resources", "TotalProjection");
            this.lblOperationType.Text = modMain.strReportsModuleFormOperationType + ":";
            this.lblCostType.Text = modMain.strReportsModuleFormTypeCost + ":";
            this.btnDirectExcel.Text = modMain.strReportsModuleExportReportDataToExcel;
            this.lblCostCategory.Text = (String)GetGlobalResourceObject("CPM_Resources", "Cost Category") + ":";
        }

        #endregion

    }
}
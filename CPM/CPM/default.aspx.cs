﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;

namespace CPM
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["LastLogin"] == null || Session["IdUser"] == null) && Session["IdModel"] == null)
            {
                Response.Redirect("/account/login.aspx");
            }
            else
            {
                Response.Redirect("/caseselection.aspx");
            }
        }
    }
}
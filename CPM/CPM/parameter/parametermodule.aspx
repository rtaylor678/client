﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="parametermodule.aspx.cs" Inherits="CPM._parametermodule" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_iconxl.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Border="false" Layout="FormLayout" OverflowY="Scroll">
        <Items>
            <ext:Panel ID="pnlParameters1" runat="server" Title="Parameters Module" IconCls="icon-parameters_16" BodyPadding="10" Border="false" Layout="Column">
                <Items>
<%--
                    <ext:FieldSet ID="FieldSet1" runat="server" Title="1. General Parameters" Flex="1" Cls="buttongroup-bordertop" Layout="AnchorLayout" ColumnWidth="1">
                        <Defaults>
                            <ext:Parameter Name="HideEmptyLabel" Value="false" Mode="Raw" />
                        </Defaults>
                        <Items>
--%>
                            <ext:Button ID="btnAggregationLevels" runat="server" Disabled="true" Cls="icon-aggregation_48" Text="Aggregation Levels" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnAggregationLevel_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnOperations" runat="server" Disabled="true" Cls="icon-operations_48" Text="Types of Operations" IconAlign="Top" Scale="Large">
                                 <DirectEvents>
                                    <Click OnEvent="btnTypeOperation_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnFields" runat="server" Disabled="true" Cls="icon-fields_48" Text="List of Fields" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnField_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnProjects" runat="server" Disabled="true" Cls="icon-projects_48" Text="List of Cases" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnProjects_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnCurrency" runat="server" Disabled="true" Cls="icon-currency_48" Text="Currency" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnCurrencies_Click" />
                                </DirectEvents>
                            </ext:Button>
<%-- 
                        </Items>
                    </ext:FieldSet>
                    <ext:FieldSet ID="FieldSet2" runat="server" Title="2. Ziff/Third-party Cost Categories" Flex="1" Cls="buttongroup-bordertop" Layout="AnchorLayout" ColumnWidth="1">
                        <Defaults>
                            <ext:Parameter Name="HideEmptyLabel" Value="false" Mode="Raw" />
                        </Defaults>
                        <Items>
--%>  
                            <ext:Button ID="btnZiffStructure" runat="server" Disabled="true" Cls="icon-ziffstructure_48" Text="Cost Projection Categories" IconAlign="Top" Scale="Large">
                                <DirectEvents>
                                    <Click OnEvent="btnZiffStructure_Click" />
                                </DirectEvents>
                            </ext:Button>
<%--
                        </Items>
                    </ext:FieldSet>
--%>
                </Items>
            </ext:Panel>   
        </Items>
    </ext:Viewport>
</body>
</html>
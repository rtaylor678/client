﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _aggregationlevels : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdLevel { get { return (Int32)Session["IdLevel"]; } set { Session["IdLevel"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private DataReportObject ExportAggregationLevels { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                IdLevel = 0;
                this.LoadButtons();
                this.LoadLabel();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winLevelsEdit.Show();
        }

        protected void btnDeleteAll_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, MessageConfirmDelete(null), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDeleteAllYES()",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                Int32 intValue = Convert.ToInt32(ddlParents.Value);
                this.Save(Convert.ToInt32(IdLevel));
                this.storeAggregationLevels.Reload();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Parent is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            this.winLevelsEdit.Hide();
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            this.winImport.Hide(); ;
        }

        protected void btnFind_Click(object sender, DirectEventArgs e)
        {
            this.storeAggregationLevels.Reload();
        }

        protected void btnClear_Click(object sender, DirectEventArgs e)
        {
            this.txtFind.Text = null;
            this.storeAggregationLevels.Reload();
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.AggregationLevels);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    // Sort to handle placing parents and subs in proper order
                    System.Data.DataView dvImportTable = objReader.ImportTable.DefaultView;
                    dvImportTable.Sort = "Level";
                    DataTable dtSortedImportTable = dvImportTable.ToTable();
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    foreach (DataRow row in dtSortedImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Aggregation Code"].ToString(), row["Aggregation Name"].ToString(), row["Parent Aggregation Code"].ToString());
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.storeAggregationLevels.Reload();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void StoreAggregationLevels_ReadData(Object sender, Ext.Net.NodeLoadEventArgs e)
        {
            this.storeAggregationLevels.Root.Clear();
            DataClass.clsAggregationLevels objAggregationLevels1 = new DataClass.clsAggregationLevels();
            DataTable dtDet1 = objAggregationLevels1.LoadFilterModelsAggregationLevel(IdModel, 0, txtFind.Text);

            Int32 intDet1 = 0;
            foreach (DataRow rowDet1 in dtDet1.Rows)
            {
                intDet1 = Convert.ToInt32(rowDet1["IdAggregationLevel"]);

                DataTable dtDet2 = objAggregationLevels1.LoadFilterModelsAggregationLevel(IdModel, intDet1, txtFind.Text);

                Ext.Net.Node nodeDet1 = new Ext.Net.Node();

                if (dtDet2.Rows.Count > 0)
                {
                    nodeDet1 = new Ext.Net.Node()
                    {
                        Text = (String)rowDet1["Name"],
                        NodeID = Convert.ToString(rowDet1["IdAggregationLevel"]),
                        Icon = Icon.BookGo,
                    };
                    e.Nodes.Add(nodeDet1);
                }
                else
                {
                    nodeDet1 = new Ext.Net.Node()
                    {
                        Text = (String)rowDet1["Name"],
                        NodeID = Convert.ToString(rowDet1["IdAggregationLevel"]),
                        Icon = Icon.BookGo,
                        Leaf = true
                    };
                    e.Nodes.Add(nodeDet1);
                }

                Int32 intDet2 = 0;
                foreach (DataRow rowDet2 in dtDet2.Rows)
                {
                    intDet2 = Convert.ToInt32(rowDet2["IdAggregationLevel"]);

                    DataTable dtDet3 = objAggregationLevels1.LoadFilterModelsAggregationLevel(IdModel, intDet2, txtFind.Text);

                    Ext.Net.Node nodeDet2 = new Ext.Net.Node();

                    if (dtDet3.Rows.Count > 0)
                    {
                        nodeDet2 = new Ext.Net.Node()
                        {
                            Text = (String)rowDet2["Name"],
                            NodeID = Convert.ToString(rowDet2["IdAggregationLevel"]),
                            Icon = Icon.CogStart
                        };
                        nodeDet1.Children.Add(nodeDet2);
                    }
                    else
                    {
                        nodeDet2 = new Ext.Net.Node()
                        {
                            Text = (String)rowDet2["Name"],
                            NodeID = Convert.ToString(rowDet2["IdAggregationLevel"]),
                            Icon = Icon.CogStart,
                            Leaf = true
                        };
                        nodeDet1.Children.Add(nodeDet2);
                    }

                    Int32 intDet3 = 0;
                    foreach (DataRow rowDet3 in dtDet3.Rows)
                    {
                        intDet3 = Convert.ToInt32(rowDet3["IdAggregationLevel"]);

                        DataTable dtDet4 = objAggregationLevels1.LoadFilterModelsAggregationLevel(IdModel, intDet3, txtFind.Text);

                        Ext.Net.Node nodeDet3 = new Ext.Net.Node();

                        if (dtDet4.Rows.Count > 0)
                        {
                            nodeDet3 = new Ext.Net.Node()
                            {
                                Text = (String)rowDet3["Name"],
                                NodeID = Convert.ToString(rowDet3["IdAggregationLevel"]),
                                Icon = Icon.Tick
                            };
                            nodeDet2.Children.Add(nodeDet3);
                        }
                        else
                        {
                            nodeDet3 = new Ext.Net.Node()
                            {
                                Text = (String)rowDet3["Name"],
                                NodeID = Convert.ToString(rowDet3["IdAggregationLevel"]),
                                Icon = Icon.Tick,
                                Leaf = true
                            };
                            nodeDet2.Children.Add(nodeDet3);
                        }

                        /////////////////////////////////////////////////////
                        Int32 intDet4 = 0;
                        foreach (DataRow rowDet4 in dtDet4.Rows)
                        {
                            intDet4 = Convert.ToInt32(rowDet4["IdAggregationLevel"]);

                            DataTable dtDet5 = objAggregationLevels1.LoadFilterModelsAggregationLevel(IdModel, intDet4, txtFind.Text);

                            Ext.Net.Node nodeDet4 = new Ext.Net.Node();

                            if (dtDet5.Rows.Count > 0)
                            {
                                nodeDet4 = new Ext.Net.Node()
                                {
                                    Text = (String)rowDet4["Name"],
                                    NodeID = Convert.ToString(rowDet4["IdAggregationLevel"]),
                                    Icon = Icon.Tick
                                };
                                nodeDet3.Children.Add(nodeDet4);

                            }
                            else
                            {
                                nodeDet4 = new Ext.Net.Node()
                                {
                                    Text = (String)rowDet4["Name"],
                                    NodeID = Convert.ToString(rowDet4["IdAggregationLevel"]),
                                    Icon = Icon.Tick,
                                    Leaf = true
                                };
                                nodeDet3.Children.Add(nodeDet4);
                            }
                            /////////////////////////////////////////////////////
                            Int32 intDet5 = 0;
                            foreach (DataRow rowDet5 in dtDet5.Rows)
                            {
                                intDet5 = Convert.ToInt32(rowDet5["IdAggregationLevel"]);

                                DataTable dtDet6 = objAggregationLevels1.LoadFilterModelsAggregationLevel(IdModel, intDet5, txtFind.Text);

                                Ext.Net.Node nodeDet5 = new Ext.Net.Node();

                                if (dtDet6.Rows.Count > 0)
                                {
                                    nodeDet5 = new Ext.Net.Node()
                                    {
                                        Text = (String)rowDet5["Name"],
                                        NodeID = Convert.ToString(rowDet5["IdAggregationLevel"]),
                                        Icon = Icon.Tick
                                    };
                                    nodeDet4.Children.Add(nodeDet5);

                                }
                                else
                                {
                                    nodeDet5 = new Ext.Net.Node()
                                    {
                                        Text = (String)rowDet5["Name"],
                                        NodeID = Convert.ToString(rowDet5["IdAggregationLevel"]),
                                        Icon = Icon.Tick,
                                        Leaf = true
                                    };
                                    nodeDet4.Children.Add(nodeDet5);
                                }
                                /////////////////////////////////////////////////////
                                Int32 intDet6 = 0;
                                foreach (DataRow rowDet6 in dtDet6.Rows)
                                {
                                    intDet6 = Convert.ToInt32(rowDet6["IdAggregationLevel"]);

                                    DataTable dtDet7 = objAggregationLevels1.LoadFilterModelsAggregationLevel(IdModel, intDet6, txtFind.Text);

                                    Ext.Net.Node nodeDet6 = new Ext.Net.Node();

                                    if (dtDet7.Rows.Count > 0)
                                    {
                                        nodeDet6 = new Ext.Net.Node()
                                        {
                                            Text = (String)rowDet6["Name"],
                                            NodeID = Convert.ToString(rowDet6["IdAggregationLevel"]),
                                            Icon = Icon.Tick
                                        };
                                        nodeDet5.Children.Add(nodeDet6);

                                    }
                                    else
                                    {
                                        nodeDet6 = new Ext.Net.Node()
                                        {
                                            Text = (String)rowDet6["Name"],
                                            NodeID = Convert.ToString(rowDet6["IdAggregationLevel"]),
                                            Icon = Icon.Tick,
                                            Leaf = true
                                        };
                                        nodeDet5.Children.Add(nodeDet6);
                                    }
                                    /////////////////////////////////////////////////////
                                    Int32 intDet7 = 0;
                                    foreach (DataRow rowDet7 in dtDet7.Rows)
                                    {
                                        intDet7 = Convert.ToInt32(rowDet7["IdAggregationLevel"]);

                                        DataTable dtDet8 = objAggregationLevels1.LoadFilterModelsAggregationLevel(IdModel, intDet7, txtFind.Text);

                                        Ext.Net.Node nodeDet7 = new Ext.Net.Node();

                                        if (dtDet8.Rows.Count > 0)
                                        {
                                            nodeDet7 = new Ext.Net.Node()
                                            {
                                                Text = (String)rowDet7["Name"],
                                                NodeID = Convert.ToString(rowDet7["IdAggregationLevel"]),
                                                Icon = Icon.Tick
                                            };
                                            nodeDet6.Children.Add(nodeDet7);

                                        }
                                        else
                                        {
                                            nodeDet7 = new Ext.Net.Node()
                                            {
                                                Text = (String)rowDet7["Name"],
                                                NodeID = Convert.ToString(rowDet7["IdAggregationLevel"]),
                                                Icon = Icon.Tick,
                                                Leaf = true
                                            };
                                            nodeDet6.Children.Add(nodeDet7);
                                        }
                                        /////////////////////////////////////////////////////
                                        Int32 intDet8 = 0;
                                        foreach (DataRow rowDet8 in dtDet8.Rows)
                                        {
                                            intDet8 = Convert.ToInt32(rowDet8["IdAggregationLevel"]);

                                            DataTable dtDet9 = objAggregationLevels1.LoadFilterModelsAggregationLevel(IdModel, intDet8, txtFind.Text);

                                            Ext.Net.Node nodeDet8 = new Ext.Net.Node();

                                            if (dtDet9.Rows.Count > 0)
                                            {
                                                nodeDet8 = new Ext.Net.Node()
                                                {
                                                    Text = (String)rowDet8["Name"],
                                                    NodeID = Convert.ToString(rowDet8["IdAggregationLevel"]),
                                                    Icon = Icon.Tick
                                                };
                                                nodeDet7.Children.Add(nodeDet8);

                                            }
                                            else
                                            {
                                                nodeDet8 = new Ext.Net.Node()
                                                {
                                                    Text = (String)rowDet8["Name"],
                                                    NodeID = Convert.ToString(rowDet8["IdAggregationLevel"]),
                                                    Icon = Icon.Tick,
                                                    Leaf = true
                                                };
                                                nodeDet7.Children.Add(nodeDet8);
                                            }
                                            /////////////////////////////////////////////////////
                                            Int32 intDet9 = 0;
                                            foreach (DataRow rowDet9 in dtDet9.Rows)
                                            {
                                                intDet9 = Convert.ToInt32(rowDet9["IdAggregationLevel"]);

                                                DataTable dtDet10 = objAggregationLevels1.LoadFilterModelsAggregationLevel(IdModel, intDet9, txtFind.Text);

                                                Ext.Net.Node nodeDet9 = new Ext.Net.Node();

                                                if (dtDet10.Rows.Count > 0)
                                                {
                                                    nodeDet9 = new Ext.Net.Node()
                                                    {
                                                        Text = (String)rowDet9["Name"],
                                                        NodeID = Convert.ToString(rowDet9["IdAggregationLevel"]),
                                                        Icon = Icon.Tick
                                                    };
                                                    nodeDet8.Children.Add(nodeDet9);

                                                }
                                                else
                                                {
                                                    nodeDet9 = new Ext.Net.Node()
                                                    {
                                                        Text = (String)rowDet9["Name"],
                                                        NodeID = Convert.ToString(rowDet9["IdAggregationLevel"]),
                                                        Icon = Icon.Tick,
                                                        Leaf = true
                                                    };
                                                    nodeDet8.Children.Add(nodeDet9);
                                                }
                                                /////////////////////////////////////////////////////
                                                Int32 intDet10 = 0;
                                                foreach (DataRow rowDet10 in dtDet10.Rows)
                                                {
                                                    intDet10 = Convert.ToInt32(rowDet10["IdAggregationLevel"]);

                                                    DataTable dtDet11 = objAggregationLevels1.LoadFilterModelsAggregationLevel(IdModel, intDet10, txtFind.Text);

                                                    Ext.Net.Node nodeDet10 = new Ext.Net.Node();

                                                    if (dtDet11.Rows.Count > 0)
                                                    {
                                                        nodeDet10 = new Ext.Net.Node()
                                                        {
                                                            Text = (String)rowDet10["Name"],
                                                            NodeID = Convert.ToString(rowDet10["IdAggregationLevel"]),
                                                            Icon = Icon.Tick
                                                        };
                                                        nodeDet9.Children.Add(nodeDet10);

                                                    }
                                                    else
                                                    {
                                                        nodeDet10 = new Ext.Net.Node()
                                                        {
                                                            Text = (String)rowDet10["Name"],
                                                            NodeID = Convert.ToString(rowDet10["IdAggregationLevel"]),
                                                            Icon = Icon.Tick,
                                                            Leaf = true
                                                        };
                                                        nodeDet9.Children.Add(nodeDet10);
                                                    }
                                                    /////////////////////////////////////////////////////
                                                    Int32 intDet11 = 0;
                                                    foreach (DataRow rowDet11 in dtDet11.Rows)
                                                    {
                                                        intDet11 = Convert.ToInt32(rowDet11["IdAggregationLevel"]);

                                                        DataTable dtDet12 = objAggregationLevels1.LoadFilterModelsAggregationLevel(IdModel, intDet11, txtFind.Text);

                                                        Ext.Net.Node nodeDet11 = new Ext.Net.Node();

                                                        if (dtDet12.Rows.Count > 0)
                                                        {
                                                            nodeDet11 = new Ext.Net.Node()
                                                            {
                                                                Text = (String)rowDet11["Name"],
                                                                NodeID = Convert.ToString(rowDet11["IdAggregationLevel"]),
                                                                Icon = Icon.Tick
                                                            };
                                                            nodeDet10.Children.Add(nodeDet11);

                                                        }
                                                        else
                                                        {
                                                            nodeDet11 = new Ext.Net.Node()
                                                            {
                                                                Text = (String)rowDet11["Name"],
                                                                NodeID = Convert.ToString(rowDet11["IdAggregationLevel"]),
                                                                Icon = Icon.Tick,
                                                                Leaf = true
                                                            };
                                                            nodeDet10.Children.Add(nodeDet11);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            DataTable dt = objAggregationLevels1.LoadExportList("IdModel = " + IdModel, "ORDER BY RelationLevel, ParentCode");

            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Code", "Aggregation Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Name", "Aggregation Name"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("ParentCode", "Parent Aggregation Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Relationlevel", "Level"));
            String strSelectedParameters = "";
            String[] columnsToCopy = { "Code", "Name", "Relationlevel", "ParentCode" };
            System.Data.DataView view = new System.Data.DataView(dt);
            DataTable dtAggregationLevels = view.ToTable(true, columnsToCopy);
            ExportAggregationLevels = new DataReportObject(dtAggregationLevels, "DATASHEET", null, strSelectedParameters, null, null, datColumnTD, null);
        }

        protected void StoreParents_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            this.storeParents.DataSource = objAggregationLevels.LoadComboBox("IdModel = " + IdModel, "ORDER BY ID");
            this.ddlParents.Select(IdLevel);
        }

        protected void grdLevels_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            IdLevel = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            if (Command == "Edit")
            {
                this.winLevelsEdit.Show();
                this.EditLevelLoad();
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, MessageConfirmDelete(_Name), new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedDeleteYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
        }

        #endregion

        #region Methods

        public void LoadButtons()
        {
            Ext.Net.Button btnExpand = new Ext.Net.Button();
            btnExpand.Text = (String)GetGlobalResourceObject("CPM_Resources","Expand All");
            btnExpand.Icon = Icon.ControlAddBlue;
            btnExpand.Listeners.Click.Handler = this.treeAggregationLevels.ClientID + ".expandAll();";

            Ext.Net.Button btnCollapse = new Ext.Net.Button();
            btnCollapse.Text = (String)GetGlobalResourceObject("CPM_Resources","Collapse All");
            btnCollapse.Icon = Icon.ControlRemoveBlue;
            btnCollapse.Listeners.Click.Handler = this.treeAggregationLevels.ClientID + ".collapseAll();";

            Toolbar1.Items.Add(new Ext.Net.ToolbarFill());
            Toolbar1.Items.Add(btnExpand);
            Toolbar1.Items.Add(new Ext.Net.ToolbarSeparator());
            Toolbar1.Items.Add(btnCollapse);
            
            StatusBar statusBar = new StatusBar();
            statusBar.ID = "StatusBar1";
            statusBar.AutoClear = 1000;
            this.pnlBody.BottomBar.Add(statusBar);

            this.treeAggregationLevels.Listeners.ItemExpand.Buffer = 30;
            this.treeAggregationLevels.Listeners.ItemCollapse.Buffer = 30;
        }

        protected void Clear()
        {
            this.IdLevel = 0;
            this.txtName.Text = "";
            this.txtName.Reset();
            this.txtCode.Text = "";
            this.txtCode.Reset();
            this.ddlParents.Reset();
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void Save(Int32 IdLevel)
        {
            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            objAggregationLevels.IdAggregationLevel = (Int32)IdLevel;
            objAggregationLevels.loadObject();

            if (IdLevel == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (objAggregationLevels.Code.ToString().ToUpper() != txtCode.Text.ToString().ToUpper())
                {
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }
            objAggregationLevels.Code = txtCode.Text;
            objAggregationLevels.Name = txtName.Text;
            objAggregationLevels.IdModel = IdModel;
            if (ddlParents.Value.ToString() == "0")
            {
                objAggregationLevels.Parent = null;
            }
            else
            {
                objAggregationLevels.Parent = this.ddlParents.Value;
            }
            if (IdLevel == 0)
            {
                objAggregationLevels.DateCreation = DateTime.Now;
                objAggregationLevels.UserCreation = (Int32)IdUserCreate;
                IdLevel = objAggregationLevels.Insert();
            }
            else
            {
                objAggregationLevels.DateModification = DateTime.Now;
                objAggregationLevels.UserModification = (Int32)IdUserCreate;
                objAggregationLevels.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winLevelsEdit.Hide();
            this.ModelUpdateTimestamp();
        }

        protected void SaveImport(String _Code, String _Name, String _Parent)
        {
            Int32 _IdParent = GetIdParent(_Parent);

            if (!String.IsNullOrEmpty(_Code) && !String.IsNullOrEmpty(_Name))
            {
                DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
                objAggregationLevels.IdAggregationLevel = (Int32)IdLevel;
                objAggregationLevels.loadObject();
                objAggregationLevels.Code = _Code;
                objAggregationLevels.Name = _Name;
                if (_IdParent != 0)
                {
                    objAggregationLevels.Parent = _IdParent;
                }
                objAggregationLevels.IdModel = IdModel;
                objAggregationLevels.DateCreation = DateTime.Now;
                objAggregationLevels.UserCreation = (Int32)IdUserCreate;
                IdLevel = objAggregationLevels.Insert();
                objAggregationLevels = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected Int32 GetIdParent(String _Parent)
        {
            Int32 Value = 0;
            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dt = objAggregationLevels.LoadList("Code = '" + _Parent + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdAggregationLevel"];
            }
            return Value;
        }

        protected void EditLevelLoad()
        {
            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dt = objAggregationLevels.LoadList("IdAggregationLevel = " + IdLevel, "");
            if (dt.Rows.Count > 0)
            {
                this.txtCode.Text = (String)dt.Rows[0]["Code"];
                this.txtName.Text = (String)dt.Rows[0]["Name"];
                if (dt.Rows[0]["Parent"] == System.DBNull.Value)
                {
                    this.ddlParents.SetValue(0);
                }
                else
                {
                    this.ddlParents.SetValue((Int32)dt.Rows[0]["Parent"]);
                }
            }
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dt = objAggregationLevels.LoadList("IdModel = " + IdModel + " AND Code = '" + txtCode.Text + "'", "");
            Value = dt.Rows.Count;

            return Value;
        }

        public void Delete()
        {
            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            objAggregationLevels.IdAggregationLevel = (Int32)IdLevel;
            objAggregationLevels.Delete();
            this.ModelUpdateTimestamp();
        }

        public void DeleteAll()
        {
            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            objAggregationLevels.IdModel = (Int32)IdModel;
            objAggregationLevels.DeleteAll();
            this.ModelUpdateTimestamp();
        }

        [DirectMethod]
        public void ClickedDeleteYES()
        {
            this.Delete();
            X.Redirect("/parameter/aggregationlevels.aspx");
        }

        [DirectMethod]
        public void ClickedDeleteAllYES()
        {
            this.DeleteAll();
            X.Redirect("/parameter/aggregationlevels.aspx");
        }

        public String MessageConfirmDelete(String itemName)
        {
            String messageConfirmDeleteReturn = "";
            if (!String.IsNullOrEmpty(itemName))
            {
                messageConfirmDeleteReturn = (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + itemName + "?";
            }
            else
            {
                messageConfirmDeleteReturn = (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete all records?");
            }
            messageConfirmDeleteReturn += "<br><br><b>" + (String)GetGlobalResourceObject("CPM_Resources", "This will also delete any related data from the following") + ":</b><br>"
                    + "<br> - " + (String)GetGlobalResourceObject("CPM_Resources", "Fields")
                    + "<br> - " + (String)GetGlobalResourceObject("CPM_Resources", "Projects");
            return messageConfirmDeleteReturn;
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportAggregationLevels.xls", "template");
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExportTD = new DataReportObject();
            StoredExportTD = ExportAggregationLevels;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExportTD);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=1.1 ExportAggregationLevels.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;

            // Update Calculated Columns
            clsAggregationLevels objAggregationLevels = new clsAggregationLevels();
            objAggregationLevels.IdModel = IdModel;
            objAggregationLevels.UpdateAggregationLevelRelationLevel(objApplication.MaxRelationLevels);
            objAggregationLevels = null;
        }

        protected void LoadLabel()
        {
            this.pnlBody.Title = modMain.strMasterAggregationLevels;
            this.lbFilter.Text = (String)GetGlobalResourceObject("CPM_Resources", "Filter") + ":";
            this.btnFind.Text = (String)GetGlobalResourceObject("CPM_Resources", "Find");
            this.btnFind.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Find");
            this.tbnClear.Text = (String)GetGlobalResourceObject("CPM_Resources", "Clear");
            this.tbnClear.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Clear");
            this.btnNew.Text = (String)GetGlobalResourceObject("CPM_Resources", "Add New Aggregation");
            this.btnNew.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Add New Aggregation");
            this.btnDelete.Text = modMain.strCommonDeleteAll;
            this.btnDelete.ToolTip = modMain.strCommonDeleteAll;
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.CommandColumn1.Text=modMain.strCommonEdit;
            this.CommandColumn2.Text = modMain.strCommonDelete;
            this.winLevelsEdit.Title = modMain.strMasterAggregationLevels;
            this.txtCode.FieldLabel = modMain.strCommonCode;
            this.txtName.FieldLabel = modMain.strCommonName;
            this.ddlParents.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Parent Level");
            this.btnSave.Text = modMain.strCommonSave;
            this.btnSave.ToolTip = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.btnCancel.ToolTip = modMain.strCommonClose;
            this.winImport.Title = modMain.strCommonImport + " " + modMain.strMasterAggregationLevels;
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnImportFile.ToolTip = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.btnCancelImport.ToolTip = modMain.strCommonClose;
            //this.CommandColumn1.Text = modMain.strCommonFunctions;
            //((Ext.Net.GridCommand)(CommandColumn1.Commands[0])).Text = "&nbsp;" + modMain.strCommonEdit;//this.CommandColumn1.Commands[0].Text = "&nbsp;" + modMain.strCommonEdit;
            //if (CommandColumn1.Commands.Count > 1)
            //{
            //    ((Ext.Net.GridCommand)(CommandColumn1.Commands[1])).Text = "&nbsp;" + modMain.strCommonDelete;//this.CommandColumn1.Commands[1].Text = "&nbsp;" + modMain.strCommonDelete;
            //}
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        #endregion

    }
}
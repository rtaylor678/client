﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Globalization;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;
using System.ComponentModel;
using System.Web.Script.Serialization;
using System.Net;
using System.Text.RegularExpressions;
using System.Web.Script.Services;

namespace CPM
{
    public partial class _currencies : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdCurrency { get { return (Int32)Session["IdCurrency"]; } set { Session["IdCurrency"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        public Int32 IdBaseCurrency { get { return (Int32)Session["IdBaseCurrency"]; } set { Session["IdBaseCurrency"] = value; } }
        public String BaseCurrencyCode { get { return (String)Session["BaseCurrencyCode"]; } set { Session["BaseCurrencyCode"] = value; } }
        public Int32 IdModelCurrency { get { return (Int32)Session["IdModelCurrency"]; } set { Session["IdModelCurrency"] = value; } }
        private DataReportObject ExportCurrencies { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private String strImportErrorMessageSpecial = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                IdCurrency = 0;
                IdModelCurrency = 0;
                this.LoadCurrencies();
                this.LoadLabel();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winCurrencyAdd.Show();
        }

        protected void btnSaveChanges_Click(object sender, DirectEventArgs e)
        {
            String rowsValues = e.ExtraParams["rowsValues"];
            this.SaveOutput(rowsValues);
            this.LoadCurrencies();
        }

        protected void btnEditCurrency_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winCurrencyEdit.Show();
        }

        protected void btnSaveBase_Click(object sender, DirectEventArgs e)
        {
            this.storeBase.Reload();
            this.winBaseCurrency.Show();
            if (Convert.ToInt32(this.GetIdBaseCurrency()) > 0)
            {
                this.ddlCurrency.Value = Convert.ToInt32(this.GetIdBaseCurrency());
            }
        }

        protected void btnUpdateBase_Click(object sender, DirectEventArgs e)
        {
            this.SaveBase();
            this.LoadCurrencies();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            String rowsValues = e.ExtraParams["rowsValues"];
            this.Save(rowsValues);
            this.LoadCurrencies();
        }

        protected void btnSaveAdd_Click(object sender, DirectEventArgs e)
        {
            SaveImport(txtCode.Text, txtName.Text, txtSymbol.Text, txtValue.Text, chkOutputAdd.Checked);
            winCurrencyAdd.Hide();
            LoadCurrencies();
        }

        protected void btnUpdateCurrency_Click(object sender, DirectEventArgs e)
        {
            String rowsValues = e.ExtraParams["rowsValues"];
            this.UpdateCurrency(rowsValues);
        }

        protected Boolean Valid()
        {
            Boolean Value = true;

           
            return Value;
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            winCurrencyEdit.Hide();
        }

        protected void btnCancelBase_Click(object sender, DirectEventArgs e)
        {
            winBaseCurrency.Hide();
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnCancelAdd_Click(object sender, DirectEventArgs e)
        {
            winCurrencyAdd.Hide();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.Currencies);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    strImportErrorMessageSpecial = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Code"].ToString(), row["Name"].ToString(), row["Sign"].ToString(), row["Value"].ToString(), row["Output"].ToString().ToUpper() == "YES" ? true : false);
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    //this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadCurrencies();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }
        
        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void StoreCurr_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsCurrencies objCurrencies = new DataClass.clsCurrencies();
            this.storeBase.DataSource = objCurrencies.LoadList("IdModel IN (0," + IdModel + ") AND Output = 'True'", "ORDER BY BaseCurrency DESC,Name");
            this.storeBase.DataBind();
        }

        protected void btnDeleteAll_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, MessageConfirmDelete(null), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDeleteAllYES()",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        protected void Clear()
        {
            DataClass.clsModelsCurrencies objModelsCurrencies = new DataClass.clsModelsCurrencies();
            DataTable dt = objModelsCurrencies.LoadList("IdModel = " + IdModel + " AND BaseCurrency = 'True'", "");
            if (dt.Rows.Count > 0)
            {
                this.lblBaseCurrency.Text = (String)GetGlobalResourceObject("CPM_Resources", "Base currency") + ": " + (String)dt.Rows[0]["Name"] + " (" + (String)dt.Rows[0]["Code"] + ")";
                this.lblValueBaseCurrency.Text = Convert.ToString(dt.Rows[0]["Value"]) +  " " + (String)dt.Rows[0]["Sign"] ;
            }
            this.LoadExchange();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });

        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void grdFields_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            IdModelCurrency = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            //if (Command == "Edit")
            //{
            //    this.winFieldsEdit.Show();
            //    this.EditFieldLoad();
            //}
            //else
            if (Command == "Delete")
            {
                X.Msg.Confirm(modMain.strCommonConfirm, MessageConfirmDelete(_Name), new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedDeleteYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
        }

        protected void SaveBase()
        {

            DataClass.clsModelsCurrencies objModelsCurrencies = new DataClass.clsModelsCurrencies();
            DataTable dt = objModelsCurrencies.LoadList("IdModel = " + IdModel + " AND Output = 'True'", "");

            foreach (DataRow row in dt.Rows)
            {
                objModelsCurrencies.IdModelCurrency = (Int32)row["IdModelCurrency"];
                objModelsCurrencies.loadObject();

                if (objModelsCurrencies.IdCurrency == Convert.ToInt32(this.ddlCurrency.Value))
                {
                    objModelsCurrencies.Value = 1;
                    objModelsCurrencies.BaseCurrency = true;
                }
                else
                { 
                    objModelsCurrencies.Value = 0;
                    objModelsCurrencies.BaseCurrency = false;
                }
                objModelsCurrencies.DateModification = DateTime.Now;
                objModelsCurrencies.UserModification = (Int32)IdUserCreate;
                objModelsCurrencies.Update();
            }

            this.FormBaseStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winBaseCurrency.Hide();
        }

        protected void SaveOutput(String _Values)
        {
            List<Object> data = JSON.Deserialize<List<Object>>(_Values);
            foreach (Object value in data)
            {
                Object _ObjectRow = new Object();
                _ObjectRow = value.ToString();

                JavaScriptSerializer ser = new JavaScriptSerializer();
                Dictionary<string, object> _Row = ser.Deserialize<Dictionary<string, object>>(_ObjectRow.ToString());

                DataClass.clsModelsCurrencies objModelsCurrencies = new DataClass.clsModelsCurrencies();

                if ((Boolean)_Row["Output"])
                {
                    Int32 intIdModelCurrency = Convert.ToInt32(_Row["IdModelCurrency"]);
                    objModelsCurrencies.IdModelCurrency = intIdModelCurrency;
                    objModelsCurrencies.loadObject();
                    objModelsCurrencies.Value = Convert.ToDouble(_Row["Value"]);
                    objModelsCurrencies.Output = Convert.ToBoolean(_Row["Output"]);
                    objModelsCurrencies.BaseCurrency = Convert.ToBoolean(_Row["BaseCurrency"]);
                    objModelsCurrencies.IdCurrency = Convert.ToInt32(_Row["IdCurrency"]);
                    objModelsCurrencies.IdModel = IdModel;
                    if (intIdModelCurrency == 0)
                    {
                        objModelsCurrencies.DateCreation = DateTime.Now;
                        objModelsCurrencies.UserCreation = Convert.ToInt32(IdUserCreate);
                        objModelsCurrencies.Insert();
                    }
                    else
                    {
                        objModelsCurrencies.DateModification = DateTime.Now;
                        objModelsCurrencies.UserModification = Convert.ToInt32(IdUserCreate);
                        objModelsCurrencies.Update();
                    }
                }
                else
                {
                    Int32 intIdModelCurrency = Convert.ToInt32(_Row["IdModelCurrency"]);
                    if (intIdModelCurrency != 0)
                    {
                        objModelsCurrencies.IdModelCurrency = intIdModelCurrency;
                        objModelsCurrencies.Delete();
                    }
                }
            }
        }

        static DataTable ConvertListToDataTable(List<string[]> list)
        {
            // New table.
            DataTable table = new DataTable();

            // Get max columns.
            int columns = 0;
            foreach (var array in list)
            {
                if (array.Length > columns)
                {
                    columns = array.Length;
                }
            }

            // Add columns.
            for (int i = 0; i < columns; i++)
            {
                table.Columns.Add();
            }

            // Add rows.
            foreach (var array in list)
            {
                table.Rows.Add(array);
            }

            return table;
        }

        protected void Save(String _Values)
        {
            IdBaseCurrency = GetIdBaseCurrency();
            DataClass.clsModelsCurrencies objModelsCurrencies = new DataClass.clsModelsCurrencies();

            DataTable dt = new DataTable();
            dt.Columns.Add("IdModelCurrency",typeof(int));
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Code", typeof(string));
            dt.Columns.Add("Value", typeof(float));

            List<Object> data = JSON.Deserialize<List<Object>>(_Values);

            foreach (Object value in data)
            {
                Object _ObjectRow = new Object();
                _ObjectRow = value.ToString();

                JavaScriptSerializer ser = new JavaScriptSerializer();

                Dictionary<string, object> _Row = ser.Deserialize<Dictionary<string, object>>(_ObjectRow.ToString());
                DataRow _NewRow = dt.NewRow();

                _NewRow["IdModelCurrency"] = _Row["IdModelCurrency"];
                _NewRow["Name"] = _Row["Name"].ToString();
                _NewRow["Code"] = _Row["Code"].ToString();
                _NewRow["Value"] = _Row["Value"].ToString();
                dt.Rows.Add(_NewRow);
            }
            
            
            foreach (DataRow row in dt.Rows)
            {
                Int32 intIdModelCurrency = Convert.ToInt32(row["IdModelCurrency"]);
                objModelsCurrencies.IdModelCurrency = intIdModelCurrency;
                objModelsCurrencies.loadObject();

                objModelsCurrencies.Value = Convert.ToDouble(row["Value"]);
                objModelsCurrencies.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winCurrencyEdit.Hide();
        }

        protected void SaveImport(String _Code, String _Name, String _Sign, String _Value, Boolean _Output)
        {
            DataClass.clsCurrencies objCurrencies = new DataClass.clsCurrencies();
            DataClass.clsModelsCurrencies objModelsCurrencies = new DataClass.clsModelsCurrencies();
            IdCurrency = GetIdCurrency(_Name, _Code);
            objCurrencies.IdCurrency = IdCurrency;

            if (!String.IsNullOrEmpty(_Code) && !String.IsNullOrEmpty(_Name) && !objCurrencies.CurrencyExists(IdModel, IdCurrency) )
            {
                if (IdCurrency == 0)
                {
                    objCurrencies.IdCurrency = (Int32)IdCurrency;
                    objCurrencies.loadObject();
                    objCurrencies.Code = _Code;
                    objCurrencies.Sign = _Sign;
                    objCurrencies.Name = _Name;
                    IdCurrency = objCurrencies.Insert();
                }

                if (!objCurrencies.CurrencyExists(IdModel, IdCurrency))
                {
                    objModelsCurrencies.IdModelCurrency = (Int32)IdModelCurrency;
                    objModelsCurrencies.loadObject();
                    objModelsCurrencies.IdModel = IdModel;
                    objModelsCurrencies.IdCurrency = IdCurrency;
                    objModelsCurrencies.Value = Convert.ToDouble(_Value);
                    objModelsCurrencies.BaseCurrency = false;
                    objModelsCurrencies.Output = _Output;
                    objModelsCurrencies.DateCreation = DateTime.Now;
                    objModelsCurrencies.UserCreation = (Int32)IdUserCreate;
                    IdModelCurrency = objModelsCurrencies.Insert();
                }

                objModelsCurrencies = null;
                objCurrencies = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected void LoadExchange()
        {
            DataClass.clsModelsCurrencies objModelsCurrencies = new DataClass.clsModelsCurrencies();
            this.storeExchange.DataSource = objModelsCurrencies.LoadList("IdModel = " + IdModel + " AND Output = 'True' AND BaseCurrency <> 'True'", "ORDER BY Name");
            this.storeExchange.DataBind();
        }

        protected Int32 GetIdCurrency(String _Name)
        {
            Int32 Value = 0;
            DataClass.clsCurrencies objCurrencies = new DataClass.clsCurrencies();

            DataTable dt = objCurrencies.LoadList("Name = '" + _Name + "'", "ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdCurrency"];
            }
            return Value;
        }

        protected Int32 GetIdCurrency(String _Name, String _Code)
        {
            Int32 Value = 0;
            DataClass.clsCurrencies objCurrencies = new DataClass.clsCurrencies();

            DataTable dt = CD.DAC.ConsultSQL("SELECT * FROM tblCurrencies WHERE Code = '" + _Code + "'").Tables[0];
            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdCurrency"];
            }
            return Value;
        }

        protected Int32 GetIdBaseCurrency()
        {
            Int32 Value = 0;
            DataClass.clsCurrencies objCurrencies = new DataClass.clsCurrencies();

            DataTable dt = objCurrencies.LoadList("BaseCurrency = 'True' AND IdModel = " + IdModel, "ORDER BY Name");
            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdCurrency"];
                IdBaseCurrency = (Int32)dt.Rows[0]["IdCurrency"];
                BaseCurrencyCode = (String)dt.Rows[0]["Code"];
            }
            return Value;
        }

        protected void LoadCurrencies()
        {
            DataClass.clsCurrencies objCurrencies = new DataClass.clsCurrencies();

            DataTable dt = objCurrencies.LoadModelsCurrencies(IdModel);

            storeCurrencies.DataSource = dt;
            storeCurrencies.DataBind();

            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Code", "Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Name", "Name"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Sign", "Sign"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Value", "Value"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Output", "Output"));
            String strSelectedParameters = "";
            String[] columnsToCopy = { "Code", "Name", "Sign", "Value", "Output" };
            System.Data.DataView view = new System.Data.DataView(dt);
            DataTable dtProjects = view.ToTable(true, columnsToCopy);
            ExportCurrencies = new DataReportObject(dtProjects, "DATASHEET", null, strSelectedParameters, null, "Name", datColumnTD, null);
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExportTD = new DataReportObject();
            StoredExportTD = ExportCurrencies;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExportTD);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=1.4 ExportCurrencies.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportCurrencies.xls", "template");
        }

        protected void LoadLabel()
        {
            this.grdCurrencies.Title = modMain.strParameterModuleExchangeRates;
            this.btnAdd.Text = (String)GetGlobalResourceObject("CPM_Resources", "Add");
            this.btnAdd.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Add");
            this.btnSaveChange.Text = modMain.strCommonSaveChanges;
            this.btnSaveChange.ToolTip = modMain.strCommonSaveChanges;
            this.btnCurrencyExchange.Text = (String)GetGlobalResourceObject("CPM_Resources", "Currency Exchange");
            this.btnCurrencyExchange.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Currency Exchange");
            this.btnChangeBase.Text = (String)GetGlobalResourceObject("CPM_Resources", "Base Currency");
            this.btnChangeBase.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Base Currency");
            this.Code.Text = modMain.strCommonCode;
            this.Name.Text = modMain.strCommonName;
            this.Sign.Text = (String)GetGlobalResourceObject("CPM_Resources", "Symbol");
            this.Value.Text = (String)GetGlobalResourceObject("CPM_Resources", "Value");
            this.chkOutput.Text = (String)GetGlobalResourceObject("CPM_Resources", "Output");
            this.winCurrencyEdit.Title = (String)GetGlobalResourceObject("CPM_Resources", "Currency Exchange");
            this.lblBaseCurrency.Text = (String)GetGlobalResourceObject("CPM_Resources", "Base Currency");
            this.colCode.Text = modMain.strCommonCode;
            this.colName.Text = modMain.strCommonName;
            this.colValue.Text = (String)GetGlobalResourceObject("CPM_Resources", "Value");
            this.btnSave.Text = modMain.strCommonSave;
            this.btnSave.ToolTip = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.btnCancel.ToolTip = modMain.strCommonClose;
            this.winBaseCurrency.Title = (String)GetGlobalResourceObject("CPM_Resources", "Currency Exchange");
            this.ddlCurrency.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Base Currency");
            this.btnSaveBaseCurrency.Text = (String)GetGlobalResourceObject("CPM_Resources", "Change");
            this.btnSaveBaseCurrency.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Change");
            this.btnCancelBase.Text = modMain.strCommonClose;
            this.btnCancelBase.ToolTip = modMain.strCommonClose;
            this.btnUpdate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Update");
            this.btnUpdate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Update");
            this.btnDelete.Text = modMain.strCommonDeleteAll;
            this.btnDelete.ToolTip = modMain.strCommonDeleteAll;
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = "&nbsp;" + modMain.strCommonDelete;
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.winCurrencyAdd.Title = (String)GetGlobalResourceObject("CPM_Resources", "Add Currency");
            this.txtCode.FieldLabel = modMain.strCommonCode;
            this.txtName.FieldLabel = modMain.strCommonName;
            this.txtSymbol.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Symbol");
            this.txtValue.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Value");
            this.chkOutputAdd.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Output");
            this.btnSaveAdd.Text = modMain.strCommonSave;
            this.btnSaveAdd.ToolTip = modMain.strCommonSave;
            this.btnCancelAdd.Text = modMain.strCommonClose;
            this.btnCancelAdd.ToolTip = modMain.strCommonClose;
        }

        protected void UpdateCurrency(String _Values)
        {
            IdBaseCurrency = GetIdBaseCurrency();
            DataClass.clsModelsCurrencies objModelsCurrencies = new DataClass.clsModelsCurrencies();

            DataTable dt = new DataTable();
            dt.Columns.Add("IdModelCurrency", typeof(int));
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Code", typeof(string));
            dt.Columns.Add("Value", typeof(float));

            List<Object> data = JSON.Deserialize<List<Object>>(_Values);

            foreach (Object value in data)
            {
                Object _ObjectRow = new Object();
                _ObjectRow = value.ToString();

                JavaScriptSerializer ser = new JavaScriptSerializer();

                Dictionary<string, object> _Row = ser.Deserialize<Dictionary<string, object>>(_ObjectRow.ToString());
                DataRow _NewRow = dt.NewRow();

                WebClient web = new WebClient();
                string url = string.Format("http://finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1&s={0}{1}=X", BaseCurrencyCode, _Row["Code"].ToString());
                string response = web.DownloadString(url);
                string[] values = Regex.Split(response, ",");
                decimal rate = System.Convert.ToDecimal(values[1]);

                _NewRow["IdModelCurrency"] = _Row["IdModelCurrency"];
                _NewRow["Name"] = _Row["Name"].ToString();
                _NewRow["Code"] = _Row["Code"].ToString();
                _NewRow["Value"] = rate.ToString();
                dt.Rows.Add(_NewRow);
            }

            storeExchange.DataSource = dt;
            storeExchange.DataBind();

            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        public String MessageConfirmDelete(String itemName)
        {
            String messageConfirmDeleteReturn = "";
            if (!String.IsNullOrEmpty(itemName))
            {
                messageConfirmDeleteReturn = (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + itemName + "?";
            }
            else
            {
                messageConfirmDeleteReturn = (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete all records?");
            }
            return messageConfirmDeleteReturn;
        }

        [DirectMethod]
        public void ClickedDeleteYES()
        {
            this.Delete();
            this.LoadCurrencies();
        }

        public void Delete()
        {
            DataClass.clsModelsCurrencies objCurrencies = new DataClass.clsModelsCurrencies();
            objCurrencies.IdModelCurrency = (Int32)IdModelCurrency;
            objCurrencies.Delete();
            this.ModelUpdateTimestamp();
        }

        [DirectMethod]
        public void ClickedDeleteAllYES()
        {
            this.DeleteAll();
            this.LoadCurrencies();
        }

        public void DeleteAll()
        {
            DataClass.clsModelsCurrencies objCurrencies = new DataClass.clsModelsCurrencies();
            objCurrencies.IdModel = (Int32)IdModel;
            objCurrencies.DeleteAll();
            this.ModelUpdateTimestamp();
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        #endregion

    }
}
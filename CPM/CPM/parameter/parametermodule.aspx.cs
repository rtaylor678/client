﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{     
    public partial class _parametermodule : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }

        private static modMain objMain = new modMain();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                this.LoadModules();
                if (this.GetProject())
                {
                    this.btnProjects.Disable();
                }
                if (this.GetEconomicModule())
                {
                    this.btnProjects.Disable();
                }
                LoadLanguage();
            }
        }

        protected void btnAggregationLevel_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/parameter/aggregationlevels.aspx");
        }

        protected void btnTypeOperation_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/parameter/typesoperation.aspx");
        }

        protected void btnField_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/parameter/fields.aspx");
        }

        protected void btnProjects_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/parameter/projects.aspx");
        }

        protected void btnCurrencies_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/parameter/currencies.aspx");
        }

        protected void btnZiffStructure_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/parameter/ziffstructure.aspx");
        }

        #endregion

        #region Methods

        public Boolean GetProject()
        {
            Boolean Value = false;
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("Level IN (1,3) AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = true;
            }

            return Value;
        }

        protected void LoadModules()
        {
            // Parameters Module
            if (objApplication.AllowedRoles.Contains(3))
            {
                // Aggregation Levels
                if (objApplication.AllowedRoles.Contains(4))
                {
                    this.btnAggregationLevels.Disabled = false;
                }
                // Types of Operations
                if (objApplication.AllowedRoles.Contains(5))
                {
                    this.btnOperations.Disabled = false;
                }
                // List of Fields
                if (objApplication.AllowedRoles.Contains(6))
                {
                    this.btnFields.Disabled = false;
                }
                // List of Cases
                if (objApplication.AllowedRoles.Contains(7))
                {
                    this.btnProjects.Disabled = false;
                }
                // Currency
                if (objApplication.AllowedRoles.Contains(8))
                {
                    this.btnCurrency.Disabled = false;
                }
                // Cost Projection Categories
                if (objApplication.AllowedRoles.Contains(44))
                {
                    this.btnZiffStructure.Disabled = false;
                }
            }
        }

        protected void LoadLanguage()
        {
            DataClass.clsLabelsLanguages objLabelsLanguages = new DataClass.clsLabelsLanguages();
            DataTable dt = objLabelsLanguages.LoadList("LanguageName = '" + Language + "' AND FormName = 'ParameterModule'", "ORDER BY IdLabel");
            foreach (DataRow row in dt.Rows)
            {
                //idLanguage = (Int32)row["IdLanguage"];
                switch ((String)row["LabelName"])
                {
                    case "Parameters Module":
                        modMain.strParameterModuleFormTitle = (String)row["LabelText"];
                        break;
                    case "1. General Parameters":
                        modMain.strParameterModuleFormSubTitle1 = (String)row["LabelText"];
                        break;
                    case "Currency":
                        modMain.strParameterModuleExchangeRates = (String)row["LabelText"];
                        break;
                    case "3. Cost Projection Categories":
                        modMain.strParameterModuleFormSubTitle3 = (String)row["LabelText"];
                        break;
                }
            }
            this.LoadLabel();
        }

        protected void LoadLabel()
        {
            this.pnlParameters1.Title = modMain.strParameterModuleFormTitle;
            //this.FieldSet1.Title = modMain.strParameterModuleFormSubTitle1;
            this.btnAggregationLevels.Text = modMain.strMasterAggregationLevels;
            this.btnOperations.Text = modMain.strMasterTypesofOperations;
            this.btnFields.Text = modMain.strMasterListofFields;
            this.btnProjects.Text = modMain.strMasterFieldProjects;
            this.btnCurrency.Text = modMain.strParameterModuleExchangeRates;
            //this.FieldSet2.Title = modMain.strParameterModuleFormSubTitle3;
            this.btnZiffStructure.Text = objMain.FormatButtonText(modMain.strMasterZiff_Third_partyCostCategories);
        }

        public Boolean GetEconomicModule()
        {
            Boolean Value = false;
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("Level IN (1,2) AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = true;
            }

            return Value;
        }

        #endregion

    }
}
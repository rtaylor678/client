﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _typesoperation : System.Web.UI.Page
    {

        #region Definitions

        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                this.LoadButtons();
                this.LoadTree();
                this.LoadLabel();
            }
        }

        protected void SubmitNodes(object sender, SubmitEventArgs e)
        {
            try
            {
                this.DeleteAll();
                var nodes = e.RootNode.Children;

                foreach (Ext.Net.SubmittedNode nodeShore in e.RootNode.Children)
                {
                    foreach (Ext.Net.SubmittedNode nodeType in nodeShore.Children)
                    {
                        foreach (Ext.Net.SubmittedNode nodeDetail1 in nodeType.Children)
                        {
                            if (nodeDetail1.Checked || nodeDetail1.Attributes.ContainsValue("NodeLocked"))
                            {
                                Int32 nodeId = Convert.ToInt32(nodeDetail1.NodeID);
                                this.Save(nodeId);
                            }
                            foreach (Ext.Net.SubmittedNode nodeDetail2 in nodeDetail1.Children)
                            {
                                if (nodeDetail2.Checked || nodeDetail2.Attributes.ContainsValue("NodeLocked"))
                                {
                                    Int32 nodeId = Convert.ToInt32(nodeDetail2.NodeID);
                                    this.Save(nodeId);
                                }
                                foreach (Ext.Net.SubmittedNode nodeDetail3 in nodeDetail2.Children)
                                {
                                    if (nodeDetail3.Checked || nodeDetail3.Attributes.ContainsValue("NodeLocked"))
                                    {
                                        Int32 nodeId = Convert.ToInt32(nodeDetail3.NodeID);
                                        this.Save(nodeId);
                                    }

                                }
                            }
                        }

                    }

                }
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Record saved successfully!"), IconCls = "icon-accept", Clear2 = false });
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }

        }

        #endregion

        #region Methods

        public void LoadButtons()
        {
            Ext.Net.Button btnExpand = new Ext.Net.Button();
            btnExpand.Text = (String)GetGlobalResourceObject("CPM_Resources","Expand All");
            btnExpand.Icon = Icon.ControlAddBlue;
            btnExpand.Listeners.Click.Handler = this.treeOperation.ClientID + ".expandAll();";

            Ext.Net.Button btnCollapse = new Ext.Net.Button();
            btnCollapse.Text = (String)GetGlobalResourceObject("CPM_Resources","Collapse All");
            btnCollapse.Icon = Icon.ControlRemoveBlue;
            btnCollapse.Listeners.Click.Handler = this.treeOperation.ClientID + ".collapseAll();";

            Toolbar1.Items.Add(new Ext.Net.ToolbarFill());
            Toolbar1.Items.Add(btnExpand);
            Toolbar1.Items.Add(new Ext.Net.ToolbarSeparator());
            Toolbar1.Items.Add(btnCollapse);
            
            this.treeOperation.Listeners.ItemExpand.Buffer = 30;
            this.treeOperation.Listeners.ItemCollapse.Buffer = 30;
        }

        private void LoadTree()
        {
            try
            {
                Ext.Net.Node root = new Ext.Net.Node()
                {
                    Text = (String)GetGlobalResourceObject("CPM_Resources","Root")
                };
                root.Expanded = true;
                this.treeOperation.Root.Add(root);

                DataClass.clsParameters objParameters = new DataClass.clsParameters();
                DataTable dt = objParameters.LoadList("IdLanguage = " + idLanguage + " AND Type = 'OperationShore'", "");

                Int32 intShore = 0;

                foreach (DataRow row in dt.Rows)
                {
                    intShore = Convert.ToInt32(row["Code"]);
                    Ext.Net.Node nodeShore = new Ext.Net.Node()
                    {
                        Text = (String)row["Name"],
                        Icon = Icon.CogStart
                    };
                    root.Children.Add(nodeShore);

                    DataTable dtType = objParameters.LoadList("IdLanguage = " + idLanguage + " AND Type = 'OperationType'", "");

                    Int32 intType = 0;

                    foreach (DataRow rowType in dtType.Rows)
                    {
                        intType = Convert.ToInt32(rowType["Code"]);

                        DataClass.clsTypesOperation objTypesOperation1 = new DataClass.clsTypesOperation();
                        DataTable dtDet1 = objTypesOperation1.LoadModelsTypeOperation(intType,intShore,IdModel,0);

                        Ext.Net.Node nodeType = new Ext.Net.Node();

                        if (dtDet1.Rows.Count > 0)
                        {
                            nodeType = new Ext.Net.Node()
                            {
                                Text = (String)rowType["Name"],
                                Icon = Icon.Zoom
                            };
                            nodeShore.Children.Add(nodeType);
                        }
                        else
                        {
                            nodeType = new Ext.Net.Node()
                            {
                                Text = (String)rowType["Name"],
                                Icon = Icon.Tick,
                                Leaf = true,
                                Checked = true
                            };
                            nodeShore.Children.Add(nodeType);
                        }



                        Int32 intDet1 = 0;
                        foreach (DataRow rowDet1 in dtDet1.Rows)
                        {
                            intDet1 = Convert.ToInt32(rowDet1["IdTypeOperation"]);

                            DataTable dtDet2 = objTypesOperation1.LoadModelsTypeOperation(intType, intShore, IdModel, intDet1);

                            Ext.Net.Node nodeDet1 = new Ext.Net.Node();

                            if (dtDet2.Rows.Count > 0)
                            {
                                nodeDet1 = new Ext.Net.Node()
                                {
                                    Text = (String)rowDet1["Name"],
                                    NodeID = Convert.ToString(rowDet1["IdTypeOperation"]),
                                    Icon = Icon.BookGo
                                };
                                nodeType.Children.Add(nodeDet1);

                            }
                            else
                            {
                                if (Convert.ToBoolean(rowDet1["TypeLocked"]))
                                {
                                    nodeDet1 = new Ext.Net.Node()
                                    {
                                        Text = (String)rowDet1["Name"],
                                        NodeID = Convert.ToString(rowDet1["IdTypeOperation"]),
                                        Icon = Icon.Lock,
                                        Checked = null,
                                        Leaf = true
                                    };
                                    nodeDet1.CustomAttributes.Add(new ConfigItem { Name = "NodeLocked", Value = "NodeLocked", Mode = ParameterMode.Value });
                                }
                                else
                                {
                                    nodeDet1 = new Ext.Net.Node()
                                    {
                                        Text = (String)rowDet1["Name"],
                                        NodeID = Convert.ToString(rowDet1["IdTypeOperation"]),
                                        IconCls = "x-hidden",
                                        Checked = Convert.ToBoolean(rowDet1["Checked"]),
                                        Leaf = true
                                    };
                                }
                                nodeType.Children.Add(nodeDet1);
                            }

                            Int32 intDet2 = 0;
                            foreach (DataRow rowDet2 in dtDet2.Rows)
                            {
                                intDet2 = Convert.ToInt32(rowDet2["IdTypeOperation"]);

                                DataTable dtDet3 = objTypesOperation1.LoadModelsTypeOperation(intType, intShore, IdModel, intDet2);

                                Ext.Net.Node nodeDet2 = new Ext.Net.Node();

                                if (dtDet3.Rows.Count > 0)
                                {
                                    nodeDet2 = new Ext.Net.Node()
                                    {
                                        Text = (String)rowDet2["Name"],
                                        NodeID = Convert.ToString(rowDet2["IdTypeOperation"]),
                                        Icon = Icon.Add
                                    };
                                    nodeDet1.Children.Add(nodeDet2);
                                }
                                else
                                {
                                    if (Convert.ToBoolean(rowDet2["TypeLocked"]))
                                    {
                                        nodeDet2 = new Ext.Net.Node()
                                        {
                                            Text = (String)rowDet2["Name"],
                                            NodeID = Convert.ToString(rowDet2["IdTypeOperation"]),
                                            Icon = Icon.Lock,
                                            Checked = null,
                                            Leaf = true
                                        };
                                        nodeDet2.CustomAttributes.Add(new ConfigItem { Name = "NodeLocked", Value = "NodeLocked", Mode = ParameterMode.Value });
                                    }
                                    else
                                    {
                                        nodeDet2 = new Ext.Net.Node()
                                        {
                                            Text = (String)rowDet2["Name"],
                                            NodeID = Convert.ToString(rowDet2["IdTypeOperation"]),
                                            IconCls = "x-hidden",
                                            Checked = Convert.ToBoolean(rowDet2["Checked"]),
                                            Leaf = true
                                        };
                                    }
                                    nodeDet1.Children.Add(nodeDet2);
                                }

                                Int32 intDet3 = 0;
                                foreach (DataRow rowDet3 in dtDet3.Rows)
                                {
                                    intDet3 = Convert.ToInt32(rowDet3["IdTypeOperation"]);
                                    Ext.Net.Node nodeDet3 = new Ext.Net.Node();

                                    if (Convert.ToBoolean(rowDet3["TypeLocked"]))
                                    {
                                        nodeDet3 = new Ext.Net.Node()
                                        {
                                            Text = (String)rowDet3["Name"],
                                            NodeID = Convert.ToString(rowDet3["IdTypeOperation"]),
                                            Icon = Icon.Lock,
                                            Checked = null,
                                            Leaf = true
                                        };
                                        nodeDet3.CustomAttributes.Add(new ConfigItem { Name = "NodeLocked", Value = "NodeLocked", Mode = ParameterMode.Value });
                                    }
                                    else
                                    {
                                        nodeDet3 = new Ext.Net.Node()
                                        {
                                            Text = (String)rowDet3["Name"],
                                            NodeID = Convert.ToString(rowDet3["IdTypeOperation"]),
                                            IconCls = "x-hidden",
                                            Checked = Convert.ToBoolean(rowDet3["Checked"]),
                                            Leaf = true
                                        };
                                    }
                                    nodeDet2.Children.Add(nodeDet3);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
            }

        }

        protected void Save(Int32 IdTypeOperation)
        {
            DataClass.clsModelsTypesOperation objModelsTypesOperation = new DataClass.clsModelsTypesOperation();
            objModelsTypesOperation.IdModel = (Int32)IdModel;
            objModelsTypesOperation.IdTypeOperation = IdTypeOperation;
            objModelsTypesOperation.Insert();
            this.LoadTree();
            
        }

        public void DeleteAll()
        {
            DataClass.clsModelsTypesOperation objModelsTypesOperation = new DataClass.clsModelsTypesOperation();

            objModelsTypesOperation.IdModel = (Int32)IdModel;
            objModelsTypesOperation.Delete();
        }

        protected void LoadLabel()
        {
            this.pnlOperation.Title=(String)GetGlobalResourceObject("CPM_Resources","Types of Operation");
            this.btnSave.Text = modMain.strCommonSaveChanges;
            this.btnSave.ToolTip = modMain.strCommonSaveChanges;
        }

        #endregion

    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _fields : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdField { get { return (Int32)Session["IdField"]; } set { Session["IdField"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private DataReportObject ExportTechnicaldrivers { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                IdField = 0;
                this.txtStarts.MinValue = LoadMinStarts();
                this.txtStarts.Reset();
                this.LoadFields();
                this.LoadLabel();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winFieldsEdit.Show();
        }

        protected void btnDeleteAll_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, MessageConfirmDelete(null), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDeleteAllYES()",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.Save();
                this.LoadFields();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            winFieldsEdit.Hide();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.Fields);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Field Code"].ToString(), row["Field Name"].ToString(), row["Aggregation Level"].ToString(), row["Type of Operation"].ToString(), Convert.ToInt32(row["Year Starts"]));
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadFields();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        protected void Clear()
        {
            this.IdField = 0;
            this.txtName.Text = "";
            this.txtName.Reset();
            this.txtCode.Text = "";
            this.txtCode.Reset();
            this.txtStarts.Text = Convert.ToString(LoadMinStarts());
            this.ddlLevels.Reset();
            this.ddlTypeOperation.Reset();
        }

        protected Int32 LoadMinStarts()
        {
            Int32 Value = 0;
            DataClass.clsModels objModels = new DataClass.clsModels();
            DataTable dt = objModels.LoadList("IdModel = " + IdModel, "");
            if (dt.Rows.Count>0)
            {
                Value = (Int32)dt.Rows[0]["BaseYear"];
            }
            return Value;
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void grdFields_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            IdField = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            if (Command == "Edit")
            {
                this.winFieldsEdit.Show();
                this.EditFieldLoad();
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, MessageConfirmDelete(_Name), new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedDeleteYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }      
        }

        protected void Save()
        {
            DataClass.clsFields objFields = new DataClass.clsFields();
            objFields.IdField = (Int32)IdField;
            objFields.loadObject();

            if (IdField == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (objFields.Code.ToString().ToUpper() != txtCode.Text.ToString().ToUpper())
                {
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }

            objFields.Code = txtCode.Text;
            objFields.Name = txtName.Text;
            // Need to validate that the new start year of the field doesn't have any cases that fall under the new date
            if (objFields.ValidateFieldNewStarts((Int32)IdField, Convert.ToInt32(txtStarts.Text)) == 0)
            {
                objFields.Starts = Convert.ToInt32(txtStarts.Text);
                objFields.IdModel = IdModel;
                objFields.IdAggregationLevel = Convert.ToInt32(this.ddlLevels.Value);
                objFields.IdTypeOperation = Convert.ToInt32(this.ddlTypeOperation.Value);
                if (IdField == 0)
                {
                    objFields.DateCreation = DateTime.Now;
                    objFields.UserCreation = (Int32)IdUserCreate;
                    IdField = objFields.Insert();
                }
                else
                {
                    objFields.DateModification = DateTime.Now;
                    objFields.UserModification = (Int32)IdUserCreate;
                    objFields.OldStarts = Convert.ToInt32(hidOldStarts.Value);
                    objFields.Update();
                }
            }
            else
            {
                X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Field New Start Date Error!!!"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Cannot change field start date when Baseline has lesser start date!!!") });
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winFieldsEdit.Hide();
            this.ModelUpdateTimestamp();
        }

        protected Int32 LoadIdLevels(String _Levels)
        {
            Int32 IdLevels = 0;
            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dt = objAggregationLevels.LoadList("IdModel = " + IdModel + " AND Name LIKE '%" + _Levels + "%'", "ORDER BY Name");

            if (dt.Rows.Count > 0)
            {
                IdLevels = (Int32)dt.Rows[0]["IdAggregationLevel"];
            }
            return IdLevels;
        }

        protected Int32 LoadIdOperation(String _Operation)
        {
            Int32 IdOperation = 0;
            DataClass.clsTypesOperation objTypesOperation = new DataClass.clsTypesOperation();
            DataTable dt = objTypesOperation.LoadList("IdModel = " + IdModel + " AND LastNode = 'True' AND Name LIKE '%" + _Operation + "%'", "ORDER BY Name");    

            if (dt.Rows.Count>0)
            {
                IdOperation = (Int32)dt.Rows[0]["IdTypeOperation"];
            }
            return IdOperation;
        }

        protected void SaveImport(String _Code, String _Name, String _AggregationLevel, String _TypeOperation, Int32 _Year)
        {
            Int32 _MinYear = LoadMinStarts();
            Int32 _IdAggregationLevel = GetIdAggregationLevel(_AggregationLevel);
            Int32 _IdTypeOperation = GetIdOperation(_TypeOperation);

            if (_Year < _MinYear)
            {
                _Year = _MinYear;
            }

            if (!String.IsNullOrEmpty(_Code) && !String.IsNullOrEmpty(_Name) && _IdAggregationLevel != 0 && _IdTypeOperation != 0)
            {
                DataClass.clsFields objFields = new DataClass.clsFields();
                objFields.IdField = (Int32)IdField;
                objFields.loadObject();
                objFields.Code = _Code;
                objFields.Name = _Name;
                objFields.Starts = _Year;
                objFields.IdModel = IdModel;
                objFields.IdAggregationLevel = _IdAggregationLevel;
                objFields.DateCreation = DateTime.Now;
                objFields.UserCreation = (Int32)IdUserCreate;
                objFields.IdTypeOperation = _IdTypeOperation;
                IdField = objFields.Insert();
                objFields = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected Int32 GetIdOperation(String _TypeOperation)
        {
            Int32 Value = 0;
            DataClass.clsTypesOperation objTypesOperation = new DataClass.clsTypesOperation();
            DataTable dt = objTypesOperation.LoadList("ParentName + ' - ' + Name = '" + _TypeOperation + "'", "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdTypeOperation"];
            }
            return Value;
        }

        protected Int32 GetIdAggregationLevel(String _AggregationLevel)
        {
            Int32 Value = 0;
            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            DataTable dt = objAggregationLevels.LoadList("Code = '" + _AggregationLevel + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdAggregationLevel"];
            }
            return Value;
        }

        protected void EditFieldLoad()
        {
            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dt = objFields.LoadList("IdField = " + IdField, "");
            if (dt.Rows.Count > 0)
            {
                this.txtCode.Text = (String)dt.Rows[0]["Code"];
                this.txtName.Text = (String)dt.Rows[0]["Name"];
                this.txtStarts.Text = Convert.ToString(dt.Rows[0]["Starts"]);
                this.hidOldStarts.Value = Convert.ToString(dt.Rows[0]["Starts"]);
                this.ddlLevels.Value = Convert.ToInt32(dt.Rows[0]["IdAggregationLevel"]);
                this.ddlTypeOperation.Value = Convert.ToInt32(dt.Rows[0]["IdTypeOperation"]);
            }
        }

        protected void StoreOperation_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsTypesOperation objTypesOperation = new DataClass.clsTypesOperation();
            this.storeOperation.DataSource = objTypesOperation.LoadList("IdModel = " + IdModel + " AND LastNode = 'True'", "ORDER BY Name");
            this.storeOperation.DataBind();
        }

        protected void StoreLevel_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsAggregationLevels objAggregationLevels = new DataClass.clsAggregationLevels();
            this.storeLevels.DataSource = objAggregationLevels.LoadList("IdModel = " + IdModel, "ORDER BY Name");
            this.storeLevels.DataBind();
        }

        public void Delete()
        {
            DataClass.clsFields objFields = new DataClass.clsFields();
            objFields.IdField = (Int32)IdField;
            objFields.Delete();
            this.ModelUpdateTimestamp();
        }

        public void DeleteAll()
        {
            DataClass.clsFields objFields = new DataClass.clsFields();
            objFields.IdModel = (Int32)IdModel;
            objFields.DeleteAll();
            this.ModelUpdateTimestamp();
        }

        [DirectMethod]
        public void ClickedDeleteYES()
        {
            this.Delete();
            this.LoadFields();
        }

        [DirectMethod]
        public void ClickedDeleteAllYES()
        {
            this.DeleteAll();
            this.LoadFields();
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dt = objFields.LoadList("IdModel = " + IdModel + " AND Code = '" + txtCode.Text + "'", "");
            Value = dt.Rows.Count;

            return Value;
        }

        protected void LoadFields()
        {
            DataClass.clsFields objFields = new DataClass.clsFields();

            DataTable dt = objFields.LoadList("IdModel = " + IdModel, "ORDER BY Name");

            storeFields.DataSource = dt;
            storeFields.DataBind();

            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Code", "Field Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Name", "Field Name"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("AggregationCode", "Aggregation Level"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("TypeOperation", "Type of Operation"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Starts", "Year Starts"));
            String strSelectedParameters = "";
            String[] columnsToCopy = { "Code", "Name", "AggregationCode", "TypeOperation", "Starts" };
            System.Data.DataView view = new System.Data.DataView(dt);
            DataTable dtTechDrivers = view.ToTable(true, columnsToCopy);
            ExportTechnicaldrivers = new DataReportObject(dtTechDrivers, "DATASHEET", null, strSelectedParameters, null, "Code", datColumnTD, null);
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExportTD = new DataReportObject();
            StoredExportTD = ExportTechnicaldrivers;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExportTD);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=1.2 ExportFields.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        public String MessageConfirmDelete(String itemName)
        {
            String messageConfirmDeleteReturn = "";
            if (!String.IsNullOrEmpty(itemName))
            {
                messageConfirmDeleteReturn = (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + itemName + "?";
            }
            else
            {
                messageConfirmDeleteReturn = (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete all records?");
            }
            messageConfirmDeleteReturn += "<br><br><b>" + (String)GetGlobalResourceObject("CPM_Resources", "This will also delete any related data from the following") + ":</b><br>"
                    + "<br> - Projects";
            return messageConfirmDeleteReturn;
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportFields.xls", "template");
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        protected void LoadLabel()
        {
            this.grdFields.Title = (String)GetGlobalResourceObject("CPM_Resources", "Fields");
            this.btnAdd.Text = (String)GetGlobalResourceObject("CPM_Resources", "Add New Field");
            this.btnAdd.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Add New Field");
            this.btnDelete.Text = modMain.strCommonDeleteAll;
            this.btnDelete.ToolTip = modMain.strCommonDeleteAll;
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.Code.Text = modMain.strCommonCode;
            this.Name.Text = modMain.strCommonName;
            this.colAggregationLevel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Aggregation Level");
            this.TypeOperation.Text = (String)GetGlobalResourceObject("CPM_Resources", "Type of Operation");
            this.colStarts.Text = (String)GetGlobalResourceObject("CPM_Resources", "Base Year");
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = "&nbsp;" + modMain.strCommonEdit;
            this.ImageCommandColumn1.Commands[1].Text = "&nbsp;" + modMain.strCommonDelete;
            this.winFieldsEdit.Title = modMain.strTechnicalModuleField.Remove(modMain.strTechnicalModuleField.Length - 1, 1);
            this.txtCode.FieldLabel = modMain.strCommonCode;
            this.txtName.FieldLabel = modMain.strCommonName;
            this.ddlLevels.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Aggregation Level");
            this.ddlTypeOperation.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Type of Operation");
            this.txtStarts.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Starts");
            this.btnSave.Text = modMain.strCommonSave;
            this.btnSave.ToolTip = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.btnCancel.ToolTip = modMain.strCommonClose;
            this.winImport.Title = modMain.strCommonImport + " " + (String)GetGlobalResourceObject("CPM_Resources", "Fields"); 
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnImportFile.ToolTip = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.btnCancelImport.ToolTip = modMain.strCommonClose;
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        #endregion

    }
}
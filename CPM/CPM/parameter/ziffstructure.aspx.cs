﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _ziffstructure : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdZiffAccount { get { return (Int32)Session["IdZiffAccount"]; } set { Session["IdZiffAccount"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private DataReportObject ExportZiffCostCategories { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Server.ScriptTimeout = 3600000;
            if (!X.IsAjaxRequest)
            {
                IdZiffAccount = 0;
                this.LoadZiffAccount();
                this.LoadLabel();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winZiffStructureEdit.Show();
        }

        protected void btnAddDefaults_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do add the standard Ziff Account records?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedAddDefaultYES()",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnDeleteAll_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, MessageConfirmDelete(null), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDeleteAllYES()",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnExport_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Alert("Message", (String)GetGlobalResourceObject("CPM_Resources", "Under Construction"));
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.Save();
                this.LoadZiffAccount();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            this.winZiffStructureEdit.Hide();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnCancelExport_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Alert("Message", (String)GetGlobalResourceObject("CPM_Resources", "Under Construction"));
        }

        protected void StoreParent_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsZiffAccount objZiffAccount = new DataClass.clsZiffAccount();

            DataTable dt = objZiffAccount.LoadList("IdModel = " + IdModel + " AND IdZiffAccount<>" + IdZiffAccount, "ORDER BY Name");
            DataRow dr = dt.NewRow();
            dr["IdZiffAccount"] = 0;
            dr["Name"] = "- None -";
            dt.Rows.Add(dr);
            dt.AcceptChanges();
            this.storeParent.DataSource = dt;
            this.storeParent.DataBind();
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.ZiffStructure);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    // Sort to handle placing parents and subs in proper order
                    System.Data.DataView dvImportTable = objReader.ImportTable.DefaultView;
                    dvImportTable.Sort = "Level";
                    DataTable dtSortedImportTable = dvImportTable.ToTable();
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    foreach (DataRow row in dtSortedImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Category Code"].ToString(), row["Category Desc"].ToString(), row["Parent Category Code"].ToString());
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadZiffAccount();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        #endregion

        #region Methods

        protected void Clear()
        {
            this.IdZiffAccount = 0;
            this.txtName.Text = "";
            this.txtName.Reset();
            this.txtCode.Text = "";
            this.txtCode.Reset();
            this.ddlParent.Reset();
            this.storeParent.Reload();
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void grdZiffStructure_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            IdZiffAccount = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            if (Command == "Edit")
            {
                this.winZiffStructureEdit.Show();
                EditZiffAccountLoad();
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, MessageConfirmDelete(_Name), new Ext.Net.MessageBoxButtonsConfig
                    {
                        Yes = new MessageBoxButtonConfig
                        {
                            Handler = "UsersX.ClickedDeleteYES()",
                            Text = modMain.strCommonYes
                        },
                        No = new MessageBoxButtonConfig
                        {
                            Text = modMain.strCommonNo
                        }
                    }).Show();
            }
        }

        protected void LoadZiffAccount()
        {
            DataClass.clsZiffAccount objZiffAccount = new DataClass.clsZiffAccount();

            DataTable dt = objZiffAccount.LoadList("IdModel = " + IdModel, "ORDER BY SortOrder");

            storeZiffStructure.DataSource = dt;
            storeZiffStructure.DataBind();

            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Name", "Category Desc"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Code", "Category Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("RelationLevel", "Level"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("ParentCategoryCode", "Parent Category Code"));
            String strSelectedParameters = "";
            String[] columnsToCopy = { "Code", "Name", "RelationLevel", "ParentCategoryCode" };
            System.Data.DataView view = new System.Data.DataView(dt);

            DataTable dtZiffCostCategories = view.ToTable(true, columnsToCopy);
            DataTable dtCloned = dtZiffCostCategories.Clone();
            dtCloned.Columns["RelationLevel"].DataType = typeof(String);
            foreach (DataRow row in dtZiffCostCategories.Rows)
            {
                dtCloned.ImportRow(row);
            }

            //Need to modify RelationLevel data where it equal 2 make it 1.1
            foreach(DataRow row in dtCloned.Rows)
            {
                string oldX = row.Field<String>("RelationLevel");
                string newX = "2".Equals(oldX, StringComparison.OrdinalIgnoreCase) ? "1.1" : row["RelationLevel"].ToString();
                row.SetField("RelationLevel", newX);
            }
            dtZiffCostCategories = dtCloned;
            ExportZiffCostCategories = new DataReportObject(dtZiffCostCategories, "DATASHEET", null, strSelectedParameters, null, "Name", datColumnTD, null);

            if (dt.Rows.Count == 0)
            {
                this.sepAddDefault.Hidden = false;
                this.btnAddDefault.Hidden = false;
            }
            else
            {
                this.sepAddDefault.Hidden = true;
                this.btnAddDefault.Hidden = true;
            }
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExport = new DataReportObject();
            StoredExport = ExportZiffCostCategories;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExport);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=1.8 ExportCostProjectionCategories.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }
        protected void EditZiffAccountLoad()
        {
            DataClass.clsZiffAccount objZiffAccount = new DataClass.clsZiffAccount();
            DataTable dt = objZiffAccount.LoadList("IdZiffAccount = " + IdZiffAccount, "");
            if (dt.Rows.Count > 0)
            {
                this.txtCode.Text = (String)dt.Rows[0]["Code"];
                this.txtName.Text = (String)dt.Rows[0]["Name"];
                if (dt.Rows[0]["Parent"] == null || dt.Rows[0]["Parent"].ToString().Trim() == "" || dt.Rows[0]["Parent"].ToString() == "0")
                    this.ddlParent.Value = 0;
                else
                    this.ddlParent.SetValue((Int32)dt.Rows[0]["Parent"]);
            }
        }

        public void Delete()
        {
            DataClass.clsZiffAccount objZiffAccount = new DataClass.clsZiffAccount();
            objZiffAccount.IdZiffAccount = IdZiffAccount;
            objZiffAccount.DeleteSubAccount();
            objZiffAccount.Delete();
            this.ModelUpdateTimestamp();
        }

        public void DeleteAll()
        {
            DataClass.clsZiffAccount objZiffAccount = new DataClass.clsZiffAccount();
            objZiffAccount.IdModel = (Int32)IdModel;
            objZiffAccount.DeleteAll();
            this.ModelUpdateTimestamp();
        }

        public void AddDefault()
        {
            DataClass.clsZiffAccount objZiffAccount = new DataClass.clsZiffAccount();
            objZiffAccount.IdModel = (Int32)IdModel;
            objZiffAccount.UserCreation = (Int32)IdUserCreate;
            objZiffAccount.InsertDefault();
            this.ModelUpdateTimestamp();
        }

        protected void Save()
        {
            DataClass.clsZiffAccount objZiffAccount = new DataClass.clsZiffAccount();
            objZiffAccount.IdZiffAccount = IdZiffAccount;
            objZiffAccount.loadObject();

            if (IdZiffAccount == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (objZiffAccount.Code != txtCode.Text)
                {
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }
            objZiffAccount.IdModel = IdModel;
            objZiffAccount.Code = txtCode.Text;
            objZiffAccount.Name = txtName.Text;
            objZiffAccount.Parent = Convert.ToInt32(ddlParent.Value);

            if (IdZiffAccount == 0)
            {
                objZiffAccount.DateCreation = DateTime.Now;
                objZiffAccount.UserCreation = (Int32)IdUserCreate;
                IdZiffAccount = objZiffAccount.Insert();
            }
            else
            {
                objZiffAccount.DateModification = DateTime.Now;
                objZiffAccount.UserModification = (Int32)IdUserCreate;
                objZiffAccount.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winZiffStructureEdit.Hide();
            this.ModelUpdateTimestamp();
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsZiffAccount objZiffAccount = new DataClass.clsZiffAccount();
            DataTable dt = objZiffAccount.LoadList("IdModel = " + IdModel + " AND Code = '" + txtCode.Text + "'", "");
            Value = dt.Rows.Count;

            return Value;
        }

        [DirectMethod]
        public void ClickedDeleteYES()
        {
            this.Delete();
            this.LoadZiffAccount();
        }

        [DirectMethod]
        public void ClickedDeleteAllYES()
        {
            this.DeleteAll();
            X.Redirect("/parameter/ziffstructure.aspx");
        }

        [DirectMethod]
        public void ClickedAddDefaultYES()
        {
            this.AddDefault();
            X.Redirect("/parameter/ziffstructure.aspx");
        }

        protected void SaveImport(String _Code, String _Name, String _Parent)
        {
            Int32 _IdParent = GetIdParent(_Parent);

            if (!String.IsNullOrEmpty(_Code) && !String.IsNullOrEmpty(_Name))
            {
                DataClass.clsZiffAccount objZiffAccount = new DataClass.clsZiffAccount();
                objZiffAccount.IdZiffAccount = (Int32)IdZiffAccount;
                objZiffAccount.loadObject();
                objZiffAccount.Code = _Code;
                objZiffAccount.Name = _Name;
                if (_IdParent != 0)
                {
                    objZiffAccount.Parent = _IdParent;
                }
                objZiffAccount.IdModel = (Int32)IdModel;
                IdZiffAccount = objZiffAccount.Insert();
                objZiffAccount = null;
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected Int32 GetIdParent(String _Parent)
        {
            Int32 Value = 0;
            DataClass.clsZiffAccount objZiffAccount = new DataClass.clsZiffAccount();
            DataTable dt = objZiffAccount.LoadList("Code = '" + _Parent + "' AND IdModel = " + IdModel, "");

            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdZiffAccount"];
            }
            return Value;
        }

        public String MessageConfirmDelete(String itemName)
        {
            String messageConfirmDeleteReturn = "";
            if (!String.IsNullOrEmpty(itemName))
            {
                messageConfirmDeleteReturn = (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + itemName + "?";
            }
            else
            {
                messageConfirmDeleteReturn = (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete all records?");
            }
            messageConfirmDeleteReturn += "<br><br><b>" + (String)GetGlobalResourceObject("CPM_Resources", "This will also delete any related data from the following") + ":</b><br>"
                    + "<br> - " + (String)GetGlobalResourceObject("CPM_Resources", "MappingZiff")
                    + "<br> - " + modMain.strMasterPeerCriteriaData
                    + "<br> - " + (String)GetGlobalResourceObject("CPM_Resources", "Ziff Base Cost")
                    + "<br> - " + modMain.strMasterTechnicalAssumptions
                    + "<br> - " + modMain.strMasterEconomicAssumptions;
            return messageConfirmDeleteReturn;
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportCostProjectionCategories.xls", "template");
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
            
            // Update Calculated Columns
            clsZiffAccount objZiffAccount = new clsZiffAccount();
            objZiffAccount.IdModel = IdModel;
            objZiffAccount.UpdateZiffAccountRelationLevel(objApplication.MaxRelationLevels);
            objZiffAccount = null;
        }

        protected void LoadLabel()
        {
            this.grdZiffStructure.Title = modMain.strMasterZiff_Third_partyCostCategories;
            this.btnAdd.Text = (String)GetGlobalResourceObject("CPM_Resources", "Add New Category");
            this.btnAdd.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Add New Category");
            this.btnDelete.Text = modMain.strCommonDeleteAll;
            this.btnDelete.ToolTip = modMain.strCommonDeleteAll;
            this.btnAddDefault.Text = (String)GetGlobalResourceObject("CPM_Resources", "AddZiffDefaults");
            this.btnAddDefault.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "AddZiffDefaults");
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.colCode.Text = modMain.strCommonCode;
            this.colName.Text = modMain.strCommonName;
            this.Column1.Text = (String)GetGlobalResourceObject("CPM_Resources", "Parent Name");
            this.colThirParty.Text = (String)GetGlobalResourceObject("CPM_Resources", "Third-party");
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = "&nbsp;" + modMain.strCommonEdit;
            this.ImageCommandColumn1.Commands[1].Text = "&nbsp;" + modMain.strCommonDelete;
            this.winZiffStructureEdit.Title = (String)GetGlobalResourceObject("CPM_Resources", "Third-party Cost Category");
            this.txtCode.FieldLabel = modMain.strCommonCode;
            this.txtName.FieldLabel = modMain.strCommonName;
            this.ddlParent.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "ParentAccount");
            this.btnSave.Text = modMain.strCommonSave;
            this.btnSave.ToolTip = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.btnCancel.ToolTip = modMain.strCommonClose;
            this.winImport.Title = modMain.strCommonImport + " " + (String)GetGlobalResourceObject("CPM_Resources", "Third-party Cost Category");
            this.fileImport.Text = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnImportFile.ToolTip = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.btnCancelImport.ToolTip = modMain.strCommonClose;
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
        }

        #endregion

    }
}
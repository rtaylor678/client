﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="currencies.aspx.cs" Inherits="CPM._currencies" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head2" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var imageRenderer = function (value) {
            if (value.toString() == 'true') {
                return "<img src='/image/star.png' border='0'>";
            }
            else {
                return "";
            }
        };
    </script>
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" Layout="FitLayout" AutoScroll="true" runat="server">
                <Items>
                    <ext:GridPanel ID="grdCurrencies" runat="server" Border="false" Title="Exchange Rates" IconCls="icon-currency_16">
                        <TopBar>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                    <ext:Button ID="btnAdd" Icon="Add" ToolTip="Add New Currency" runat="server" Text="Add New Currency">
                                        <DirectEvents>
                                            <Click OnEvent="btnAdd_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarSeparator />
                                    <ext:Button ID="btnDelete" Icon="Delete" ToolTip="Delete All" runat="server" Text="Delete All">
                                        <DirectEvents>
                                            <Click OnEvent="btnDeleteAll_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarSeparator />
                                    <ext:Button ID="btnSaveChange" Icon="DatabaseSave" ToolTip="Save Changes" runat="server" Text="Save Changes">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveChanges_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="rowsValues" Value="#{grdCurrencies}.getRowsValues(false)" Mode="Raw"
                                                        Encode="true" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarSeparator />
                                    <ext:Button ID="btnCurrencyExchange" Icon="Money" ToolTip="Change Currency Exchange" runat="server" Text="Currency Exchange">
                                        <DirectEvents>
                                            <Click OnEvent="btnEditCurrency_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarSeparator />
                                    <ext:Button ID="btnChangeBase" Icon="MoneyDollar" ToolTip="Change Base Currency" runat="server" Text="Base Currency">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveBase_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarSeparator />
                                    <ext:Button ID="btnImport" Icon="BookAdd" runat="server" Text="Import">
                                        <DirectEvents>
                                            <Click OnEvent="btnImport_Click">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:ToolbarFill />
                                    <ext:Button ID="btnSaveExcel" runat="server" Text="Export Data To Excel" Icon="PageExcel">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveExcel_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnDownloadTemplate" runat="server" Text="Download Import Template" Icon="PageSave">
                                        <DirectEvents>
                                            <Click OnEvent="btnDownloadTemplate_Click" IsUpload="true">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>

                                </Items>
                            </ext:Toolbar>
                        </TopBar>
                        <Store>
                            <ext:Store ID="storeCurrencies" runat="server">
                                <Model>
                                    <ext:Model ID="Model1" runat="server" IDProperty="IdCurrency">
                                        <Fields>
                                            <ext:ModelField Name="IdModelCurrency" Type="Int" />
                                            <ext:ModelField Name="IdCurrency" Type="Int" />
                                            <ext:ModelField Name="Code" Type="String" />
                                            <ext:ModelField Name="Name" Type="String" />
                                            <ext:ModelField Name="Sign" Type="String" />
                                            <ext:ModelField Name="Value" Type="Float" />
                                            <ext:ModelField Name="Output" Type="Boolean" />
                                            <ext:ModelField Name="BaseCurrency" Type="Boolean" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel ID="ColumnModel1" runat="server">
                            <Columns>
                                <ext:Column ID="Code" runat="server" Text="Code" DataIndex="Code" Flex="1" />
                                <ext:Column ID="Column1" Resizable="false" MenuDisabled="true" Width="22" runat="server" DataIndex="BaseCurrency">
                                    <Renderer Fn="imageRenderer" />
                                </ext:Column>
                                <ext:Column ID="Name" runat="server" Text="Name" DataIndex="Name" Flex="1" />
                                <ext:Column ID="Sign" runat="server" Text="Symbol" DataIndex="Sign" Flex="1" Align="Center" />
                                <ext:NumberColumn ID="Value" Align="Center" Format="0.000000000" runat="server" Text="Value" DataIndex="Value" Flex="1" />
                                <ext:ComponentColumn ID="chkOutput" runat="server" Editor="true" Align="Center" DataIndex="Output" Flex="1" Text="Output">
                                    <Component>
                                        <ext:Checkbox ID="Checkbox1" Checked="true" runat="server" />
                                    </Component>
                                </ext:ComponentColumn>
                                <ext:ImageCommandColumn Align="Center" Width="150" Text="Functions" ID="ImageCommandColumn1" runat="server">
                                    <%--<Commands>
                                        <ext:ImageCommand CommandName="Edit" IconCls="icon-edit_16" Text="&nbsp;Edit" />
                                    </Commands>--%>
                                    <Commands>
                                        <ext:ImageCommand CommandName="Delete" IconCls="icon-delete_16" Text="&nbsp;Delete" />
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="grdFields_Command">
                                            <ExtraParams>
                                                <ext:Parameter Name="command" Value="command" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="Id" Value="record.data.IdModelCurrency" Mode="Raw">
                                                </ext:Parameter>
                                                <ext:Parameter Name="Name" Value="record.data.Name" Mode="Raw">
                                                </ext:Parameter>
                                            </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:ImageCommandColumn>
                            </Columns>
                        </ColumnModel>
                        <SelectionModel>
                            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" />
                        </SelectionModel>
                        <Features>
                            <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                                <Filters>
                                    <ext:StringFilter DataIndex="Name" />
                                    <ext:StringFilter DataIndex="Code" />
                                </Filters>
                            </ext:GridFilters>
                        </Features>
                    </ext:GridPanel>
                    <ext:Window ID="winCurrencyEdit" runat="server" Title="Currency Exchange" Hidden="true" IconCls="icon-currency_16" Resizable="true" Width="600" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="pnlCurrenciesEdit" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                                <Items>
                                    <ext:Panel ID="Panel1" runat="server" Border="false" Header="false" ColumnWidth=".5" Layout="Form" LabelAlign="Top" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:Label ID="lblBaseCurrency" Disabled="false" runat="server" FieldLabel="Base Currency" AnchorHorizontal="92%" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel2" runat="server" Border="false" Header="false" ColumnWidth=".5" Layout="Form" LabelAlign="Top" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:Label ID="lblValueBaseCurrency" Disabled="false" runat="server" FieldLabel="" AnchorHorizontal="92%" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel3" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" LabelAlign="Top" Cls="PopupFormColumnPanelGrid">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:GridPanel ID="grdExchange" runat="server" Height="120">
                                                <Store>
                                                    <ext:Store ID="storeExchange" runat="server">
                                                        <Model>
                                                            <ext:Model ID="modelExchange" runat="server" IDProperty="IdModelCurrency">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdModelCurrency" Type="Int" />
                                                                    <ext:ModelField Name="Name" Type="String" />
                                                                    <ext:ModelField Name="Code" Type="String" />
                                                                    <ext:ModelField Name="Value" Type="Float" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                    </ext:Store>
                                                </Store>
                                                <ColumnModel ID="cmExchange" runat="server">
                                                    <Columns>
                                                        <ext:Column Hidden="true" ID="colIdModelCurrency" runat="server" Text="ID" DataIndex="IdModelCurrency" />
                                                        <ext:Column ID="colCode" runat="server" Text="Code" DataIndex="Code" Flex="1" />
                                                        <ext:Column ID="colName" runat="server" Text="Name" DataIndex="Name" Flex="1" />
                                                        <ext:NumberColumn ID="colValue" Align="Center" Format="0.000000000" runat="server" Text="Value" DataIndex="Value">
                                                            <Editor>
                                                                <ext:NumberField DecimalPrecision="9" MinValue="0" ID="numValue" runat="server" />
                                                            </Editor>
                                                        </ext:NumberColumn>
                                                    </Columns>
                                                </ColumnModel>
                                                <Features>
                                                    <ext:GridFilters runat="server" ID="GridFilters2" Local="true">
                                                        <Filters>
                                                            <ext:StringFilter DataIndex="Code" />
                                                            <ext:StringFilter DataIndex="Name" />
                                                            <ext:NumericFilter DataIndex="Value" />
                                                        </Filters>
                                                    </ext:GridFilters>
                                                </Features>
                                                <SelectionModel>
                                                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" />
                                                </SelectionModel>
                                                <Plugins>
                                                    <ext:CellEditing ID="CellEditing1" runat="server">
                                                    </ext:CellEditing>
                                                </Plugins>
                                                <View>
                                                    <ext:GridView MarkDirty="false" />
                                                </View>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnUpdate" runat="server" Icon="ArrowRefresh" Text="Update" Visible="false">
                                        <DirectEvents>
                                            <Click OnEvent="btnUpdateCurrency_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="rowsValues" Value="#{grdExchange}.getRowsValues(false)" Mode="Raw"
                                                        Encode="true" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnSave" runat="server" Text="Save" Icon="DatabaseSave" Disabled="true" FormBind="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnSave_Click">
                                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="rowsValues" Value="#{grdExchange}.getRowsValues(false)" Mode="Raw"
                                                        Encode="true" />
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnCancel" runat="server" Icon="Decline" Text="Close">
                                        <DirectEvents>
                                            <Click OnEvent="btnCancel_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                                <BottomBar>
                                    <ext:StatusBar ID="FormStatusBar" runat="server" />
                                </BottomBar>
                                <Listeners>
                                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                            });
                                            #{btnSave}.setDisabled(!valid);" />
                                </Listeners>
                            </ext:FormPanel>
                        </Items>
                    </ext:Window>
                    <ext:Window ID="winBaseCurrency" runat="server" Title="Change Base Currency" Hidden="true" IconCls="icon-currency_16" Resizable="false" Width="400" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="FormPanel1" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                                <Items>
                                    <ext:Panel ID="Panel4" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" LabelAlign="Top" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:ComboBox ID="ddlCurrency" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" FieldLabel="Base Currency" ValueField="IdCurrency" Cls="PopupFormField">
                                                <ListConfig LoadingText="Searching...">
                                                    <ItemTpl ID="ItemTpl4" runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    <h3>{Name}</h3>
							                                    <span>Code: {Code}</span>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                <Store>
                                                    <ext:Store ID="storeBase" runat="server" OnReadData="StoreCurr_ReadData">
                                                        <Model>
                                                            <ext:Model ID="Model4" runat="server" IDProperty="IdCurrency">
                                                                <Fields>
                                                                    <ext:ModelField Name="IdCurrency" />
                                                                    <ext:ModelField Name="Name" />
                                                                    <ext:ModelField Name="Code" />
                                                                </Fields>
                                                            </ext:Model>
                                                        </Model>
                                                        <Proxy>
                                                            <ext:PageProxy>
                                                                <Reader>
                                                                    <ext:JsonReader />
                                                                </Reader>
                                                            </ext:PageProxy>
                                                        </Proxy>
                                                    </ext:Store>
                                                </Store>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Panel>
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnSaveBaseCurrency" runat="server" Text="Change" Icon="MoneyAdd" Selectable="true" Disabled="true" FormBind="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnUpdateBase_Click">
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnCancelBase" runat="server" Icon="Decline" Text="Close">
                                        <DirectEvents>
                                            <Click OnEvent="btnCancelBase_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                                <BottomBar>
                                    <ext:StatusBar ID="FormBaseStatusBar" runat="server" />
                                </BottomBar>
                                <Listeners>
                                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                    text : valid ? 'Form is valid' : 'Form is invalid', 
                                                    iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                });
                                                #{btnSaveBaseCurrency}.setDisabled(!valid);" />
                                </Listeners>
                            </ext:FormPanel>
                        </Items>
                    </ext:Window>
                    <ext:Window ID="winImport" runat="server" Title="Import Currencies" Hidden="true" Icon="ArrowLeft" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="frmImportFile" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                                <Items>
                                    <ext:Panel ID="pnlImportFile1" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:FileUploadField ID="fileImport" FieldLabel="File" runat="server" Icon="Attach" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnImportFile" runat="server" Text="Import" Icon="TableRefresh" Disabled="true" FormBind="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnImportFile_Click" >
                                                <EventMask ShowMask="true" Target="CustomTarget" CustomTarget="vpMain" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnCancelImport" runat="server" Icon="Decline" Text="Close">
                                        <DirectEvents>
                                            <Click OnEvent="btnCancelImport_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                                <BottomBar>
                                    <ext:StatusBar ID="FormImportStatusBar" runat="server" />
                                </BottomBar>
                                <Listeners>
                                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                                text : valid ? 'Form is valid' : 'Form is invalid', 
                                                                iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                            });
                                                            #{btnImportFile}.setDisabled(!valid);" />
                                </Listeners>
                            </ext:FormPanel>
                        </Items>
                    </ext:Window>
                    <ext:Window ID="winCurrencyAdd" runat="server" Title="Add Currency" Hidden="true" IconCls="icon-ziffstructure_16" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                        <Items>
                            <ext:FormPanel ID="pnlAddCurrency" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                                <Items>
                                    <ext:Panel ID="Panel5" runat="server" Border="false" Layout="Form" ColumnWidth=".5" LabelAlign="Top" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:TextField ID="txtCode" runat="server" FieldLabel="Code" AnchorHorizontal="80%" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel6" runat="server" Border="false" Layout="Form" ColumnWidth=".5" LabelAlign="Top" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:TextField ID="txtName" runat="server" FieldLabel="Name" AnchorHorizontal="80%" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel7" runat="server" Border="false" Header="false" ColumnWidth=".5" Layout="Form" LabelAlign="Top" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="true" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:TextField ID="txtSymbol" runat="server" FieldLabel="Symbol" AnchorHorizontal="80%" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel8" runat="server" Border="false" Header="false" ColumnWidth=".5" Layout="Form" LabelAlign="Top" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="true" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:TextField ID="txtValue" runat="server" FieldLabel="Value" AnchorHorizontal="80%" Cls="PopupFormField" />
                                        </Items>
                                    </ext:Panel>
                                    <ext:Panel ID="Panel9" runat="server" Border="false" Header="false" ColumnWidth=".5" Layout="Form" LabelAlign="Top" Cls="PopupFormColumnPanel">
                                        <Defaults>
                                            <ext:Parameter Name="AllowBlank" Value="true" Mode="Raw" />
                                            <ext:Parameter Name="MsgTarget" Value="side" />
                                        </Defaults>
                                        <Items>
                                            <ext:Checkbox ID="chkOutputAdd" runat="server" FieldLabel="Output" Cls="PopupFormField" ReadOnly="False" />
                                        </Items>
                                    </ext:Panel>
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnSaveAdd" runat="server" Text="Save" Icon="DatabaseSave" Disabled="true" FormBind="true">
                                        <DirectEvents>
                                            <Click OnEvent="btnSaveAdd_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button ID="btnCancelAdd" runat="server" Icon="Decline" Text="Close">
                                        <DirectEvents>
                                            <Click OnEvent="btnCancelAdd_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                                <BottomBar>
                                    <ext:StatusBar ID="StatusBar1" runat="server" />
                                </BottomBar>
                                <Listeners>
                                    <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                                    text : valid ? 'Form is valid' : 'Form is invalid', 
                                                                    iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                                });
                                                                #{btnSave}.setDisabled(!valid);" />
                                </Listeners>
                            </ext:FormPanel>
                        </Items>
                    </ext:Window>
                </Items>
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>

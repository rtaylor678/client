﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="typesoperation.aspx.cs" Inherits="CPM._typesoperation" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" Layout="FitLayout" AutoScroll="true" runat="server">
                <Items>
                    <ext:Panel ID="pnlOperation" runat="server" Title="Types of Operation" IconCls="icon-operations_16" Cls="Backgorund" BodyPadding="0" AutoScroll="true" Border="false" Layout="Column" LabelAlign="Top">
                        <Items>
                            <ext:TreePanel ID="treeOperation" runat="server" Collapsible="true" UseArrows="true" Animate="true" Draggable="false" Width="750" Header="false" Border="false" RootVisible="false" OnSubmit="SubmitNodes" MultiSelect="true" FolderSort="true">
                                <Fields>
                                    <ext:ModelField Name="NodeLocked" Type="String" />
                                </Fields>
                                <TopBar>
                                    <ext:Toolbar ID="Toolbar1" runat="server">
                                        <Items>
                                            <ext:Button ID="btnSave" Icon="DatabaseSave" runat="server" Text="Save Changes">
                                                <Listeners>
                                                    <Click Handler="#{treeOperation}.submitNodes();" />
                                                </Listeners>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                                <Listeners>
                                    <CheckChange Handler="var node = Ext.get(this.getView().getNode(item));
                                                          node[checked ? 'addCls' : 'removeCls']('complete')" />
                                    <Render Handler="this.getRootNode().expand(true);" Delay="50" />
                                </Listeners>
                            </ext:TreePanel>
                        </Items>
                    </ext:Panel>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="FormStatusBar" Text="" runat="server" />
                </BottomBar>    
            </ext:Panel>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="aggregationlevels.aspx.cs" Inherits="CPM._aggregationlevels" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" />
    <link href="/_theme/blue/css/blue_icon.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmMaster" runat="server">
    <ext:ResourceManager ID="rscManager" runat="server" DirectMethodNamespace="UsersX" />
    <ext:Viewport Resizable="false" ID="vpMain" runat="server" StyleSpec="background-color: transparent;" Layout="FitLayout">
        <Items>
            <ext:Panel ID="pnlBody" AutoScroll="true" runat="server" Title="Aggregation Levels" IconCls="icon-aggregation_16" Border="false">
                <Items>
                    <ext:Panel ID="pnlFilter" runat="server" Layout="HBoxLayout" Border="false" Style="margin: 0 0 0 0">
                        <Items>
                            <ext:Label Style="margin: 10px 10px 10px 10px" ID="lbFilter" runat="server" Text="Filter:"></ext:Label>
                            <ext:TextField Style="margin: 10px 10px 10px 10px" Width="200" ID="txtFind" runat="server"></ext:TextField>
                            <ext:Button Style="margin: 10px 10px 10px 10px" ID="btnFind" TextAlign="Center" Icon="Zoom" Text="Find" runat="server">
                                <DirectEvents>
                                    <Click OnEvent="btnFind_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button Style="margin: 10px 10px 10px 10px" ID="tbnClear" TextAlign="Center" Icon="ZoomOut" Text="Clear" runat="server">
                                <DirectEvents>
                                    <Click OnEvent="btnClear_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:Panel>
                    <ext:Panel ID="pnlLevels" runat="server" AutoScroll="true" Border="false">
                        <Items>
                            <ext:TreePanel ID="treeAggregationLevels" runat="server" Width="850" Collapsible="true" AutoScroll="true" UseArrows="true" Animate="true" Draggable="false" Header="false" Border="false" RootVisible="false" MultiSelect="true" FolderSort="true">
                                <Store>
                                    <ext:TreeStore ID="storeAggregationLevels" runat="server" OnReadData="StoreAggregationLevels_ReadData">
                                        <Proxy>
                                            <ext:PageProxy>
                                            </ext:PageProxy>
                                        </Proxy>
                                    </ext:TreeStore>
                                </Store>
                                <TopBar>
                                    <ext:Toolbar ID="Toolbar1" runat="server">
                                        <Items>
                                            <ext:Button ID="btnNew" Icon="Add" runat="server" Text="Add New Aggregation">
                                                <DirectEvents>
                                                    <Click OnEvent="btnAdd_Click" />
                                                </DirectEvents>
                                            </ext:Button>
                                            <ext:ToolbarSeparator />
                                            <ext:Button ID="btnDelete" Icon="Delete" ToolTip="Delete All" runat="server" Text="Delete All">
                                                <DirectEvents>
                                                    <Click OnEvent="btnDeleteAll_Click">
                                                        <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                            <ext:ToolbarSeparator />
                                            <ext:Button ID="btnImport" Icon="BookAdd" runat="server" Text="Import">
                                                <DirectEvents>
                                                    <Click OnEvent="btnImport_Click" />
                                                </DirectEvents>
                                            </ext:Button>
                                            <ext:ToolbarFill />
                                            <ext:Button ID="btnSaveExcel" runat="server" Text="Export Data To Excel" Icon="PageExcel">
                                                <DirectEvents>
                                                    <Click OnEvent="btnSaveExcel_Click" IsUpload="true">
                                                    </Click>
                                                </DirectEvents>
                                            </ext:Button>
                                            <ext:Button ID="btnDownloadTemplate" runat="server" Text="Download Import Template" Icon="PageSave">
                                                <DirectEvents>
                                                    <Click OnEvent="btnDownloadTemplate_Click" IsUpload="true" />
                                                </DirectEvents>
                                            </ext:Button>
                                            <ext:ToolbarFill />
                                            <ext:ToolbarSeparator />
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                                <Root>
                                    <ext:Node NodeID="IdRoot" Text="Root">
                                    </ext:Node>
                                </Root>
                                <Listeners>
                                    <Render Handler="this.getRootNode().expand(true);" Delay="50" />
                                    <Load Handler="this.getRootNode().expand(true);" Delay="50" />
                                </Listeners>
                                <ColumnModel>
                                    <Columns>
                                        <ext:TreeColumn ID="TreeColumn1" runat="server" Flex="1" DataIndex="text" />
                                        <ext:CommandColumn Align="Center" ID="CommandColumn1" Resizable="false" runat="server" Width="50">
                                            <Commands>
                                                <ext:GridCommand CommandName="Edit" IconCls="icon-edit_16" Text="Edit" ToolTip-Text="Edit">
                                                    <ToolTip Text="Edit" />
                                                </ext:GridCommand>
                                            </Commands>
                                            <PrepareToolbar Handler="return record.data.id;" />
                                            <DirectEvents>
                                                <Command OnEvent="grdLevels_Command">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw" />
                                                        <ext:Parameter Name="Name" Value="record.data.text" Mode="Raw" />
                                                        <ext:Parameter Name="Id" Value="record.data.id" Mode="Raw" />
                                                    </ExtraParams>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                        <ext:CommandColumn Align="Center" ID="CommandColumn2" Resizable="false" runat="server" Width="60">
                                            <Commands>
                                                <ext:GridCommand CommandName="Delete" IconCls="icon-delete_16" Text="Delete" ToolTip-Text="Delete">
                                                    <ToolTip Text="Delete" />
                                                </ext:GridCommand>
                                            </Commands>
                                            <PrepareToolbar Handler="return record.data.leaf;" />
                                            <DirectEvents>
                                                <Command OnEvent="grdLevels_Command">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="command" Value="command" Mode="Raw" />
                                                        <ext:Parameter Name="Name" Value="record.data.text" Mode="Raw" />
                                                        <ext:Parameter Name="Id" Value="record.data.id" Mode="Raw" />
                                                    </ExtraParams>
                                                </Command>
                                            </DirectEvents>
                                        </ext:CommandColumn>
                                    </Columns>
                                </ColumnModel>
                            </ext:TreePanel>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:Panel>
            <ext:Window ID="winLevelsEdit" runat="server" Title="Aggregation Levels" Hidden="true" IconCls="icon-aggregation_16" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                <Items>
                    <ext:FormPanel ID="pnlwLevels" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                        <Items>
                            <ext:Panel ID="Panel2" runat="server" Border="false" Header="false" ColumnWidth=".5" Layout="Form" LabelAlign="Top" Cls="PopupFormColumnPanel">
                                <Defaults>
                                    <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                    <ext:Parameter Name="MsgTarget" Value="side" />
                                </Defaults>
                                <Items>
                                    <ext:TextField ID="txtCode" runat="server" FieldLabel="Code" AnchorHorizontal="92%" Cls="PopupFormField" />
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="Panel3" runat="server" Border="false" Layout="Form" ColumnWidth=".5" LabelAlign="Top" Cls="PopupFormColumnPanel">
                                <Defaults>
                                    <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                    <ext:Parameter Name="MsgTarget" Value="side" />
                                </Defaults>
                                <Items>
                                    <ext:TextField ID="txtName" runat="server" FieldLabel="Name" AnchorHorizontal="92%" Cls="PopupFormField" />
                                </Items>
                            </ext:Panel>
                            <ext:Panel ID="Panel1" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" LabelAlign="Top" Cls="PopupFormColumnPanel">
                                <Defaults>
                                    <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                    <ext:Parameter Name="MsgTarget" Value="side" />
                                </Defaults>
                                <Items>
                                    <ext:ComboBox ID="ddlParents" runat="server" DisplayField="Name" Editable="true" TypeAhead="false" PageSize="10" MinChars="0" FieldLabel="Parent Level" ValueField="ID" Cls="PopupFormField">
                                        <ListConfig LoadingText="Searching..." ID="ctl302">
                                            <ItemTpl ID="ItemTpl1" runat="server">
                                                <Html>
                                                    <div class="search-item">
							                        <h3>{Name}</h3>
							                        <span>Parent: </span>{ParentName}</h3>
						                        </div>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Store>
                                            <ext:Store ID="storeParents" runat="server" OnReadData="StoreParents_ReadData">
                                                <Model>
                                                    <ext:Model ID="mdlLabels" runat="server" IDProperty="ID">
                                                        <Fields>
                                                            <ext:ModelField Name="ID" />
                                                            <ext:ModelField Name="Name" />
                                                            <ext:ModelField Name="ParentName" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                                <Proxy>
                                                    <ext:PageProxy>
                                                        <Reader>
                                                            <ext:JsonReader />
                                                        </Reader>
                                                    </ext:PageProxy>
                                                </Proxy>
                                            </ext:Store>
                                        </Store>
                                    </ext:ComboBox>
                                </Items>
                            </ext:Panel>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnSave" runat="server" Text="Save" Icon="DatabaseSave" Disabled="true" FormBind="true">
                                <DirectEvents>
                                    <Click OnEvent="btnSave_Click" />
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnCancel" runat="server" Icon="Decline" Text="Close">
                                <DirectEvents>
                                    <Click OnEvent="btnCancel_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                        <BottomBar>
                            <ext:StatusBar ID="FormStatusBar" runat="server" />
                        </BottomBar>
                        <Listeners>
                            <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                        text : valid ? 'Form is valid' : 'Form is invalid', 
                                                        iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                    });
                                                    #{btnSave}.setDisabled(!valid);" />
                        </Listeners>
                    </ext:FormPanel>
                </Items>
            </ext:Window>
            <ext:Window ID="winImport" runat="server" Title="Import Aggregation Levels" Hidden="true" Icon="ArrowLeft" Resizable="false" Width="600" Modal="true" Closable="false" BodyPadding="5">
                <Items>
                    <ext:FormPanel ID="frmImportFile" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
                        <Items>
                            <ext:Panel ID="pnlImportFile1" runat="server" Border="false" Header="false" ColumnWidth="1" Layout="Form" LabelAlign="Top" Cls="PopupFormColumnPanel">
                                <Defaults>
                                    <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                    <ext:Parameter Name="MsgTarget" Value="side" />
                                </Defaults>
                                <Items>
                                    <ext:FileUploadField ID="fileImport" FieldLabel="File" runat="server" Icon="Attach" Cls="PopupFormField" />
                                </Items>
                            </ext:Panel>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnImportFile" runat="server" Text="Import" Icon="TableRefresh" Disabled="true" FormBind="true">
                                <DirectEvents>
                                    <Click OnEvent="btnImportFile_Click">
                                        <EventMask ShowMask="true" Target="CustomTarget" CustomTarget="vpMain" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnCancelImport" runat="server" Icon="Decline" Text="Close">
                                <DirectEvents>
                                    <Click OnEvent="btnCancelImport_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                        <BottomBar>
                            <ext:StatusBar ID="FormImportStatusBar" runat="server" />
                        </BottomBar>
                        <Listeners>
                            <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                                        text : valid ? 'Form is valid' : 'Form is invalid', 
                                                        iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                    });
                                                    #{btnImportFile}.setDisabled(!valid);" />
                        </Listeners>
                    </ext:FormPanel>
                </Items>
            </ext:Window>
        </Items>
    </ext:Viewport>
    </form>
</body>
</html>
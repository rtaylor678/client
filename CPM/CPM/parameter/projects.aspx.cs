﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using CD;
using DataClass;

namespace CPM
{
    public partial class _projects : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdModel { get { return (Int32)Session["IdModel"]; } set { Session["IdModel"] = value; } }
        public Int32 IdProject { get { return (Int32)Session["IdProject"]; } set { Session["IdProject"] = value; } }
        public String NameModel { get { return (String)Session["NameModel"]; } set { Session["NameModel"] = value; } }
        public String Language { get { return (String)Session["Language"]; } }// set { Session["Language"] = value; } }
        private DataReportObject ExportProjects { get { return (DataReportObject)Session["Export01"]; } set { Session["Export01"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"];}  }//  set { Session["idLanguage"] = value; } } }

        private Int32 intFailedImportRowCount = 0;
        private String strImportErrorMessage = "";
        private String strImportErrorMessageSpecial = "";
        private Int32 intCurrentImportRow = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                IdProject = 0;
                this.LoadProjects();
                this.ChangeLimit();
                this.LoadLabel();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winProjectsEdit.Show();
        }

        protected void btnDeleteAll_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, MessageConfirmDelete(null), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDeleteAllYES()",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImport_Click(object sender, DirectEventArgs e)
        {
            this.ClearImport();
            this.winImport.Show();
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            try
            {
                this.Save();
                this.LoadProjects();
            }
            catch (Exception ex)
            {
                String strMessage = ex.Message;
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Error while saving"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            winProjectsEdit.Hide();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void btnCancelImport_Click(object sender, DirectEventArgs e)
        {
            winImport.Hide();
            this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void ChangeLimit()
        {
            Int32 IdOpretaion = Convert.ToInt32(this.ddlOperation.Value);

            if (ddlField.Text == "")
                return;

            if (IdOpretaion == 2)
            {
                this.txtStarts.MinValue = this.LoadMinStartsFields(null);
                this.txtStarts.Reset();
                this.txtStarts.Text = Convert.ToString(LoadMinStartsFields(null));
            }
            else
            {
                this.txtStarts.MinValue = this.LoadMinStarts(null);
                this.txtStarts.Reset();
                this.txtStarts.Text = Convert.ToString(LoadMinStarts(null));
            }        
        }

        protected void ddlOperation_Select(object sender, DirectEventArgs e)
        {
            this.ddlField.Reset();
            this.storeField.Reload();
            this.ChangeLimit();
        }

        protected void ddlField_Select(object sender, DirectEventArgs e)
        {
            this.ChangeLimit();
        }

        protected void btnDownloadTemplate_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data template?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedDownloadTemplateYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnSaveExcel_Click(object sender, DirectEventArgs e)
        {
            X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you would like to download this data?"), new Ext.Net.MessageBoxButtonsConfig
            {
                Yes = new MessageBoxButtonConfig
                {
                    Handler = "UsersX.ClickedSaveExcelReportYES({isUpload:true})",
                    Text = modMain.strCommonYes
                },
                No = new MessageBoxButtonConfig
                {
                    Text = modMain.strCommonNo
                }
            }).Show();
        }

        protected void btnImportFile_Click(object sender, DirectEventArgs e)
        {
            String strReturnMessage = null;
            if (this.fileImport.HasFile)
            {
                DataFileReader objReader = null;
                DataFileType objType = new DataFileType();
                objType.CheckFileType(fileImport.PostedFile.FileName);

                if (!objType.IsValidType)
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Invalid File Type."), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }

                if (objType.ImportFileClass == DataFileType.FileClass.Excel)
                {
                    objReader = new ExcelReader(fileImport.PostedFile.InputStream, DataFileReader.ImportType.Projects);
                    objReader.GetDataTable(ref strReturnMessage);
                }
                else
                {
                    strReturnMessage = (String)GetGlobalResourceObject("CPM_Resources", "Unable to determine the file type of the file being imported.");
                }

                if (String.IsNullOrEmpty(strReturnMessage))
                {
                    intFailedImportRowCount = 0;
                    strImportErrorMessage = "";
                    strImportErrorMessageSpecial = "";
                    intCurrentImportRow = 0;
                    foreach (DataRow row in objReader.ImportTable.Rows)
                    {
                        intCurrentImportRow++;
                        this.SaveImport(row["Case Code"].ToString(), row["Case Name"].ToString(), row["Field Code"].ToString(), row["Type of Case"].ToString(), Convert.ToInt32(row["Year Starts"]), row["Selection"].ToString().ToUpper() == "YES" ? true : false);
                    }
                    if (intFailedImportRowCount != 0)
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = Convert.ToString(GetGlobalResourceObject("CPM_Resources", "Records imported, but there were errors importing ### records!")).Replace(@"###", intFailedImportRowCount.ToString()), IconCls = "icon-accept", Clear2 = false });
                    }
                    else
                    {
                        this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!"), IconCls = "icon-accept", Clear2 = false });
                    }
                    this.winImport.Hide();
                    this.ModelUpdateTimestamp();
                    //Trim Error if too long
                    if (strImportErrorMessage.Length > 1024)
                    {
                        strImportErrorMessage = strImportErrorMessage.Substring(0, 1024) + "...";
                    }
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Import Complete"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Records imported successfully!") + (strImportErrorMessage == "" ? "!" : " (" + (String)GetGlobalResourceObject("CPM_Resources", "with exceptions") + ")!<br />" + strImportErrorMessage) });
                }
                else
                {
                    this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = strReturnMessage, IconCls = "icon-exclamation", Clear2 = false });
                }

                this.LoadProjects();
            }
            else
            {
                this.FormImportStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Import is invalid"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void StoreOperation_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsParameters objParameters = new DataClass.clsParameters();
            this.storeOperation.DataSource = objParameters.LoadList("Type = 'OperationProject' AND IdLanguage=" + idLanguage, "ORDER BY Name");
            this.storeOperation.DataBind();
        }

        protected void StoreField_ReadData(object sender, StoreReadDataEventArgs e)
        {
            String strFilter = "";
            if (this.ddlOperation.Value != null)
            {
                Int32 IdOpretaion = Convert.ToInt32(this.ddlOperation.Value);
                if (IdOpretaion == 0)
                {
                    this.storeField.RemoveAll();
                }
                else
                {
                    if (IdOpretaion == 1)
                    {
                        if (this.ddlField.Value != null)
                        {
                            String strSubFilter = "";
                            if (this.ddlField.Value != "")
                            {
                                Int32 IdField = Convert.ToInt32(this.ddlField.Value);
                                if (IdField > 0)
                                {
                                    strSubFilter = " AND [IdField]<>" + IdField;
                                }
                            }
                            //REMOVED to allow multiple baselines for a given FIELD
                            //strFilter = " AND IdField NOT IN (SELECT [IdField] FROM tblProjects WHERE [Operation]=1 and [IdModel]=" + IdModel + strSubFilter + ")";
                        }
                    }
                    DataClass.clsFields objFields = new DataClass.clsFields();
                    this.storeField.DataSource = objFields.LoadList("IdModel = " + IdModel + strFilter, "ORDER BY Name");
                    this.storeField.DataBind();
                }
            }
            else
            {
                this.storeField.RemoveAll();
            }
        }

        #endregion

        #region Methods

        protected void Clear()
        {
            this.IdProject = 0;
            this.txtName.Text = "";
            this.txtName.Reset();
            this.txtCode.Text = "";
            this.txtCode.Reset();
            this.ddlOperation.Reset();
            this.ddlField.Reset();
            this.ChangeLimit();
        }

        protected Int32 LoadMinStarts(Int32? _IdField)
        {
            Int32 Value = 0;
            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dt = objFields.LoadList("IdField = " + (_IdField == null ? ddlField.Value : _IdField), "");
            if (dt.Rows.Count>0)
            {
                Value = (Int32)dt.Rows[0]["Starts"];
            }
            return Value;
        }

        protected Int32 LoadMinStartsFields(Int32? _IdField)
        {
            Int32 Value = 0;
            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dt = objFields.LoadList("IdField = " + (_IdField == null ? ddlField.Value : _IdField), "");
            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["Starts"];
            }
            return Value + 1;
        }

        protected void ClearImport()
        {
            this.fileImport.Text = "";
        }

        protected void grdProjects_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            String _Name = (String)e.ExtraParams["Name"].ToString();
            IdProject = Convert.ToInt32(e.ExtraParams["Id"].ToString());

            if (Command == "Edit")
            {
                this.winProjectsEdit.Show();
                this.EditProjectLoad();
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, MessageConfirmDelete(_Name), new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedDeleteYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }      
        }

        protected void Save()
        {
            DataClass.clsProjects objProjects = new DataClass.clsProjects();
            objProjects.IdProject = IdProject;
            objProjects.loadObject();

            if (IdProject == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (objProjects.Code.ToString().ToUpper() != txtCode.Text.ToString().ToUpper())
                {
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }

            objProjects.Code = txtCode.Text;
            objProjects.Name = txtName.Text;
            objProjects.Starts = Convert.ToInt32(txtStarts.Text);
            objProjects.IdModel = IdModel;
            objProjects.IdField = Convert.ToInt32(this.ddlField.Value);
            objProjects.Operation = Convert.ToInt32(this.ddlOperation.Value);
            objProjects.TypeAllocation = null;
            if (chkCaseSelection.Checked)
            {
                objProjects.CaseSelection = true;
                if (objProjects.Operation == 2 && !objProjects.IsCaseSelectionChecked(objProjects.IdModel, objProjects.IdField))
                {
                    objProjects.CaseSelection = false;
                    X.Msg.Show(new MessageBoxConfig { Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.INFO, Title = (String)GetGlobalResourceObject("CPM_Resources", "Case Selection Error!!!"), Message = (String)GetGlobalResourceObject("CPM_Resources", "Cannot select Incremental line when no Baseline selected")});
                }
            }
            else
            {
                objProjects.CaseSelection = false;
            }
            if (IdProject == 0)
            {
                objProjects.DateCreation = DateTime.Now;
                objProjects.UserCreation = (Int32)IdUserCreate;
                IdProject = objProjects.Insert();
            }
            else
            {
                objProjects.DateModification = DateTime.Now;
                objProjects.UserModification = (Int32)IdUserCreate;
                objProjects.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
            this.winProjectsEdit.Hide();
            this.ModelUpdateTimestamp();
        }

        protected void SaveImport(String _Code, String _Name, String _Field, String _Operation, Int32 _Year, Boolean _CaseSelection)
        {
            Int32 _IdField = GetIdField(_Field);
            Int32 _IdOperation = GetIdOperation(_Operation);
            Int32 _MinYear = LoadMinStarts(_IdField);
            DataClass.clsProjects objProjects = new DataClass.clsProjects();
            objProjects.IdModel = IdModel;
            objProjects.IdField = _IdField;
          
            if (_Year < _MinYear)
            {
                _Year = _MinYear;
            }

            if (!String.IsNullOrEmpty(_Code) && !String.IsNullOrEmpty(_Name) && _IdField != 0 && _IdOperation != 0)
            {
                if (ProjectCodeExists(IdModel, _Code))
                {
                    strImportErrorMessageSpecial += "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Case") + " '" + _Code + "' " + (String)GetGlobalResourceObject("CPM_Resources", "could not be imported due to an existing case with the same code");
                }
                else if (BaselineExists(IdModel, _IdField, _Code) && _IdOperation == 1)
                {
                    strImportErrorMessageSpecial += "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Case") + " '" + _Code + "' " + (String)GetGlobalResourceObject("CPM_Resources", "could not be imported due to an existing baseline");
                }
                else if (BusinessOpportunityExists(IdModel, _IdField, _Code) && _IdOperation == 2)
                {
                    strImportErrorMessageSpecial += "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Case") + " '" + _Code + "' " + (String)GetGlobalResourceObject("CPM_Resources", "could not be imported due to an existing business opportunity");
                }
                else if (_IdOperation == 2 && !objProjects.IsCaseSelectionChecked(objProjects.IdModel, objProjects.IdField))
                    {
                        strImportErrorMessageSpecial += "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Case") + " '" + _Code + "' " + (String)GetGlobalResourceObject("CPM_Resources", "could not be imported due to business opportunity being selected but not a baseline");
                    }
                else
                {
                    objProjects.IdProject = (Int32)IdProject;
                    objProjects.loadObject();
                    objProjects.IdModel = IdModel;
                    objProjects.IdField = _IdField;
                    objProjects.Code = _Code;
                    objProjects.Name = _Name;
                    objProjects.Operation = _IdOperation;
                    objProjects.Starts = _Year;
                    objProjects.DateCreation = DateTime.Now;
                    objProjects.UserCreation = (Int32)IdUserCreate;
                    objProjects.CaseSelection = _CaseSelection;
                    IdProject = objProjects.Insert();
                    objProjects = null;
                }
            }
            else
            {
                intFailedImportRowCount++;
                strImportErrorMessage += (strImportErrorMessage == "" ? "<br />" + (String)GetGlobalResourceObject("CPM_Resources", "Error row") + " '" + intCurrentImportRow.ToString() + "'" : ", '" + intCurrentImportRow.ToString() + "'");
            }
        }

        protected Boolean ProjectCodeExists(Int32 _IdModel, String _Code)
        {
            Boolean Value = false;
            DataClass.clsProjects objProjects = new DataClass.clsProjects();
            DataTable dt = objProjects.LoadList("IdModel = " + _IdModel + " AND CODE = '" + _Code + "'", "");
            if (dt.Rows.Count > 0)
            {
                Value = true;
            }
            return Value;
        }

        protected Boolean BaselineExists(Int32 _IdModel, Int32 _IdField, String _Code)
        {
            Boolean Value = false;
            DataClass.clsProjects objProjects = new DataClass.clsProjects();
            DataTable dt = objProjects.LoadList("IdModel = " + _IdModel + " AND IdField = " + _IdField + " AND Operation = 1 AND CODE = '" + _Code + "'", "");
            if (dt.Rows.Count > 0)
            {
                Value = true;
            }
            return Value;
        }

        protected Boolean BusinessOpportunityExists(Int32 _IdModel, Int32 _IdField, String _Code)
        {
            Boolean Value = false;
            DataClass.clsProjects objProjects = new DataClass.clsProjects();
            DataTable dt = objProjects.LoadList("IdModel = " + _IdModel + " AND IdField = " + _IdField + " AND Operation = 2 AND CODE = '" + _Code + "'", "");
            if (dt.Rows.Count > 0)
            {
                Value = true;
            }
            return Value;
        }

        protected Int32 GetIdField(String _Field)
        {
            Int32 Value = 0;
            DataClass.clsFields objFields = new DataClass.clsFields();
            DataTable dt = objFields.LoadList("Code = '" + _Field + "' AND IdModel = " + IdModel, "");
            
            if (dt.Rows.Count > 0)
            {
                Value = (Int32)dt.Rows[0]["IdField"];
            }
            return Value;
        }

        protected Int32 GetIdOperation(String _Operation)
        {
            Int32 IdOperation = 0;
            DataClass.clsParameters objParameters = new DataClass.clsParameters();
            DataTable dt = objParameters.LoadList("Type = 'OperationProject' AND Name = '" + _Operation + "'", "");

            if (dt.Rows.Count > 0)
            {
                IdOperation = (Int32)dt.Rows[0]["Code"];
            }
            return IdOperation;
        }

        protected void EditProjectLoad()
        {
            DataClass.clsProjects objProjects = new DataClass.clsProjects();
            DataTable dt = objProjects.LoadList("IdProject = " + IdProject, "");
            if (dt.Rows.Count > 0)
            {
                this.txtCode.Text = (String)dt.Rows[0]["Code"];
                this.txtName.Text = (String)dt.Rows[0]["Name"];

                this.ddlOperation.Value = Convert.ToInt32(dt.Rows[0]["Operation"]);
                this.ddlField.Value = Convert.ToInt32(dt.Rows[0]["IdField"]);
                this.storeField.Reload();
                this.ChangeLimit();
                this.txtStarts.Text = Convert.ToString(dt.Rows[0]["Starts"]);
                this.chkCaseSelection.Checked = Convert.ToBoolean(dt.Rows[0]["CaseSelection"]);
            }
        }

        public void Delete()
        {
            DataClass.clsProjects objProjects = new DataClass.clsProjects();
            objProjects.IdProject = (Int32)IdProject;
            objProjects.Delete();
            this.ModelUpdateTimestamp();
        }

        public void DeleteAll()
        {
            DataClass.clsProjects objProjects = new DataClass.clsProjects();
            objProjects.IdModel = (Int32)IdModel;
            objProjects.DeleteAll();
            this.ModelUpdateTimestamp();
        }

        [DirectMethod]
        public void ClickedDeleteYES()
        {
            this.Delete();
            this.LoadProjects();
        }

        [DirectMethod]
        public void ClickedDeleteAllYES()
        {
            this.DeleteAll();
            this.LoadProjects();
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsProjects objProjects = new DataClass.clsProjects();
            DataTable dt = objProjects.LoadList("IdModel = " + IdModel + " AND Code = '" + txtCode.Text + "'", "");
            Value = dt.Rows.Count;

            return Value;
        }

        protected void LoadProjects()
        {
            DataClass.clsProjects objProjects = new DataClass.clsProjects();

            DataTable dt = objProjects.LoadList("IdModel = " + IdModel + " AND IdLanguage = " + idLanguage, "ORDER BY Name");

            storeProjects.DataSource = dt;
            storeProjects.DataBind();

            //Generate Export Details
            ArrayList datColumnTD = new ArrayList();
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Code", "Case Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Name", "Case Name"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("FieldCode", "Field Code"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("OperationName", "Type of Case"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("CaseSelection", "Selection"));
            datColumnTD.Add(new DataClass.DataTableColumnDisplay("Starts", "Year Starts"));
            String strSelectedParameters = "";
            String[] columnsToCopy = { "Code", "Name", "FieldCode", "OperationName", "CaseSelection","Starts" };
            System.Data.DataView view = new System.Data.DataView(dt);
            DataTable dtProjects = view.ToTable(true, columnsToCopy);
            ExportProjects = new DataReportObject(dtProjects, "DATASHEET", null, strSelectedParameters, null, "Name", datColumnTD, null);
        }

        [DirectMethod]
        public void ClickedSaveExcelReportYES()
        {
            String strReturnMessage = null;
            DataFileWriter objWriter = null;

            clsUsers objUser = new clsUsers();
            objUser.IdUser = IdUserCreate;
            objUser.loadObject();

            DataReportObject StoredExportTD = new DataReportObject();
            StoredExportTD = ExportProjects;

            objWriter = new ExcelWriter(DataFileWriter.ExportType.Excel);
            objWriter.ExportReportCollection = new List<DataReportObject>();
            objWriter.ExportReportCollection.Add(StoredExportTD);
            objWriter.BuildExportData(ref strReturnMessage, objUser.Name, DataFileWriter.ExcelType.Excel2003);
            if (String.IsNullOrEmpty(strReturnMessage))
            {
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.AddHeader("content-disposition", "attachment; filename=1.3 ExportCases.xls");
                context.Response.ContentType = "application/octet-stream";
                objWriter.objExcel.Save(context.Response.OutputStream);
                context.Response.End();
            }
        }

        public String MessageConfirmDelete(String itemName)
        {
            String messageConfirmDeleteReturn = "";
            if (!String.IsNullOrEmpty(itemName))
            {
                messageConfirmDeleteReturn = (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + itemName + "?";
            }
            else
            {
                messageConfirmDeleteReturn = (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete all records?");
            }
            //messageConfirmDeleteReturn += "<br><br><b>This will also delete any related data from the following:</b><br>"
            //        + "<br> - Mapping (Ziff Cost Structure VS Client Cost Structure)"
            //        + "<br> - Peer criteria Data"
            //        + "<br> - Ziff Base Cost"
            //        + "<br> - Technical Assumptions"
            //        + "<br> - Economic Assumptions";
            return messageConfirmDeleteReturn;
        }

        [DirectMethod]
        public void ClickedDownloadTemplateYES()
        {
            HttpContext context = HttpContext.Current;
            objApplication.StreamFile(context, "ImportCases.xls", "template");
        }

        public void ModelUpdateTimestamp()
        {
            clsModels objModelUpdate = new clsModels();
            objModelUpdate.IdModel = IdModel;
            objModelUpdate.UpdateTimestamp(true, false, false, false, false);
            objModelUpdate = null;
        }

        protected void LoadLabel()
        {
            this.grdProjects.Title = modMain.strMasterFieldProjects;
            this.btnAdd.Text = (String)GetGlobalResourceObject("CPM_Resources", "Add New Project");
            this.btnAdd.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Add New Project");
            this.btnDelete.Text = modMain.strCommonDeleteAll;
            this.btnDelete.ToolTip = modMain.strCommonDeleteAll;
            this.btnImport.Text = modMain.strCommonImport;
            this.btnImport.ToolTip = modMain.strCommonImport;
            this.btnDownloadTemplate.Text = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.btnDownloadTemplate.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Download Import Template");
            this.Code.Text = modMain.strCommonCode;
            this.Name.Text = modMain.strCommonName;
            this.colField.Text = modMain.strTechnicalModuleField.Remove(modMain.strTechnicalModuleField.Length - 1, 1);
            this.Operation.Text = (String)GetGlobalResourceObject("CPM_Resources", "Type of Project");
            this.colStarts.Text = (String)GetGlobalResourceObject("CPM_Resources", "Starts");
            this.CaseSelection.Text = (String)GetGlobalResourceObject("CPM_Resources", "Case Selection");
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = "&nbsp;" + modMain.strCommonEdit;
            this.ImageCommandColumn1.Commands[1].Text = "&nbsp;" + modMain.strCommonDelete;
            this.winProjectsEdit.Title = (String)GetGlobalResourceObject("CPM_Resources", "Project");
            this.txtCode.FieldLabel = modMain.strCommonCode;
            this.txtName.FieldLabel = modMain.strCommonName;
            this.ddlOperation.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Type of Project");
            this.ddlField.FieldLabel = modMain.strTechnicalModuleField;
            this.txtStarts.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Starts");
            this.btnSave.Text = modMain.strCommonSave;
            this.btnSave.ToolTip = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
            this.btnCancel.ToolTip = modMain.strCommonClose;
            this.winImport.Title = modMain.strCommonImport + " " + modMain.strMasterFieldProjects;
            this.fileImport.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "File");
            this.btnImportFile.Text = modMain.strCommonImport;
            this.btnImportFile.ToolTip = modMain.strCommonImport;
            this.btnCancelImport.Text = modMain.strCommonClose;
            this.btnCancelImport.ToolTip = modMain.strCommonClose;
            this.btnSaveExcel.Text = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.btnSaveExcel.ToolTip = (String)GetGlobalResourceObject("CPM_Resources", "Export Data To Excel");
            this.chkCaseSelection.FieldLabel = (String)GetGlobalResourceObject("CPM_Resources", "Case Selection");
        }

        #endregion

    }
}
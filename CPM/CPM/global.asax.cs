﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace CPM
{
    public class _global : System.Web.HttpApplication
    {

        protected void Session_Start(object sender, EventArgs e)
        {
            AppClass.clsApplication objApplication = new AppClass.clsApplication();
            objApplication.LoadAppLicense();
            Session["Application"] = objApplication;
            if (objApplication.LicenseValid)
            {
                Response.Redirect("/account/login.aspx");
            }
            else
            {
                Response.Redirect("/about/license_error.aspx");
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {
            
        }

        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (context != null && context.Session != null)
            {
                Session["GlobalError"] = "ERROR: " + Server.GetLastError().Message;
                Server.Transfer("/ERROR.aspx");
            }
        }

    }
}
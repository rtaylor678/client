﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="passwordreset.aspx.cs" Inherits="CPM._passwordreset" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_login.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <ext:ResourceManager ID="rscManager" runat="server" />

    <ext:Viewport ID="viewportLogin" Frame="false" runat="server">
        <LayoutConfig>
            <ext:VBoxLayoutConfig Align="Center" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Panel ID="Panel1" runat="server" Frame="true">
                <Items>
                    <ext:FormPanel ID="frmPanelLogin" runat="server" Title="Login" Header="false" Icon="User" Border="False" Frame="false" ButtonAlign="Center" BodyBorder="0" BodyPadding="20" DefaultAnchor="100%">
                        <Items>
                            <ext:Label ID="lblMessage" Text="" />
                            <ext:Panel Border="false">
                                <Content><br /></Content>
                            </ext:Panel>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnLogin" Icon="Key" FormBind="true" runat="server" Text="...">
                                <DirectEvents>
                                    <Click OnEvent="btnLogin_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                        <BottomBar>
                            <ext:StatusBar CtCls="back-login" ID="FormStatusBar" runat="server" />
                        </BottomBar>
                    </ext:FormPanel>
                </Items>
            </ext:Panel>
        </Items>
    </ext:Viewport>
</body>
</html>
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using Ext.Net.Utilities;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Threading;
using System.Net.Mail;
using CD;
using DataClass;

namespace CPM
{
    public partial class _passwordreset : System.Web.UI.Page
    {

        #region Definitions

        String strTokenValue = "";
        
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                // Start Parse QueryString
                NameValueCollection strValues;
                String strKey;
                strValues = Request.QueryString;
                for (int i = 0; i < strValues.AllKeys.Length; i++)
                {
                    strKey = strValues.AllKeys[i];
                    if (strKey.ToLower() == "token")
                    {
                        strTokenValue = strValues[i];
                    }
                }
                // End Parse QueryString

                if (!String.IsNullOrEmpty(strTokenValue))
                {
                    DataClass.clsUsers objUsers = new DataClass.clsUsers();
                    objUsers.IdUser = objUsers.PasswordResetValidate(strTokenValue);

                    switch (objUsers.IdUser)
                    {
                        case -2:
                            {
                                this.lblMessage.Html = "The password reset token has previously been used. You must request your password reset again.";
                                this.btnLogin.Text = "Back to Login page...";
                                this.btnLogin.Visible = true;
                                break;
                            }
                        case -1:
                            {
                                this.lblMessage.Html = "An error occured while attempting to reset your password. Please try again.";
                                this.btnLogin.Text = "Back to Login page...";
                                this.btnLogin.Visible = true;
                                break;
                            }
                        case 0:
                            {
                                this.lblMessage.Html = "Invalid password reset token, please try resetting your password again.";
                                this.btnLogin.Text = "Back to Login page...";
                                this.btnLogin.Visible = true;
                                break;
                            }
                        default:
                            {
                                objUsers.LogActivity(UserLog.UserActivity.PasswordResetCompleted, null, null);
                                this.lblMessage.Html = "Your password has been successfully reset to 'P@55word'.<br><br>Please change your password after you login the next time.";
                                this.btnLogin.Text = "Retry Login...";
                                this.btnLogin.Visible = true;
                                break;
                            }
                    }
                }
                else
                {
                    this.lblMessage.Text = "Invalid password reset token, please try resetting your password again.";
                    this.btnLogin.Visible = false;
                }
            }
        }
        
        protected void btnLogin_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("/account/login.aspx");
        }

        #endregion

        #region Methods
        #endregion

    }
}
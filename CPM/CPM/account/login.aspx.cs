﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using Ext.Net.Utilities;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Threading;
using System.Net.Mail;
using CD;
using DataClass;
using System.Globalization;

namespace CPM
{
    public partial class _login : System.Web.UI.Page
    {

        #region Definitions

        public AppClass.clsApplication objApplication { get { return (AppClass.clsApplication)Session["Application"]; } }
        public DataClass.clsUsers objUsers;
        public Int32 IdUser { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public String NameUser { get { return (String)Session["NameUser"]; } set { Session["NameUser"] = value; } }
        public String Language { get { return (String)Session["Language"]; } set { Session["Language"] = value; } }
        private Int32 idLanguage { get { return (Int32)Session["idLanguage"]; } set { Session["idLanguage"] = value; } }
        public DateTime LastLogin { get { return (DateTime)Session["LastLogin"]; } set { Session["LastLogin"] = value; } }
        public String Email { get; set; }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!objApplication.LicenseValid)
            {
                Response.Redirect("/about/license_error.aspx");
            }
            if (!X.IsAjaxRequest)
            {
                Session["IdUser"] = null;
                Session["IdModel"] = null;
                Session["Language"] = null;
                Session["IdLanguage"] = null;

                SmtpClient objSmtpClient = new SmtpClient();
                if (String.IsNullOrEmpty(objSmtpClient.Host))
                {
                    this.imgForgot.Visible = false;
                }
                objSmtpClient.Dispose();
                objSmtpClient = null;
                if (Request.Cookies["UserName"] != null && Request.Cookies["Password"] != null)
                {
                    txtUserName.Text = Request.Cookies["UserName"].Value;
                    txtPassword.Attributes["value"] = CD.DAC.Decrypt(Request.Cookies["Password"].Value);
                    chkRemember.Checked = true; 
                }
                else
                {
                    txtUserName.Text = "";
                    txtPassword.Text ="";
                    chkRemember.Checked = false;
                }

                this.ddlLanguage_Load();
                this.LoadLanguage();
                this.LoadApplication();
            }
        }

        protected void ddlLanguage_Select(object sender, DirectEventArgs e)
        {
            Language = ddlLanguage.SelectedItem.Text.ToString();
            idLanguage = Convert.ToInt32(ddlLanguage.SelectedItem.Value);
            this.LoadLanguage();
            switch (ddlLanguage.SelectedItem.Text.ToString())
            {
                case "English":
                    //English call
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                    Session["MyUICulture"] = Thread.CurrentThread.CurrentUICulture;
                    Session["MyCulture"] = Thread.CurrentThread.CurrentCulture;
                    break;
                case "Español":
                    //Spanish call
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("es-ES");
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("es-ES");
                    Session["MyUICulture"] = Thread.CurrentThread.CurrentUICulture;
                    Session["MyCulture"] = Thread.CurrentThread.CurrentCulture;
                    break;
                default:
                    break;
            }
             
        }

        protected void imgForgot_Click(object sender, DirectEventArgs e)
        {
            String username = txtUserName.Text;
            String strNewToken = "";
            String strEmailBody = "";

            if (username != "")
            {
                this.UserLoad();
                if (objUsers != null)
                {
                    if (objUsers.IdUser != 0)
                    {
                        strEmailBody = objApplication.ReadFile("email_passwordresettoken.txt", "resource");
                        if (!String.IsNullOrEmpty(strEmailBody))
                        {
                            SmtpClient objSmtpClient = new SmtpClient();
                            if (!String.IsNullOrEmpty(objSmtpClient.Host))
                            {
                                strNewToken = Guid.NewGuid().ToString().Replace("-", "").ToUpper() + Guid.NewGuid().ToString().Replace("-", "").ToUpper();
                                if (objUsers.PasswordResetRequest(strNewToken) == 1)
                                {
                                    strEmailBody = strEmailBody.Replace("«Logo»", "<img src='" + HttpContext.Current.Request.Url.AbsoluteUri.Replace("/account/login.aspx", "/image/solomon-cpm-email.png") + "' alt='CPM' />");
                                    strEmailBody = strEmailBody.Replace("«TokenLink»", "<a href='" + HttpContext.Current.Request.Url.AbsoluteUri.Replace("login.aspx", "passwordreset.aspx") + "?token=" + strNewToken + "'>Password Reset Token Link</a>");
                                    System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                                    msg.To.Add(Email);
                                    msg.Subject = "CPM Password Reset Token";
                                    msg.SubjectEncoding = System.Text.Encoding.UTF8;
                                    msg.Body = strEmailBody;
                                    msg.BodyEncoding = System.Text.Encoding.UTF8;
                                    msg.IsBodyHtml = true;
                                    try
                                    {
                                        objSmtpClient.Send(msg);
                                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strLogForgotMessageMail, IconCls = "icon-accept", Clear2 = false });
                                        objUsers.LogActivity(UserLog.UserActivity.PasswordResetRequested, null, null);
                                        return;
                                    }
                                    catch (Exception ex)
                                    {
                                        objUsers.PasswordDeleteToken(strNewToken);
                                        String strMessage = ex.Message;
                                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strLogForgotMessageError, IconCls = "icon-exclamation", Clear2 = false });
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strLogForgotMessageUser, IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
                else
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strLogForgotMessageUser, IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["Password"].Expires = DateTime.Now.AddDays(-1);
            txtUserName.Text = "";
            txtPassword.Text = "";
            chkRemember.Checked = false;
        }

        protected void btnAccept_Click(object sender, DirectEventArgs e)
        {
            String uname = txtUserName.Text;
            String pass = txtPassword.Text;
            String LogMessage = "";

            Language = ddlLanguage.SelectedItem.Text.ToString();
            idLanguage = Convert.ToInt32(ddlLanguage.SelectedItem.Value);

            objUsers = new DataClass.clsUsers();

            LogMessage = "HTTP_REMOTE_ADDR:" + Request.ServerVariables["REMOTE_ADDR"] + " " + Request.ServerVariables["ALL_HTTP"];

            if ((uname == "") || (pass == ""))
            {
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strLogLoginMessageError, IconCls = "icon-exclamation", Clear2 = false });
                return;
            }
            else
            {
                DataTable dt = objUsers.LoadList("UserName = '" + uname + "' AND Password = '" + DAC.Encrypt(pass) + "'", "ORDER BY Name");
                if (dt.Rows.Count > 0)
                {

                    if (chkRemember.Checked)
                    {
                        Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(30);
                        Response.Cookies["Password"].Expires = DateTime.Now.AddDays(30);
                    }
                    else
                    {
                        Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies["Password"].Expires = DateTime.Now.AddDays(-1);

                    }
                    Response.Cookies["UserName"].Value = txtUserName.Text.Trim();
                    Response.Cookies["Password"].Value = CD.DAC.Encrypt(txtPassword.Text.Trim());

                    IdUser = (Int32)dt.Rows[0]["IdUser"];
                    NameUser = (String)dt.Rows[0]["Name"];
                    Email = (String)dt.Rows[0]["Email"];
                    LastLogin = (DateTime)dt.Rows[0]["LastLogin"];
                    this.SaveUser();
                    objUsers.IdUser = IdUser;
                    objUsers.LogActivity(UserLog.UserActivity.LoginSuccess, null, LogMessage);
                    X.Redirect("/caseselection.aspx");
                }
                else
                {
                    DataTable dtBadPass = objUsers.LoadList("UserName = '" + uname + "' AND Password <> '" + DAC.Encrypt(pass) + "'", "ORDER BY Name");
                    if (dtBadPass.Rows.Count > 0)
                    {
                        IdUser = (Int32)dtBadPass.Rows[0]["IdUser"];
                        objUsers.IdUser = IdUser;
                        objUsers.LogActivity(UserLog.UserActivity.LoginFailed, null, LogMessage);
                        Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies["Password"].Expires = DateTime.Now.AddDays(-1);
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strLogLoginMessageError, IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                    else
                    {
                        Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies["Password"].Expires = DateTime.Now.AddDays(-1);
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strLogLoginMessageError, IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }
        }

        #endregion

        #region Methods

        protected void LoadApplication()
        {
            this.lblVersion.Text = objApplication.AppVersion;
        }

        protected void SaveUser()
        {
            DataClass.clsUsers objUsers = new DataClass.clsUsers();

            objUsers.IdUser = IdUser;
            objUsers.loadObject();

            objUsers.LastLogin = DateTime.Now;
            objUsers.Update();
        }

        protected void LoadLanguage()
        {
            DataClass.clsLabelsLanguages objLabelsLanguages = new DataClass.clsLabelsLanguages();
            DataTable dt = objLabelsLanguages.LoadList("LanguageName = '" + Language + "' AND FormName = 'Login'", "ORDER BY IdLabel");
            foreach (DataRow row in dt.Rows)
            {
                switch ((String)row["LabelName"])
                {
                    case "Title Form":
                        modMain.strLogTitleForm = (String)row["LabelText"];
                        break;
                    case "UserName":
                        modMain.strLogUserName = (String)row["LabelText"];
                        break;
                    case "Password":
                        modMain.strLogPassword = (String)row["LabelText"];
                        break;
                    case "Language":
                        modMain.strLogLanguage = (String)row["LabelText"];
                        break;
                    case "Remember Me":
                        modMain.strLogRememberMe = (String)row["LabelText"];
                        break;
                    case "Forgot Password?":
                        modMain.strLogForgotPassword = (String)row["LabelText"];
                        break;
                    case "Login Button":
                        modMain.strLogLoginButton = (String)row["LabelText"];
                        break;
                    case "Login Message Error":
                        modMain.strLogLoginMessageError = (String)row["LabelText"];
                        break;
                    case "Forgot Password? Message Error":
                        modMain.strLogForgotMessageError = (String)row["LabelText"];
                        break;
                    case "Forgot Password? User Error":
                        modMain.strLogForgotMessageUser = (String)row["LabelText"];
                        break;
                    case "Forgot Password? Message Mail":
                        modMain.strLogForgotMessageMail = (String)row["LabelText"];
                        break;
                    case "Valid":
                        modMain.strLogValid = (String)row["LabelText"];
                        break;
                }
            }
            this.LoadLabel();
        }

        protected void LoadLabel()
        {
            this.Title = modMain.strLogTitleForm;
            this.frmPanelLogin.Title = modMain.strLogTitleForm;
            this.lblUserName.Text = modMain.strLogUserName;
            this.txtUserName.EmptyText = modMain.strLogUserName;
            this.txtUserName.BlankText = modMain.strLogValid;
            this.txtUserName.MaxLength = 30;
            this.txtUserName.Reset();

            this.lblPassword.Text = modMain.strLogPassword;
            this.txtPassword.EmptyText = modMain.strLogPassword;
            this.txtPassword.BlankText = modMain.strLogValid;
            this.txtPassword.MaxLength = 20;
            this.txtPassword.Reset();

            this.lblLanguage.Text = modMain.strLogLanguage;
            this.chkRemember.FieldLabel = modMain.strLogRememberMe;
            this.imgForgot.Text = modMain.strLogForgotPassword;
            this.btnAccept.Text = modMain.strLogLoginButton;
            this.btnAccept.IconCls = "icon-login";
        }

        protected void ddlLanguage_Load()
        {
            Store store = this.ddlLanguage.GetStore();

            List<Object> objList = new List<Object>();

            DataClass.clsLanguages objLanguages = new DataClass.clsLanguages();

            DataTable dt = objLanguages.LoadList("", "ORDER BY Name");
            foreach (DataRow row in dt.Rows)
            {
                Icon iconFlag = new Icon();
                if ((String)row["Flag"] == "FlagUs")
                {
                    iconFlag = Icon.FlagUs;
                }
                else if ((String)row["Flag"] == "FlagEs")
                {
                    iconFlag = Icon.FlagEs;
                }
                else if ((String)row["Flag"] == "FlagDe")
                {
                    iconFlag = Icon.FlagDe;
                }
                else if ((String)row["Flag"] == "FlagIt")
                {
                    iconFlag = Icon.FlagIt;
                }

                if ((Boolean)row["Default"] == true)
                {
                    if (Language == null)
                    {
                        Language = (String)row["Name"];
                        idLanguage = Convert.ToInt32(row["IdLanguage"]);
                    }
                }

                objList.Add(new object[] { ResourceManager.GetIconClassName(iconFlag), (String)row["Name"], Convert.ToInt32(row["IdLanguage"])});
                this.rscManager.RegisterIcon(iconFlag);
            }

            store.DataSource = objList;
            store.DataBind();
            this.ddlLanguage.SelectedItems.Add(new Ext.Net.ListItem(idLanguage));

            switch (Language)
            {
                case "English":
                    //English call
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                    Session["MyUICulture"] = Thread.CurrentThread.CurrentUICulture;
                    Session["MyCulture"] = Thread.CurrentThread.CurrentCulture;
                    break;
                case "Español":
                    //Spanish call
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("es-ES");
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("es-ES");
                    Session["MyUICulture"] = Thread.CurrentThread.CurrentUICulture;
                    Session["MyCulture"] = Thread.CurrentThread.CurrentCulture;
                    break;
                default:
                    break;
            }

            Session["Language"] = Language;
            Session["IdLanguage"] = idLanguage;
        }

        protected void UserLoad()
        {
            objUsers = new DataClass.clsUsers();
            DataTable dt = objUsers.LoadList("UserName = '" + txtUserName.Text + "'", "");
            if (dt.Rows.Count > 0)
            {
                IdUser = (Int32)dt.Rows[0]["IdUser"];
                NameUser = (String)dt.Rows[0]["Name"];
                Email = (String)dt.Rows[0]["Email"];
                LastLogin = (DateTime)dt.Rows[0]["LastLogin"];
            }
            else 
            {
                IdUser = 0;
                NameUser = "";
                Email = "";
                LastLogin = DateTime.Now;
            }
            objUsers.IdUser = IdUser;
            objUsers.UserName = NameUser;
            objUsers.Email = Email;
            objUsers.LastLogin = LastLogin;
        }

        #endregion

    }
}
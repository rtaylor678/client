﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="CPM._login" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <title>CPM</title>
    <link href="/css/Extnet.css" rel="stylesheet" type="text/css" />
    <link href="/_theme/blue/css/blue_login.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        if (self != top)
            top.location.href = window.location.href;
    </script>
    <script runat="server">
        protected override void InitializeCulture()
        {
            if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
            {
                Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
                Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
            }
            base.InitializeCulture();
        }
    </script>
</head>
<body>
    <ext:ResourceManager ID="rscManager" runat="server" />
    <ext:Viewport ID="viewportLogin" Frame="false" runat="server">
        <LayoutConfig>
            <ext:VBoxLayoutConfig Align="Center" Pack="Center" />
        </LayoutConfig>
        <Items>
            <ext:Panel ID="Panel1" runat="server" Frame="true">
                <Items>
                    <ext:Panel ID="Panel2" Width="300" Border="false" runat="server">
                        <Items>
                            <ext:Image ID="imgCompany" runat="server" Align="Middle" ImageUrl="/image/solomon-cpm-logon.png" Height="120" Width="300" />
                            <ext:Label ID="lblVersion" runat="server" Text=" " Cls="x-text-version" />
                        </Items>
                    </ext:Panel>
                    <ext:FormPanel ID="frmPanelLogin" runat="server" Title="Login" Header="false" Icon="User" Border="False" Frame="false" ButtonAlign="Center" Width="300" BodyBorder="0" BodyPadding="20" DefaultAnchor="100%">
                        <Items>
                            <ext:Label ID="lblUserName" runat="server" Text="Username:" Cls="x-text-login" />
                            <ext:TextField ID="txtUserName" HideLabel="true" runat="server" Height="40" FormItemCls="x-text-input-focus" FieldBodyCls="x-text-input-height" FieldCls="x-text-input-login" AllowBlank="true" Name="user" EmptyText="username" />
                            <ext:Label ID="lblPassword" runat="server" Text="Password:" Cls="x-text-login"/>
                            <ext:TextField ID="txtPassword" HideLabel="true" runat="server" Height="40" FieldBodyCls="x-text-input-height" FieldCls="x-text-input-login" AllowBlank="true" Name="pass" EmptyText="password" InputType="Password" />
                            <ext:Label ID="lblLanguage" runat="server" Text="Language:" Cls="x-text-login" />
                            <ext:ComboBox HideLabel="true" ID="ddlLanguage" FieldLabel="Language" runat="server" Width="250" Height="22" FieldBodyCls="x-text-combo-height" FieldCls="x-text-combo-login" Editable="false" DisplayField="name" ForceSelection="true" ValueField="IdLanguage" QueryMode="Local" Selectable="true" OnDirectSelect="ddlLanguage_Select" TriggerAction="All" ReadOnly="false">
                                <Store>
                                    <ext:Store ID="Store1" runat="server">
                                        <Model>
                                            <ext:Model ID="Model1" runat="server">
                                                <Fields>
                                                    <ext:ModelField Name="iconCls" />
                                                    <ext:ModelField Name="name" />
                                                    <ext:ModelField Name ="IdLanguage" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                                <ListConfig>
                                    <ItemTpl ID="ItemTpl1" runat="server">
                                        <Html>
                                            <div class="icon-combo-item {iconCls}">
                                                <div class="x-text-combo-login">{name}</div>
                                            </div>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <Listeners>
                                    <Change Handler="if(this.valueModels.length>0){this.setIconCls(this.valueModels[0].get('iconCls'));}" />
                                </Listeners>
                            </ext:ComboBox>
                            <ext:Panel ID="pnlSpacer" runat="server" Border="false">
                                <Content><br /></Content>
                            </ext:Panel>
                            <ext:Checkbox ID="chkRemember" LabelWidth="160" LabelCls="x-text-login" runat="server" FieldLabel="Remember me" Name="remember" />
                        </Items>
                        <Buttons>
                            <ext:LinkButton Cls="forgot-login" ID="imgForgot" runat="server" IconCls="icon-forgot" Text="Forgot Password" IconAlign="Right">
                                <DirectEvents>
                                    <Click OnEvent="imgForgot_Click">
                                        <EventMask ShowMask="true" Target="CustomTarget" CustomTarget="viewportLogin" />
                                    </Click>
                                </DirectEvents>
                            </ext:LinkButton>
                            <ext:Button ID="btnAccept" FormBind="true" runat="server" Text="Login" CtCls="button-login">
                                <DirectEvents>
                                    <Click OnEvent="btnAccept_Click" />
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                        <BottomBar>
                            <ext:StatusBar ID="FormStatusBar" runat="server" CtCls="back-login" Height="20" />
                        </BottomBar>
                        <Listeners>
                            <ValidityChange Handler="App.FormStatusBar.setStatus({
                                                            text : valid ? null : null, 
                                                            iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                                        });
                                                        #{btnAccept}.setDisabled(!valid);" />
                        </Listeners>
                    </ext:FormPanel>
                </Items>
            </ext:Panel>
        </Items>
    </ext:Viewport>
</body>
</html>
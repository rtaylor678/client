﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.IO;
using System.Dynamic;
using System.Data;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Xml;
using CD;
using DataClass;
using System.Globalization;

namespace CPM
{     
    public partial class _settings : System.Web.UI.Page
    {

        #region Definitions

        public Int32 IdUserCreate { get { return (Int32)Session["IdUser"]; } set { Session["IdUser"] = value; } }
        public Int32 IdProfile { get { return (Int32)Session["IdProfile"]; } set { Session["IdProfile"] = value; } }
        public String NameUser { get { return (String)Session["NameUser"]; } set { Session["NameUser"] = value; } }
        public String Language { get { return (String)Session["Language"]; }}// set { Session["Language"] = value; } }
        public Int32 idLanguage { get { return (Int32)Session["idLanguage"]; }}//  }//  set { Session["idLanguage"] = value; } } }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                this.LoadSetting();
                this.LoadLanguage();
            }
        }

        protected void btnAdd_Click(object sender, DirectEventArgs e)
        {
            this.Clear();
            this.winSettingEdit.Show();
            this.LoadRoles();
        }

        protected void btnModel_Click(object sender, DirectEventArgs e)
        {
            Response.Redirect("/caseselection.aspx");
        }

        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            if (ValidForm())
            {
                this.SaveSetting(Convert.ToInt32(IdProfile));
                //this.LoadSetting();
                String rowsValues = e.ExtraParams["rowsValues"];
                this.SaveRoles(rowsValues);
                this.Clear();
                this.winSettingEdit.Hide();
            }
            else
            {
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources","Enter records correctly!"), IconCls = "icon-exclamation", Clear2 = false });
            }
        }

        protected void grdSetting_Command(object sender, DirectEventArgs e)
        {
            String Command = e.ExtraParams["command"].ToString();
            IdProfile = int.Parse(e.ExtraParams["IdProfile"].ToString());
            String _Name = (String)e.ExtraParams["Name"].ToString();
            if (Command == "Edit")
            {
                this.ddlModels.Value = 0;
                //this.txtName.Text = "";
                this.winSettingEdit.Show();
                this.EditSettingLoad(IdProfile);
                this.LoadRoles();
                //this.ddlModels.Select(0);
            }
            else
            {
                X.Msg.Confirm(modMain.strCommonConfirm, (String)GetGlobalResourceObject("CPM_Resources", "Are you sure you want to do delete the record:") + " " + _Name + "?", new Ext.Net.MessageBoxButtonsConfig
                {
                    Yes = new MessageBoxButtonConfig
                    {
                        Handler = "UsersX.ClickedYES()",
                        Text = modMain.strCommonYes
                    },
                    No = new MessageBoxButtonConfig
                    {
                        Text = modMain.strCommonNo
                    }
                }).Show();
            }
        }

        protected void ddlModels_Select(object sender, DirectEventArgs e)
        {
            this.LoadRoles();
        }

        [DirectMethod]
        public void ClickedYES()
        {
            this.DeleteRoles(IdProfile);
            this.DeleteSetting(IdProfile);
            this.LoadSetting();
        }

        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            this.winSettingEdit.Hide();
            this.Clear();
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        #endregion

        #region Methods

        protected void StoreModels_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsModels objModels = new DataClass.clsModels();
            this.storeModels.DataSource = objModels.LoadListWithGeneral("", "ORDER BY SortOrder, Name");
            this.storeModels.DataBind();
        }

        protected void StoreType_ReadData(object sender, StoreReadDataEventArgs e)
        {
            DataClass.clsParameters objParameters = new DataClass.clsParameters();
            this.storeType.DataSource = objParameters.LoadList("Type = 'ProfileOption' AND IdLanguage = " + this.LoadLanguageId(), "ORDER BY Name");
            this.storeType.DataBind();
        }

        protected void EditSettingLoad(Int32 IdProfile)
        {
            DataClass.clsProfiles objProfiles = new DataClass.clsProfiles();

            DataTable dt = objProfiles.LoadList("IdProfile = " + IdProfile, "");
            if (dt.Rows.Count > 0)
            {
                IdProfile = (Int32)dt.Rows[0]["IdProfile"];
                this.txtName.Text = (String)dt.Rows[0]["Name"];
                this.ddlType.Value = (Int32)dt.Rows[0]["Type"];
            }
        }

        protected Int32 LoadLanguageId()
        {
            Int32 IdLanguage = 0;
            DataClass.clsLanguages objLanguages = new DataClass.clsLanguages();

            DataTable dt = objLanguages.LoadList("Name = '" + Language + "'", "");
            if (dt.Rows.Count > 0)
            {
                IdLanguage = (Int32)dt.Rows[0]["IdLanguage"]; ;
            }
            return IdLanguage;
        }

        protected void LoadSetting()
        {
            DataClass.clsProfiles objProfiles = new DataClass.clsProfiles();
            DataTable dt = objProfiles.LoadList("", "ORDER BY Name");
            storeProfiles.DataSource = dt;
            storeProfiles.DataBind();
        }

        protected void LoadRoles()
        {
            DataClass.clsProfileRoles objProfileRoles = new DataClass.clsProfileRoles();

            Int32 IdModel = 0;
            if (ddlModels.SelectedItem.Value != null)
            {
                IdModel = int.Parse(ddlModels.SelectedItem.Value);
            }

            objProfileRoles.IdModel = IdModel;
            objProfileRoles.IdProfile = IdProfile;
            objProfileRoles.IdLanguage = idLanguage;
            DataTable dt = objProfileRoles.LoadProfileRoles();

            storeRoles.DataSource = dt;
            storeRoles.DataBind();
        }

        protected void Clear()
        {
            this.IdProfile = 0;
            this.txtName.Text = "";
            this.ddlModels.Reset();
            this.ddlType.Reset();
            this.ddlType.Value = 2;
            this.ddlModels.Value = 0;
        }

        protected Boolean ValidForm()
        {
            Boolean Valid = true;
            if (this.txtName.Text == "")
            {
                Valid = false;
            }
            
            return Valid;
        }

        protected Int32 Valid()
        {
            Int32 Value = 0;

            DataClass.clsProfiles objProfiles = new DataClass.clsProfiles();
            DataTable dt = objProfiles.LoadList("Name = '" + txtName.Text + "'", "");
            Value = dt.Rows.Count;

            return Value;
        }

        protected void SaveSetting(Int32 ID)
        {
            DataClass.clsProfiles objProfiles = new DataClass.clsProfiles();
            objProfiles.IdProfile = (Int32)ID;
            objProfiles.loadObject();
            if (IdProfile == 0)
            {
                if (this.Valid() > 0)
                {
                    this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                    return;
                }
            }
            else
            {
                if (objProfiles.Name != txtName.Text)
                {
                    if (this.Valid() > 0)
                    {
                        this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "The record already exists"), IconCls = "icon-exclamation", Clear2 = false });
                        return;
                    }
                }
            }

            objProfiles.Name = this.txtName.Text;
            objProfiles.Type = Convert.ToInt32(this.ddlType.SelectedItem.Value);
            if (ID == 0)
            {
                objProfiles.DateCreation = DateTime.Now;
                objProfiles.UserCreation = (Int32)IdUserCreate;
                IdProfile = objProfiles.Insert();
            }
            else
            {
                objProfiles.DateModification = DateTime.Now;
                objProfiles.UserModification = (Int32)IdUserCreate;
                objProfiles.Update();
            }
            this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = modMain.strCommonFormValid, IconCls = "icon-accept", Clear2 = false });
        }

        protected void SaveRoles(String strValue)
        {
            DataTable dt = JSON.Deserialize<DataTable>(strValue);
            foreach (DataRow row in dt.Rows)
            {
                DataClass.clsProfileRoles objProfileRoles = new DataClass.clsProfileRoles();

                objProfileRoles.IdProfileRoles = Convert.ToInt32(row["IdProfileRoles"]);
                objProfileRoles.loadObject();

                objProfileRoles.IdProfile = IdProfile;
                objProfileRoles.IdModel = Convert.ToInt32(ddlModels.SelectedItem.Value);
                objProfileRoles.IdRole = Convert.ToInt32(row["IdRole"]);
                objProfileRoles.DenyAccess = (Boolean)row["DenyAccess"];
                if (objProfileRoles.DenyAccess)
                {
                    objProfileRoles.GrantAccess = false;
                }
                else
                {
                    objProfileRoles.GrantAccess = (Boolean)row["GrantAccess"];
                }

                if (Convert.ToInt32(row["IdProfileRoles"]) == 0)
                {
                    if ((Boolean)row["DenyAccess"] == true || (Boolean)row["GrantAccess"] == true)
                    {
                        objProfileRoles.DateCreation = DateTime.Now;
                        objProfileRoles.UserCreation = (Int32)IdUserCreate;
                        objProfileRoles.Insert();
                    }
                }
                else
                {
                    if ((Boolean)row["DenyAccess"] == false && (Boolean)row["GrantAccess"] == false)
                    {
                        objProfileRoles.Delete();
                    }
                    else
                    {
                        objProfileRoles.DateModification = DateTime.Now;
                        objProfileRoles.UserModification = (Int32)IdUserCreate;
                        objProfileRoles.Update();
                    }
                }
            }
        }

        public void DeleteSetting(Int32 ID)
        {
            try
            {
                DataClass.clsProfiles objProfiles = new DataClass.clsProfiles();

                objProfiles.IdProfile = (Int32)ID;
                objProfiles.Delete();
                this.FormStatusBar.SetStatus(new StatusBarStatusConfig { Text = (String)GetGlobalResourceObject("CPM_Resources", "Record deleted successfully!"), IconCls = "icon-accept", Clear2 = false });
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("FK_tblUsers_tblProfiles"))
                X.Msg.Show(new MessageBoxConfig
                {
                    Title = modMain.strCommonDelete,
                    Message = (String)GetGlobalResourceObject("CPM_Resources", "A user is using this profile"),
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                });
            }
        }

        public void DeleteRoles(Int32 ID)
        {
            DataClass.clsProfileRoles objProfileRoles = new DataClass.clsProfileRoles();

            objProfileRoles.IdProfile = (Int32)ID;
            objProfileRoles.DeleteRoles();
        }

        protected void LoadLanguage()
        {
            DataClass.clsLabelsLanguages objLabelsLanguages = new DataClass.clsLabelsLanguages();
            DataTable dt = objLabelsLanguages.LoadList("LanguageName = '" + Language + "' AND FormName = 'RolesPermissions'", "ORDER BY IdLabel");
            foreach (DataRow row in dt.Rows)
            {
                switch ((String)row["LabelName"])
                {
                    case "Add New Role":
                        modMain.strSettingsAddNewRole = (String)row["LabelText"];
                        break;
                    case "Case Selection":
                        modMain.strSettingsCaseSelection = (String)row["LabelText"];
                        break;
                    case "Roles":
                        modMain.strSettingsRoles = (String)row["LabelText"];
                        break;
                    case "Role Type":
                        modMain.strSettingsRoleType = (String)row["LabelText"];
                        break;
                    case "Selected Case":
                        modMain.strSettingsSelectedCase = (String)row["LabelText"];
                        break;
                    case "Selected Role Permissions":
                        modMain.strSettingsSelectedRolePermissions = (String)row["LabelText"];
                        break;
                    case "Grant All":
                        modMain.strSettingsGrantAll = (String)row["LabelText"];
                        break;
                    case "Deny All":
                        modMain.strSettingsDenyAll = (String)row["LabelText"];
                        break;
                    case "Clear All":
                        modMain.strSettingsClearAll = (String)row["LabelText"];
                        break;
                    case "Grant Access":
                        modMain.strSettingsGrantAccess = (String)row["LabelText"];
                        break;
                    case "Deny Access":
                        modMain.strSettingsDenyAccess = (String)row["LabelText"];
                        break;
                    case "Role":
                        modMain.strSettingsRole = (String)row["LabelText"];
                        break;
                }
            }
            this.LoadLabel();
        }

        protected void LoadLabel()
        {
            //TopBar
            this.btnAdd.Text = "<B>" + modMain.strSettingsAddNewRole + "</B>";
            this.btnAdd.ToolTip = modMain.strSettingsAddNewRole;
            this.btnModels.Text = "<B>" + modMain.strSettingsCaseSelection + "</B>";
            this.btnModels.ToolTip = modMain.strSettingsCaseSelection;

            //GridPanel
            this.grdSettings.Title = modMain.strSettingsRoles;
            this.Name.Text = modMain.strCommonName;
            this.ImageCommandColumn1.Text = modMain.strCommonFunctions;
            this.ImageCommandColumn1.Commands[0].Text = modMain.strCommonEdit; //Edit option
            this.ImageCommandColumn1.Commands[1].Text = modMain.strCommonDelete; //Delete option

        //    //Window Popup
            this.winSettingEdit.Title = modMain.strSettingsRole;
            this.txtName.FieldLabel = modMain.strCommonName;
            this.ddlType.FieldLabel = modMain.strSettingsRoleType;
            this.ddlModels.FieldLabel = modMain.strSettingsSelectedCase;
            this.FieldSet1.Title = modMain.strSettingsRoles;
            this.grdRoles.Title = modMain.strSettingsSelectedRolePermissions;
            this.btnGrantAll.Text = modMain.strSettingsGrantAll;
            this.btnDenyAll.Text = modMain.strSettingsDenyAll;
            this.btnClearAll.Text = modMain.strSettingsClearAll;
            this.RoleName.Text = modMain.strCommonName;
            this.chkGrantAccess.Text = modMain.strSettingsGrantAccess;
            this.chkDenyAccess.Text = modMain.strSettingsDenyAccess;
            this.btnSave.Text = modMain.strCommonSave;
            this.btnCancel.Text = modMain.strCommonClose;
        }

        #endregion

    }
}
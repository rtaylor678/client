﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_master/default.master" AutoEventWireup="true" CodeBehind="caseselection.aspx.cs" Inherits="CPM._caseselection" culture="auto" uiculture="auto"%>

<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    protected override void InitializeCulture()
    {
        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];
        }
        base.InitializeCulture();
    }
</script>

<asp:Content ID="ContentCenter" ContentPlaceHolderID="cntCenter" runat="server">

    <ext:Panel ID="pnlTitlePanel" Layout="FitLayout" AutoScroll="true" runat="server" BodyStyle="background-color: transparent;">
        <TopBar>
            <ext:Toolbar Height="40" ID="tbMenu" runat="server" Flat="true" Cls="menubackground">
                <Items>
                    <ext:ToolbarSeparator runat="server" ID="sepAddModel" Border="false" Width="2" />
                    <ext:Button ID="btnAdd" runat="server" Disabled="true" IconCls="menu-addnew" Scale="Large" ClientIDMode="Inherit">
                        <DirectEvents>
                            <Click OnEvent="btnAdd_Click">
                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:ToolbarSeparator />
                    <ext:Button ID="btnMultiFieldsProjection" runat="server" Disabled="false" IconCls="menu-MultiFieldsReport" Text="Multiple Models Projection" Scale="Large" ClientIDMode="Inherit">
                        <DirectEvents>
                            <Click OnEvent="btnMultiFieldsProjection_Click">
                                <EventMask ShowMask="true" Target="Body" CustomTarget="vpMain" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </TopBar>
        <Items>
            <ext:GridPanel ID="grdModels" runat="server" Border="false" Title="Models" Icon="World" Header="false">
                <Store>
                    <ext:Store ID="storeModel" runat="server">
                        <Model>
                            <ext:Model ID="modelModels" runat="server">
                                <Fields>
                                    <ext:ModelField Name="IdModel" />
                                    <ext:ModelField Name="Name" />
                                    <ext:ModelField Name="CreatedBy" Type="String" />
                                    <ext:ModelField Name="DateCreation" Type="Date" />
                                    <ext:ModelField Name="BaseYear" Type="Int" />
                                    <ext:ModelField Name="Projection" Type="String" />
                                    <ext:ModelField Name="StatusName" Type="String" />
                                    <ext:ModelField Name="Notes" Type="String" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:Column ID="gIdModel" Hidden="true" runat="server" Width="70" Text="ID" DataIndex="IdModel"></ext:Column>
                        <ext:ImageCommandColumn Align="Center" Width="70" ID="ImageCommandColumn1" runat="server">
                            <Commands>
                                <ext:ImageCommand CommandName="Select" IconCls="icon-open_16" />
                            </Commands> 
                            <DirectEvents>                                       
                                <Command OnEvent="grdModels_Command">
                                    <ExtraParams>  
                                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>  
                                            <ext:Parameter Name="IdModel" Value="record.data.IdModel" Mode="Raw"></ext:Parameter> 
                                            <ext:Parameter Name="Name" Value="record.data.Name" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="Status" Value="record.data.StatusName" Mode="Raw"></ext:Parameter>
                                    </ExtraParams> 
                                </Command>
                            </DirectEvents>
                        </ext:ImageCommandColumn>
                        <ext:Column ID="gName" runat="server" Text="Name" DataIndex="Name" Flex="1"></ext:Column>
                        <ext:Column ID="gCreatedBy" runat="server" Text="Created By" Width="150" DataIndex="CreatedBy"></ext:Column>
                        <ext:DateColumn ID="gDateCreations" Align="Center" runat="server" Text="Date" DataIndex="DateCreation" Format="yyyy-MM-dd"></ext:DateColumn>
                        <ext:Column ID="gBaseYear" Align="Center" runat="server" Text="Base Year" DataIndex="BaseYear"></ext:Column>
                        <ext:Column ID="gProjection" Align="Center" runat="server" Text="Projection" DataIndex="Projection"></ext:Column>
                        <ext:Column ID="gNotes" runat="server" Text="Notes" DataIndex="Notes" Flex="1"></ext:Column>
                        <ext:Column ID="gStatusName" Align="Left" runat="server" Text="Status" DataIndex="StatusName"></ext:Column>
                        <ext:ImageCommandColumn Align="Center" Width="140" Text="Functions" ID="cmdAdminModels" runat="server">
                            <Commands>
                                <ext:ImageCommand CommandName="Edit" IconCls="icon-edit_16" />
                            </Commands> 
                                <Commands>
                                <ext:ImageCommand CommandName="Delete" IconCls="icon-delete_16" />
                            </Commands> 
                            <DirectEvents>                                       
                                <Command OnEvent="grdModels_Command">
                                    <ExtraParams>  
                                            <ext:Parameter Name="command" Value="command" Mode="Raw"></ext:Parameter>  
                                            <ext:Parameter Name="IdModel" Value="record.data.IdModel" Mode="Raw"></ext:Parameter> 
                                            <ext:Parameter Name="Name" Value="record.data.Name" Mode="Raw"></ext:Parameter>
                                            <ext:Parameter Name="Status" Value="record.data.StatusName" Mode="Raw"></ext:Parameter>
                                        </ExtraParams> 
                                </Command>
                            </DirectEvents>
                        </ext:ImageCommandColumn>
                    </Columns>
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Multi" />
                </SelectionModel>
                <Features>
                    <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                        <Filters>
                            <ext:StringFilter DataIndex="Name" />
                            <ext:StringFilter DataIndex="CreatedBy" />
                            <ext:DateFilter DataIndex="DateCreation"></ext:DateFilter>
                            <ext:NumericFilter DataIndex="BaseYear" />
                            <ext:StringFilter DataIndex="Projection" />
                            <ext:StringFilter DataIndex="Notes" />
                            <ext:StringFilter DataIndex="StatusName" />
                        </Filters>
                    </ext:GridFilters>
                </Features>         
            </ext:GridPanel>            
        </Items>
    </ext:Panel>
    <ext:Window ID="winModelEdit" runat="server" Title="Case Details" Hidden="true" IconCls="icon-edit_16" Resizable="false" Width="500" Modal = "true" Closable="false" BodyPadding="5">
        <Items>
            <ext:FormPanel ID="pnModel" runat="server" Title=" " BodyPadding="10" Hidden="false" ButtonAlign="Center" Layout="Column">
            <Items>
                <ext:Panel ID="FieldSet1" runat="server" Border="false" Layout="Form" ColumnWidth="1" Cls="PopupFormColumnPanel">
                    <Defaults>
                        <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                        <ext:Parameter Name="MsgTarget" Value="side" />
                    </Defaults>
                    <Items>
                        <ext:FieldContainer ID="FieldConteinerBaseModel" runat="server" Layout="HBoxLayout">
                            <Items>
                                <ext:Checkbox runat="server" ID="chkBaseModel" FieldLabel="Base Case" Checked="false" Cls="PopupFormField">
                                    <Listeners>
                                        <Change Handler="if (!this.checked) {#{ddlBaseModel}.hide();}else{#{ddlBaseModel}.show();}"></Change>
                                    </Listeners>
                                    <DirectEvents>
                                        <Change OnEvent="chkBaseModel_OnChange" />
                                    </DirectEvents>
                                </ext:Checkbox>
                                <ext:ComboBox ID="ddlBaseModel" runat="server" DisplayField="Name" Editable="false" Hidden="true" TriggerAction="All" ValueField="ID" Style="margin-left: 10px;" Cls="PopupFormField" Width="308">
                                    <Store>
                                        <ext:Store ID="storeBaseModel" runat="server" OnReadData="StoreModel_ReadData">
                                            <Model>
                                                <ext:Model ID="modelBaseModel" runat="server" IDProperty="ID">
                                                    <Fields>
                                                        <ext:ModelField Name="ID" />
                                                        <ext:ModelField Name="Name" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                            <Proxy>
                                                <ext:PageProxy>
                                                    <Reader>
                                                        <ext:JsonReader  />
                                                    </Reader>
                                                </ext:PageProxy>
                                            </Proxy>
                                        </ext:Store>
                                    </Store>
                                    <DirectEvents>
                                        <Select OnEvent="ddlBaseModel_Select" />
                                    </DirectEvents>
                                </ext:ComboBox>
                            </Items>
                        </ext:FieldContainer>
                        <ext:TextField ID="txtName" runat="server" FieldLabel="Name" AnchorHorizontal="92%" Cls="PopupFormField" MaxLengthText="100" />
                        <ext:TextField ID="txtCreateBy" ReadOnly="true" runat="server" FieldLabel="Created By" AnchorHorizontal="92%" Cls="PopupFormField" />
                        <ext:DateField ID="txtCreateDate" ReadOnly="true" FieldLabel="Created" runat="server" Cls="PopupFormField" />
                        <ext:ComboBox ID="ddlBaseYear" runat="server" DisplayField="Name" Editable="false" TriggerAction="All" FieldLabel="Base Year" ValueField="ID" Cls="PopupFormField">
                            <Store>
                                <ext:Store ID="storeBaseYear" runat="server" OnReadData="StoreBaseYear_ReadData">
                                    <Model>
                                        <ext:Model ID="modelBaseYear" runat="server" IDProperty="ID">
                                            <Fields>
                                                <ext:ModelField Name="ID" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                    <Proxy>
                                        <ext:PageProxy>
                                            <Reader>
                                                <ext:JsonReader  />
                                            </Reader>
                                        </ext:PageProxy>
                                    </Proxy>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <ext:ComboBox ID="ddlProjection" runat="server" DisplayField="Name" Editable="false" TriggerAction="All" FieldLabel="Projection" ValueField="ID" Cls="PopupFormField">
                            <Store>
                                <ext:Store ID="storeProjection" runat="server" OnReadData="StoreProjection_ReadData">
                                    <Model>
                                        <ext:Model ID="modelProjection" runat="server" IDProperty="ID">
                                            <Fields>
                                                <ext:ModelField Name="ID" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                    <Proxy>
                                        <ext:PageProxy>
                                            <Reader>
                                                <ext:JsonReader  />
                                            </Reader>
                                        </ext:PageProxy>
                                    </Proxy>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <ext:ComboBox ID="ddlStatus" runat="server" DisplayField="Name" Editable="false" TriggerAction="All" FieldLabel="Status" ValueField="Code" Cls="PopupFormField">
                            <Store>
                                <ext:Store ID="storeStatus" runat="server" OnReadData="StoreStatus_ReadData">
                                    <Model>
                                        <ext:Model ID="modelStatus" runat="server" IDProperty="Code">
                                            <Fields>
                                                <ext:ModelField Name="Code" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                    <Proxy>
                                        <ext:PageProxy>
                                            <Reader>
                                                <ext:JsonReader  />
                                            </Reader>
                                        </ext:PageProxy>
                                    </Proxy>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                        <ext:ComboBox ID="ddlLevel" runat="server" DisplayField="Name" Editable="false" TriggerAction="All" FieldLabel="Level" ValueField="Code" Cls="PopupFormField">
                            <Store>
                                <ext:Store ID="storeLevel" runat="server" OnReadData="StoreLevel_ReadData">
                                    <Model>
                                        <ext:Model ID="modelLevel" runat="server" IDProperty="Code">
                                            <Fields>
                                                <ext:ModelField Name="Code" />
                                                <ext:ModelField Name="Name" />
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                    <Proxy>
                                        <ext:PageProxy>
                                            <Reader>
                                                <ext:JsonReader  />
                                            </Reader>
                                        </ext:PageProxy>
                                    </Proxy>
                                </ext:Store>
                            </Store>
                        </ext:ComboBox>
                    </Items>
                </ext:Panel>
                <ext:Panel ID="FieldSet2" runat="server" Border="false" Layout="Form" ColumnWidth="1" Cls="PopupFormColumnPanel">
                    <Items>
                        <ext:TextArea ID="txtNote" runat="server" AllowBlank="true" FieldLabel="Note" Rows="4" Cls="PopupFormField" MaxLengthText="255" />
                    </Items>
                </ext:Panel>
            </Items>
            <Buttons>
                <ext:Button ID="btnSave" runat="server" Text="Save" icon="DatabaseSave" Disabled="true" FormBind="true">
                    <DirectEvents>
                        <Click OnEvent="btnSave_Click">
                            <EventMask ShowMask="true" Msg="Saving changes..." />
                        </Click> 
                    </DirectEvents>
                </ext:Button>
                <ext:Button ID="btnCancel" runat="server" icon="Decline" Text="Close">
                    <DirectEvents>
                        <Click OnEvent="btnCancel_Click" />
                    </DirectEvents>
                </ext:Button>
            </Buttons>
            <BottomBar>
                <ext:StatusBar ID="FormStatusBar" runat="server" />
            </BottomBar>
            <Listeners>
                <ValidityChange Handler="this.dockedItems.get(1).setStatus({
                                            text : valid ? 'Form is valid' : 'Form is invalid', 
                                            iconCls: valid ? 'icon-accept' : 'icon-exclamation'
                                        });
                                        #{btnSave}.setDisabled(!valid);" />
            </Listeners>
        </ext:FormPanel>
        </Items>
    </ext:Window>

</asp:Content>